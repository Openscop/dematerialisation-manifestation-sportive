# État actuel du système disciplines-fédérations-agents

À l'heure actuelle, deux mécanismes sont à revoir dans l'application :
1. Le système de hiérarchie entre disciplines sportives et fédérations
2. Le système qui désigne les agents d'une fédération pour donner un aval sur l'organisation de l'événement.

Une discipline sportive est liée à une seule fédération (dans ce système, le football ne peut pas être
lié à la fois à la FFT et aux fédérations départementales). Également, lorsq'une manifestation est créée dans le système,
on sait immédiatement à quelle fédération on demande un avis (ce serait différent s'il existait plusieurs fédérations pour la même
activité, il faudrait choisir la fédération à laquelle on demande la permission).


Pour la version 3 de l'application, on souhaite étendre le système de la façon suivante :
1. Les disciplines sportives peuvent être l'objet de plusieurs fédérations. Le football sera géré par une fédération par département,
mais il peut aussi y avoir des fédérations régionales et une fédération nationale en jeu.  
2. Les avis de fédération qui sont créés par défaut pour toutes les manifestations, vont soit être dispatchés par défaut (demande à la fédération
départementale par défaut), soit être dispatchées via action de l'utilisateur. Cette information est à définir.

Si le choix de fédération n'est plus automatique, alors le workflow des manifestations doit changer pour refléter le besoin de choix.
