
$(document).ready(function () {

    $('#submit-id-submit').attr('disabled', true);
    var voiepublique;
    var emprise;
    var type_circuit;
    $('#div_id_circuit').hide();
    $('#div_id_type_circuit').hide();

    if ($('#id_departement').val() != "") {
        $('#id_departement').trigger("change");
        $('#id_departement').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen
    }

    // Ajouter une div pour l'affichage des icones
    $('#div_id_discipline').append('<div class="col-sm-2 text-end disc-icone"></div>');
    $('#div_id_emprise').append('<div class="col-sm-2 text-end voie-icone"></div>');
    $('#div_id_nb_participants').append('<div class="col-sm-2 text-end nbp-icone"></div>');
    $('#div_id_competition').parent().parent().append('<div class="col-sm-2 text-end comp-icone"></div>');
    $('#div_id_type_circuit').append('<div class="col-sm-2 text-end circ-icone"></div>');
    var ivtm = '<span class="fa-stack" data-bs-toggle="tooltip" title="Avec véhicule terrestre à moteur"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fa fa-car fa-stack-1x fa-inverse"></i></span>';
    var icycl = '<span class="fa-stack" data-bs-toggle="tooltip" title="Cyclisme"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fas fa-bicycle fa-stack-1x fa-inverse"></i></span>';
    var i_vtm = '<span class="fa-stack" data-bs-toggle="tooltip" title="Sans véhicule terrestre à moteur"><i class="fa fa-car fa-stack-1x"></i><i class="fa fa-ban fa-stack-2x text-danger"></i>';
    var ivoie = '<span class="fa-stack" data-bs-toggle="tooltip" title="Sur voie publique"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fa fa-road fa-stack-1x fa-inverse"></i></span>';
    var i_voie = '<span class="fa-stack" data-bs-toggle="tooltip" title="Hors voie publique"><i class="fa fa-road fa-stack-1x"></i><i class="fa fa-ban fa-stack-2x text-danger"></i></span>';
    var icirc = '<span class="fa-stack" data-bs-toggle="tooltip" title="Cicuit non permanent"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fa fa-route fa-stack-1x fa-inverse"></i></span>';
    var ihomo = '<span class="fa-stack" data-bs-toggle="tooltip" title="Cicuit homologue"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fa fa-draw-polygon fa-stack-1x fa-inverse"></i></span>';
    var i_homo = '<span class="fa-stack" data-bs-toggle="tooltip" title="Cicuit non homologue"><i class="fa fa-draw-polygon fa-stack-1x"></i><i class="fa fa-ban fa-stack-2x text-danger"></i></span>';
    var icomp = '<span class="fa-stack" data-bs-toggle="tooltip" title="Compétition"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fa fa-trophy fa-stack-1x fa-inverse"></i></span>';
    var i_comp = '<span class="fa-stack" data-bs-toggle="tooltip" title="Sans compétition"><i class="fa fa-trophy fa-stack-1x"></i><i class="fa fa-ban fa-stack-2x text-danger"></i></span>';
    var igroup = '<span class="fa-stack" data-bs-toggle="tooltip" title="Circulation groupée"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fa fa-users fa-stack-1x fa-inverse"></i></span>';

    function recherche_formulaire() {

        // DVTM
        if (vtm == true && !$('[name="circuit"]').is(':checked') && (emprise == _emprise.partiel || emprise == _emprise.total) &&
            !$('[name="competition"]').is(':checked') && $('[name="nb_participants"]').val() >= 50) {return "Dvtm";}

        // AVTM
        if (vtm == true && !$('[name="circuit"]').is(':checked')  && $('[name="competition"]').is(':checked')) {return "Avtm";}
        if (vtm == true && $('[name="circuit"]').is(':checked') && type_circuit == _circuit.non_permanent) {return "Avtm";}
        if (vtm == true && $('[name="circuit"]').is(':checked') && (type_circuit == _circuit.permanent_non_homologue ||
            type_circuit == _circuit.permanent_homologue_autre_discipline)) {return "Avtm";}

        // DVTMCIR
        if (vtm == true  && $('[name="circuit"]').is(':checked') && type_circuit == _circuit.permanent_homologue &&
            emprise == _emprise.hors) {return "Dvtmcir";}

        // DNM
        if (vtm == false && (emprise == _emprise.partiel || emprise == _emprise.total || emprise == _emprise.temp) &&
            !$('[name="competition"]').is(':checked') && $('[name="nb_participants"]').val() >= 100 &&
            ($('#id_discipline option:selected').text() !== 'Cyclisme' && $('#id_discipline option:selected').text() !== 'Cyclotourisme')){return "Dnm";}

        // DNMC
        if (vtm == false && (emprise == _emprise.partiel || emprise == _emprise.total || emprise == _emprise.temp) &&
            !$('[name="competition"]').is(':checked') && $('[name="nb_participants"]').val() >= 100 &&
            ($('#id_discipline option:selected').text() === 'Cyclisme' || $('#id_discipline option:selected').text() === 'Cyclotourisme')){return "Dnmc";}

        // DCNM
        if (vtm == false && (emprise == _emprise.partiel || emprise == _emprise.total || emprise == _emprise.temp) &&
            $('[name="competition"]').is(':checked') && $('#id_discipline option:selected').text() !== 'Cyclisme'){return "Dcnm";}

        //DCNMC
        if (vtm == false && (emprise == _emprise.partiel || emprise == _emprise.total || emprise == _emprise.temp) &&
            $('[name="competition"]').is(':checked') && $('#id_discipline option:selected').text() == 'Cyclisme'){return "Dcnmc";}

        return "";
    }
    $('#id_circuit').on('change', function (e){
        if ($(this).is(":checked")){
            $('#div_id_type_circuit').show();
        }
        else {
            $('#div_id_type_circuit').hide();
        }
    });


    function check_vtm(discipline) {
        for (var i in listevtm){
            if (discipline == listevtm[i]) {
                return true;
            }
        }
        return false;
    }

    function affich_discipl(motor) {
        if (motor == true) {

            $('#div_id_circuit').show();
            if ($("#id_circuit").is(":checked")){
                $('#div_id_type_circuit').show();
            }
            else {
                $('#div_id_type_circuit').hide();
            }
            $('.disc-icone').html(ivtm);
            if ($('[name="nb_participants"]').val() > 50) $('.nbp-icone').html(igroup);
        } else {
            $('#div_id_circuit').hide();
            $('#div_id_type_circuit').hide();
            if ($('#id_discipline option:selected').text() == 'Cyclisme') {
                $('.disc-icone').html(icycl);
            } else {
                $('.disc-icone').html(i_vtm);
            }
            if ($('[name="nb_participants"]').val() > 100) {
                $('.nbp-icone').html(igroup);
            } else {
                $('.nbp-icone').empty();
            }
        }
    }

    function get_page_aide(discipline) {
        // on va chercher l'eventuelle page d'aide
        $.get('/sports/ajax/discipline/aide/?pk='+discipline, function (data) {
            $('#aide_discipline').remove();
            if (data){
                let html = `<a id="aide_discipline" href="${data}" target="_blank" class="float-end">Consulter la page d'aide dédiée à cette discipline</a>`;
                $('#id_discipline').parent().append(html);
            }
        });
    }

    // Lorsqu'une sélection est effectuée dans le champ extra_activite
    $('#id_extra_activite').bind('added', function () {
        // Récupérer la valeur du champ (le nom exact de l'activité)
        var act_name = he.decode($(this).val());

        // Récupérer l'ID de la discipline en AJAX
        $.ajax({
            url: "/sports/ajax/discipline/by_activite_name/",
            type: 'GET',
            data: 'name=' + encodeURIComponent(act_name),
            dataType: 'html',
            success: function (data, status) {
                // On a récupéré l'ID de la discipline sous forme de chaîne

                // On sélectionne l'option du select de disciplines dont l'ID correspond
                // Cela va, via le javascript de clever-selects, peupler les options de
                // id_discipline. On va donc ensuite sélectionner la bonne option dans le
                // champ de disciplines
                $('#id_discipline').val(data);
                $('#id_discipline').trigger("change");
                $('#id_discipline').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen

                // Refaire le même type de selection sur le champ en cascade (activité)
                setTimeout(function () {
                    $.ajax({
                        url: "/sports/ajax/activite/by_name/",
                        type: 'GET',
                        data: 'name=' + act_name,
                        dataType: 'html',
                        success: function (data, status) {
                            // On a récupéré l'ID de la discipline sous forme de chaîne

                            // On sélectionne l'option du select d'activités dont l'ID correspond
                            $('#id_activite').val(data);
                            $('#id_activite').trigger("change");
                            $('#id_activite').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen
                        }
                    });
                }, 50);
            }
        });
        $(this).val('');

    });

    $('#searchform').on('click', function () {
        // Récupérer les données du formulaire
        var donnees = $('#evenement-form').serializeArray();
        /*
            jQuery.each(donnees, function (i, item) {
                console.log(item);
                console.log(item['name']);
                console.log(item['value']);
            });
        */
        // Controler les champs nécessaires
        var nb_departement = $('#id_nb_departement').val()
        var discipl = $('#id_discipline').val();
        var nb_part = $('[name="nb_participants"]').val();
        type_circuit = $('#id_type_circuit').val();
        emprise = $('#id_emprise').val();
        if (discipl == "" || nb_part == "" || emprise == _emprise.null || !nb_departement || ($('[name="circuit"]').is(':checked') && type_circuit == _circuit.null)) {
            $('#info-cerfa').html("Questionnaire incomplet pour la recherche du formulaire !");
            scroll_to("#info-cerfa");
        }
        else if (nb_departement > 20){
            $('#info-cerfa').html("Actuellement, la plateforme ne prend en charge que les manifestations de moins de 20 départements. Veuillez contacter le service instructeur de la Préfecture de votre département de départ ou le Ministère de l'Intérieur pour connaitre la démarche à suivre.");
            scroll_to("#info-cerfa");
        }
        else {

            form = recherche_formulaire();
            if (form !== "") {
                $('#info-cerfa').html(form);
                $('#submit-id-submit').attr('disabled', false);
                $('#id_formulaire').val(form);
                get_url = '/aide/CERFA/' + form + '?embed=true';
                $.get( get_url, function (data) {
                    $('#info-cerfa').html(data);
                    scroll_to("#info-cerfa");
                });
            } else {
                $('#info-cerfa').html("D’après les informations fournies ci-dessus et selon le Code du sport," +
                    "vous n'êtes pas soumis à la déclaration ou à une demande d’autorisation pour votre manifestation." +
                    " Toutefois, des démarches peuvent être nécessaires selon votre réglementation locale ou d’autres codes en vigueur.");
                scroll_to("#info-cerfa");
            }
        }
    });

    // Désactiver le bouton d'accès au formulaire et vider la div 'info-cerfa' quand le questionnaire change
    $(".form-control").on('change', function () {
        $('#submit-id-submit').attr('disabled', true);
        $('#info-cerfa').empty();
    });
    $(".form-check-input").on('change', function () {
        $('#submit-id-submit').attr('disabled', true);
        $('#info-cerfa').empty();
    });

    // Gérer les actions dûes aux différents champs
    // Champ discipline
    if ($('#id_discipline').val() != "") {
        $('#id_discipline').trigger("change");
        $('#id_discipline').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen
        var discipline = $('#id_discipline').val();
        vtm = check_vtm(discipline);
        affich_discipl(vtm);
        get_page_aide(discipline);
    }

    $('#id_discipline').on('change', function () {
        var discipline = $('#id_discipline').val();
        vtm = check_vtm(discipline);
        affich_discipl(vtm);
        if (discipline != "") {
            get_page_aide(discipline);
        }
    });

    // Champ compétition
    if ($('[name="competition"]').is(':checked')) {
        $('.comp-icone').html(icomp);
    } else {
        $('.comp-icone').html(i_comp);
    }

    $('#id_competition').on('change', function() {
        if ($('[name="competition"]').is(':checked')) {
            $('.comp-icone').html(icomp);
        } else {
            $('.comp-icone').html(i_comp);
        }
    });

    // Champ emprise
    if ($('#id_emprise').val() > _emprise.hors) {
        $('.voie-icone').html(ivoie);
    } else if ($('#id_emprise').val() == _emprise.hors) {
        $('.voie-icone').html(i_voie);
    } else {
        $('.voie-icone').empty();
    }

    $('#id_emprise').on('change', function() {
        if ($('#id_emprise').val() > _emprise.hors) {
            $('.voie-icone').html(ivoie);
        } else if ($('#id_emprise').val() == _emprise.hors) {
            $('.voie-icone').html(i_voie);
        } else {
            $('.voie-icone').empty();
        }
    });

    // Champ type_circuit
    if ($('#id_type_circuit').val() == _circuit.null) {
        $('.circ-icone').empty();
    } else if ($('#id_type_circuit').val() == _circuit.non_permanent) {
        $('.circ-icone').html(icirc);
    } else if ($('#id_type_circuit').val() == _circuit.permanent_homologue) {
        $('.circ-icone').html(ihomo);
    } else {
        $('.circ-icone').html(i_homo);
    }

    $('#id_type_circuit').on('change', function() {
        if ($('#id_type_circuit').val() == _circuit.null) {
            $('.circ-icone').empty();
        } else if ($('#id_type_circuit').val() == _circuit.non_permanent) {
            $('.circ-icone').html(icirc);
        } else if ($('#id_type_circuit').val() == _circuit.permanent_homologue) {
            $('.circ-icone').html(ihomo);
        } else {
            $('.circ-icone').html(i_homo);
        }
    });
});

