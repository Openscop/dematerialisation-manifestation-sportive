/**
 * Created by knt on 22/01/17.
 */
$(document).ready(function () {


    $('#id_departements_traverses').trigger("change");  // Provoquer la mise à jour du champ Communes traversées
    $('#id_departements_traverses').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen
    var quatre_option = false
    // Sinon dcnmc et pas épreuve de masse on va concerver que deux des options
    if (type!=="dcnmc" || $('#id_activite').val()==="123"){
        quatre_option = true
        $('#div_id_respect_code_route_detail').parent().find('legend').html('Régime demandé en matière de circulation publique <small>(un choix minimum obligatoire)</small>')
    }
    else {
        $('#div_id_respect_code_route_detail').parent().find('legend').html('Régime demandé en matière de circulation publique <small>(un choix obligatoire)</small>')
        $('#div_id_priorite_passage').hide()
        $('#div_id_respect_code_route').hide()
    }

    $("textarea").autogrow({flickering: false, horizontal: false});
    // Pour afficher le texte des cases à cocher sur toute la longueur disponible
    $(".switchToggle").each(function () {
        $(this).parent().removeClass('col-sm-6').addClass('col-sm-9');
    });

    // BOF Modification pour "joindre des documents"
    // Pour les champs FileField disabled, n'afficher que le lien vers le fichier chargé
    $('.clearablefileinput').each(function () {
        if ($(this).prop("disabled")) {
            var a = $(this).parent().contents().filter("a");
            $(this).parent().html('Actuellement: ').append(a);
        }
    });
    // EOF

    // BOF Modification pour l'édition de manifestation
    // Marquer les champs de classe "requis"
    $('.requis').each(function () {
        var div_id = '#div_' + $(this).attr('id');
        $(div_id).append('<div class="col-sm-3 text-center completer"></div>');
        if ($(this).val() == '') {
            $(div_id).children('.completer').append('<div><i class="a-completer"></i> à completer</div>');
        }
    });
    // Marquer les champs de classe "modifier"
    $('.modifier').each(function () {
        var div_id = '#div_' + $(this).attr('id');
        $(div_id).append('<div class="col-sm-3 text-center a-modifier"></div>');
        $(div_id).children('.a-modifier').append('<div><i class="a-completer"></i> à modifier</div>');
    });
    AfficherIcones();
    // Gérer le marquage en fonction du remplissage
    $('body').on('change', '.requis', function () {
        var div_id = '#div_' + $(this).attr('id');
        if ($(this).val() != '') {
            $(div_id).find('.completer').empty();
        } else {
            $(div_id).find('.completer').append('<div><i class="a-completer"></i> à completer</div>');
        }
        AfficherIcones();
    });
    // Gérer le marquage en fonction du remplissage pour les champs 'date'
    $('.requis').on('dp.change', function () {
        var div_id = '#div_' + $(this).attr('id');
        if ($(this).val() != '') {
            $(div_id).children('.completer').empty();
        } else {
            $(div_id).children('.completer').append('<div><i class="a-completer"></i> à completer</div>');
        }
        AfficherIcones();
    });
    // EOF

    // BOF Ajout du champ "Dossier technique cyclisme"
    $('#id_activite').parent().parent().after('<div id="id_cycling" class="form-group">' +
        '<label class="control-label col-sm-3">Dossier technique cyclisme</label>' +
        '<div class="controls col-sm-6" style="padding-top: 7px">' +
        'Veuillez d\'ores et déjà ' +
        '<a href="https://www.ffc.fr/wp-content/uploads/2018/01/Dossier-technique-manifestation-cycliste-2018.pdf" ' +
        'target="_blank"> télécharger le document vierge</a>' +
        '. Vous devrez le remplir et le joindre à votre demande.</div></div>');
    // On cache le champ par défaut
    $('#id_cycling').hide();

    // Cas d'édition
    if ($('#id_discipline option:selected').text() == 'Cyclisme') {
        $('#id_cycling').show();
    }
    // Cas de création
    $('#id_discipline').change(function () {
        if ($('#id_discipline option:selected').text() == 'Cyclisme') {
            $('#id_cycling').show(400);
        } else {
            $('#id_cycling').hide(200);
        }
    });
    // EOF

    // BOF Régime de circulation
    function check_traffic() {
        // Renvoie la liste des switches commutés
        var list = [false, false, false, false];
        if ($('[name="respect_code_route"]').is(':checked')) {
            list[0] = true;
        }
        if ($('[name="priorite_passage"]').is(':checked')) {
            list[1] = true;
        }
        if ($('[name="usage_temporaire"]').is(':checked')) {
            list[2] = true;
        }
        if ($('[name="usage_privatif"]').is(':checked')) {
            list[3] = true;
        }
        return list;
    }

    function set_detail(list) {
        // Affichage les champs "détails" pour chaque switch
        if (list[0]) {
            $('#div_id_respect_code_route_detail').show(400);
            $('#div_id_respect_code_route_detail').autogrow({flickering: false, horizontal: false});
        } else {
            $('#div_id_respect_code_route_detail').hide(400);
            $('#id_respect_code_route_detail').text('');
        }
        if (list[1]) {
            $('#div_id_priorite_passage_detail').show(400);
            $('#div_id_priorite_passage_detail').autogrow({flickering: false, horizontal: false});
        } else {
            $('#div_id_priorite_passage_detail').hide(400);
            $('#id_priorite_passage_detail').text('');
        }
        if (list[2]) {
            $('#div_id_usage_temporaire_detail').show(400);
            $('#div_id_usage_temporaire_detail').autogrow({flickering: false, horizontal: false});
        } else {
            $('#div_id_usage_temporaire_detail').hide(400);
            $('#id_usage_temporaire_detail').text('');
        }
        if (list[3]) {
            $('#div_id_usage_privatif_detail').show(400);
            $('#div_id_usage_privatif_detail').autogrow({flickering: false, horizontal: false});
        } else {
            $('#div_id_usage_privatif_detail').hide(400);
            $('#id_usage_privatif_detail').text('');
        }
    }

    function which_traffic(id) {
        // Renvoie le switch qui a communté parmi les quatre
        var list = [false, false, false, false];
        if (id === 'id_respect_code_route') {
            list[0] = true;
        }
        if (id === 'id_priorite_passage') {
            list[1] = true;
        }
        if (id === 'id_usage_temporaire') {
            list[2] = true;
        }
        if (id === 'id_usage_privatif') {
            list[3] = true;
        }
        return list
    }

    function set_traffic(list) {
        // Communte les switches
        if (list[0]) {
            $('#id_respect_code_route').prop("checked", true);
        }
        else {
            $('#id_respect_code_route').prop("checked", false);
        }
        if (list[1]) {
            $('#id_priorite_passage').prop("checked", true);
        }
        else {
            $('#id_priorite_passage').prop("checked", false);
        }
        if (list[2]) {
            $('#id_usage_temporaire').prop("checked", true);
        }
        else {
            $('#id_usage_temporaire').prop("checked", false);
        }
        if (list[3]) {
            $('#id_usage_privatif').prop("checked", true);
        }
        else {
            $('#id_usage_privatif').prop("checked", false);
        }
    }
    // Gérer l'affichage initial
    if ($('[name="respect_code_route_detail"]').text() != '') {
        $('#div_id_respect_code_route_detail').show();
        $('#div_id_respect_code_route_detail').autogrow({flickering: false, horizontal: false});
    } else {
        $('#div_id_respect_code_route_detail').hide();
    }
    if ($('[name="priorite_passage_detail"]').text() != '') {
        $('#div_id_priorite_passage_detail').show();
        $('#div_id_priorite_passage_detail').autogrow({flickering: false, horizontal: false});
    } else {
        $('#div_id_priorite_passage_detail').hide();
    }
    if ($('[name="usage_temporaire_detail"]').text() != '') {
        $('#div_id_usage_temporaire_detail').show();
        $('#div_id_usage_temporaire_detail').autogrow({flickering: false, horizontal: false});
    } else {
        $('#div_id_usage_temporaire_detail').hide();
    }
    if ($('[name="usage_privatif_detail"]').text() != '') {
        $('#div_id_usage_privatif_detail').show();
        $('#div_id_usage_privatif_detail').autogrow({flickering: false, horizontal: false});
    } else {
        $('#div_id_usage_privatif_detail').hide();
    }
    // Gérer l'affichage quand les switches sont commutés
    $('.traffic').click(function () {
        var previous = which_traffic($(this).attr('id'));
        var etat = check_traffic();
        if (!quatre_option){
            etat[0] = false
            etat[1] = false
        }
        if (!(etat[0] | etat[1] | etat[2] | etat[3])) {
            if (quatre_option){
                // pas de switch, on communte le suivant
                set_traffic([previous[3], previous[0], previous[1], previous[2]]);
            }
            else {
                // on active l'autre dans le car des sans masse
                if ($(this).attr('id')==="id_usage_privatif"){
                    set_traffic([false, false, true, false])
                }
                else {
                    set_traffic([false, false, false, true])
                }
            }
            // alert('Vous devez sélectionner au moins un régime');
        } else {
            if (!quatre_option){
                if (etat[2] && etat[3]){
                    if ($(this).attr('id')==="id_usage_privatif"){
                        etat[2] = false
                    }
                    else {
                        etat[3] = false
                    }
                    set_traffic(etat)
                }
            }

            var prev = etat[0];
            var xor = true;
            for (var i = 1; i < etat.length; i++) {
                if (prev == 1 & etat[i] == 1) {
                    // plus de un switch, on affiche le détail pour chaque
                    xor = false;
                    set_detail(etat);
                    break;
                }
                prev = prev | etat[i];
            }
            // un seul switch, pas de détails
            if (xor) {
                set_detail([0, 0, 0, 0]);
            }
        }
    });
    // EOF

    // BOF Dispositif de sécurité
    // Gérer l'affichage du champ "moyens affectés" si le switch est true initialement
    if ($('[name="police_municipale"]').is(':checked')) {
        $('#div_id_detail_police_municipale').show();
        $('#div_id_detail_police_municipale').autogrow({flickering: false, horizontal: false});
    } else {
        $('#div_id_detail_police_municipale').hide();
    }
    if ($('[name="police_nationale"]').is(':checked')) {
        $('#div_id_detail_police_nationale').show();
        $('#div_id_detail_police_nationale').autogrow({flickering: false, horizontal: false});
        $('#id_detail_police_nationale').attr("placeholder", "Vous devez inclure votre convention dans les pièces jointes");
    } else {
        $('#div_id_detail_police_nationale').hide();
    }
    // Gérer l'affichage du champ "moyens affectés" si le switch est commuté
    $('#id_police_municipale').click(function () {
        $('#div_id_detail_police_municipale').toggle(400);
        $('#div_id_detail_police_municipale').autogrow({flickering: false, horizontal: false});
    });
    $('#id_police_nationale').click(function () {
        $('#div_id_detail_police_nationale').toggle(400);
        $('#div_id_detail_police_nationale').autogrow({flickering: false, horizontal: false});
        $('#id_detail_police_nationale').attr("placeholder", "Vous devez inclure votre convention dans les pièces jointes");
    });
    // Gérer l'affichage des signaleurs si le nombre est rempli initialement
    if ($('[name="nb_signaleurs"]').val() != '' && $('[name="nb_signaleurs"]').val() != 0) {
        $('#div_id_nb_signaleurs_fixes').show();
        $('#div_id_nb_signaleurs_autos').show();
        $('#div_id_nb_signaleurs_motos').show();
    } else {
        $('#div_id_nb_signaleurs_fixes').hide();
        $('#div_id_nb_signaleurs_autos').hide();
        $('#div_id_nb_signaleurs_motos').hide();
    }
    // Gérer l'affichage des signaleurs si le nombre est rempli
    $('[name="nb_signaleurs"]').on("change", function () {
        if ($('[name="nb_signaleurs"]').val() != '' && $('[name="nb_signaleurs"]').val() != 0) {
            $('#div_id_nb_signaleurs_fixes').show(400);
            $('#div_id_nb_signaleurs_autos').show(400);
            $('#div_id_nb_signaleurs_motos').show(400);
        } else {
            $('#div_id_nb_signaleurs_fixes').hide(400);
            $('#div_id_nb_signaleurs_autos').hide(400);
            $('#div_id_nb_signaleurs_motos').hide(400);
        }
    });
    // EOF

    // BOF swithes fédération
    // Gérer l'affichage si le switch est true initialement
    if ($('[name="dans_calendrier"]').is(':checked')) {
        $('#div_id_avec_convention_fede').hide();
    } else {
        $('#div_id_avec_convention_fede').show();
    }
    if ($('[name="avec_convention_fede"]').is(':checked')) {
        $('#div_id_dans_calendrier').hide();
    } else {
        $('#div_id_dans_calendrier').show();
    }
    // Gérer l'affichage si le switch est commuté
    $('#id_dans_calendrier').click(function () {
        $('#div_id_avec_convention_fede').toggle(200);
    });
    $('#id_avec_convention_fede').click(function () {
        $('#div_id_dans_calendrier').toggle(200);
    });
    // EOF

    // gerer le lien d'aide pour la discipline
    if ($('#id_activite').val()) {
        $.get('/sports/ajax/activite/aide/?pk=' + $('#id_activite').val(), function (data) {
            if (data) {
                let html = `<a href="${data}" target="_blank" class="float-end">Consulter la page d'aide dédiée à cette discipline</a>`;
                $('#id_activite_chosen').after(html);
            }
        });
    }

});

// fonction async ne pas mettre dans le ready

$('#div_id_ville_depart_interdep').hide()
$('#div_id_ville_depart_interdep').after(`<div id="div_display_interdep"></div>`)
var ville_default_pas_parse = $('#id_ville_depart_interdep').text()
var interdep_a_modifier = false;
if ($('#id_ville_depart_interdep').hasClass('modifier')) {
    interdep_a_modifier = true;
}
var ville_default = ville_default_pas_parse ? JSON.parse($('#id_ville_depart_interdep').text()) : {}
liste_champ_modif_obj = liste_champ_modif ? JSON.parse(liste_champ_modif) : []
var ville_traverse_default = $('#id_villes_traversees').val()
var dep_default = $('#id_departement_depart').val()
console.log(dep_default)
var cache_ville = {}
var cache_dep_traverse = []
var cache_ville_selectionne = {}
var cache_ville_traverse = []
var donne = null
var first = true
if (ville_default){
    for (let k = 0; k < ville_default.length; k++) {
        cache_ville_selectionne[ville_default[k].departement_pk] = ville_default[k].commune_pk
    }
}

function remplir_element_parcour(){
    console.log('function')
    // Permet de préremplir les champs communes traversées avec les donnés des parcours
        $.get(url_get_donnee_parcours+"?object=commune", function (data) {
            var concat_data = data[1].concat(data[0]) // combine les données initialise et les données présentes en carto
            $('.ville_traverse_interdep').val(concat_data).trigger('chosen:updated'); // Met à jour le champ chosen
            $('#id_villes_traversees').val(concat_data).trigger('chosen:updated');
            // Ajoute des couleurs selon si l'élément est présent en cerfa et/ou en parcour
            for(var commune in data[0]){
                $("#div_display_interdep .chosen-choices li.search-choice[data-value='"+data[0][commune]+"']").css('background', '#afc7ed')
                $("#div_id_villes_traversees .chosen-choices li.search-choice[data-value='"+data[0][commune]+"']").css('background', '#afc7ed')
            }
            for(var commune in data[1]){
                if(data[0].includes(data[1][commune])){
                    $("#div_display_interdep .chosen-choices li.search-choice[data-value='"+data[1][commune]+"']").css('background', '#caedaf')
                    $("#div_id_villes_traversees .chosen-choices li.search-choice[data-value='"+data[1][commune]+"']").css('background', '#caedaf')
                }
                else{
                    $("#div_display_interdep .chosen-choices li.search-choice[data-value='"+data[1][commune]+"']").css('background', '#edafaf')
                    $("#div_id_villes_traversees .chosen-choices li.search-choice[data-value='"+data[1][commune]+"']").css('background', '#edafaf')
                }
            }
        });
        // Permet de préremplir les champs n2k traversées avec les donnés des parcours
        $.get(url_get_donnee_parcours + "?object=n2k", function (data) {
            var concat_data = data[1].concat(data[0]) // combine les données initialise et les données présentes en carto
            $('#id_sites_natura2000').val(concat_data).trigger('chosen:updated'); // Met à jour le champ chosen
            // Ajoute des couleurs selon si l'élément' est présent en cerfa et/ou en parcour
            for (var n2k in data[0]) {
                $("#id_sites_natura2000_chosen .chosen-choices li.search-choice[data-value='" + data[0][n2k] + "']").css('background', '#afc7ed')
            }
            for (var n2k in data[1]) {
                if (data[0].includes(data[1][n2k])) {
                    $("#id_sites_natura2000_chosen .chosen-choices li.search-choice[data-value='" + data[1][n2k] + "']").css('background', '#caedaf')
                } else {
                    $("#id_sites_natura2000_chosen .chosen-choices li.search-choice[data-value='" + data[1][n2k] + "']").css('background', '#edafaf')
                }
            }
        });
         // Permet de préremplir les champs rnr traversées avec les donnés des parcours
        $.get(url_get_donnee_parcours + "?object=rnr", function (data) {
            var concat_data = data[1].concat(data[0]) // combine les données initialise et les données présentes en carto
            $('#id_zones_rnr').val(concat_data).trigger('chosen:updated'); // Met à jour le champ chosen
            // Ajoute des couleurs selon si l'élément est présent en cerfa et/ou en parcour
            for (var rnr in data[0]) {
                $("#id_zones_rnr_chosen .chosen-choices li.search-choice[data-value='" + data[0][rnr] + "']").css('background', '#afc7ed')
            }
            for (var rnr in data[1]) {
                if (data[0].includes(data[1][rnr])) {
                    $("#id_zones_rnr_chosen .chosen-choices li.search-choice[data-value='" + data[1][rnr] + "']").css('background', '#caedaf')
                } else {
                    $("#id_zones_rnr_chosen .chosen-choices li.search-choice[data-value='" + data[1][rnr] + "']").css('background', '#edafaf')
                }
            }
        });
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function remplir_champ_ville_depart_interdep() {
    if (!first){
        cache_ville_traverse[dep_default] =$(`#ville_traverse_${dep_default}`).val()
        if (cache_dep_traverse && cache_dep_traverse.length){
            for (let i=0; i<cache_dep_traverse.length; i++){
                cache_ville_selectionne[cache_dep_traverse[i]] = $(`#ville_depart_interdep_${cache_dep_traverse[i]}`).val()
                cache_ville_traverse[cache_dep_traverse[i]] = $(`#ville_traverse_${cache_dep_traverse[i]}`).val()
            }
        }
    }
    cache_dep_traverse = []
    let list_dep = $('#id_departements_traverses').val()
    if (list_dep && list_dep.length) {
        $('#div_display_interdep').html(html_chargement_en_cours())
        AfficherIcones()
        $('#div_id_villes_traversees').hide()
        let html = ``
        if (typeof cache_ville[dep_default] === "undefined"){
            let data = await $.get(`${url_liste_commune}?departement=${dep_default}`)
            donne = JSON.parse(data)
            cache_ville[dep_default] = donne
        }
        else {
            donne = cache_ville[dep_default]
        }
        html += `<div class="row mb-2" id="div_ville_traverse_${dep_default}">
                <label class="col-form-label col-sm-3" for="ville_traverse_${dep_default}">
                    Autres communes traversées dans le département de départ</label>
                <div class="col-sm-6" style="margin-left: 0.5em">
                <select data-placeholder="Séléctionnez les communes traversées" multiple="multiple" name="ville_traverse_${dep_default}"
                    id="ville_traverse_${dep_default}" class="select ville_traverse_interdep form-select"`
        if (!en_instruction || liste_champ_modif_obj.indexOf("villes_traversees") >= 0){
            html += `>`;
        } else {
            html += ` disabled>`;
        }
        for (let j = 0; j < donne.length; j++) {
            if (ville_traverse_default.find(element => element == donne[j][0])) {
                html += `<option selected value="${donne[j][0]}">${donne[j][1]}</option>`
            } else {
                html += `<option value="${donne[j][0]}">${donne[j][1]}</option>`
            }
        }
        if (liste_champ_modif_obj.indexOf("villes_traversees") >= 0) {
            html += "</select></div><div class=\"col-sm-2 text-center modifier\" style='margin-left:5em'><i class=\"a-completer\"></i> à modifier</div></div>";
        } else {
            html += "</select></div></div>";
        }
        for (let i = 0; i < list_dep.length; i++) {
            cache_dep_traverse.push(list_dep[i])
            if (typeof cache_ville[list_dep[i]] === "undefined"){
                let data = await $.get(`${url_liste_commune}?departement=${list_dep[i]}`)
                donne = JSON.parse(data)
                cache_ville[list_dep[i]] = donne
            }
            else {
                donne = cache_ville[list_dep[i]]
            }
            let nom_champ = $('#id_departements_traverses').find(`option[value="${list_dep[i]}"]`).html()
            html += `<div class="row mb-2" id="div_ville_depart_interdep_${list_dep[i]}">
                    <label class="col-form-label col-sm-3" for="ville_depart_interdep_${list_dep[i]}">
                        Commune de départ dans le département</br>${nom_champ}*</label>
                    <div class="col-sm-6" style="margin-left: 0.5em">
                    <select data-placeholder="Sélectionnez la commune de départ" name="ville_depart_interdep_${list_dep[i]}"
                    id="ville_depart_interdep_${list_dep[i]}" class="select form-select requis ville_depart_interdep"`
            if (typeof cache_ville_selectionne[list_dep[i]] === 'undefined' || !en_instruction) {
                html += `><option selected disabled value="">Sélectionnez la commune de départ</option>`
            } else {
                if (interdep_a_modifier === true && liste_champ_modif_obj.indexOf("ville_depart_interdep_" + list_dep[i]) >= 0) {
                    html += `>`
                } else {
                    html += ` disabled>`
                }
            }
            for (let j = 0; j < donne.length; j++) {
                if (donne[j][0] == cache_ville_selectionne[list_dep[i]]) {
                    html += `<option selected value="${donne[j][0]}">${donne[j][1]}</option>`
                } else {
                    html += `<option value="${donne[j][0]}">${donne[j][1]}</option>`
                }

            }
            if (typeof cache_ville_selectionne[list_dep[i]] === 'undefined') {
                html += "</select></div><div class=\"col-sm-2 text-center completer\" style='margin-left:5em'><i class=\"a-completer\"></i> à completer</div></div>"
            } else {
                if (interdep_a_modifier === true && liste_champ_modif_obj.indexOf("ville_depart_interdep_" + list_dep[i]) >= 0) {
                    html += "</select></div><div class=\"col-sm-2 text-center modifier\" style='margin-left:5em'><i class=\"a-completer\"></i> à modifier</div></div>"
                } else {
                    html += "</select></div></div>"
                }
            }
            html += `<div class="row mb-2" id="div_ville_traverse_${list_dep[i]}">
                    <label class="col-form-label col-sm-3" for="ville_traverse_${list_dep[i]}">
                        Autres communes traversées dans le département</br>${nom_champ}</label>
                    <div class="col-sm-6" style="margin-left: 0.5em">
                    <select data-placeholder="Sélectionnez les communes traversées" multiple="multiple" name="ville_traverse_${list_dep[i]}"
                    id="ville_traverse_${list_dep[i]}" class="select form-select requis ville_traverse_interdep"`
            if (!en_instruction ||  typeof cache_ville_selectionne[list_dep[i]] === 'undefined' || liste_champ_modif_obj.indexOf("villes_traversees") >= 0){
                html += `>`;
            } else {
                html += ` disabled>`;
            }
            for (let j = 0; j < donne.length; j++) {
                if (ville_traverse_default.find(element => element == donne[j][0])) {
                    html += `<option selected value="${donne[j][0]}">${donne[j][1]}</option>`
                } else {
                    html += `<option value="${donne[j][0]}">${donne[j][1]}</option>`
                }
            }
            if (liste_champ_modif_obj.indexOf("villes_traversees") >= 0) {
                html += "</select></div><div class=\"col-sm-2 text-center modifier\" style='margin-left:5em'><i class=\"a-completer\"></i> à modifier</div></div>";
            } else {
                html += "</select></div></div>";
            }
        }
        $('#div_display_interdep').html(html).show()
        AfficherIcones()
        if (first){
            if ($('#id_villes_traversees').val() && $('#id_villes_traversees').val().length){
                let tab_temp = $('#id_villes_traversees').val()
                var temp = []
                for (let p=0; p<tab_temp.length; p++){
                    if (cache_ville[dep_default].find(ele => ele[0] == tab_temp[p])){
                        temp.push(tab_temp[p])
                    }
                }
                cache_ville_traverse[dep_default] = temp
                for (let o=0; o<cache_dep_traverse.length; o++){
                    for (let p=0; p<tab_temp.length; p++){
                        if (cache_ville[cache_dep_traverse[o]].find(ele => ele[0] == tab_temp[p])){
                            temp.push(tab_temp[p])
                        }
                    }
                    cache_ville_traverse[cache_dep_traverse[o]] = tab_temp
                }
            }
        }
        $(`#ville_traverse_${dep_default}`).val(cache_ville_traverse[dep_default]).trigger('chosen:updated')
        for (let p=0; p<cache_dep_traverse.length; p++){
            $(`#ville_traverse_${cache_dep_traverse[p]}`).val(cache_ville_traverse[cache_dep_traverse[p]]).trigger('chosen:updated')
        }

        first = false
        $('.ville_depart_interdep').chosen({width: 636})
        $('.ville_traverse_interdep').chosen({width: 636})
    } else {
        $('#div_display_interdep').empty()
        $('#div_id_villes_traversees').show()

        $('#id_villes_traversees').chosen({width: 636})
        $('#id_villes_traversees_chosen').css("width", "636px")
    }

    if(url_get_donnee_parcours){
        await sleep(500)
        remplir_element_parcour()
    }
}

const initial_dep_traverse = $('#id_departements_traverses').val() // Conserve les valeurs initiales du champ département traversé
$.get(url_get_donnee_parcours + "?object=dep", function (data) {
    var concat_data = data[1].concat(data[0]) // combine les départements initialise et les départements présents en carto
    $('#id_departements_traverses').val(concat_data).trigger('chosen:updated'); // Met à jour le champ département traversé chosen
    // Ajoute des couleurs selon si le département est présent en cerfa et/ou en parcours
    for (var dep in data[0]) {
        $("#id_departements_traverses_chosen .chosen-choices li.search-choice[data-value='" + data[0][dep] + "']").css('background', '#afc7ed')
    }
    for (var dep in data[1]) {
        if (data[0].includes(data[1][dep])) {
            $("#id_departements_traverses_chosen .chosen-choices li.search-choice[data-value='" + data[1][dep] + "']").css('background', '#caedaf')
        } else {
            $("#id_departements_traverses_chosen .chosen-choices li.search-choice[data-value='" + data[1][dep] + "']").css('background', '#edafaf')
        }
    }
    // Appel la fonction pour mettre à jour les champs commune
    remplir_champ_ville_depart_interdep()
});

$('#id_departements_traverses').on('change', async function () {
    await remplir_champ_ville_depart_interdep()
})
