$(document).ready(function () {

    $('.btn_etat_1, .btn_etat_2, #btn_etat_all').click(function (e) {
        e.preventDefault();
        let url_type = url_tableau_organisateur_liste;
        url_type += window.location.search;

        let btn_etat = $(this).attr('id');
        if (url_type.indexOf('?')=== -1) {
            // Pas de query, on crée
            url_type += `?`;
        } else {
            // Une query existe, on ajoute
            url_type += "&";
        }
        url_type += `filtre_etat=${btn_etat.split('_')[2]}`;
        if (btn_etat==="btn_etat_all"){
            $(this).after(' <i class="spinner attente_chargement"></i>');
            AfficherIcones();
        }

        $.get(url_type, function (data) {
            $('#block_tableaubord_liste').html(data);
            $('.attente_chargement').remove();
            AfficherIcones();
        });
    });

    $('#annee').on('change', function (e) {
        e.preventDefault()
        let annee = $(this).val()
        let parametres = window.location.search;

        $('#annee_chosen').after('<span  class="text-info ms-4 attente_chargement">Recherche des archives en cours. Merci de patienter... <i class="far fa-spinner fa-spin"></i></span>');



        if (annee !== "") {
            if (parametres.search('annee') !== -1) {
                const regex = /annee=\d+/gi;
                parametres = parametres.replace(regex, "annee=" + annee);
            } else {
                if (parametres.indexOf('?') !== -1) {
                    parametres += "&annee=" + annee;
                } else {
                    parametres += "?annee=" + annee;
                }
            }

             $.get(url_tableau_organisateur_liste+parametres, function (d){
                 $('#block_tableaubord_liste').html(d);
                $('.attente_chargement').remove();
                AfficherIcones();
             })
        }

    })

    $('.btn_consult').click(function (e) {
        e.preventDefault();
        $(this).after(' <i class="spinner attente_chargement"></i>');
        AfficherIcones();

        let parametres = window.location.search;
        let url_complete = url_consultation_liste + parametres
        $.get(url_complete, function (data) {
            $('#block_tableaubord_consult').html(data);
            $('#block_tableaubord_liste').empty();
            $('.attente_chargement').remove();
            AfficherIcones();
        });
    });

});