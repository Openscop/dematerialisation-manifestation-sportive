# coding: utf-8

from instructions.factories import InstructionFactory
from .base import EvenementsTestsBase
from ..factories.manif import DvtmcirFactory


class DvtmcirTests(EvenementsTestsBase):
    """ Tests Dvtmcir """

    def setUp(self):
        """
        La manifestation créée ici est une manifestation sportive motorisée.
        """
        self.instruction = InstructionFactory.create(manif=DvtmcirFactory.create())
        self.instruction.manif.description = "description manif"
        self.instruction.manif.prenom_contact = "john"
        self.instruction.manif.nom_contact = "Doe"
        self.instruction.manif.tel_contact = "0666666666"
        liste_fichiers = [
            ('reglement_manifestation', "/reglement"),
        ]
        for fichier in liste_fichiers:
            pj = self.instruction.manif.piece_jointe.get(champ_cerfa=fichier[0])
            pj.document_attache = fichier[1]
            pj.save()
        super().setUp()

    # Tests
    def test_get_absolute_url(self):
        """ Tester les URL d'accès """
        print('Tests Dvtmcir')
        manifestation = DvtmcirFactory.create(pk=5)
        self.assertEqual(manifestation.get_absolute_url(), '/Dvtmcir/5/')

    def test_completude(self):
        self.assertTrue(self.manifestation.formulaire_complet())
        self.assertTrue(self.manifestation.carto_complet())
        self.assertEqual(len(self.manifestation.liste_manquants()), 0)
        self.assertTrue(self.manifestation.dossier_complet())

    def test_display_natura2000_eval_panel(self):
        """ Vérifier que le panneau d'évaluation N2K doit s'afficher dans les cas suivants """
        self.manifestation.lucratif = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.lucratif = False
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = False
        self.manifestation.delivrance_titre = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.delivrance_titre = False
        self.manifestation.vtm_hors_circulation = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.vtm_hors_circulation = False
        self.manifestation.demande_homologation = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())

    def test_legal_delay(self):
        self.assertEqual(self.manifestation.get_delai_legal(), 60)
