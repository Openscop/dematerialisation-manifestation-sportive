import time

from django.test import tag, override_settings
from django.utils import timezone
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

from .test_base_selenium import SeleniumCommunClass
from sports.factories import ActiviteFactory, DisciplineFactory
from evenements.models import Manif


class TestAiguillageAvtm1(SeleniumCommunClass):
    """
    Test du circuit organisateur avec sélénium
        circuit_GGD : Avis GGD + préavis EDSR
        instruction par arrondissement
        openrunner false par défaut
    Test de l'aiguillage organisateur sur un cerfa AVTM
    Vérification des champs rentrés, des fichiers rentrés
    Vérification du TdB instructeur
    Vérification et ajout des pièces jointes à J-6 et J-21
    Vérification des alertes retards sur les fichiers
    """

    DELAY = 0.4

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets nécessaires au formulaire de création de manifestation
            Création des fichiers nécessaires à joindre à la création de la manifestation
        """
        print()
        print('========== Manifestation AVTM compétition sur voie publique ==========')
        SeleniumCommunClass.init_setup(cls)

        disc = DisciplineFactory(motorise=True, name='Moto')
        cls.activ = ActiviteFactory.create(discipline=disc)

        super().setUpClass()

    @tag('selenium')
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
    def test_Avtm(self):
        """
        Circuit organisateur pour une déclaration avec véhicules
        :return:
        """
        print('**** test aiguillage ****')
        # Connexion
        self.connexion('organisateur')

        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Déclarer ou demander').click()
        time.sleep(self.DELAY)
        # remplir formulaire d'aiguillage
        self.chosen_select('id_discipline_chosen', 'Moto')
        time.sleep(self.DELAY)
        self.chosen_select('id_activite_chosen', 'Activite')
        time.sleep(self.DELAY)
        self.chosen_select('id_departement_chosen', '42')
        time.sleep(self.DELAY)
        self.chosen_select('id_ville_depart_chosen', 'Bard')
        time.sleep(self.DELAY)
        self.chosen_select('id_emprise_chosen', 'Totalement sur voies publiques ou ouvertes à la circulation')
        time.sleep(self.DELAY)
        entries = self.selenium.find_element(By.ID, 'id_nb_participants')
        self.selenium.execute_script("window.scroll(0, 2000)")
        time.sleep(self.DELAY * 5)
        entries.send_keys('150')
        nb_departements = self.selenium.find_element(By.ID, 'id_nb_departement')
        nb_departements.send_keys('1')
        time.sleep(self.DELAY*3)
        self.selenium.find_element(By.XPATH, "//button[contains(text(),'Rechercher')]").click()
        time.sleep(self.DELAY)
        # Tester bon aiguillage
        self.assertIn('Avtm', self.selenium.page_source)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()

        self.assertIn('Organisation d\'une manifestation sportive', self.selenium.page_source)

        print('**** remplissage formulaire ****')
        # Remplir formulaire manifestation
        time.sleep(self.DELAY*4)
        self.remplir_form('avtm')
        time.sleep(self.DELAY*2)
        self.remplir_carto()

        print('**** test dossier complet ****')
        # Tester la présence de la page détail
        self.assertIn('une grosse course qui déchire', self.selenium.page_source)
        # Tester le status de la manifestation
        boutons = self.selenium.find_elements(By.CSS_SELECTOR, '.page-header li')
        try:
            boutons[0].find_element(By.CLASS_NAME, 'fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="L'étape 'Préparation du dossier' n\'est pas vert")
        try:
            boutons[2].find_element(By.CLASS_NAME, 'action-requise-blanc')
        except NoSuchElementException:
            self.assertTrue(False, msg="L'étape 'Envoi de la demande' n\'est pas rouge")
        # Tester les fichiers manquants
        card = self.selenium.find_element(By.XPATH, "//div[@class='card-header bg-danger text-white']//parent::*")
        self.assertIn('Liste des pièces jointes manquantes', card.text)
        files = card.find_elements(By.CSS_SELECTOR, 'li')
        self.assertEqual(5, len(files))
        for file in files:
            if file.text not in ('Réglement de la manifestation',
                                 'Carte zone publique', 'Dossier sécurité',
                                 'Dispositions prises pour la sécurité', 'Itinéraire horaire'):
                self.assertTrue(False, msg="le fichier "+file.text+" n\'est pas dans la liste")

        print('**** test ajout fichiers ****')
        self.selenium.find_element(By.XPATH, "//button[contains(.,'Pièces jointes')]").click()
        time.sleep(self.DELAY*5)
        self.selenium.execute_script("window.scroll(0, 700)")

        self.charger_fichier('reglement_manifestation')
        self.charger_fichier('disposition_securite')
        self.charger_fichier('itineraire_horaire')
        self.charger_fichier('carte_zone_public')
        self.charger_fichier('dossier_securite')

        self.selenium.execute_script("window.scroll(0, 0)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, "//button[contains(.,'Détail de la manifestation')]").click()
        time.sleep(self.DELAY*5)

        print('**** test envoi ****')
        self.envoyer_la_demande()
        # Retour en page détail
        self.assertIn('une grosse course qui déchire', self.selenium.page_source)
        # Tester le status
        boutons = self.selenium.find_elements(By.CSS_SELECTOR, '.page-header li')
        try:
            boutons[0].find_element(By.CLASS_NAME, 'fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="L'étape 'Préparation du dossier' n\'est pas vert")
        try:
            boutons[2].find_element(By.CLASS_NAME, 'fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="L'étape 'Envoi de la demande' n\'est pas vert")

        self.deconnexion()

        print('**** test instructeur ****')
        # Connexion
        self.connexion('instructeur')
        # Tester la présence de la manifestation
        self.presence_avis('nouveau')
        self.deconnexion()

        print('**** test Pjs manquantes delai limite ****')
        # Connexion
        self.connexion('organisateur')
        # Tester la présence de la manifestation
        self.presence_avis('encours')
        self.vue_detail()
        self.aucune_action()
        self.retourTdB()

        print('**** test Pjs manquantes delai etape 1-2 ****')
        manif = Manif.objects.last()
        manif.date_debut = timezone.now() + timezone.timedelta(days=25)
        manif.date_fin = timezone.now() + timezone.timedelta(days=25)
        manif.save()
        self.selenium.find_element(By.ID, 'btn_etat_demande').click()
        time.sleep(self.DELAY)
        self.vue_detail()
        action = self.selenium.find_element(By.XPATH, "//div[contains(@class, 'card')][contains(string(),'Actions requises')]")
        self.assertIn('pièces jointes manquantes', action.text)
        self.assertIn('Avis fédération délégataire', action.text)
        self.retourTdB()
        manif.date_debut = timezone.now() + timezone.timedelta(days=19)
        manif.date_fin = timezone.now() + timezone.timedelta(days=19)
        manif.save()

        self.selenium.find_element(By.ID, 'btn_etat_demande').click()
        time.sleep(self.DELAY)
        self.vue_detail()
        action = self.selenium.find_element(By.XPATH, "//div[contains(@class, 'card')][contains(string(),'Actions requises')]")
        self.assertIn('pièces jointes manquantes', action.text)
        self.assertIn('Vous êtes en retard', action.text)
        self.assertIn('Avis fédération délégataire', action.text)

        self.selenium.find_element(By.XPATH, "//button[contains(.,'Pièces jointes')]").click()

        self.charger_fichier('avis_federation_delegataire')
        self.selenium.execute_script("window.scroll(0, 0)")
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.XPATH, "//button[contains(.,'Détail de la manifestation')]").click()
        time.sleep(self.DELAY*5)

        action = self.selenium.find_element(By.XPATH, "//div[contains(@class, 'card')][contains(string(),'Actions requises')]")
        self.assertIn('pièces jointes manquantes', action.text)
        self.assertIn('assurance', action.text)
        self.assertIn('participants', action.text)
        self.retourTdB()

        print('**** test Pjs manquantes delai etape 2-3 ****')
        manif.date_debut = timezone.now() + timezone.timedelta(days=5)
        manif.date_fin = timezone.now() + timezone.timedelta(days=5)
        manif.save()
        self.selenium.find_element(By.ID, 'btn_etat_demande').click()
        time.sleep(self.DELAY)
        self.vue_detail()
        action = self.selenium.find_element(By.XPATH, "//div[contains(@class, 'card')][contains(string(),'Actions requises')]")
        self.assertIn('pièces jointes manquantes', action.text)
        self.assertIn('Vous êtes en retard', action.text)

        self.selenium.find_element(By.XPATH, "//button[contains(.,'Pièces jointes')]").click()
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY*5)

        self.charger_fichier('participants')
        self.charger_fichier('certificat_assurance')

        self.selenium.execute_script("window.scroll(0, 0)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, "//button[contains(.,'Détail de la manifestation')]").click()
        time.sleep(self.DELAY*5)

        self.aucune_action()
