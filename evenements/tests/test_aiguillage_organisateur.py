import re, os, json
from datetime import timedelta, datetime

from django.test import TransactionTestCase, override_settings
from django.contrib.auth.hashers import make_password as make
from django.contrib.contenttypes.models import ContentType

from allauth.account.models import EmailAddress
from post_office.models import EmailTemplate, Email
from bs4 import BeautifulSoup as Bs

from core.factories import UserFactory
from core.models import User, Instance, ConfigurationGlobale
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from sports.factories import ActiviteFactory
from messagerie.models import Message
from structure.factories.structure import StructureOrganisatriceFactory


class Aiguillage_OrganisateurTests(TransactionTestCase):
    """
    Test du circuit organisateur de la nouvelle version
        circuit_GGD : Avis GGD + préavis EDSR
        instruction par arrondissement
        openrunner false par défaut
    """

    serialized_rollback = True
    # Avec l'exportation asynchrone, la db ne suit pas pendant le test
    # sync_to_async(...) has its own database transaction #1110
    # il faut passer par la classe TransactionTestCase avec serialized_rollback = True
    # plus changement du setup et regroupement des tests

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création des objets nécessaires au formulaire de création de manifestation
            Création des fichiers nécessaires à joindre à la création de la manifestation
        """
        print()
        print('========= Aiguillage organisateur =========')

        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        ConfigurationGlobale.objects.create(pk=1, mail_action=True, mail_suivi=True, mail_tracabilite=True,
                                            mail_actualite=True)

        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                            instance__etat_s="ouvert")
        cls.other_dep = DepartementFactory.create(name='43',
                                                  instance__name="instance de test",
                                                  instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                                  instance__etat_s="ouvert")
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        arrondissement43 = ArrondissementFactory.create(name='Yssingeaux', code='433', departement=cls.other_dep)
        cls.prefecture = arrondissement.organisations.first()
        cls.prefecture43 = arrondissement43.organisations.first()

        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)
        cls.autrecommune43 = CommuneFactory(name='tence', arrondissement=arrondissement43)

        cls.organisateur = UserFactory.create(username='organisateur', password=make("123"),
                                              default_instance=cls.dep.instance, email='orga@test.fr')
        # Création d'une structure
        cls.structure_organisatrice = StructureOrganisatriceFactory.create(precision_nom_s="structure_test",
                                                                           commune_m2m=cls.commune, instance_fk=cls.dep.instance)
        cls.organisateur.organisation_m2m.add(cls.structure_organisatrice)

        cls.instructeur = UserFactory.create(username='instructeur', password=make("123"),
                                             default_instance=cls.dep.instance, email='inst@test.fr')
        cls.instructeur.organisation_m2m.add(cls.prefecture)

        # cls.structure = StructureFactory(commune=cls.commune, organisateur=organisateur)
        cls.activ = ActiviteFactory.create()
        for user in User.objects.all():
            user.optionuser.notification_mail = True
            user.optionuser.action_mail = True
            user.optionuser.tracabilite_mail = True
            user.optionuser.save()

        EmailTemplate.objects.create(name='new_msg')

        for file in ('reglement_manifestation', 'disposition_securite',
                     'itineraire_horaire', 'carte_zone_public', 'dossier_securite',
                     'itineraire_horaire_'+str(cls.dep.pk), 'itineraire_horaire_'+str(cls.other_dep.pk),
                     'dossier_tech_cycl', 'convention_police'):
            mon_fichier = open("/tmp/"+file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test aiguillage Organisateur")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Suppression des fichiers créés dans /tmp
        """
        for file in ('reglement_manifestation', 'disposition_securite',
                     'itineraire_horaire', 'carte_zone_public', 'dossier_securite',
                     'itineraire_horaire_'+str(cls.dep.pk), 'itineraire_horaire_'+str(cls.other_dep.pk),
                     'dossier_tech_cycl', 'convention_police'):
            os.remove("/tmp/"+file+".txt")

        os.system('rm -R /tmp/maniftest/media/42/')

        super(Aiguillage_OrganisateurTests, cls).tearDownClass()

    def nouvelle_manif(self, reponse):
        """
        Appel de la vue d'aiguillage
        :param reponse: réponse précédente
        :return: réponse suivante
        """
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Déclarer ou demander ', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        reponse = self.client.get(action.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Recherche de formulaire', count=2)
        self.assertContains(reponse, 'Rechercher le formulaire')
        self.assertContains(reponse, 'Accéder au formulaire')

        return reponse

    def donnees_aiguillage(self, reponse, formulaire):
        """
        Remplissage des champs pour aiguiller sur la bonne manifestation spécialisée
        :param reponse: reponse précédente
        :param formulaire: cerfa demandé
        :return: data à poster
        """
        data = {}
        # Informations générales
        data['csrf_token'] = reponse.context['csrf_token']

        data['discipline'] = self.activ.discipline.pk
        data['activite'] = self.activ.pk
        data['nb_participants'] = 200
        data['ville_depart'] = self.commune.pk
        data['departement'] = self.commune.arrondissement.departement.pk
        data['emprise'] = 3
        data['type_circuit'] = 0
        data['formulaire'] = formulaire
        return data

    def donnees_formulaire(self, reponse):
        """
        Récupération des données du formulaire concernant le formset de contact qui a des données à restituer
        et remplissage des champs communs
        :param reponse: reponse précédente
        :return: data à poster
        """
        data = {}
        # Informations générales
        data['csrf_token'] = reponse.context['csrf_token']
        data['description'] = 'une grosse course'
        data['nb_spectateurs'] = 15
        data['nb_organisateurs'] = 15
        data['villes_traversees'] = self.autrecommune.pk
        data['nom_contact'] = 'durand'
        data['prenom_contact'] = 'joseph'
        data['tel_contact'] = '0605555555'
        # Le champ n'est pas rempli avec le kwarg de la vue, alors on le passe
        data['departement_depart'] = self.dep.pk
        """
        print('#' * 30)
        for i in sorted(data.keys()):
            print(i, '\t:', data[i])
        print('#' * 30)
        """
        return data

    def page_fichiers(self, reponse):
        """ Appel de la page pièces jointes """
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        print('\t' + url_script.group('url'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        return reponse

    def joindre_fichiers(self, reponse, cas='d'):
        """
        Ajout des fichiers nécessaires à la manifestation
        :param reponse: reponse précédente
        :param cas: suivant cerfa considéré
        :return: reponse suivante
        """
        # appel de la vue pour joindre les fichiers
        reponse = self.page_fichiers(reponse)
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation",  "disposition_securite"]
        if cas == 'v':
            liste_files += ["itineraire_horaire"]
        if cas == 'd':
            liste_files += ["itineraire_horaire"]
        if cas == 'dx':
            # cas interdepartementalité
            liste_files += ['itineraire_horaire_'+str(self.dep.pk), 'itineraire_horaire_'+str(self.other_dep.pk)]
        if cas == 'c':
            liste_files += ["dossier_tech_cycl", "itineraire_horaire"]
        if cas == 'cc':
            liste_files += ["itineraire_horaire", "convention_police"]
        if cas == 'm':
            liste_files += ["itineraire_horaire", "carte_zone_public", "dossier_securite"]
        if cas == 's':
            liste_files.remove('disposition_securite')
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')
        # Retour à la vue de détail
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail1 = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail1, 'group'))
        reponse = self.client.get(detail1.group('url'), HTTP_HOST='127.0.0.1:8000')
        return reponse

    def fonction_test_page_fichiers(self, reponse, liste_pj, liste_pj_item):
        # appel de la page
        page_pj = self.page_fichiers(reponse)
        # extraction des lignes du tableau
        html = Bs(page_pj.content.decode('utf-8'), 'html.parser')
        # print(html)
        pjs = html.select('tr')[1:]
        self.assertEqual(len(pjs), len(liste_pj))
        for pj in pjs:
            # une ligne du tableau doit correspondre à un des titres
            self.assertTrue(any(piece in str(pj) for piece in liste_pj))
            for idx, piece in enumerate(liste_pj):
                if piece in str(pj):
                    # Vérification de la date limite de dépôt
                    self.assertIn(liste_pj_item[idx][0], str(pj))
                    # Vérification de mise en avant
                    if liste_pj_item[idx][1]:
                        self.assertIn("Pièce requise", str(pj))
                    else:
                        self.assertNotIn("Pièce requise", str(pj))

    def declarer_manif(self, reponse):
        """
        Soumettre la manifestation à l'instructeur
        :param reponse: reponse précédente
        :return: reponse suivante
        """
        # appel de la vue pour déclarer la manifestation
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(declar, 'group'))
        print('\t' + declar.group('url'))
        reponse = self.client.get(declar.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Demander l\'instruction', count=2)
        # Soumettre la déclaration
        reponse = self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        return reponse

    def verification_instructeur(self, nb):
        """
        Vérification dans la dashboard instructeur de l'affichage de la manifestation crée
        :return:
        """
        nb_str = str(nb)
        # Connexion avec l'instructeur
        self.assertTrue(self.client.login(username="instructeur", password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/instructions/tableaudebord/', HTTP_HOST='127.0.0.1:8000', follow=True)
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual(nb_str, nb_bloc.group('nb'))
        nb_bloc = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('0', nb_bloc.group('nb'))
        nb_bloc = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('0', nb_bloc.group('nb'))
        nb_bloc = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('0', nb_bloc.group('nb'))
        nb_bloc = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('0', nb_bloc.group('nb'))
        reponse = self.client.get(f'/instructions/tableaudebord/{self.prefecture.pk}/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'La grosse course ' + nb_str)
        self.assertContains(reponse, 'table_afaire', count=nb)
        # f = open('/var/log/manifsport/test_output.html', 'w')
        # f.write(reponse.content.decode('utf-8'))
        # f.close()

    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
    def test_Aiguillage(self):
        """ Test de l'aiguillage """

        print('**** test Inscription ****')
        reponse = self.client.post('/inscription/organisateur',
                                   {'first_name': 'Jean',
                                    'last_name': 'Dupont',
                                    'username': 'jandup',
                                    'email': 'ne-pas-repondre-42@manifestationsportive.fr',
                                    'password1': 'azeaz5646dfzdsf',
                                    'password2': 'azeaz5646dfzdsf',
                                    'cgu': True,
                                    'structure_name': 'MonAssoc',
                                    'type_of_structure': self.structure_organisatrice.type_organisation_fk.pk,
                                    'address': '12 rue du repos éternel',
                                    'departement': self.dep.pk,
                                    'commune': self.commune.pk,
                                    'phone': '04 77 74 33 55',
                                    },
                                   follow=True,
                                   HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Vérifiez votre adresse email', count=2)
        email = EmailAddress.objects.last()
        email.verified = True
        email.save()
        user = User.objects.get(username='jandup')
        user.is_active = True
        user.save()
        reponse = self.client.post('/accounts/login/',
                                   {'login': 'jandup',
                                    'password': 'azeaz5646dfzdsf',
                                    },
                                   follow=True,
                                   HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Connexion avec jandup réussie', count=1)

        print('**** test Dnm ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue tableau de bord nouvelle apps
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dnm')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Déclaration initiale')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nom'] = 'La grosse course 1'
        data['nb_signaleurs'] = 3
        data['nb_signaleurs_fixes'] = 1
        data['nb_signaleurs_autos'] = 1
        data['nb_signaleurs_motos'] = 1
        data['nb_vehicules_accompagnement'] = 0
        data['date_debut'] = datetime.now() + timedelta(days=29)
        data['date_fin'] = datetime.now() + timedelta(days=29, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print('\t' + url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk').values()
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0]['to'], [self.organisateur.email])
        self.assertIn("Connexion", messages[0].corps)
        self.assertIn("créer un nouveau dossier", messages[2].corps)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=31)
        data['date_fin'] = datetime.now() + timedelta(days=31, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+\\n.+Dossier à envoyer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie manquante', count=1)
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'Itinéraire horaire', count=1)

        fichier = open(os.path.join('evenements/tests/data-carto.json'))
        data = fichier.read()
        jsonData = json.loads(data)
        jsonData['manif'] = action.group('url').split('/')[2]

        self.client.post('/carto/parcours/', jsonData, content_type="application/json", follow=True, HTTP_HOST='127.0.0.1:8000')
        url = "/".join(action.group('url').split('/')[:3]) + "/"
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie manquante')

        # Test de l'export
        response = self.client.get("/evenement/"+action.group('url').split('/')[2]+"/export/organisateur/", HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(response.status_code, 200)

        # Test de la page Pièces jointes
        date1 = (datetime.now() + timedelta(days=2)).strftime('%d/%m/%Y')
        date2 = (datetime.now() + timedelta(days=26)).strftime('%d/%m/%Y')
        liste_pj = ["Réglement de la manifestation", "Dispositions prises pour la sécurité",
                    "Itinéraire horaire", "Attestation d'assurance"]
        liste_pj_item = [(date1, "requise"), (date1, "requise"),
                         (date1, "requise"), (date2, "")]
        self.fonction_test_page_fichiers(reponse, liste_pj, liste_pj_item)

        reponse = self.joindre_fichiers(reponse)

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Envoi du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur(1)

        print('**** test Dnmc avec convention de service d\'ordre ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue tableau de bord nouvelle apps
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dnmc')
        data1['manif'] = action.group('url').split('/')[2]

        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Déclaration initiale')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nom'] = 'La grosse course 2'
        data['police_nationale'] = True
        data['nb_signaleurs'] = 3
        data['nb_signaleurs_fixes'] = 1
        data['nb_signaleurs_autos'] = 1
        data['nb_signaleurs_motos'] = 1
        data['nb_vehicules_accompagnement'] = 0
        data['date_debut'] = datetime.now() + timedelta(days=29)
        data['date_fin'] = datetime.now() + timedelta(days=29, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print('\t' + url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=31)
        data['date_fin'] = datetime.now() + timedelta(days=31, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+\\n.+Dossier à envoyer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie manquante', count=1)
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'Itinéraire horaire', count=1)
        self.assertContains(reponse, 'Convention avec la police', count=2)

        fichier = open(os.path.join('evenements/tests/data-carto.json'))
        data = fichier.read()
        jsonData = json.loads(data)
        jsonData['manif'] = action.group('url').split('/')[2]

        self.client.post('/carto/parcours/', jsonData, content_type="application/json", follow=True, HTTP_HOST='127.0.0.1:8000')
        url = "/".join(action.group('url').split('/')[:3]) + "/"
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie manquante')

        # Test de la page Pièces jointes
        date1 = (datetime.now() + timedelta(days=2)).strftime('%d/%m/%Y')
        date2 = (datetime.now() + timedelta(days=26)).strftime('%d/%m/%Y')
        liste_pj = ["Réglement de la manifestation", "Dispositions prises pour la sécurité", "Itinéraire horaire",
                    "Attestation d'assurance", "Convention avec la police nationale ou la gendarmerie"]
        liste_pj_item = [(date1, "requise"), (date1, "requise"), (date1, "requise"),
                         (date2, ""), (date1, "requise")]
        self.fonction_test_page_fichiers(reponse, liste_pj, liste_pj_item)

        reponse = self.joindre_fichiers(reponse, 'cc')

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Envoi du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur(2)

        print('**** test Dcnm ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dcnm')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Déclaration initiale')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nom'] = 'La grosse course 3'
        data['nb_signaleurs'] = 3
        data['nb_signaleurs_fixes'] = 1
        data['nb_signaleurs_autos'] = 1
        data['nb_signaleurs_motos'] = 1
        data['nb_vehicules_accompagnement'] = 0
        data['date_debut'] = datetime.now() + timedelta(days=59)
        data['date_fin'] = datetime.now() + timedelta(days=59, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print('\t' + url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content.decode('utf-8'))
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=61)
        data['date_fin'] = datetime.now() + timedelta(days=61, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+\\n.+Dossier à envoyer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie manquante', count=1)
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'Itinéraire horaire', count=1)

        fichier = open(os.path.join('evenements/tests/data-carto.json'))
        data = fichier.read()
        jsonData = json.loads(data)
        jsonData['manif'] = action.group('url').split('/')[2]

        self.client.post('/carto/parcours/', jsonData, content_type="application/json", follow=True, HTTP_HOST='127.0.0.1:8000')
        url = "/".join(action.group('url').split('/')[:3]) + "/"
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie manquante')

        # Test de la page Pièces jointes
        date1 = (datetime.now() + timedelta(days=2)).strftime('%d/%m/%Y')
        date2 = (datetime.now() + timedelta(days=41)).strftime('%d/%m/%Y')
        date3 = (datetime.now() + timedelta(days=56)).strftime('%d/%m/%Y')
        liste_pj = ["Réglement de la manifestation", "Dispositions prises pour la sécurité",
                    "Itinéraire horaire", "Attestation d'assurance", "Liste des signaleurs",
                    'Attestation de présence de médecin(s)', 'Attestation de présence de secouriste(s)',
                    'Attestation de présence de ambulance(s)']
        liste_pj_item = [(date1, "requise"), (date1, "requise"),
                         (date1, "requise"), (date3, ""), (date2, ""),
                         ("Aucune", ""), ("Aucune", ""), ("Aucune", "")]
        self.fonction_test_page_fichiers(reponse, liste_pj, liste_pj_item)

        reponse = self.joindre_fichiers(reponse)

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Envoi du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur(3)
        print('**** test Dcnm sur deux départements ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dcnm')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Déclaration initiale')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nom'] = 'La grosse course 4'
        data['nb_signaleurs'] = 3
        data['nb_signaleurs_fixes'] = 1
        data['nb_signaleurs_autos'] = 1
        data['nb_signaleurs_motos'] = 1
        data['nb_vehicules_accompagnement'] = 0
        data['departements_traverses'] = (self.other_dep.pk,)
        data[f'ville_depart_interdep_{(self.other_dep.pk)}'] = (self.autrecommune43.pk)
        data['date_debut'] = datetime.now() + timedelta(days=89)
        data['date_fin'] = datetime.now() + timedelta(days=89, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print('\t' + url)

        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=91)
        data['date_fin'] = datetime.now() + timedelta(days=91, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+\\n.+Dossier à envoyer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie manquante', count=1)
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'Itinéraire horaire', count=2)

        fichier = open(os.path.join('evenements/tests/data-carto.json'))
        data = fichier.read()
        jsonData = json.loads(data)
        jsonData['manif'] = action.group('url').split('/')[2]

        self.client.post('/carto/parcours/', jsonData, content_type="application/json", follow=True, HTTP_HOST='127.0.0.1:8000')
        url = "/".join(action.group('url').split('/')[:3]) + "/"
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie manquante')

        # Test de la page Pièces jointes
        date1 = (datetime.now() + timedelta(days=2)).strftime('%d/%m/%Y')
        date2 = (datetime.now() + timedelta(days=71)).strftime('%d/%m/%Y')
        date3 = (datetime.now() + timedelta(days=86)).strftime('%d/%m/%Y')
        liste_pj = ["Réglement de la manifestation", "Dispositions prises pour la sécurité",
                    "Itinéraire horaire pour le département 42 - Loire", "Itinéraire horaire pour le département 43 - Haute-Loire",
                    "Attestation d'assurance",
                    "Liste des signaleurs pour le département 42 - Loire", "Liste des signaleurs pour le département 43 - Haute-Loire",
                    'Attestation de présence de médecin(s)', 'Attestation de présence de secouriste(s)',
                    'Attestation de présence de ambulance(s)']
        liste_pj_item = [(date1, "requise"), (date1, "requise"),
                         (date1, "requise"), (date1, "requise"),
                         (date3, ""), (date2, ""), (date2, ""),
                         ("Aucune", ""), ("Aucune", ""), ("Aucune", "")]
        self.fonction_test_page_fichiers(reponse, liste_pj, liste_pj_item)

        reponse = self.joindre_fichiers(reponse, cas="dx")

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Envoi du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur(4)

        print('**** test Dcnmc ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dcnmc')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Déclaration initiale')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nom'] = 'La grosse course 5'
        data['nb_signaleurs'] = 3
        data['nb_signaleurs_fixes'] = 1
        data['nb_signaleurs_autos'] = 1
        data['nb_signaleurs_motos'] = 1
        data['nb_vehicules_accompagnement'] = 0
        data['date_debut'] = datetime.now() + timedelta(days=59)
        data['date_fin'] = datetime.now() + timedelta(days=59, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print('\t' + url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content.decode('utf-8'))
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=61)
        data['date_fin'] = datetime.now() + timedelta(days=61, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+\\n.+Dossier à envoyer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie manquante', count=1)
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'Itinéraire horaire', count=1)
        self.assertContains(reponse, 'Dossier technique cyclisme', count=1)

        fichier = open(os.path.join('evenements/tests/data-carto.json'))
        data = fichier.read()
        jsonData = json.loads(data)
        jsonData['manif'] = action.group('url').split('/')[2]

        self.client.post('/carto/parcours/', jsonData, content_type="application/json", follow=True, HTTP_HOST='127.0.0.1:8000')
        url = "/".join(action.group('url').split('/')[:3]) + "/"
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie manquante')

        # Test de la page Pièces jointes
        date1 = (datetime.now() + timedelta(days=2)).strftime('%d/%m/%Y')
        date2 = (datetime.now() + timedelta(days=41)).strftime('%d/%m/%Y')
        date3 = (datetime.now() + timedelta(days=56)).strftime('%d/%m/%Y')
        liste_pj = ["Réglement de la manifestation", "Dispositions prises pour la sécurité",
                    "Itinéraire horaire", "Attestation d'assurance", "Liste des signaleurs",
                    'Attestation de présence de médecin(s)', 'Attestation de présence de secouriste(s)',
                    'Attestation de présence de ambulance(s)', "Dossier technique cyclisme"]
        liste_pj_item = [(date1, "requise"), (date1, "requise"),
                         (date1, "requise"), (date3, ""), (date2, ""),
                         ("Aucune", ""), ("Aucune", ""), ("Aucune", ""), (date1, "requise")]
        self.fonction_test_page_fichiers(reponse, liste_pj, liste_pj_item)

        reponse = self.joindre_fichiers(reponse, 'c')

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Envoi du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur(5)

        print('**** test Dvtm ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dvtm')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Déclaration initiale')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nom'] = 'La grosse course 6'
        data['vehicules'] = 2
        data['nb_vehicules_accompagnement'] = 1
        data['nb_personnes_pts_rassemblement'] = 2
        data['date_debut'] = datetime.now() + timedelta(days=59)
        data['date_fin'] = datetime.now() + timedelta(days=59, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print('\t' + url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=61)
        data['date_fin'] = datetime.now() + timedelta(days=61, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+\\n.+Dossier à envoyer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie manquante', count=1)
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'Itinéraire horaire', count=1)

        fichier = open(os.path.join('evenements/tests/data-carto.json'))
        data = fichier.read()
        jsonData = json.loads(data)
        jsonData['manif'] = action.group('url').split('/')[2]

        self.client.post('/carto/parcours/', jsonData, content_type="application/json", follow=True, HTTP_HOST='127.0.0.1:8000')
        url = "/".join(action.group('url').split('/')[:3]) + "/"
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie manquante')

        # Test de la page Pièces jointes
        date1 = (datetime.now() + timedelta(days=2)).strftime('%d/%m/%Y')
        date2 = (datetime.now() + timedelta(days=41)).strftime('%d/%m/%Y')
        date3 = (datetime.now() + timedelta(days=56)).strftime('%d/%m/%Y')
        liste_pj = ["Réglement de la manifestation", "Dispositions prises pour la sécurité",
                    "Itinéraire horaire", "Attestation d'assurance",
                    'Attestation de présence de médecin(s)', 'Attestation de présence de secouriste(s)',
                    'Attestation de présence de ambulance(s)']
        liste_pj_item = [(date1, "requise"), (date1, "requise"),
                         (date1, "requise"), (date3, ""),
                         ("Aucune", ""), ("Aucune", ""), ("Aucune", "")]
        self.fonction_test_page_fichiers(reponse, liste_pj, liste_pj_item)

        reponse = self.joindre_fichiers(reponse, 'v')

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Envoi du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur(6)

        print('**** test Avtm ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Avtm')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Déclaration initiale')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nom'] = 'La grosse course 7'
        data['vehicules'] = 2
        data['nb_vehicules_accompagnement'] = 1
        data['date_debut'] = datetime.now() + timedelta(days=89)
        data['date_fin'] = datetime.now() + timedelta(days=89, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print('\t' + url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=91)
        data['date_fin'] = datetime.now() + timedelta(days=91, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+\\n.+Dossier à envoyer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie manquante', count=1)
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'Carte zone publique', count=1)
        self.assertContains(reponse, 'Dossier sécurité', count=1)
        self.assertContains(reponse, 'Itinéraire horaire', count=1)
        self.assertNotContains(reponse, 'Liste complète des participants')

        fichier = open(os.path.join('evenements/tests/data-carto.json'))
        data = fichier.read()
        jsonData = json.loads(data)
        jsonData['manif'] = action.group('url').split('/')[2]

        self.client.post('/carto/parcours/', jsonData, content_type="application/json", follow=True, HTTP_HOST='127.0.0.1:8000')
        url = "/".join(action.group('url').split('/')[:3]) + "/"
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie manquante')

        # Test de la page Pièces jointes
        date1 = (datetime.now() + timedelta(days=2)).strftime('%d/%m/%Y')
        date2 = (datetime.now() + timedelta(days=71)).strftime('%d/%m/%Y')
        date3 = (datetime.now() + timedelta(days=86)).strftime('%d/%m/%Y')
        liste_pj = ["Réglement de la manifestation", "Dispositions prises pour la sécurité",
                    "Dossier sécurité", "Attestation d'assurance", "Carte zone publique", "Itinéraire horaire",
                    "Liste complète des participants (voir a331-21)", "Avis fédération délégataire",
                    'Attestation de présence de médecin(s)', 'Attestation de présence de secouriste(s)',
                    'Attestation de présence de ambulance(s)']
        liste_pj_item = [(date1, "requise"), (date1, "requise"),
                         (date1, "requise"), (date3, ""), (date1, "requise"), (date1, "requise"),
                         (date3, ""), (date2, ""),
                         ("Aucune", ""), ("Aucune", ""), ("Aucune", "")]
        self.fonction_test_page_fichiers(reponse, liste_pj, liste_pj_item)

        reponse = self.joindre_fichiers(reponse, 'm')

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Envoi du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur(7)

        print('**** test Dvtmcir ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dvtmcir')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Déclaration initiale')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nom'] = 'La grosse course 8'
        data['date_debut'] = datetime.now() + timedelta(days=59)
        data['date_fin'] = datetime.now() + timedelta(days=59, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print('\t' + url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=61)
        data['date_fin'] = datetime.now() + timedelta(days=61, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+\\n.+Dossier à envoyer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)

        # Test de la page Pièces jointes
        date1 = (datetime.now() + timedelta(days=2)).strftime('%d/%m/%Y')
        date2 = (datetime.now() + timedelta(days=41)).strftime('%d/%m/%Y')
        date3 = (datetime.now() + timedelta(days=56)).strftime('%d/%m/%Y')
        liste_pj = ["Réglement de la manifestation", "Dispositions prises pour la sécurité",
                    "Liste des dates des manifestations", "Attestation d'assurance",
                    'Attestation de présence de médecin(s)', 'Attestation de présence de secouriste(s)',
                    'Attestation de présence de ambulance(s)']
        liste_pj_item = [(date1, "requise"), ("Aucune", ""),
                         ("Aucune", ""), (date3, ""),
                         ("Aucune", ""), ("Aucune", ""), ("Aucune", "")]
        self.fonction_test_page_fichiers(reponse, liste_pj, liste_pj_item)
        reponse = self.joindre_fichiers(reponse, 's')

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Envoi du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur(8)

        # print(reponse.content.decode('utf-8'))
        # f = open('/var/log/manifsport/test_output.html', 'w', encoding='utf-8')
        # f.write(str(reponse.content.decode('utf-8')).replace('\\n',""))
        # f.close()
