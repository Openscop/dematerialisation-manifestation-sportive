import re, os

from django.test import TestCase
from django.contrib.auth.hashers import make_password as make
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from allauth.account.models import EmailAddress
from post_office.models import EmailTemplate

from core.factories import UserFactory
from core.models import User, Instance, ConfigurationGlobale
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from sports.factories import ActiviteFactory
from evenements.factories import AvtmFactory
from evaluation_incidence.factories import N2kSiteFactory
from structure.factories.structure import StructureOrganisatriceFactory


class Avmt_Clonage(TestCase):
    """
    Test du clonage d'une manifestation
        circuit_GGD : Avis GGD + préavis EDSR
        instruction par arrondissement
        openrunner false par défaut
    """
    # La modification de MEDIA_ROOT n'est pas possible içi
    # le clonage utilise le settings MEDIA_ROOT original
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
            Création des objets nécessaires à la création de la manifestation
            Création des fichiers nécessaires à joindre à la création de la manifestation
        """
        print()
        print('========= Clonage manifestation =========')
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        ConfigurationGlobale.objects.create(pk=1, mail_action=True, mail_suivi=True, mail_tracabilite=True,
                                            mail_actualite=True)
        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__instruction_mode=Instance.IM_CIRCUIT_B)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        prefecture = arrondissement.organisations.first()
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        cls.organisateur = UserFactory.create(username='organisateur', password=make("123"), default_instance=cls.dep.instance)
        EmailAddress.objects.create(user=cls.organisateur, email='orga@test.fr', primary=True, verified=True)
        # Création d'une structure
        cls.structure_organisatrice = StructureOrganisatriceFactory.create(precision_nom_s="structure_test",
                                                                           commune_m2m=cls.commune, instance_fk=cls.dep.instance)
        cls.organisateur.organisation_m2m.add(cls.structure_organisatrice)

        activ = ActiviteFactory.create()
        cls.instructeur = UserFactory.create(username='instructeur', password=make("123"),
                                             default_instance=cls.dep.instance, email='inst@test.fr')
        cls.instructeur.organisation_m2m.add(prefecture)

        EmailTemplate.objects.create(name='new_msg')
        for user in User.objects.all():
            user.optionuser.notification_mail = True
            user.optionuser.action_mail = True
            user.optionuser.tracabilite_mail = True
            user.optionuser.save()

        cls.manifestation = AvtmFactory.create(ville_depart=cls.commune, structure_organisatrice_fk=cls.structure_organisatrice, nom='Manifestation_Test',
                                               activite=activ, villes_traversees=(cls.autrecommune,))
        cls.manifestation.nb_participants = 150
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_organisateurs = 5
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.vehicules = 150
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.save()
        n2ksite = N2kSiteFactory.create()
        cls.manifestation.sites_natura2000.add(n2ksite)

        for file in ('reglement_manifestation', 'disposition_securite',
                     'itineraire_horaire', 'carte_zone_public', 'dossier_securite'):
            mon_fichier = open("/tmp/"+file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test clonage avtm")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Suppression des fichiers créés dans /tmp
        """
        for file in ('reglement_manifestation', 'disposition_securite',
                     'itineraire_horaire', 'carte_zone_public', 'dossier_securite'):
            os.remove("/tmp/"+file+".txt")

        super().tearDownClass()

    def joindre_fichiers(self, reponse):
        """
        Ajout des fichiers nécessaires à la manifestation
        :param reponse: reponse précédente
        :param cas: suivant cerfa considéré
        :return: reponse suivante
        """
        # appel de la vue pour joindre les fichiers
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        print('\t' + url_script.group('url'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite"]
        liste_files += ["itineraire_horaire", 'carte_zone_public', 'dossier_securite']
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')
        # Retour à la vue de détail
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail1 = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail1, 'group'))
        reponse = self.client.get(detail1.group('url'), HTTP_HOST='127.0.0.1:8000')
        return reponse

    def declarer_manif(self, reponse):
        """
        Soumettre la manifestation à l'instructeur
        :param reponse: reponse précédente
        :return: reponse suivante
        """
        # appel de la vue pour déclarer la manifestation
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(declar, 'group'))
        print('\t' + declar.group('url'))
        reponse = self.client.get(declar.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Demander l\'instruction', count=2)
        # Soumettre la déclaration
        reponse = self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        return reponse

    def test_clonage_avtm(self):
        """
        Test du clonage d'une manifestation
        """
        print('**** Complèter Avtm ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('1', nb_bloc.group('nb'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/liste?&filtre_etat=attente',
                                  HTTP_HOST='127.0.0.1:8000')
        detail1 = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail1, 'group'))
        reponse = self.client.get(detail1.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')

        reponse = self.joindre_fichiers(reponse)

        # reponse = self.declarer_manif(reponse)
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(declar, 'group'))
        print('\t' + declar.group('url'))
        # retour à la vue de détail et test breadcrumbs
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        # etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Envoi de la demande', reponse.content.decode('utf-8'))
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+\\n.+Dossier à envoyer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        # self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')
        self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton vert Déclaration')

        print('**** Clonage Avtm ****')
        # Recherche de l'url de clonage
        cloner = re.search('href="(?P<url>(/[^"]+)).+Créer un nouveau dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(cloner, 'group'))
        print('\t' + cloner.group('url'))
        # Informations de clonage
        data = {}
        data['nom'] = 'Clonage_de_manifestation'
        data['date_debut'] = self.manifestation.date_debut.strftime('%d/%m/%Y 8:00')
        data['date_fin'] = self.manifestation.date_fin.strftime('%d/%m/%Y 20:00')

        # Clonage de la manifestation et redirect sur le détail de la nouvelle manifestation
        reponse = self.client.post(cloner.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        reponse = self.client.get(reponse.content.decode('utf-8'), HTTP_HOST='127.0.0.1:8000')
        self.assertTrue(self.manifestation.dossier_complet())
        self.assertNotContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Clonage_De_Manifestation')

        # Test breadcrumbs
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+\\n.+Dossier à envoyer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Recherche du pk de la manifestation
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        el = action.group('url').split('/')
        self.assertTrue(el[2].isdecimal(), msg='pas un nombre')
        self.assertNotEqual(el[2], str(self.manifestation.pk), msg="le pk du clone est identique à l'original")

        # print('**** Suppression modèle ****')
        # reponse = self.client.get(detail1.group('url'), HTTP_HOST='127.0.0.1:8000')
        # self.assertContains(reponse, 'Manifestation_Test')
        # action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Supprimer ce dossier', reponse.content.decode('utf-8'))
        # self.assertTrue(hasattr(action, 'group'))
        # print('\t' + action.group('url'))
        # reponse = self.client.post(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # self.assertContains(reponse, 'Tableau de bord', count=3)
        # nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        # self.assertTrue(hasattr(nb_bloc, 'group'))
        # self.assertEqual('1', nb_bloc.group('nb'))

        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail2 = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail2, 'group'))
        reponse = self.client.get(detail2.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Clonage_De_Manifestation')
        self.assertContains(reponse, 'Roche')
        self.assertContains(reponse, 'Natura2000_')
        # Si tous les fichiers sont présents
        self.assertContains(reponse, 'Envoyer la demande')

        print('**** Suppression des médias du clone ****')
        if el[2]:
            path = settings.MEDIA_ROOT + self.dep.name + '/' + el[2]
            print(path)
            os.system('rm -R ' + path)

        # print(reponse.content.decode('utf-8'))
        # f = open('/var/log/manifsport/test_output.html', 'w', encoding='utf-8')
        # f.write(str(reponse.content.decode('utf-8')).replace('\\n',""))
        # f.close()
