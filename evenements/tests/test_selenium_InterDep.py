import re, time

from django.contrib.auth.hashers import make_password as make
from django.test import tag, override_settings

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from allauth.account.models import EmailAddress

from .test_base_selenium import SeleniumCommunClass
from sports.factories import ActiviteFactory, DisciplineFactory
from evenements.factories import DcnmFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from evenements.models import Manif
from core.models import Instance
from core.factories import UserFactory
from structure.factories.service import ServiceConsulteFactory, TypeServiceFactory


class TestInterdepartementalite(SeleniumCommunClass):
    """
    Test de l'interdepartementalite
    """

    DELAY = 0.4

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets nécessaires au formulaire de création de manifestation
        """
        print()
        print('========== Interdépartementalité ==========')
        SeleniumCommunClass.init_setup(cls)

        cls.autre_dep = DepartementFactory.create(name='43',
                                                  instance__name="autre instance de test",
                                                  instance__etat_s="ouvert",
                                                  instance__instruction_mode=Instance.IM_CIRCUIT_C,
                                                  instance__acces_arrondissement=2)

        cls.autre_dep_inactf = DepartementFactory.create(name='69',
                                                         instance__name="autre instance de test inactif",
                                                         instance__etat_s="ferme",
                                                         instance__instruction_mode=Instance.IM_CIRCUIT_A,
                                                         )

        autre_arrondissement = ArrondissementFactory.create(name='Brioude', code='100', departement=cls.autre_dep)
        cls.prefecture_2 = autre_arrondissement.organisations.first()

        cls.edsr = cls.autre_dep.organisation_set.get(precision_nom_s="EDSR")
        cls.edsr.autorise_rendre_avis_b = True
        cls.edsr.save()

        type_autre_service = TypeServiceFactory(categorie_fk__nom_s="Autre Service")
        cls.service = ServiceConsulteFactory(instance_fk=cls.autre_dep.instance, type_service_fk=type_autre_service, porte_entree_avis_b=True)

        autre_dep_commune = CommuneFactory(name='Agnat', arrondissement=autre_arrondissement)
        cls.autre_dep_commune = autre_dep_commune

        autre_arrondissement_inactif = ArrondissementFactory.create(name='Lyon', code='000', departement=cls.autre_dep_inactf)
        autre_dep_inactif_commune = CommuneFactory(name='Lyon', arrondissement=autre_arrondissement_inactif)
        cls.autre_dep_inactif_commune = autre_dep_inactif_commune

        disc = DisciplineFactory(motorise=False, name='Vélo')
        cls.activ = ActiviteFactory.create(discipline=disc)

        disc_2 = DisciplineFactory(motorise=True, name='Moto')
        cls.activ_2 = ActiviteFactory.create(discipline=disc_2)

        cls.instructeur_2 = UserFactory.create(username='instructeur_2',
                                               email='instructeur2@example.com',
                                               password=make("123"),
                                               default_instance=cls.autre_dep.instance)

        cls.agent = UserFactory.create(username='agent_service',
                                       email='agent_service@example.com',
                                       password=make("123"),
                                       default_instance=cls.autre_dep.instance)

        EmailAddress.objects.create(user=cls.agent, email='agent_service@example.com', primary=True,
                                    verified=True)

        cls.agent.organisation_m2m.add(cls.service)

        # cls.service = cls.agent.get_service()
        cls.service.departement_m2m.add(cls.dep)
        cls.service.departement_m2m.add(cls.autre_dep)
        cls.service.autorise_rendre_avis_b = True
        cls.service.porte_entree_avis_b = True
        cls.service.save()

        EmailAddress.objects.create(user=cls.instructeur_2, email='instructeur2@example.com', primary=True,
                                    verified=True)
        # InstructeurFactory.create(user=cls.instructeur_2, prefecture=cls.prefecture_2)
        cls.instructeur_2.organisation_m2m.add(cls.prefecture_2)

        cls.manifestation = DcnmFactory.create(ville_depart=cls.autre_dep_commune, structure_organisatrice_fk=cls.structure_organisatrice,
                                               nom='La grosse course_Dcnm', activite=cls.activ)
        cls.manifestation.nb_participants = 1
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.departements_traverses.add(cls.dep)
        cls.manifestation.ville_depart_interdep_mtm.add(cls.commune)
        cls.manifestation.gestion_instance_instruites()
        cls.manifestation.save()
        cls.manifestation.notifier_creation()

        super().setUpClass()

    @tag('selenium')
    @override_settings(DEBUG=True)
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
    def test_Interdepartementalite(self):
        """
        Manifestation déclarer sur plusieurs départements
        :return:
        """

        print('**** test 1 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username=self.organisateur.username, password='123'))
        # Appel de la page tableau de bord organisateur
        self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/liste?&filtre_etat=attente',
                                  HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))",
                               reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = [("reglement_manifestation", "reglement_manifestation"),
                       ("disposition_securite", "disposition_securite"),
                       ("itineraire_horaire_" + str(self.dep.pk), "itineraire_horaire"),
                       ("itineraire_horaire_" + str(self.autre_dep.pk), "itineraire_horaire")
                       ]
        for file, fichier in liste_files:
            with open('/tmp/' + fichier + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test aiguillage ****')
        # Connexion
        self.connexion('organisateur')
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Déclarer ou demander').click()
        time.sleep(self.DELAY)
        # remplir formulaire d'aiguillage
        self.chosen_select('id_discipline_chosen', 'Moto')
        time.sleep(self.DELAY)
        self.chosen_select('id_activite_chosen', 'Activite')
        time.sleep(self.DELAY)
        self.chosen_select('id_departement_chosen', '43')
        time.sleep(self.DELAY)
        self.chosen_select('id_ville_depart_chosen', 'Agnat')
        time.sleep(self.DELAY)
        self.chosen_select('id_emprise_chosen', 'Partiellement sur voies publiques ou ouvertes à la circulation')
        time.sleep(self.DELAY)
        entries = self.selenium.find_element(By.ID, 'id_nb_participants')
        entries.send_keys('150')
        # Test la limite de 20 département
        nb_departements = self.selenium.find_element(By.ID, 'id_nb_departement')
        nb_departements.send_keys('21')
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, "//button[contains(text(),'Rechercher')]").click()
        time.sleep(self.DELAY)
        self.assertIn('Actuellement, la plateforme ne prend en charge que les manifestations de moins de 20 départements.', self.selenium.page_source)
        nb_departements.clear()
        nb_departements.send_keys('2')

        self.selenium.find_element(By.XPATH, "//button[contains(text(),'Rechercher')]").click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        self.assertIn('Organisation d\'une manifestation sportive', self.selenium.page_source)

        print('**** remplissage formulaire ****')
        # Remplir formulaire manifestation
        self.remplir_form("interdepV")
        time.sleep(self.DELAY * 5)
        self.remplir_carto()
        self.selenium.execute_script("window.scroll(0, 1000)")
        self.assertIn('Autres départements traversés par la manifestation :', self.selenium.page_source)
        self.assertIn('Communes de départ pour le département 42 - Loire :', self.selenium.page_source)
        self.assertIn('Bard', self.selenium.page_source)
        self.assertIn('Départements où ce dossier peut être pris en charge par cette plateforme :',
                      self.selenium.page_source)
        self.assertIn('Veuillez prendre contact avec le service instructeur des départements',
                      self.selenium.page_source)

        # Tester les fichiers manquants
        card = self.selenium.find_element(By.XPATH, "//div[@class='card-header bg-danger text-white']//parent::*")
        self.assertIn('Liste des pièces jointes manquantes', card.text)
        files = card.find_elements(By.CSS_SELECTOR, 'li')
        self.assertEqual(7, len(files))
        for file in files:
            if file.text not in ('Critères nationaux', 'Réglement de la manifestation',
                                 'Carte zone publique', 'Dossier sécurité',
                                 'Dispositions prises pour la sécurité',
                                 'Itinéraire horaire pour le département 42 - Loire',
                                 'Itinéraire horaire pour le département 43 - Haute-Loire'):
                self.assertTrue(False, msg="le fichier " + file.text + " n\'est pas dans la liste")

        print('**** test ajout fichiers ****')
        manif = Manif.objects.last()
        # Eviter l'obligation d'évaluation Natura 2000
        manif.vtm_hors_circulation = False
        manif.save()
        self.selenium.execute_script("window.scroll(0, 0)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, 'pj_orga').click()
        time.sleep(self.DELAY * 2)
        self.charger_fichier('reglement_manifestation')
        self.charger_fichier('disposition_securite')
        self.charger_fichier('itineraire_horaire_' + str(self.dep.pk))
        self.charger_fichier('itineraire_horaire_' + str(self.autre_dep.pk))
        self.charger_fichier('carte_zone_public')
        self.charger_fichier('dossier_securite')

        print('**** test envoi ****')
        self.selenium.execute_script("window.scroll(0, 0)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, "//button[contains(.,'Détail de la manifestation')]").click()
        time.sleep(self.DELAY * 3)
        self.envoyer_la_demande()
        # Tester le status
        boutons = self.selenium.find_elements(By.CSS_SELECTOR, '.page-header li')
        try:
            boutons[0].find_element(By.CLASS_NAME, 'fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="L'étape 'Préparation du dossier' n\'est pas vert")
        try:
            boutons[2].find_element(By.CLASS_NAME, 'fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="L'étape 'Envoi de la demande' n\'est pas vert")
        time.sleep(self.DELAY * 2)
        self.deconnexion()

        print('**** test instructeur 1 ****')
        # Connexion
        self.connexion('instructeur')
        self.selenium.find_element(By.ID, 'btn_etat_demande').click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.XPATH, '//td/span[contains(text(), "La grosse course_Vtm")]').click()

        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        time.sleep(self.DELAY * 4)
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        time.sleep(self.DELAY * 4)
        # Rendre un avis
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Rendre l\'avis').click()
        time.sleep(self.DELAY * 4)
        self.selenium.find_element(By.XPATH, "//span[contains(text(), 'Avis favorable')]/..").click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Avis de la Préfecture rendu', self.selenium.page_source)
        # Vérification des piéces jointes accessible
        self.selenium.find_element(By.ID, 'pj_instru').click()
        time.sleep(self.DELAY)
        self.assertIn('Itinéraire horaire pour le département 42 - Loire', self.selenium.page_source)
        self.assertNotIn('Itinéraire horaire pour le département 43 - Haute-Loire', self.selenium.page_source)
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Tableau de bord').click()
        self.selenium.find_element(By.ID, 'btn_etat_demande').click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 1000)")
        time.sleep(self.DELAY * 4)
        self.selenium.find_element(By.XPATH, '//td/span[contains(text(), "La grosse course_Dcnm")]').click()
        # Vérifier lecture d'état
        self.assertIn('État des instructions dans tous les départements', self.selenium.page_source)
        time.sleep(self.DELAY * 3)
        # Envoie d'avis au service simple
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Envoyer une demande d'avis").click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % 'Autre Service').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'name_service').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'btn-action-selecteur').click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.ID, 'btn-close-modal-avis').click()
        time.sleep(self.DELAY * 4)
        self.deconnexion()

        print('**** test instructeur 2 ****')
        self.connexion('instructeur_2')
        self.selenium.find_element(By.ID, 'btn_etat_demande').click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.XPATH, '//td/span[contains(text(), "La grosse course_Dcnm")]').click()
        self.assertIn('État des instructions dans tous les départements', self.selenium.page_source)
        # Envoie une demande d'avis
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Envoyer une demande d'avis").click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % 'Autre Service').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'name_service').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'btn-action-selecteur').click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.ID, 'btn-close-modal-avis').click()
        time.sleep(self.DELAY * 3)
        # Vérification des piéces jointes accessible
        self.selenium.find_element(By.ID, 'pj_instru').click()
        time.sleep(self.DELAY)
        self.assertNotIn('Itinéraire horaire pour le département 42 - Loire', self.selenium.page_source)
        self.assertIn('Itinéraire horaire pour le département 43 - Haute-Loire', self.selenium.page_source)
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Tableau de bord').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn_etat_demande').click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 1000)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.XPATH, '//td/span[contains(text(), "La grosse course_Vtm")]').click()
        time.sleep(self.DELAY * 5)
        # Vérification de l'état des avis dans l'autre département
        self.selenium.find_element(By.ID, 'accordion_prefecture').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('Favorable', self.selenium.page_source)
        self.deconnexion()

        print('**** test agent service ****')
        # Avis du 43
        self.connexion(self.agent.username)
        self.presence_avis('nouveau')
        self.selenium.find_element(By.ID, 'btn_etat_demande').click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.XPATH, '//td/span[contains(text(), "La grosse course_Dcnm")]').click()
        time.sleep(self.DELAY * 2)
        # Vérifier l'action disponible
        self.assertIn('Rédiger l\'avis', self.selenium.page_source)
        # Valider l'événement
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Rédiger l\'avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Enregistrer').click()
        time.sleep(self.DELAY * 10)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Valider et rendre l\'avis').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.CLASS_NAME, 'test-btn-valider').click()
        time.sleep(self.DELAY * 2)
        # Avis du 42
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Tableau de bord').click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.ID, "dept_43").click()
        self.presence_avis('nouveau')
        self.selenium.find_element(By.ID, 'btn_etat_demande').click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.XPATH, '//td/span[contains(text(), "La grosse course_Dcnm")]').click()
        time.sleep(self.DELAY * 2)
        # Vérifier l'action disponible
        self.assertIn('Rédiger l\'avis', self.selenium.page_source)
        # Valider l'événement
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Rédiger l\'avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Enregistrer').click()
        time.sleep(self.DELAY * 10)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Valider et rendre l\'avis').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.CLASS_NAME, 'test-btn-valider').click()
        time.sleep(self.DELAY * 3)
        self.deconnexion()
