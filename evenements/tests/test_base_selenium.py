import os, time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.hashers import make_password as make
from post_office.models import EmailTemplate
from django.contrib.contenttypes.models import ContentType

from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium import webdriver

from allauth.account.models import EmailAddress

from core.models import Instance
from core.factories import UserFactory
from evenements.factories import DcnmFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from sports.factories import ActiviteFactory
from structure.factories.structure import StructureOrganisatriceFactory
from structure.factories.service import ServiceConsulteFactory
from carto.services.migrationHelper import createPoiData
from carto.models import PoiApi, PoiCategorieApi


class SeleniumCommunClass(StaticLiveServerTestCase):
    """
    Méthodes communes aux tests Sélénium
    """
    DELAY = 0.35


    def init_setup(self):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets sur le 42
            Création des utilisateurs
            Création de l'événement
        """
        # To prevent download dialog
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        profile = webdriver.FirefoxProfile()
        profile.set_preference('browser.download.folderList', 2)  # custom location
        profile.set_preference('browser.download.manager.showWhenStarting', False)
        profile.set_preference('browser.download.dir', '/tmp')
        profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'application/gpx,text/csv')

        self.selenium = webdriver.Firefox(profile)
        self.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        self.selenium.set_window_size(800, 1200)

        self.dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__etat_s="ouvert",
                                        instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                        instance__acces_arrondissement=2)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=self.dep)
        self.prefecture = arrondissement.organisations.first()
        self.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        self.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        # Création des utilisateurs
        self.organisateur = UserFactory.create(username='organisateur',
                                               email='organisateur@example.com',
                                               password=make("123"),
                                               default_instance=self.dep.instance)
        EmailAddress.objects.create(user=self.organisateur, email='organisateur@example.com', primary=True, verified=True)
        self.structure_organisatrice = StructureOrganisatriceFactory.create(instance_fk=self.dep.instance)
        self.structure_organisatrice.commune_m2m.add(self.commune)
        self.organisateur.organisation_m2m.add(self.structure_organisatrice)


        self.instructeur = UserFactory.create(username='instructeur',
                                              email='instructeur@example.com',
                                              password=make("123"),
                                              default_instance=self.dep.instance)
        self.instructeur.organisation_m2m.add(self.prefecture)

        EmailAddress.objects.create(user=self.instructeur, email='instructeur@example.com', primary=True, verified=True)

        EmailTemplate.objects.create(name='new_msg')

        self.activ = ActiviteFactory.create()

        self.manifestation = DcnmFactory.create(ville_depart=self.commune, structure_organisatrice_fk=self.structure_organisatrice, activite=self.activ,
                                                nom='Manifestation_Test')

        for file in ('reglement_manifestation', 'disposition_securite', 'dossier_tech_cycl', 'dossier_securite',
                     'itineraire_horaire', 'certificat_assurance', 'liste_signaleurs', 'convention_police',
                     'participants', 'avis_federation_delegataire', 'carte_zone_public', 'plan_masse'):
            mon_fichier = open("/tmp/"+file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test Sélénium")
            mon_fichier.close()
        # on ajoute les données des poi à la main, car le code python des migrations n’est pas exécuté
        createPoiData(PoiApi, PoiCategorieApi)

    @classmethod
    def tearDownClass(self):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """
        for file in ('reglement_manifestation', 'disposition_securite', 'dossier_tech_cycl', 'dossier_securite',
                     'itineraire_horaire', 'certificat_assurance', 'liste_signaleurs', 'convention_police',
                     'participants', 'avis_federation_delegataire', 'carte_zone_public', 'plan_masse'):
            os.remove("/tmp/"+file+".txt")

        os.system('rm -R /tmp/maniftest/media/42/')

        self.selenium.quit()
        super().tearDownClass()

    def connexion(self, username):
        """ Connexion de l'utilisateur 'username' """
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element(By.NAME, "login")
        pseudo_input.send_keys(username)
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element(By.NAME, "password")
        password_input.send_keys('123')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.assertIn('Connexion avec ' + username + ' réussie', self.selenium.page_source)

    def deconnexion(self):
        """ Deconnexion de l'utilisateur """
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.CLASS_NAME, 'deconnecter').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element(By.ID, id_chosen)
        chosen_select.click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements(By.TAG_NAME, 'li')
        for result in results:
            if value in result.text:
                result.click()

    def chosen_select_multi(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element(By.ID, id_chosen)
        chosen_select.find_element(By.CSS_SELECTOR,  "input").click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements(By.TAG_NAME, 'li')
        for result in results:
            if value in result.text:
                result.click()

    def vue_detail(self):
        """
        Appel de la vue de détail de la manifestation
        """
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        url = self.selenium.current_url
        self.selenium.find_element(By.XPATH, "//td[contains(text(),'La grosse course')]").click()
        wait = WebDriverWait(self.selenium, 15)
        wait.until(lambda driver: self.selenium.current_url != url)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        time.sleep(self.DELAY)

    def aucune_action(self):
        """
        Test auncune action affichée dans la zone action de la dashboard
        """
        action = self.selenium.find_element(By.XPATH, "//div[contains(@class, 'card')][contains(string(),'Actions requises')]")
        self.assertIn('Pas d\'actions requises', action.text)

    def retourTdB(self):
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)

    def presence_avis(self, state):
        """
        Test de la présence et l'état de l'événement
        :param state: couleur de l'événement
        """
        # Test du blocs concerné
        if state == 'nouveau':  # TdB instructeur
            bloc = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_demande')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element(By.CLASS_NAME, 'test_nb_atraiter').text)
            declar = self.selenium.find_element(By.TAG_NAME, 'tbody')
            try:
                declar.find_element(By.XPATH, "//td/span[contains(text(),'La grosse course')]")
                declar.find_element(By.CLASS_NAME, 'table_afaire')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de nouveau dossier")
        elif state == 'atraiter':  # Tdb Organisateur
            bloc = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_attente')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element(By.CLASS_NAME, 'test_nb_atraiter').text)
            declar = self.selenium.find_element(By.TAG_NAME, 'tbody')
            try:
                declar.find_element(By.XPATH, "//td[contains(text(),'La grosse course')]")
                declar.find_element(By.CLASS_NAME, 'table_afaire')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de dossier en cours")
        elif state == 'encours':  # Tdb Organisateur
            bloc = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_demande')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element(By.CLASS_NAME, 'test_nb_encours').text)
            declar = self.selenium.find_element(By.TAG_NAME, 'tbody')
            try:
                declar.find_element(By.XPATH, "//td[contains(text(),'La grosse course')]")
                declar.find_element(By.CLASS_NAME, 'table_encours')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de dossier en cours")
        else:
            self.assertEqual('0', '1', 'paramètre inconnu')

    def charger_fichier(self, fichier):
        """
        Upload du fichier passé en paramètre
        """
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY * 5)
        file = self.selenium.find_element(By.XPATH, '//form[@data-name="' + fichier + '"]')
        file.find_element(By.XPATH, 'span/a').click()
        fileinput = file.find_element(By.XPATH, 'span/input[@type="file"]')
        if fichier.startswith("itineraire_horaire"):
            fichier = "_".join(fichier.split('_')[:2])
        fileinput.send_keys('/tmp/' + fichier + '.txt')
        file.find_element(By.XPATH, 'span/input[@type="submit"]').click()
        time.sleep(self.DELAY*5)

    def remplir_form(self, cas="dnm"):
        name = self.selenium.find_element(By.ID, 'id_nom')
        name.send_keys('La grosse course')

        description = self.selenium.find_element(By.ID, 'id_description')
        description.send_keys('une grosse course qui déchire')
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)

        if cas == "dvtm":
            audience = self.selenium.find_element(By.ID, 'id_nb_personnes_pts_rassemblement')
            audience.send_keys('150')
            time.sleep(self.DELAY)

        audience = self.selenium.find_element(By.ID, 'id_nb_spectateurs')
        audience.send_keys('100')
        time.sleep(self.DELAY*2)

        organisateurs = self.selenium.find_element(By.ID, 'id_nb_organisateurs')
        organisateurs.send_keys('20')

        if cas in ["avtm", "dvtm"]:
            vehicules_participants = self.selenium.find_element(By.ID, 'id_vehicules')
            vehicules_participants.send_keys('150')
            time.sleep(self.DELAY*2)

        if cas != "dvtmcir":
            time.sleep(self.DELAY * 2)
            self.selenium.execute_script("window.scroll(0, 300)")
            time.sleep(self.DELAY*2)
            self.chosen_select_multi('id_villes_traversees_chosen', 'Roche')
            time.sleep(self.DELAY)
            vehicles = self.selenium.find_element(By.ID, 'id_nb_vehicules_accompagnement')
            vehicles.send_keys('1')
            time.sleep(self.DELAY)

        if cas == "police":
            self.selenium.execute_script("window.scroll(0, 2000)")
            time.sleep(self.DELAY)
            self.selenium.find_element(By.XPATH, "//span[contains(text(),'convention avec la police')]").click()
            time.sleep(self.DELAY)

        if cas in ["dnm", "police"]:
            self.selenium.execute_script("window.scroll(0, 1000)")
            time.sleep(self.DELAY)
            signaleurs = self.selenium.find_element(By.ID, 'id_nb_signaleurs')
            signaleurs.send_keys('1')
            time.sleep(self.DELAY)

        if cas[:-1] == "interdep":
            if cas[-1:] == "D":
                signaleurs = self.selenium.find_element(By.ID, 'id_nb_signaleurs')
                signaleurs.send_keys('1')
                name.send_keys('_Dcnm')
            else:
                vehicules_participants = self.selenium.find_element(By.ID, 'id_vehicules')
                vehicules_participants.send_keys('150')
                name.send_keys('_Vtm')
            self.chosen_select('id_departements_traverses_chosen', 'Loire')
            time.sleep(self.DELAY * 2)
            self.chosen_select('ville_depart_interdep_' + str(self.dep.pk) + '_chosen', 'Bard')
            time.sleep(self.DELAY)
            self.chosen_select('id_departements_traverses_chosen', 'Rhône')
            time.sleep(self.DELAY * 2)
            self.chosen_select('ville_depart_interdep_' + str(self.autre_dep_inactf.pk) + '_chosen', 'Lyon')

        nom_contact = self.selenium.find_element(By.ID, 'id_nom_contact')
        nom_contact.send_keys('durand')
        time.sleep(self.DELAY)

        prenom_contact = self.selenium.find_element(By.ID, 'id_prenom_contact')
        prenom_contact.send_keys('joseph')
        time.sleep(self.DELAY)

        tel_contact = self.selenium.find_element(By.ID, 'id_tel_contact')
        tel_contact.send_keys('0605555555')
        self.selenium.execute_script("window.scroll(0, document.body.scrollHeight)")
        time.sleep(self.DELAY*2)

        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY*2)

    def remplir_carto(self):
        self.selenium.find_element(By.XPATH, "//button[contains(.,'Cartographie')]").click()
        time.sleep(self.DELAY*2)

        self.selenium.find_element(By.ID, 'btn-selecteur-add-parcours').click()
        time.sleep(self.DELAY*2)

        self.selenium.find_element(By.XPATH, "//button//span[contains(text(),'Charger ancien parcours')]").click()
        time.sleep(self.DELAY)

        search_field = self.selenium.find_element(By.CLASS_NAME, 'vs__search')
        search_field.send_keys('parcour')
        time.sleep(self.DELAY * 2)

        self.selenium.find_element(By.XPATH, "//ul[contains(@id, 'listbox')]//li[contains(text(),'Parcours')]").click()
        time.sleep(self.DELAY)

        self.selenium.find_element(By.XPATH, "//button[contains(text(),'Chargez le parcours')]").click()
        time.sleep(self.DELAY * 5)

        self.selenium.find_element(By.XPATH, "//div[contains(@id,'icon-save-parcours')]").click()
        time.sleep(self.DELAY * 2)

        parcours = self.selenium.find_element(By.ID, 'save-parcours-description')
        parcours.send_keys('parcours de 10Km')

        self.selenium.find_element(By.XPATH, "//button//span[contains(text(),'Enregistrer')]").click()
        time.sleep(self.DELAY)

        self.selenium.find_element(By.XPATH, "//button[contains(.,'Détail de la manifestation')]").click()
        time.sleep(self.DELAY)

    def envoyer_la_demande(self):
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Envoyer la demande').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, '//input[@name="dispos"]').click()
        self.selenium.find_element(By.XPATH, '//input[@name="pieces"]').click()
        self.selenium.find_element(By.XPATH, '//input[@name="arretes"]').click()
        self.selenium.find_element(By.XPATH, '//input[@name="exact"]').click()
        self.selenium.find_element(By.XPATH, '//input[@name="confirme_anomalie"]').click()
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY)
