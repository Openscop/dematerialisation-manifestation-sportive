import re, os, json
from datetime import timedelta, datetime

from django.test import TestCase, override_settings
from django.contrib.auth.hashers import make_password as make
from django.contrib.contenttypes.models import ContentType

from post_office.models import EmailTemplate

from core.factories import UserFactory
from core.models import User, Instance, ConfigurationGlobale
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from evenements.models import Manif
from sports.factories import ActiviteFactory
from messagerie.models import Message
from structure.factories.structure import StructureOrganisatriceFactory


class ManifHorsDelaiTests(TestCase):
    """
    Test de l'autorisation de dépôt d'une manifestation hors délai
    """

    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
            Création des objets nécessaires au formulaire de création de manifestation
            Création des fichiers nécessaires à joindre à la création de la manifestation
        """
        print()
        print('========= Manifestation hors délai =========')

        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        ConfigurationGlobale.objects.create(pk=1, mail_action=True, mail_suivi=True, mail_tracabilite=True,
                                            mail_actualite=True)
        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                            instance__etat_s="ouvert")
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        cls.prefecture = arrondissement.organisations.first()
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        cls.organisateur = UserFactory.create(username='organisateur', password=make("123"),
                                              default_instance=cls.dep.instance, email='orga@test.fr')
        # Création d'une structure
        cls.structure_organisatrice = StructureOrganisatriceFactory.create(precision_nom_s="structure_test",
                                                                           commune_m2m=cls.commune, instance_fk=cls.dep.instance)
        cls.organisateur.organisation_m2m.add(cls.structure_organisatrice)

        cls.instructeur = UserFactory.create(username='instructeur', password=make("123"),
                                             default_instance=cls.dep.instance, email='inst@test.fr')
        cls.instructeur.organisation_m2m.add(cls.prefecture)

        cls.activ = ActiviteFactory.create()
        EmailTemplate.objects.create(name='new_msg')
        for user in User.objects.all():
            user.optionuser.notification_mail = True
            user.optionuser.action_mail = True
            user.optionuser.tracabilite_mail = True
            user.optionuser.save()

        for file in ('reglement_manifestation', 'disposition_securite', 'liste_signaleurs',
                     'participants', 'itineraire_horaire', 'itineraire_horaire_'+str(cls.dep.pk), 'dossier_tech_cycl',
                     'convention_police', 'plan_masse'):
            mon_fichier = open("/tmp/"+file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test manif hors delai")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Suppression des fichiers créés dans /tmp
        """
        for file in ('reglement_manifestation', 'disposition_securite', 'liste_signaleurs',
                     'participants', 'itineraire_horaire', 'itineraire_horaire_'+str(cls.dep.pk), 'dossier_tech_cycl',
                     'convention_police', 'plan_masse'):
            os.remove("/tmp/"+file+".txt")

        os.system('rm -R /tmp/maniftest/media/42/')

        super().tearDownClass()

    def nouvelle_manif(self, reponse):
        """
        Appel de la vue d'aiguillage
        :param reponse: réponse précédente
        :return: réponse suivante
        """
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Déclarer ou demander ', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        reponse = self.client.get(action.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Recherche de formulaire', count=2)
        self.assertContains(reponse, 'Rechercher le formulaire')
        self.assertContains(reponse, 'Accéder au formulaire')

        return reponse

    def donnees_aiguillage(self, reponse, formulaire):
        """
        Remplissage des champs pour aiguiller sur la bonne manifestation spécialisée
        :param reponse: reponse précédente
        :param formulaire: cerfa demandé
        :return: data à poster
        """
        data = {}
        # Informations générales
        data['csrf_token'] = reponse.context['csrf_token']

        data['discipline'] = self.activ.discipline.pk
        data['activite'] = self.activ.pk
        data['nb_participants'] = 200
        data['ville_depart'] = self.commune.pk
        data['departement'] = self.commune.arrondissement.departement.pk
        data['emprise'] = 3
        data['type_circuit'] = 0
        data['formulaire'] = formulaire
        return data

    def donnees_formulaire(self, reponse):
        """
        Récupération des données du formulaire concernant le formset de contact qui a des données à restituer
        et remplissage des champs communs
        :param reponse: reponse précédente
        :return: data à poster
        """
        data = {}
        # Informations générales
        data['csrf_token'] = reponse.context['csrf_token']
        data['description'] = 'une grosse course'
        data['nb_spectateurs'] = 15
        data['nb_organisateurs'] = 15
        data['villes_traversees'] = self.autrecommune.pk
        data['nom_contact'] = 'durand'
        data['prenom_contact'] = 'joseph'
        data['tel_contact'] = '0605555555'
        # Le champ n'est pas rempli avec le kwarg de la vue, alors on le passe
        data['departement_depart'] = self.dep.pk
        """
        print('#' * 30)
        for i in sorted(data.keys()):
            print(i, '\t:', data[i])
        print('#' * 30)
        """
        return data

    def joindre_fichiers(self, reponse, cas='d'):
        """
        Ajout des fichiers nécessaires à la manifestation
        :param reponse: reponse précédente
        :param cas: suivant cerfa considéré
        :return: reponse suivante
        """
        # appel de la vue pour joindre les fichiers
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        print('\t' + url_script.group('url'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation",  "disposition_securite"]
        if cas == 'v':
            liste_files += ["itineraire_horaire"]
        if cas == 'd':
            liste_files += ["itineraire_horaire"]
        if cas == 'dx':
            # cas interdepartementalité
            liste_files += ['itineraire_horaire_'+str(self.dep.pk), 'itineraire_horaire_'+str(self.other_dep.pk)]
        if cas == 'c':
            liste_files += ["dossier_tech_cycl", "itineraire_horaire"]
        if cas == 'cc':
            liste_files += ["itineraire_horaire", "convention_police"]
        if cas == 'm':
            liste_files += ["itineraire_horaire", "participants"]
        if cas == 's':
            liste_files.remove('disposition_securite')
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')
        # Retour à la vue de détail
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail1 = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail1, 'group'))
        reponse = self.client.get(detail1.group('url'), HTTP_HOST='127.0.0.1:8000')
        return reponse

    def declarer_manif(self, reponse):
        """
        Soumettre la manifestation à l'instructeur
        :param reponse: reponse précédente
        :return: reponse suivante
        """
        # appel de la vue pour déclarer la manifestation
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(declar, 'group'))
        print('\t' + declar.group('url'))
        reponse = self.client.get(declar.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Demander l\'instruction', count=2)
        # Soumettre la déclaration
        reponse = self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        return reponse

    def verification_instructeur(self, nb):
        """
        Vérification dans la dashboard instructeur de l'affichage de la manifestation crée
        :return:
        """
        # Connexion avec l'instructeur
        self.assertTrue(self.client.login(username="instructeur", password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/instructions/tableaudebord/', HTTP_HOST='127.0.0.1:8000', follow=True)
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual("1", nb_bloc.group('nb'))
        nb_bloc = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('0', nb_bloc.group('nb'))
        nb_bloc = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('0', nb_bloc.group('nb'))
        nb_bloc = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('0', nb_bloc.group('nb'))
        nb_bloc = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('0', nb_bloc.group('nb'))
        reponse = self.client.get(f'/instructions/tableaudebord/{self.prefecture.pk}/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'La grosse course')
        self.assertContains(reponse, 'table_afaire', count=nb)
        # f = open('/var/log/manifsport/test_output.html', 'w')
        # f.write(reponse.content.decode('utf-8'))
        # f.close()

    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
    def test_Hors_delai(self):
        """ Test de l'autorisation de dépôt hors délai """

        print('**** test Dnm hors délai ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue tableau de bord nouvelle apps
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dnm')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Déclaration initiale')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nom'] = 'La grosse course'
        data['nb_signaleurs'] = 3
        data['nb_signaleurs_fixes'] = 1
        data['nb_signaleurs_autos'] = 1
        data['nb_signaleurs_motos'] = 1
        data['nb_vehicules_accompagnement'] = 0
        data['date_debut'] = datetime.now() + timedelta(days=29)
        data['date_fin'] = datetime.now() + timedelta(days=29, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print('\t' + url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+\\n.+Dossier à envoyer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie manquante', count=1)
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'Itinéraire horaire', count=1)

        fichier = open(os.path.join('evenements/tests/data-carto.json'))
        data = fichier.read()
        jsonData = json.loads(data)
        jsonData['manif'] = action.group('url').split('/')[2]

        self.client.post('/carto/parcours/', jsonData, content_type="application/json", follow=True, HTTP_HOST='127.0.0.1:8000')
        url = "/".join(action.group('url').split('/')[:3]) + "/"
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie manquante')

        reponse = self.joindre_fichiers(reponse)
        self.assertContains(reponse, 'Pas d\'actions requises')
        self.assertContains(reponse, 'Octroyer un accès en lecture')
        data = {
            'acces': 'simple',
            'model': 'Prefecture',
            'service_id': self.prefecture.pk,
        }
        manif = Manif.objects.last()
        reponse = self.client.post('/instructions/acces/' + str(manif.pk) + '/create/', data,
                                   HTTP_HOST='127.0.0.1:8000', HTTP_REFERER='/evenement/' + str(manif.pk) + '/')
        self.assertContains(reponse, 'True')
        reponse = self.client.get('/Dnm/' + str(manif.pk) + '/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'La grosse course')
        self.assertContains(reponse, 'Le dossier est accessible en lecture')
        self.assertContains(reponse, 'Préfecture de Montbrison')
        self.client.logout()

        print('**** test Autorisation instructeur ****')
        # Connexion avec l'instructeur
        self.assertTrue(self.client.login(username="instructeur", password='123'))
        Message.objects.all().delete()
        # Appel de la vue dashboard
        reponse = self.client.get('/instructions/tableaudebord/', HTTP_HOST='127.0.0.1:8000', follow=True)
        self.assertContains(reponse, 'Tableau de bord', count=3)
        self.assertContains(reponse, 'Lecture seule')
        nb_lecture = re.search('Lecture seule <span\\n.+">(?P<nb>(\d))</span', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_lecture, 'group'))
        self.assertEqual('1', nb_lecture.group('nb'))
        # appel ajax pour avoir la liste
        url_lect_list = "/consultation/" + str(self.prefecture.pk) + "/"
        reponse = self.client.get(url_lect_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'La grosse course')
        self.assertContains(reponse, 'Autoriser le dépôt hors délai')
        data = {
            'motif': 'La raison est simple, je fais ce que je veux',
        }
        reponse = self.client.post('/evenement/' + str(manif.pk) + '/autoriser/', data,
                                   follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'La grosse course')
        self.assertContains(reponse, 'Autorisation de dépôt hors délai')
        self.assertContains(reponse, 'Aucune action possible sur ce dossier')
        # Vérifier les messages envoyés
        messages = Message.objects.all()
        # for mess in messages:
        #     for env in mess.message_enveloppe.all():
        #         print("**" + env.destinataire_txt)
        #     print("---" + str(mess.pk) + mess.corps)
        self.assertEqual(len(messages), 2)
        self.assertEqual(messages[0].corps, '<p>L\'autorisation de dépôt hors délai pour le dossier La Grosse Course, vient d\'être accordée.</p>')
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.organisateur)
        self.assertEqual(messages[1].message_enveloppe.first().destinataire_id, self.instructeur)
        self.client.logout()

        print('**** test Dnm hors délai - envoi ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue tableau de bord nouvelle apps
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('1', nb_bloc.group('nb'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(f'/tableau-de-bord-organisateur/{self.structure_organisatrice.pk}/liste?&filtre_etat=attente',
                                  HTTP_HOST='127.0.0.1:8000')
        detail1 = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail1, 'group'))
        reponse = self.client.get(detail1.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'La grosse course')
        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Envoi du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur(1)

        # print(reponse.content.decode('utf-8'))
