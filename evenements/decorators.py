# coding: utf-8
from django.shortcuts import render, get_object_or_404
from asgiref.sync import sync_to_async
from core.util.permissions import login_requis, login_requis_async
from evenements.models.manifestation import Manif
from structure.models import ServiceConsulte
from structure.models.organisation import Organisation


def verifier_acces_evenement_function(request, acces=False, *args, **kwargs):
    # Creation d'une function separée du décorateur pour pouvoir l'utiliser en async
    # Dans le cas d'une édition d'évaluation n2k, le pk fourni est celui de l'évaluation'
    try:
        o_support = ServiceConsulte.objects.get(type_service_fk__categorie_fk__nom_s="_Support")
        if o_support in request.user.organisation_m2m.all():
            return False
    except ServiceConsulte.DoesNotExist:
        pass
    organisation = Organisation.objects.get(pk=request.session['service_pk'])
    if request.path.split('/')[-4] == 'n2keval' and request.path.split('/')[-2] == 'edit':
        manif = get_object_or_404(Manif, n2kevaluation__pk=kwargs['pk'])
    # Dans le cas d'une édition d'évaluation rnr, le pk fourni est celui de l'évaluation'
    elif request.path.split('/')[-4] == 'rnreval' and request.path.split('/')[-2] == 'edit':
        manif = get_object_or_404(Manif, rnrevaluation__pk=kwargs['pk'])
    else:
        # Cas général ou le pk est celui de la manif
        pk = kwargs.get('pk') or kwargs.get('manif_pk')
        manif = get_object_or_404(Manif, pk=pk)
    if  manif.structure_organisatrice_fk == organisation:  # noqa
        return False
    else:
        # Vérifier autorisation d'accès
        if acces and organisation in [acces.organisation for acces in manif.acces.filter(date_revocation__isnull=True)]:
            return False
        return render(request, "core/access_restricted.html",
                      {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)


def verifier_acces_evenement(function=None, acces=False):
    """ Décorateur : limite l'accès aux organisateurs d'un événement
        ou aux organisations ayant un accés en consultation """

    def decorator(view_func):
        @login_requis()
        def _wrapped_view(request, *args, **kwargs):
            erreur = verifier_acces_evenement_function(request, acces, *args, **kwargs)
            if erreur:
                return erreur
            return view_func(request, *args, **kwargs)

        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def verifier_acces_evenement_async(function=None, acces=False):

    def decorator(view_func):
        @login_requis_async()
        async def _wrapped_view(request, *args, **kwargs):
            erreur = await sync_to_async(verifier_acces_evenement_function)(request, acces, *args, **kwargs)
            if erreur:
                return erreur
            return await view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def verifier_proprietaire_function(request, acces=False, *args, **kwargs):
    # Creation d'une function separée du décorateur pour pouvoir l'utiliser en async
    # Dans le cas d'une édition d'évaluation n2k, le pk fourni est celui de l'évaluation'
    try:
        o_support = ServiceConsulte.objects.get(type_service_fk__categorie_fk__nom_s="_Support")
        if o_support in request.user.organisation_m2m.all():
            return False
    except ServiceConsulte.DoesNotExist:
        pass

    if request.path.split('/')[-4] == 'n2keval' and request.path.split('/')[-2] == 'edit':
        manif = get_object_or_404(Manif, n2kevaluation__pk=kwargs['pk'])
    # Dans le cas d'une édition d'évaluation rnr, le pk fourni est celui de l'évaluation'
    elif request.path.split('/')[-4] == 'rnreval' and request.path.split('/')[-2] == 'edit':
        manif = get_object_or_404(Manif, rnrevaluation__pk=kwargs['pk'])
    else:
        # Cas général ou le pk est celui de la manif
        pk = kwargs.get('pk') or kwargs.get('manif_pk')
        manif = get_object_or_404(Manif, pk=pk)
    # Creation d'une function separée du décorateur pour pouvoir l'utiliser en async
    if  manif.structure_organisatrice_fk in request.user.organisation_m2m.all():  # noqa
        return False
    else:
        return render(request, "core/access_restricted.html",
                      {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)


def verifier_proprietaire(function=None ,acces=False):
    """ Décorateur : limite l'accès aux organisateurs d'un événement """

    def decorator(view_func):
        @login_requis()
        def _wrapped_view(request, *args, **kwargs):
            erreur = verifier_proprietaire_function(request, acces, *args, **kwargs)
            if erreur:
                return erreur
            return view_func(request, *args, **kwargs)

        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def verifier_proprietaire_async(function=None, acces=False):

    def decorator(view_func):
        @login_requis_async()
        async def _wrapped_view(request, *args, **kwargs):
            erreur = await sync_to_async(verifier_proprietaire_function)(request, acces, *args, **kwargs)
            if erreur:
                return erreur
            return await view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator
