from django import template

register = template.Library()


@register.filter(name='get_pj_instructeur')
def get_pj_instructeur(manif, request):
    return manif.get_pj(request)