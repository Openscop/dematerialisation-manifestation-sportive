from django import template

from django.shortcuts import reverse
from django.utils import timezone

register = template.Library()


@register.simple_tag()
def get_history_pk(obj, name, *args, **kwargs):
    return get_history(obj, name, pk=kwargs['pk'])


@register.filter(name='get_history')
def get_history(obj, name, pk=None):
    """
    params: object: manif ou evaluation
    params: name: str du champ désiré
    """
    if pk:
        name = f"{name}_{pk}"
    history = obj.get_history(name)
    if not history:
        return ""
    data_str = ""
    if obj.__class__.__name__ in ["EvaluationRnr"]:
        manif = obj.manif
        url = reverse('evenements:historique_popup', kwargs={"pk": manif.pk})
        url += f"?rnr_name={name}"
    elif obj.__class__.__name__ in ["EvaluationN2K"]:
        manif = obj.manif
        url = reverse('evenements:historique_popup', kwargs={"pk": manif.pk})
        url += f"?n2k_name={name}"
    elif obj.__class__.__name__ == "Manif":
        manif = obj
        url = reverse('evenements:historique_popup', kwargs={"pk": manif.pk})
        url += f"?name={name}"
    elif hasattr(obj, 'manifestation_ptr'):
        url = reverse('evenements:historique_popup', kwargs={"pk": obj.pk})
        url += f"?name={name}"
    else:
        url = "#"
    for data in history:
        if data['data'] in ["True", "False"]:
            donne = "oui" if data['data'] == "True" else "non"
        elif data['type'] == "list":
            donne = "" if data['data'] else "Non renseigné"
            for x in data['data']:
                donne += f"{x}, "
        elif data['type'] == "int":
            try:
                champ = getattr(obj, data['champ'])
                donne = str(champ._meta.model.objects.get(pk=int(data['data'])))
            except (AttributeError, Exception):
                donne = "Non renseigné"
        else:
            donne = data['data'].replace('"', '＂') if data['data'] else "Non renseigné"
        date = timezone.make_aware(timezone.datetime.strptime(data['date'].split('.')[0], "%Y-%m-%d %H:%M:%S"), timezone.utc).astimezone().strftime("%d/%m/%Y à %H:%M")
        if len(donne) < 41:
            data_str += f'Le <em>{date}</em> : {donne}<hr>'
        else:
            data_str += f'Le <em>{date}</em> : <br/>{donne}<hr>'

    data_str = data_str[:-4]
    html = f'<span class="float-end" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-html="true" title="{data_str}"><a href="{url}" class="history_link" target="_blank"><i class="far fa-history"></i></a></span>'
    return html


@register.filter(name='date_history')
def date_history(valeur):
    return timezone.make_aware(timezone.datetime.strptime(valeur.split('.')[0], "%Y-%m-%d %H:%M:%S"), timezone.utc).astimezone().strftime("%d/%m/%Y à %H:%M")
