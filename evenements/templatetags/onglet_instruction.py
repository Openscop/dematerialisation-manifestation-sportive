from django import template

register = template.Library()


@register.filter(name='onglet_instruction')
def onglet_instruction(manif, request):
    if manif.instance_instruites.all().count() > 1 and manif.instruction.all():
        if not manif.get_type_manif() == "avtm":
            return True
        if (request.organisation.is_service_instructeur()  and request.organisation.instance_fk == manif.instance) or request.session['o_support']:
            return True
    return False