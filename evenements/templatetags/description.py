from django import template

from core.models import User
from structure.models import Organisation

register = template.Library()


@register.filter
def iconic(value):
    """ Remplacer la valeur True/False par une icône """
    if value:
        return '<i class="fas fa-check fa-lg text-success"></i>'
    return '<i class="fas fa-times fa-lg text-danger"></i>'


@register.filter
def objet_user(json):
    """ Retouner l'objet correspondant au pk """
    if json:
        pk = json['user_origine']
        return User.objects.get(pk=pk) if User.objects.filter(pk=pk) else None


@register.filter
def objet_service(json):
    """ Retouner l'objet correspondant au pk """
    if json:
        pk = json['service_origine']
        return Organisation.objects.get(pk=pk) if Organisation.objects.filter(pk=pk) else None
