from django import template

from preview_generator.manager import PreviewManager

from configuration.settings import *
import shutil


register = template.Library()


def createthumbnail(value):
    """
        Fonction pour créer des miniature.
        :param value: .ulr du fichier media voulu
        :return: url de l'image generée
        """
    try:
        url_fichier_source = value.path
        if not os.path.exists(url_fichier_source):
            return '/static/portail/img/vignette.png'
        cache_path = value.file.name[:-len(os.path.basename(value.file.name))] + "thumbnail/"
        temp_path = value.file.name[:-len(os.path.basename(value.file.name))] + "tmp/"
        nom_def = f"{cache_path}{os.path.basename(value.file.name).split('.')[0]}.jpg"
        if os.path.exists(nom_def):
            return MEDIA_URL + nom_def[len(MEDIA_ROOT):]
        if not os.path.exists(temp_path):
            os.mkdir(temp_path)
        manager = PreviewManager(cache_path, create_folder=True)
        if not os.path.exists(temp_path + os.path.basename(value.file.name)):
            shutil.copy(url_fichier_source, temp_path + os.path.basename(value.file.name))
        image_reduite = manager.get_jpeg_preview(temp_path + os.path.basename(value.file.name), width=1000, height=500)
        shutil.move(image_reduite, nom_def)
        for file in os.listdir(temp_path):
            file_path = os.path.join(temp_path, file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except:
                pass
        return MEDIA_URL + nom_def[len(MEDIA_ROOT):]
    except Exception as e:
        print(e)
        return '/static/portail/img/vignette.png'


@register.filter(name='thumbnail')
def thumbnail(value):
    """
    Filtre pour calculer le nom de la miniature
    Cette fonctionne ne génére PLUS la miniature.
    C'est celery qui s'occupe de la création
    """
    try:
        url_fichier_source = value.path
        if not os.path.exists(url_fichier_source):
            return '/static/portail/img/vignette.png'
        cache_path = value.file.name[:-len(os.path.basename(value.file.name))] + "thumbnail/"
        nom_def = f"{cache_path}{os.path.basename(value.file.name).split('.')[0]}.jpg"
        if os.path.exists(nom_def):
            return MEDIA_URL + nom_def[len(MEDIA_ROOT):]
        else:
            return '/static/portail/img/vignette.png'
    except:
        return '/static/portail/img/vignette.png'
