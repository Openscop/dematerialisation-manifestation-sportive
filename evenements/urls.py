# coding: utf-8
from django.urls import path

from .views.dnm import DnmCreate, DnmDetail, DnmUpdate, DnmDelete
from .views.dcnm import DcnmCreate, DcnmDetail, DcnmUpdate, DcnmDelete
from .views.dnmc import DnmcCreate, DnmcDetail, DnmcUpdate, DnmcDelete
from .views.dcnmc import DcnmcCreate, DcnmcDetail, DcnmcUpdate, DcnmcDelete
from .views.dvtm import DvtmCreate, DvtmDetail, DvtmUpdate, DvtmDelete
from .views.avtm import AvtmCreate, AvtmDetail, AvtmUpdate, AvtmDelete
from .views.dvtmcir import DvtmcirCreate, DvtmcirDetail, DvtmcirUpdate, DvtmcirDelete
from .views.dashboard import TableauOrganisateurBody, TableauOrganisateurListe, ConsultManifListe
from .views.manif import ManifInstructeurUpdate, ManifPublicDetail, ManifUrlGen, ManifHistoriquePopup
from .views.ajax import ManifClonageAjax, ManifDepotAjax, ManifInstructeurDemandeModifAjax, GetParcoursElementView
from .views.sansinstance import SansInstanceManifCreate
from .views.create import CreationEvenementView
from .views.exportation import ExportPourInstructeur, ExportPourOrganisateur, ExportPourAgent
from .views.piecejointe import OrganisateurPieceJointeView


app_name = 'evenements'
urlpatterns = [

    # Tableau de bord principal
    path('tableau-de-bord-organisateur/', TableauOrganisateurBody.as_view(), name="tableau-de-bord-organisateur"),
    path('tableau-de-bord-organisateur/<int:pk>/', TableauOrganisateurBody.as_view(), name="tableau-de-bord-organisateur"),
    path('tableau-de-bord-organisateur/<int:pk>/liste', TableauOrganisateurListe.as_view(), name="tableau_organisateur_liste"),

    path('evenement/creation/', CreationEvenementView.as_view(), name='creation_evenement'),

    # Manifestation sans type (non supporté)
    path('sansinstance/add/', SansInstanceManifCreate.as_view(), name="manif_sansinstance_add"),

    # Modifications instructeur
    path('evenement/<int:pk>/instruction/', ManifInstructeurUpdate.as_view(), name="manifestation_instructeur"),
    path('evenement/<int:pk>/modification/', ManifInstructeurDemandeModifAjax.as_view(), name="manifestation_modification"),
    path('evenement/<int:pk>/cloner/', ManifClonageAjax.as_view(), name="manif_clone"),
    path('evenement/<int:pk>/autoriser/', ManifDepotAjax.as_view(), name="manif_depot"),
    path('evenement/<int:pk>/commune_traversee/', GetParcoursElementView.as_view(), name="commune_traversee"),

    path('evenement/<int:pk>/piecejointe/', OrganisateurPieceJointeView.as_view(), name='pj_organisateur'),

    path('evenement/<int:pk>/historique/', ManifHistoriquePopup.as_view(), name='historique_popup'),

    path('evenement/<int:pk>/export/instructeur/', ExportPourInstructeur.as_view(), name="export_manif_instructeur"),
    path('evenement/<int:pk>/export/organisateur/', ExportPourOrganisateur.as_view(), name="export_manif_organisateur"),
    path('evenement/<int:pk>/export/agent/', ExportPourAgent.as_view(), name="export_manif_agent"),

    path('evenement/<int:pk>/', ManifPublicDetail.as_view(), name="manif_detail"),
    path('manifestation/<int:pk>/', ManifUrlGen.as_view(), name="manif_url"),
    path('consultation/<int:pk>/', ConsultManifListe.as_view(), name="consultation_manif"),

    # Dnm urls
    path('Dnm/add/', DnmCreate.as_view(), name="dnm_add"),
    path('Dnm/<int:pk>/edit/', DnmUpdate.as_view(), name="dnm_update"),
    path('Dnm/<int:pk>/delete/', DnmDelete.as_view(), name="dnm_delete"),
    path('Dnm/<int:pk>/', DnmDetail.as_view(), name="dnm_detail"),

    # Dnmc urls
    path('Dnmc/add/', DnmcCreate.as_view(), name="dnmc_add"),
    path('Dnmc/<int:pk>/edit/', DnmcUpdate.as_view(), name="dnmc_update"),
    path('Dnmc/<int:pk>/delete/', DnmcDelete.as_view(), name="dnmc_delete"),
    path('Dnmc/<int:pk>/', DnmcDetail.as_view(), name="dnmc_detail"),

    # Dcnm urls
    path('Dcnm/add/', DcnmCreate.as_view(), name="dcnm_add"),
    path('Dcnm/<int:pk>/edit/', DcnmUpdate.as_view(), name="dcnm_update"),
    path('Dcnm/<int:pk>/delete/', DcnmDelete.as_view(), name="dcnm_delete"),
    path('Dcnm/<int:pk>/', DcnmDetail.as_view(), name="dcnm_detail"),

    # Dcnmc urls
    path('Dcnmc/add/', DcnmcCreate.as_view(), name="dcnmc_add"),
    path('Dcnmc/<int:pk>/edit/', DcnmcUpdate.as_view(), name="dcnmc_update"),
    path('Dcnmc/<int:pk>/delete/', DcnmcDelete.as_view(), name="dcnmc_delete"),
    path('Dcnmc/<int:pk>/', DcnmcDetail.as_view(), name="dcnmc_detail"),

    # Dvtm urls
    path('Dvtm/add/', DvtmCreate.as_view(), name="dvtm_add"),
    path('Dvtm/<int:pk>/edit/', DvtmUpdate.as_view(), name="dvtm_update"),
    path('Dvtm/<int:pk>/delete/', DvtmDelete.as_view(), name="dvtm_delete"),
    path('Dvtm/<int:pk>/', DvtmDetail.as_view(), name="dvtm_detail"),

    # Avtm urls
    path('Avtm/add/', AvtmCreate.as_view(), name="avtm_add"),
    path('Avtm/<int:pk>/edit/', AvtmUpdate.as_view(), name="avtm_update"),
    path('Avtm/<int:pk>/delete/', AvtmDelete.as_view(), name="avtm_delete"),
    path('Avtm/<int:pk>/', AvtmDetail.as_view(), name="avtm_detail"),

    # Dvtmcir urls
    path('Dvtmcir/add/', DvtmcirCreate.as_view(), name="dvtmcir_add"),
    path('Dvtmcir/<int:pk>/edit/', DvtmcirUpdate.as_view(), name="dvtmcir_update"),
    path('Dvtmcir/<int:pk>/delete/', DvtmcirDelete.as_view(), name="dvtmcir_delete"),
    path('Dvtmcir/<int:pk>/', DvtmcirDetail.as_view(), name="dvtmcir_detail"),

]
