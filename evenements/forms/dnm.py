# coding: utf-8
from crispy_forms.layout import Layout, Fieldset, HTML
from django.template.loader import render_to_string
from django import forms

from ..models.dnm import Dnm, Dnmc, Dcnm, Dcnmc
from .manif import ManifForm, FIELDS_MARKUP, FORM_WIDGETS, FIELDS_CONTACT


class DnmForm(ManifForm):
    """ Formulaire """
    FORM_WIDGETS['detail_police_municipale'] = forms.Textarea(attrs={'rows': 1})
    FORM_WIDGETS['detail_police_nationale'] = forms.Textarea(attrs={'rows': 1})
    FORM_WIDGETS['respect_code_route_detail'] = forms.Textarea(attrs={'rows': 1, 'placeholder': 'nom de la voie et créneau horaire de passage'})
    FORM_WIDGETS['priorite_passage_detail'] = forms.Textarea(attrs={'rows': 1, 'placeholder': 'nom de la voie et créneau horaire de passage'})
    FORM_WIDGETS['usage_temporaire_detail'] = forms.Textarea(attrs={'rows': 1, 'placeholder': 'nom de la voie et créneau horaire de passage'})
    FORM_WIDGETS['usage_privatif_detail'] = forms.Textarea(attrs={'rows': 1, 'placeholder': 'nom de la voie et créneau horaire de passage'})
    # Définir les classes supplémentaires ici plutôt que dans helper.layout qui écrase les classes "requis" et "modifier"
    FORM_WIDGETS['respect_code_route'] = forms.CheckboxInput(attrs={'class': 'traffic'})
    FORM_WIDGETS['priorite_passage'] = forms.CheckboxInput(attrs={'class': 'traffic'})
    FORM_WIDGETS['usage_temporaire'] = forms.CheckboxInput(attrs={'class': 'traffic'})
    FORM_WIDGETS['usage_privatif'] = forms.CheckboxInput(attrs={'class': 'traffic'})

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML("<p>Vous pouvez enregistrer ce formulaire à tout moment puis le reprendre ultérieurement.</p><hr>"),
                                    Fieldset(*(self.FIELDS_INIT)),
                                    Fieldset(*(self.FIELDS_MANIF + ['depart_groupe', 'circul_groupe'])),
                                    Fieldset(*self.FIELDS_GEOGRAPHIE),
                                    Fieldset("Coordonnateur Sécurité", 'securite_nom', 'securite_prenom', 'securite_tel', 'securite_email'),
                                    Fieldset("Régime demandé en matière de circulation publique",
                                             'respect_code_route', 'respect_code_route_detail',
                                             'priorite_passage', 'priorite_passage_detail',
                                             'usage_temporaire', 'usage_temporaire_detail',
                                             'usage_privatif', 'usage_privatif_detail'),
                                    Fieldset("Informations sur le dispositif de sécurité de la manifestation",
                                             Fieldset("Véhicules d'accompagnement :", 'nb_vehicules_accompagnement', 'vehicule_ouverture', 'vehicule_debut', 'vehicule_fin', 'vehicule_organisation', css_class='special_fieldset'),
                                             Fieldset("Signaleurs :", 'nb_signaleurs', 'nb_signaleurs_fixes', 'nb_signaleurs_autos', 'nb_signaleurs_motos', css_class='special_fieldset'),
                                             Fieldset("Forces de l'ordre :", 'police_municipale', 'detail_police_municipale', 'police_nationale', 'detail_police_nationale', css_class='special_fieldset')
                                             ),
                                    Fieldset(*FIELDS_MARKUP),
                                    Fieldset(*(self.FIELDS_N2K[:-1])),
                                    Fieldset(*FIELDS_CONTACT))
        if self.instance and self.instance.get_liste_champs_a_modifier():
            self.ajouter_css_class_modifier_aux_champs_demandes()
        else:
            self.ajouter_css_class_requis_aux_champs_etape_0()

    # Meta
    class Meta:
        model = Dnm
        exclude = ['descriptions_parcours', 'parcours_openrunner', 'structure_organisatrice_fk', 'delivrance_titre',
                   'demande_homologation', 'instance', 'dans_calendrier'] + ManifForm.Meta.exclude
        widgets = FORM_WIDGETS


class DnmcForm(DnmForm):
    """ Formulaire """

    # Meta
    class Meta:
        model = Dnmc
        exclude = DnmForm.Meta.exclude
        widgets = FORM_WIDGETS


class DcnmForm(ManifForm):
    """ Formulaire """
    FORM_WIDGETS['detail_police_municipale'] = forms.Textarea(attrs={'rows': 1})
    FORM_WIDGETS['detail_police_nationale'] = forms.Textarea(attrs={'rows': 1})
    FORM_WIDGETS['respect_code_route_detail'] = forms.Textarea(attrs={'rows': 1, 'placeholder': 'nom de la voie et créneau horaire de passage'})
    FORM_WIDGETS['priorite_passage_detail'] = forms.Textarea(attrs={'rows': 1, 'placeholder': 'nom de la voie et créneau horaire de passage'})
    FORM_WIDGETS['usage_temporaire_detail'] = forms.Textarea(attrs={'rows': 1, 'placeholder': 'nom de la voie et créneau horaire de passage'})
    FORM_WIDGETS['usage_privatif_detail'] = forms.Textarea(attrs={'rows': 1, 'placeholder': 'nom de la voie et créneau horaire de passage'})
    # Définir les classes supplémentaires ici plutôt que dans helper.layout qui écrase les classes "requis" et "modifier"
    FORM_WIDGETS['respect_code_route'] = forms.CheckboxInput(attrs={'class': 'traffic'})
    FORM_WIDGETS['priorite_passage'] = forms.CheckboxInput(attrs={'class': 'traffic'})
    FORM_WIDGETS['usage_temporaire'] = forms.CheckboxInput(attrs={'class': 'traffic'})
    FORM_WIDGETS['usage_privatif'] = forms.CheckboxInput(attrs={'class': 'traffic'})

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.FIELDS_MANIF.insert(9, 'avec_convention_fede')
        self.helper.layout = Layout(HTML("<p>Vous pouvez enregistrer ce formulaire à tout moment puis le reprendre ultérieurement.</p><hr>"),
                                    Fieldset(*(self.FIELDS_INIT)),
                                    Fieldset(*(self.FIELDS_MANIF + ['depart_groupe', 'circul_groupe'])),
                                    Fieldset(*self.FIELDS_GEOGRAPHIE),
                                    Fieldset("Coordonnateur Sécurité", 'securite_nom', 'securite_prenom', 'securite_tel', 'securite_email'),
                                    Fieldset("Régime demandé en matière de circulation publique",
                                             'respect_code_route', 'respect_code_route_detail',
                                             'priorite_passage', 'priorite_passage_detail',
                                             'usage_temporaire', 'usage_temporaire_detail',
                                             'usage_privatif', 'usage_privatif_detail'),
                                    Fieldset("Informations sur le dispositif de sécurité de la manifestation",
                                             Fieldset("Véhicules d'accompagnement :", 'nb_vehicules_accompagnement', 'vehicule_ouverture', 'vehicule_debut', 'vehicule_fin', 'vehicule_organisation', css_class='special_fieldset'),
                                             Fieldset("Signaleurs :", 'nb_signaleurs', 'nb_signaleurs_fixes', 'nb_signaleurs_autos', 'nb_signaleurs_motos', css_class='special_fieldset'),
                                             Fieldset("Forces de l'ordre :", 'police_municipale', 'detail_police_municipale', 'police_nationale', 'detail_police_nationale', css_class='special_fieldset')
                                             ),
                                    Fieldset(*FIELDS_MARKUP),
                                    Fieldset(*(self.FIELDS_N2K[:-1])),
                                    Fieldset(*FIELDS_CONTACT))
        if self.instance and self.instance.get_liste_champs_a_modifier():
            self.ajouter_css_class_modifier_aux_champs_demandes()
        else:
            self.ajouter_css_class_requis_aux_champs_etape_0()

    # Meta
    class Meta:
        model = Dcnm
        exclude = ['descriptions_parcours', 'parcours_openrunner', 'structure_organisatrice_fk', 'demande_homologation',
                   'instance', 'avec_convention_fede_confirme'] + ManifForm.Meta.exclude
        widgets = FORM_WIDGETS


class DcnmcForm(DcnmForm):
    """ Formulaire """

    # Meta
    class Meta:
        model = Dcnmc
        exclude = DcnmForm.Meta.exclude
        widgets = FORM_WIDGETS
