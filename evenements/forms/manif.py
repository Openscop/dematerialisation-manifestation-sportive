# coding: utf-8
import logging
from datetime import timedelta

from bootstrap_datepicker_plus import DateTimePickerInput
from django import forms
from django.core.exceptions import ValidationError
from django.urls import reverse_lazy
from django.forms import ModelChoiceField
from django.forms.models import ModelMultipleChoiceField
from django.utils import timezone
from crispy_forms.layout import Submit, Layout, Fieldset, ButtonHolder, HTML
from crispy_forms.helper import FormHelper

from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from clever_selects.form_fields import ChainedModelMultipleChoiceField
from clever_selects.forms import ChainedChoicesModelForm
from core.forms.base import GenericForm
from ..models.manifestation import Manif
from evaluation_incidence.models import N2kSite, PdesiLieu, RnrZone


logger = logging.getLogger('django.request')

# Contenus de Fieldsets standards
# Modifiés à la volée dans les helpers des formulaires hérités
FIELDS_INITIALS = ["Déclaration initiale (non modifiable)", 'activite', 'competition', 'circuit', 'emprise', 'nb_participants']
FIELDS_MANIFESTATION = ["Détail de la manifestation", 'nom', 'date_debut', 'date_fin', 'description', 'observation', 'public',
                        'afficher_adresse_structure', 'dans_calendrier', 'nb_organisateurs', 'nb_spectateurs']
FIELDS_GEOGRAPHIE = ["Géographie et parcours", HTML("""<div class='alert alert-info'>Cette partie pourra être remplie 
                     automatiquement en renseignant vos parcours dans l'outil cartographique.<br>
                     <ul>
                     <li><span class='px-3 mx-1' style='background-color: #caedaf'></span>Élément présent dans le cerfa et dans les parcours.</li>
                     <li><span class='px-3 mx-1' style='background-color: #afc7ed'></span>Élément présent dans les parcours.</li>
                     <li><span class='px-3 mx-1' style='background-color: #edafaf'></span>Élément présent dans le cerfa mais pas dans les parcours.</li>
                     </ul></div>"""),
                     'departement_depart', 'departements_traverses', 'ville_depart', 'ville_depart_interdep',
                     'villes_traversees',]
FIELDS_MARKUP = ["Balisage", 'convention_balisage', 'balisage']
FIELDS_NATURA2000 = ["Zones protégées", 'gros_budget', 'lucratif', 'delivrance_titre', 'vtm_hors_circulation',
                     'sites_natura2000', 'signature_charte_dispense_site_n2k', 'lieux_pdesi', 'zones_rnr',
                     'demande_homologation']
FIELDS_CONTACT = ["Personne à contacter sur place", 'nom_contact', 'prenom_contact', 'tel_contact']

# Dates par défaut de début et fin de manifestation
DEFAULT_START = timezone.now() + timedelta(days=150)
# Inversion jour et mois pour datepicker4
DEFAULT_START_STR = DEFAULT_START.strftime('%m/%d/%Y 8:00')
DEFAULT_END_STR = DEFAULT_START.strftime('%m/%d/%Y 20:00')

# Widgets
FORM_WIDGETS = {
    'date_debut': DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'defaultDate': DEFAULT_START_STR, 'locale': 'fr'}),
    'date_fin': DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'defaultDate': DEFAULT_END_STR, 'locale': 'fr'}),
    'description': forms.Textarea(attrs={'rows': 1}),
    'observation': forms.Textarea(attrs={'rows': 1}),
    'balisage': forms.Textarea(attrs={'rows': 1}),
    'tel_contact': forms.TextInput(attrs={'pattern': '^0[0-9]{9}'},)
}


class ManifForm(GenericForm, ChainedChoicesModelForm):
    """ Création de manifestation """

    # Champs
    departement_depart = ModelChoiceField(required=False, queryset=Departement.objects.configured(),
                                          label="Département de départ", disabled=True)
    departements_traverses = ModelMultipleChoiceField(required=False, queryset=Departement.objects.all(),
                                                      label="Autres départements traversés",
                                                      help_text="attention : si votre manifestation se déroule sur "
                                                                "plusieurs départements, alors le délai d'instruction "
                                                                "est de 3 mois !")
    villes_traversees = ChainedModelMultipleChoiceField('departements_traverses',
                                                        reverse_lazy('administrative_division:commune_widget'),
                                                        Commune, label="Autres communes traversées", required=False,
                                                        help_text="Ne pas sélectionner la commune de départ.")
    sites_natura2000 = ChainedModelMultipleChoiceField('departements_traverses',
                                                       reverse_lazy('evaluation_incidence:sitesn2k_widget'),
                                                       N2kSite, required=False, help_text=Manif.AIDE_N2K,
                                                       label="Sites Natura2000")
    lieux_pdesi = ChainedModelMultipleChoiceField('departements_traverses',
                                                  reverse_lazy('evaluation_incidence:lieuxpdesi_widget'),
                                                  PdesiLieu, required=False, help_text=Manif.AIDE_PDESI,
                                                  label="Lieux PDESI")
    zones_rnr = ChainedModelMultipleChoiceField('departements_traverses',
                                                reverse_lazy('evaluation_incidence:zonesrnr_widget'),
                                                RnrZone, required=False, help_text=Manif.AIDE_RNR, label="Zones RNR")
    # Créer un champ à substituer au champ "privé"
    public = forms.BooleanField(initial=True, label="Publier la manifestation dans le calendrier", required=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        self.routes = kwargs.pop('routes', None)
        self.extradata = kwargs.pop('extradata', None)

        # Prendre la valeur du champ "privé" pour positionner le champ de remplacement
        if 'instance' in kwargs:
            if hasattr(kwargs['instance'], 'prive') and kwargs['instance'].prive:
                if 'initial' in kwargs:
                    kwargs['initial']['public'] = False

        super().__init__(*args, **kwargs)
        self.FIELDS_MANIF = FIELDS_MANIFESTATION[:]
        self.FIELDS_N2K = FIELDS_NATURA2000[:]
        self.FIELDS_GEOGRAPHIE = FIELDS_GEOGRAPHIE[:]
        self.FIELDS_INIT = FIELDS_INITIALS[:]
        if 'circuit' in self.initial and not self.initial['circuit']:
            self.FIELDS_INIT.remove('circuit')
        if 'delivrance_titre' not in self.fields:
            self.FIELDS_N2K.remove('delivrance_titre')
        if 'dans_calendrier' not in self.fields:
            self.FIELDS_MANIF.remove('dans_calendrier')
        if hasattr(self, 'helper'):
            self.helper.form_tag = False
        self.fields['departements_traverses'].widget.attrs = {'data-placeholder': "1. Sélectionnez les départements traversés"}
        self.fields['villes_traversees'].widget.attrs = {'data-placeholder': "2. Sélectionnez les autres communes traversées"}
        self.fields['sites_natura2000'].widget.attrs = {'data-placeholder': "Faites votre sélection"}
        self.fields['zones_rnr'].widget.attrs = {'data-placeholder': "Faites votre sélection"}
        self.fields['lieux_pdesi'].widget.attrs = {'data-placeholder': "Faites votre sélection"}
        self.fields['activite'].widget.attrs = {'data-placeholder': "Choisissez une discipline"}
        if 'initial' in kwargs and 'departement_depart' in kwargs['initial']:
            dept = Departement.objects.get(pk=kwargs['initial']['departement_depart'])
            instance = dept.get_instance()
            if not instance.is_master() and hasattr(instance, 'natura2kdepartementconfig'):
                if not instance.natura2kdepartementconfig.pdesi:
                    self.FIELDS_N2K.remove('lieux_pdesi')
                    del self.fields['lieux_pdesi']

    # Validation
    def clean_date_debut(self):
        """ Valider la date de départ """
        begin_date = self.cleaned_data['date_debut']
        if not begin_date:
            raise ValidationError("La date de départ de la manifestation ne peut pas être laissée vide")
        if not hasattr(self, 'instance') or not self.instance or not self.instance.id:
            if begin_date < timezone.now():
                raise ValidationError("La date de départ de la manifestation ne peut pas être dans le passé")
        return begin_date

    def clean_nom(self):
        """ Valider le nom de la manif """
        nom = self.cleaned_data['nom']
        if not nom:
            raise ValidationError("Le nom de la manifestation ne peut pas être laissée vide")
        return nom

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('nom', None):
            cleaned_data['nom'] = cleaned_data['nom'].replace('"', '＂')
        begin_date = cleaned_data.get('date_debut')
        end_date = cleaned_data.get('date_fin')
        if begin_date and end_date and begin_date >= end_date:
            message = "La date de fin de la manifestation doit être supérieure à la date de début."
            self.add_error("date_fin", self.error_class([message]))
        if begin_date and end_date and end_date >= begin_date + timedelta(days=365):
            message = "La manifestation ne peut excéder 365 jours."
            self.add_error("date_fin", self.error_class([message]))

        # Vérifier que "departure_city" n'est pas dans la liste de "crossed_cities"
        depart = cleaned_data.get('ville_depart')
        liste = list(cleaned_data.get('villes_traversees'))
        if liste:
            cleaned_data['villes_traversees'] = [x for x in liste if x != depart]

        # Convertir la valeur du champ "public" au champ "prive"
        public = cleaned_data.get('public')
        if public:
            cleaned_data['prive'] = False
        else:
            cleaned_data['prive'] = True

        # Vérifier les datas modifiées
        if self.instance.get_instruction():
            # Interdiction de supprimer un département traversé, l'instruction est déjà lancée
            for dep in self.instance.departements_traverses.all():
                if dep not in cleaned_data['departements_traverses']:
                    raise forms.ValidationError("Vous ne pouvez plus supprimer un département traversé !")
            # Seul les champs à modifier sont autorisés
            if self.instance.modif_json:
                liste_champs_a_modifier = []
                for formulaire, champ, date in self.instance.get_liste_champs_a_modifier():
                    if formulaire == 'manif':
                        liste_champs_a_modifier.append(champ)
                if not all(change in liste_champs_a_modifier for change in self.changed_data if change != "ville_depart_interdep"):
                    raise forms.ValidationError("Des champs modifiés ne sont pas autorisés")

        return cleaned_data

    def is_valid(self):
        # supprimer la valeur initiale injectée par le get_form_kwargs pour le champ "departements_traverses"
        # sinon le champ se retrouve dans Changed_data et s'il est disabled, c'est la valeur initiale qui est transmise
        # alors qu'elle n'est pas dans la liste et une erreur est retournée
        try:
            self.initial['departements_traverses'].remove(self.instance.ville_depart.get_departement())
        except (ValueError, KeyError):
            pass
        return super().is_valid()

    def ajouter_css_class_requis_aux_champs_etape_0(self):
        """
        Ajoute une classe css nommée 'requis' à tous les champs obligatoire à l'étape 0 de l'instruction,
        c'est à dire pour être envoyé aux instructeurs
        """
        for cle in self.fields:
            field = self.fields[cle]
            if cle in self.instance.get_liste_champs_etape_0():
                css = field.widget.attrs.get('class', '')
                field.widget.attrs['class'] = 'requis ' + css
            if cle in ['nb_participants', 'activite', 'ville_depart', 'competition', 'circuit', 'emprise']:
                field.disabled = True

    class Meta:
        model = Manif
        exclude = ["instance_instruites", "modif_json", "history_json", "dans_calendrier_confirme", "declarant",]


class ManifInstructeurUpdateForm(GenericForm):
    """ Formulaire destiné aux instructeurs """

    class Meta:
        model = Manif
        fields = ['afficher_adresse_structure', 'cache']

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if hasattr(self, 'helper'):
            self.helper.form_tag = False


class ManifFilesForm(GenericForm):
    """ Formulaire d'upload des fichiers d'une manifestation """

    class Meta:
        model = Manif
        widgets = FORM_WIDGETS
        exclude = []


class ManifDepotForm(forms.ModelForm):
    """
    Formulaire pour le dépôt de manifestation hors délai
    """
    motif = forms.CharField(label="Motif de l'autorisation", widget=forms.Textarea(attrs={'rows': 1}))

    def __init__(self, *args, **kwargs):
        """ Initialiser le formulaire """
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_id = 'horsdepot-form'
        self.helper.label_class = 'col-sm-12'
        self.helper.field_class = 'col-sm-12'
        self.helper.layout = Layout(
            Fieldset(" ", 'motif'),
            ButtonHolder(Submit('save', "Autoriser le dépôt hors délai", css_class='btn-success btn-block horsdepot_submit'))
        )

    class Meta:
        model = Manif
        fields = ('motif',)


class ManifCloneForm(GenericForm):
    """
    Formulaire de clonage de manifestation
    """
    def __init__(self, *args, **kwargs):
        """ Initialiser le formulaire """
        super().__init__(*args, **kwargs)
        self.initial = {}
        self.helper.form_tag = False
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-9'
        self.helper.layout = Layout(
            Fieldset(" ", 'nom', 'date_debut', 'date_fin'),
            ButtonHolder(Submit('save', "Créer puis afficher", css_class='btn-success btn-block clone_submit'))
        )

    def clean_nom(self):
        """ Valider le nom de la manifestation """
        if self.cleaned_data.get('nom', None):
            self.cleaned_data['nom'] = self.cleaned_data['nom'].replace('"', '＂')
        nom = self.cleaned_data['nom']
        if not nom:
            raise ValidationError("Le nom de la manifestation ne peut pas être laissée vide")
        if nom:
            if Manif.objects.filter(nom=nom).exists():
                raise ValidationError("Le nom est dejà associé à une autre manifestion")
        return nom

    def clean_date_debut(self):
        """ Valider la date de départ """
        begin_date = self.cleaned_data['date_debut']
        if not begin_date:
            raise ValidationError("La date de départ de la manifestation ne peut pas être laissée vide")
        if begin_date < timezone.now():
            raise ValidationError("La date de départ de la manifestation ne peut pas être dans le passé")
        return begin_date

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('nom', None):
            cleaned_data['nom'] = cleaned_data['nom'].replace('"', '＂')
        begin_date = cleaned_data.get('date_debut')
        end_date = cleaned_data.get('date_fin')
        if begin_date and end_date and begin_date >= end_date:
            message = "La date de fin de la manifestation doit être supérieure à la date de début."
            self.add_error("date_fin", self.error_class([message]))
        if begin_date and end_date and end_date >= begin_date + timedelta(days=90):
            message = "La manifestation ne peut excéder 90 jours."
            self.add_error("date_fin", self.error_class([message]))

    class Meta:
        model = Manif
        fields = ('nom', 'date_debut', 'date_fin')
        labels = {'nom': 'Nom', 'date_debut': 'Début', 'date_fin': 'Fin'}
        widgets = {
            'date_debut': DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'defaultDate': DEFAULT_START_STR, 'locale': 'fr'}),
            'date_fin': DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'defaultDate': DEFAULT_END_STR, 'locale': 'fr'})}
