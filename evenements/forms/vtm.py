# coding: utf-8
from crispy_forms.layout import Layout, Fieldset, HTML
from django.template.loader import render_to_string

from ..models import Avtm, Dvtmcir, Dvtm
from .manif import (ManifForm, FIELDS_MARKUP, FORM_WIDGETS, FIELDS_CONTACT)


class DvtmForm(ManifForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML("<p>Vous pouvez enregistrer ce formulaire à tout moment puis le reprendre ultérieurement.</p><hr>"),
                                    Fieldset(*(self.FIELDS_INIT)),
                                    Fieldset(*self.FIELDS_GEOGRAPHIE),
                                    Fieldset("Organisateur Technique", 'technique_nom', 'technique_prenom', 'technique_tel',
                                             'technique_email', css_class='orga_tech_fieldset'),
                                    Fieldset(*(self.FIELDS_MANIF + ['nb_personnes_pts_rassemblement', 'vehicules', 'nb_vehicules_accompagnement'])),
                                    Fieldset(*FIELDS_MARKUP),
                                    Fieldset(*(self.FIELDS_N2K[:-1])),
                                    Fieldset(*FIELDS_CONTACT))
        if self.instance and self.instance.get_liste_champs_a_modifier():
            self.ajouter_css_class_modifier_aux_champs_demandes()
        else:
            self.ajouter_css_class_requis_aux_champs_etape_0()

    # Meta
    class Meta:
        model = Dvtm
        exclude = ['descriptions_parcours', 'parcours_openrunner', 'structure_organisatrice_fk', 'demande_homologation', 'instance',
                   'dans_calendrier'] + ManifForm.Meta.exclude
        widgets = FORM_WIDGETS


class AvtmForm(ManifForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML("<p>Vous pouvez enregistrer ce formulaire à tout moment puis le reprendre ultérieurement.</p><hr>"),
                                    Fieldset(*(self.FIELDS_INIT)),
                                    Fieldset(*self.FIELDS_GEOGRAPHIE),
                                    Fieldset("Organisateur Technique", 'technique_nom', 'technique_prenom', 'technique_tel',
                                             'technique_email', css_class='orga_tech_fieldset'),
                                    Fieldset(*(self.FIELDS_MANIF + ['vehicules', 'nb_vehicules_accompagnement'])),
                                    Fieldset(*FIELDS_MARKUP),
                                    Fieldset(*(self.FIELDS_N2K[:-1])),
                                    Fieldset(*FIELDS_CONTACT))
        if self.instance and self.instance.get_liste_champs_a_modifier():
            self.ajouter_css_class_modifier_aux_champs_demandes()
        else:
            self.ajouter_css_class_requis_aux_champs_etape_0()

    # Meta
    class Meta:
        model = Avtm
        exclude = ['descriptions_parcours', 'parcours_openrunner', 'structure_organisatrice_fk', 'demande_homologation', 'instance'] + ManifForm.Meta.exclude
        widgets = FORM_WIDGETS


class DvtmcirForm(ManifForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML("<p>Vous pouvez enregistrer ce formulaire à tout moment puis le reprendre ultérieurement.</p><hr>"),
                                    Fieldset(*(self.FIELDS_INIT)),
                                    Fieldset(*self.FIELDS_GEOGRAPHIE),
                                    Fieldset("Organisateur Technique", 'technique_nom', 'technique_prenom', 'technique_tel',
                                             'technique_email', css_class='orga_tech_fieldset'),
                                    Fieldset(*(self.FIELDS_MANIF[:9] + ['avec_convention_fede'] + self.FIELDS_MANIF[11:12]) + self.FIELDS_MANIF[14:]),
                                    Fieldset(*FIELDS_CONTACT))
        if self.instance and self.instance.get_liste_champs_a_modifier():
            self.ajouter_css_class_modifier_aux_champs_demandes()
        else:
            self.ajouter_css_class_requis_aux_champs_etape_0()

    # Meta
    class Meta:
        model = Dvtmcir
        exclude = ['descriptions_parcours', 'parcours_openrunner', 'departements_traverses', 'villes_traversees',
                   'vehicules', 'structure_organisatrice_fk', 'demande_homologation', 'instance', 'avec_convention_fede_confirme',
                   'ville_depart_interdep'] + ManifForm.Meta.exclude
        widgets = FORM_WIDGETS
