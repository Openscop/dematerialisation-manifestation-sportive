# coding: utf-8

from django.forms.models import ModelMultipleChoiceField, ModelChoiceField

from administrative_division.models.departement import Departement
from evenements.forms.manif import ManifForm, FORM_WIDGETS
from evenements.models.sansinstance import SansInstanceManif


class SansInstanceManifForm(ManifForm):
    """ Formulaire de base pour les manifestations non prises en charge """

    # Champs
    departements_traverses = ModelMultipleChoiceField(required=False, queryset=Departement.objects.all(), label="Autres départements traversés")
    departement_depart = ModelChoiceField(required=False, queryset=Departement.objects.unconfigured(), label="Département de départ", disabled=True)

    # Meta
    class Meta:
        model = SansInstanceManif
        widgets = FORM_WIDGETS
        fields = ['id', 'nom', 'date_debut', 'date_fin', 'description', 'activite', 'departement_depart', 'ville_depart',
                  'departements_traverses', 'villes_traversees', 'nb_participants', 'nb_spectateurs']
