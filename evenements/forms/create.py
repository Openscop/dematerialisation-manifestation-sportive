from django import forms
from django.forms import ModelChoiceField
from clever_selects.forms import ChainedChoicesForm
from clever_selects.form_fields import ChainedModelChoiceField
from django.urls import reverse_lazy
from ajax_select.fields import AutoCompleteField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, HTML, ButtonHolder
from crispy_forms.bootstrap import StrictButton

from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from sports.models import Discipline
from sports.models.sport import Activite
from evenements.models import Manif


# Textes d'aide
HELP_EXTRA_ACTIVITE = "recherchez votre activité sportive dans ce champ.Cliquez ensuite sur l'activité pour remplir automatiquement les champs ci-dessous."
BLOC_INFORMATION = "<div class='col-10 mx-auto mt-3'>" \
                   "<div class='card'>" \
                   "<div class='card-header bg-secondary text-white text-center'>Information</div>" \
                   "<div class='card-body'>" \
                   "<p id='info-cerfa' class='card-text text-justify'>Veuillez remplir le formulaire ci-dessus afin de rechercher le formulaire adapté à votre manifestation...</p>"


class CreationEvenementForm(ChainedChoicesForm):

    extra_activite = AutoCompleteField('activite', label="Recherche d'activité sportive", required=False, help_text=HELP_EXTRA_ACTIVITE, show_help_text=False)
    discipline = ModelChoiceField(required=False, queryset=Discipline.objects.all())
    activite = ChainedModelChoiceField('discipline', reverse_lazy('sports:activite_widget'), Activite, label="Activité", required=False)
    departement = ModelChoiceField(required=False, queryset=Departement.objects.filter(instance__etat_s__in=['ouvert', 'test']), label="Département de départ")
    ville_depart = ChainedModelChoiceField('departement', reverse_lazy('administrative_division:commune_widget'), Commune, label="Commune de départ", required=False)
    circuit = forms.BooleanField(label="Sur circuit, terrains ou parcours", required=False, initial=False)
    emprise = forms.ChoiceField(label="Emprise de la manifestation", choices=Manif.CHOIX_EMPRISE, required=False)
    competition = forms.BooleanField(label="Classement, chronométrage, horaire fixé à l'avance", required=False, initial=True)
    type_circuit = forms.ChoiceField(label="Site de pratique", choices=Manif.CHOIX_CIRCUIT, required=False)
    nb_participants = forms.IntegerField(label='Nombre de participants', required=False, help_text="En cas d'incertitude, prévoyez \"large\" car vous ne pourrez plus modifier cette information et elle influe directement sur le formulaire nécessaire.")
    nb_departement = forms.IntegerField(label="Nombre de départements traversés", required=False)
    formulaire = forms.CharField(max_length=20, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        """ Initialiser le formulaire """
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'
        self.helper.layout = Layout(
            Fieldset("", 'extra_activite', 'discipline', 'activite', 'departement', 'ville_depart',
                     'competition', 'circuit', "type_circuit", 'emprise', 'nb_participants', 'nb_departement', 'formulaire'),
            StrictButton('Rechercher le formulaire', css_class="btn-primary w-100", id="searchform"),
            HTML(BLOC_INFORMATION),
            ButtonHolder(Submit('submit', 'Accéder au formulaire', css_class='btn-success w-100')),
            HTML('</div></div></div>'),

        )

    def clean_nb_participants(self):
        data = self.cleaned_data['nb_participants']
        if data is None:
            raise forms.ValidationError("Ce champ doit être renseigné !")
        if data == 0:
            raise forms.ValidationError("Ce champ ne doit pas être égal à 0 !")
        return data

    def clean_ville_depart(self):
        data = self.cleaned_data['ville_depart']
        if data is None:
            raise forms.ValidationError("Ce champ doit être renseigné !")
        return data

    def clean_departement(self):
        data = self.cleaned_data['departement']
        if data is None:
            raise forms.ValidationError("Ce champ doit être renseigné !")
        return data

    def clean_activite(self):
        data = self.cleaned_data['activite']
        if data is None:
            raise forms.ValidationError("Ce champ doit être renseigné !")
        return data

    def clean_emprise(self):
        data = self.cleaned_data['emprise']
        if not data:
            raise forms.ValidationError("Vous devez faire un choix !")
        return data

    def clean_type_circuit(self):
        cleaned_data = super().clean()
        data = cleaned_data['type_circuit']
        if cleaned_data['competition']:
            if not cleaned_data['type_circuit']:
                raise forms.ValidationError("Vous devez faire un choix !")
        return data
