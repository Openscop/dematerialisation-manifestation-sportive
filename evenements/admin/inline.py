# coding: utf-8
from django.contrib import admin
from django import forms

from evenements.models import PieceJointe


class PieceJointeInlineForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        """ Initialiser le formulaire """
        super().__init__(*args, **kwargs)
        if 'info' in self.initial and self.initial['info'] == "":
            self.fields['info'].widget = forms.HiddenInput()


class PiecejointeInline(admin.StackedInline):

    def __init__(self, *args, **kwargs):
        super(PiecejointeInline, self).__init__(*args, **kwargs)
        # Laisser la possibilité a quiconque d'effacer une pj "cerfa" est trop dangereux pour la verification de la complétude d'un dossier
        # L'instructeur peut toujours effacer le fichier inclu dedans mais pas l'objet
        self.can_delete = False

    model = PieceJointe
    extra = 1
    # readonly_fields = ('date_limite', 'scan_antivirus', 'champ_cerfa')
    form = PieceJointeInlineForm
    fieldsets = (
        (None, {'fields': (('nom', 'champ_cerfa'), 'info', ('date_demande', 'date_limite', 'date_televersement'),
                           ('document_attache', 'validation_instructeur', 'scan_antivirus'))}),
    )
