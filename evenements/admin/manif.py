# coding: utf-8
from ajax_select.helpers import make_ajax_form
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin
from django.urls import reverse
from django.utils.html import format_html

from instructions.admin.instruction import InstructionInline
from core.util.admin import RelationOnlyFieldListFilter
from core.models import LogConsult
from evaluation_incidence.admin import N2kEvalInline, RNREvalInline
from evenements.admin.filter import EncoursFilter, CalendrierFilter, AVenirFilter, TypeManifFilter, ChampCerfaFilter
from evenements.admin.inline import PiecejointeInline
from evenements.models.dnm import Dnm, Dnmc, Dcnm, Dcnmc
from evenements.models.vtm import Dvtm, Avtm, Dvtmcir
from evenements.models.manifestation import Manif
from evenements.models.sansinstance import SansInstanceManif
from evenements.models.piecejointe import PieceJointe

FIELDSET_GEN = {'fields': ('instance_instruites', 'nom', ('date_creation', 'date_debut', 'date_fin'), ('description', 'observation'),
                           ('structure_organisatrice_fk', 'activite'), 'declarant', ('nb_organisateurs', 'nb_participants', 'nb_spectateurs'))}
FIELDSET_GEO = {'classes': ('collapse',),
                'fields': (('ville_depart', 'ville_depart_interdep_mtm', 'villes_traversees', 'departements_traverses'))}
FIELDSET_CAL = {'classes': ('collapse',),
                'fields': (('dans_calendrier', 'dans_calendrier_confirme', 'prive', 'cache', 'afficher_adresse_structure'))}
FIELDSET_CAL_FEDE = {'classes': ('collapse',),
                'fields': (('dans_calendrier', 'dans_calendrier_confirme', 'avec_convention_fede', 'avec_convention_fede_confirme',
                            'prive', 'cache', 'afficher_adresse_structure'))}
FIELDSET_BAL = {'classes': ('collapse',),
                'fields': ('convention_balisage', 'balisage')}
FIELDSET_CRE = {'classes': ('collapse',),
                'fields': ('emprise', 'competition', 'circuit')}
FIELDSET_NAT = {'classes': ('collapse',),
                'fields': ('gros_budget', 'delivrance_titre', 'lucratif', 'vtm_hors_circulation',
                           'signature_charte_dispense_site_n2k', 'sites_natura2000', 'lieux_pdesi',
                           'demande_homologation', 'zones_rnr')}
FIELDSET_CIR = {'classes': ('collapse',),
                'fields': (('depart_groupe', 'circul_groupe'), 'nb_vehicules_accompagnement',
                           ('vehicule_ouverture', 'vehicule_debut', 'vehicule_fin', 'vehicule_organisation'),
                           ('nb_signaleurs', 'nb_signaleurs_fixes', 'nb_signaleurs_autos', 'nb_signaleurs_motos'),
                           'police_municipale', 'detail_police_municipale', 'police_nationale', 'detail_police_nationale',
                           'respect_code_route', 'respect_code_route_detail', 'priorite_passage', 'priorite_passage_detail',
                           'usage_temporaire', 'usage_temporaire_detail', 'usage_privatif', 'usage_privatif_detail')}
FIELDSET_SEC = {'classes': ('collapse',),
                'fields': (('securite_nom', 'securite_prenom'), ('securite_tel', 'securite_email'))}
FIELDSET_TEC = {'classes': ('collapse',),
                'fields': (('technique_nom', 'technique_prenom'), ('technique_tel', 'technique_email'))}
FIELDSET_CON = {'classes': ('collapse',),
                'fields': (('prenom_contact', 'nom_contact', 'tel_contact'),)}


class AdminMixin(object):
    """ Mixin qui regroupe les méthodes partagées """

    list_display = ('__str__', 'date_debut', 'en_cours_instruction', 'soumise_natura2000', 'soumise_rnr',
                    'ville_depart__arrondissement__departement')
    list_filter = ['instruction__etat', ('ville_depart__arrondissement__departement', RelationOnlyFieldListFilter)]
    readonly_fields = ('date_creation', 'instance_instruites', 'structure_organisatrice_fk', 'declarant')
    search_fields = ['nom__unaccent', 'structure_organisatrice_fk__precision_nom_s__unaccent', 'activite__name']
    inlines = [PiecejointeInline, InstructionInline,
               RNREvalInline, N2kEvalInline]
    list_per_page = 25
    ordering = ['-date_debut']

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.has_group('_support'):
            queryset = queryset.filter(ville_depart__arrondissement__departement=request.user.get_departement())
        return queryset

    def save_formset(self, request, form, formset, change):
        # Surcharge pour enregistrer le user en référent pour tous changements
        if formset.model._meta.object_name == 'Instruction':
            for form in formset.forms:
                if not form.instance.referent:
                    form.instance.referent = request.user
                    # Si la partie instruction n'est pas modifiée, super n'enregistrera pas 'instruction'
                    form.instance.save()
        return super().save_formset(request, form, formset, change)


@admin.register(Manif)
class ManifAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des manifestations """

    # Configuration
    list_display = ('link_to_specialized', 'en_cours_instruction', 'date_creation', 'get_type_manif', 'nom',
                    'structure_organisatrice_fk', 'activite__name', 'date_debut', 'ville_depart__arrondissement__departement', 'pk')
    list_filter = [TypeManifFilter, EncoursFilter, CalendrierFilter, AVenirFilter,
                   ('ville_depart__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['nom__unaccent', 'structure_organisatrice_fk__precision_nom_s__unaccent', 'activite__name', 'pk']
    date_hierarchy = 'date_debut'
    list_per_page = 25
    ordering = ['-date_creation']
    # Pas de configuration de détail, voir les manifestations spécialisées

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.has_group('_support'):
            queryset = queryset.filter(ville_depart__arrondissement__departement=request.user.get_departement())
        return queryset

    def changelist_view(self, request, extra_context=None):
        """surcharge pour ajouter un log"""
        response = super().changelist_view(request, extra_context)
        LogConsult.objects.create_from_list(response, request)
        return response

    def link_to_specialized(self, obj):
        if obj.get_type_manif():
            link = reverse("admin:evenements_" + obj.get_type_manif() + "_change", args=[obj.get_cerfa().id])
            return format_html('<a href="{}">Consulter</a>', link)
        return '-'
    link_to_specialized.short_description = 'Dossier'


@admin.register(SansInstanceManif)
class SansInstanceManifAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des manifestations sans instance """

    # Configuration
    list_display = ('nom', 'structure_organisatrice_fk', 'activite__name', 'date_debut', 'ville_depart__arrondissement__departement')
    list_filter = [('ville_depart__arrondissement__departement', RelationOnlyFieldListFilter)]
    readonly_fields = ['date_creation']
    search_fields = ['nom__unaccent', 'structure_organisatrice_fk__precision_nom_s__unaccent', 'activite__name']
    list_per_page = 25
    ordering = ['-date_creation']

    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', FIELDSET_GEO),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(SansInstanceManif, {'ville_depart': 'commune', 'villes_traversees': 'commune',
                                              'departements_traverses': 'departement', 'structure_organisatrice_fk': 'structureorganisatrice'})


@admin.register(Dnm)
class DnmAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des manifestations non motorisées soumises à déclaration """

    # Configuration
    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', FIELDSET_GEO),
        ('Calendrier', FIELDSET_CAL),
        ('Balisage', FIELDSET_BAL),
        ('Natura2000', FIELDSET_NAT),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Coordonateur Sécurité', FIELDSET_SEC),
        ('Circulation', FIELDSET_CIR),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Dnm, {'ville_depart': 'commune', 'villes_traversees': 'commune', 'lieux_pdesi': 'lieupdesi',
                                'departements_traverses': 'departement', 'sites_natura2000': 'siten2k',
                                'zones_rnr': 'zonernr', 'structure_organisatrice_fk': 'structureorganisatrice'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(Dnmc)
class DnmcAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des manifestations cyclistes non motorisées soumises à déclaration """

    # Configuration
    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', FIELDSET_GEO),
        ('Calendrier', FIELDSET_CAL),
        ('Balisage', FIELDSET_BAL),
        ('Natura2000', FIELDSET_NAT),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Coordonateur Sécurité', FIELDSET_SEC),
        ('Circulation', FIELDSET_CIR),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Dnmc, {'ville_depart': 'commune', 'ville_depart_interdep_mtm': 'commune', 'villes_traversees': 'commune', 'lieux_pdesi': 'lieupdesi',
                                 'departements_traverses': 'departement', 'sites_natura2000': 'siten2k',
                                 'zones_rnr': 'zonernr', 'structure_organisatrice_fk': 'structureorganisatrice'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(Dcnm)
class DcnmAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des manifestations compétitives non motorisées soumises à déclaration """

    # Configuration
    fieldsets = (
        (None, {'fields': ('instance_instruites', 'nom', ('date_creation', 'date_debut', 'date_fin'),
                           ('description', 'observation'), ('structure_organisatrice_fk', 'activite'), 'declarant',
                           ('nb_participants', 'nb_spectateurs'))}),
        ('Géographie', FIELDSET_GEO),
        ('Calendrier', FIELDSET_CAL_FEDE),
        ('Balisage', FIELDSET_BAL),
        ('Natura2000', FIELDSET_NAT),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Coordonateur Sécurité', FIELDSET_SEC),
        ('Circulation', FIELDSET_CIR),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Dcnm, {'instance_instruites': 'instance', 'ville_depart': 'commune', 'ville_depart_interdep_mtm': 'commune', 'villes_traversees': 'commune', 'lieux_pdesi': 'lieupdesi',
                                 'departements_traverses': 'departement', 'sites_natura2000': 'siten2k',
                                 'zones_rnr': 'zonernr', 'structure_organisatrice_fk': 'structureorganisatrice'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(Dcnmc)
class DcnmcAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des manifestations cyclistes compétitives non motorisées soumises à déclaration """

    # Configuration
    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', FIELDSET_GEO),
        ('Calendrier', FIELDSET_CAL_FEDE),
        ('Balisage', FIELDSET_BAL),
        ('Natura2000', FIELDSET_NAT),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Coordonateur Sécurité', FIELDSET_SEC),
        ('Circulation', FIELDSET_CIR),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Dcnmc, {'ville_depart': 'commune', 'ville_depart_interdep_mtm': 'commune', 'villes_traversees': 'commune', 'lieux_pdesi': 'lieupdesi',
                                  'departements_traverses': 'departement', 'sites_natura2000': 'siten2k',
                                  'zones_rnr': 'zonernr', 'structure_organisatrice_fk': 'structureorganisatrice'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(Dvtm)
class DvtmAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des concentrations motorisées """

    # Configuration
    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', FIELDSET_GEO),
        ('Calendrier', FIELDSET_CAL),
        ('Balisage', FIELDSET_BAL),
        ('Natura2000', FIELDSET_NAT),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Organisateur Technique', FIELDSET_TEC),
        ('Circulation', {'classes': ('collapse',), 'fields': ('vehicules', 'nb_vehicules_accompagnement',
                                                              'nb_personnes_pts_rassemblement')}),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Dvtm, {'ville_depart': 'commune', 'ville_depart_interdep_mtm': 'commune', 'villes_traversees': 'commune', 'lieux_pdesi': 'lieupdesi',
                                 'departements_traverses': 'departement', 'sites_natura2000': 'siten2k',
                                 'zones_rnr': 'zonernr', 'structure_organisatrice_fk': 'structureorganisatrice'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(Avtm)
class AvtmAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des manifestations motorisées """

    # Configuration
    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', FIELDSET_GEO),
        ('Calendrier', FIELDSET_CAL),
        ('Balisage', FIELDSET_BAL),
        ('Natura2000', FIELDSET_NAT),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Organisateur Technique', FIELDSET_TEC),
        ('Circulation', {'classes': ('collapse',), 'fields': ('vehicules', 'nb_vehicules_accompagnement')}),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Avtm, {'instance_instruites': 'instance', 'ville_depart': 'commune', 'ville_depart_interdep_mtm': 'commune', 'villes_traversees': 'commune', 'lieux_pdesi': 'lieupdesi',
                                 'departements_traverses': 'departement', 'sites_natura2000': 'siten2k',
                                 'zones_rnr': 'zonernr', 'structure_organisatrice_fk': 'structureorganisatrice'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(Dvtmcir)
class DvtmcirAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des courses motorisées """

    # Configuration
    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', {'classes': ('collapse',), 'fields': ('ville_depart',)}),
        ('Calendrier', FIELDSET_CAL_FEDE),
        ('Balisage', FIELDSET_BAL),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Organisateur Technique', FIELDSET_TEC),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Dvtmcir, {'instance_instruites': 'instance', 'ville_depart': 'commune', 'structure_organisatrice_fk': 'structureorganisatrice'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(PieceJointe)
class PieceJointeAdmin(admin.ModelAdmin):
    """ Administration des pièces jointes """

    # Configuration
    list_display = ['nom', 'document_attache', 'date_televersement', 'scan', 'validation']
    list_filter = ['scan_antivirus', 'validation_instructeur', ChampCerfaFilter]
    search_fields = ['nom__unaccent', 'info__unaccent', 'manif__pk']

    def scan(self, obj):
        return obj.scan_antivirus
    scan.short_description = "scan"
    scan.boolean = True

    def validation(self, obj):
        return obj.validation_instructeur
    validation.short_description = "validée"
    validation.boolean = True
