from django.apps import AppConfig


class EvenementsConfig(AppConfig):
    name = 'evenements'
    verbose_name = 'Manifestations sportives'

    def ready(self):
        pass

default_app_config = 'evenements.EvenementsConfig'
