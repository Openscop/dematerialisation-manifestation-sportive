# Generated by Django 4.0.6 on 2022-07-12 16:15

import configuration.directory
import core.FileTypevalidator
from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import re


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('administrative_division', '0001_initial'),
        ('sports', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0001_initial'),
        ('evaluation_incidence', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FichierExportation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.CharField(default='', max_length=500)),
                ('date', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Manif',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(blank=True, max_length=255, null=True, unique=True, verbose_name='nom de la manifestation')),
                ('date_creation', models.DateField(auto_now_add=True, verbose_name='date de création')),
                ('date_debut', models.DateTimeField(blank=True, null=True, verbose_name='date de début')),
                ('date_fin', models.DateTimeField(blank=True, null=True, verbose_name='date de fin')),
                ('description', models.TextField(blank=True, help_text="précisez les modalités d'organisation et les caractéristiques de la manifestation", null=True, verbose_name='description')),
                ('observation', models.TextField(blank=True, default='', help_text="précisez les observations sur la manifestation, destinées aux services d'instruction", verbose_name='observation')),
                ('dans_calendrier', models.BooleanField(default=False, verbose_name='Vous êtes affiliés à une fédération délégataire de la discipline et la manifestation est inscrite sur son calendrier')),
                ('dans_calendrier_confirme', models.BooleanField(default=None, null=True, verbose_name="Confirmation d'inscription dans le calendrier")),
                ('ville_depart_interdep', models.JSONField(blank=True, max_length=2000, null=True, verbose_name="Villes de départ en cas d'interdepartementalité (champs texte technique)")),
                ('parcours_openrunner', models.CharField(blank=True, max_length=255, validators=[django.core.validators.RegexValidator(re.compile('^\\d+(?:,\\d+)*\\Z'), code='invalid', message='Enter only digits separated by commas.')], verbose_name='cartographie de parcours')),
                ('descriptions_parcours', models.TextField(blank=True, default='', help_text='précisez pour chaque parcours : nb km, nb participants, heure de départ, départ groupé ou échelonné...', verbose_name='description des parcours')),
                ('nb_organisateurs', models.PositiveIntegerField(blank=True, help_text="renseigner le nombre d'organisateurs présents pour la manifestation", null=True, verbose_name="nombre d'organisateurs")),
                ('nb_participants', models.PositiveIntegerField(blank=True, help_text='renseigner le nombre de participants de la précedente édition ou le nombre de participants estimé si première édition', null=True, verbose_name='nombre de participants')),
                ('nb_spectateurs', models.PositiveIntegerField(blank=True, null=True, verbose_name='nombre max. de spectateurs')),
                ('prenom_contact', models.CharField(blank=True, max_length=200, null=True, verbose_name='prénom')),
                ('nom_contact', models.CharField(blank=True, max_length=200, null=True, verbose_name='nom de famille')),
                ('tel_contact', models.CharField(blank=True, max_length=14, null=True, verbose_name='numéro de téléphone')),
                ('delai_cours', models.CharField(blank=True, max_length=30, null=True, verbose_name='Delai en cours')),
                ('prive', models.BooleanField(default=False, verbose_name='Ne pas publier la manifestation dans le calendrier')),
                ('cache', models.BooleanField(default=False, verbose_name='Ne pas publier la manifestation dans le calendrier')),
                ('afficher_adresse_structure', models.BooleanField(default=True, verbose_name='Afficher les coordonnées de la structure dans le calendrier')),
                ('convention_balisage', models.BooleanField(default=False, verbose_name="Vous êtes signataire d'une charte de balisage temporaire ou départementale ou régionale")),
                ('balisage', models.TextField(blank=True, null=True, verbose_name='Type et mode de balisage utilisé')),
                ('emprise', models.SmallIntegerField(blank=True, choices=[(0, '--------'), (1, 'Hors voies publiques ou hors voies ouvertes à la circulation'), (2, 'Partiellement sur voies publiques ou ouvertes à la circulation'), (3, 'Totalement sur voies publiques ou ouvertes à la circulation'), (4, 'Voies non ouvertes ou temporairement fermées à la circulation publique')], default=0, verbose_name='Emprise sur voie publique')),
                ('competition', models.BooleanField(default=False, verbose_name='Compétition')),
                ('circuit_non_permanent', models.BooleanField(default=False, verbose_name='Circuit non permanent')),
                ('circuit_homologue', models.BooleanField(default=False, verbose_name='Circuit homologué dans la discipline')),
                ('gros_budget', models.BooleanField(default=False, verbose_name='le budget de la manifestation excède 100 000 €')),
                ('delivrance_titre', models.BooleanField(default=False, verbose_name="manifestation donnant lieu à la délivrance d'un titre national ou international")),
                ('lucratif', models.BooleanField(default=False, verbose_name='manifestation présentant un caractère lucratif et regroupant sur un même site plus de 1500 personnes')),
                ('vtm_hors_circulation', models.BooleanField(default=False, verbose_name='manifestation avec engagement de véhicules terrestres à moteur se déroulant en dehors des voies ouvertes à la circulation')),
                ('signature_charte_dispense_site_n2k', models.BooleanField(default=False, verbose_name='une charte de dispense en cours de validité pour le site Natura 2000 a été signée')),
                ('demande_homologation', models.BooleanField(default=False, verbose_name="manifestation avec demande d'homologation d'un circuit de sports motorisés")),
                ('modif_json', models.JSONField(blank=True, max_length=2000, null=True, verbose_name='modifications demandées (champs texte technique)')),
                ('history_json', models.JSONField(blank=True, max_length=100000, null=True, verbose_name='Historique des modifications (champs texte technique)')),
                ('activite', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sports.activite', verbose_name='activité')),
                ('declarant', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='declarant', to=settings.AUTH_USER_MODEL, verbose_name='organisateur déclarant')),
                ('departements_traverses', models.ManyToManyField(blank=True, to='administrative_division.departement', verbose_name='autres départements traversés par la manifestation')),
                ('instance', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.instance', verbose_name="Configuration d'instance")),
                ('instance_instruites', models.ManyToManyField(blank=True, related_name='manif_instruite', to='core.instance', verbose_name='Instance instruites par la plateforme')),
                ('lieux_pdesi', models.ManyToManyField(blank=True, help_text='sélectionner les lieux inscrits au plan PDESI traversés par la manifestation', to='evaluation_incidence.pdesilieu', verbose_name='lieux inscrits au PDESI')),
                ('sites_natura2000', models.ManyToManyField(blank=True, help_text='sélectionnez les sites Natura 2000 traversés par la manifestation', to='evaluation_incidence.n2ksite', verbose_name='sites Natura 2000')),
            ],
            options={
                'verbose_name': 'manifestation',
                'verbose_name_plural': 'manifestations',
                'default_related_name': 'manifs',
            },
        ),
        migrations.CreateModel(
            name='PieceJointe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=100, verbose_name='nom de la pièce jointe')),
                ('champ_cerfa', models.CharField(blank=True, max_length=100, null=True, verbose_name='Champ du cerfa')),
                ('info', models.TextField(blank=True, verbose_name='Aide')),
                ('validation_instructeur', models.BooleanField(default=False, verbose_name="Validé par l'instructeur")),
                ('date_demande', models.DateTimeField(blank=True, null=True, verbose_name='Date de demande')),
                ('date_limite', models.DateTimeField(blank=True, null=True, verbose_name="Date limite d'envoi")),
                ('date_televersement', models.DateTimeField(blank=True, null=True, verbose_name='Date de dépot de la pièce jointe')),
                ('scan_antivirus', models.BooleanField(default=True, verbose_name='Scan antivirus effectué')),
                ('ancienne_pj', models.BooleanField(default=False, verbose_name='Pièce jointe créé via le transfert')),
                ('document_attache', models.FileField(blank=True, max_length=512, null=True, upload_to=configuration.directory.UploadPath(''), validators=[core.FileTypevalidator.file_type_validator], verbose_name='Fichier')),
                ('manif', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='piece_jointe', to='evenements.manif')),
            ],
        ),
        migrations.CreateModel(
            name='Avtm',
            fields=[
                ('vehicules', models.PositiveIntegerField(blank=True, null=True, verbose_name='nombre de véhicules')),
                ('technique_nom', models.CharField(blank=True, default='', max_length=200, verbose_name='nom de famille')),
                ('technique_prenom', models.CharField(blank=True, default='', max_length=200, verbose_name='prénom')),
                ('technique_tel', models.CharField(blank=True, default='', max_length=14, verbose_name='numéro de téléphone')),
                ('technique_email', models.EmailField(blank=True, default='', max_length=200, verbose_name='adresse email')),
                ('manifestation_ptr', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='avtm', serialize=False, to='evenements.manif')),
                ('nb_vehicules_accompagnement', models.PositiveIntegerField(blank=True, help_text="nombre de véhicules d'accompagnement", null=True, verbose_name="nombre de véhicules d'accompagnement")),
            ],
            options={
                'verbose_name': 'manifestation avec véhicules terrestres à moteur soumise à autorisation',
                'verbose_name_plural': 'manifestations avec véhicules terrestres à moteur soumises à autorisation',
                'default_related_name': 'avtm',
            },
            bases=('evenements.manif', models.Model),
        ),
        migrations.CreateModel(
            name='Dcnm',
            fields=[
                ('depart_groupe', models.BooleanField(default=False, verbose_name='départ groupé des participants')),
                ('circul_groupe', models.BooleanField(default=False, verbose_name='circulation groupée des participants')),
                ('nb_vehicules_accompagnement', models.PositiveIntegerField(blank=True, help_text="nombre de véhicules d'accompagnement", null=True, verbose_name="nombre de véhicules d'accompagnement")),
                ('securite_nom', models.CharField(blank=True, default='', max_length=200, verbose_name='nom de famille')),
                ('securite_prenom', models.CharField(blank=True, default='', max_length=200, verbose_name='prénom')),
                ('securite_tel', models.CharField(blank=True, default='', max_length=14, verbose_name='numéro de téléphone')),
                ('securite_email', models.EmailField(blank=True, default='', max_length=200, verbose_name='adresse email')),
                ('vehicule_ouverture', models.BooleanField(default=False, verbose_name='présence d\'un véhicule d\'ouverture (véhicule "pilote")')),
                ('vehicule_debut', models.BooleanField(default=False, verbose_name='présence d\'un véhicule de début de course (véhicule "tête de course")')),
                ('vehicule_fin', models.BooleanField(default=False, verbose_name="présence d'un véhicule de fin de course")),
                ('vehicule_organisation', models.BooleanField(default=False, verbose_name="présence d'autres véhicules d'organisation (auto ou moto)")),
                ('nb_signaleurs', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='nombre de signaleurs')),
                ('nb_signaleurs_fixes', models.PositiveSmallIntegerField(default=0, verbose_name='en postes fixes')),
                ('nb_signaleurs_autos', models.PositiveSmallIntegerField(default=0, verbose_name='mobiles en voitures')),
                ('nb_signaleurs_motos', models.PositiveSmallIntegerField(default=0, verbose_name='mobiles en motos')),
                ('police_municipale', models.BooleanField(default=False, verbose_name="Disposerez-vous d'un encadrement de la police municipale ?")),
                ('detail_police_municipale', models.TextField(blank=True, default='', verbose_name='Précisez les moyens affectés')),
                ('police_nationale', models.BooleanField(default=False, verbose_name='Avez-vous passez une convention avec la police nationale ou la gendarmerie ?')),
                ('detail_police_nationale', models.TextField(blank=True, default='', verbose_name='Précisez les moyens affectés')),
                ('respect_code_route', models.BooleanField(default=True, verbose_name='respect code de la route')),
                ('priorite_passage', models.BooleanField(default=False, verbose_name='priorité de passage')),
                ('usage_temporaire', models.BooleanField(default=False, verbose_name='usage exclusif temporaire de la chaussée')),
                ('usage_privatif', models.BooleanField(default=False, verbose_name='usage privatif de la chaussée')),
                ('respect_code_route_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('priorite_passage_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('usage_temporaire_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('usage_privatif_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('manifestation_ptr', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='dcnm', serialize=False, to='evenements.manif')),
                ('avec_convention_fede', models.BooleanField(default=False, verbose_name='Vous avez une convention avec une fédération agréée')),
                ('avec_convention_fede_confirme', models.BooleanField(default=None, null=True, verbose_name='Confirmation de convention avec une fédération agréée')),
            ],
            options={
                'verbose_name': 'manifestation compétitive sans véhicules terrestres à moteur soumise à déclaration',
                'verbose_name_plural': 'manifestations compétitives sans véhicules terrestres à moteur soumises à déclaration',
                'default_related_name': 'dcnm',
            },
            bases=('evenements.manif', models.Model),
        ),
        migrations.CreateModel(
            name='Dcnmc',
            fields=[
                ('depart_groupe', models.BooleanField(default=False, verbose_name='départ groupé des participants')),
                ('circul_groupe', models.BooleanField(default=False, verbose_name='circulation groupée des participants')),
                ('nb_vehicules_accompagnement', models.PositiveIntegerField(blank=True, help_text="nombre de véhicules d'accompagnement", null=True, verbose_name="nombre de véhicules d'accompagnement")),
                ('securite_nom', models.CharField(blank=True, default='', max_length=200, verbose_name='nom de famille')),
                ('securite_prenom', models.CharField(blank=True, default='', max_length=200, verbose_name='prénom')),
                ('securite_tel', models.CharField(blank=True, default='', max_length=14, verbose_name='numéro de téléphone')),
                ('securite_email', models.EmailField(blank=True, default='', max_length=200, verbose_name='adresse email')),
                ('vehicule_ouverture', models.BooleanField(default=False, verbose_name='présence d\'un véhicule d\'ouverture (véhicule "pilote")')),
                ('vehicule_debut', models.BooleanField(default=False, verbose_name='présence d\'un véhicule de début de course (véhicule "tête de course")')),
                ('vehicule_fin', models.BooleanField(default=False, verbose_name="présence d'un véhicule de fin de course")),
                ('vehicule_organisation', models.BooleanField(default=False, verbose_name="présence d'autres véhicules d'organisation (auto ou moto)")),
                ('nb_signaleurs', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='nombre de signaleurs')),
                ('nb_signaleurs_fixes', models.PositiveSmallIntegerField(default=0, verbose_name='en postes fixes')),
                ('nb_signaleurs_autos', models.PositiveSmallIntegerField(default=0, verbose_name='mobiles en voitures')),
                ('nb_signaleurs_motos', models.PositiveSmallIntegerField(default=0, verbose_name='mobiles en motos')),
                ('police_municipale', models.BooleanField(default=False, verbose_name="Disposerez-vous d'un encadrement de la police municipale ?")),
                ('detail_police_municipale', models.TextField(blank=True, default='', verbose_name='Précisez les moyens affectés')),
                ('police_nationale', models.BooleanField(default=False, verbose_name='Avez-vous passez une convention avec la police nationale ou la gendarmerie ?')),
                ('detail_police_nationale', models.TextField(blank=True, default='', verbose_name='Précisez les moyens affectés')),
                ('priorite_passage', models.BooleanField(default=False, verbose_name='priorité de passage')),
                ('usage_privatif', models.BooleanField(default=False, verbose_name='usage privatif de la chaussée')),
                ('respect_code_route_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('priorite_passage_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('usage_temporaire_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('usage_privatif_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('usage_temporaire', models.BooleanField(default=True, verbose_name='usage exclusif temporaire de la chaussée')),
                ('respect_code_route', models.BooleanField(default=False, verbose_name='respect code de la route')),
                ('manifestation_ptr', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='dcnmc', serialize=False, to='evenements.manif')),
                ('avec_convention_fede', models.BooleanField(default=False, verbose_name='Vous avez une convention avec une fédération agréée')),
                ('avec_convention_fede_confirme', models.BooleanField(default=None, null=True, verbose_name='Confirmation de convention avec une fédération agréée')),
            ],
            options={
                'verbose_name': 'manifestation compétitive de cyclisme sans véhicules terrestres à moteur soumise à déclaration',
                'verbose_name_plural': 'manifestations compétitives de cyclisme sans véhicules terrestres à moteur soumises à déclaration',
                'default_related_name': 'dcnmc',
            },
            bases=('evenements.manif', models.Model),
        ),
        migrations.CreateModel(
            name='Dnm',
            fields=[
                ('depart_groupe', models.BooleanField(default=False, verbose_name='départ groupé des participants')),
                ('circul_groupe', models.BooleanField(default=False, verbose_name='circulation groupée des participants')),
                ('nb_vehicules_accompagnement', models.PositiveIntegerField(blank=True, help_text="nombre de véhicules d'accompagnement", null=True, verbose_name="nombre de véhicules d'accompagnement")),
                ('securite_nom', models.CharField(blank=True, default='', max_length=200, verbose_name='nom de famille')),
                ('securite_prenom', models.CharField(blank=True, default='', max_length=200, verbose_name='prénom')),
                ('securite_tel', models.CharField(blank=True, default='', max_length=14, verbose_name='numéro de téléphone')),
                ('securite_email', models.EmailField(blank=True, default='', max_length=200, verbose_name='adresse email')),
                ('vehicule_ouverture', models.BooleanField(default=False, verbose_name='présence d\'un véhicule d\'ouverture (véhicule "pilote")')),
                ('vehicule_debut', models.BooleanField(default=False, verbose_name='présence d\'un véhicule de début de course (véhicule "tête de course")')),
                ('vehicule_fin', models.BooleanField(default=False, verbose_name="présence d'un véhicule de fin de course")),
                ('vehicule_organisation', models.BooleanField(default=False, verbose_name="présence d'autres véhicules d'organisation (auto ou moto)")),
                ('nb_signaleurs', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='nombre de signaleurs')),
                ('nb_signaleurs_fixes', models.PositiveSmallIntegerField(default=0, verbose_name='en postes fixes')),
                ('nb_signaleurs_autos', models.PositiveSmallIntegerField(default=0, verbose_name='mobiles en voitures')),
                ('nb_signaleurs_motos', models.PositiveSmallIntegerField(default=0, verbose_name='mobiles en motos')),
                ('police_municipale', models.BooleanField(default=False, verbose_name="Disposerez-vous d'un encadrement de la police municipale ?")),
                ('detail_police_municipale', models.TextField(blank=True, default='', verbose_name='Précisez les moyens affectés')),
                ('police_nationale', models.BooleanField(default=False, verbose_name='Avez-vous passez une convention avec la police nationale ou la gendarmerie ?')),
                ('detail_police_nationale', models.TextField(blank=True, default='', verbose_name='Précisez les moyens affectés')),
                ('respect_code_route', models.BooleanField(default=True, verbose_name='respect code de la route')),
                ('priorite_passage', models.BooleanField(default=False, verbose_name='priorité de passage')),
                ('usage_temporaire', models.BooleanField(default=False, verbose_name='usage exclusif temporaire de la chaussée')),
                ('usage_privatif', models.BooleanField(default=False, verbose_name='usage privatif de la chaussée')),
                ('respect_code_route_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('priorite_passage_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('usage_temporaire_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('usage_privatif_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('manifestation_ptr', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='dnm', serialize=False, to='evenements.manif')),
            ],
            options={
                'verbose_name': 'manifestation sans véhicules terrestres à moteur soumise à déclaration',
                'verbose_name_plural': 'manifestations sans véhicules terrestres à moteur soumises à déclaration',
                'default_related_name': 'dnm',
            },
            bases=('evenements.manif', models.Model),
        ),
        migrations.CreateModel(
            name='Dnmc',
            fields=[
                ('depart_groupe', models.BooleanField(default=False, verbose_name='départ groupé des participants')),
                ('circul_groupe', models.BooleanField(default=False, verbose_name='circulation groupée des participants')),
                ('nb_vehicules_accompagnement', models.PositiveIntegerField(blank=True, help_text="nombre de véhicules d'accompagnement", null=True, verbose_name="nombre de véhicules d'accompagnement")),
                ('securite_nom', models.CharField(blank=True, default='', max_length=200, verbose_name='nom de famille')),
                ('securite_prenom', models.CharField(blank=True, default='', max_length=200, verbose_name='prénom')),
                ('securite_tel', models.CharField(blank=True, default='', max_length=14, verbose_name='numéro de téléphone')),
                ('securite_email', models.EmailField(blank=True, default='', max_length=200, verbose_name='adresse email')),
                ('vehicule_ouverture', models.BooleanField(default=False, verbose_name='présence d\'un véhicule d\'ouverture (véhicule "pilote")')),
                ('vehicule_debut', models.BooleanField(default=False, verbose_name='présence d\'un véhicule de début de course (véhicule "tête de course")')),
                ('vehicule_fin', models.BooleanField(default=False, verbose_name="présence d'un véhicule de fin de course")),
                ('vehicule_organisation', models.BooleanField(default=False, verbose_name="présence d'autres véhicules d'organisation (auto ou moto)")),
                ('nb_signaleurs', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='nombre de signaleurs')),
                ('nb_signaleurs_fixes', models.PositiveSmallIntegerField(default=0, verbose_name='en postes fixes')),
                ('nb_signaleurs_autos', models.PositiveSmallIntegerField(default=0, verbose_name='mobiles en voitures')),
                ('nb_signaleurs_motos', models.PositiveSmallIntegerField(default=0, verbose_name='mobiles en motos')),
                ('police_municipale', models.BooleanField(default=False, verbose_name="Disposerez-vous d'un encadrement de la police municipale ?")),
                ('detail_police_municipale', models.TextField(blank=True, default='', verbose_name='Précisez les moyens affectés')),
                ('police_nationale', models.BooleanField(default=False, verbose_name='Avez-vous passez une convention avec la police nationale ou la gendarmerie ?')),
                ('detail_police_nationale', models.TextField(blank=True, default='', verbose_name='Précisez les moyens affectés')),
                ('respect_code_route', models.BooleanField(default=True, verbose_name='respect code de la route')),
                ('priorite_passage', models.BooleanField(default=False, verbose_name='priorité de passage')),
                ('usage_temporaire', models.BooleanField(default=False, verbose_name='usage exclusif temporaire de la chaussée')),
                ('usage_privatif', models.BooleanField(default=False, verbose_name='usage privatif de la chaussée')),
                ('respect_code_route_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('priorite_passage_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('usage_temporaire_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('usage_privatif_detail', models.TextField(blank=True, default='', help_text='avec le créneau horaire', verbose_name='Précisez les voies empruntées')),
                ('manifestation_ptr', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='dnmc', serialize=False, to='evenements.manif')),
            ],
            options={
                'verbose_name': 'manifestation de cyclisme sans véhicules terrestres à moteur soumise à déclaration',
                'verbose_name_plural': 'manifestations de cyclisme sans véhicules terrestres à moteur soumises à déclaration',
                'default_related_name': 'dnmc',
            },
            bases=('evenements.manif', models.Model),
        ),
        migrations.CreateModel(
            name='Dvtm',
            fields=[
                ('vehicules', models.PositiveIntegerField(blank=True, null=True, verbose_name='nombre de véhicules')),
                ('technique_nom', models.CharField(blank=True, default='', max_length=200, verbose_name='nom de famille')),
                ('technique_prenom', models.CharField(blank=True, default='', max_length=200, verbose_name='prénom')),
                ('technique_tel', models.CharField(blank=True, default='', max_length=14, verbose_name='numéro de téléphone')),
                ('technique_email', models.EmailField(blank=True, default='', max_length=200, verbose_name='adresse email')),
                ('manifestation_ptr', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='dvtm', serialize=False, to='evenements.manif')),
                ('nb_vehicules_accompagnement', models.PositiveIntegerField(blank=True, help_text="nombre de véhicules d'accompagnement", null=True, verbose_name="nombre de véhicules d'accompagnement")),
                ('nb_personnes_pts_rassemblement', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='nombre de personnes aux points de rassemblement')),
            ],
            options={
                'verbose_name': 'manifestation avec véhicules terrestres à moteur soumise à déclaration',
                'verbose_name_plural': 'manifestations avec véhicules terrestres à moteur soumises à déclaration',
                'default_related_name': 'dvtm',
            },
            bases=('evenements.manif', models.Model),
        ),
        migrations.CreateModel(
            name='Dvtmcir',
            fields=[
                ('vehicules', models.PositiveIntegerField(blank=True, null=True, verbose_name='nombre de véhicules')),
                ('technique_nom', models.CharField(blank=True, default='', max_length=200, verbose_name='nom de famille')),
                ('technique_prenom', models.CharField(blank=True, default='', max_length=200, verbose_name='prénom')),
                ('technique_tel', models.CharField(blank=True, default='', max_length=14, verbose_name='numéro de téléphone')),
                ('technique_email', models.EmailField(blank=True, default='', max_length=200, verbose_name='adresse email')),
                ('manifestation_ptr', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='dvtmcir', serialize=False, to='evenements.manif')),
                ('avec_convention_fede', models.BooleanField(default=False, verbose_name='Vous avez une convention avec une fédération agréée')),
                ('avec_convention_fede_confirme', models.BooleanField(default=None, null=True, verbose_name='Confirmation de convention avec une fédération agréée')),
            ],
            options={
                'verbose_name': 'manifestations se déroulant sur un circuit permanent homologué',
                'verbose_name_plural': 'courses avec véhicules terrestres à moteur',
                'default_related_name': 'dvtmcir',
            },
            bases=('evenements.manif', models.Model),
        ),
        migrations.CreateModel(
            name='PieceJointeHistorique',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_televersement', models.DateTimeField(blank=True, null=True, verbose_name='Date de dépot de la pièce jointe')),
                ('document_attache', models.FileField(blank=True, max_length=512, null=True, upload_to=configuration.directory.UploadPath(''), validators=[core.FileTypevalidator.file_type_validator], verbose_name='Fichier')),
                ('pj', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='historique', to='evenements.piecejointe', verbose_name='Pièce jointe')),
            ],
        ),
    ]
