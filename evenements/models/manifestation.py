import sys

from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.core.validators import validate_comma_separated_integer_list
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.urls import reverse
from django.utils import timezone
from django.template.loader import render_to_string
from model_utils.managers import InheritanceQuerySet

from core.models.instance import Instance
from core.models import User
from structure.models.service import Federation
from carto.util.analyse_cartographie import poi_manquant, ligne_manquante

class ManifQuerySet(InheritanceQuerySet):
    """ Queryset des manifestations """

    # Getter
    def par_instance(self, request=None, instances=None):
        """
        Renvoyer les manifestations pour l'utilisateur ou les instances

        :param request: requête HTTP
        :param instances: liste d'instances
        :type instances: list<core.instance>
        """
        if request and not request.user.is_anonymous:
            # Renvoyer les manifestations pour l'utilisateur s'il est renseigné
            return self.filter(instance=request.user.get_instance())
        elif instances:
            # Renvoyer les manifestations pour les instances si elles sont renseignées
            return self.filter(instance__in=instances)
        else:
            return self

    def par_organisateur(self, organisateur):
        """ Renvoyer les manifestations créées par l'organisateur """
        return self.filter(structure__organisateur=organisateur)

    def prisesencharge(self):
        """ Renvoyer les manifestations prises en charge """
        return self.filter(instance__departement__isnull=False)

    def nonprisesencharge(self):
        """ Renvoyer les manifestations non prises en charge """
        return self.filter(instance__departement__isnull=True)


class Manif(models.Model):
    """ Demande de manifestation sportive

    Attention :
    Lors d'une modification dans ce modèle, bien veillez à vérifier les impacts dans :
    - evenements.views.manif.ManifClonageAjax.form_valid
    - evenements.views.exportation.Exportdossier
    - api.serializer.manif.ManifestationSerializer

    """

    CHOIX_VTM = [["dvtm", "dvtm"], ["avtm", "avtm"]]
    CHOIX_NM = (("dnm", "dnm"), ("dnmc", "dnmc"), ("dcnm", "dcnm"), ("dcnmc", "dcnmc"))
    CHOIX_MANIF = [["dnm", "dnm"], ["dnmc", "dnmc"], ["dcnm", "dcnm"], ["dcnmc", "dcnmc"],
                   ["dvtm", "dvtm"], ["avtm", "avtm"], ["dvtmcir", "dvtmcir"]]
    CHOIX_EMPRISE = [(0, "--------"),
                     (1, "Hors voies publiques"),  # non
                     (4, "Voies non ouvertes ou temporairement fermées à la circulation publique"),  # motorisé
                     (2, "Partiellement sur voies publiques ou ouvertes à la circulation"),
                     (3, "Totalement sur voies publiques ou ouvertes à la circulation")]
    EMPRISE = {"null": 0, "hors": 1, "partiel": 2, "total": 3, "temp": 4}  # Inverse de CHOIX_EMPRISE pour faciliter la lecture du code
    CHOIX_CIRCUIT = [(0, "--------"),
                     (1, "Circuit non permanent, terrains ou parcours"),
                     (2, "Circuit permanent non homologué dans la discipline de la manifestation"),
                     (3, "Circuit permanent homologué dans une discipline différente "),
                     (4, "Circuit permanent homologué dans la discipline de la manifestation")]
    CIRCUIT = {"null": 0, "non_permanent": 1, "permanent_non_homologue": 2, "permanent_homologue_autre_discipline": 3,
               "permanent_homologue": 4}  # Inverse de CHOIX_CIRCUIT pour faciliter la lecture du code

    # Texte d'aide et descriptions
    AIDE_DESCRIPTION = "précisez les modalités d'organisation et les caractéristiques de la manifestation"
    AIDE_OBSERVATION = "précisez les observations sur la manifestation, destinées aux services d'instruction"
    AIDE_PARCOURS_DESCRIPTION = "précisez pour chaque parcours : nb km, nb participants, heure de départ, départ groupé ou échelonné..."
    DESC_DANS_CALENDRIER = "Vous êtes affiliés à une fédération délégataire de la discipline et la manifestation est inscrite sur son calendrier"
    DESC_DEPARTEMENTS = "autres départements traversés par la manifestation"
    AIDE_MULTISELECT = "maintenez appuyé « Ctrl », ou « Commande (touche pomme) » sur un Mac, pour en sélectionner plusieurs."
    AIDE_NB_PARTICIPANTS = "renseigner le nombre de participants de la précedente édition ou le nombre de participants estimé si première édition"
    AIDE_NB_ORGANISATEURS = "renseigner le nombre d'organisateurs présents pour la manifestation"
    DESC_CONVENTION_BALISAGE = "Vous êtes signataire d'une charte de balisage temporaire ou départementale ou régionale"
    AIDE_CONVENTION_BALISAGE = ""
    AIDE_SENS_UNIQUE = "veuillez lister les voies publiques à emprunter en sens unique lors de la manifestation"
    AIDE_VOIE_FERMEE = "veuillez lister les voies publiques à fermer lors de la manifestation"
    DESC_GROS_BUDGET = "le budget de la manifestation excède 100 000 €"
    DESC_GROS_TITRE = "manifestation donnant lieu à la délivrance d'un titre national ou international"
    DESC_LUCRATIF = "manifestation présentant un caractère lucratif et regroupant sur un même site plus de 1500 personnes"
    DESC_VTM_HORS_CIRC = "manifestation avec engagement de véhicules terrestres à moteur se déroulant en dehors des voies ouvertes à la circulation"
    DESC_CHARTE_DISPENSE = "une charte de dispense en cours de validité pour le site Natura 2000 a été signée"
    DESC_VTM_N2K = "manifestation avec engagement de véhicules terrestres à moteur se déroulant sur des voies ouvertes à la circulation et à l'intérieur " \
                   "d'un périmètre Natura 2000"
    AIDE_N2K = "sélectionnez les sites Natura 2000 traversés par la manifestation"
    AIDE_RNR = "sélectionnez les zones Réserve Naturelle Régionale traversées par la manifestation"
    AIDE_PDESI = "sélectionner les lieux inscrits au plan PDESI traversés par la manifestation"
    DESC_HOMOLOGATION = "manifestation avec demande d'homologation d'un circuit de sports motorisés"

    DICT_FICHIER = {'reglement_manifestation': 'Réglement de la manifestation',
                    'disposition_securite': "Dispositions prises pour la sécurité",
                    'dossier_securite': "Dossier sécurité",
                    'presence_docteur': "Attestation de présence de médecin(s)",
                    'presence_secouriste': "Attestation de présence de secouriste(s)",
                    'presence_ambulance': "Attestation de présence de ambulance(s)",
                    'certificat_assurance': 'Attestation d\'assurance',
                    'charte_dispense_site_n2k': 'Charte de dispense Natura 2000',
                    'convention_police': 'Convention avec la police nationale ou la gendarmerie',
                    'visa_federation': "Preuve d'inscription au calendrier fédéral",
                    'convention_federation': 'Convention avec la fédération agréée',
                    'charte_balisage': 'Charte de balisage',
                    'itineraire_horaire': 'Itinéraire horaire',  # par departement
                    'dossier_tech_cycl': 'Dossier technique cyclisme',
                    'liste_signaleurs': 'Liste des signaleurs',  # par departement
                    'avis_federation_delegataire': 'Avis fédération délégataire',
                    'carte_zone_public': 'Carte zone publique',
                    'participants': 'Liste complète des participants (voir a331-21)',
                    'plan_masse': 'Plan de masse',
                    'liste_dates_manifestations': 'Liste des dates des manifestations'}

    # Attributs généraux
    instance = models.ForeignKey('core.instance', null=True, verbose_name="Configuration d'instance", on_delete=models.SET_NULL)
    structure_organisatrice_fk = models.ForeignKey('structure.StructureOrganisatrice', null=True, verbose_name="Structure organisatrice", on_delete=models.SET_NULL)
    nom = models.CharField(verbose_name="nom de la manifestation", max_length=255, null=True, blank=True, unique=True)
    date_creation = models.DateField("date de création", auto_now_add=True)
    date_debut = models.DateTimeField("date de début", null=True, blank=True)
    date_fin = models.DateTimeField("date de fin", null=True, blank=True)
    description = models.TextField("description", null=True, blank=True, help_text=AIDE_DESCRIPTION)
    observation = models.TextField("observation", default='', blank=True, help_text=AIDE_OBSERVATION)
    activite = models.ForeignKey('sports.activite', verbose_name="activité", on_delete=models.CASCADE)
    dans_calendrier = models.BooleanField(verbose_name=DESC_DANS_CALENDRIER, default=False)
    dans_calendrier_confirme = models.BooleanField(verbose_name="Confirmation d'inscription dans le calendrier", null=True, default=None)
    ville_depart = models.ForeignKey('administrative_division.commune', verbose_name="commune de départ", on_delete=models.CASCADE)
    ville_depart_interdep = models.JSONField(verbose_name="Villes de départ en cas d'interdepartementalité (champs texte technique)", blank=True, null=True, max_length=2000)
    ville_depart_interdep_mtm = models.ManyToManyField('administrative_division.commune', verbose_name="Communes de depart pour chaque departements", blank=True, related_name="manif_interdep")
    instance_instruites = models.ManyToManyField('core.instance', blank=True, verbose_name="Instance instruites par la plateforme", related_name="manif_instruite")
    parcours_openrunner = models.CharField("cartographie de parcours", validators=[validate_comma_separated_integer_list], max_length=255, blank=True)
    descriptions_parcours = models.TextField("description des parcours", default='', blank=True, help_text=AIDE_PARCOURS_DESCRIPTION)
    nb_organisateurs = models.PositiveIntegerField("nombre d'organisateurs", null=True, blank=True, help_text=AIDE_NB_ORGANISATEURS)
    nb_participants = models.PositiveIntegerField("nombre de participants", null=True, blank=True, help_text=AIDE_NB_PARTICIPANTS)
    nb_spectateurs = models.PositiveIntegerField("nombre max. de spectateurs", null=True, blank=True)
    villes_traversees = models.ManyToManyField('administrative_division.commune', related_name="%(app_label)s_%(class)s_crossed_by", verbose_name="communes traversées", blank=True)
    departements_traverses = models.ManyToManyField('administrative_division.departement', verbose_name=DESC_DEPARTEMENTS, blank=True)
    prenom_contact = models.CharField("prénom", max_length=200, null=True, blank=True)
    nom_contact = models.CharField("nom de famille", max_length=200, null=True, blank=True)
    tel_contact = models.CharField("numéro de téléphone", max_length=14, null=True, blank=True)
    declarant = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name="declarant", verbose_name='organisateur déclarant', on_delete=models.SET_NULL)

    # delai
    delai_cours = models.CharField('Delai en cours', max_length=30, null=True, blank=True)

    # Informations de publication
    prive = models.BooleanField(default=False, verbose_name="Ne pas publier la manifestation dans le calendrier")  # Organisateurs
    cache = models.BooleanField(default=False, verbose_name="Ne pas publier la manifestation dans le calendrier")  # Instructeurs
    afficher_adresse_structure = models.BooleanField(default=True, verbose_name="Afficher les coordonnées de la structure dans le calendrier")  # Organisateur

    # Informations de balisage
    convention_balisage = models.BooleanField(verbose_name=DESC_CONVENTION_BALISAGE, help_text=AIDE_CONVENTION_BALISAGE, default=False)
    balisage = models.TextField("Type et mode de balisage utilisé", null=True, blank=True)

    # Questions aiguillage cerfa, les libellés sont affinés dans form.create
    emprise = models.SmallIntegerField("Emprise sur voie publique", choices=CHOIX_EMPRISE, default=EMPRISE['null'], blank=True)
    circuit = models.SmallIntegerField("Site de pratique", choices=CHOIX_CIRCUIT, default=CIRCUIT['null'], blank=True)
    competition = models.BooleanField(verbose_name="Compétition", default=False)

    # Questions natura 2000
    gros_budget = models.BooleanField(verbose_name=DESC_GROS_BUDGET, default=False)
    delivrance_titre = models.BooleanField(verbose_name=DESC_GROS_TITRE, default=False)
    lucratif = models.BooleanField(verbose_name=DESC_LUCRATIF, default=False)
    vtm_hors_circulation = models.BooleanField(verbose_name=DESC_VTM_HORS_CIRC, default=False)

    signature_charte_dispense_site_n2k = models.BooleanField(verbose_name=DESC_CHARTE_DISPENSE, default=False)
    sites_natura2000 = models.ManyToManyField('evaluation_incidence.n2ksite', blank=True, verbose_name="sites Natura 2000", help_text=AIDE_N2K)
    lieux_pdesi = models.ManyToManyField('evaluation_incidence.pdesilieu', blank=True, verbose_name="lieux inscrits au PDESI", help_text=AIDE_PDESI)
    demande_homologation = models.BooleanField(verbose_name=DESC_HOMOLOGATION, default=False)
    zones_rnr = models.ManyToManyField('evaluation_incidence.rnrzone', blank=True, verbose_name="zones RNR", help_text=AIDE_RNR)

    modif_json = models.JSONField(verbose_name="modifications demandées (champs texte technique)", blank=True, null=True, max_length=2000)
    history_json = models.JSONField(verbose_name="Historique des modifications (champs texte technique)", blank=True, null=True, max_length=100000)
    autorisation_hors_delai_json = models.JSONField(verbose_name="Autorisation de dépôt hors délai", blank=True, null=True, max_length=2000)

    objects = ManifQuerySet.as_manager()

    # Overrides
    def __str__(self):
        if self.nom:
            return self.nom.title()
        return ''

    def save(self, *args, **kwargs):
        # Définir l'instance à celle de la ville de départ
        if self.ville_depart and not self.instance and not self.ville_depart.get_instance().is_master():
            self.instance = self.ville_depart.get_departement().get_instance()
        if self.pk and self.get_type_manif():
            delai = self.get_cerfa().delai_en_cours()
            if not delai == 21:
                self.delai_cours = str(delai)
            else:
                depot = self.get_cerfa().delaiDepot
                self.delai_cours = str(depot) + '-21'
        super().save(*args, **kwargs)
        if self.get_type_manif():
            self.creer_pj_cerfa()
        for instruction in self.instruction.all():
            instruction.set_service_instructeurs()

    def gestion_instance_instruites(self):
        # regénérer la liste des instances départementales concernées par la manifestion, ainsi que la liste de PJ attendues
        if not self.get_instruction():
            [self.instance_instruites.remove(instance) for instance in self.instance_instruites.all()]
            self.instance_instruites.add(self.instance)
            for dep in self.departements_traverses.all():
                if Instance.objects.filter(departement=dep, etat_s="ouvert"):
                    self.instance_instruites.add(Instance.objects.get(departement=dep))
            self.creer_pj_cerfa()

    def gestion_instance_instruites_manif_en_cours_instruction(self):
        from instructions.models import Instruction
        if self.get_instruction():
            # les instance instruites ne sont pas effacé car on n'efface jamais les instructions
            old_instance_instruites = list(self.instance_instruites.all())
            for dep in self.departements_traverses.all():
                if Instance.objects.filter(departement=dep, etat_s="ouvert"):
                    self.instance_instruites.add(Instance.objects.get(departement=dep))
            if old_instance_instruites != list(self.instance_instruites.all()):
                self.creer_pj_cerfa()
                for instance in self.instance_instruites.all():
                    if not self.get_instruction(instance=instance):
                        instruction = Instruction(manif=self, instance=instance, instruction_principale=self.get_instruction())
                        instruction.save()
                        instruction.refresh_from_db()
                        instruction.notifier_creation()
                        if not self.get_type_manif() in ['avtm']:
                            instruction.envoi_avis_fede()
                        else:
                            instruction.generer_avis_prefecture()

    def creer_pj_cerfa(self):
        from evenements.models import PieceJointe
        cerfa = self.get_cerfa()
        liste_fichiers_conditionels = ['convention_police', 'charte_dispense_site_n2k', 'visa_federation',
                                       'convention_federation', 'charte_balisage', 'plan_masse']

        def generer_delai(manif, file, cerfa, pj):
            if file in cerfa.get_liste_fichier_etape_0():
                pj.date_limite = manif.get_date_limite()
            elif file in cerfa.LISTE_FICHIERS_ETAPE_1:
                pj.date_limite = manif.get_date_etape_1()
            elif file in cerfa.LISTE_FICHIERS_ETAPE_2:
                pj.date_limite = manif.get_date_etape_2()
            pj.save()

        for file in cerfa.LISTE_FICHIERS:
            if self.instance_instruites.all().count() > 1 and file in ["liste_signaleurs", "itineraire_horaire"]:
                if PieceJointe.objects.filter(manif=self, champ_cerfa=file).exists() and not self.get_instruction():
                    PieceJointe.objects.get(manif=self, champ_cerfa=file).delete()
                for instance in self.instance_instruites.all():
                    dep = instance.departement
                    fichier_nom_cerfa = f"{file}_{str(dep.pk)}"
                    fichier_nom = f"{self.DICT_FICHIER.get(file)} pour le département {str(dep)}"
                    if PieceJointe.objects.filter(manif=self, champ_cerfa=fichier_nom_cerfa).exists():
                        pj = PieceJointe.objects.get(manif=self, champ_cerfa=fichier_nom_cerfa)
                    else:
                        pj = PieceJointe(manif=self, nom=fichier_nom, champ_cerfa=fichier_nom_cerfa)
                    generer_delai(self, file, cerfa, pj)
                for pj in PieceJointe.objects.filter(manif=self, champ_cerfa__icontains=file + "_"):
                    ok = False
                    for instance in self.instance_instruites.all():
                        if str(instance.departement.pk) in pj.champ_cerfa:
                            ok = True
                    if not ok and not self.get_instruction():
                        pj.delete()
            elif file not in liste_fichiers_conditionels or \
                    file in liste_fichiers_conditionels and file in cerfa.get_liste_fichier_etape_0():
                # cas ou une personne serait sortie de l'interdepartementalité réinitialisation des champs speciaux
                if not self.get_instruction() and self.instance_instruites.all().count() == 1:
                    if PieceJointe.objects.filter(manif=self, champ_cerfa__icontains=file + "_").exists():
                        PieceJointe.objects.filter(manif=self, champ_cerfa__icontains=file + "_").delete()
                if PieceJointe.objects.filter(manif=self, champ_cerfa=file).exists():
                    pj = PieceJointe.objects.get(manif=self, champ_cerfa=file)
                else:
                    pj = PieceJointe(manif=self, nom=self.DICT_FICHIER.get(file), champ_cerfa=file)
                generer_delai(self, file, cerfa, pj)
            elif file in liste_fichiers_conditionels and file not in cerfa.get_liste_fichier_etape_0():
                if PieceJointe.objects.filter(manif=self, champ_cerfa=file).exists():
                    PieceJointe.objects.get(manif=self, champ_cerfa=file).delete()

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de consultation """
        url = ''.join(['evenements:', ContentType.objects.get_for_model(self).model, '_detail'])
        return reverse(url, kwargs={'pk': self.pk})

    def get_history(self, nom_champ):
        if not self.history_json:
            return []
        liste_champ = list(filter(lambda x: x['champ'] == nom_champ, self.history_json))
        if not liste_champ:
            return []
        liste_champ.sort(key=lambda x: x['date'], reverse=True)
        return liste_champ

    def get_instance(self, instance=None):
        """
        Renvoyer l'instance associée à la manifestation et à l'instance choisie
        :rtype: core.models.Instance
        """
        return self.instance

    def get_commune_traverse_parcours(self):
        """
        Avoir la listes des communes traversées en fonction des parcours
        """
        tab = []
        for parcour in self.parcours.all():
            [tab.append(commune) if not commune in tab else "" for commune in parcour.lister_communes_traversees()]
        return tab

    def get_anomalie_commune_parcours_cerfa(self):
        """
        Avoir les anomalies de non-concordances des parcours et des cerfa au niveau des communes
        """
        communes = self.get_commune_traverse_parcours()
        communes_en_moins = []
        for commune in communes:
            if commune not in self.ville_depart_interdep_mtm.all() and commune not in self.villes_traversees.all():
                communes_en_moins.append(commune)
        communes_en_trop = []
        for commune in self.ville_depart_interdep_mtm.all():
            if commune not in communes:
                communes_en_trop.append(commune)
        for commune in self.villes_traversees.all():
            if commune not in communes:
                communes_en_trop.append(commune)
        if communes_en_trop or communes_en_moins:
            return {"communes_en_trop": communes_en_trop, "communes_en_moins": communes_en_moins}
        return None

    def get_arrondissement_traverse_parcours(self):
        """
                Avoir la listes des arrondissements traversés en fonction des parcours
                """
        tab = []
        for parcour in self.parcours.all():
            [tab.append(arron) if not arron in tab else "" for arron in parcour.lister_arrondissemeent_traverses()]
        return tab

    def get_departement_traverse_parcours(self):
        """
                Avoir la listes des départements traversés en fonction des parcours
                """
        tab = []
        for parcour in self.parcours.all():
            [tab.append(dep) if not dep in tab else "" for dep in parcour.lister_departement_traverses()]
        return tab

    def get_anomalie_departement_parcours_cerfa(self):
        """
        Avoir les anomalies de non-concordances des parcours et des cerfa au niveau des départements
        """
        dep = self.get_departement_traverse_parcours()
        departements_en_moins = []
        for departement in dep:
            if departement != self.ville_depart.arrondissement.departement and departement not in self.departements_traverses.all():
                departements_en_moins.append(departement)
        departements_en_trop = []
        if self.ville_depart.arrondissement.departement not in dep:
            departements_en_trop.append(self.ville_depart.arrondissement.departement)
        for departement in self.departements_traverses.all():
            if departement not in dep:
                departements_en_trop.append(departement)
        if departements_en_trop or departements_en_moins:
            return {"departements_en_trop": departements_en_trop, "departements_en_moins": departements_en_moins}
        return None

    def get_natura_traverse_parcours(self):
        """
                Avoir la listes des zones natura 2000 traversées en fonction des parcours
                """
        tab = []
        for parcour in self.parcours.all():
            [tab.append(site) if not site in tab else "" for site in parcour.lister_natura_traverses()]
        return tab

    def get_rnr_traverse_parcours(self):
        """
                Avoir la listes des zones rnr traversées en fonction des parcours
                """
        tab = []
        for parcour in self.parcours.all():
            [tab.append(site) if not site in tab else "" for site in parcour.lister_rnr_traverses()]
        return tab

    def get_departement_non_pris_en_charge(self):
        deps = []
        deps_pris_en_charge = [instance.departement for instance in self.instance_instruites.all()]
        [deps.append(departement) if departement not in deps_pris_en_charge else "" for departement in self.departements_traverses.all()]
        return deps

    def get_departement_depart_nom(self):
        """ Renvoyer le nom simple du département de départ """
        return self.ville_depart.get_departement().name

    def get_etat(self):
        """return l'etat du total des instructions"""
        if not self.get_instruction():
            return "aucun"
        if self.instance_instruites.count() == 1 or self.get_type_manif() in ['avtm']:
            return self.get_instruction().etat
        etats = [instruction.etat for instruction in self.instruction.all()]
        if "demandée" in etats:
            if not "autorisée" in etats and not "interdite" in etats and not "distribuée" in etats and not "annulée" in etats:
                return "demandée"
            elif not "autorisée" in etats and not "interdite" in etats and not "annulée" in etats:
                return "distribuée"
            else:
                return "multiple"
        if "distribuée" in etats:
            if not "autorisée" in etats and not "interdite" in etats and not "annulée" in etats:
                return "distribuée"
            else:
                return "multiple"
        if "autorisée" in etats:
            if not "interdite" in etats and not "annulée" in etats:
                return "autorisée"
            else:
                return "multiple"
        if "interdite" in etats and not "annulée" in etats:
            return "interdite"
        if "annulée" in etats:
            return "annulée"
        # cas impossible on met quand même une sécurité en prenant l'etat de l'instruction principale
        return self.get_instruction().etat

    def get_federation(self, departement=None):
        """ Renvoyer la fédération pour le département et pour la discipline """
        if not departement:
            fede = Federation.non_polymorphic_objects.get_for_departement(self.activite.discipline, self.ville_depart.get_departement())
        else:
            fede = Federation.non_polymorphic_objects.get_for_departement(self.activite.discipline, departement)
        return fede

    def get_organisateur(self):
        """ Renvoyer l'organisateur """
        return self.structure_organisatrice_fk.get_users_list()

    def get_instruction(self, instance=None):
        """ Renvoyer l'autorisation si disponible """
        if not instance:  # ce cas la est utilisé pour renvoyer l'instruction dite principale
            instruction = self.instruction.all().order_by('pk')
        else:
            instruction = self.instruction.filter(instance=instance)
        return instruction.first() if instruction else None

    def get_instructeurs_manif(self, instance=None):
        """
        Pour le cas ou l'on veut les instructeurs d'une manif en préparation (sans instruction)
        Utilisée pour l'envoi de messages
        """
        from instructions.models import Instruction
        if self.instruction.all():
            if instance:
                return self.get_instruction(instance=instance).get_instructeurs_avec_services()
            else:
                recipients = []
                for instruction in self.instruction.all():
                    recipients += instruction.get_instructeurs_avec_services()
                return recipients
        else:
            recipients = Instruction.get_instructeurs_mairie(self, message=True)
            if not recipients:
                recipients = Instruction.get_instructeurs_prefecture(self, message=True)
            return recipients

    def get_lecture_user(self):
        recipients = []
        for acces in self.acces.all():
            [recipients.append([user, acces.organisation]) for user in acces.organisation.get_users_qs()]
        return recipients

    def get_pj(self, request=None):
        """
        Renvoyer les pj mais en triant pour que l'on est les pj cerfa avant
        :return:
        """

        pj = []
        if not request or request.organisation.is_structure_organisatrice() or request.user.get_instance().is_master() or self.instance_instruites.all().count() == 1:
            [pj.append(piece) for piece in self.piece_jointe.filter(champ_cerfa__isnull=False).order_by('pk')]
        else:
            #  séparation des pièces jointes specifiques par département
            departement_pk = str(request.user.get_instance().departement.pk)
            [pj.append(piece) if not ("liste_signaleurs" in piece.champ_cerfa or "itineraire_horaire" in piece.champ_cerfa) or \
                    departement_pk in piece.champ_cerfa else "" for piece in self.piece_jointe.filter(champ_cerfa__isnull=False).order_by('pk')]
        [pj.append(piece) for piece in self.piece_jointe.filter(champ_cerfa__isnull=True).order_by('pk')]
        return pj

    def get_departements_traverses(self):
        """ Renvoyer les départements traversés """
        return list(set(list(self.departements_traverses.all()) + [self.ville_depart.get_departement()]))

    def get_villes_traversees(self):
        """
        Renvoyer les communes traversées

        :returns: toutes les communes traversées (dont celle de départ)
        """
        return list(set(list(self.villes_traversees.all()) + [self.ville_depart]))

    def get_nom_departements_traverses(self):
        """ Renvoyer les noms (01, 42, 78 etc.) des départements traversés """
        return [departement.name for departement in self.get_departements_traverses()]

    def sur_plusieurs_arrondissement(self):
        """
        Circuit 5 : Savoir si la manifestation se déroule sur plusieurs arrondissement
        """
        if self.villes_traversees.all():
            arrond_depart = self.ville_depart.arrondissement
            for ville in self.villes_traversees.all():
                if ville.arrondissement != arrond_depart:
                    return True
        if self.departements_traverses.all().count() != 0:
            return True
        return False

    def pas_supportee(self):
        """ Renvoyer si la manifestation n'est pas prise en charge """
        return self.instance is None or self.instance.departement is None

    def visible_dans_calendrier(self):
        """ Renvoyer l'état de visibilité dans le calendrier """
        return not (self.prive or self.cache)
    visible_dans_calendrier.short_description = "Dans le calendrier"
    visible_dans_calendrier.boolean = True

    def soumise_natura2000(self):
        """ Renvoyer si la manifestation est soumise à une éval N2K """
        return hasattr(self, 'natura2000evaluations') or hasattr(self, 'n2kevaluation')
    soumise_natura2000.short_description = "est soumise à évaluation natura2000"
    soumise_natura2000.boolean = True

    def soumise_rnr(self):
        """ Renvoyer si la manifestation est soumise à une éval RNR """
        return hasattr(self, 'rnrevaluations') or hasattr(self, 'rnrevaluation')
    soumise_rnr.short_description = "est soumise à évaluation RNR"
    soumise_rnr.boolean = True

    def get_delai_legal(self):
        """ Renvoie le délai légal en jours pour déclarer ou envoyer la demande d'autorisation """
        return self.get_cerfa().delaiDepot

    def get_date_limite(self):
        """ Retourne la date limite pour déclarer ou envoyer la demande d'autorisation """
        return (self.date_debut - timezone.timedelta(days=self.get_delai_legal())).replace(hour=23, minute=59)

    def get_date_etape_1(self):
        """ Retourne la première date limite pour déposer des documents manquants """
        return (self.date_debut - timezone.timedelta(days=self.get_cerfa().delaiDocComplement1)).replace(hour=23, minute=59)

    def get_date_etape_2(self):
        """ Retourne la deuxième date limite pour déposer des documents manquants """
        return (self.date_debut - timezone.timedelta(days=self.get_cerfa().delaiDocComplement2)).replace(hour=23, minute=59)

    def deux_semaines_restantes(self):
        """ Renvoie true or false suivant le nombre de semaines restantes (sous 2 semaines ou plus de 2 semaines) """
        return self.get_date_limite().date() < (timezone.now().date() + timezone.timedelta(weeks=2))

    def delai_depasse(self):
        """ Si le délai officiel est dépassé, renvoie True, sinon, renvoie False """
        return timezone.now() > self.get_date_limite()

    def date_depassee(self):
        """ Si la manif est passée, renvoie True, sinon, renvoie False """
        return timezone.now() > self.date_debut

    def get_nbr_ppl(self):
        """
        Renvoie un int contenant le nombre de personne actuellement sur le dossier
        :return:
        """
        last_time = timezone.now() - timezone.timedelta(minutes=settings.INACTIVE_TIME)
        count = User.objects.filter(last_manif=self.pk, last_visit__gte=last_time).count()
        return count

    def get_parcours_manif(self):
        """
        Renvoyer les parcours sélectionnés pour la manifestation
        :returns: un dictionnaire id parcours:nom parcours
        :rtype: dict
        """
        return {parcours.id: parcours.name for parcours in self.parcours.all()}

    def get_manifs_meme_jour(self, include_self=False):
        """
        Renvoyer les manifestations qui ont lieu aux mêmes dates que cette manifestation en excluant les manifestations annulées
        (et qui bien entendu, ne sont pas cette même manifestation)

        Dans la pratique, si cette manifestation est nommée e1, et la manifestation à
        comparer est nommée e2, les deux manifestations coïncident si :
        - (e2.debut <= e1.debut ET e2.fin >= e1.debut) OU
        - (e2.debut <= e1.fin ET e2.fin >= e1.fin) OU
        - (e2.debut >= e1.debut ET e2.fin <= e1.fin)

        Et plus simplement, elles coïncident si :
        - NON (e2.debut > e1.fin OU e2.fin < e1.debut)
        """
        if self.nom and self.date_debut and self.date_fin:
            exclusion = {} if include_self else {'nom': self.nom}
            return Manif.objects.exclude(
                instruction__etat="annulée").exclude(
                models.Q(date_debut__gt=self.date_fin) | models.Q(date_fin__lt=self.date_debut)).exclude(
                **exclusion).distinct()
        return Manif.objects.none()

    def get_manifs_meme_jour_meme_ville(self, include_self=False):
        """
        Renvoyer les manifestations qui passent dans les mêmes villes
        que la manifestation actuelle.
        """
        communes = self.get_villes_traversees()
        return self.get_manifs_meme_jour(include_self=include_self).filter(
            models.Q(ville_depart__in=communes) | models.Q(villes_traversees__in=communes)).distinct()

    def get_str_ids_parcours_en_conflit(self, include_self=True):
        """
        Renvoyer les ids des parcours probablement en conflit formatté en chaine avec séparateur "-"
        Les ids de parcours de la manifestation en cours sont inclus.

        :rtype: list
        """
        # Pour ne pas interroger OpenRunner lorsque l'on joue les Tests de Django avec manage.py
        if 'test' in sys.argv[1:]:
            return '1'

        conflits_possible = self.get_manifs_meme_jour_meme_ville(include_self=include_self)
        parcours_ids = []
        for manifestation in conflits_possible:
            parcours = manifestation.get_parcours_manif()
            for key in parcours.keys():  # Récupérer les ids
                parcours_ids.append(key)
        parcours_ids = list(set(parcours_ids))  # dédupliquer
        return '-'.join([str(parcours) for parcours in parcours_ids])

    def afficher_panneau_eval_rnr(self):
        """ Renvoyer si l'encart d'évaluation RNR doit être affiché """
        from evaluation_incidence.models import EvaluationManif
        return EvaluationManif.evaluation_rnr_requise(self)

    def afficher_panneau_eval_n2000(self, cause=None):
        """ Renvoyer si l'encart d'évaluation Natura2000 doit être affiché """
        from evaluation_incidence.models import EvaluationManif
        return EvaluationManif.evaluation_n2k_requise(self, cause)

    def get_breadcrumbs(self):
        """ Renvoie les breadcrumbs indiquant l'avancement de l'instruction """
        # breadcrumbs est une liste d'étape d'instruction, chaque étape d'instruction est une liste composée ainsi :
        # ["Libellé de l'étape", boolean qui indique si l'étape est terminée ]
        breadcrumbs = [["<i class='detail'></i>Préparation du dossier",
                        False,
                        self.get_absolute_url()+"edit/?dept="+self.get_departement_depart_nom()]]
        if self.formulaire_complet():
            breadcrumbs = [["<i class='detail'></i>Préparation du dossier", True, ""]]
        if self.afficher_panneau_eval_rnr():
            breadcrumbs.append(["<i class='feuille'></i>EI simplifiée RNR",
                                False,
                                reverse('evaluation_incidence:rnreval_add', kwargs={'manif_pk': self.id})])
        if hasattr(self, 'rnrevaluation') and not self.rnrevaluation.formulaire_rnr_complet():
            breadcrumbs.append(["<i class='feuille'></i>EI simplifiée RNR",
                                False,
                                reverse('evaluation_incidence:rnreval_update', kwargs={'pk': self.rnrevaluation.id})])
        elif hasattr(self, 'rnrevaluations') or (hasattr(self, 'rnrevaluation') and self.rnrevaluation.formulaire_rnr_complet()):
            breadcrumbs.append(["<i class='feuille'></i>EI simplifiée RNR", True, ""])
        if self.afficher_panneau_eval_n2000():
            breadcrumbs.append(["<i class='feuille'></i>EI simplifiée Natura 2000",
                                False,
                                reverse('evaluation_incidence:n2keval_add', kwargs={'manif_pk': self.id})])
        if (hasattr(self, 'n2kevaluation') and
                (not self.n2kevaluation.formulaire_n2k_complet() or
                 (not self.n2kevaluation.date_soumission and self.get_instruction()))):
            breadcrumbs.append(["<i class='feuille'></i>EI simplifiée Natura 2000",
                                False,
                                reverse('evaluation_incidence:n2keval_update', kwargs={'pk': self.n2kevaluation.id})])
        elif hasattr(self, 'natura2000evaluations') or (hasattr(self, 'n2kevaluation') and self.n2kevaluation.formulaire_n2k_complet()):
            breadcrumbs.append(["<i class='feuille'></i>EI simplifiée Natura 2000", True, ""])
        if not self.get_instruction():
            breadcrumbs.append(["<i class='envoyer'></i>Dossier à envoyer", False, ""])
        else:
            breadcrumbs.append(["<i class='envoyer'></i>Envoi du dossier&nbsp;&nbsp;", True, ""])
            if not self.dossier_complet():
                breadcrumbs.append(["<i class='doc'></i>Pièce à joindre", False, "#pieceajoindre"])
        return breadcrumbs

    def etape_en_cours(self):
        """ Renvoyer la période de temps actuelle en fonction des différents delais """
        if timezone.now() < self.get_date_limite():
            return 'etape 0'
        elif timezone.now() < self.get_date_etape_1():
            return 'etape 1'
        elif timezone.now() < self.get_date_etape_2():
            return 'etape 2'
        return 'etape 3'

    def date_limite_etape_en_cours(self):
        """Renvoi la date limite de la prochaine étape"""
        if timezone.now() < self.get_date_limite():
            return self.get_date_limite()
        elif timezone.now() < self.get_date_etape_1():
            return self.get_date_etape_1()
        elif timezone.now() < self.get_date_etape_2():
            return self.get_date_etape_2()
        return self.date_debut

    def delai_en_cours(self):
        """ Renvoyer le delai qui court à la date en cours """
        if timezone.now() < self.get_date_limite():
            return self.delaiDepot
        elif timezone.now() < self.get_date_etape_1():
            return self.delaiDocComplement1
        elif timezone.now() < self.get_date_etape_2():
            return self.delaiDocComplement2
        return 0

    def ecrire_delai(self, manif=None, all=False):
        if not manif:

            if all:
                manifs = Manif.objects.all()
            else:
                manifs = Manif.objects.filter(date_debut__gte=timezone.now())

            for manif in manifs:
                if hasattr(manif.get_cerfa(), "delaiDepot"):
                    delai = manif.get_cerfa().delai_en_cours()
                    if not delai == 21:
                        manif.delai_cours = str(delai)
                    else:
                        depot = manif.get_cerfa().delaiDepot
                        manif.delai_cours = str(depot)+'-21'
                    manif.save()
            oldmanifs = Manif.objects.filter(date_debut__lte=timezone.now()).filter(delai_cours__isnull=False)
            for manif in oldmanifs:
                manif.delai_cours = None
                manif.save()
        else:
            delai = manif.get_cerfa().delai_en_cours()
            if not delai == 21:
                manif.delai_cours = str(delai)
            else:
                depot = manif.get_cerfa().delaiDepot
                manif.delai_cours = str(depot) + '-21'
            manif.save()

    def formulaire_complet(self):
        """ Indiquer si le formulaire est complet """
        for champ in self.get_liste_champs_etape_0():
            if getattr(self, champ) is None:
                return False
            elif isinstance(getattr(self, champ), str) and getattr(self, champ) == '':
                return False
        # verifier que les ville de depart mtm correspondent aux nombre de dep traverse
        # pour prevenir le cas de manif interdep clonées faites avant la mise en place de l'interdep
        if self.departements_traverses.count():
            if self.departements_traverses.count() + 1 != self.ville_depart_interdep_mtm.count():
                return False
        return True

    def carto_complet(self):
        """ Indiquer si la carto est complete """
        if self.get_type_manif() == "dvtmcir":
            return True
        # Ne pas afficher de message d'alerte si le dossier est déja en instruction
        if self.get_instruction():
            return True
        if self.parcours.all():
            if ligne_manquante(self):
                return ligne_manquante(self)
            # Selon certaine manif, test si des POI sont manquants
            if self.get_type_manif() in ['avtm', 'dcnm', 'dcnmc']:
                if poi_manquant(self):
                    return poi_manquant(self)
            return True
        # Pas de cartographie associée à la manifestation
        return {
                'bloquant': True,
                'code_erreur': 0,
                'type': "Cartographie manquante",
                'message': "Vous devez fournir vos parcours au sein de l'outil cartographique"
                           "<a target=\"_blank\" href=\"/aide/FAQ-organisateurs/540?tab=103\">"
                           "<i class=\"ctx-help- fas fa-info-circle\"></i></a>"
                }

    def dossier_complet(self):
        """ Indiquer si le dossier est complet en fonction de la date courante et des pièces manquantes """

        from .piecejointe import PieceJointe
        if self.afficher_panneau_eval_n2000():
            return False
        if hasattr(self, 'n2kevaluation'):
            if not self.n2kevaluation.formulaire_n2k_complet():
                return False
        if self.afficher_panneau_eval_rnr():
            return False
        if hasattr(self, 'rnrevaluation'):
            if not self.rnrevaluation.formulaire_rnr_complet():
                return False
        if not self.formulaire_complet():
            return False
        carto_complete = self.carto_complet()
        if carto_complete is not True and carto_complete['bloquant']:
            return False
        for fichier in self.get_liste_fichier_etape_0():
            if PieceJointe.objects.filter(manif=self, champ_cerfa__icontains=fichier, document_attache=""):
                return False
        if self.etape_en_cours() == 'etape 1':
            for fichier in self.LISTE_FICHIERS_ETAPE_1:
                if PieceJointe.objects.filter(manif=self, champ_cerfa__icontains=fichier, document_attache=""):
                    return False
        if self.etape_en_cours() == 'etape 2' or self.etape_en_cours() == 'etape 3':
            liste = self.LISTE_FICHIERS_ETAPE_1 + self.LISTE_FICHIERS_ETAPE_2
            for fichier in liste:
                if PieceJointe.objects.filter(manif=self, champ_cerfa__icontains=fichier, document_attache=""):
                    return False
        return True

    def liste_manquants(self):
        """
        Donner la liste des documents manquants en fonction de la date courante
        On détermine le modèle de dossier puis on retrouve le champ dans le méta du modèle pour extraire le verbose_name
        """
        listemanquants = []
        from .piecejointe import PieceJointe
        for fichier in self.get_liste_fichier_etape_0():
            pjs = PieceJointe.objects.filter(manif=self, champ_cerfa__icontains=fichier)
            [listemanquants.append(pj.nom) if pj and not pj.document_attache else "" for pj in pjs]
        if self.etape_en_cours() == 'etape 1':
            for fichier in self.LISTE_FICHIERS_ETAPE_1:
                pjs = PieceJointe.objects.filter(manif=self, champ_cerfa__icontains=fichier)
                [listemanquants.append(pj.nom) if pj and not pj.document_attache else "" for pj in pjs]
        if self.etape_en_cours() == 'etape 2' or self.etape_en_cours() == 'etape 3':
            liste = self.LISTE_FICHIERS_ETAPE_1 + self.LISTE_FICHIERS_ETAPE_2
            for fichier in liste:
                pjs = PieceJointe.objects.filter(manif=self, champ_cerfa__icontains=fichier)
                [listemanquants.append(pj.nom) if pj and not pj.document_attache else "" for pj in pjs]
        return listemanquants

    def instruction_par_mairie(self):
        if (not self.villes_traversees.all() and not self.get_type_manif() in ['dvtm', 'avtm', 'dvtmcir'] and
                self.instance_instruites.all().count() == 1):
            return True
        return False

    def est_instructible_mairie(self):
        from instructions.models import Instruction
        if self.instruction_par_mairie():
            if not Instruction.get_service_mairie_qs(self).exists():
                return False
            service = Instruction.get_service_mairie_qs(self).first()
            if not service.get_users_actif_qs():
                return False
        return True


    def en_cours_instruction(self):
        """
        Renvoyer si une spécialisation de manifestation existe
        En d'autres mots, renvoyer si le formulaire principal est validé.
        """
        if not self.get_instruction():
            return False
        return True
    en_cours_instruction.short_description = "Formulaire validé"
    en_cours_instruction.boolean = True

    def a_modifier(self):
        """ Renvoye une liste des champs à modifier"""
        modif_list = []
        if self.modif_json:
            for line in self.modif_json:
                if not line['done']:
                    if 'pj' in line:
                        modif_list.append("pièce jointe")
                    elif 'parcours' in line:
                        modif_list.append("parcours")
                    elif 'champ_n2k' in line:
                        modif_list.append("formulaire n2k")
                    elif 'champ_rnr' in line:
                        modif_list.append("formulaire rnr")
                    else:
                        modif_list.append("formulaire manifestation")
        return modif_list

    def a_modifier_verbal(self):
        """ Renvoye une string propre des champs à modifier"""
        modif_string = ""
        if self.modif_json:
            for line in self.modif_json:
                if not line['done']:
                    if 'pj' in line and "Pièce" not in modif_string:
                        modif_string += "- Pièce jointe <br>"
                    elif 'parcours' in line and "Parcours" not in modif_string:
                        modif_string += "- Parcours <br>"
                    elif 'champ_n2k' in line and "n2k" not in modif_string:
                        modif_string += "- Formulaire n2k <br>"
                    elif 'champ_rnr' in line  and "rnr" not in modif_string:
                        modif_string += "- Formulaire rnr <br>"
                    elif "manifestation" not in modif_string:
                        modif_string += "- Formulaire manifestation <br>"
        return modif_string

    def get_doc_officiels(self):
        doc = []
        for instruction in self.instruction.all():
            [doc.append(document) for document in instruction.documents.all()]
        return doc

    def count_doc_officiel(self):
        return len(self.get_doc_officiels())

    def get_docs_complementaires_manquants(self):
        """ Renvoyer les documents complémentaires dont le fichier n'est pas renseigné """
        return self.piece_jointe.filter(champ_cerfa__startswith="demande_instructeur", document_attache='')

    def get_type_manif(self):
        """
        Renvoyer l'attribut qui définit le type de manifestation

        :rtype: str
        """
        for name in ['dnm', 'dnmc', 'dcnm', 'dcnmc', 'dvtm', 'avtm', 'dvtmcir']:
            try:
                getattr(self, name)
                return name
            except (AttributeError, ObjectDoesNotExist):
                pass
        return None
    get_type_manif.short_description = "Type"

    def get_cerfa(self):
        """ Renvoyer l'objet manifestation spécialisé  """
        name = self.get_type_manif()
        if name:
            return getattr(self, name)
        return self

    def is_not_dncmc_sans_masse(self):
        """ Vérifier si la manifestation est autre chose qu'une Dcnmc sans épreuve de masse """
        if self.get_type_manif() == "dcnmc" and self.activite.pk != 123:
            return False
        else: # tous les cerfa + les dcnmc avec épreuves de masse
            return True

    @property
    def cerfa(self):
        """ Renvoyer l'objet manifestation spécialisé  """
        name = self.get_type_manif()
        if name:
            return getattr(self, name)
        return self

    def get_context_fede(self):
        """ Renvoyer un mot clé suivant le cas détecté pour affichage dans la description """
        if self.en_cours_instruction():
            # selectionner la catégorie fédération avec le pk
            if not self.get_instruction().avis.filter(service_consulte_fk__type_service_fk__categorie_fk__pk=12):
                if self.get_type_manif() == 'avtm':
                    if self.activite.discipline.name == 'Autres sports motorisés':
                        return "avtm_autres"
                    else:
                        return "cdsr"
                else:
                    if self.dans_calendrier:
                        return "calendrier"
                    else:
                        return "sans_fede"
        return ""

    def notifier_creation(self):
        """ Enregistrer l'action de création """
        from messagerie.models import Message
        data = {
            "manif": self,
            "url": reverse("evenements:manif_url", kwargs={'pk': self.pk}),
            "structure": self.structure_organisatrice_fk,
            "user": self.declarant,
        }
        contenu = render_to_string('messagerie/mail/message_organisateur_creation_manif.txt', data)
        Message.objects.creer_et_envoyer('tracabilite', None, self.structure_organisatrice_fk.get_utilisateurs_avec_service(),
                                         'Création du dossier de manifestation',
                                         contenu, manifestation_liee=self, objet_lie_nature="dossier")

    def notifier_manif_modifiee(self, champ):
        """ Notifier la modification de la manifestation si l'intruction est en cours """
        from messagerie.models import Message
        expediteur = self.declarant
        destinatairesaction, destinataires = [], []
        for instruction in self.instruction.all():
            destinatairesaction += instruction.get_instructeurs_avec_services()
            destinataires += instruction.get_tous_agents_avec_service()
            destinataires += instruction.manif.get_lecture_user()
        destinataires += self.get_lecture_user()
        if champ.startswith('ville_depart_interdep'):
            champ = '_'.join(champ.split('_')[:-1])
        titre = "Modification de dossier réalisée par l'organisateur"
        contenu = f"<p>L'organisateur vient de modifiée l'information <em>{champ}</em> sur son dossier de manifestation {self.nom}<br>"
        Message.objects.creer_et_envoyer('action', [expediteur, self.structure_organisatrice_fk], destinatairesaction, titre, contenu,
                                         manifestation_liee=self, objet_lie_nature="dossier")
        Message.objects.creer_et_envoyer('info_suivi', [expediteur, self.structure_organisatrice_fk], destinataires,
                                         titre, contenu,
                                         manifestation_liee=self, objet_lie_nature="dossier")

    def notifier_manif_a_modifier(self, user, organisation, cle_champ, champ):
        """ Notifier la demande de modification de la manifestation si l'intruction est en cours """
        from messagerie.models import Message
        if cle_champ == 'eval_n2k':
            titre = "Demande d'ajout d'une évaluation Natura2000 par l'instructeur"
            contenu = f"<p>L'instructeur sollicite l'ajout d'une évaluation Natura2000 sur votre dossier de manifestation {self.nom}.</p>"
        else:
            if cle_champ in ['champ_n2k', 'champ_rnr']:
                titre = "Demande de modification de l'évaluation"
                if cle_champ == 'champ_n2k':
                    verbose = self.n2kevaluation._meta.get_field(champ).verbose_name
                    titre += "Natura2000 par l'instructeur"
                else:
                    verbose = self.rnrevaluation._meta.get_field(champ).verbose_name
                    titre += "RNR par l'instructeur"
            else:
                if champ.startswith('ville_depart_interdep'):
                    champ = '_'.join(champ.split('_')[:-1])
                titre = "Demande de modification de dossier"
                verbose = self.get_cerfa()._meta.get_field(champ).verbose_name
            contenu = f"<p>L'instructeur sollicite une modification de l'information {verbose} sur votre dossier de manifestation {self.nom}</p>" +  \
                      f"Attention : vous ne pourrez modifier l'information qu'une seule fois.</p>"
        Message.objects.creer_et_envoyer('action', [user, organisation], self.structure_organisatrice_fk.get_utilisateurs_avec_service(),
                                     titre, contenu, manifestation_liee=self, objet_lie_nature="dossier")

    def notifier_annulation_modification(self, user, organisation, cle_champ, champ):
        """ Notifier l'annulation de la demande de modification de la manifestation """
        from messagerie.models import Message
        if cle_champ in ['champ_n2k', 'champ_rnr']:
            titre = "Annulation de la demande de modification d'évaluation par l'instructeur"
            if cle_champ == 'champ_n2k':
                verbose = self.n2kevaluation._meta.get_field(champ).verbose_name
            else:
                verbose = self.rnrevaluation._meta.get_field(champ).verbose_name
        else:
            titre = "Annulation de la demande de modification de dossier"
            try:
                verbose = self.get_cerfa()._meta.get_field(champ).verbose_name
            except:  # prevenir des champs interdep
                verbose = champ
        contenu = f"<p>L'instructeur a annulé la solicitation de la modification de l'information {verbose} sur votre dossier de manifestation {self.nom}</p>"
        Message.objects.creer_et_envoyer('action', [user, organisation], self.structure_organisatrice_fk.get_utilisateurs_avec_service(),
                                         titre, contenu, manifestation_liee=self, objet_lie_nature="dossier")

    def has_access(self, user, organisation_origine=None):
        """
        params: user
        return : False ou l'organisation autorisé à consulter la manif.
        """
        ok = None
        tab_ok = []
        if not user.is_anonymous:
            for organisation in user.organisation_m2m.all():
                if self in organisation.get_manifs_ecriture() or self in organisation.get_manifs_lecture():
                    tab_ok.append(organisation.pk)
            from structure.models import ServiceConsulte
            o_support = ServiceConsulte.objects.get(type_service_fk__categorie_fk__nom_s="_Support")
            if o_support in user.organisation_m2m.all():
                tab_ok.append(o_support.pk)
        if tab_ok:
            if organisation_origine and organisation_origine.pk in tab_ok:
                return organisation_origine
            return user.organisation_m2m.get(pk=tab_ok[0])
        return ok

    def get_url_manif_associe(self,  service):
        """
        Permet d'avoir l'url d'une dossier selon le service qui la consulte
        """
        from instructions.models.acces import AutorisationAcces
        url = self.get_absolute_url()
        if service.is_structure_organisatrice():
             return self.get_cerfa().get_absolute_url()
        if service.is_service_instructeur() and self.get_instruction(instance=service.instance_fk) \
                and self.get_instruction(instance=service.instance_fk) in service.instructions.all():
            return self.get_instruction(instance=service.instance_fk).get_absolute_url()
        if not self.get_instruction() and service.is_service_instructeur() and AutorisationAcces.objects.filter(manif=self, organisation=service).exists():
            return self.get_cerfa().get_absolute_url()
        if service.is_service_consulte():
            dep = service.departement_m2m.all()
            if not dep and self.get_instruction(instance=service.instance_fk) and self.get_instruction(instance=service.instance_fk).get_avis_service(service):
                return self.get_instruction(instance=service.instance_fk).get_avis_service(service).get_absolute_url()
            elif dep:
                instructions = self.instruction.filter(instance__departement__in=dep)
                for instruction in instructions:
                    if instruction.get_avis_service(service):
                        return instruction.get_avis_service(service).get_absolute_url()
            else:
                return self.get_absolute_url()
        from instructions.models import AutorisationAcces
        from structure.models import Organisation
        organisation = Organisation.objects.get(pk=service.pk)
        if AutorisationAcces.objects.filter(organisation=organisation, date_revocation__isnull=True, instruction__manif=self):
            return AutorisationAcces.objects.filter(organisation=organisation, date_revocation__isnull=True, instruction__manif=self).first().instruction.get_absolute_url()
        if AutorisationAcces.objects.filter(organisation=organisation, date_revocation__isnull=True, avis__instruction__manif=self):
            return AutorisationAcces.objects.filter(organisation=organisation, date_revocation__isnull=True, avis__instruction__manif=self).first().avis.get_absolute_url()
        if AutorisationAcces.objects.filter(organisation=organisation, date_revocation__isnull=True, manif=self):
            return AutorisationAcces.objects.filter(organisation=organisation, date_revocation__isnull=True, manif=self).first().manif.get_cerfa().get_absolute_url()
        return url

    # Meta
    class Meta:
        verbose_name = "manifestation"
        verbose_name_plural = "manifestations"
        default_related_name = "manifs"
        app_label = "evenements"


class ManifRelatedModel(models.Model):
    """
    Tous les modèles liés à manifestation peuvent hériter de cette classe
    afin de bénéficier de méthodes liées aux informations de manifestation
    """

    # Getter
    def get_manif(self):
        """ Renvoyer la manifestation de l'objet """
        return self.manif

    def get_instance(self):
        """ Renvoyer l'instance liée à la manifestation liée à l'objet """
        return self.instance

    # Meta
    class Meta:
        abstract = True


class FichierExportation(models.Model):

    url = models.CharField(max_length=500, default="")
    date = models.DateTimeField(auto_now=True)