from django.db import models

from . import Manif
from .instructionconfig import AbstractInstructionConfig


class AbstractVtm(AbstractInstructionConfig):
    """ Manifestation avec véhicule motorisé """

    # Appel en deuxième position -> mixin : pas de surcharge des méthodes !!!!
    # Déplacement de la méthode "afficher_panneau_eval_n2000" dans les classes filles.

    # Champs
    vehicules = models.PositiveIntegerField("nombre de véhicules", blank=True, null=True)
    # Ajout de default dans les CharField et TextField pour la DB existante
    technique_nom = models.CharField("nom de famille", max_length=200, blank=True, default='')
    technique_prenom = models.CharField("prénom", max_length=200, blank=True, default='')
    technique_tel = models.CharField("numéro de téléphone", max_length=14, blank=True, default='')
    technique_email = models.EmailField("adresse email", max_length=200, blank=True, default='')

    # Meta
    class Meta:
        abstract = True


class Dvtm(AbstractVtm, Manif):
    """ Manifestation avec véhicule motorisé hors compétition """

    # Constantes
    refCerfa = "cerfa_15848-01"
    consultFederation = False
    consultServices = AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation optionnelle']
    # delaiDepot = defaut = 60
    # delaiDepot_ndept = defaut = 90
    # delaiDocComplement1 = defaut = 21
    # delaiDocComplement2 = defaut = 6
    LISTE_CHAMPS_ETAPE_0 = AbstractInstructionConfig.LISTE_CHAMPS_ETAPE_0 + ['nb_vehicules_accompagnement',
                                                                             'nb_personnes_pts_rassemblement', 'vehicules']
    LISTE_FICHIERS = AbstractInstructionConfig.LISTE_FICHIERS + ['itineraire_horaire', 'presence_docteur', 'presence_secouriste',
                                                                 'presence_ambulance']
    LISTE_FICHIERS_ETAPE_0 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_0 + ['itineraire_horaire']

    # Champs
    manifestation_ptr = models.OneToOneField('evenements.Manif', parent_link=True, related_name='dvtm', on_delete=models.CASCADE)
    nb_vehicules_accompagnement = models.PositiveIntegerField("nombre de véhicules d'accompagnement", blank=True, null=True,
                                                              help_text="nombre de véhicules d'accompagnement")
    nb_personnes_pts_rassemblement = models.PositiveSmallIntegerField("nombre de personnes aux points de rassemblement",
                                                                      blank=True, null=True)

    def get_delai_legal(self):
        """ Renvoyer le délai légal pour l'obtention d'une autorisation """
        if self.departements_traverses.count() < 20:
            return self.delaiDepot
        else:
            return self.delaiDepot_ndept

    # Meta
    class Meta:
        verbose_name = "manifestation avec véhicules terrestres à moteur soumise à déclaration"
        verbose_name_plural = "manifestations avec véhicules terrestres à moteur soumises à déclaration"
        default_related_name = "dvtm"
        app_label = "evenements"


class Avtm(AbstractVtm, Manif):
    """ Manifestation avec véhicule motorisé sousmise à autorisation """

    # Constantes
    refCerfa = "cerfa_15847-01"
    consultFederation = False
    consultServices = AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation obligatoire']
    delaiDepot = 90
    # delaiDepot_ndept = defaut = 90
    # delaiDocComplement1 = defaut = 21
    # delaiDocComplement2 = defaut = 6
    LISTE_CHAMPS_ETAPE_0 = AbstractInstructionConfig.LISTE_CHAMPS_ETAPE_0 + ['nb_vehicules_accompagnement', 'vehicules']
    LISTE_FICHIERS = AbstractInstructionConfig.LISTE_FICHIERS + ['itineraire_horaire', 'avis_federation_delegataire', 'carte_zone_public',
                                                                 'participants', 'plan_masse', 'presence_docteur', 'presence_secouriste',
                                                                 'presence_ambulance', 'dossier_securite']
    LISTE_FICHIERS_ETAPE_0 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_0 + ['itineraire_horaire', 'carte_zone_public', 'dossier_securite']
    LISTE_FICHIERS_ETAPE_1 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_1 + ['avis_federation_delegataire']
    LISTE_FICHIERS_ETAPE_2 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_2 + ['participants']

    # Champs
    manifestation_ptr = models.OneToOneField('evenements.Manif', parent_link=True, related_name='avtm', on_delete=models.CASCADE)
    nb_vehicules_accompagnement = models.PositiveIntegerField("nombre de véhicules d'accompagnement", blank=True, null=True,
                                                              help_text="nombre de véhicules d'accompagnement")

    # Meta
    class Meta:
        verbose_name = "manifestation avec véhicules terrestres à moteur soumise à autorisation"
        verbose_name_plural = "manifestations avec véhicules terrestres à moteur soumises à autorisation"
        default_related_name = "avtm"
        app_label = "evenements"


class Dvtmcir(AbstractVtm, Manif):
    """ Manifestation avec véhicule motorisé """
    # Modèle non utilisé pour le moment avec les décrets 2017

    # Constantes
    refCerfa = "cerfa_15862-01"
    consultFederation = True
    consultServices = AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation optionnelle']
    # delaiDepot = 60
    # delaiDepot_ndept = defaut = 90
    # delaiDocComplement1 = defaut = 21
    # delaiDocComplement2 = defaut = 6
    LISTE_CHAMPS_ETAPE_0 = AbstractInstructionConfig.LISTE_CHAMPS_ETAPE_0
    LISTE_FICHIERS = AbstractInstructionConfig.LISTE_FICHIERS + ['liste_dates_manifestations', 'presence_docteur', 'presence_secouriste',
                                                                 'presence_ambulance']
    LISTE_FICHIERS_ETAPE_0 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_0[:-1]
    LISTE_FICHIERS_ETAPE_2 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_2

    # Champs
    manifestation_ptr = models.OneToOneField('evenements.Manif', parent_link=True, related_name='dvtmcir', on_delete=models.CASCADE)
    avec_convention_fede = models.BooleanField("Vous avez une convention avec une fédération agréée", default=False)
    avec_convention_fede_confirme = models.BooleanField("Confirmation de convention avec une fédération agréée", null=True, default=None)

    # Méta
    class Meta:
        verbose_name = "manifestations se déroulant sur un circuit permanent homologué"
        verbose_name_plural = "courses avec véhicules terrestres à moteur"
        default_related_name = "dvtmcir"
        app_label = "evenements"
