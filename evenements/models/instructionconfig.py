from django.db import models


class AbstractInstructionConfig(models.Model):
    """ Configuration des cerfas """

    LISTE_MODE_CONSULT = {"Sans consultation": 0, "Consultation optionnelle": 1, "Consultation obligatoire": 2}
    # Il s'agit des champs et des fichiers requis pendant l'étape pour passer à l'étape suivante
    LISTE_CHAMPS_ETAPE_0 = ['nom', 'date_debut', 'date_fin', 'description', 'nb_spectateurs', 'nb_organisateurs',
                            'nom_contact', 'prenom_contact', 'tel_contact']
    LISTE_FICHIERS = ['reglement_manifestation', 'disposition_securite', 'certificat_assurance', 'charte_dispense_site_n2k',
                      'convention_police', 'visa_federation', 'convention_federation', 'charte_balisage']
    LISTE_FICHIERS_ETAPE_0 = ['reglement_manifestation', 'disposition_securite']
    LISTE_FICHIERS_ETAPE_1 = []
    LISTE_FICHIERS_ETAPE_2 = ['certificat_assurance', ]

    refCerfa = ""
    consultFederation = True
    modeConsultServices = 0
    delaiDepot = 60  # Délai pour le dépôt de la demande de manifestation
    delaiDepot_ndept = 90  # Délai pour le dépôt de la demande de manifestation sur plusieurs départements
    delaiDocComplement1 = 21  # Délai #1 pour le dépôt de documents requis pour la demande de manifestation
    delaiDocComplement2 = 6  # Délai #2 pour le dépôt de documents requis pour la demande de manifestation

    def get_liste_champs_etape_0(self):
        """
        Retourne la liste des champs obligatoires à l'étape 0 de l'instruction,
        c'est à dire pour être envoyé aux instructeurs
        """

        # Simplifié par la suppression du choix openrunner
        return self.LISTE_CHAMPS_ETAPE_0

    def get_liste_fichier_etape_0(self):
        """
        Retourne la liste des fichiers obligatoires à l'étape 0 de l'instruction,
        c'est à dire pour être envoyé aux instructeurs
        """

        # Simplifié par la suppression du choix openrunner
        liste = self.LISTE_FICHIERS_ETAPE_0[:]
        if self.signature_charte_dispense_site_n2k:
            liste.append('charte_dispense_site_n2k')

        if hasattr(self.cerfa, 'police_nationale') and self.cerfa.police_nationale:
            liste.append('convention_police')

        if self.dans_calendrier and self.manifestation_ptr.get_type_manif() in ['avtm', 'dvtmcir', 'dcnm', 'dcnmc']:
            liste.append('visa_federation')

        if self.manifestation_ptr.get_type_manif() in ['dvtmcir', 'dcnm', 'dcnmc'] and self.avec_convention_fede:
            liste.append('convention_federation')

        if self.convention_balisage:
            liste.append('charte_balisage')

        if self.circuit:
            liste.append('plan_masse')

        return liste

    def get_liste_champs_a_modifier(self):
        """
        Retourne la liste des champs à modifier pendant l'instruction,
        c'est à dire les champs demandés par les instructeurs
        """
        liste_champ_modif = []
        if self.modif_json:
            for line in self.modif_json:
                if not line['done'] and 'manif' in line:
                    liste_champ_modif.append(('manif', line['manif'], line['date']))
                if not line['done'] and 'champ_n2k' in line:
                    liste_champ_modif.append(('n2k', line['champ_n2k'], line['date']))
                if not line['done'] and 'champ_rnr' in line:
                    liste_champ_modif.append(('rnr', line['champ_rnr'], line['date']))
        return liste_champ_modif

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.manifestation_ptr.creer_pj_cerfa()

    # Meta
    class Meta:
        abstract = True
