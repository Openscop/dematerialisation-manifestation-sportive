# coding: utf-8
from django.db import models
from django.utils import timezone

from configuration.directory import UploadPath
from evenements.models.manifestation import Manif
from messagerie.models.message import Message
from core.FileTypevalidator import file_type_validator


class PieceJointe(models.Model):
    manif = models.ForeignKey(Manif, on_delete=models.CASCADE, related_name='piece_jointe')
    nom = models.CharField(max_length=100, verbose_name='nom de la pièce jointe')
    champ_cerfa = models.CharField(max_length=100, null=True, blank=True, verbose_name="Champ du cerfa")
    info = models.TextField(blank=True, verbose_name="Aide")
    validation_instructeur = models.BooleanField(default=False, verbose_name='Validé par l\'instructeur')
    date_demande = models.DateTimeField(null=True, blank=True, verbose_name="Date de demande")
    date_limite = models.DateTimeField(null=True, blank=True, verbose_name="Date limite d'envoi")
    date_televersement = models.DateTimeField(null=True, blank=True, verbose_name="Date de dépot de la pièce jointe")
    scan_antivirus = models.BooleanField(default=True, verbose_name="Scan antivirus effectué")

    document_attache = models.FileField(upload_to=UploadPath(""), blank=True, null=True, verbose_name="Fichier",
                                        max_length=512, validators=[file_type_validator])

    def __str__(self):
        return self.nom

    def save(self, *args, **kwargs):
        if self.pk and self.document_attache:
            pj_old = PieceJointe.objects.get(pk=self.pk)
            pareil = self.document_attache == pj_old.document_attache
            if not pj_old.document_attache and not pareil:
                self.date_televersement = timezone.now()
            if pj_old.document_attache and not pareil:
                self.date_televersement = timezone.now()
                if self.manif.get_instruction():
                    # sauvegarde de l'ancienne pj
                    historique_pj = PieceJointeHistorique(pj=self, date_televersement=pj_old.date_televersement,
                                                          document_attache=pj_old.document_attache)
                    historique_pj.save()
        super().save(*args, **kwargs)

    def demande_pj(self, user, organisation):
        """ Notifier la demande de document """
        titre = "Pièce jointe complémentaire requise"
        contenu = "<p>" + titre + ' pour la manifestation ' + self.manif.nom +\
                  " concernant la demande :</p><p><em>" + self.nom + \
                  "</em></p><p>(Veuillez déposer votre document dans l'onglet 'Pièces jointes'.)</p>"
        Message.objects.creer_et_envoyer('action', [user, organisation],
                                         self.manif.structure_organisatrice_fk.get_utilisateurs_avec_service(),
                                         titre, contenu,
                                         manifestation_liee=self.manif,
                                         objet_lie_nature="piece_jointe", objet_lie_pk=self.pk)

    def notifier_doc_fourni(self):
        """ Notifier la provision de document uniquement si l'instruction est en cours"""
        expediteur = self.manif.get_organisateur()
        destinatairesaction = []
        destinataires = []
        destinataires += self.manif.get_lecture_user()
        for instruction in self.manif.instruction.all():
            destinatairesaction += instruction.get_instructeurs_avec_services()
            destinataires += instruction.get_tous_agents_avec_service()
        titre = "Pièces jointes envoyés par l'organisateur"
        contenu = "<p>" + titre + ' pour la manifestation ' + self.manif.nom + \
                  " concernant la pièce :</p><p><em>" + self.nom + "</em></p>"
        Message.objects.creer_et_envoyer('info_suivi', [expediteur, self.manif.structure_organisatrice_fk], destinataires, titre, contenu,
                                         manifestation_liee=self.manif,
                                         objet_lie_nature="piece_jointe", objet_lie_pk=self.pk)
        Message.objects.creer_et_envoyer('action', [expediteur, self.manif.structure_organisatrice_fk], destinatairesaction, titre, contenu,
                                         manifestation_liee=self.manif,
                                         objet_lie_nature="piece_jointe", objet_lie_pk=self.pk)

    def requise(self):
        # savoiir si une piece jointe est requise
        etape = self.manif.etape_en_cours()
        if self.champ_cerfa and not self.document_attache:
            if etape == "etape 0":
                for liste in self.manif.cerfa.get_liste_fichier_etape_0():
                    if liste in self.champ_cerfa:
                        return True
            elif etape == "etape 1":
                for liste in self.manif.cerfa.LISTE_FICHIERS_ETAPE_1:
                    if liste in self.champ_cerfa:
                        return True
            elif etape in ["etape 2", "etape 3"]:
                for liste in self.manif.cerfa.LISTE_FICHIERS_ETAPE_1 + self.manif.cerfa.LISTE_FICHIERS_ETAPE_2:
                    if liste in self.champ_cerfa:
                        return True
        return False

    def retard(self):
        # savoir si une piece jointe ets en retard
        if self.champ_cerfa and not self.document_attache:
            delta = (self.date_limite - timezone.now()).days if self.date_limite else None
            if delta and delta < 0:
                return True
        return False

    def presque_retard(self):
        # savoir si une ipièce jointe doit etre rendue dans 7 jours
        if self.champ_cerfa and not self.document_attache:
            delta = (self.date_limite - timezone.now()).days if self.date_limite else None
            if delta and not self.retard() and delta < 7:
                return True
        return False

    def delta(self):
        # avoir le delta du restart ou de la date limite
        delta = (self.date_limite - timezone.now()).days if self.date_limite else None
        if self.retard():
            delta = delta*-1
        else:
            delta += 1
        return delta

    def a_modifier(self):
        # savoir si la pj a été invalidé par l'instructeur
        modif = False
        if self.manif.modif_json:
            for line in self.manif.modif_json:
                if 'pj' in line and line['pj'] == self.pk and not line['done']:
                    modif = timezone.make_aware(timezone.datetime.strptime(line['date'], "%d/%m/%Y %H:%M:%S"), timezone.utc).astimezone().strftime("%d/%m/%Y à %H:%M")
        return modif

    def nature(self):
        # Savoir quelle étiquette donner à la PJ
        if self.champ_cerfa:
            if "demande_instructeur" in self.champ_cerfa:
                return "Demande"
            if self.champ_cerfa not in ["charte_balisage"]:
                return "Cerfa"
        return "Complément"


class PieceJointeHistorique(models.Model):
    """
    Modele pour stocker les historique de pièce jointe
    """
    pj = models.ForeignKey(PieceJointe, models.CASCADE, related_name="historique", verbose_name="Pièce jointe")
    date_televersement = models.DateTimeField(null=True, blank=True, verbose_name="Date de dépot de la pièce jointe")
    document_attache = models.FileField(upload_to=UploadPath(""), blank=True, null=True, verbose_name="Fichier",
                                        max_length=512, validators=[file_type_validator])
