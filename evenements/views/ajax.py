# coding: utf-8
import copy, shutil, os

from django.views.generic.detail import SingleObjectMixin
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views.generic import View
from django.views.generic.edit import UpdateView
from django.conf import settings
from django.db.models import Q

from core.models import Instance
from evenements.decorators import verifier_acces_evenement
from instructions.decorators import verifier_secteur_instruction
from ..forms.manif import ManifCloneForm, ManifDepotForm
from ..models import Manif
from messagerie.models import Message
from structure.models import Organisation


@method_decorator(verifier_acces_evenement(acces=True), name='dispatch')
class ManifDepotAjax(UpdateView):
    """ Autoriser le dépôt du dossier hors délai """
    model = Manif
    form_class = ManifDepotForm
    template_name = 'evenements/forms/evenement_depot_form.html'

    def form_valid(self, form):
        request = self.request
        self.object = form.save(commit=False)
        raison = form.cleaned_data['motif']
        raison_json = {'date': timezone.now().strftime('%d/%m/%Y %H:%M:%S'),
                       'user_origine': self.request.user.pk,
                       'service_origine': self.request.organisation.pk, 'motif': raison}
        self.object.autorisation_hors_delai_json = raison_json
        titre = "Autorisation de dépôt hors délai"
        message = f"L'autorisation de dépôt hors délai pour le dossier {self.object}, vient d'être accordée."
        destinataires = self.object.structure_organisatrice_fk.get_utilisateurs_avec_service()
        destinataires += self.object.get_lecture_user()
        Message.objects.creer_et_envoyer('info_suivi', [request.user, request.organisation], destinataires, titre,
                                         message, manifestation_liee=self.object, objet_lie_nature="dossier")
        Message.objects.creer_et_envoyer('tracabilite', None, [request.user, request.organisation],  titre, message)
        return super().form_valid(form)

    def get_success_url(self):
        return self.object.get_cerfa().get_absolute_url()


@method_decorator(verifier_acces_evenement, name='dispatch')
class ManifClonageAjax(UpdateView):
    """ Clonage de manifestation """
    model = Manif
    form_class = ManifCloneForm
    template_name = 'evenements/evenement_clone_form.html'

    def form_valid(self, form):
        manif = self.get_object()
        # Copie du cerfa et modification des données
        new_cerfa = copy.deepcopy(manif.cerfa)
        new_cerfa.nom = form.cleaned_data['nom']
        new_cerfa.date_debut = form.cleaned_data['date_debut']
        new_cerfa.date_fin = form.cleaned_data['date_fin']
        # Suppression du pk et id pour créer une nouvelle manifestation
        new_cerfa.id = None
        new_cerfa.pk = None
        new_cerfa.history_json = []
        new_cerfa.modif_json = []
        if hasattr(new_cerfa, "dans_calendrier_confirme"):
            new_cerfa.dans_calendrier_confirme = None
        if hasattr(new_cerfa, "avec_convention_fede_confirme"):
            new_cerfa.avec_convention_fede_confirme = None
        new_cerfa.save()
        for parcours in manif.parcours.all():
            ancien_parcours = parcours
            new_shape = copy.deepcopy(parcours.shape)
            new_shape.pk = None
            new_shape.save()
            new_parcours = copy.deepcopy(parcours)
            new_parcours.pk = None
            new_parcours.shape = new_shape
            new_parcours.manif = new_cerfa
            new_parcours.save()
            new_parcours.refresh_from_db()
            new_line_waypoint = copy.deepcopy(parcours.shape.line_waypoint)
            new_line_waypoint.pk = None
            new_line_waypoint.shape = new_shape
            new_line_waypoint.save()
            if hasattr(parcours.shape, "line_shape") and parcours.shape.line_shape:
                new_line_shape = copy.deepcopy(parcours.shape.line_shape)
                new_line_shape.pk = None
                new_line_shape.shape = new_shape
                new_line_shape.save()
            if hasattr(parcours.shape, "line_shape_reduced") and parcours.shape.line_shape_reduced:
                new_line_shape_reduced = copy.deepcopy(parcours.shape.line_shape_reduced)
                new_line_shape_reduced.pk = None
                new_line_shape_reduced.shape = new_shape
                new_line_shape_reduced.save()
            for poi in ancien_parcours.pois.all():
                new_poi = copy.deepcopy(poi)
                new_poi.pk = None
                new_poi.parcours = new_parcours
                new_poi.save()
            for zone in ancien_parcours.zone.all():
                new_zone = copy.deepcopy(zone)
                new_zone.pk = None
                new_zone.parcours = new_parcours
                new_zone.save()
        # Gestion des m2m
        [new_cerfa.villes_traversees.add(ville) for ville in manif.villes_traversees.all()]
        [new_cerfa.departements_traverses.add(departement) for departement in manif.departements_traverses.all()]
        new_cerfa.instance_instruites.add(new_cerfa.instance)
        for dep in new_cerfa.departements_traverses.all():
            if Instance.objects.filter(departement=dep, etat_s="ouvert"):
                new_cerfa.instance_instruites.add(Instance.objects.get(departement=dep))
        [new_cerfa.ville_depart_interdep_mtm.add(ville) for ville in manif.ville_depart_interdep_mtm.all()]
        [new_cerfa.sites_natura2000.add(site) for site in manif.sites_natura2000.all()]
        [new_cerfa.lieux_pdesi.add(lieu) for lieu in manif.lieux_pdesi.all()]
        [new_cerfa.zones_rnr.add(zone) for zone in manif.zones_rnr.all()]
        # Gestion des pièces jointes
        path = settings.MEDIA_ROOT + new_cerfa.get_departement_depart_nom() + '/'
        if os.path.isdir(path + str(manif.cerfa.pk)):
            # Un dossier de PJs existe, copie du dossier
            shutil.copytree(path + str(manif.cerfa.pk), path + str(new_cerfa.pk))
            for pj in manif.piece_jointe.filter(Q(champ_cerfa__isnull=False) & ~Q(champ_cerfa__startswith="demande_instructeur")):
                # Verification de l'existance de la pj cerfa qui a pu être déclaré obsolète d'une manif à l'autre
                if pj.document_attache and new_cerfa.manifestation_ptr.piece_jointe.filter(champ_cerfa=pj.champ_cerfa):
                    new_pj = new_cerfa.manifestation_ptr.piece_jointe.get(champ_cerfa=pj.champ_cerfa)
                    # Si le champ est rempli, modification du chemin
                    path_list = pj.document_attache.name.split('/')
                    path_list[1] = str(new_cerfa.pk)
                    setattr(new_pj, 'document_attache', '/'.join(path_list))
                    new_pj.save()
        if new_cerfa.emprise == 0:
            new_cerfa.emprise = 1
        new_cerfa.save()
        # envoi des messages de traçabilité
        # traçabilité sur l'ancienne manif
        Message.objects.creer_et_envoyer('tracabilite', None, [self.request.user, self.request.organisation],
                                         'Dossier cloné',
                                         'Ce dossier a été cloné vers la manifestation <em>' + new_cerfa.nom +
                                         '</em> (' + str(new_cerfa.pk) + ')',
                                         manifestation_liee=manif)
        # traçabilité sur la nouvelle manif
        Message.objects.creer_et_envoyer('tracabilite', None, [self.request.user, self.request.organisation],
                                         'Dossier cloné',
                                         'Ce dossier a été créé par un clonage de la manifestation <em>' + manif.nom +
                                         '</em> (' + str(manif.pk) + ')',
                                         manifestation_liee=new_cerfa)
        return HttpResponse(new_cerfa.get_absolute_url())

    def form_invalid(self, form):
        reponse = super().form_invalid(form)
        reponse.status_code = 400
        return render(self.request, 'evenements/evenement_clone_form.html', {'form': form}, status=400)


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class ManifInstructeurDemandeModifAjax(SingleObjectMixin, View):
    """ Vue instructeur pour marquer le champ d'une manif à modifier """

    model = Manif

    # Marquer le champ à modifier une pièce jointe
    def get(self, request, *args, **kwargs):
        manif = self.get_object()
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        champ = request.GET.get('champ')
        cle_champ = request.GET.get('form', 'manif')
        annule = request.GET.get('annule')
        if not manif.modif_json:
            manif.modif_json = []
        if not annule:
            manif.modif_json.append(
                {'date': timezone.now().strftime('%d/%m/%Y %H:%M:%S'), cle_champ: champ, 'done': False})
            manif.notifier_manif_a_modifier(request.user, organisation, cle_champ, champ)
        else:
            for idx, line in enumerate(manif.modif_json):
                if not line['done'] and cle_champ in line and line[cle_champ] == champ:
                    manif.notifier_annulation_modification(request.user, organisation, cle_champ, champ)
                    manif.modif_json.pop(idx)
                    break

        manif.save()
        manif.get_instruction(instance=organisation.instance_fk).referent = request.user
        manif.get_instruction(instance=organisation.instance_fk).save()
        return HttpResponse('ok')


@method_decorator(verifier_acces_evenement, name='dispatch')
class GetParcoursElementView(SingleObjectMixin, View):
    """Retourne un tableau des pk des elements traversées selon les parcours d'une manifestation"""
    model = Manif

    def get(self, request, *args, **kwargs):

        manif = self.get_object()
        request_object = request.GET.get('object', None)
        if request_object == "commune":
            communes, communes_initial = [], []
            for commune in manif.get_commune_traverse_parcours():
                communes.append(str(commune.pk))
            for commune in manif.villes_traversees.all():
                communes_initial.append(str(commune.pk))
            return JsonResponse([communes, communes_initial], safe=False)

        if request_object == "dep":
            deps, deps_initial = [], []
            for dep in manif.get_departement_traverse_parcours():
                deps.append(str(dep.pk))
            for dep in manif.departements_traverses.all():
                deps_initial.append(str(dep.pk))
            return JsonResponse([deps, deps_initial], safe=False)

        if request_object == "n2k":
            n2ks, n2ks_initial = [], []
            for n2k in manif.get_natura_traverse_parcours():
                n2ks.append(str(n2k.pk))
            for n2k in manif.sites_natura2000.all():
                n2ks_initial.append(str(n2k.pk))
            return JsonResponse([n2ks, n2ks_initial], safe=False)

        if request_object == "rnr":
            rnrs, rnrs_initial = [], []
            for rnr in manif.zones_rnr.all():
                rnrs_initial.append(str(rnr.pk))
            for rnr in manif.get_rnr_traverse_parcours():
                rnrs.append(str(rnr.pk))
            return JsonResponse([rnrs, rnrs_initial], safe=False)

        return JsonResponse([], safe=False)
