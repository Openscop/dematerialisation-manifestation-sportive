# coding: utf-8
from .manif import ManifDetail, ManifCreate, ManifUpdate, ManifDelete
from ..forms.dnm import DcnmForm
from ..models.dnm import Dcnm


class DcnmDetail(ManifDetail):
    """ Détail de manifestation """

    # Configuration
    model = Dcnm


class DcnmCreate(ManifCreate):
    """ Création de manifestation """

    # Configuration
    model = Dcnm
    form_class = DcnmForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dcnm'
        return context


class DcnmUpdate(ManifUpdate):
    """ Modifier une manifestation """

    # Configuration
    model = Dcnm
    form_class = DcnmForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dcnm'
        return context


class DcnmDelete(ManifDelete):
    """ Supprimer une manifestation """

    # Configuration
    model = Dcnm