from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views.generic import View
from django.conf import settings

from evenements.decorators import verifier_acces_evenement
from instructions.decorators import verifier_secteur_instruction
from ..models import Manif, PieceJointe
from instructions.models import Instruction
from administrative_division.models import Departement
from core.tasks import gestion_fichier
from core.FileTypevalidator import file_type_valide_non_form
from messagerie.models import Message
from structure.models.organisation import Organisation

@method_decorator(verifier_acces_evenement(), 'dispatch')
class OrganisateurPieceJointeView(View):
    """
    Vue pour afficher en envoyer les piece jointe coté organisteur
    """

    def get(self, request, pk):
        manif = get_object_or_404(Manif, pk=pk)
        pj = manif.get_pj()
        now = timezone.now()
        context = {
            "manif": manif,
            "piecejointes_cerfa": pj,
            "now": now
        }
        return render(request, 'evenements/piecejointeform.html', context=context)

    def post(self, request, pk):
        manif = get_object_or_404(Manif, pk=pk)
        # Ajout d'un fichier à une piece dejà existante
        pk_pj = request.GET.get('pj')
        effacer = request.POST.get('effacer')
        if pk_pj and pk_pj.isdecimal():
            if not manif.piece_jointe.filter(pk=pk_pj).exists():
                return HttpResponse("La pièce jointe ne fait pas partie du dossier")
            piece = manif.piece_jointe.get(pk=pk_pj)
            # verification de l'extension du fichier
            if not effacer:
                if not request.FILES.get('fichier'):
                    return HttpResponse("pas de fichier sélectionné")
                if not file_type_valide_non_form(request.FILES.get('fichier')):
                    return HttpResponse("Format de fichier incorrect")
                if piece.validation_instructeur:
                    return HttpResponse("Pièce verrouillée par l'instructeur")
                piece.document_attache = request.FILES.get('fichier')
                piece.date_televersement = timezone.now()
                # si celery n'est pas actif le scan antivirus ne peut être fait
                if settings.CELERY_ENABLED:
                    piece.scan_antivirus = False
                    piece.save()
                    gestion_fichier.delay(manif.pk, piece.pk)
                if manif.get_instruction():
                    # Marquer la line de demande de modification DONE si elle existe
                    if manif.modif_json:
                        for idx, line in enumerate(manif.modif_json):
                            if 'pj' in line and line['pj'] == piece.pk and not line['done']:
                                manif.modif_json[idx]['done'] = True
                                manif.save()
                                break
                    piece.notifier_doc_fourni()
            else:
                piece.document_attache.name = ""
                piece.date_televersement = None
            piece.validation_instructeur = False
            piece.save()
            return HttpResponse('ok')

        # ajout d'une piece complémentaire
        if request.GET.get('ajout', None):
            erreur = None
            if (not request.POST.get('nom') or not request.POST.get('aide') or
                    not request.FILES.get('fichier') or request.POST.getlist('portee') == []):
                erreur = "Merci de remplir la totalité du formulaire"
            elif not file_type_valide_non_form(request.FILES.get('fichier')):
                erreur = 'Format de fichier incorrect'
            if erreur:
                return HttpResponse(erreur)
            nom = request.POST['nom']
            choix_dep = request.POST.getlist('portee')
            dep_qs = Departement.objects.filter(pk__in=[dep.pk for dep in manif.get_departements_traverses()])
            if not any([True if choix == "0" else False for choix in choix_dep]):
                if len(choix_dep) == 1:
                    if dep_qs.filter(pk=request.POST.get('portee')).exists():
                        nom += f" pour le département {dep_qs.filter(pk=request.POST.get('portee')).get()}"
                else:
                    nom += " pour les départements"
                    for idx, dep in enumerate(choix_dep):
                        if dep_qs.filter(pk=dep).exists():
                            nom += f" {dep_qs.filter(pk=dep).get().name}"
                            if idx + 1 < len(choix_dep):
                                nom += " -"

            piece = PieceJointe(manif=manif, nom=nom, info=request.POST['aide'],
                                document_attache=request.FILES['fichier'])
            # si celery n'est pas actif le scan antivirus ne peut être fait
            if settings.CELERY_ENABLED:
                piece.scan_antivirus = False
                piece.save()
                gestion_fichier.delay(manif.pk, piece.pk)
            piece.validation_instructeur = False
            piece.date_televersement = timezone.now()
            piece.save()
            if manif.get_instruction():
                piece.notifier_doc_fourni()
            return HttpResponse('ok')


@method_decorator(verifier_secteur_instruction(), 'dispatch')
class ValidationInstructeur(View):
    """
    Class pour valider ou non une  pièce jointe
    """
    def post(self, request, pk):
        instruction = get_object_or_404(Instruction, pk=pk)
        pk_pj = request.GET.get('pk')
        if pk_pj and pk_pj.isdecimal():
            if not instruction.manif.piece_jointe.filter(pk=pk_pj).exists():
                return HttpResponse("La pièce jointe ne fait pas partie du dossier")
        pj = instruction.manif.piece_jointe.get(pk=pk_pj)
        if request.GET.get('valider') == "1":
            pj.validation_instructeur = True
            autre_instructeurs = []
            [autre_instructeurs.append(instru.referent) if instru.referent else "None" for instru in instruction.manif.instruction.exclude(pk=instruction.pk)]
            if autre_instructeurs:
                Message.objects.creer_et_envoyer('tracabilite', [self.request.user, self.request.organisation],
                                                 autre_instructeurs,
                                                 'Pièce jointe validée',
                                                 f"Une pièce jointe a été validée par l'instructeur : {self.request.user.get_full_name()}",
                                                 manifestation_liee=instruction.manif, objet_lie_nature="piece_jointe")
        elif request.GET.get('revalider') == "1":
            for idx, line in enumerate(pj.manif.modif_json):
                if not line['done'] and 'pj' in line and line['pj'] == pj.pk:
                    pj.manif.modif_json.pop(idx)
                    Message.objects.creer_et_envoyer('info_suivi', [self.request.user, self.request.organisation],
                                                     instruction.manif.structure_organisatrice_fk.get_utilisateurs_avec_service(),
                                                     'Pièce jointe reverouillée',
                                                     'Une pièce jointe a été reverouillée par l\'instructeur',
                                                     manifestation_liee=instruction.manif,
                                                     objet_lie_nature="piece_jointe")
                    break
            pj.manif.save()
        elif request.GET.get('valider') == "0":
            pj.validation_instructeur = False
            # Ajouter une ligne de demande de modification
            if not instruction.manif.modif_json:
                instruction.manif.modif_json = []
            instruction.manif.modif_json.append(
                {'date': timezone.now().strftime('%d/%m/%Y %H:%M:%S'), 'pj': pj.pk, 'done': False})
            instruction.manif.save()
            Message.objects.creer_et_envoyer('info_suivi', [self.request.user, self.request.organisation],
                                             instruction.manif.structure_organisatrice_fk.get_utilisateurs_avec_service(),
                                             "Pièce jointe invalidée > Demande de modification de dossier",
                                             f"<p>L'instructeur sollicite une modification de la pièce jointe {pj.nom} sur votre dossier de manifestation {pj.manif.nom}</p>" + \
                                             f"Attention : vous ne pourrez modifier l'information qu'une seule fois.</p>",
                                             manifestation_liee=instruction.manif, objet_lie_nature="piece_jointe")
        pj.save()

        tous_verif = True
        for pj in instruction.manif.get_pj(request):
            if pj.document_attache and not pj.validation_instructeur:
                tous_verif = False
                break
        if tous_verif:
            instruction.doc_verif = True
            Message.objects.creer_et_envoyer('info_suivi', [self.request.user, self.request.organisation],
                                             instruction.manif.structure_organisatrice_fk.get_utilisateurs_avec_service(),
                                             'Pièces jointes validées', 'Vos pièces jointes ont été validées par l\'instructeur',
                                             manifestation_liee=instruction.manif, objet_lie_nature="piece_jointe")
        else:
            instruction.doc_verif = False
        instruction.referent = request.user
        instruction.save()

        return HttpResponse('ok')


@method_decorator(verifier_secteur_instruction(), 'dispatch')
class DemandePieceComplementaireInstructeur(View):
    """
    Class pour que l'instructeur demande une pièce complémentaire
    """

    def post(self, request, pk):
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        instruction = get_object_or_404(Instruction, pk=pk)
        if not request.POST.get('nom') or not request.POST.get('aide') or not request.POST.get('date'):
            return HttpResponse('formulaire incomplet')
        portee = request.POST.get('portee')
        titre_fichier = request.POST['nom']
        nom_fichier = "demande_instructeur"
        if portee:
            dep = instruction.get_instance().departement
            titre_fichier += f" pour le département {str(dep)}"
            nom_fichier += f"_{str(dep.pk)}"
            
        pj = PieceJointe(manif=instruction.manif, nom=titre_fichier, info=request.POST['aide'],
                         champ_cerfa=nom_fichier, date_limite=request.POST['date'],
                         date_demande=timezone.now())
        pj .save()
        pj.demande_pj(request.user, organisation)
        instruction.referent = request.user
        instruction.save()
        return HttpResponse('ok')
