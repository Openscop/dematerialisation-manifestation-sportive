# coding: utf-8
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views.generic import View
from django.shortcuts import render, redirect
from django.contrib.contenttypes.models import ContentType

from core.util.permissions import require_type_organisation
from evenements.models import Manif
from instructions.models import AutorisationAcces
from core.util.permissions import login_requis
from structure.models.organisation import StructureOrganisatrice, Organisation


class TableauOrganisateurBody(View):
    @method_decorator(require_type_organisation([StructureOrganisatrice,]))
    def dispatch(self, *args, **kwargs):
        """ Protéger la vue pour un accès par les organisateurs uniquement """
        return super().dispatch(*args, **kwargs)

    def get(self, request, pk=None):
        temporalite = self.request.GET.get('temporalite', default='')
        annee = self.request.GET.get('annee', None)
        if not pk:
            if request.user.organisation_m2m.all().count() > 0:
                pk = request.user.organisation_m2m.first().pk
                return redirect('evenements:tableau-de-bord-organisateur', pk=pk)
            else:
                return redirect('profile')
        user_structure = StructureOrganisatrice.objects.get(pk=pk)
        manifs = Manif.objects.filter(structure_organisatrice_fk=user_structure).order_by('-pk')
        if temporalite == "archive":
            manifs = manifs.filter(date_fin__lt=timezone.now())
            liste_annee_archives = []
            [liste_annee_archives.append(str(manif.date_fin.year)) if str(
                manif.date_fin.year) not in liste_annee_archives else "" for manif in manifs]
            liste_annee_archives.sort()
            if annee:
                manifs = manifs.filter(date_fin__year=annee)
        else:
            liste_annee_archives = []
            manifs = manifs.filter(date_fin__gte=timezone.now())

        non_envoye = 0
        en_cours = 0
        autorise = 0
        multiple = 0
        interdite = 0
        annule = 0
        for manif in manifs:
            if manif.get_cerfa().get_instruction():
                if manif.get_etat() == "interdite":
                    interdite += 1
                elif manif.get_etat() == "autorisée":
                    autorise += 1
                elif manif.get_etat() == "annulée":
                    annule += 1
                elif manif.get_etat() == "multiple":
                    multiple += 1
                else:
                    en_cours += 1
            else:
                non_envoye += 1
        # service = self.request.user.organisation_m2m
        consult = None
        if user_structure:
            consult = AutorisationAcces.objects.filter(organisation=user_structure, date_revocation__isnull=True,
                                                       manif__date_fin__gte=timezone.now()).count()
            if temporalite == "archive":
                consult = AutorisationAcces.objects.filter(organisation=user_structure, date_revocation__isnull=True,
                                                           manif__date_fin__lt=timezone.now()).count()
                if annee:
                    consult = AutorisationAcces.objects.filter(organisation=user_structure, date_revocation__isnull=True,
                                                               manif__date_fin__lt=timezone.now(), manif__date_fin__year=annee).count()
        context = {
            "non_envoye": non_envoye,
            "en_cours": en_cours,
            "autorise": autorise,
            "multiple": multiple,
            "interdite": interdite,
            "annule": annule,
            "consultation": consult,
            "nb_dossiers": manifs.count(),
            "archive": temporalite,
            "liste_annee_archives": liste_annee_archives,
            "annee": annee,
            'structure': user_structure
        }
        return render(request, 'evenements/dashboard_organisateur_body.html', context)


class TableauOrganisateurListe(View):

    @method_decorator(require_type_organisation([StructureOrganisatrice,]))
    def dispatch(self, *args, **kwargs):
        """ Protéger la vue pour un accès par les organisateurs uniquement """
        return super().dispatch(*args, **kwargs)

    def get(self, request, pk):
        temporalite = self.request.GET.get('temporalite', default='')
        annee = self.request.GET.get('annee', None)
        user_structure = StructureOrganisatrice.objects.get(pk=pk)
        manifs = Manif.objects.filter(structure_organisatrice_fk=user_structure).order_by('-pk')
        if temporalite == "archive":
            manifs = manifs.filter(date_fin__lt=timezone.now())
            if annee:
                manifs = manifs.filter(date_fin__year=annee)
        else:
            manifs = manifs.filter(date_fin__gte=timezone.now())
        context = {
            "manifs": manifs,
        }
        if request.GET.get('filtre_etat', None):
            trimanif = []
            for manif in manifs:
                if request.GET['filtre_etat'] == "attente":
                    if not manif.get_cerfa().get_instruction():
                        trimanif.append(manif)
                elif request.GET['filtre_etat'] == "demande":
                    if manif.get_cerfa().get_instruction() and manif.get_etat() in ["demandée", "distribuée"]:
                        trimanif.append(manif)
                elif request.GET['filtre_etat'] == "autorise":
                    if manif.get_cerfa().get_instruction() and manif.get_etat() == "autorisée":
                        trimanif.append(manif)
                elif request.GET['filtre_etat'] == "interdite":
                    if manif.get_cerfa().get_instruction() and manif.get_etat() == "interdite":
                        trimanif.append(manif)
                elif request.GET['filtre_etat'] == "multiple":
                    if manif.get_cerfa().get_instruction() and manif.get_etat() == "multiple":
                        trimanif.append(manif)
                elif request.GET['filtre_etat'] == "annule":
                    if manif.get_cerfa().get_instruction() and manif.get_etat() == "annulée":
                        trimanif.append(manif)
                elif request.GET['filtre_etat'] == "all":
                    trimanif.append(manif)
            context = {
                'manifs': trimanif
            }

        return render(request, "evenements/dashboard_organisateur_liste.html", context)


class ConsultManifListe(View):
    """Retourne la liste des accès donnés à une organisation"""

    @method_decorator(login_requis(), name='dispatch')
    def dispatch(self, *args, **kwargs):
        """ Protéger la vue pour un accès par les utilisateurs connectés uniquement """
        return super().dispatch(*args, **kwargs)

    def get(self, request, pk):
        temporalite = self.request.GET.get('temporalite', default='')
        annee = self.request.GET.get('annee', None)
        service = Organisation.objects.get(pk=self.request.session['service_pk'])
        liste_manif = list()
        liste_acces = AutorisationAcces.objects.filter(organisation=service, date_revocation__isnull=True,
                                                       manif__date_fin__gte=timezone.now())
        if temporalite == "archive":
            liste_acces = AutorisationAcces.objects.filter(organisation=service, date_revocation__isnull=True,
                                                           manif__date_fin__lt=timezone.now())
            if annee:
                liste_acces = AutorisationAcces.objects.filter(organisation=service, date_revocation__isnull=True,
                                                               manif__date_fin__year=annee)
        for acces in liste_acces:
            if acces.instruction:
                acces.manif.href = acces.instruction.get_absolute_url()
            elif acces.avis:
                acces.manif.href = acces.avis.get_absolute_url()
            else:
                acces.manif.href = acces.manif.get_cerfa().get_absolute_url()
            liste_manif.append(acces.manif)
        context = {
            'user_is_instructeur': service.is_service_instructeur(),
            'manifs': liste_manif,
            'consult': True,
            'service': service,
        }
        return render(request, "evenements/dashboard_organisateur_liste.html", context)
