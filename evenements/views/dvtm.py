# coding: utf-8
from .manif import ManifDetail, ManifCreate, ManifUpdate, ManifDelete
from ..forms.vtm import DvtmForm
from ..models.vtm import Dvtm


class DvtmDetail(ManifDetail):
    """ Détail de la manifestation """

    # Configuration
    model = Dvtm


class DvtmCreate(ManifCreate):
    """ Création de la manifestation """

    # Configuration
    model = Dvtm
    form_class = DvtmForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dvtm'
        return context


class DvtmUpdate(ManifUpdate):
    """ Modification de la manifestation """

    # Configuration
    model = Dvtm
    form_class = DvtmForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dvtm'
        return context


class DvtmDelete(ManifDelete):
    """ Supprimer une manifestation """

    # Configuration
    model = Dvtm