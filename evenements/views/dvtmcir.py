# coding: utf-8
from django import forms

from .manif import ManifDetail, ManifCreate, ManifUpdate, ManifDelete
from ..forms.vtm import DvtmcirForm
from ..models.vtm import Dvtmcir


class DvtmcirDetail(ManifDetail):
    """ Détail de la manifestation """

    # Configiratiuon
    model = Dvtmcir


class DvtmcirCreate(ManifCreate):
    """ Création de la manifestation """

    # Configuration
    model = Dvtmcir
    form_class = DvtmcirForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dvtmcir'
        return context


class DvtmcirUpdate(ManifUpdate):
    """ Modification de la manifestation """

    # Configuration
    model = Dvtmcir
    form_class = DvtmcirForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dvtmcir'
        return context


class DvtmcirDelete(ManifDelete):
    """ Supprimer une manifestation """

    # Configuration
    model = Dvtmcir