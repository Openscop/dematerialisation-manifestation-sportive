import asyncio
import os
import shutil
import zipfile
import hashlib
import pytz
from asgiref.sync import sync_to_async
from io import BytesIO
import xhtml2pdf.pisa as pisa
from PyPDF3 import PdfFileMerger
from preview_generator.manager import PreviewManager
from PIL import Image

from django.http import HttpResponse
from django.template.loader import get_template
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.core import serializers
from django.views.generic import View
from django.shortcuts import get_object_or_404
from django.utils.decorators import classonlymethod
from django.conf import settings
from core.tasks import export_multiserver
from evenements.models import Manif, FichierExportation
from instructions.models import *
from evenements.decorators import verifier_acces_evenement_async
from instructions.decorators import verifier_secteur_instruction_async, verifier_service_avis_async
from messagerie.models import Message
from carto.views import export_parcours

"""
Détail des librairies :
- Zipfile : permet de gérer les fichiers zip en python
- Pisa : permet de créer un PDF à partir d'un template django
- ByteIO : permet à Pisa d'écrire le fichier PDF
- PreviewManager : permet ici de transformer des fichiers en PDF
- PILLOW : permet ici de transformer les images en PDF ce que ne fait pas PreviewManager

Fonctionnement global :
L'utilisateur selon son rôle va arriver sur une des classes "export_pour". 
On vérifie s’il souhaite les avis ou préavis correspondant à ses droits d'accès et s’il veut le zip ou le pdf.
Puis on passe par la classe Exportdossier. L'initialisation nous permet de récupérer la manif et de créer ou nettoyer
les dossiers utilisés.
Ensuite, on va créer le résumé pdf notamment avec la classe RenderSave; On y intégrera s'il y a les avis ou préavis.
Après nous regroupons tous les fichiers concernant une manifestation : fichiers joints, doc officiels, doc complémentaires.
Pièces jointes de l'avis ou préavis sélectionné, dans un tableau pour les mettre dans le zip ou le pdf.

Si le format de l'export est zip,
on va d'abord chercher le fichier GPS des parcours associés à cette manifestation puis on l'intègre au tableau des fichiers.
On met le résumé puis tous les fichiers présents dans le tableau dans le ZIP.
On finit par répondre en donnant le zip.

Si le format de l'export est PDF, on n'intègre pas de carto, car il est impossible d'obtenir quelque chose convertissable en pdf.
On va parcourir le tableau de fichier, pour transformer toutes les pièces jointes en pdf, soit pour les images en utilisant Pillow
soit en utilisant PreviewManager pour les fichiers text et documents docx odt etc...
On met tous ces fichiers transformés dans un tableau.
Puis on fusionne le résumé avec tous les fichiers pdf.
On finit par répondre en donnant ce pdf.
"""


def link_callback(uri, rel):
    """
    afficher les images dans le pdf, Il transforme l'uri dans le template dans le bon format
    :param uri:
    :param rel:
    :return:
    """
    s_url = settings.STATIC_URL  # Typically /static/
    s_root = settings.STATIC_ROOT  # Typically /home/userX/project_static/
    m_url = settings.MEDIA_URL  # Typically /static/media/
    m_root = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

    if uri.startswith(m_url):
        path = os.path.join(m_root, uri.replace(m_url, ""))
    elif uri.startswith(s_url):
        path = os.path.join(s_root, uri.replace(s_url, ""))
    else:
        return uri
    if not os.path.isfile(path):
        raise Exception(
            'media URI must start with %s or %s' % (s_url, m_url)
        )
    return path


def file_to_pdf(file, path):
    """
    Transformer des fichier (sauf image) en pdf
    """
    manager = PreviewManager(path, create_folder=True)
    temp = manager.get_pdf_preview(file)
    return temp


async def convert_avec_libreoffice(file, path):
    """
    Transformer des fichiers en pdf avec libreoffice
    :return:
    """
    os.system('soffice --convert-to pdf ' + file + ' --outdir ' + path)
    # verification que le fichier a bien été généré (au cas où office plante)
    if os.path.exists(path + file.split('/')[-1].replace(file.split('.')[-1], '') + 'pdf'):
        return path + file.split('/')[-1].replace(file.split('.')[-1], '') + 'pdf'
    else:
        return None


class Rendersave:
    """
    créer un pdf et le sauvegarder
    """
    @staticmethod
    def render(path: str, params: dict, name: str):
        template = get_template(path)
        html = template.render(params)
        file = open(settings.MEDIA_ROOT+name+'.pdf', "w+b")
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), dest=file, link_callback=link_callback)
        file.close()
        if not pdf.err:
            return file
        else:
            return pdf.err


def creer_pdf_resume(template, data, nom):
    """
    Fonction pour contourner le problème d'enregistrement du pdf en forçant plusieurs tentative
    """
    tentative = 0
    pdf = None
    while tentative < 5 and not pdf:
        tentative += 1
        try:
            pdf = Rendersave.render(template, data, nom)
        except Exception as error:
            print(error)
    return pdf


class Exportdossier:
    """
    envoi du dossier pdf ou zip
    """

    @staticmethod
    async def export(user, pk, avis=None, preavis=None, preferelezip=False):
        """
        Export de la manif. Nous allons
        - préparer le pdf
        - Créer une liste de fichier à mettre dans le zip ou le pdf
        - Créer soi le zip soi le pdf
        """
        # on va eviter de devoir transformer toutes les fonctions de model en ascyn
        # du coup on va reunir tous les appels dans une fonction sync pour pouvoir la transformer en async d'un coup
        @sync_to_async
        def tache_synchrone(user, pk, avis, preavis, preferelezip):
            manif = get_object_or_404(Manif, pk=pk)

            # création d'un msg traçabilité
            if avis or preavis:
                titre = "Export avec avis ou préavis sous zip" if preferelezip else "Export avec avis ou préavis sous pdf"
            else:
                titre = "Export du dossier simple sous zip" if preferelezip else "Export du dossier simple sous pdf"
            Message.objects.creer_et_envoyer('tracabilite', None, [user], titre, titre, manifestation_liee=manif)

            # si pas de dossier tmp on le créé
            if os.path.exists(settings.MEDIA_ROOT + "exportation"):
                pass
            else:
                os.makedirs(settings.MEDIA_ROOT + "exportation")
            pathdelamanif = settings.MEDIA_ROOT + "exportation/" + str(manif.pk) + "/"

            # on nettoie ou crée le dossier temporaire exclusif à la manif
            if os.path.exists(pathdelamanif):
                for file in os.listdir(pathdelamanif):
                    file_path = os.path.join(pathdelamanif, file)
                    try:
                        if os.path.isfile(file_path):
                            os.unlink(file_path)
                    except Exception as e:
                        print(e)
            else:
                os.makedirs(pathdelamanif)

            # Préparation du pdf du résumé de la manifestation
            data = serializers.serialize('python', [manif, ])
            parametres = {
                'manif': manif,
                'n2kevaluation': manif.n2kevaluation if hasattr(manif, 'n2kevaluation') else None,
                'rnrevaluation': manif.rnrevaluation if hasattr(manif, 'rnrevaluation') else None,
                'export': True
            }
            nom_zip = "exportation/" + str(manif.pk) + "/detail_manifestation"
            pdf = creer_pdf_resume('evenements/resume_pdf.html', parametres, nom_zip)

            if avis:
                parametres = {
                    'manif': manif,
                    'liste_avis': avis,
                    'export': True
                }
                nom_zip = "exportation/" + str(manif.pk) + "/avis"
                pdf_avis = creer_pdf_resume('evenements/avis_pdf.html', parametres, nom_zip)
            else:
                pdf_avis = None

            if preavis:
                parametres = {
                    'manif': manif,
                    'liste_avis': preavis,
                    'export': True
                }
                nom_zip = "exportation/" + str(manif.pk) + "/preavis"
                pdf_preavis = creer_pdf_resume('evenements/avis_pdf.html', parametres, nom_zip)
            else:
                pdf_preavis = None

            # on prend tout les fichier en rapport avec la manifestationd dans un tableau pour ensuite le mettre dans le zip ou pdf
            fichier = []

            for pj in manif.piece_jointe.filter(champ_cerfa__isnull=False, scan_antivirus=True):
                fichier.append(pj.document_attache.path) if pj.document_attache else ""
            pjs = fichier[:]
            # documents complementaires
            doccomple = []
            for doc in manif.piece_jointe.filter(champ_cerfa__isnull=True, scan_antivirus=True):
                fichier.append(doc.document_attache.path) if doc.document_attache else ''
                doccomple.append(doc.document_attache.path) if doc.document_attache else ''
            # documents officiels
            docoffi = []
            if hasattr(manif, "instruction"):
                for doc in DocumentOfficiel.objects.filter(instruction__manif=manif):
                    fichier.append(doc.fichier.path) if doc.fichier else ''
                    docoffi.append(doc.fichier.path) if doc.fichier else ''
            # avis
            avispj = []
            if avis:
                for avi in avis:
                    for doc in PieceJointeAvis.objects.filter(avis=avi):
                        fichier.append(doc.fichier.path) if doc.fichier else ""
                        avispj.append(doc.fichier.path) if doc.fichier else ""
            # preavis
            preavispj = []
            if preavis:
                for preavi in preavis:
                    for doc in PieceJointeAvis.objects.filter(avis=preavi):
                        fichier.append(doc.fichier.path) if doc.fichier else ""
                        preavispj.append(doc.fichier.path) if doc.fichier else ""

            cartos = [p.pk for p in manif.parcours.all()]

            departement_number = manif.get_instance().get_departement_name()
            out_path = f"{settings.MEDIA_ROOT}{departement_number}/{manif.pk}/exportation/"
            if not os.path.exists(out_path):
                os.makedirs(out_path)

            return manif, pathdelamanif, pdf, pdf_avis, pdf_preavis, fichier, pjs, doccomple, docoffi, avispj, preavispj, cartos, out_path

        manif, pathdelamanif, pdf, pdf_avis, pdf_preavis, fichier, pjs, doccomple, docoffi, avispj, preavispj, cartos, out_path = await tache_synchrone(user, pk, avis, preavis, preferelezip)

        # initialisation
        cache_path = pathdelamanif
        # si l'option zip est faite
        if preferelezip:
            resume = []
            if pdf:
                resume.append(pdf.name)
            if avis and pdf_avis:
                resume.append(pdf_avis.name)
            if preavis and pdf_preavis:
                resume.append(pdf_preavis.name)

            # cartographie
            async def carto_download(carto):
                nom_carto = await sync_to_async(export_parcours)(carto)
                print(nom_carto)
                return nom_carto
            coros = [carto_download(carto) for carto in cartos]
            tableau_carto = await asyncio.gather(*coros)
            print(tableau_carto)
            # creation du zip
            nom_zip = "exportation/" + str(manif.pk) + "/" + manif.nom.replace(' ', '_').replace('/', '_')
            zip_filename = settings.MEDIA_ROOT + nom_zip + ".zip"
            zipf = zipfile.ZipFile(zip_filename, "w", zipfile.ZIP_DEFLATED)
            nomgrosdossier = str(manif.date_debut.date()) + "_dossier_" + manif.nom.replace(' ', '_').replace('/', '_')[:20] + '/'
            # remplissage du zip avec tout les fichiers
            for file in resume:
                if os.path.exists(file):
                    dir, base_filename = os.path.split(file)
                    os.chdir(dir)
                    zipf.write(base_filename, nomgrosdossier+base_filename)
            for file in pjs:
                if os.path.exists(file):
                    dir, base_filename = os.path.split(file)
                    os.chdir(dir)
                    zipf.write(base_filename, nomgrosdossier+"piece_jointe/"+base_filename)
            for file in tableau_carto:
                if os.path.exists(file):
                    dir, base_filename = os.path.split(file)
                    os.chdir(dir)
                    zipf.write(base_filename, nomgrosdossier + "carto/" + base_filename)
            for file in doccomple:
                if os.path.exists(file):
                    dir, base_filename = os.path.split(file)
                    os.chdir(dir)
                    zipf.write(base_filename, nomgrosdossier+"doc_complementaire/"+base_filename)
            for file in docoffi:
                if os.path.exists(file):
                    dir, base_filename = os.path.split(file)
                    os.chdir(dir)
                    zipf.write(base_filename,  nomgrosdossier+"doc_officiel/"+base_filename)
            for file in avispj:
                if os.path.exists(file):
                    dir, base_filename = os.path.split(file)
                    os.chdir(dir)
                    zipf.write(base_filename,  nomgrosdossier+"doc_avis/"+base_filename)
            for file in preavispj:
                if os.path.exists(file):
                    dir, base_filename = os.path.split(file)
                    os.chdir(dir)
                    zipf.write(base_filename,  nomgrosdossier+"doc_preavis/"+base_filename)
            zipf.close()
            now = timezone.now()
            nom_final = f"{out_path}{now.strftime('%m_%d_%Y_%H_%M_')}dossier_{manif.nom.replace(' ', '_').replace('/', '_')}_du_{str(manif.date_debut.date())}.zip"
            if os.path.exists(nom_final):
                os.remove(nom_final)
            shutil.copy(zip_filename, nom_final)


        # si option pdf

        else:

            # l'appel d'un model étant sync on doit le faire dans une function converties en async
            @sync_to_async
            def remplir_tableau(tableau_pdf):
                if pdf:
                    tableau_pdf.append(pdf.name)
                if avis and pdf_avis:
                    tableau_pdf.append(pdf_avis.name)
                if preavis and pdf_preavis:
                    tableau_pdf.append(pdf_preavis.name)
                return tableau_pdf
            tableau_pdf = await remplir_tableau([])

            async def transformation(file, pathdelamanif):
                ok = False
                if file.split('.')[-1] == "jpg" or file.split('.')[-1] == "jpeg" or file.split('.')[-1] == "png":
                    if os.path.exists(file):
                        image = Image.open(file)
                        im1 = image.convert('RGB')
                        im1.save(cache_path+file.split('/')[-1]+'.pdf')
                        return cache_path+file.split('/')[-1]+'.pdf'
                    else:
                        return None
                elif not file.split('.')[-1] == 'pdf':
                    if not os.path.exists(file):
                        return None
                    # le await ici n'attend pas que la commande finisse mais qu'elle est finie de débuté
                    process = await asyncio.create_subprocess_exec("soffice", "--headless", "--nolockcheck", '--norestore', '--convert-to', 'pdf', file, '--outdir', pathdelamanif)
                    try:
                        # on attend que le process finissent ou on coupe au bout de 20 s
                        await asyncio.wait_for(process.wait(), timeout=30)
                        if os.path.exists(pathdelamanif + file.split('/')[-1].replace(file.split('.')[-1], '') + 'pdf'):
                            return pathdelamanif + file.split('/')[-1].replace(file.split('.')[-1], '') + 'pdf'
                # BOF kill du process
                # On veut être sur que quelque soit le cas de figure le process soffice sera fermé pour eviter les fuites de memoire
                # les erreurs sont du à la tentative de fermeture d'un process dejà fermé
                        else: # cas ou le fichier n'a pas été généré
                            process.kill()

                    except asyncio.TimeoutError:
                        try:
                            process.kill()
                        except:  # erreur du kill lors du timeout
                            pass
                    except:  # erreur du kill lors du cas hors timeout (normalement impossible)
                        pass
                # EOF kill du process
                if not ok:
                    def convert_preview():
                        manager = PreviewManager(cache_path, create_folder=True)
                        temp = manager.get_pdf_preview(file)
                        return temp
                    try:
                        test = await asyncio.wait_for(sync_to_async(convert_preview)(), timeout=30)
                    except asyncio.TimeoutError:
                        return None
                    except Exception:
                        return None
                    if os.path.exists(test):
                        return test
                    else:
                        return None

            # ici on va reunir tout les appel coroutines de transformations des fichiers pour tout executer en même temps
            coros = [transformation(file, pathdelamanif) for file in fichier]
            tableau_transform = await asyncio.gather(*coros)
            # clean des None comme résultats
            for files in tableau_transform:
                tableau_pdf.append(files) if files else ""

            # fusion dans un pdf final
            # false pour eviter les crashs de pdf
            merger = PdfFileMerger(strict=False)
            for file in tableau_pdf:
                merger.append(file)
            try:
                merger.write(cache_path+"export"+manif.nom.replace(' ', '_').replace('/', '_')+".pdf")
                now = timezone.now()
                nom_final = f"{out_path}{now.strftime('%m_%d_%Y_%H_%M_')}dossier_{manif.nom.replace(' ', '_').replace('/', '_')}_du_{str(manif.date_debut.date())}.pdf"
                if os.path.exists(nom_final):
                    os.remove(nom_final)
                shutil.copy(cache_path+"export"+manif.nom.replace(' ', '_').replace('/', '_')+".pdf", nom_final)

            except Exception:
                nom_final = False

        @sync_to_async
        def msg_fin():
            limite = timezone.now() + timezone.timedelta(hours=1)
            timezoneLocal = pytz.timezone('Europe/Paris')
            if avis or preavis:
                titre = "Export avec avis ou préavis sous zip" if preferelezip else "Export avec avis ou préavis sous pdf"
            else:
                titre = "Export du dossier simple sous zip" if preferelezip else "Export du dossier simple sous pdf"
            if nom_final:
                message = (f"<p>Voici le lien du fichier d'exportation<br>"
                           f"<a href=\"/media/{nom_final.split(settings.MEDIA_ROOT)[1]}\" target='_blank'>{settings.MAIN_URL}media/{nom_final.split(settings.MEDIA_ROOT)[1]}</a><br>"
                           f"Attention, votre fichier n'est disponible que pendant 1 heure, c'est à dire jusqu'à {limite.astimezone(timezoneLocal).strftime('%H:%M')}.</p>")
            else:
                message = f"Impossible de convertir en pdf le dossier, merci de réessayer sous format zip"
            Message.objects.creer_et_envoyer('info_suivi', None, [user], titre, message, manifestation_liee=manif)
            FichierExportation(url=nom_final).save()


        await msg_fin()
        return nom_final if nom_final else None


@method_decorator(verifier_secteur_instruction_async, name='dispatch')
class ExportPourInstructeur(View):

    # réécriture necessaire pour que la classe View puisse accepter de l'async
    @classonlymethod
    def as_view(cls, **initkwargs):
        view = super().as_view(**initkwargs)
        view._is_coroutine = asyncio.coroutines._is_coroutine
        return view

    async def get(self, request, pk):

        zip = "1" if request.GET.get("zip", None) else ""
        avis = "1" if request.GET.get('avis', None) else ""

        export_multiserver.delay(request.user.pk, request.organisation.pk, pk, type="instructeur", zip=zip, avis_demande=avis)
        return HttpResponse('ok')


@method_decorator(verifier_service_avis_async, name='dispatch')
class ExportPourAgent(View):

    # réécriture necessaire pour que la classe View puisse accepter de l'async
    @classonlymethod
    def as_view(cls, **initkwargs):
        view = super().as_view(**initkwargs)
        view._is_coroutine = asyncio.coroutines._is_coroutine
        return view

    async def get(self, request, pk):

        zip = "1" if request.GET.get("zip", None) else ""
        avis = "1" if request.GET.get('avis', None) else ""
        export_multiserver.delay(request.user.pk, request.organisation.pk, pk, type="agent", zip=zip, avis_demande=avis)
        return HttpResponse('ok')


@method_decorator(verifier_acces_evenement_async, name='dispatch')
class ExportPourOrganisateur(View):

    # réécriture necessaire pour que la classe View puisse accepter de l'async
    @classonlymethod
    def as_view(cls, **initkwargs):
        view = super().as_view(**initkwargs)
        view._is_coroutine = asyncio.coroutines._is_coroutine
        return view

    async def get(self, request, pk):

        zip = "1" if request.GET.get("zip", None) else ""
        key = hashlib.md5(str(settings.SECRET_KEY).encode('utf-8')).hexdigest()
        url = 'http://' + settings.EXPORTATION_IP + '/evenement/export/interne/?type=organisateur&pk_user=' + str(request.user.pk) +\
              '&pk_manif=' + str(pk) + "&zip=" + zip + '&key=' + str(key)
        print(url)
        if settings.TESTS_IN_PROGRESS:
            response = HttpResponse()
            response.content = url
        else:
            export_multiserver.delay(request.user.pk, request.organisation.pk, pk, type="organisateur", zip=zip)
            response = HttpResponse('ok')
        return response
