# coding: utf-8
import copy, shutil, os
import json

from django.forms.models import ModelChoiceField
from django.views.generic.detail import SingleObjectMixin
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render, reverse, get_object_or_404, redirect
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views.generic import DetailView, CreateView, DeleteView, View
from django.views.generic.edit import UpdateView
from django.forms.models import model_to_dict
from django.conf import settings
from django.db.models import Q

from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from administrative_division.models.region import Region
from sports.models import Activite
from core.models import Instance
from core.util.permissions import require_type_organisation
from evenements.decorators import verifier_acces_evenement, verifier_proprietaire
from instructions.decorators import verifier_secteur_instruction
from ..forms.manif import ManifInstructeurUpdateForm, ManifCloneForm
from ..models import Manif
from messagerie.models import Message
from structure.models import Organisation, StructureOrganisatrice, ServiceInstructeur, ServiceConsulte


@method_decorator(verifier_acces_evenement(acces=True), name='dispatch')
class ManifDetail(DetailView):
    """ Détail d'une manifestation """

    template_name = 'evenements/evenement_detail.html'

    # Overrides
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # recalcul des instances instruites au cas où des instances où été activées entre temps
        self.object.gestion_instance_instruites()
        context['manquants'] = context['declarer'] = context['toutbon'] = context['formulaire_manquant'] = context['carto_manquant'] = False
        context['sans_avis_fede'] = self.object.get_context_fede()
        context['type'] = self.object.get_type_manif()
        context['last_action'] = self.object.message_manifestation.filter(type='action').last()
        # Accès en lecture
        context['acces_consult'] = self.object.acces.filter(date_revocation__isnull=True)
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        context['btn_autoriser_depot'] = False
        if self.object.acces.filter(organisation=organisation, date_revocation__isnull=True).exists():
            acces = self.object.acces.get(organisation=organisation, date_revocation__isnull=True)
            acces.date_derniere_consult = timezone.now()
            acces.save()
            context['consult'] = acces
            from instructions.models import Instruction
            if (organisation in Instruction.get_service_prefecture_list(self.object)[0] and
                    self.object.delai_depasse() and not self.object.date_depassee() and
                    not self.object.get_instruction() and not self.object.autorisation_hors_delai_json):
                context['btn_autoriser_depot'] = True
        else:
            context['consult'] = None
        # Pour la barre de progression
        total = 0
        avis_fait = 0
        for instruction in self.object.instruction.all():
            avis_fait += int(instruction.get_nb_avis_rendus())
            total += int(instruction.get_nb_avis())
        context['progres'] = int(avis_fait / total * 100) if total else 0

        # Pour afficher l'instructeur
        if self.object.instance_instruites.count() == 1 and self.object.get_instruction(instance=organisation.instance_fk) \
                and self.object.get_instruction(instance=organisation.instance_fk).referent:
            context['referent'] = self.object.get_instruction(instance=organisation.instance_fk).referent.get_full_name()
        else:
            context['referent'] = ''
        # Pour gérer l'affichage dans le blocs Actions
        context['carto_complet'] = self.object.carto_complet()
        # Modification demandée
        if self.object.modif_json:
            context['modif'] = self.object.a_modifier()
            context['champ'] = json.dumps(self.object.get_cerfa().get_liste_champs_a_modifier())
        if self.object.dossier_complet():
            if not self.object.get_docs_complementaires_manquants().exists():
                if (not self.object.delai_depasse() or self.object.autorisation_hors_delai_json) and not self.object.get_instruction():
                    context['declarer'] = True
                else:
                    if not any([mot_cle in context for mot_cle in ["modif_pj", "modif_parcours", "modif_champ", "modif_n2k", "modif_rnr"]]):
                        context['toutbon'] = True

        else:
            if hasattr(self.object, 'n2kevaluation'):
                if not self.object.n2kevaluation.formulaire_n2k_complet():
                    context['n2k_manquant'] = True
            if self.object.afficher_panneau_eval_n2000():
                context['cause'] = self.object.afficher_panneau_eval_n2000(cause=True)
            if hasattr(self.object, 'rnrevaluation'):
                if not self.object.rnrevaluation.formulaire_rnr_complet():
                    context['rnr_manquant'] = True
            if not self.object.formulaire_complet():
                context['formulaire_manquant'] = True

            if self.object.liste_manquants():
                context['manquants'] = True
                # Afficher le retard pour avertissement
                if self.object.etape_en_cours() == 'etape 2':
                    # Pendant l'étape 2, seul les fichiers à fournir à l'étape 1 peuvent être en retard
                    for fichier in self.object.LISTE_FICHIERS_ETAPE_1:
                        # Si un seul fichier est en retard, on passe le retard dans le contexte
                        if self.object.piece_jointe.filter(champ_cerfa=fichier) and not self.object.piece_jointe.get(champ_cerfa=fichier).document_attache:
                            context['delta'] = (timezone.now().date() - self.object.get_date_etape_1().date()).days
                if self.object.etape_en_cours() == 'etape 3':
                    # Pendant l'étape 3, tous les manquants sont en retard
                    context['delta'] = (timezone.now().date() - self.object.get_date_etape_2().date()).days
        context["instances"] = Instance.objects.configured().order_by('departement__name')
        context["regions"] = Region.objects.all()
        context["service"] = organisation

        return context


class ManifCreate(CreateView):
    """ Création de manifestation """

    # Configuration
    template_name = 'evenements/evenement_form.html'

    # Overrides
    @method_decorator(require_type_organisation([StructureOrganisatrice,]))
    def dispatch(self, *args, **kwargs):
        if 'activite' not in self.request.session or not self.request.GET.get('dept'):
            return render(self.request,
                          "core/access_restricted.html",
                          {'message': "Demande incorrecte. "
                                      "Veuillez utiliser l'outil de <a href=\"/evenement/creation/?dept=\">recherche de formulaire</a>."},
                          status=403)
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.instance = self.object.ville_depart.get_instance()
        user_structure = StructureOrganisatrice.objects.get(pk=self.request.session['service_pk'])
        self.object.structure_organisatrice_fk = user_structure
        self.object.declarant = self.request.user
        self.object.emprise = self.request.session['emprise']
        self.object.competition = self.request.session['competition']
        self.object.circuit = int(self.request.session['circuit'])
        self.object.save()
        form.save_m2m()
        self.request.session.pop('activite')
        delai = self.object.get_cerfa().delai_en_cours()
        if not delai == 21:
            self.object.delai_cours = str(delai)
        else:
            depot = self.object.get_cerfa().delaiDepot
            self.object.delai_cours = str(depot) + '-21'
        # ajout des instances instruites
        # gestion des villes de depart
        self.object.ville_depart_interdep_mtm.add(self.object.ville_depart)
        if self.object.departements_traverses.all():
            tab = []
            for dep in self.object.departements_traverses.all():
                commune_pk = self.request.POST.get(f"ville_depart_interdep_{str(dep.pk)}")
                if commune_pk:
                    commune = Commune.objects.get(pk=commune_pk)
                    self.object.ville_depart_interdep_mtm.add(commune)
                    tab.append({
                        'departement_pk': str(dep.pk),
                        'departement_name': str(dep),
                        'commune_pk': str(commune_pk),
                        'commune_name': str(commune)
                    })
            self.object.ville_depart_interdep = tab
            [self.object.villes_traversees.remove(ville) for ville in self.object.villes_traversees.all()]
            datas = self.request.POST.getlist(f"ville_traverse_{str(self.object.instance.departement.pk)}")
            if datas:
                [self.object.villes_traversees.add(Commune.objects.get(pk=data)) for data in datas]
            for dep in self.object.departements_traverses.all():
                datas = self.request.POST.getlist(f"ville_traverse_{str(dep.pk)}")
                if datas:
                    [self.object.villes_traversees.add(Commune.objects.get(pk=data)) for data in datas]
        self.object.save()
        self.object.notifier_creation()
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        request_departement = self.request.GET.get('dept')
        kwargs = super().get_form_kwargs()
        kwargs['extradata'] = request_departement
        self.request.session['extradata'] = request_departement
        self.request.session.save()
        if request_departement:
            kwargs['initial'].update({'departement_depart': Departement.objects.get_by_name(request_departement).pk})
        return kwargs

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.target = form.instance

        request_departement = self.request.session['extradata']
        form.fields['activite'].initial = self.request.session['activite']
        form.fields['competition'].initial = self.request.session['competition']
        form.fields['circuit'].initial = self.request.session['circuit']
        form.fields['emprise'].initial = self.request.session['emprise']
        form.fields['vtm_hors_circulation'].initial = self.request.session.get('vtm_hors_circulation', False)
        form.fields['nb_participants'].initial = self.request.session['nb_participants']
        form.fields['departements_traverses'].queryset = Departement.objects.exclude(name=request_departement)
        form.fields['ville_depart'] = ModelChoiceField(required=True, queryset=Commune.objects.by_departement_name(request_departement),
                                                       label="Commune de départ", disabled=True)
        form.fields['ville_depart'].initial = self.request.session['commune']
        form._meta.exclude += ('departement_depart',)
        return form

    def get_initial(self):
        initial = super().get_initial()
        if self.request.GET.get('manif', default=None):
            ancien_manif = Manif.objects.get(pk=self.request.GET.get('manif'))
            initial = model_to_dict(ancien_manif)
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = self.model._meta.model_name
        context['cerfa'] = self.model.refCerfa
        context['verbose'] = self.model._meta.verbose_name
        if self.request.GET.get('manif', default=None):
            context['manif'] = Manif.objects.get(pk=self.request.GET.get('manif'))
        return context


class ManifUpdate(UpdateView):
    """ Modifier une manifestation """

    # Configuration
    template_name = 'evenements/evenement_form.html'

    # Overrides
    @method_decorator(verifier_proprietaire)
    def dispatch(self, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['pk'])
        # En accès direct par url; si la manifestation a été envoyée, il est trop tard pour la modifier
        if manif.en_cours_instruction():
            if manif.modif_json:
                for line in manif.modif_json:
                    if 'manif' in line and not line['done']:
                        return super().dispatch(*args, **kwargs)
            return render(self.request, "core/access_restricted.html",
                          {'message': "Cette manifestation ne peut plus être modifiée !"}, status=403)
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        request_departement = self.request.GET.get('dept')
        kwargs = super().get_form_kwargs()
        kwargs['extradata'] = request_departement
        self.request.session['extradata'] = request_departement
        self.request.session.save()
        kwargs['initial'].update({'departement_depart': self.object.ville_depart.get_departement().pk})
        # Pour afficher les communes traversées du département de départ, il faut ajouter le département de départ à la liste des département traversés
        # Cela doit venir de clever-select
        # le kwarg écrase la valeur initiale tirée de la DB d'où l'ajout à la liste
        dept_trav = list(self.object.departements_traverses.all())
        dept_trav.append(self.object.ville_depart.get_departement())
        kwargs['initial'].update({'departements_traverses': dept_trav})
        return kwargs

    def get_form(self, form_class=None):
        request_departement = self.request.GET.get('dept')
        form = super().get_form(form_class)
        form.target = form.instance
        form.request = self.request

        # Remplacer les champs commune de départ et département si le département est passé dans l"URL
        if request_departement:
            form.fields['departements_traverses'].queryset = Departement.objects.exclude(name=request_departement)
            form.fields['activite'].queryset = Activite.objects.filter(discipline=form.instance.activite.discipline)
            form.fields['ville_depart'].queryset = Commune.objects.by_departement_name(request_departement)
            form._meta.exclude += ('departement_depart',)
        return form

    def form_valid(self, form):
        def gestion_ville_depart_interdep(obj, dep_pk):
            commune_pk = self.request.POST.get(f"ville_depart_interdep_{dep_pk}")
            if commune_pk:
                commune = Commune.objects.get(pk=commune_pk)
                obj.ville_depart_interdep_mtm.add(commune)
                if not {'departement_pk': str(dep.pk), 'departement_name': str(dep),'commune_pk': str(commune_pk), 'commune_name': str(commune)} in tab_ville_depart_interdep:
                    tab_ville_depart_interdep.append({
                        'departement_pk': str(dep.pk),
                        'departement_name': str(dep),
                        'commune_pk': str(commune_pk),
                        'commune_name': str(commune)
                    })

        # Mémorisation des m2m et json
        list_ville_depart_interdep = list(self.object.ville_depart_interdep_mtm.all())
        list_villes_traversees = list(self.object.villes_traversees.all())
        list_departements_traverses = list(self.object.departements_traverses.all())
        old_ville_depart_interdep = list(self.object.ville_depart_interdep) if self.object.ville_depart_interdep else []
        tab_ville_depart_interdep = list(self.object.ville_depart_interdep) if self.object.ville_depart_interdep else []
        # Définir les modifications demandées sur les m2m
        modif_ville_depart_interdep, modif_villes_traversees, modif_departements_traverses = None, False, False
        if self.object.get_instruction() and self.object.modif_json:
            for line in self.object.modif_json:
                if 'manif' in line and 'ville_depart_interdep' in line['manif'] and not line['done']:
                    modif_ville_depart_interdep = line['manif'].split("_")[-1:][0]
                if 'manif' in line and 'villes_traversees' in line['manif'] and not line['done']:
                    modif_villes_traversees = True
                if 'manif' in line and 'departements_traverses' in line['manif'] and not line['done']:
                    modif_departements_traverses = True
        # Traitement de l'évaluation n2k en fonction du champ signature_charte_dispense_site_n2k
        if form.instance.signature_charte_dispense_site_n2k:
            if form.instance.sites_natura2000.all().count() == 1:
                if hasattr(form.instance, 'n2kevaluation') and not (form.instance.n2kevaluation.pourquoi_instructeur or
                                                                    form.instance.n2kevaluation.pourquoi_organisateur):
                    form.instance.n2kevaluation.delete()
        self.object.declarant = self.request.user
        retour = super().form_valid(form)
        self.object.refresh_from_db()
        # gestion des villes de départ effectuée avant instruction ou en édition du champ pendant l'instruction
        if not self.object.get_instruction() or modif_ville_depart_interdep or modif_departements_traverses:
            # Supprimer la ville concernée dans "ville_depart_interdep_mtm" et modifier le tableau en conséquence
            if modif_ville_depart_interdep:
                dep = Departement.objects.get(pk=int(modif_ville_depart_interdep))
                [self.object.ville_depart_interdep_mtm.remove(ville) for ville in self.object.ville_depart_interdep_mtm.filter(arrondissement__departement=dep)]
                [tab_ville_depart_interdep.remove(item) for item in tab_ville_depart_interdep if item['departement_pk'] == modif_ville_depart_interdep]
            if modif_departements_traverses:
                # Suppression : normalement interdit par le clean()
                for dep in list_departements_traverses:
                    if dep not in self.object.departements_traverses.all():
                        [self.object.ville_depart_interdep_mtm.remove(ville) for ville in self.object.ville_depart_interdep_mtm.filter(arrondissement__departement=dep)]
                        [tab_ville_depart_interdep.remove(item) for item in tab_ville_depart_interdep if item['departement_pk'] == str(dep.pk)]
            # Avant instruction, seule la ville de départ est conservée
            if not self.object.get_instruction():
                [self.object.ville_depart_interdep_mtm.remove(ville) for ville in self.object.ville_depart_interdep_mtm.all()]
                self.object.ville_depart_interdep_mtm.add(self.object.ville_depart)
            # Si plusieurs départements, ajout de la ville de départ dans "ville_depart_interdep_mtm" en fonction du POST
            if self.object.departements_traverses.all():
                for dep in self.object.departements_traverses.all():
                    gestion_ville_depart_interdep(self.object, str(dep.pk))

        # gestion des villes traversées effectuée avant instruction ou en édition du champ pendant l'instruction
        if not self.object.get_instruction() or modif_villes_traversees or modif_departements_traverses:
            if modif_departements_traverses:
                # Suppression : normalement interdit par le clean()
                for dep in list_departements_traverses:
                    if dep not in self.object.departements_traverses.all():
                        [self.object.villes_traversees.remove(ville) for ville in self.object.villes_traversees.filter(arrondissement__departement=dep)]
                # Ajout
                for dep in self.object.departements_traverses.all():
                    if dep not in list_departements_traverses:
                        datas = self.request.POST.getlist(f"ville_traverse_{str(dep.pk)}")
                        if datas:
                            [self.object.villes_traversees.add(Commune.objects.get(pk=data)) for data in datas]
            if modif_villes_traversees or (not self.object.get_instruction() and self.object.departements_traverses.all().count() > 0):
                if self.object.departements_traverses.all().count() > 0:
                    datas = self.request.POST.getlist(f"ville_traverse_{str(self.object.instance.departement.pk)}")
                else:
                    datas = self.request.POST.getlist("villes_traversees")
                if datas:
                    # Ajout
                    for data in datas:
                        if not self.object.villes_traversees.filter(pk=int(data)):
                            self.object.villes_traversees.add(Commune.objects.get(pk=data))
                    # Suppression
                    for ville in self.object.villes_traversees.filter(arrondissement__departement=self.object.instance.departement):
                        if str(ville.pk) not in datas:
                            self.object.villes_traversees.remove(ville)
                else:
                    [self.object.villes_traversees.remove(ville) for ville in self.object.villes_traversees.filter(arrondissement__departement=self.object.instance.departement)]
                for dep in self.object.departements_traverses.all():
                    datas = self.request.POST.getlist(f"ville_traverse_{str(dep.pk)}")
                    if datas:
                        # Ajout
                        for data in datas:
                            if not self.object.villes_traversees.filter(pk=int(data)):
                                self.object.villes_traversees.add(Commune.objects.get(pk=data))
                        # Suppression
                        for ville in self.object.villes_traversees.filter(arrondissement__departement=dep):
                            if str(ville.pk) not in datas:
                                self.object.villes_traversees.remove(ville)
                    else:
                        [self.object.villes_traversees.remove(ville) for ville in self.object.villes_traversees.filter(arrondissement__departement=dep)]
        # Définir si les données m2m ont changé (champs spéciaux hors formulaire)
        if list(self.object.ville_depart_interdep_mtm.all()) != list_ville_depart_interdep:
            ville_depart_interdep_changed = True
        else:
            ville_depart_interdep_changed = False
        if list(self.object.villes_traversees.all()) != list_villes_traversees:
            villes_traversees_changed = True
        else:
            villes_traversees_changed = False
        # Historique des champs géré avec changed_data
        history_json = self.object.history_json
        if not history_json:
            history_json = []
        if form.changed_data and form.instance.get_instruction():
            for changed_data in form.changed_data:
                if changed_data not in ["ville_depart_interdep", "villes_traversees"]:
                    old_data = form.initial.get(changed_data)
                    date_changement = timezone.now()
                    if str(type(old_data).__name__) == "datetime":
                        old_data = old_data.astimezone().strftime("%d/%m/%Y %H:%M")
                    elif str(type(old_data).__name__) == 'list':
                        temp = [str(x) for x in old_data]
                        old_data = temp
                    elif str(type(old_data).__name__) == 'int' and hasattr(getattr(form.instance, changed_data), 'pk'):
                        pass
                    else:
                        old_data = str(old_data)
                    history_json.append({"champ": changed_data, "data": old_data, "date": str(date_changement),
                                         "type": str(type(old_data).__name__)})
                # Envoyer une information de suivie si le nom de la manifestation a changé
                if changed_data == "nom":
                    new_name = form.cleaned_data['nom']
                    destinataires = []
                    for instruction in self.object.instruction.all():
                        destinataires += instruction.get_instructeurs_avec_services()
                        destinataires += instruction.get_tous_agents_avec_service()
                        destinataires += instruction.manif.get_lecture_user()
                    titre = "Modification du nom d'un dossier"
                    contenu = f"<p>Le dossier anciennement nommé \"{old_data}\" a été renommé \"{new_name}\" par l'organisateur.<br>"
                    Message.objects.creer_et_envoyer('info_suivi',
                                                     [self.object.declarant, self.object.structure_organisatrice_fk],
                                                     destinataires,
                                                     titre, contenu,
                                                     manifestation_liee=self.object, objet_lie_nature="dossier")

        # Historique de ville_depart_interdep
        if self.object.get_instruction() and (ville_depart_interdep_changed or modif_departements_traverses):
            changement_ville_depart = []
            [changement_ville_depart.append(x) if x not in tab_ville_depart_interdep else "" for x in old_ville_depart_interdep]
            for old_donne in changement_ville_depart:
                history_json.append({"champ": f"ville_depart_interdep_{str(old_donne['departement_pk'])}",
                                     "data": old_donne['commune_name'], "date": str(timezone.now()),
                                     "type": str(type(old_donne['commune_name']).__name__)})
            liste_dep_in_ville_depart = [x['departement_pk'] for x in old_ville_depart_interdep]
            ajout_ville_depart = []
            [ajout_ville_depart.append(x) if x['departement_pk'] not in liste_dep_in_ville_depart else "" for x in tab_ville_depart_interdep]
            for new_donne in ajout_ville_depart:
                history_json.append({"champ": f"ville_depart_interdep_{str(new_donne['departement_pk'])}",
                                     "data": "", "date": str(timezone.now()), "type": str(type("").__name__)})
        # Historique de villes_traversées
        if self.object.get_instruction() and (villes_traversees_changed or modif_departements_traverses):
            history_json.append({"champ": "villes_traversees", "data": [str(x) for x in list_villes_traversees],
                                 "date": str(timezone.now()), "type": "list"})
        self.object.history_json = history_json
        # Marquer la line de demande de modification DONE si elle existe
        if self.object.get_instruction() and self.object.modif_json:
            for idx, line in enumerate(self.object.modif_json):
                if ('manif' in line and
                        (line['manif'] in form.changed_data or
                         modif_villes_traversees and villes_traversees_changed or
                         modif_ville_depart_interdep and ville_depart_interdep_changed) and
                        not line['done']):
                    form.instance.modif_json[idx]['done'] = True
                    self.object.notifier_manif_modifiee(line['manif'])
        self.object.ville_depart_interdep = tab_ville_depart_interdep if tab_ville_depart_interdep else None
        self.object.save()
        self.object.refresh_from_db()
        if self.object.get_instruction():
            self.object.gestion_instance_instruites_manif_en_cours_instruction()
        self.object.gestion_instance_instruites()
        self.object.creer_pj_cerfa()
        return retour

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['champ'] = json.dumps(
            [champ for formulaire, champ, date in self.object.get_cerfa().get_liste_champs_a_modifier() if formulaire == 'manif'])
        return context


class ManifDelete(DeleteView):
    """ Supprimer une manifestation """

    # Configuration
    template_name = 'evenements/evenement_suppression.html'

    # Overrides
    @method_decorator(verifier_proprietaire)
    def dispatch(self, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['pk'])
        # En accès direct par url; si la manifestation a été envoyée, il est trop tard pour la supprimer
        if manif.en_cours_instruction():
            return render(self.request, "core/access_restricted.html",
                          {'message': "Cette manifestation ne peut plus être supprimée !"}, status=403)
        return super().dispatch(*args, **kwargs)

    def form_valid(self, request, *args, **kwargs):
        manif = self.get_object()
        path = settings.MEDIA_ROOT + manif.get_departement_depart_nom() + '/'
        if os.path.isdir(path + str(manif.cerfa.pk)):
            os.system('rm -R ' + path + str(manif.cerfa.pk))
        return super().form_valid(request, *args, **kwargs)

    def get_success_url(self):
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        return redirect('evenements:tableau-de-bord-organisateur').url


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class ManifInstructeurUpdate(UpdateView):
    """ Formulaire instructeur """

    model = Manif
    form_class = ManifInstructeurUpdateForm
    template_name = 'evenements/evenement_form.html'

    # Overrides
    def get_success_url(self):
        """ URL lors de la validation du formulaire """
        return reverse("instructions:instruction_detail", kwargs={'pk': self.object.get_instruction(instance=Organisation.objects.get(pk=self.request.session['service_pk']).instance_fk).id})

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.target = form.instance
        form.fields['afficher_adresse_structure'].required = False
        return form


class ManifPublicDetail(DetailView):
    """ Détails publics d'une manifestation """

    # Configuration
    model = Manif
    template_name = 'evenements/evenementpublic_detail.html'

    def dispatch(self, request, *args, **kwargs):
        instance = self.get_object()
        if not instance.visible_dans_calendrier():
            if not request.organisation.is_service_instructeur():
                if instance.structure_organisatrice_fk != request.organisation:
                    return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)


class ManifUrlGen(View):
    def get(self, request, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=kwargs.get('pk'))
        if request.user.is_authenticated:
            organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
            if type(organisation) == StructureOrganisatrice:
                if manif in organisation.get_manifs_ecriture():
                    url_name = 'evenements:' + manif.get_type_manif() + '_detail'
                    return HttpResponseRedirect(reverse(url_name, kwargs={'pk': manif.pk}))
                return HttpResponseRedirect(reverse('evenements:manif_detail', kwargs={'pk': manif.pk}))
            if type(organisation) == ServiceInstructeur:
                if manif.en_cours_instruction() and manif in organisation.get_manifs_ecriture():
                    return HttpResponseRedirect(reverse(
                        'instructions:instruction_detail',
                        kwargs={'pk': manif.get_instruction(instance=organisation.instance_fk).pk}))
            if type(organisation) == ServiceConsulte:
                if manif.en_cours_instruction():
                    for instruction in manif.instruction.all():
                        avis = instruction.get_avis_service(organisation)
                        if avis:
                            return HttpResponseRedirect(reverse('instructions:avis_detail', kwargs={'pk': avis.pk}))
                raise Http404("Pas d'avis pour cette manifestation")
            raise Http404("Pas de role utilisateur pour orienter la vue de détail")
        else:
            return render(request, 'core/access_restricted.html', status=403)


class ManifHistoriquePopup(View):
    def get(self, request, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=kwargs.get('pk'))
        if not request.user.is_authenticated or not manif.has_access(request.user):
            return render(request, "core/access_restricted.html",
                          {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)
        n2k = request.GET.get('n2k_name')
        rnr = request.GET.get('rnr_name')
        pj = request.GET.get('pj_pk')
        name = request.GET.get('name')
        data = ""
        type_pj = False
        if n2k:
            data = manif.n2kevaluation.get_history(n2k)
        elif rnr:
            data = manif.rnrevaluation.get_history(rnr)
        elif pj:
            data = manif.piece_jointe.filter(pk=pj).first().historique.all()
            type_pj = True
        elif name:
            data = manif.get_history(name)

        type_data = type(data[0]['data']).__name__ if not type_pj and data else None
        context = {
            "data": data,
            "type_pj": type_pj,
            "type_data": type_data
        }
        return render(request, "evenements/historique_popup.html", context=context)
