# coding: utf-8
from .manif import ManifDetail, ManifCreate, ManifUpdate, ManifDelete
from ..forms.dnm import DnmcForm
from ..models.dnm import Dnmc


class DnmcDetail(ManifDetail):
    """ Détail de manifestation """

    # Configuration
    model = Dnmc


class DnmcCreate(ManifCreate):
    """ Création de manifestation """

    # Configuration
    model = Dnmc
    form_class = DnmcForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dnmc'
        return context


class DnmcUpdate(ManifUpdate):
    """ Modifier une manifestation """

    # Configuration
    model = Dnmc
    form_class = DnmcForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dnmc'
        return context


class DnmcDelete(ManifDelete):
    """ Supprimer une manifestation """

    # Configuration
    model = Dnmc