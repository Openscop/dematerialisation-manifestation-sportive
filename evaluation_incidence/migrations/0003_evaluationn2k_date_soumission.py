# Generated by Django 4.0.6 on 2022-08-03 08:30
import datetime

from django.db import migrations, models
from django.utils.timezone import make_aware


def remplir_date(apps, schema_editor):
    EvalN2k = apps.get_model('evaluation_incidence', 'EvaluationN2K')
    print('')
    print('remplir champ date de soumission : ', end='')
    for index, evaln2k in enumerate(EvalN2k.objects.all()):
        if index % 500 == 0:
            print('+', end='')
        if evaln2k.manif.instruction.all().exists():
            date = evaln2k.manif.instruction.all().order_by('pk').first().date_creation
            time = datetime.time(0, 30)
            date = datetime.datetime.combine(date, time)
            evaln2k.date_soumission = make_aware(date)
            evaln2k.save()


class Migration(migrations.Migration):

    dependencies = [
        ('evaluation_incidence', '0002_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='evaluationn2k',
            name='date_soumission',
            field=models.DateTimeField(blank=True, null=True, verbose_name='date de la soumission'),
        ),
        migrations.RunPython(remplir_date),
    ]
