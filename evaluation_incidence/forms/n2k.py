# coding: utf-8
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import FormActions, AppendedText
from crispy_forms.layout import Submit, Layout, Fieldset, HTML
from django.conf import settings
from django.template.loader import render_to_string

from core.forms.base import GenericForm
from ..models import EvaluationN2K
from django import forms


IMPACTS_HELP = HTML(render_to_string('evaluation_incidence/forms/help/impacts_help.html', {'target': settings.DDT_SITE}))

N2K_INFORMATIONS_FIELDSET = [
    'diurne',
    'nocturne',
    HTML('<strong>Fréquence de la manifestation :</strong>'),
    'annuel',
    'premiere_edition',
    'autre_frequence',
    HTML('<strong>Budget de la manifestation :</strong>'),
    'cout',
]

N2K_LOCALISATION_FIELDSET = [
    HTML("{% if sites %}"
         "<div class='row' id='accordion'><div class='col-sm-3'>Rappel des sites Natura 2000 traversés :</div>"
         "<div class='col-sm-6 sitesn2k'><ul>{% for site in sites %}<li class='mb-1' id='zone_{{ site.id }}'>{{ site  }}"
         "<a class='float-end' role='button' href='#collapse{{ site.id }}' data-bs-toggle='collapse' data-bs-target='#collapse{{ site.id }}'"
         "aria-expanded='false' aria-controls='collapse{{ site.id }}'>"
         "&nbsp;&nbsp;&nbsp;<i class='rechercher'></i> voir l'opérateur</a></li>{% endfor %}</ul></div>"
         "{% for site in sites %}"
         "<table class='collapse w-100 mb-3' id='collapse{{ site.id }}' aria-labelledby='site_{{ site.id }}' data-parent='#accordion'>"
         "<thead><tr class='table-secondary'><th>Administrateur</th><th>Adresse</th><th>Commune</th><th>Opérateur(s)</th><th>E-mail</th>"
         "</tr></thead>"
         "<tbody><tr class='table-active'><td>{{ site.operateursiten2k.nom }}</td><td>{{ site.operateursiten2k.adresse|default:'' }}</td>"
         "<td>{{ site.operateursiten2k.commune|default:'' }}</td><td>{{ site.operateursiten2k.contacts|linebreaksbr }}</td>"
         "<td>{{ site.operateursiten2k.email|urlize }}</td></tr></tbody></table>{% endfor %}</div>{% endif %}"),
    'terrestre',
    'nautique',
    'aerien',
    'sensibilite',
    'lieu',
    AppendedText('Longueur_totale_parcours', 'km'),
    AppendedText('distance_site', 'km'),
    'precision',
    'impact_calcule',
]

N2K_INCIDENCES_FIELDSET = [
    'mesures_pietinement', 'pietinement_',
    'mesures_emissions_sonores', 'emissions_sonores_',
    'mesures_emissions_lumiere', 'emissions_lumiere_',
    'mesures_pollution_eau', 'pollution_eau_',
    'mesures_cours_eau', 'cours_eau_',
    'mesures_pollution_terre', 'pollution_terre_',
    'mesures_ravitaillement', 'ravitaillement_',
    'mesures_balisage', 'balisage_',
    'mesures_sensibilisation', 'sensibilisation_',
    'mesures_emprise_amenagement', 'emprise_amenagement_',
    'mesures_public', 'public_',
    'mesures_parking', 'parking_',
    'mesures_engins_aeriens', 'engins_aeriens_',
]

N2K_CONCLUSIONS_FIELDSET = [
    'impact_estime',
    'conclusion_natura_2000',
]
FORM_WIDGETS = {
    'mesures_pietinement': forms.Textarea(attrs={'rows': 1}),
    'mesures_emissions_sonores': forms.Textarea(attrs={'rows': 1}),
    'mesures_emissions_lumiere': forms.Textarea(attrs={'rows': 1}),
    'mesures_pollution_eau': forms.Textarea(attrs={'rows': 1}),
    'mesures_pollution_terre': forms.Textarea(attrs={'rows': 1}),
    'mesures_sensibilisation': forms.Textarea(attrs={'rows': 1}),
    'mesures_emprise_amenagement': forms.Textarea(attrs={'rows': 1}),
    'mesures_public': forms.Textarea(attrs={'rows': 1}),
    'mesures_parking': forms.Textarea(attrs={'rows': 1}),
    'mesures_engins_aeriens': forms.Textarea(attrs={'rows': 1}),
    'mesures_cours_eau': forms.Textarea(attrs={'rows': 1}),
    'mesures_ravitaillement': forms.Textarea(attrs={'rows': 1}),
    'mesures_balisage': forms.Textarea(attrs={'rows': 1}),
}


class EvaluationN2kForm(GenericForm):
    """ Formulaire d'évaluation N2K """

    impact_calcule = forms.CharField(label="Impact calculé", disabled=True, required=False)
    sensibilisation = forms.BooleanField(label="Sensibilisation à l'environnement", initial=True, required=False)
    precision = forms.CharField(label="Informations complémentaires à propos des parcours", required=False,
                                widget=forms.Textarea(attrs={'rows': 1, 'placeholder': 'Précisions sur les divers parcours '
                                                                                       'et leur rapport aux sites N2000'}))
    pietinement_ = forms.BooleanField(initial=False, label="Destruction par piétinement", required=False)
    emissions_sonores_ = forms.BooleanField(initial=False, label="Dérangement par le bruit", required=False)
    emissions_lumiere_ = forms.BooleanField(initial=False, label="Dérangement par émissions lumineuses nocturnes", required=False)
    pollution_eau_ = forms.BooleanField(initial=False, label="Pollution de milieux humides", required=False)
    pollution_terre_ = forms.BooleanField(initial=False, label="Pollution de milieux terrestre", required=False)
    sensibilisation_ = forms.BooleanField(initial=False, label="Sensibilisation à l'environnement", required=False)
    emprise_amenagement_ = forms.BooleanField(initial=False, label="Emprise des aménagements", required=False)
    public_ = forms.BooleanField(initial=False, label="Présence du public", required=False)
    parking_ = forms.BooleanField(initial=False, label="Stationnement des véhicules", required=False)
    engins_aeriens_ = forms.BooleanField(initial=False, label="Présence d’engins aériens", required=False)
    cours_eau_ = forms.BooleanField(initial=False, label="Traversée des cours d'eau", required=False)
    ravitaillement_ = forms.BooleanField(initial=False, label="Zone de ravitaillement", required=False)
    balisage_ = forms.BooleanField(initial=False, label="Utilisation de balisage", required=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        # Prendre la valeur des champs incidences pour positionner le champ de remplacement
        if 'instance' in kwargs:
            for champ in ['pietinement', 'emissions_sonores', 'emissions_lumiere', 'pollution_eau', 'pollution_terre', 'sensibilisation',
                          'emprise_amenagement', 'public', 'parking', 'engins_aeriens', 'cours_eau', 'ravitaillement', 'balisage']:
                if hasattr(kwargs['instance'], champ) and not getattr(kwargs['instance'], champ):
                    if 'initial' in kwargs:
                        kwargs['initial'][champ + "_"] = True

        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset("Informations générales", *N2K_INFORMATIONS_FIELDSET),
            Fieldset("Localisation de la manifestation", *N2K_LOCALISATION_FIELDSET),
            Fieldset("Incidences potentielles", *N2K_INCIDENCES_FIELDSET, css_class="offset_remove"),
            Fieldset("Conclusions", *N2K_CONCLUSIONS_FIELDSET),
            FormActions(
                Submit('save', "soumettre".capitalize()),
                HTML('<a class="btn btn-danger", href="javascript:history.back()">Annuler</a>'),
            )
        )
        if self.instance.manif and self.instance.manif.cerfa.get_liste_champs_a_modifier():
            self.ajouter_css_class_modifier_aux_champs_demandes('n2k')
        else:
            self.ajouter_css_class_requis_aux_champs_requis()

    def clean(self):
        cleaned_data = super().clean()
        # Convertir la valeur des champs de remplacement aux champs incidences
        for champ in ['pietinement', 'emissions_sonores', 'emissions_lumiere', 'pollution_eau', 'pollution_terre', 'sensibilisation',
                      'emprise_amenagement', 'public', 'parking', 'engins_aeriens', 'cours_eau', 'ravitaillement', 'balisage']:
            valeur = cleaned_data.get(champ + "_")
            if valeur:
                cleaned_data[champ] = False
            else:
                cleaned_data[champ] = True
        for data in self.changed_data:
            if self.fields[data].widget.__class__.__name__ == "Textarea" and cleaned_data[data] and not data == 'history_json':
                cleaned_data[data] = cleaned_data[data].replace("<", "&lt;").replace(">", "&gt;")
        # Vérifier les datas modifiées
        if self.instance and hasattr(self.instance, 'manif') and self.instance.manif and self.instance.manif.get_instruction():
            if self.instance.date_soumission:
                if self.instance.manif.modif_json:
                    liste_champs_a_modifier = []
                    for formulaire, champ, date in self.instance.manif.get_cerfa().get_liste_champs_a_modifier():
                        if formulaire == 'n2k':
                            liste_champs_a_modifier.append(champ)
                    if not all(change in liste_champs_a_modifier or change[:-1] in liste_champs_a_modifier or change == "history_json" for change in self.changed_data):
                        raise forms.ValidationError("Des champs modifiés ne sont pas autorisés")

        return cleaned_data

    # Meta
    class Meta:
        model = EvaluationN2K
        exclude = ('manif',)
        widgets = FORM_WIDGETS
        help_texts = {
            'sensibilite': "Veuillez prendre contact avec le (ou les) opérateur(s) de site éventuel(s) pour connaître la sensibilité de la zone"}


class DemandeN2kForm(forms.ModelForm):
    """ Formulaire de demande d'évaluation N2k par l'instructeur du dossier """
    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_id = 'demandereval'
        self.helper.label_class = 'col-sm-12'
        self.helper.field_class = 'col-sm-12 overflow-auto'
        self.helper.layout = Layout(
            Fieldset(
                "Motivation de la demande".capitalize(),
                'pourquoi_instructeur', 'pourquoi_organisateur'
            ),
            FormActions(
                Submit('save', "soumettre".capitalize())
            )
        )

    # Meta
    class Meta:
        model = EvaluationN2K
        fields = ['pourquoi_instructeur', 'pourquoi_organisateur']
        widgets = {'pourquoi_instructeur': forms.Textarea(attrs={'rows': 1}),
                   'pourquoi_organisateur': forms.Textarea(attrs={'rows': 1})}
