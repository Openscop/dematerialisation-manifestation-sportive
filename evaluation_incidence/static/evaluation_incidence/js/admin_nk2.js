$(document).ready(function () {
    if ($("#id_operateursiten2k-0-departement").val() && !$("#id_operateursiten2k-0-commune").val()) {
        $.ajax({
            type: "GET",
            url: "/administrative_division/ajax/commune/",
            data: {
                'departement': $("#id_operateursiten2k-0-departement").val(),
            },
            success: function(data){
                let reponse = JSON.parse(data)
                $("#id_operateursiten2k-0-commune").empty().append('<option value=""></option>')
                for (i = 0; i < reponse.length; i++){
                    $("#id_operateursiten2k-0-commune").append(`<option value="${reponse[i][0]}">${reponse[i][1]}</option>`)
                }
            },
            error: function(){
                console.table("Une erreur est survenu");
            }
        });
    }
})