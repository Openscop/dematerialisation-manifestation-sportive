/**
 * Created by knt on 23/02/17.
 */
$(document).ready(function () {


    $("textarea").autogrow({flickering: false, horizontal: false});

    // Mise en forme du formulaire
    $('div[id*="mesures"]').addClass('ms-5');
    $('div[id*="description"]').addClass('ms-5');
    $('.offset_remove .form-check').each(function () {
        $(this).parent().removeClass().addClass('col-sm-9');
    });

    // BOF Modification pour l'édition de l'évaluation
    // Marquer les champs de classe "requis"
    $('.requis').each(function () {
        var div_id = '#div_' + $(this).attr('id');
        $(div_id).append('<div class="col-sm-2 text-end completer"></div>');
        if ($(this).val() == '') {
            $(div_id).children('.completer').append('<i class="a-completer"></i> à completer</div>');
        }
    });
    // Marquer les champs de classe "modifier"
    $('.modifier').each(function () {
        var div_id = '#div_' + $(this).attr('id');
        $(div_id).append('<div class="col-sm-3 text-center a-modifier"></div>');
        $(div_id).children('.a-modifier').append('<div><i class="a-completer"></i> à modifier</div>');
    });
    AfficherIcones();
    // Gérer le marquage en fonction du remplissage
    $('body').on('change', '.requis', function () {
        var div_id = '#div_' + $(this).attr('id');
        if ($(this).val() != '') {
            $(div_id).children('.completer').empty();
        } else {
            $(div_id).children('.completer').append('<i class="a-completer"></i> à completer');
        }
        AfficherIcones();
    });
    // Gérer le marquage en fonction du remplissage pour les champs 'date'
    $('.requis').on('dp.change', function () {
        var div_id = '#div_' + $(this).attr('id');
        if ($(this).val() != '') {
            $(div_id).children('.completer').empty();
        } else {
            $(div_id).children('.completer').append('<i class="a-completer"></i> à completer');
        }
        AfficherIcones();
    });
    // EOF

    // BOF Gérer l'affichage des champs de description
    $('[id^="div_id_mesures"]').hide();
    $('[class*="ctx-help"]').hide();
    $('[class*="btn_mesures"]').hide();
    $('[type|="checkbox"]').each(function () {
        if ($(this).is(':not(:checked)')) {
            let desc = $(this).attr("name").slice(0,-1);
            let desc_assoc = "mesures_" + desc;
            $('[class^="' + desc + '_ctx-help"]').show();
            $('#div_id_' + desc_assoc).show();
            $('.btn_' + desc_assoc).show();
        }
    });
    // Suivant selection
    $('[type|="checkbox"]').on('change', function () {
        let desc = $(this).attr("name").slice(0,-1);
        let desc_assoc = "mesures_" + desc;
        let desc_div = $('#div_id_' + desc_assoc);
        desc_div.toggle("fast", "swing");
        if ($(this).is(':not(:checked)')) {
            $('.btn_' + desc_assoc).show("slow");
            $('[class^="' + desc + '_ctx-help"]').show("slow");
        } else {
            $('.btn_' + desc_assoc).hide("slow");
            $('[class*="' + desc + '_ctx-help"]').hide("slow");
        }
        if (desc_div.is(":visible")) {
            $('[name^="' + desc_assoc + '"]').addClass('requis');
            if (desc_div.children('.completer').length == 0) {
                desc_div.append('<div class="col-sm-2 text-end completer"></div>');
            }
            desc_div.children('.completer').empty();
            if ($('[name^="' + desc_assoc + '"]').val() == '') {
                desc_div.children('.completer').append('<i class="a-completer"></i> à completer</div>');
            }
        }
        AfficherIcones();
    });
    // Avec le bouton
    $('[class*="btn_mesures"]').on('click', function (e) {
        e.preventDefault();
        let desc_assoc = $(this).attr("data-champ");
        $('.btn_' + desc_assoc).toggle("fast");
        $('[class^="' + desc_assoc + '_ctx-help"]').toggle("slow");
    });

    // Gérer les champs visible avec milieu "Terrestre"
    if ($('[name="terrestre"]').length != 0 && !$('[name="terrestre"]').is(':checked')) {
        $('#div_id_lieu').hide();
        $('[id^="div_id"][id$="pietinement"]').hide();
        $('.btn_mesures_pietinement').hide();
        $('[class*="pietinement"]').hide();
    }
    $('body').on('change', '[name="terrestre"]', function () {
        $('#div_id_lieu').toggle("slow");
        $('#titre_pietinement').toggle("slow");
        $('#div_id_pietinement_').toggle("slow");
        if ($('[name="terrestre"]').is(':checked')) {
            $('[name="pietinement_"]').prop('checked', false);
            $('#div_id_mesures_pietinement').show("slow");
            $('.btn_mesures_pietinement').show("slow");
            $('[class^="pietinement"]').show("slow");
        } else {
            $('[name="pietinement_"]').prop('checked', true);
            $('#div_id_mesures_pietinement').hide("slow");
            $('.btn_mesures_pietinement').hide("slow");
            $('[class^="pietinement"]').hide("slow");
        }
        $('[class^="mesures_pietinement"]').hide();
    });

    // Affichage de l'impact
    function aff_impact(indice) {
        if (indice == 2) {
            $('#id_impact_calcule').val('Impact FORT');
        } else if (indice == 1) {
            $('#id_impact_calcule').val('Impact MOYEN');
        } else {
            $('#id_impact_calcule').val('Impact FAIBLE');
        }
    }
    // Calcul de l'impact
    // Variable "impact" déjà positionnée par la vue et transmise par le template
    // 2 pour avtm.vehicules > 250, 1 pour avtm.vehicules entre 100 et 250 et 0 pour avtm.vehicules < 100
    if ($('[name="sensibilite"]').is(':checked')) {
        if ($('.sitesn2k ul').children().length != 0) {
            impact = 2;
        }
    } else {
        if (impact != 2) {
            if ($('.sitesn2k ul').children().length != 0) {
                impact = 1;
            }
        }
    }
    $('body').on('change', '[name="sensibilite"]', function () {
        if ($('[name="sensibilite"]').is(':checked')) {
            if ($('.sitesn2k ul').children().length != 0) {
                impact = 2;
            }
        } else {
            if (impact != 2) {
                if ($('.sitesn2k ul').children().length != 0) {
                    impact = 1;
                }
            }
        }
        aff_impact(impact);
    });
    aff_impact(impact);

    // Gérer les champs dépendants. Pas de changement possible sur la page, les sites n2k ne sont pas modifiable içi
    if ($('#id_impact_calcule').val() == 'Impact FAIBLE') {
        $('[id^="div_id"][id*="emprise_amenagement"]').hide();
        $('[id^="titre_"][id$="emprise_amenagement"]').hide();
        $('[name="emprise_amenagement_"]').prop('checked', true);
        $('[class*="emprise_amenagement"]').hide();
        $('[id^="div_id"][id*="public"]').hide();
        $('[id^="titre_"][id$="public"]').hide();
        $('[name="public_"]').prop('checked', true);
        $('[class*="public"]').hide();
        $('[id^="div_id"][id*="parking"]').hide();
        $('[id^="titre_"][id$="parking"]').hide();
        $('[name="parking_"]').prop('checked', true);
        $('[class*="parking"]').hide();
        $('[id^="div_id"][id*="engins_aeriens"]').hide();
        $('[id^="titre_"][id$="engins_aeriens"]').hide();
        $('[name="engins_aeriens_"]').prop('checked', true);
        $('[class*="engins_aeriens"]').hide();
    }
});
