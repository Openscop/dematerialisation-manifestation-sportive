# coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import method_decorator
from django.utils import timezone

from evaluation_incidence.models import RnrZone, EvaluationRnr
from evaluation_incidence.forms import EvaluationRNRForm
from administrative_division.models.departement import Departement
from evenements.decorators import verifier_acces_evenement
from evenements.models import Manif
from core.models import Instance


class RNRListe(ListView):
    """ Afficher la liste des réserves naturelles régionales """

    # Configuration
    model = RnrZone

    def get_queryset(self):
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement().name if instance.get_departement() else ''
        param = self.kwargs.get('dept')
        if param and param.isdecimal() and param != '0':
            departement = param
        if departement and not param == '0':
            return RnrZone.objects.filter(departements__name=departement).order_by('nom')
        return RnrZone.objects.all().order_by('nom')

    def get_context_data(self, **kwargs):
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement().name if instance.get_departement() else ''
        context = super().get_context_data(**kwargs)
        param = self.kwargs.get('dept')
        if param and param.isdecimal() and param != '0':
            departement = param
        if departement and not param == '0':
            context['current_departement'] = departement
        context['departements'] = Departement.objects.all()
        return context


class RNREvalCreate(CreateView):
    """ Création d'évaluation RNR """

    # Configuration
    model = EvaluationRnr
    form_class = EvaluationRNRForm

    # Overrides
    @method_decorator(verifier_acces_evenement())
    def dispatch(self, *args, **kwargs):
        self.manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if EvaluationRnr.objects.filter(manif=self.manif).exists():
            return redirect(self.manif.get_absolute_url())
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dept'] = self.manif.ville_depart.get_departement().name
        context['zones'] = self.manif.zones_rnr.all()
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.manif = self.manif
        return super().form_valid(form)


@method_decorator(verifier_acces_evenement(), name='dispatch')
class RNREvalUpdate(UpdateView):
    """ Modification d'évaluation RNR """

    # Configuration
    model = EvaluationRnr
    form_class = EvaluationRNRForm

    # Overrides
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dept'] = self.get_object().manif.ville_depart.get_departement().name
        context['zones'] = self.get_object().manif.zones_rnr.all()
        return context

    def form_valid(self, form):
        history_json = self.object.history_json
        if not history_json:
            history_json = []
        if form.changed_data and form.instance.manif.get_instruction():
            for changed_data in form.changed_data:
                if not changed_data in ["modif_json", 'history_json']:
                    old_data = form.initial.get(changed_data)
                    date_changement = timezone.now()
                    if str(type(old_data).__name__) == "datetime":
                        old_data = old_data.astimezone().strftime("%d/%m/%Y %H:%M")
                    elif str(type(old_data).__name__) == 'list':
                        temp = [str(x) for x in old_data]
                        old_data = temp
                    else:
                        old_data = str(old_data)
                    history_json.append({"champ": changed_data, "data": old_data, "date": str(date_changement),
                                         "type": str(type(old_data).__name__)})
        retour = super().form_valid(form)
        self.object.refresh_from_db()
        self.object.history_json = history_json
        self.object.save()
        if self.object.manif.get_instruction():
            # Marquer la line de demande de modification DONE si elle existe
            if self.object.manif.modif_json:
                for idx, line in enumerate(self.object.manif.modif_json):
                    if ('champ_rnr' in line and
                            (line['champ_rnr'] in form.changed_data or line['champ_rnr'] + "_" in form.changed_data) and
                            not line['done']):
                        self.object.manif.modif_json[idx]['done'] = True
                        self.object.notifier_eval_modifiee(line['champ_rnr'])
                        self.object.manif.save()
        return retour
