# coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import method_decorator
from django.utils import timezone

from evaluation_incidence.models import N2kSite, EvaluationN2K
from evaluation_incidence.forms import EvaluationN2kForm
from administrative_division.models.departement import Departement
from evenements.decorators import verifier_acces_evenement
from evenements.models import Manif
from core.models import Instance


class SiteN2KListe(ListView):
    """ Afficher la liste des sites Natura 2000 """

    # Configuration
    model = N2kSite

    def get_queryset(self):
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement().name if instance.get_departement() else ''
        param = self.kwargs.get('dept')
        if param and param.isdecimal() and param != '0':
            departement = param
        if departement and not param == '0':
            return N2kSite.objects.filter(departements__name=departement).order_by('nom')
        return N2kSite.objects.all().order_by('nom')

    def get_context_data(self, **kwargs):
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement().name if instance.get_departement() else ''
        context = super().get_context_data(**kwargs)
        param = self.kwargs.get('dept')
        if param and param.isdecimal() and param != '0':
            departement = param
        if departement and not param == '0':
            context['current_departement'] = departement
        context['departements'] = Departement.objects.all()
        return context


class N2kEvalCreate(CreateView):
    """ Création d'évaluation N2K """

    # Configuration
    model = EvaluationN2K
    form_class = EvaluationN2kForm

    # Overrides
    @method_decorator(verifier_acces_evenement)
    def dispatch(self, *args, **kwargs):
        self.manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if EvaluationN2K.objects.filter(manif=self.manif).exists():
            return redirect(self.manif.get_absolute_url())
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        milieu = self.manif.activite.discipline.get_milieu_display()
        if 'diurne' not in kwargs['initial']:
            kwargs['initial'].update({'diurne': True})
        if milieu == 'Terrestre':
            kwargs['initial'].update({'terrestre': True})
        elif milieu == 'Nautique':
            kwargs['initial'].update({'nautique': True})
        elif milieu == 'Aérien':
            kwargs['initial'].update({'aerien': True})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        impact = 0
        if self.manif.get_type_manif() == 'avtm':
            if self.manif.cerfa.vehicules:
                if self.manif.cerfa.vehicules > 250:
                    impact = 2
                elif self.manif.cerfa.vehicules >= 100:
                    impact = 1
        context['impact'] = impact
        context['dept'] = self.manif.ville_depart.get_departement().name
        context['sites'] = self.manif.sites_natura2000.all()
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.manif = self.manif
        return super().form_valid(form)


@method_decorator(verifier_acces_evenement(), name='dispatch')
class N2kEvalUpdate(UpdateView):
    """ Modification de l'évaluation N2K """

    # Configuration
    model = EvaluationN2K
    form_class = EvaluationN2kForm

    # Overrides
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        manif = self.get_object().manif
        milieu = manif.activite.discipline.get_milieu_display()
        if milieu == 'Terrestre':
            kwargs['initial'].update({'terrestre': True})
        elif milieu == 'Nautique':
            kwargs['initial'].update({'nautique': True})
        elif milieu == 'Aérien':
            kwargs['initial'].update({'aerien': True})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        manif = self.get_object().manif
        impact = 0
        if manif.get_type_manif() == 'avtm':
            if manif.cerfa.vehicules:
                if manif.cerfa.vehicules > 250:
                    impact = 2
                elif manif.cerfa.vehicules >= 100:
                    impact = 1
        context['impact'] = impact
        context['dept'] = manif.ville_depart.get_departement().name
        context['sites'] = manif.sites_natura2000.all()
        return context

    def form_valid(self, form):
        history_json = self.object.history_json
        if not history_json:
            history_json = []
        if form.instance.date_soumission and form.changed_data and form.instance.manif.get_instruction():
            for changed_data in form.changed_data:
                if not changed_data in ["modif_json", 'history_json']:
                    old_data = form.initial.get(changed_data)
                    date_changement = timezone.now()
                    if str(type(old_data).__name__) == "datetime":
                        old_data = old_data.astimezone().strftime("%d/%m/%Y %H:%M")
                    elif str(type(old_data).__name__) == 'list':
                        temp = [str(x) for x in old_data]
                        old_data = temp
                    else:
                        old_data = str(old_data)
                    history_json.append({"champ": changed_data, "data": old_data, "date": str(date_changement),
                                         "type": str(type(old_data).__name__)})
        retour = super().form_valid(form)
        self.object.refresh_from_db()
        self.object.history_json = history_json
        self.object.save()
        if self.object.manif.get_instruction():
            modification_champ = False
            # Marquer la line de demande de modification DONE si elle existe
            if self.object.manif.modif_json:
                for idx, line in enumerate(self.object.manif.modif_json):
                    if ('champ_n2k' in line and
                            (line['champ_n2k'] in form.changed_data or line['champ_n2k'] + "_" in form.changed_data) and
                            not line['done']):
                        self.object.manif.modif_json[idx]['done'] = True
                        self.object.notifier_eval_modifiee(line['champ_n2k'])
                        self.object.manif.save()
                        # Il s'agit donc d'une modification de champ demandé par l'instructeur
                        modification_champ = True
            # S'il ne s'agit pas d'une modification de champs, c'est qu'il s'agit d'une nouvelle demande d'évaluation n2k par l'instructeur.
            if not modification_champ:
                self.object.manif.notifier_manif_modifiee("évaluation Natura2000")
        return retour
