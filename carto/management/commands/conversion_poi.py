# coding: utf-8
from django.core.management.base import BaseCommand
from carto.models import Poi

tableau = [
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-1",
            "title": "Poi.1",
            "type": "default"
        },
        "apres" : {
            "id": "1",
            "name": "Poi.1",
            "i18n": "signaleur",
            "html_code": "&#65",
            "type": "default"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-5",
            "title": "Poi.5",
            "type": "default"
        },
        "apres" : {
            "id": "5",
            "name": "Poi.5",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#66"
        }
    },

    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-2",
            "title": "Poi.2",
            "type": "default"
        },
        "apres" : {
            "id": "2",
            "name": "Poi.2",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#67"
        }
    },

    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-4",
            "title": "Poi.4",
            "type": "default"
        },
        "apres" : {
            "id": "4",
            "name": "Poi.4",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#68"
        }
    },

    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-45",
            "title": "Poi.45",
            "type": "default"
        },
        "apres" : {
            "id": "45",
            "name": "Poi.45",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#69"
        }
    },

    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-54",
            "title": "Poi.54",
            "type": "default"
        },
        "apres" : {
            "id": "54",
            "name": "Poi.54",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#70"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-55",
            "title": "Poi.55",
            "type": "default"
        },
        "apres" : {
            "id": "55",
            "name": "Poi.55",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#71"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-46",
            "title": "Poi.46",
            "type": "default"
        },
        "apres" : {
            "id": "46",
            "name": "Poi.46",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#73"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-42",
            "title": "Poi.42",
            "type": "default"
        },
        "apres" : {
            "id": "42",
            "name": "Poi.42",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#74"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-43",
            "title": "Poi.43",
            "type": "default"
        },
        "apres" : {
            "id": "43",
            "name": "Poi.43",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#75"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-47",
            "title": "Poi.47",
            "type": "default"
        },
        "apres" : {
            "id": "47",
            "name": "Poi.47",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#76"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-51",
            "title": "Poi.51",
            "type": "default"
        },
        "apres" : {
            "id": "51",
            "name": "Poi.51",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#77"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-70",
            "title": "Poi.70",
            "type": "default"
        },
        "apres" : {
            "id": "70",
            "name": "Poi.70",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#78"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-71",
            "title": "Poi.71",
            "type": "default"
        },
        "apres" : {
            "id": "71",
            "name": "Poi.71",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#79"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-69",
            "title": "Poi.69",
            "type": "default"
        },
        "apres" : {
            "id": "69",
            "name": "Poi.69",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#80"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-20",
            "title": "Poi.20",
            "type": "default"
        },
        "apres" : {
            "id": "20",
            "name": "Poi.20",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#81"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-21",
            "title": "Poi.21",
            "type": "default"
        },
        "apres" : {
            "id": "21",
            "name": "Poi.21",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#82"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-75",
            "title": "Poi.75",
            "type": "default"
        },
        "apres" : {
            "id": "75",
            "name": "Poi.75",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#83"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-61",
            "title": "Poi.61",
            "type": "default"
        },
        "apres" : {
            "id": "61",
            "name": "Poi.61",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#84"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-3",
            "title": "Poi.3",
            "type": "default"
        },
        "apres" : {
            "id": "3",
            "name": "Poi.3",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#85"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-49",
            "title": "Poi.49",
            "type": "default"
        },
        "apres" : {
            "id": "49",
            "name": "Poi.49",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#86"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-104",
            "title": "Poi.104",
            "type": "default"
        },
        "apres" : {
            "id": "104",
            "name": "Poi.104",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#87"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-103",
            "title": "Poi.103",
            "type": "default"
        },
        "apres" : {
            "id": "103",
            "name": "Poi.103",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#88"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-50",
            "title": "Poi.50",
            "type": "default"
        },
        "apres" : {
            "id": "50",
            "name": "Poi.50",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#89"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-48",
            "title": "Poi.48",
            "type": "default"
        },
        "apres" : {
            "id": "48",
            "name": "Poi.48",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#90"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-72",
            "title": "Poi.72",
            "type": "default"
        },
        "apres" : {
            "id": "72",
            "name": "Poi.72",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#97"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-114",
            "title": "Poi.114",
            "type": "default"
        },
        "apres" : {
            "id": "114",
            "name": "Poi.114",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#98"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-60",
            "title": "Poi.60",
            "type": "default"
        },
        "apres" : {
            "id": "60",
            "name": "Poi.60",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#99"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-6",
            "title": "Poi.6",
            "type": "default"
        },
        "apres" : {
            "id": "6",
            "name": "Poi.6",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#100"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-52",
            "title": "Poi.52",
            "type": "default"
        },
        "apres" : {
            "id": "52",
            "name": "Poi.52",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#101"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-53",
            "title": "Poi.53",
            "type": "default"
        },
        "apres" : {
            "id": "53",
            "name": "Poi.53",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#102"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-105",
            "title": "Poi.105",
            "type": "default"
        },
        "apres" : {
            "id": "105",
            "name": "Poi.105",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#103"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-62",
            "title": "Poi.62",
            "type": "default"
        },
        "apres" : {
            "id": "62",
            "name": "Poi.62",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#104"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-63",
            "title": "Poi.63",
            "type": "default"
        },
        "apres" : {
            "id": "63",
            "name": "Poi.63",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#105"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-64",
            "title": "Poi.64",
            "type": "default"
        },
        "apres" : {
            "id": "64",
            "name": "Poi.64",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#106"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-65",
            "title": "Poi.65",
            "type": "default"
        },
        "apres" : {
            "id": "65",
            "name": "Poi.65",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#107"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-66",
            "title": "Poi.66",
            "type": "default"
        },
        "apres" : {
            "id": "66",
            "name": "Poi.66",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#108"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-17",
            "title": "Poi.17",
            "type": "default"
        },
        "apres" : {
            "id": "17",
            "name": "Poi.17",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#109"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-19",
            "title": "Poi.19",
            "type": "default"
        },
        "apres" : {
            "id": "19",
            "name": "Poi.19",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#110"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-38",
            "title": "Poi.38",
            "type": "default"
        },
        "apres" : {
            "id": "38",
            "name": "Poi.38",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#112"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-40",
            "title": "Poi.40",
            "type": "default"
        },
        "apres" : {
            "id": "40",
            "name": "Poi.40",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#114"
        }
    },
    {
        "avant" :  {
            "id": "or-icon icon-poi-symbol-41",
            "title": "Poi.41",
            "type": "default"
        },
        "apres" : {
            "id": "41",
            "name": "Poi.41",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#115"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-7",
            "title": "Poi.7",
            "type": "default"
        },
        "apres" : {
            "id": "7",
            "name": "Poi.7",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#116"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-8",
            "title": "Poi.8",
            "type": "default"
        },
        "apres" : {
            "id": "8",
            "name": "Poi.8",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#117"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-9",
            "title": "Poi.9",
            "type": "default"
        },
        "apres" : {
            "id": "9",
            "name": "Poi.9",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#118"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-10",
            "title": "Poi.10",
            "type": "default"
        },
        "apres" : {
            "id": "10",
            "name": "Poi.10",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#119"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-14",
            "title": "Poi.14",
            "type": "default"
        },
        "apres" : {
            "id": "14",
            "name": "Poi.14",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#120"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-15",
            "title": "Poi.15",
            "type": "default"
        },
        "apres" : {
            "id": "15",
            "name": "Poi.15",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#121"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-16",
            "title": "Poi.16",
            "type": "default"
        },
        "apres" : {
            "id": "16",
            "name": "Poi.16",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#122"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-36",
            "title": "Poi.36",
            "type": "default"
        },
        "apres" : {
            "id": "36",
            "name": "Poi.36",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#49"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-109",
            "title": "Poi.109",
            "type": "default"
        },
        "apres" : {
            "id": "109",
            "name": "Poi.109",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#50"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-110",
            "title": "Poi.110",
            "type": "default"
        },
        "apres" : {
            "id": "110",
            "name": "Poi.110",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#51"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-111",
            "title": "Poi.111",
            "type": "default"
        },
        "apres" : {
            "id": "111",
            "name": "Poi.111",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#52"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-112",
            "title": "Poi.112",
            "type": "default"
        },
        "apres" : {
            "id": "112",
            "name": "Poi.112",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#53"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-34",
            "title": "Poi.34",
            "type": "default"
        },
        "apres" : {
            "id": "34",
            "name": "Poi.34",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#55"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-56",
            "title": "Poi.56",
            "type": "default"
        },
        "apres" : {
            "id": "56",
            "name": "Poi.56",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#56"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-57",
            "title": "Poi.57",
            "type": "default"
        },
        "apres" : {
            "id": "57",
            "name": "Poi.57",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#57"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-59",
            "title": "Poi.59",
            "type": "default"
        },
        "apres" : {
            "id": "59",
            "name": "Poi.59",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#38"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-67",
            "title": "Poi.67",
            "type": "default"
        },
        "apres" : {
            "id": "67",
            "name": "Poi.67",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#233"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-68",
            "title": "Poi.68",
            "type": "default"
        },
        "apres" : {
            "id": "68",
            "name": "Poi.68",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#232"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-107",
            "title": "Poi.107",
            "type": "default"
        },
        "apres" : {
            "id": "107",
            "name": "Poi.107",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#40"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-108",
            "title": "Poi.108",
            "type": "default"
        },
        "apres" : {
            "id": "108",
            "name": "Poi.108",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#41"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-35",
            "title": "Poi.35",
            "type": "default"
        },
        "apres" : {
            "id": "35",
            "name": "Poi.35",
            "i18n": "signaleur",
            "type": "default",
            "html_code": "&#59"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-200",
            "title": "Poi.200",
            "type": "split"
        },
        "apres" : {
            "id": "200",
            "name": "Poi.200",
            "i18n": "signaleur",
            "type": "split",
            "html_code": "&#58"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-201",
            "title": "Poi.201",
            "type": "split"
        },
        "apres" : {
            "id": "201",
            "name": "Poi.201",
            "i18n": "signaleur",
            "type": "split",
            "html_code": "&#46"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-14",
            "title": "Poi.14",
            "type": "split"
        },
        "apres" : {
            "id": "204",
            "name": "Poi.204",
            "i18n": "signaleur",
            "type": "split",
            "html_code": "&#120"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-202",
            "title": "Poi.202",
            "type": "split"
        },
        "apres" : {
            "id": "202",
            "name": "Poi.202",
            "i18n": "signaleur",
            "type": "split",
            "html_code": "&#249"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-poi-symbol-203",
            "title": "Poi.203",
            "type": "split"
        },
        "apres" : {
            "id": "203",
            "name": "Poi.203",
            "i18n": "signaleur",
            "type": "split",
            "html_code": "&#37"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-ed-poi-text",
            "title": "Poi.text",
            "type": "text"
        },
        "apres" : {
            "id": "400",
            "name": "Poi.400",
            "i18n": "Text",
            "type": "text",
            "html_code": "&#59748"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-ed-poi-text",
            "title": "Poi.text",
            "type": "text"
        },
        "apres" : {
            "id": "400",
            "name": "Poi.400",
            "i18n": "Text",
            "type": "text",
            "html_code": "&#59748"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm AES",
            "title": "vtm.AES",
            "type": "vtm"
        },
        "apres" : {
            "id": "AES",
            "name": "vtm.AES",
            "type": "vtm",
            "classes": "picto-vtm AES"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm CH",
            "title": "vtm.CH",
            "type": "vtm"
        },
        "apres" : {
            "id": "CH",
            "name": "vtm.CH",
            "type": "vtm",
            "classes": "picto-vtm CH"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm CSP",
            "title": "vtm.CSP",
            "type": "vtm"
        },
        "apres" : {
            "id": "CSP",
            "name": "vtm.CSP",
            "type": "vtm",
            "classes": "picto-vtm CSP"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm depan",
            "title": "vtm.depan",
            "type": "vtm"
        },
        "apres" : {
            "id": "depan",
            "name": "vtm.depan",
            "type": "vtm",
            "classes": "picto-vtm depan"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm DES",
            "title": "vtm.DES",
            "type": "vtm"
        },
        "apres" : {
            "id": "DES",
            "name": "vtm.DES",
            "type": "vtm",
            "classes": "picto-vtm DES"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm evac_sanitaire",
            "title": "vtm.evac_sanitaire",
            "type": "vtm"
        },
        "apres" : {
            "id": "evac_sanitaire",
            "name": "vtm.evac_sanitaire",
            "type": "vtm",
            "classes": "picto-vtm evac_sanitaire"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm extincteur",
            "title": "vtm.extincteur",
            "type": "vtm"
        },
        "apres" : {
            "id": "extincteur",
            "name": "vtm.extincteur",
            "type": "vtm",
            "classes": "picto-vtm extincteur"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm fin_zone",
            "title": "vtm.fin_zone",
            "type": "vtm"
        },
        "apres" : {
            "id": "fin_zone",
            "name": "vtm.fin_zone",
            "type": "vtm",
            "classes": "picto-vtm fin_zone"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm lignePTT",
            "title": "vtm.lignePTT",
            "type": "vtm"
        },
        "apres" : {
            "id": "lignePTT",
            "name": "vtm.lignePTT",
            "type": "vtm",
            "classes": "picto-vtm lignePTT"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm medecin",
            "title": "vtm.medecin",
            "type": "vtm"
        },
        "apres" : {
            "id": "medecin",
            "name": "vtm.medecin",
            "type": "vtm",
            "classes": "picto-vtm medecin"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm panneau-direct-ZP-D",
            "title": "vtm.panneau-direct-ZP-D",
            "type": "vtm"
        },
        "apres" : {
            "id": "panneau-direct-ZP-D",
            "name": "vtm.panneau-direct-ZP-D",
            "type": "vtm",
            "classes": "picto-vtm panneau-direct-ZP-D"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm panneau-direct-ZP-G",
            "title": "vtm.panneau-direct-ZP-G",
            "type": "vtm"
        },
        "apres" : {
            "id": "panneau-direct-ZP-G",
            "name": "vtm.panneau-direct-ZP-G",
            "type": "vtm",
            "classes": "picto-vtm panneau-direct-ZP-G"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm pietons_interdit",
            "title": "vtm.pietons_interdit",
            "type": "vtm"
        },
        "apres" : {
            "id": "pietons_interdit",
            "name": "vtm.pietons_interdit",
            "type": "vtm",
            "classes": "picto-vtm pietons_interdit"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm poste_commissaire",
            "title": "vtm.poste_commissaire",
            "type": "vtm"
        },
        "apres" : {
            "id": "poste_commissaire",
            "name": "vtm.poste_commissaire",
            "type": "vtm",
            "classes": "picto-vtm poste_commissaire"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm presignal_AES",
            "title": "vtm.presignal_AES",
            "type": "vtm"
        },
        "apres" : {
            "id": "presignal_AES",
            "name": "vtm.presignal_AES",
            "type": "vtm",
            "classes": "picto-vtm presignal_AES"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm presignal_control_hor",
            "title": "vtm.presignal_control_hor",
            "type": "vtm"
        },
        "apres" : {
            "id": "presignal_control_hor",
            "name": "vtm.presignal_control_hor",
            "type": "vtm",
            "classes": "picto-vtm presignal_control_hor"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm presignal_poste_commissaire",
            "title": "vtm.presignal_poste_commissaire",
            "type": "vtm"
        },
        "apres" : {
            "id": "presignal_poste_commissaire",
            "name": "vtm.presignal_poste_commissaire",
            "type": "vtm",
            "classes": "picto-vtm presignal_poste_commissaire"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm presignal_zebra",
            "title": "vtm.presignal_zebra",
            "type": "vtm"
        },
        "apres" : {
            "id": "presignal_zebra",
            "name": "vtm.presignal_zebra",
            "type": "vtm",
            "classes": "picto-vtm presignal_zebra"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm public_interdit",
            "title": "vtm.public_interdit",
            "type": "vtm"
        },
        "apres" : {
            "id": "public_interdit",
            "name": "vtm.public_interdit",
            "type": "vtm",
            "classes": "picto-vtm public_interdit"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm zebra-chgt-dir",
            "title": "vtm.zebra-chgt-dir",
            "type": "vtm"
        },
        "apres" : {
            "id": "zebra-chgt-dir",
            "name": "vtm.zebra-chgt-dir",
            "type": "vtm",
            "classes": "picto-vtm zebra-chgt-dir"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm zone_casque",
            "title": "vtm.zone_casque",
            "type": "vtm"
        },
        "apres" : {
            "id": "zone_casque",
            "name": "vtm.zone_casque",
            "type": "vtm",
            "classes": "picto-vtm zone_casque"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm ZP",
            "title": "vtm.ZP",
            "type": "vtm"
        },
        "apres" : {
            "id": "ZP",
            "name": "vtm.ZP",
            "type": "vtm",
            "classes": "picto-vtm ZP"
        }
    },
    {
        "avant" : {
            "id": "picto-vtm ZP_interdit_ok",
            "title": "vtm.ZP_interdit_ok",
            "type": "vtm"
        },
        "apres" : {
            "id": "ZP_interdit_ok",
            "name": "vtm.ZP_interdit_ok",
            "type": "vtm",
            "classes": "picto-vtm ZP_interdit_ok"
        }
    },
    {
        "avant" : {
            "id": "or-icon icon-ed-poi-number",
            "title": "Poi.number",
            "type": "number"
        },
        "apres" : {
            "id": "300",
            "name": "Poi.300",
            "i18n": "Number",
            "type": "number",
            "html_code": "&#59758"
        }
    },
    {
        "avant" : {'id': 'fak fa-fa-poi-1', 'type': 'default', 'title': 'Poi.1'},
        "apres" : {
            "id": "1",
            "name": "Poi.1",
            "i18n": "signaleur",
            "html_code": "A",
            "type": "default"
        }
    }
]

tableau_number = [
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-37",
        "type": "default",
        "title": "Poi.37"
      }
    },
    "apres": {
      "icon": {
        "id": "37",
        "name": "Poi.37",
        "i18n": "Point d'eau potable",
        "type": "default",
        "html_code": "&#113"
      }
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-74",
        "type": "default",
        "title": "Poi.74"
      }
    },
    "apres": {
      "icon": {
        "id": "67",
        "name": "Poi.67",
        "i18n": "Parc à vélos",
        "type": "default",
        "html_code": "&#233"
      }
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-58",
        "type": "default",
        "title": "Poi.58"
      }
    },
    "apres": {
      "icon": {
        "id": "46",
        "name": "Poi.46",
        "i18n": "signaleur",
        "type": "default",
        "html_code": "&#73"
      }
    }
  },

  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-76",
        "type": "default",
        "title": "Poi.76"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "0"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-77",
        "type": "default",
        "title": "Poi.77"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "1"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-78",
        "type": "default",
        "title": "Poi.78"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "2"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-79",
        "type": "default",
        "title": "Poi.79"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "3"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-80",
        "type": "default",
        "title": "Poi.80"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "4"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-81",
        "type": "default",
        "title": "Poi.81"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "5"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-82",
        "type": "default",
        "title": "Poi.82"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "6"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-83",
        "type": "default",
        "title": "Poi.83"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "7"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-84",
        "type": "default",
        "title": "Poi.84"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "8"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-85",
        "type": "default",
        "title": "Poi.85"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "9"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-86",
        "type": "default",
        "title": "Poi.86"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "10"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-87",
        "type": "default",
        "title": "Poi.87"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "11"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-88",
        "type": "default",
        "title": "Poi.88"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "12"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-89",
        "type": "default",
        "title": "Poi.89"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "13"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-90",
        "type": "default",
        "title": "Poi.90"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "14"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-91",
        "type": "default",
        "title": "Poi.91"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "15"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-92",
        "type": "default",
        "title": "Poi.92"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "16"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-93",
        "type": "default",
        "title": "Poi.93"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "17"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-94",
        "type": "default",
        "title": "Poi.94"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "18"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-95",
        "type": "default",
        "title": "Poi.95"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "19"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-96",
        "type": "default",
        "title": "Poi.96"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "20"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-97",
        "type": "default",
        "title": "Poi.97"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "21"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-98",
        "type": "default",
        "title": "Poi.98"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "22"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-99",
        "type": "default",
        "title": "Poi.99"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "23"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-100",
        "type": "default",
        "title": "Poi.100"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "24"
    }
  },
  {
    "avant": {
      "icon": {
        "id": "or-icon icon-poi-symbol-101",
        "type": "default",
        "title": "Poi.101"
      }
    },
    "apres": {
      "icon": {
        "id": "300",
        "name": "Poi.300",
        "i18n": "Number",
        "type": "number",
        "html_code": "&#59758"
      },
      "content": "25"
    }
  }
]

class Command(BaseCommand):
    """ Créer les utilisateurs Openrunner via l'API """
    help = "Changer les anciens pois en nouveaux"

    def handle(self, *args, **options):
        """ Exécuter la commande """
        total = Poi.objects.count()
        encours = 0
        for pois in Poi.objects.all():
            encours += 1
            print(f"{encours}/{total}")
            entre = list(filter(lambda sch: sch['avant'] == pois.properties["icon"], tableau))
            entre_deux = list(filter(lambda sch: sch['avant']['icon'] == pois.properties["icon"], tableau_number))
            if entre:
                pois.properties['icon'] = entre[0]["apres"]
                pois.save()
            elif entre_deux:
                pois.properties['icon'] = entre_deux[0]['apres']['icon']
                pois.properties['content'] = entre_deux[0]['apres']['content'] if entre_deux[0]['apres'].get('content') else ""
                pois.save()


