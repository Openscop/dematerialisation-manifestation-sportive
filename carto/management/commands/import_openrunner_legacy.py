import js2py
import sqlalchemy
import json
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from concurrent.futures import ThreadPoolExecutor

from django.core.management.base import BaseCommand
from django.contrib.gis.geos import GEOSGeometry

from carto.models import *
from evenements.models import Manif


def transformation(encoded):
    js = """function decode(encoded) {
        const len = encoded.length;
        let index = 0;
        const array = [];
        let lat = 0;
        let lng = 0;

        while (index < (len - 1)) {
          let b;
          let shift = 0;
          let result = 0;
          do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
          } while (b >= 0x20);
          const dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
          lat += dlat;

          shift = 0;
          result = 0;
          do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
          } while (b >= 0x20);

          const dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
          lng += dlng;
          array.push([lng * 1.0e-5, lat * 1.0e-5]);
        }
        return array;
      }
      decode(
      """

    js += f"\"{encoded}\""
    js += ");"
    return js2py.eval_js(js)


class Command(BaseCommand):
    args = ''
    help = "importation des parcours openrunner (necessite une base importée sur mariadb/mysql nommée openrunner)"

    def add_arguments(self, parser):
        parser.add_argument('passwd', nargs='?', type=str, default='123')

    def handle(self, *args, **options):
        maria_user = 'root'
        maria_password = options['passwd']
        engine = sqlalchemy.create_engine(f"mysql+pymysql://{maria_user}:{maria_password}@localhost:3306/carto_or")
        Base = declarative_base(engine)

        class TRIP(Base):
            __tablename__ = 'TRIP'
            __table_args__ = {'autoload': True}

        class TRIP_EXTRA(Base):
            __tablename__ = 'TRIP_EXTRA'
            __table_args__ = {'autoload': True}

        class PROFILE(Base):
            __tablename__ = 'PROFILE'
            __table_args__ = {'autoload': True}

        def loadSession():
            """"""
            metadata = Base.metadata
            Session = sessionmaker(bind=engine)
            session = Session()
            return session

        def importation(manifs):
            manif_manquante = []
            for manif in manifs:
                ok = True
                for parcour in manif.parcours_openrunner.split(","):
                    deja_la = Parcours.objects.filter(manif=manif, ancient_id=parcour)
                    if not deja_la:
                        session = loadSession()
                        res_trip_extra_count = session.query(TRIP_EXTRA).filter_by(ID_TRIP=int(parcour)).count()
                        res_trip_count = session.query(TRIP).filter_by(ID_TRIP=int(parcour)).count()
                        session.close()
                        if res_trip_count and res_trip_extra_count:
                            ok = False
                if not ok:
                    manif_manquante.append(manif)

            total = len(manif_manquante)
            count = 0
            for manif in manif_manquante:
                count += 1
                print(f"{count}/{total}")

                parcours = manif.parcours_openrunner.split(",")
                for parcour in parcours:
                    session = loadSession()
                    res_trip_extra_count = session.query(TRIP_EXTRA).filter_by(ID_TRIP=int(parcour)).count()
                    res_trip_count = session.query(TRIP).filter_by(ID_TRIP=int(parcour)).count()

                    if res_trip_count and res_trip_extra_count:
                        res_trip = session.query(TRIP).filter_by(ID_TRIP=int(parcour)).first()
                        res_trip_extra = session.query(TRIP_EXTRA).filter_by(ID_TRIP=int(parcour)).first()
                        res_profil = session.query(PROFILE).filter_by(ID_TRIP=int(parcour)).count()
                        if res_profil:
                            res_profil = session.query(PROFILE).filter_by(ID_TRIP=int(parcour)).first()
                            elevation = True
                        else:
                            elevation = {}

                        deja_la = Parcours.objects.filter(manif=manif, ancient_id=parcour)
                        if not deja_la:

                            print(parcour)
                            mode = None
                            routing = None
                            if not str(res_trip_extra.ID_ROUTING_ENGINE) == "0":
                                if str(res_trip_extra.ID_ROUTING_MODE) == '0':
                                    mode = "car"
                                elif str(res_trip_extra.ID_ROUTING_MODE) == "1":
                                    mode = "walk"
                                elif str(res_trip_extra.ID_ROUTING_MODE) == "2":
                                    mode = "bike"
                                routing = "graphhopper"
                            print(f"shape sauve-> {parcour}")
                            shape = Shape(routing_engine=routing,
                                          routing_engine_mode=mode,
                                          stroke_color=res_trip_extra.TXT_STROKE_COLOR,
                                          stroke_opacity=str(res_trip_extra.NUM_STROKE_OPACITY),
                                          stroke_width=str(res_trip_extra.NUM_STROKE_WIDTH))
                            shape.save()
                            if elevation:
                                elevation = {
                                    "max": res_profil.NUM_MAX_ALT,
                                    "min": res_profil.NUM_MIN_ALT,
                                    "ascent": res_trip.NUM_DEN_POS,
                                    "passes": [],
                                    "descent": res_trip.NUM_DEN_NEG,
                                    "interval": res_profil.NUM_INTERVAL*1000,
                                    "full_encoded": res_profil.TXT_ALT
                                }

                            print(f"parcours sauve-> {parcour}")
                            new_pacours = Parcours(manif=manif, activity=str(res_trip.ID_SPORT), name=res_trip.TXT_NAME,
                                                   keyword=res_trip.TXT_KEYWORD, description=res_trip.TXT_KEYWORD,
                                                   length_in_meter=res_trip.NUM_DISTANCE*1000, is_private=str(res_trip.FLG_PRIVATE),
                                                   is_tested_by_user=str(res_trip.FLG_TRIPUSED), difficulty=str(res_trip.ID_DIFFICULTY),
                                                   start_lat=str(res_trip.NUM_LAT_DEC_START),
                                                   start_lng=str(res_trip.NUM_LNG_DEC_START), end_lat=str(res_trip.NUM_LAT_DEC_END),
                                                   end_lng=str(res_trip.NUM_LNG_DEC_END), shape=shape, elevation=elevation,
                                                   ancient_id=str(res_trip.ID_TRIP))

                            new_pacours.save()
                            print(new_pacours)

                            # poi
                            #print(f"poi sauve-> {parcour}")
                            if res_trip_extra.TXT_ENCOD_POI:
                                pois = json.loads(res_trip_extra.TXT_ENCOD_POI.replace('\\',''))
                                for poi in pois:
                                    offx = 0
                                    offy = 0
                                    grid_position = poi.get("grid_position", "0000")
                                    if grid_position == "1000":
                                        offx = -1
                                    elif grid_position == "0100":
                                        offy = 1
                                    elif grid_position == "0010":
                                        offx = 1
                                    elif grid_position == "0001":
                                        offy = -1
                                    elif grid_position == "1100":
                                        offx = -1
                                        offy = 1
                                    elif grid_position == "1001":
                                        offx = -1
                                        offy = -1
                                    elif grid_position == "0011":
                                        offx = 1
                                        offy = -1
                                    elif grid_position == "0110":
                                        offx = 1
                                        offy = 1
                                    properties = {
                                      "icon": {
                                        "id": f"or-icon icon-poi-symbol-{poi.get('poi_id', '')}",
                                        "type": "default",
                                        "title": f"Poi.{poi.get('poi_id', '')}"
                                      },
                                      "type": "default",
                                      "color": "#fff",
                                      "title": poi.get('poi_title', ''),
                                      "bgColor": "#3e005d",
                                      "content": "",
                                      "offsetX": offx,
                                      "offsetY": offy,
                                      "tooltip": {
                                        "show": False,
                                        "direction": "right",
                                        "permanent": False
                                      },
                                      "visibility": "private",
                                      "showOnPrint": True,
                                      "showOnPrintMap": False,
                                      "showOnElevation": False,
                                      "cumulatedLocation": False,
                                      "showOnPrintElevation": False,
                                      "stickedDistanceInMeter": -1,
                                      "showTextOnPrintElevation": False
                                    }
                                    geometry = {
                                      "type": "Point",
                                      "coordinates": [
                                        poi.get('poi_lng'),
                                        poi.get('poi_lat')
                                      ]
                                    }
                                    Poi(properties=properties, geometry=GEOSGeometry(json.dumps(geometry)),
                                                   parcours=new_pacours).save()

                            #print(f"waypoint sauve-> {parcour}")
                            waypoints = json.loads(str(transformation(res_trip_extra.TXT_ENCOD_POINTS.replace('\\', '\\\\'))))
                            for waypoint in waypoints:
                                properties = {
                                    "type": "A"
                                  }
                                geometry = {
                                    "type": "Point",
                                    "coordinates": [
                                      waypoint[0],
                                      waypoint[1]
                                    ]
                                  }
                                PointWaypointEncoded(shape=shape, properties=properties,
                                                     geometry=GEOSGeometry(json.dumps(geometry))).save()
                            #print(f"shape sauve-> {parcour}")
                            if res_trip_extra.TXT_ENCOD_ROUPOINTS:
                                shapeencodeds = json.loads(str(transformation(res_trip_extra.TXT_ENCOD_ROUPOINTS.replace('\\', '\\\\'))))
                                for shapeencoded in shapeencodeds:
                                    properties = {}
                                    geometry = {
                                        "type": "Point",
                                        "coordinates": [
                                            shapeencoded[0],
                                            shapeencoded[1]
                                        ]
                                    }
                                    PointShapeEncoded(shape=shape, properties=properties,
                                                      geometry=GEOSGeometry(json.dumps(geometry))).save()
                    else:
                        print(f"rien pour-> {parcour}")
                    session.close()

        def chunks(lst, n):
            """Yield successive n-sized chunks from lst."""
            for i in range(0, len(lst), n):
                yield lst[i:i + n]


        manifs = Manif.objects.filter(parcours_openrunner__isnull=False).exclude(
                parcours_openrunner="")
        size = int(len(manifs) / 1)
        print(size)

        with ThreadPoolExecutor(max_workers=1) as executor:
            for page in list(chunks(manifs, size)):
                future = executor.submit(importation, manifs)
                print(f"future{future.result()}")
        print('finit')