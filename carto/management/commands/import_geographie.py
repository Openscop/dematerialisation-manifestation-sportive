from django.core.management.base import BaseCommand
from carto.models import CommuneGeo, ArrondissementsGeo, DepartementGeo, NK2000Geo, RnrZoneGeo
from administrative_division.models import Commune, Arrondissement, Departement
from django.contrib.gis.db import models

from evaluation_incidence.models import N2kSite, RnrZone


class Command(BaseCommand):
    """ Refaire les couches géographique en les important du geoserver """
    help = "Refaire les couches géographique en les important du geoserver "

    def handle(self, *args, **options):


        print("choississez l'import à faire")
        print('0=> tout, 1=> communes, 2=> arrondissement, 3=> departement')
        data = input()

        if data in ['0', '1']:
            class GeoCommunes(models.Model):
                gid = models.AutoField(primary_key=True)
                id = models.CharField(max_length=24, blank=True, null=True)
                nom = models.CharField(max_length=50, blank=True, null=True)
                nom_m = models.CharField(max_length=50, blank=True, null=True)
                insee_com = models.CharField(max_length=5, blank=True, null=True)
                statut = models.CharField(max_length=26, blank=True, null=True)
                population = models.IntegerField(blank=True, null=True)
                insee_can = models.CharField(max_length=5, blank=True, null=True)
                insee_arr = models.CharField(max_length=2, blank=True, null=True)
                insee_dep = models.CharField(max_length=3, blank=True, null=True)
                insee_reg = models.CharField(max_length=2, blank=True, null=True)
                siren_epci = models.CharField(max_length=20, blank=True, null=True)
                geom = models.MultiPolygonField(srid=4365, blank=True, null=True)

                class Meta:
                    managed = False
                    db_table = 'communes'
                    app_label = 'carto'

            CommuneGeo.objects.all().delete()
            print('Commune qui non pas pu être importées')
            print('pk, insee, nom')
            for c in Commune.objects.all():
                a = GeoCommunes.objects.using('geoserver').filter(insee_com=c.code)
                if a:
                    a = a.get()
                    b = CommuneGeo(commune=c, insee=a.insee_com, geom=a.geom)
                    b.save()
                else:
                    print(c.pk, c.code, c.name)

        if data in ['0', '2']:
            class GeoArrondissement(models.Model):
                gid = models.AutoField(primary_key=True)
                id = models.CharField(max_length=24, blank=True, null=True)
                nom_m = models.CharField(max_length=200, blank=True, null=True)
                nom = models.CharField(max_length=200, blank=True, null=True)
                insee_arr = models.CharField(max_length=1, blank=True, null=True)
                insee_dep = models.CharField(max_length=3, blank=True, null=True)
                insee_reg = models.CharField(max_length=2, blank=True, null=True)
                geom = models.MultiPolygonField(srid=4365, blank=True, null=True)

                class Meta:
                    managed = False
                    db_table = 'arrondissement'
                    app_label = "carto"

            ArrondissementsGeo.objects.all().delete()
            for a in GeoArrondissement.objects.using('geoserver').all():
                code = a.insee_dep + a.insee_arr
                arrondissement = Arrondissement.objects.get(code=code)
                g = ArrondissementsGeo(arrondissement=arrondissement, insee=code, geom=a.geom)
                g.save()

        if data in ['0', '3']:
            class GeoDepartement(models.Model):
                gid = models.AutoField(primary_key=True)
                id = models.CharField(max_length=24, blank=True, null=True)
                nom_m = models.CharField(max_length=30, blank=True, null=True)
                nom = models.CharField(max_length=30, blank=True, null=True)
                insee_dep = models.CharField(max_length=3, blank=True, null=True)
                insee_reg = models.CharField(max_length=2, blank=True, null=True)
                geom = models.MultiPolygonField(srid=4365, blank=True, null=True)

                class Meta:
                    managed = False
                    db_table = 'departements'
                    app_label = "carto"

            DepartementGeo.objects.all().delete()
            print('pas de géographie pour les départements suivants : ')
            for departement in Departement.objects.all():
                g = GeoDepartement.objects.using('geoserver').filter(insee_dep=departement.name)
                if g:
                    g = g.get()
                    depgeo = DepartementGeo(departement=departement, insee=g.insee_dep, geom=g.geom)
                    depgeo.save()
                else:
                    print(departement.name, str(departement))

        if data in ['0', '4']:
            class GeoNaturaDirectiveHabitat(models.Model):
                gid = models.AutoField(primary_key=True)
                sitecode = models.CharField(max_length=9, blank=True, null=True)
                sitename = models.CharField(max_length=160, blank=True, null=True)
                geom = models.MultiPolygonField(srid=4365, blank=True, null=True)

                class Meta:
                    managed = False
                    db_table = 'natura_directive_habitat'
                    app_label = "carto"

            class GeoNaturaProtectionSpeciale(models.Model):
                gid = models.AutoField(primary_key=True)
                sitecode = models.CharField(max_length=9, blank=True, null=True)
                sitename = models.CharField(max_length=160, blank=True, null=True)
                geom = models.MultiPolygonField(srid=4365, blank=True, null=True)

                class Meta:
                    managed = False
                    db_table = 'natura_protection_speciale'
                    app_label = "carto"

            NK2000Geo.objects.all().delete()
            print("Pas de géographie pour les site n2k :")
            for site in N2kSite.objects.all():
                ga = GeoNaturaProtectionSpeciale.objects.using('geoserver').filter(sitecode=site.index)
                gb = GeoNaturaDirectiveHabitat.objects.using('geoserver').filter(sitecode=site.index)
                if ga:
                    ga = ga.first()
                    nk_geo = NK2000Geo(n2k=site, geom=ga.geom)
                    nk_geo.save()
                elif gb:
                    gb = gb.first()
                    nk_geo = NK2000Geo(n2k=site, geom=gb.geom)
                    nk_geo.save()
                else:
                    print(site.nom, site.pk)

        if data in ['0', '5']:
            class GeoReserveNatRegionale(models.Model):
                gid = models.AutoField(primary_key=True)
                id_mnhn = models.CharField(max_length=9, blank=True, null=True)
                id_org = models.CharField(max_length=12, blank=True, null=True)
                nom = models.CharField(max_length=160, blank=True, null=True)
                date = models.CharField(max_length=10, blank=True, null=True)
                superficie = models.IntegerField(blank=True, null=True)
                id_local = models.CharField(max_length=15, blank=True, null=True)
                prn_asso = models.CharField(max_length=15, blank=True, null=True)
                code_r_enp = models.CharField(max_length=5, blank=True, null=True)
                nom_site = models.CharField(max_length=254, blank=True, null=True)
                date_crea = models.DateField(blank=True, null=True)
                modif_adm = models.DateField(blank=True, null=True)
                modif_geo = models.DateField(blank=True, null=True)
                url_fiche = models.CharField(max_length=254, blank=True, null=True)
                surf_off = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
                acte_deb = models.CharField(max_length=50, blank=True, null=True)
                acte_fin = models.CharField(max_length=50, blank=True, null=True)
                gest_site = models.CharField(max_length=100, blank=True, null=True)
                operateur = models.CharField(max_length=50, blank=True, null=True)
                src_geom = models.CharField(max_length=100, blank=True, null=True)
                src_annee = models.CharField(max_length=4, blank=True, null=True)
                marin = models.CharField(max_length=1, blank=True, null=True)
                p1_nature = models.CharField(max_length=1, blank=True, null=True)
                p2_culture = models.CharField(max_length=1, blank=True, null=True)
                p3_paysage = models.CharField(max_length=1, blank=True, null=True)
                p4_geologi = models.CharField(max_length=1, blank=True, null=True)
                p5_speleo = models.CharField(max_length=1, blank=True, null=True)
                p6_archeo = models.CharField(max_length=1, blank=True, null=True)
                p7_paleob = models.CharField(max_length=1, blank=True, null=True)
                p8_anthrop = models.CharField(max_length=1, blank=True, null=True)
                p9_science = models.CharField(max_length=1, blank=True, null=True)
                p10_public = models.CharField(max_length=1, blank=True, null=True)
                p11_dd = models.CharField(max_length=1, blank=True, null=True)
                p12_autre = models.CharField(max_length=1, blank=True, null=True)
                objectid = models.FloatField(blank=True, null=True)
                precision = models.CharField(max_length=2, blank=True, null=True)
                layer = models.CharField(max_length=254, blank=True, null=True)
                path = models.CharField(max_length=254, blank=True, null=True)
                geom = models.MultiPolygonField(srid=4365, blank=True, null=True)

                class Meta:
                    managed = False
                    db_table = 'reserve_nat_regionale'
                    app_label = "carto"

            RnrZoneGeo.objects.all().delete()
            print('pas de géographie pour les zones rnr :')
            for site in RnrZone.objects.all():
                geo = GeoReserveNatRegionale.objects.using('geoserver').filter(id_mnhn=site.code)
                if geo:
                    geo = geo.first()
                    rnr_geo = RnrZoneGeo(rnr=site, geom=geo.geom)
                    rnr_geo.save()
                else:
                    print(site.nom, site.pk)






