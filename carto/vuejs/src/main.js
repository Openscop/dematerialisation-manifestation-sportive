import Vue from 'vue'
import App from "./App.vue";
import store from "./store";

import "./plugins/Emitter"
import "./plugins/Tooltip"
import "./plugins/Select"
import "./plugins/Draggable"
import "./plugins/Api"
import "./plugins/Scroll"

import "./assets/editor.css"

Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(App)
}).$mount('#vueApp');
