import Vue from 'vue';
import Vuex from "vuex";
import {emitter} from "@/plugins/Emitter";
import {Events} from "@/modules/events/Events";
import {traces} from "./traces/index";
import {pois} from "./pois/index";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    editorSize: {
      width: 100,
      height: 900,
    },
    init: false,
    mode: 2,
    isInstructeur: false,
    isInInstruction: false,
    user: null,
    loading: false,
  },
  getters: {
    getEditorSize: (state) => {
      return state.editorSize;
    },
    isModeEdition: (state) => {
      return state.mode === 'edition';
    },
    isInit: (state) => {
      return state.init;
    },
    isLoading: (state) => {
      return state.loading;
    },
    isInstructeur: (state) => {
      return state.isInstructeur;
    },
    isInInstruction: (state) => {
      return state.isInInstruction;
    },
  },
  mutations: {
    completedInit(state) {
      state.init = true;
    },
    setMode(state, mode) {
      state.mode = mode;
    },
    setUser(state, user) {
      state.user = user;
    },
    setLoading(state, loading){
      state.loading = loading;
    },
    setIsInstructeur(state, isInstructeur) {
      state.isInstructeur = isInstructeur;
    },
    setIsInInstruction(state, isInInstruction) {
      state.isInInstruction = isInInstruction;
    },
  },
  actions: {
    completedInit({commit}) {
      commit("completedInit");
    },
    setMode({commit}, mode) {
      commit("setMode", mode);
    },
    setUser({commit}, user) {
      commit("setUser", user);
      emitter.emit(Events.UPDATE_USER, user);
    },
    setLoading({commit}, loading){
      commit("setLoading", loading);
    },
    setIsInstructeur({commit}, isInstructeur) {
      commit("setIsInstructeur", isInstructeur)
    },
    setIsInInstruction({commit}, isInInstruction) {
      commit("setIsInInstruction", isInInstruction)
    },
  },
  modules: {
    traces,
    pois,
  }
});
