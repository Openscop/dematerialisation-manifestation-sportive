import {emitter} from '@/plugins/Emitter';
import {Events} from '@/modules/events/Events';

export const traces = {
  namespaced: true,
  state: () => ({
    loadingTraces: [],
    activeTrace: null,
    traces: [],
    loadedTraces: [],
  }),
  getters: {
    getTraces(state) { return state.traces.filter(trace => !trace.is_archived) },
    getArchivedTraces(state) { return state.traces.filter(trace => trace.is_archived) },
    getLoadedTraces(state) { return state.loadedTraces },
    isLoadedTrace: (state) => (id) => { return !!state.loadedTraces.find(trace => trace.id === id) },
    isTraceActive: (state) => (id) => {
      return state.activeTrace === id;
    },
    isTraceChanged: (state) => (id) => {
      return !!state.loadedTraces.find(trace => trace.id === id && trace.changed);
    },
    activeTrace(state) {
      return state.activeTrace;
    },
    isTraceAModifier: (state) => (id) => {
      return state.traces.find(t => t.id === id)?.a_modifier
    },
    hasHistorique: (state) => (id) => { return state.traces.find(t => t.id === id)?.has_historique },
    isLoadingTrace: (state) => (id) => { return state.loadingTraces.includes(id) },
    getTraceColor: (state) => (id) => {
      return state.loadedTraces.find(trace => trace.id === id)?.options?.color || state.traces.find(trace => trace.id === id)?.shape?.stroke_color || state.traces.find(trace => trace.id === id)?.options?.color
    },
    isTraceVisible: (state) => (id) => {
      return !!state.loadedTraces.find(trace => trace.id === id && trace.show)
    },
    isBigTrace: (state) => (id) => {
      return !!state.loadedTraces.find(trace => trace.id === id && trace.waypointsCount > 1000)
    },
    getTrace: (state) => (id) => { return state.traces.find(trace => trace.id === id)}
  },
  actions: {
    async setAModifier({commit, state}, {id, status}) {
      const list = state.traces.slice()
      const idx = list.findIndex(t => t.id === id)
      list[idx].a_modifier = status
      commit('setTraces', list)
    },
    async setArchived({commit, state}, {id, status}) {
      const list = state.traces.slice()
      const idx = list.findIndex(t => t.id === id)
      list[idx].is_archived = status
      commit('setTraces', list)
    },
    async setTraces({commit}, traces) {
      commit('setTraces', traces)
    },
    async addTrace({commit, state}, trace) {
      const traces = state.traces.slice()
      traces.push(trace)
      commit('setTraces', traces)
    },
    async setActive({commit}, traceId) {
      commit("setActive", traceId);
      emitter.emit(Events.SELECT_TRACE, traceId);
    },
    async updateActive({commit}, traceId) {
      commit("setActive", traceId);
    },
    async removeTrace({commit, dispatch, state}, id) {
      const traces = state.traces.filter(trace => trace.id !== id)
      await dispatch('unloadTrace', id)
      commit("setTraces", traces);
    },
    async setChanged({commit, state}, payload) {
      const traceIdx = state.loadedTraces.findIndex(trace => trace.id === payload.id);
      if (traceIdx !== -1) {
        const traces = state.loadedTraces.slice()
        traces[traceIdx] = Object.assign(traces[traceIdx], payload)
        commit("setLoadedTraces", traces);
      }
    },
    async updateTrace({commit, state}, {oldId, trace, updatedTrace}){
      const idx = state.traces.findIndex(t => t.id === oldId)
      if(idx >= 0) {
        const traces = state.traces.slice()
        traces[idx] = trace
        commit('setTraces', traces)
      }
      const loadedIndex = state.loadedTraces.findIndex(track => track.id === oldId)
      if(loadedIndex > -1) {
        const loaded = state.loadedTraces.slice()
        loaded[loadedIndex] = updatedTrace
        commit('setLoadedTraces', loaded)
      }
    },
    async addLoadingTrace({ commit, state }, trace) {
      if(!state.loadingTraces.includes(trace.id)) {
        const newLoading = [...state.loadingTraces]
        newLoading.push(trace)
        commit('setLoadingTraces', newLoading)
      }
    },
    async removeLoadingTrace({ commit, state }, id) {
      if(state.loadingTraces.includes(id)) {
        const newLoading = state.loadingTraces.filter(loadingId => loadingId !== id)
        commit('setLoadingTraces', newLoading)
      }
    },
    async loadTrace({commit, dispatch, state}, trace) {
      if(!state.traces.find(track => track.id === trace.id)) {
        dispatch("addTrace", trace)
      }
      // charge un parcours dans l'editeur
      if(!state.loadedTraces.find(track => track.id === trace.id)) {
        const loaded = state.loadedTraces.slice()
        loaded.push(trace)
        commit('setLoadedTraces', loaded)
      }
    },
    async unloadTrace({commit, dispatch, state}, id) {
      // decharge un parcours de l'editeur
      if(state.loadedTraces.find(track => track.id === id)) {
        const loaded = state.loadedTraces.filter(track => track.id !== id)
        commit('setLoadedTraces', loaded)
      }
    }
  },
  mutations: {
    setLoadingTraces(state, traces) {
      state.loadingTraces = traces
    },
    setTraces(state, traces) {
      state.traces = traces
    },
    setActive(state, id) {
      state.activeTrace = id;
    },
    setChanged(state, traces) {
      state.traces = traces;
    },
    setLoadedTraces(state, loaded) {
      state.loadedTraces = loaded
    },
  },
};
