export const pois = {
    namespaced: true,
    state: () => ({
        pois: [],
    }),
    getters: {
        getPois(state) {
            return state.pois
        },
    },
    actions: {
        searchPois({state}, id) {
            let found = false
            Object.values(state.pois).forEach(category => {
                const test = category.symbols.find(poiContent => String(poiContent.id) === String(id))
                if (test) found = test
            })
            return found
        },
        setPois({commit}, pois) {
            commit('setPois', pois)
        },
    },
    mutations: {
        setPois(state, pois) {
            state.pois = pois
        },
    },
};
