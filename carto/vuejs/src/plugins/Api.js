import {Api} from "@/modules/Api";
import Vue from "vue";
export const api = new Api({
  baseUrl: "/",
  headers: {"Content-Type": "application/json"},
  redirect: "follow",
});
if(!Vue.prototype.$api) {
  Vue.prototype.$api = api;
}
