import Vue from "vue";
import mitt from "mitt";

export const emitter = mitt();

Vue.prototype.$emitter = emitter;
