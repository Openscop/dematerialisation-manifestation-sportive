export class Api {
  options = {
    credentials: "same-origin"
  };

  url = null;

  constructor(options) {
    this.options = Object.assign(this.options, options);
  }

  set baseUrl(value){
    this.options.baseUrl = value;
  }

  #query(method, url, data = {}, overrideOptions = {}){
    this.url = new URL(url, (this.options.baseUrl || overrideOptions.baseUrl));
    const headers = new Headers();

    Object.keys(this.options.headers)
      .forEach((key) => {
        headers.append(key, this.options.headers[key]);
      });
    if (overrideOptions.headers) {
      Object.keys(overrideOptions.headers)
        .forEach((key) => {
          if(headers.has(key)){
            headers.delete(key);
          }
          headers.append(key, overrideOptions.headers[key]);
        });
    }

    const options = {
      method: method.toUpperCase(),
      mode: overrideOptions.mode || this.options.mode,
      credentials: overrideOptions.credentials || this.options.credentials,
      redirect: overrideOptions.redirect || this.options.redirect,
      headers,
    };

    // traitement des data
    if (["GET", "DELETE"].includes(options.method)) {
      Object.keys(data)
        .forEach((key) => this.url.searchParams.append(key, data[key]));
    } else {
      options.body = JSON.stringify(data);
    }

    return new Promise((resolve, reject) => {
      fetch(this.url.toString(), options)
        .then(Api.#checkStatus)
        .then(Api.#parseResponse)
        .then((body) => {
          resolve(body);
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  static async #parseResponse(response){
    const type = response.headers.get('Content-Type');
    if (type === 'application/gpx') {
      let filename = response.headers.get("content-disposition")
          .split('"')[1]
      return {
        filename: filename,
        blob: await response.blob()
      }
    } else {
      return response.json()
    }
  }
  static #checkStatus(response){
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else if(response.status === 302) {
      return response;
    } else {
      const error = new Error(response.statusText);
      error.response = response;
      throw error;
    }
  }


  searchTrace(name, isInstructeur){
    if(isInstructeur) {
      return this.#query("get", "/carto/parcours/instructeur/", { pk: manif });
    }
    return this.#query("get", "/carto/parcours/");
  }
  loadManifParcours(manifPk) {
    return this.#query("get", `/carto/parcours/parcoursmanif/`, {pk: manifPk});
  }
  loadParcours(parcourId) {
    return this.#query("get", `/carto/parcours/parcours/`, {pk: parcourId});
  }
  createTrace(trace){
    return this.#query("post", "/carto/parcours/", trace);
  }
  updateTrace(trace, finalizeEdit){
    return this.#query("put", `/carto/parcours/${trace.id}/?done=${finalizeEdit}`, trace);
  }
  deleteTrace(id){
    return this.#query("delete", `/carto/parcours/${id}/`);
  }
  exportGpx(id){
    return this.#query("get", `/carto/export/${id}/`);
  }
  unlockItem(id) {
    return this.#query("post", `/carto/parcours/modif/`, { pk: id });
  }
  archive(id, status) {
    return this.#query("post", `/carto/parcours/archive/`, { pk: id, status });
  }
  finaliseTrace(id) {
    return this.#query("post", `/carto/parcours/finalise/`, { pk: id });
  }
  getHistorique(id) {
    return this.#query("get", `/carto/parcours/historique/`, { pk: id });
  }
}
