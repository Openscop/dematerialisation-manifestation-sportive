import store from '../../store/index'


export function fromGpx(files) {
    return Promise.all(Array.prototype.map.call(files, readAsDataURL))
        .then(async (gpxs) => {
            let trackIndex = 0
            // gpxs === les features collection correspondante a chaque fichier
            const data = {
                traces: [],
                waypoints: [],
            }
            for (const gpx of gpxs) {
                const indexFile = gpxs.indexOf(gpx);

                for (const track of gpx.tracks) {
                    const index = gpx.tracks.indexOf(track);
                    trackIndex++
                    track.properties.fileIndex = indexFile
                    track.properties.id = trackIndex
                    track.properties.name = `${trackIndex} - ${files[indexFile].name.replace('.gpx', '')}`.substring(0,100)
                    const trackPois = []
                    for (let poi of track.properties.pois) {
                        poi = await updatePoi(poi)
                        trackPois.push(poi)
                    }
                    track.properties.pois = trackPois
                    data.traces.push(track)
                }
                for (let poi of gpx.pois) {
                    poi = await updatePoi(poi)
                    data.waypoints.push(poi)
                }
            }
            return data
        })
}

function readAsDataURL(file) {
    return new Promise((resolve, reject) => {
        const fr = new FileReader()
        fr.onerror = reject
        fr.onload = function () {
            resolve(gpx(new DOMParser().parseFromString(fr.result, 'text/xml')))
        }
        fr.readAsText(file)
    })
}

export function convertDataToTrace({traces, waypoints}) {
    let tracks = []
    if (Array.isArray(traces)) {
        tracks = traces.map(geojson => convertGeojsonToTrace(geojson))
    } else {
        tracks.push(convertGeojsonToTrace(traces))
    }
    if (waypoints) {
        if (tracks[0].pois) {
            tracks[0].pois = tracks[0].pois.concat(waypoints)
        } else {
            tracks[0].pois = waypoints
        }
    }
    return tracks
}

export function convertGeojsonToTrace(geojson) {
    return {
        name: geojson?.properties?.name,
        description: geojson?.properties?.description,
        manif: manifPk,
        shape: {
            line_waypoint: geojson,
            line_shape: geojson,
            stroke_color: geojson?.properties?.shape?.stroke_color || 'red',
            stroke_width: geojson?.properties?.shape?.stroke_width || 4,
        },
        pois: [].concat(geojson?.properties?.pois),
        zone: [].concat(geojson?.properties?.zones),
    }
}

const updatePoi = async (poi) => {
    const searchingId = poi.properties?.icon?.id || poi.properties?.waypointIcon || 1
    poi.properties.icon = await store.dispatch('pois/searchPois', searchingId)
    poi.properties.title = poi.properties.waypointTitle
    return poi
}

"use strict"

function gpx(file) {
    return {
        tracks: getAllTracksType(file),
        pois: getPois(file, 'gpx'),
    }
}

function getElementsByTagName(node, tag) {
    return [].slice.call(node.getElementsByTagName(tag))
}

function getAttribute(element, attribute, convert = false) {
    if (convert) {
        return Number(element?.getAttribute(attribute))
    }
    return element?.getAttribute(attribute)
}

function getNodeValue(element, path, convert = false) {
    if (convert) {
        return Number(element?.getElementsByTagName(path)[0]?.textContent)
    }
    return element?.getElementsByTagName(path)[0]?.textContent
}

function getAllTracksType(file) {
    return [].concat(getTracks(file), getRoutes(file))
}

function getRoutes(file) {
    return getElementsByTagName(file, 'rte').map(track => {
        return {
            type: "Feature",
            geometry: {
                type: "LineString",
                coordinates: getRouteWaypoints(track),
            },
            properties: {
                ...getTrackInfo(track),
                zones: getZones(track),
                pois: getPois(track, 'rte'),
                shape: {
                    stroke_color: 'red',
                    stroke_width: 4,
                }
            }
        }
    })
}

function getTracks(file) {
    return getElementsByTagName(file, 'trk').map(track => {
        return {
            type: "Feature",
            geometry: {
                type: "LineString",
                coordinates: getTrackWaypoints(track),
            },
            properties: {
                ...getTrackInfo(track),
                zones: getZones(track),
                pois: getPois(track, 'trk'),
                shape: {
                    stroke_color: 'red',
                    stroke_width: 4,
                }
            }
        }
    })
}

function getTrackInfo(track) {
    return {
        name: getNodeValue(track, 'name'),
        desc: getNodeValue(track, 'desc'),
        type: getNodeValue(track, 'type'),
    }
}

function getTrackWaypoints(node) {
    return getElementsByTagName(node, 'trkpt').map(el => {
        return [
            getAttribute(el, 'lon', true),
            getAttribute(el, 'lat', true),
        ]
    })
}

function getZones(node) {
    const zones = getElementsByTagName(node, 'zone')
    return zones.map(zone => {
        const options = JSON.parse(getAttribute(zone, 'option') || '{}')
        const points = getZonePoints(zone)
        return {
            type: "Feature",
            properties: options,
            geometry: {
                type: "Polygon",
                coordinates: points
            },
            parcours: 4
        }
    })
}

function getZonePoints(zone) {
    return [getElementsByTagName(zone, 'zonepoint').map(point => {
        return [
            getAttribute(point, 'lon'),
            getAttribute(point, 'lat'),
        ]
    })]
}

function getPois(node, parent) {
    // il y a un parent donc c'est un poi d'un parcours
    if (node.parentNode) {
        return getElementsByTagName(node, 'wpt').map(wpt => {
            return {
                type: 'Feature',
                geometry: {
                    type: 'Point',
                    coordinates: [getAttribute(wpt, 'lon'), getAttribute(wpt, 'lat')]
                },
                properties: Object.assign(
                    {},
                    {
                        waypointType: getNodeValue(wpt, 'type'),
                        waypointIcon: getNodeValue(wpt, 'sym'),
                        waypointTitle: getNodeValue(wpt, 'name'),
                    },
                    JSON.parse(getAttribute(getElementsByTagName(wpt, 'option')[0], 'option') || '{}'),
                ),
            }
        })
    }
    // pas de parent donc poi global
    return getElementsByTagName(node, 'wpt').filter(wpt => wpt.parentNode.tagName === parent).map(wpt => {
        return {
            type: 'Feature',
            geometry: {
                type: 'Point',
                coordinates: [getAttribute(wpt, 'lon'), getAttribute(wpt, 'lat')]
            },
            properties: Object.assign(
                {},
                {
                    waypointType: getNodeValue(wpt, 'type'),
                    waypointIcon: getNodeValue(wpt, 'sym'),
                    waypointTitle: getNodeValue(wpt, 'name'),
                },
                JSON.parse(getAttribute(getElementsByTagName(wpt, 'option')[0], 'option') || '{}'),
            )
        }
    })
}

function getRouteWaypoints(node) {
    return getElementsByTagName(node, 'rtept').map(el => {
        return [el.getAttribute('lon'), el.getAttribute('lat')]
    })
}
