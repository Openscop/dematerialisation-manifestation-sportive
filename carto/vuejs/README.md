# manif-vue

## Project setup

dans carto/vuejs faire :
```
npm i
npm start
```

installer et lancer le projet manifestation-carto-wrapper
lancer le serveur manif et acceder au pages cartographie

les fichiers vuejs sont compiler dans carto/static/cartoVue, il peut etre nécessaire de faire un collectstatics apres chaque build.

### Compiles and minifies for production
```
npm run build
```
