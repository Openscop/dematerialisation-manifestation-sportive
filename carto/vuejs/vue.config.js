const path = require("path");

module.exports = {
    filenameHashing: false,
     publicPath: '/static/123456/carto/',
    outputDir: path.resolve(__dirname, "../static/carto/cartoVue"),
    // delete HTML related webpack plugins
    // mutate config for production...
    configureWebpack: {
        optimization: {
            splitChunks: {
                cacheGroups: {
                    app: {
                        chunks: 'all',
                        name: 'main',
                        test: /[\\/]src[\\/](.*)[\\/]/,
                    },
                    vendors: {
                        chunks: 'all',
                        name: 'vendors',
                        test: /[\\/]node_modules[\\/]/,
                    },
                    // Merge all the CSS into one file
                    styles: {
                        name: 'styles',
                        test: /\.s?css$/,
                        chunks: 'all',
                        minChunks: 1,
                        reuseExistingChunk: false,
                        enforce: true,
                    }
                }
            }
        },
    },
    chainWebpack: config => {
        config.optimization.delete('splitChunks')
        config.module
            .rule('images')
            .use('url-loader')
            .loader('url-loader')
            .tap(options => Object.assign(options, {limit: 10240}))
        config.plugins
            .delete('html')
            .delete('prefetch')
            .delete('preload')
        config
            .entry('print')
            .add('./src/print.js')
        config
            .entry('conflicts')
            .add('./src/conflicts.js')
        config
            .entry('historiques')
            .add('./src/historique.js')
    }
}