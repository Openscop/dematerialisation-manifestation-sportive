from django.contrib import admin

# Register your models here.


from .models.parcours import *
from .models.poi import PoiApi, PoiCategorieApi


@admin.register(Parcours)
class ParcoursAdmin(admin.ModelAdmin):
    list_display = ['pk', 'manif', 'name', 'date', 'import_v4', 'ancient_id']
    search_fields = ['pk', 'name__unaccent', 'ancient_id']
    date_hierarchy = 'date'
    ordering = ['-date']

    def import_v4(self, obj):
        return True if obj.ancient_id else False
    import_v4.short_description = "Importation v4"
    import_v4.boolean = True


@admin.register(Shape)
class ShapeAdmin(admin.ModelAdmin):
    pass


@admin.register(PointWaypointEncoded)
class PointWaypointEncodedAdmin(admin.ModelAdmin):
    pass


@admin.register(PointShapeEncoded)
class PointShapeEncodedAdmin(admin.ModelAdmin):
    pass


@admin.register(PointShapeReducedEncoded)
class PointShapeReducedEncodedAdmin(admin.ModelAdmin):
    pass


@admin.register(Poi)
class PoiAdmin(admin.ModelAdmin):
    pass


@admin.register(Zone)
class ZoneAdmin(admin.ModelAdmin):
    pass


@admin.register(PoiApi)
class PoiApiAdmin(admin.ModelAdmin):
    list_display = ('id', 'i18n', 'html_code', 'classes', 'categorie', 'description')
    pass


@admin.register(PoiCategorieApi)
class PoiCategorieApiAdmin(admin.ModelAdmin):
    list_display = ('id', 'title_i18n')
    pass
