import json

from django.http import HttpResponse
from django.views import View
from django.shortcuts import render
from django.conf import settings

from carto.services.editorHelper import generateEditorSettings, generateEditorUser


class Impression(View):

    def get(self, request):
        if request.user.is_anonymous:
            return HttpResponse('non connecté', status=403)
        context = {}
        file = 'print'
        context['openrunnerSettings'] = json.dumps(generateEditorSettings(file))
        # defini l'user pour l'editeur :
        context['editorUser'] = json.dumps(generateEditorUser(request.user))
        return render(request, "carto_print.html", context)
