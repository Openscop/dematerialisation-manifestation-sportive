from django.http import HttpResponse
from django.views import View
from django.shortcuts import render, get_object_or_404
import json

from carto.services.editorHelper import generateEditorSettings, generateEditorUser
from evenements.models import Manif


class Conflit(View):
    def get(self, request, pk):

        if request.user.is_anonymous:
            return HttpResponse('non connecté', status=403)

        context = {}
        manif = get_object_or_404(Manif, pk=pk)
        if not manif.has_access(request.user):
            return HttpResponse("erreur de droit d'acces", status=403)

        context['manif'] = manif
        file = 'conflit'
        context['openrunnerSettings'] = json.dumps(generateEditorSettings(file))
        # defini l'user pour l'editeur :
        context['editorUser'] = json.dumps(generateEditorUser(request.user))
        return render(request, "carto_conflit.html", context)
