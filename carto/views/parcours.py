import json
import os
import xml.etree.ElementTree as ET
from xml.dom import minidom

from django.utils import timezone
from django.contrib.gis.geos import GEOSGeometry
from django.shortcuts import get_object_or_404, render, HttpResponse
from django.views import View
from django.conf import settings

from oauth2_provider.models import AccessToken
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework import filters

from carto.models import *
from carto.serializers import ParcoursSerializer, ListParcoursSerializer
from evenements.models import Manif
from instructions.views.tableaudebord import get_instructions
from rest_framework.pagination import LimitOffsetPagination
from structure.models.organisation import Organisation


def export_parcours(parcour_pk):
    """
    exportation d'un parcours sous le format gpx
    """
    parcours_carto = Parcours.objects.get(pk=parcour_pk)
    list = []
    if hasattr(parcours_carto.shape, 'line_shape') and parcours_carto.shape.line_shape:
        for c in parcours_carto.shape.line_shape.geometry.coords:
            list.append({'lon': c[0], 'lat': c[1]})
    elif hasattr(parcours_carto.shape, 'line_waypoint') and parcours_carto.shape.line_waypoint:
        for c in parcours_carto.shape.line_waypoint.geometry.coords:
            list.append({'lon': c[0], 'lat': c[1]})

    gpx = ET.Element("gpx", version="1.1", creator="", xmlns="http://www.topografix.com/GPX/1/1")
    trk = ET.SubElement(gpx, "trk")
    name = ET.SubElement(trk, "name")
    name.text = parcours_carto.name
    desc = ET.SubElement(trk, "desc")
    desc.text = parcours_carto.description
    if parcours_carto.is_archived:
        archived = ET.SubElement(trk, "archived")
        archived.text = "Ce parcours est une archive."
    trkseg = ET.SubElement(trk, 'trkseg')
    for poi in list:
        ET.SubElement(trkseg, "trkpt", lat=str(poi["lat"]), lon=str(poi["lon"]))

    for poi in parcours_carto.pois.all():
        wpt = ET.SubElement(trk, 'wpt', lat=str(poi.geometry.y), lon=str(poi.geometry.x))
        name = ET.SubElement(wpt, 'name')
        name.text = poi.properties['title']
        sym = ET.SubElement(wpt, 'sym')
        sym.text = poi.properties['icon']['id']
        type = ET.SubElement(wpt, 'type')
        type.text = "WPT"
        extensions = ET.SubElement(wpt, "extensions")
        ET.SubElement(extensions, 'option', option=json.dumps(poi.properties))

    for a in parcours_carto.zone.all():
        extensions = ET.SubElement(trk, "extensions")
        zone = ET.SubElement(extensions, 'zone', option=json.dumps(a.properties))
        for b in a.geometry.coords[0]:
            ET.SubElement(zone, "zonepoint", lat=str(b[1]), lon=str(b[0]))
    xmlstr = minidom.parseString(ET.tostring(gpx)).toprettyxml(encoding="utf-8", indent="  ")

    # utilisation du dossier d'exportation de la manif
    # si pas de dossier tmp on le créé
    if os.path.exists(settings.MEDIA_ROOT + "exportation"):
        pass
    else:
        os.makedirs(settings.MEDIA_ROOT + "exportation")
    pathdelamanif = settings.MEDIA_ROOT + "exportation/" + str(parcours_carto.manif.pk) + "/"
    if not os.path.exists(pathdelamanif):
        os.makedirs(pathdelamanif)

    nom_fichier = f"{pathdelamanif}{str(parcours_carto.pk)}.gpx"
    with open(nom_fichier, "wb") as f:
        f.write(xmlstr)

    return nom_fichier


class ExportParcoursView(View):

    def get(self, request, pk):
        parcours_carto = get_object_or_404(Parcours, pk=pk)
        token = request.META.get('HTTP_AUTHORIZATION').split('Bearer ') if request.META.get('HTTP_AUTHORIZATION') else ""
        user = None
        if request.user.is_authenticated:
            user = request.user
        if not user:
            # pour l'api on fait une verifi d'auth avec une cle api
            accestoken = AccessToken.objects.filter(token__in=token)
            if accestoken:
                accestoken = accestoken.first()
                user = accestoken.user if accestoken.expires > timezone.now() else None

        if not user or (not parcours_carto.manif.has_access(user) and not user.has_group('_API_environnement')):
            return render(request, "core/access_restricted.html",
                          {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)

        fichier = export_parcours(parcours_carto.pk)
        f = open(fichier, 'rb')
        response = HttpResponse(f, content_type='application/gpx')
        response['Content-Disposition'] = f"attachment; filename=\"{parcours_carto.name}.gpx\""
        return response


class CsrfExemptSessionAuthentication(SessionAuthentication):

    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class CustomLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 1000


class ParcoursViewSet(viewsets.ModelViewSet):
    queryset = Parcours.objects.all()
    serializer_class = ParcoursSerializer
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    filter_backends = [filters.SearchFilter]
    search_fields = ['$name', '$pk']
    pagination_class = CustomLimitOffsetPagination


    def get_queryset(self):
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        if organisation.is_structure_organisatrice():
            queryset = Parcours.objects.filter(manif__structure_organisatrice_fk=organisation).order_by('-pk')
        elif organisation.is_service_consulte():
            instructions_pk = []
            instructions = get_instructions(organisation, "", "", self.request, annee=None, all=True)
            [instructions_pk.append(instruction.pk) if instruction.pk not in instructions_pk else "" for instruction in instructions]
            queryset = Parcours.objects.filter(manif__instruction__pk__in=instructions_pk).order_by('-pk')
        else:
            queryset = Parcours.objects.none()
        return queryset

    def update(self, request, *args, **kwargs):
        parcours_carto = Parcours.objects.get(pk=kwargs['pk'])
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        if not organisation.is_structure_organisatrice() or not organisation == parcours_carto.manif.structure_organisatrice_fk:
            return Response("Pas d'accès à ce parcours", status=403)
        if request.GET.get('done', False) == 'true':
            if parcours_carto.manif.get_instruction():
                # Marquer la line de demande de modification DONE si elle existe
                if parcours_carto.manif.modif_json:
                    sortie = True
                    for idx, line in enumerate(parcours_carto.manif.modif_json):
                        if 'parcours' in line and line['parcours'] == parcours_carto.pk and not line['done']:
                            parcours_carto.manif.modif_json[idx]['done'] = True
                            parcours_carto.manif.save()
                            sortie = False
                            parcours_carto.notifier_parcours_modifie()
                            break
                    if sortie:
                        return Response('La manif est instruite', status=401)
                    else:
                        old_data = ParcoursSerializer(parcours_carto).data
                        parcours_historique = ParcoursHistorique(parcours=parcours_carto, date=parcours_carto.date,
                                                                 data_json=old_data)
                        parcours_historique.save()
                        parcours_carto.has_historique = True
                else:
                    return Response('La manif est instruite', status=401)
        data = request.data
        parcours_carto.date = timezone.now()
        parcours_carto.shape.routing_engine_mode = data["shape"].get('routing_engine_mode', '')
        parcours_carto.shape.routing_engine = data["shape"].get('routing_engine', "")
        parcours_carto.shape.stroke_color = data["shape"]["stroke_color"]
        parcours_carto.shape.stroke_opacity = data["shape"]["stroke_opacity"]
        parcours_carto.shape.stroke_width = data["shape"]["stroke_width"]
        parcours_carto.shape.save()
        parcours_carto.name = data["name"]
        parcours_carto.description = data["description"]
        parcours_carto.activity = data.get("activity", "")
        parcours_carto.keyword = data.get("keyword", "")
        parcours_carto.length_in_meter = data["length_in_meter"]
        parcours_carto.is_private = data.get("is_private", "")
        parcours_carto.is_tested_by_user = data.get("is_tested_by_user", "")
        parcours_carto.difficulty = data.get("difficulty", "")
        parcours_carto.source = data.get("source", "")
        parcours_carto.thumb = data.get("thumb", "")
        parcours_carto.start_lat = data.get("start_lat", "")
        parcours_carto.start_lng = data.get("start_lng", "")
        parcours_carto.end_lat = data.get("end_lat", "")
        parcours_carto.end_lng = data.get("end_lng", "")
        parcours_carto.elevation = data["elevation"]
        parcours_carto.osm_details = data.get("osm_details",)
        parcours_carto.discipline = data.get("discipline", "")
        parcours_carto.save()

        # on va supprimer tous les points et zone pour ensuite les recréer
        Poi.objects.filter(parcours=parcours_carto).delete()
        [Poi(parcours=parcours_carto, properties=jsonpoi['properties'],
             geometry=GEOSGeometry(json.dumps(jsonpoi['geometry']))).save() for jsonpoi in data['pois']]

        Zone.objects.filter(parcours=parcours_carto).delete()
        [Zone(parcours=parcours_carto, properties=jsonzone['properties'],
              geometry=GEOSGeometry(json.dumps(jsonzone['geometry']))).save() for jsonzone in data['zone']]

        if data['shape'].get('line_waypoint'):
            if hasattr(parcours_carto.shape, 'line_waypoint') and parcours_carto.shape.line_waypoint:
                parcours_carto.shape.line_waypoint.properties = data['shape']['line_waypoint']['properties']
                parcours_carto.shape.line_waypoint.geometry = GEOSGeometry(json.dumps((data['shape']['line_waypoint']['geometry'])))
                parcours_carto.shape.line_waypoint.save()
            else:
                LineWaypoint(shape=parcours_carto.shape, properties=data['shape']['line_waypoint']['properties'],
                             geometry=GEOSGeometry(json.dumps(data['shape']['line_waypoint']['geometry']))).save()

        if data['shape'].get('line_shape'):
            if hasattr(parcours_carto.shape, 'line_shape') and parcours_carto.shape.line_shape:
                parcours_carto.shape.line_shape.properties = data['shape']['line_shape']['properties']
                parcours_carto.shape.line_shape.geometry = GEOSGeometry(json.dumps((data['shape']['line_shape']['geometry'])))
                parcours_carto.shape.line_shape.save()
            else:
                LineShape(shape=parcours_carto.shape, properties=data['shape']['line_shape']['properties'],
                          geometry=GEOSGeometry(json.dumps(data['shape']['line_shape']['geometry']))).save()

        if data['shape'].get("line_shape_reduced"):
            if hasattr(parcours_carto.shape, 'line_shape_reduced') and parcours_carto.shape.line_shape_reduced:
                parcours_carto.shape.line_shape_reduced.properties = data['shape']['line_shape_reduced']['properties']
                parcours_carto.shape.line_shape_reduced.geometry = GEOSGeometry(json.dumps((data['shape']['line_shape_reduced']['geometry'])))
                parcours_carto.shape.line_shape_reduced.save()
            else:
                LineShapeReduced(shape=parcours_carto.shape, properties=data['shape']['line_shape_reduced']['properties'],
                                 geometry=GEOSGeometry(
                                     json.dumps(data['shape']['line_shape_reduced']['geometry']))).save()

        return Response('ok')

    def create(self, request, *args, **kwargs):
        data = request.data
        manif = Manif.objects.get(pk=data["manif"])
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        if not organisation.is_service_instructeur():
            if not organisation.is_structure_organisatrice() or not organisation == manif.structure_organisatrice_fk:
                return Response("Pas accès à cette manifestation", status=403)
        shape = Shape(
            routing_engine=data["shape"].get('routing_engine', ''), routing_engine_mode=data["shape"].get('routing_engine_mode', ''),
            stroke_color=data["shape"].get("stroke_color", ""), stroke_width=data["shape"].get("stroke_width", ""),
            stroke_opacity=data["shape"].get("stroke_opacity", ""))
        shape.save()
        parcours_carto = Parcours(
            manif=manif, activity=data.get("activity", ""), keyword=data.get("keyword", ""),  name=data["name"],
            description=data["description"], length_in_meter=data.get("length_in_meter"), is_private=data.get("is_private", ""),
            is_tested_by_user=data.get("is_tested_by_user", ""), difficulty=data.get("difficulty", ""), source=data.get("source", ""),
            thumb=data.get("thumb", ""), start_lat=data.get("start_lat", ""), start_lng=data.get("start_lng", ""),
            end_lat=data.get("end_lat", ""), end_lng=data.get("end_lng", ""), shape=shape, elevation=data.get("elevation"),
            osm_details=data.get("osm_details"))
        parcours_carto.save()
        if data.get('pois'):
            [Poi(parcours=parcours_carto, properties=jsonpoi['properties'],
                 geometry=GEOSGeometry(json.dumps(jsonpoi['geometry']))).save() for jsonpoi in data['pois']]

        if data.get('zone'):
            [Zone(parcours=parcours_carto, properties=jsonzone['properties'],
                  geometry=GEOSGeometry(json.dumps(jsonzone['geometry']))).save() for jsonzone in data['zone']]

        if data['shape'].get('line_waypoint'):
            LineWaypoint(shape=shape, properties=data['shape']['line_waypoint']['properties'],
                         geometry=GEOSGeometry(json.dumps(data['shape']['line_waypoint']['geometry']))).save()

        if data['shape'].get('line_shape'):
            LineShape(shape=shape, properties=data['shape']['line_shape']['properties'],
                         geometry=GEOSGeometry(json.dumps(data['shape']['line_shape']['geometry']))).save()

        if data['shape'].get('line_shape_reduced'):
            LineShapeReduced(shape=shape, properties=data['shape']['line_shape_reduced']['properties'],
                         geometry=GEOSGeometry(json.dumps(data['shape']['line_shape_reduced']['geometry']))).save()

        return Response(parcours_carto.pk)

    def destroy(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        parcours_carto = get_object_or_404(Parcours, pk=kwargs['pk'])
        if parcours_carto.manif.get_instruction():
            return Response('La manif est instruite', status=401)
        if organisation == parcours_carto.manif.structure_organisatrice_fk:
            parcours_carto.delete()
            return Response('ok')
        else:
            return Response('Le parcours ne vous appartient pas', status=403)

    @action(methods=['post'], detail=False)
    def archive(self, request, *args, **kwargs):
        data = request.data
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        parcours_carto = get_object_or_404(Parcours, pk=data['pk'])
        if not organisation.is_structure_organisatrice() or not organisation == parcours_carto.manif.structure_organisatrice_fk:
            return Response("Pas d'accès à ce parcours", status=403)
        if parcours_carto.manif.get_instruction() and not parcours_carto.a_modifier():
            return Response('La manif est instruite', status=403)
        parcours_carto.is_archived = data['status']
        parcours_carto.save()
        return Response('ok')

    @action(methods=['post'], detail=False)
    def finalise(self, request, *args, **kwargs):
        data = request.data
        parcours_carto = get_object_or_404(Parcours, pk=data['pk'])
        if parcours_carto.manif.get_instruction():
            # Marquer la line de demande de modification DONE si elle existe
            if parcours_carto.manif.modif_json:
                sortie = True
                for idx, line in enumerate(parcours_carto.manif.modif_json):
                    if 'parcours' in line and line['parcours'] == parcours_carto.pk and not line['done']:
                        parcours_carto.manif.modif_json[idx]['done'] = True
                        parcours_carto.manif.save()
                        sortie = False
                        parcours_carto.notifier_parcours_modifie()
                        break
                if sortie:
                    return Response('La manif est instruite', status=401)
                else:
                    old_data = ParcoursSerializer(parcours_carto).data
                    parcours_historique = ParcoursHistorique(parcours=parcours_carto, date=parcours_carto.date,
                                                             data_json=old_data)
                    parcours_historique.save()
                    parcours_carto.has_historique = True
            else:
                return Response('La manif est instruite', status=401)
        return Response('ok')

    @action(methods=['post'], detail=False)
    def modif(self, request, *args, **kwargs):
        data = request.data
        parcours_carto = get_object_or_404(Parcours, pk=data['pk'])
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        if not organisation.is_service_instructeur():
            return Response("Vous n'avez pas accès à cette fonction", status=403)
        if not parcours_carto.manif.get_instruction():
            return Response("La manif n'est pas encore instruite", status=401)
        # Ajouter une ligne de demande de modification
        if not parcours_carto.manif.modif_json:
            parcours_carto.manif.modif_json = []
        parcours_carto.manif.modif_json.append(
            {'date': timezone.now().strftime('%d/%m/%Y %H:%M:%S'), 'parcours': parcours_carto.pk, 'done': False})
        parcours_carto.manif.save()
        parcours_carto.notifier_parcours_a_modifier(request.user, organisation)
        return Response('ok')

    @action(methods=['get'], detail=False)
    def parcoursmanif(self, request, *args, **kwargs):
        """
        Avoir les parcours associès à une manifestation
        """
        manif = get_object_or_404(Manif, pk=request.query_params.get('pk', None))
        if not manif.has_access(request.user):
            return Response("Vous n'avez pas accès à ce parcour", status=403)
        parcours_liste = []
        [parcours_liste.append(ListParcoursSerializer(obj).data) for obj in Parcours.objects.filter(manif=manif)]
        return Response(parcours_liste)

    @action(methods=['get'], detail=False)
    def parcours(self, request, *args, **kwargs):
        """
        Avoir un parcours associès à une manifestation
        """
        parcours = get_object_or_404(Parcours, pk=request.query_params.get('pk', None))
        if not parcours.manif.has_access(request.user):
            return Response("Vous n'avez pas accès à ce parcours", status=403)

        return Response(ParcoursSerializer(parcours).data)

    @action(methods=['get'], detail=False)
    def historique(self, request, *args, **kwargs):
        """
        Avoir l'historique d'un parcours selectionné
        """
        parcours = get_object_or_404(Parcours, pk=request.query_params.get('pk', None))
        if not parcours.manif.has_access(request.user):
            return Response("Vous n'avez pas accès à ce parcours", status=403)
        parcours_liste = []
        parcours_liste.append(ParcoursSerializer(parcours).data)
        [parcours_liste.append(parcours.data_json) for parcours in ParcoursHistorique.objects.filter(parcours=parcours).order_by('-date')]
        return Response(parcours_liste)

    @action(methods=['get'], detail=False)
    def instructeur(self, request, *args, **kwargs):
        """
        Avoir les parcours d'un utilisateur selon la manif donnée
        """
        manif = get_object_or_404(Manif, pk=request.query_params.get('pk', None))
        if not request.user.is_agent_service_instructeur():
            return Response("Vous n'avez pas accès à ces parcours", status=403)
        organisation = manif.structure_organisatrice_fk
        parcours_liste = Parcours.objects.filter(manif__structure_organisatrice_fk=organisation)
        parcours_serialiser = []
        [parcours_serialiser.append(ParcoursSerializer(obj).data) for obj in parcours_liste]
        return Response({"results": parcours_serialiser})

    @action(methods=['get'], detail=False)
    def conflit_parcours(self, request, *args, **kwargs):
        """
        Lister tout les parcours en conflit
        """
        is_organisateur = True if request.user.is_organisateur() else False
        is_instructeur = True if request.user.is_agent_service_instructeur() else False
        is_agent = True if request.user.is_agent_service_consulte() else False
        manif = get_object_or_404(Manif, pk=request.query_params.get('pk', None))
        manifs = manif.get_manifs_meme_jour_meme_ville(include_self=True)
        tab_manif = []
        for manifestation in manifs:
            instruction_encours = manifestation.en_cours_instruction()
            getparcours = False
            # l'instructeur a access a tous
            if is_instructeur:
                getparcours = True
            # les agents ne voient que les manifestation en cours d'instruction
            elif is_agent and instruction_encours:
                getparcours = True
            # l'organisateur ne doit avoir que les parcours des manifestations qui sont accepté et publié au calendrier
            elif is_organisateur and instruction_encours and manifestation.get_instruction().etat == 'autorisée':
                getparcours = True
            if getparcours or manifestation.pk == manif.pk:
                tab_parcours = [ParcoursSerializer(parcour).data for parcour in manifestation.parcours.all()]
                tab_manif.append({"manif_pk": str(manifestation.pk), "manif_name": str(manifestation.nom), "parcours": tab_parcours})
        return Response(tab_manif)
