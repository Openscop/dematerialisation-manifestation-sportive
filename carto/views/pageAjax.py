from django.shortcuts import render, get_object_or_404, HttpResponse
from django.views import View
import json
from django.conf import settings

from carto.services.editorHelper import generateEditorSettings, generateEditorUser
from evenements.models import Manif
from instructions.decorators import verifier_secteur_instruction_function
from structure.models.organisation import Organisation


class PageAjax(View):
    def get(self, request):
        if request.user.is_anonymous:
            return HttpResponse('non connecté', status=403)
        if request.GET.get('manif'):
            manif = request.GET['manif']
            manif = get_object_or_404(Manif, pk=manif)
        else:
            manif = None

        if not manif.has_access(request.user):
            return HttpResponse("erreur de droit d'acces", status=403)

        hasEditableParcours = False
        if manif.get_instruction():
            # Marquer la line de demande de modification DONE si elle existe
            if manif.modif_json:
                for idx, line in enumerate(manif.modif_json):
                    if 'parcours' in line and not line['done']:
                        hasEditableParcours = True
                        break
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        isOrganisateur = organisation.is_structure_organisatrice() and organisation == manif.structure_organisatrice_fk
        isInInstruction = manif.en_cours_instruction()
        isInstructeur = False
        if isInInstruction:
            if organisation.is_service_instructeur():
                for instruction in manif.instruction.all():
                    if not verifier_secteur_instruction_function(request, False, None, **{'pk': instruction.pk}):
                        isInstructeur = True
                        break
        isAgent = True if request.user.is_agent_service_consulte() else False
        context = {}
        file = 'instructeur'
        if (isOrganisateur and not isInInstruction) \
                or (isOrganisateur and isInInstruction and hasEditableParcours):
            file = 'organisateur'
        context['openrunnerSettings'] = json.dumps(generateEditorSettings(file))
        # defini l'user pour l'editeur :

        context['editorUser'] = json.dumps(generateEditorUser(request.user))
        context['object'] = manif
        context['isInInstruction'] = isInInstruction
        context['isOrganisateur'] = isOrganisateur
        context['isInstructeur'] = isInstructeur
        context['isAgent'] = isAgent
        context['service'] = Organisation.objects.get(pk=self.request.session['service_pk'])
        context['showConflits'] = True if (isAgent or isOrganisateur or isInstructeur) else False

        showconflitslink = False
        for conflit in manif.get_manifs_meme_jour_meme_ville():
            if isAgent and conflit.en_cours_instruction():
                showconflitslink = True
            # l'organisateur n'étant pas forcement assigné ) l'instance principale de la manifestation, on prend la principale par default
            elif isOrganisateur and conflit.en_cours_instruction() and conflit.get_instruction().etat == 'autorisée':
                showconflitslink = True
            elif isInstructeur:
                showconflitslink = True
        context['showconflitslink'] = showconflitslink
        return render(request, "carto_ajax.html", context)
