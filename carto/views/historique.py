import json

from django.http import HttpResponse
from django.views import View
from django.shortcuts import render

from carto.services.editorHelper import generateEditorSettings, generateEditorUser


class Historique(View):

    def get(self, request):
        if request.user.is_anonymous:
            return HttpResponse('non connecté', status=403)
        context = {}
        file = 'historique'
        context['openrunnerSettings'] = json.dumps(generateEditorSettings(file))
        # defini l'user pour l'editeur :
        context['editorUser'] = json.dumps(generateEditorUser(request.user))
        return render(request, "carto_historique.html", context)
