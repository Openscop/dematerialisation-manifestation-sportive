import factory
import json

from .models import Parcours, Shape, LineWaypoint, Poi


class ParcoursFactory(factory.django.DjangoModelFactory):

    name = factory.Sequence(lambda n: 'Parcours_{0}'.format(n))
    shape = factory.SubFactory('carto.factories.ShapeFactory')
    manif = factory.SubFactory('evenements.factories.manif.DnmFactory')
    pois = factory.RelatedFactoryList('carto.factories.PoiFactory', 'parcours', size=2)

    # Meta
    class Meta:
        model = Parcours


class ShapeFactory(factory.django.DjangoModelFactory):

    line_waypoint = factory.RelatedFactory('carto.factories.LineWaypointFactory', "shape")

    # Meta
    class Meta:
        model = Shape


class LineWaypointFactory(factory.django.DjangoModelFactory):

    geometry = json.dumps({
        'type': 'LineString',
        'coordinates': [
          [
            4.362789,
            45.447317
          ],
          [
            4.383748,
            45.423477
          ],
          [
            4.418214,
            45.432788
          ],
          [
            4.401679,
            45.452059
          ]
        ]})

    # Meta
    class Meta:
        model = LineWaypoint


class PoiFactory(factory.django.DjangoModelFactory):

    parcours = factory.SubFactory('carto.factories.ParcoursFactory')
    properties = factory.Sequence(lambda n: {
        'title': 'salut',
        'type': "default",
        'icon': {
            'id': "{0}".format(5 if (n % 2) == 1 else 2),
            'html_code': "&#6{0}".format(6 if (n % 2) == 1 else 7),
            'name': "Poi.1",
            'i18n': "signaleur",
            'type': "default",
        }}
    )
    geometry = factory.Sequence(lambda n: json.dumps({
        'type': 'Point',
        'coordinates': [4.43 + n*0.05, 45.44]}
    ))

    # Meta
    class Meta:
        model = Poi
