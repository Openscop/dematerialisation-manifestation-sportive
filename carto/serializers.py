from rest_framework.fields import BooleanField

from carto.models import *
from rest_framework_gis import serializers
from rest_framework import serializers as original_serialisers

from carto.models.poi import PoiCategorieApi, PoiApi


class ZoneSerializer(serializers.GeoModelSerializer):
    class Meta:
        model = Zone
        exclude = ['id']


class PoiSerializer(serializers.GeoModelSerializer):
    class Meta:
        model = Poi
        exclude = ['id']


class LineWaypointSerializer(serializers.GeoModelSerializer):
    class Meta:
        model = LineWaypoint
        exclude = ['id']


class LineShapeSerializer(serializers.GeoModelSerializer):
    class Meta:
        model = LineShape
        exclude = ['id']


class LineShapeReducedSerializer(serializers.GeoModelSerializer):
    class Meta:
        model = LineShapeReduced
        exclude = ['id']


class ShapeSerializer(serializers.ModelSerializer):
    """
    Serializer pour être utilisé avec le module cartographie
    """
    line_waypoint = LineWaypointSerializer()
    line_shape = LineShapeSerializer()
    line_shape_reduced = LineShapeReducedSerializer()

    class Meta:
        model = Shape
        exclude = ['id']


class ParcoursSerializer(serializers.ModelSerializer):
    """
    Serializer pour être utilisé avec le module cartographie
    """
    pois = PoiSerializer(many=True)
    zone = ZoneSerializer(many=True)
    shape = ShapeSerializer()
    a_modifier = BooleanField()

    class Meta:
        model = Parcours
        exclude = []


class LimitedShapeApiSerializer(serializers.ModelSerializer):
    """
    Serializers utilisé pour une distribution via api externe
    """

    class Meta:
        model = Shape
        fields = ['stroke_color', 'stroke_opacity', 'stroke_width']


class ListParcoursSerializer(serializers.ModelSerializer):
    """
    Serializer pour être utilisé avec le module cartographie
    """
    a_modifier = BooleanField()
    shape = LimitedShapeApiSerializer()

    class Meta:
        model = Parcours
        exclude = []


class ShapeApiSerializer(serializers.ModelSerializer):
    """
    Serializers utilisé pour une distribution via api externe
    """
    line_waypoint = LineWaypointSerializer()
    line_shape = LineShapeSerializer()

    class Meta:
        model = Shape
        fields = ['line_waypoint', 'line_shape', 'routing_engine', 'routing_engine_mode']


class ParcoursHistoriqueApiSerializer(serializers.ModelSerializer):
    """
    Serialiser de l'historique d'un parcours
    """

    data_json = original_serialisers.SerializerMethodField()

    class Meta:
        model = ParcoursHistorique
        exclude = ()

    def get_data_json(self, data):
        obj = data.data_json
        return {"pk": obj["id"],
                "date": obj['date'],
                'name': obj['name'],
                "description": obj['description'],
                'pois': obj['pois'],
                'zone': obj['zone'],
                "shape": {
                    "line_shape": obj['shape']["line_shape"],
                    "line_waypoint": obj['shape']['line_waypoint']
                }}


class ParcoursApiSerializer(serializers.ModelSerializer):
    """
    Serializers utilisé pour une distribution via api externe
    """
    pois = PoiSerializer(many=True)
    zone = ZoneSerializer(many=True)
    shape = ShapeApiSerializer()
    historique = ParcoursHistoriqueApiSerializer(many=True)

    class Meta:
        model = Parcours
        fields = ["pois", "zone", "shape", "pk", "name", "description", "is_archived", "historique"]


class PoiApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = PoiApi
        fields = ['id', 'type', 'def_svg_symbol_id', 'html_code', 'classes', 'i18n', 'categorie']
        depth = 1


class PoiCategorieApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = PoiCategorieApi
        fields = ['id', 'title_i18n', 'symbols']
        depth = 1
