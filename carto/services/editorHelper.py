import json

from carto.serializers import PoiCategorieApiSerializer
from carto.services.repository import getPoisConfig
from configuration import settings


def generateEditorSettings(file):
    configFinal = {}
    fileToOpen = f'carto/static/carto/settings/editor/{file}.json'
    with open('carto/static/carto/settings/commun.json') as configCommun:
        configFinal.update(json.load(configCommun))
    with open(fileToOpen) as configEditor:
        configFinal.update(json.load(configEditor))
    # on récupère l’url du service depuis les settings django
    configFinal.get('routingService', {})['url'] = settings.OPENRUNNER_ROUTING_ROUTE
    configFinal.get('overlaysService', {})['url'] = settings.CARTO_OVERLAY_SERVICE_URL
    poisData = getPoisConfig()
    poisConfig = PoiCategorieApiSerializer(poisData, many=True)
    configFinal['pois'] = poisConfig.data
    return configFinal


def generateEditorUser(user):
    editorUser = {
        'hash': user.id,
        'username': user.username,
        'preferences': user.openrunner_option.get('preferences', {})
    }
    with open('carto/static/carto/settings/user.json') as userJson:
        editorUser.update(json.load(userJson))
    return editorUser
