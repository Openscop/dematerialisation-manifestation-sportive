def createPoisData(apps, schema_editor):
    Category = apps.get_model("carto", "PoiCategorieApi")
    Poi = apps.get_model("carto", "PoiApi")

    # data :
    createPoiData(Poi, Category)


def createPoiData(poiModel, categorieModel):
    categories = [
        {
            "id": "CATICO-1",
            "title_i18n": "Organisation de course"
        },
        {
            "id": "CATICO-2",
            "title_i18n": "Signalisation routière"
        },
        {
            "id": "CATICO-3",
            "title_i18n": "Informations touristiques",
        },
        {
            "id": "CATICO-6",
            "title_i18n": "Séparateur de zone"
        },
        {
            "id": "CATICO-7",
            "title_i18n": "Numéro et texte"
        },
        {
            "id": "CATICO-99",
            "title_i18n": "VTM"
        },
    ]
    poisByCat = [
        [
            {
                "id": 1,
                "type": "default",
                "def_svg_symbol_id": "41",
                "html_code": "&#x41",
                "i18n": "Signaleur"
            },
            {
                "id": 2,
                "type": "default",
                "def_svg_symbol_id": "43",
                "html_code": "&#x43",
                "i18n": "Secours"
            },
            {
                "id": 3,
                "type": "default",
                "def_svg_symbol_id": "55",
                "html_code": "&#x55",
                "i18n": "Ravitaillement"
            },
            {
                "id": 4,
                "type": "default",
                "def_svg_symbol_id": "44",
                "html_code": "&#x44",
                "i18n": "Secours mobile"
            },
            {
                "id": 5,
                "type": "default",
                "def_svg_symbol_id": "42",
                "html_code": "&#x42",
                "i18n": "Signaleur avec permis"
            },
            {
                "id": 6,
                "type": "default",
                "def_svg_symbol_id": "64",
                "html_code": "&#x64",
                "i18n": "Point de Contr\u00f4le"
            },
            {
                "id": 17,
                "type": "default",
                "def_svg_symbol_id": "6d",
                "html_code": "&#x6d",
                "i18n": "Meilleur Grimpeur"
            },
            {
                "id": 19,
                "type": "default",
                "def_svg_symbol_id": "6e",
                "html_code": "&#x6e",
                "i18n": "Sprint Meilleur Sprinteur"
            },
            {
                "id": 35,
                "type": "default",
                "def_svg_symbol_id": "3b",
                "html_code": "&#x3b",
                "i18n": "Balise"
            },
            {
                "id": 43,
                "type": "default",
                "def_svg_symbol_id": "4b",
                "html_code": "&#x4b",
                "i18n": "SDIS"
            },
            {
                "id": 45,
                "type": "default",
                "def_svg_symbol_id": "45",
                "html_code": "&#x45",
                "i18n": "Dropping Zone "
            },
            {
                "id": 48,
                "type": "default",
                "def_svg_symbol_id": "5a",
                "html_code": "&#x5a",
                "i18n": "Epongeage"
            },
            {
                "id": 49,
                "type": "default",
                "def_svg_symbol_id": "56",
                "html_code": "&#x56",
                "i18n": "Ravitaillement chaud"
            },
            {
                "id": 50,
                "type": "default",
                "def_svg_symbol_id": "59",
                "html_code": "&#x59",
                "i18n": "Bananes"
            },
            {
                "id": 51,
                "type": "default",
                "def_svg_symbol_id": "4d",
                "html_code": "&#x4d",
                "i18n": "T\u00e9l\u00e9phone secours"
            },
            {
                "id": 52,
                "type": "default",
                "def_svg_symbol_id": "65",
                "html_code": "&#x65",
                "i18n": "Chronom\u00e8tre"
            },
            {
                "id": 53,
                "type": "default",
                "def_svg_symbol_id": "66",
                "html_code": "&#x66",
                "i18n": "Barri\u00e8re horaire"
            },
            {
                "id": 54,
                "type": "default",
                "def_svg_symbol_id": "46",
                "html_code": "&#x46",
                "i18n": "D\u00e9fibrillateur"
            },
            {
                "id": 55,
                "type": "default",
                "def_svg_symbol_id": "47",
                "html_code": "&#x47",
                "i18n": "M\u00e9decin"
            },
            {
                "id": 56,
                "type": "default",
                "def_svg_symbol_id": "38",
                "html_code": "&#x38",
                "i18n": "Panneau randonneur"
            },
            {
                "id": 57,
                "type": "default",
                "def_svg_symbol_id": "39",
                "html_code": "&#x39",
                "i18n": "POI G\u00e9n\u00e9rique"
            },
            {
                "id": 59,
                "type": "default",
                "def_svg_symbol_id": "26",
                "html_code": "&#x26",
                "i18n": "Note"
            },
            {
                "id": 60,
                "type": "default",
                "def_svg_symbol_id": "63",
                "html_code": "&#x63",
                "i18n": "Photographe"
            },
            {
                "id": 61,
                "type": "default",
                "def_svg_symbol_id": "54",
                "html_code": "&#x54",
                "i18n": "Massage"
            },
            {
                "id": 62,
                "type": "default",
                "def_svg_symbol_id": "68",
                "html_code": "&#x68",
                "i18n": "Col 1\u00e8re cat."
            },
            {
                "id": 63,
                "type": "default",
                "def_svg_symbol_id": "69",
                "html_code": "&#x69",
                "i18n": "Col 2\u00e8me cat."
            },
            {
                "id": 64,
                "type": "default",
                "def_svg_symbol_id": "6a",
                "html_code": "&#x6a",
                "i18n": "Col 3\u00e8me cat."
            },
            {
                "id": 65,
                "type": "default",
                "def_svg_symbol_id": "6b",
                "html_code": "&#x6b",
                "i18n": "Col 4\u00e8me cat."
            },
            {
                "id": 66,
                "type": "default",
                "def_svg_symbol_id": "6c",
                "html_code": "&#x6c",
                "i18n": "Col Hors cat."
            },
            {
                "id": 67,
                "type": "default",
                "def_svg_symbol_id": "e9",
                "html_code": "&#xe9",
                "i18n": "Parc \u00e0 v\u00e9los"
            },
            {
                "id": 73,
                "type": "default",
                "def_svg_symbol_id": "6f",
                "html_code": "&#x6f",
                "i18n": "D\u00e9part r\u00e9el"
            },
            {
                "id": 103,
                "type": "default",
                "def_svg_symbol_id": "58",
                "html_code": "&#x58",
                "i18n": "Oranges"
            },
            {
                "id": 104,
                "type": "default",
                "def_svg_symbol_id": "57",
                "html_code": "&#x57",
                "i18n": "Bouteille d'eau"
            },
            {
                "id": 105,
                "type": "default",
                "def_svg_symbol_id": "67",
                "html_code": "&#x67",
                "i18n": "Col"
            },
            {
                "id": 114,
                "type": "default",
                "def_svg_symbol_id": "62",
                "html_code": "&#x62",
                "i18n": "Musique"
            }
        ],
        [
            {
                "id": 7,
                "type": "default",
                "def_svg_symbol_id": "74",
                "html_code": "&#x74",
                "i18n": "Virage \u00e0 droite"
            },
            {
                "id": 8,
                "type": "default",
                "def_svg_symbol_id": "75",
                "html_code": "&#x75",
                "i18n": "Virage \u00e0 gauche"
            },
            {
                "id": 9,
                "type": "default",
                "def_svg_symbol_id": "76",
                "html_code": "&#x76",
                "i18n": "Succession de virages"
            },
            {
                "id": 10,
                "type": "default",
                "def_svg_symbol_id": "77",
                "html_code": "&#x77",
                "i18n": "Succession de virages"
            },
            {
                "id": 14,
                "type": "default",
                "def_svg_symbol_id": "78",
                "html_code": "&#x78",
                "i18n": "Danger"
            },
            {
                "id": 15,
                "type": "default",
                "def_svg_symbol_id": "79",
                "html_code": "&#x79",
                "i18n": "Descente dangereuse"
            },
            {
                "id": 16,
                "type": "default",
                "def_svg_symbol_id": "7a",
                "html_code": "&#x7a",
                "i18n": "Risque de chute de pierres"
            },
            {
                "id": 36,
                "type": "default",
                "def_svg_symbol_id": "31",
                "html_code": "&#x31",
                "i18n": "Tunnel"
            },
            {
                "id": 109,
                "type": "default",
                "def_svg_symbol_id": "32",
                "html_code": "&#x32",
                "i18n": "R\u00e9tr\u00e9cissement"
            },
            {
                "id": 110,
                "type": "default",
                "def_svg_symbol_id": "33",
                "html_code": "&#x33",
                "i18n": "R\u00e9tr\u00e9cissement \u00e0 droite"
            },
            {
                "id": 111,
                "type": "default",
                "def_svg_symbol_id": "34",
                "html_code": "&#x34",
                "i18n": "R\u00e9tr\u00e9cissement \u00e0 gauche"
            },
            {
                "id": 112,
                "type": "default",
                "def_svg_symbol_id": "35",
                "html_code": "&#x35",
                "i18n": "Ralentisseur"
            }
        ],
        [
            {
                "id": 20,
                "type": "default",
                "def_svg_symbol_id": "51",
                "html_code": "&#x51",
                "i18n": "Parking"
            },
            {
                "id": 21,
                "type": "default",
                "def_svg_symbol_id": "52",
                "html_code": "&#x52",
                "i18n": "Toilettes"
            },
            {
                "id": 34,
                "type": "default",
                "def_svg_symbol_id": "37",
                "html_code": "&#x37",
                "i18n": "T\u00e9l\u00e9ph\u00e9rique"
            },
            {
                "id": 37,
                "type": "default",
                "def_svg_symbol_id": "71",
                "html_code": "&#x71",
                "i18n": "Point d'eau potable"
            },
            {
                "id": 38,
                "type": "default",
                "def_svg_symbol_id": "70",
                "html_code": "&#x70",
                "i18n": "Poste de d\u00e9pannage"
            },
            {
                "id": 40,
                "type": "default",
                "def_svg_symbol_id": "72",
                "html_code": "&#x72",
                "i18n": "Point de vue"
            },
            {
                "id": 41,
                "type": "default",
                "def_svg_symbol_id": "73",
                "html_code": "&#x73",
                "i18n": "Point d'information"
            },
            {
                "id": 68,
                "type": "default",
                "def_svg_symbol_id": "e8",
                "html_code": "&#xe8",
                "i18n": "Vestiaires"
            },
            {
                "id": 69,
                "type": "default",
                "def_svg_symbol_id": "50",
                "html_code": "&#x50",
                "i18n": "Douches"
            },
            {
                "id": 70,
                "type": "default",
                "def_svg_symbol_id": "4e",
                "html_code": "&#x4e",
                "i18n": "Navette Bus"
            },
            {
                "id": 71,
                "type": "default",
                "def_svg_symbol_id": "4f",
                "html_code": "&#x4f",
                "i18n": "Arr\u00eat de bus"
            },
            {
                "id": 72,
                "type": "default",
                "def_svg_symbol_id": "61",
                "html_code": "&#x61",
                "i18n": "Aire de recyclage"
            },
            {
                "id": 75,
                "type": "default",
                "def_svg_symbol_id": "53",
                "html_code": "&#x53",
                "i18n": "Toilettes Handicap\u00e9s"
            }
        ],
        [
            {
                "id": 200,
                "type": "split",
                "def_svg_symbol_id": "3a",
                "html_code": "&#x3a",
                "i18n": "Swmirun d\u00e9but natation"
            },
            {
                "id": 201,
                "type": "split",
                "def_svg_symbol_id": "2e",
                "html_code": "&#x2e",
                "i18n": "Swmirun fin natation"
            },
            {
                "id": 202,
                "type": "split",
                "def_svg_symbol_id": "f9",
                "html_code": "&#xf9",
                "i18n": "Gravel d\u00e9but section"
            },
            {
                "id": 203,
                "type": "split",
                "def_svg_symbol_id": "25",
                "html_code": "&#x25",
                "i18n": "Gravel fin section"
            },
            {
                "id": 204,
                "type": "split",
                "def_svg_symbol_id": "2f",
                "html_code": "&#x2f",
                "i18n": "Trail running"
            },
            {
                "id": 205,
                "type": "split",
                "def_svg_symbol_id": "30",
                "html_code": "&#x30",
                "i18n": "Kayak"
            },
            {
                "id": 206,
                "type": "split",
                "def_svg_symbol_id": "78",
                "html_code": "&#x78",
                "i18n": "Danger"
            }
        ],
        [
            {
                "id": 300,
                "type": "number",
                "def_svg_symbol_id": "39",
                "html_code": "&#x39",
                "i18n": "POI G\u00e9n\u00e9rique"
            },
            {
                "id": 301,
                "type": "text",
                "def_svg_symbol_id": "39",
                "html_code": "&#x39",
                "i18n": "POI G\u00e9n\u00e9rique"
            }
        ],
        [
            {
                "id": "AES",
                "i18n": "AES",
                "type": "vtm",
                "classes": "picto-vtm AES"
            },
            {
                "id": "CH",
                "i18n": "CH",
                "type": "vtm",
                "classes": "picto-vtm CH"
            },
            {
                "id": "CSP",
                "i18n": "CSP",
                "type": "vtm",
                "classes": "picto-vtm CSP"
            },
            {
                "id": "depan",
                "i18n": "Depan",
                "type": "vtm",
                "classes": "picto-vtm depan"
            },
            {
                "id": "DES",
                "i18n": "DES",
                "type": "vtm",
                "classes": "picto-vtm DES"
            },
            {
                "id": "evac_sanitaire",
                "i18n": "Evac sanitaire",
                "type": "vtm",
                "classes": "picto-vtm evac_sanitaire"
            },
            {
                "id": "extincteur",
                "i18n": "Extincteur",
                "type": "vtm",
                "classes": "picto-vtm extincteur"
            },
            {
                "id": "fin_zone",
                "i18n": "Fin zone",
                "type": "vtm",
                "classes": "picto-vtm fin_zone"
            },
            {
                "id": "lignePTT",
                "i18n": "ligne PTT",
                "type": "vtm",
                "classes": "picto-vtm lignePTT"
            },
            {
                "id": "medecin",
                "i18n": "Medecin",
                "type": "vtm",
                "classes": "picto-vtm medecin"
            },
            {
                "id": "panneau-direct-ZP-D",
                "i18n": "Panneau direct ZP D",
                "type": "vtm",
                "classes": "picto-vtm panneau-direct-ZP-D"
            },
            {
                "id": "panneau-direct-ZP-G",
                "i18n": "Panneau direct ZP G",
                "type": "vtm",
                "classes": "picto-vtm panneau-direct-ZP-G"
            },
            {
                "id": "pietons_interdit",
                "i18n": "Pietons interdit",
                "type": "vtm",
                "classes": "picto-vtm pietons_interdit"
            },
            {
                "id": "poste_commissaire",
                "i18n": "Poste commissaire",
                "type": "vtm",
                "classes": "picto-vtm poste_commissaire"
            },
            {
                "id": "presignal_AES",
                "i18n": "Presignal AES",
                "type": "vtm",
                "classes": "picto-vtm presignal_AES"
            },
            {
                "id": "presignal_control_hor",
                "i18n": "Presignal control hor",
                "type": "vtm",
                "classes": "picto-vtm presignal_control_hor"
            },
            {
                "id": "presignal_poste_commissaire",
                "i18n": "Presignal poste commissaire",
                "type": "vtm",
                "classes": "picto-vtm presignal_poste_commissaire"
            },
            {
                "id": "presignal_zebra",
                "i18n": "Presignal zebra",
                "type": "vtm",
                "classes": "picto-vtm presignal_zebra"
            },
            {
                "id": "public_interdit",
                "i18n": "Public interdit",
                "type": "vtm",
                "classes": "picto-vtm public_interdit"
            },
            {
                "id": "zebra-chgt-dir",
                "i18n": "Zebra chgt dir",
                "type": "vtm",
                "classes": "picto-vtm zebra-chgt-dir"
            },
            {
                "id": "zone_casque",
                "i18n": "Zone casque",
                "type": "vtm",
                "classes": "picto-vtm zone_casque"
            },
            {
                "id": "ZP",
                "i18n": "ZP",
                "type": "vtm",
                "classes": "picto-vtm ZP"
            },
            {
                "id": "ZP_interdit_ok",
                "i18n": "ZP interdit ok",
                "type": "vtm",
                "classes": "picto-vtm ZP_interdit_ok"
            }
        ]
    ]

    # on crée les categories en premier pour pouvoir utiliser leur id apres
    categoriesList = list()
    for category in categories:
        item = categorieModel(id=category.get('id'), title_i18n=category.get('title_i18n'))
        categoriesList.append(item)
    categorieModel.objects.bulk_create(categoriesList)

    # on crée les differents pois en utilisant leur index dans le tableau comme pk des category
    poisList = list()
    index = 0

    for pois in poisByCat:
        for poi in pois:
            item = poiModel(id=poi.get('id'), i18n=poi.get('i18n'), classes=poi.get('classes'),
                            html_code=poi.get('html_code'), type=poi.get('type'),
                            def_svg_symbol_id=poi.get('def_svg_symbol_id'),
                            categorie=categorieModel.objects.get(pk=categories[index].get('id')))
            poisList.append(item)
        index += 1
    poiModel.objects.bulk_create(poisList)


def removePoisData(apps, schema_editor):
    Category = apps.get_model("carto", "PoiCategorieApi")
    Poi = apps.get_model("carto", "PoiApi")
    Category.objects.all().delete()
    Poi.objects.all().delete()
