from carto.models.poi import PoiApi, PoiCategorieApi


def getPoisConfig():
    return PoiCategorieApi.objects.all().order_by('id')


def getPoisList():
    return PoiApi.objects.all().order_by('id')
