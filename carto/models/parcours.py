from django.contrib.gis.db import models

from administrative_division.models import Commune, Arrondissement, Departement
from evaluation_incidence.models import N2kSite, RnrZone


class Parcours(models.Model):
    manif = models.ForeignKey('evenements.Manif', related_name='parcours', on_delete=models.CASCADE, null=True, blank=True)

    activity = models.CharField(max_length=300, verbose_name="Activité", null=True, blank=True)
    name = models.CharField(max_length=100, verbose_name="Nom", null=True, blank=True)
    keyword = models.CharField(max_length=150, verbose_name="Keyword", null=True, blank=True)
    description = models.TextField("description du parcours", default='', blank=True)
    length_in_meter = models.FloatField(verbose_name="Longeur", null=True, blank=True)
    is_private = models.CharField(verbose_name='is_private', max_length=150, null=True, blank=True)
    is_tested_by_user = models.BooleanField(verbose_name='tested', null=True, blank=True)
    difficulty = models.CharField(verbose_name="difficulty", max_length=150, null=True, blank=True)
    source = models.CharField(verbose_name='source', max_length=150, null=True, blank=True)
    thumb = models.CharField(verbose_name="thumb", max_length=150, null=True, blank=True)
    start_lat = models.CharField(verbose_name="start_lat", max_length=150, null=True, blank=True)
    start_lng = models.CharField(verbose_name="start_lng", max_length=150, null=True, blank=True)
    end_lat = models.CharField(verbose_name="end_lat", max_length=150, null=True, blank=True)
    end_lng = models.CharField(verbose_name="end_lng", max_length=150, null=True, blank=True)
    is_archived = models.BooleanField(verbose_name='archived', default=False)

    shape = models.OneToOneField("carto.Shape", on_delete=models.CASCADE)
    elevation = models.JSONField(max_length=5000, verbose_name="elevation", null=True, blank=True)
    osm_details = models.JSONField(max_length=5000, verbose_name='osm_details', null=True, blank=True)

    date = models.DateTimeField(auto_now_add=True)
    ancient_id = models.CharField(max_length=5000, verbose_name="ancien id", null=True, blank=True)

    has_historique = models.BooleanField(default=False, verbose_name="Le aprcours a un ou plusieurs historique")

    def a_modifier(self):
        # savoir si le parcours a été invalidé par l'instructeur
        if self.manif.modif_json:
            for line in self.manif.modif_json:
                if 'parcours' in line and line['parcours'] == self.pk and not line['done']:
                    return True
        return False

    def notifier_parcours_modifie(self):
        """ Notifier la modification du parcours si l'intruction est en cours """
        from messagerie.models.message import Message
        expediteur = self.manif.declarant
        destinatairesaction = []
        for instruction in self.manif.instruction.all():
            destinatairesaction += instruction.get_instructeurs_avec_services()
        titre = "Parcours modifié par l'organisateur"
        contenu = "<p>" + titre + ' pour la manifestation ' + self.manif.nom + \
                  " concernant le parcours :</p><p><em>" + self.name + "</em></p>"
        Message.objects.creer_et_envoyer('action', [expediteur, self.manif.structure_organisatrice_fk], destinatairesaction, titre, contenu,
                                         manifestation_liee=self.manif, objet_lie_nature="dossier")

    def notifier_parcours_a_modifier(self, demandeur, service_demandeur):
        """ Notifier la demande de modification du parcours si l'intruction est en cours """
        from messagerie.models.message import Message
        titre = "Modification de parcours demandé par l'instructeur"
        contenu = "<p>" + titre + ' pour la manifestation ' + self.manif.nom + \
                  " concernant le parcours :</p><p><em>" + self.name + "</em></p>"
        Message.objects.creer_et_envoyer('action', [demandeur, service_demandeur],
                                         self.manif.structure_organisatrice_fk.get_utilisateurs_avec_service(), titre, contenu,
                                         manifestation_liee=self.manif, objet_lie_nature="dossier")

    def lister_communes_traversees(self):
        return Commune.objects.filter(geographie__geom__intersects=self.shape.line_shape.geometry).order_by("zip_code")

    def lister_arrondissemeent_traverses(self):
        return Arrondissement.objects.filter(geographie__geom__intersects=self.shape.line_shape.geometry)

    def lister_departement_traverses(self):
        return Departement.objects.filter(geographie__geom__intersects=self.shape.line_shape.geometry)

    def lister_natura_traverses(self):
        return N2kSite.objects.filter(geographie__geom__intersects=self.shape.line_shape.geometry)

    def lister_rnr_traverses(self):
        return RnrZone.objects.filter(geographie__geom__intersects=self.shape.line_shape.geometry)

class Shape(models.Model):
    routing_engine = models.CharField(max_length=150, null=True, blank=True)
    routing_engine_mode = models.CharField(max_length=150, null=True, blank=True)
    stroke_color = models.CharField(max_length=50)
    stroke_opacity = models.CharField(max_length=5)
    stroke_width = models.CharField(max_length=50)


class PointWaypointEncoded(models.Model):
    shape = models.ForeignKey(Shape, on_delete=models.CASCADE, related_name="waypoints_encoded")
    properties = models.JSONField(null=True, blank=True)
    geometry = models.PointField()


class LineWaypoint(models.Model):
    shape = models.OneToOneField(Shape, on_delete=models.CASCADE, related_name='line_waypoint')
    properties = models.JSONField(null=True, blank=True)
    geometry = models.LineStringField()


class PointShapeEncoded(models.Model):
    shape = models.ForeignKey(Shape, on_delete=models.CASCADE, related_name="shape_encoded")
    properties = models.JSONField(null=True, blank=True)
    geometry = models.PointField()


class LineShape(models.Model):
    shape = models.OneToOneField(Shape, on_delete=models.CASCADE, related_name='line_shape')
    properties = models.JSONField(null=True, blank=True)
    geometry = models.LineStringField()


class PointShapeReducedEncoded(models.Model):
    shape = models.ForeignKey(Shape, on_delete=models.CASCADE, related_name="shape_reduced_encoded",)
    properties = models.JSONField(null=True, blank=True)
    geometry = models.PointField()


class LineShapeReduced(models.Model):
    shape = models.OneToOneField(Shape, on_delete=models.CASCADE, related_name='line_shape_reduced')
    properties = models.JSONField(null=True, blank=True)
    geometry = models.LineStringField()


class Poi(models.Model):
    parcours = models.ForeignKey(Parcours, on_delete=models.CASCADE, related_name='pois')
    properties = models.JSONField(null=True, blank=True)
    geometry = models.PointField()


class Zone(models.Model):
    parcours = models.ForeignKey(Parcours, on_delete=models.CASCADE, related_name='zone')
    properties = models.JSONField(null=True, blank=True)
    geometry = models.PolygonField()


class ParcoursHistorique(models.Model):
    parcours = models.ForeignKey(Parcours, on_delete=models.CASCADE, related_name='historique')
    data_json = models.JSONField(verbose_name="donnée", null=True, blank=True, max_length=50000000000)
    date = models.DateField(null=True, blank=True)
