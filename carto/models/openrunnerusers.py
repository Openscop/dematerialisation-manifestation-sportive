# coding: utf-8
from django.db import models
from django.utils import timezone


class OpenRunnerUsersQuerySet(models.QuerySet):
    """ Queryset des informations connues OpenRunner """

    # Getter
    def is_known(self, user):
        """
        Renvoyer si des données pour l'utilisateur sont connues

        Permet de ne pas avoir à envoyer une requête à l'API Openrunner
        pour créer un utilisateur, et gagner quelques ressources.
        :param user: utilisateur ou nom d'utilisateur
        """
        if not isinstance(user, str):
            user = user.username
        return self.filter(username=user).exists()


class OpenRunnerUsers(models.Model):
    """ Modèle destiné à conserver les identifiants d'utilisateurs connus sur OR """

    # Champs
    username = models.CharField(max_length=100, db_index=True, verbose_name="Nom d'utilisateur")
    created = models.DateTimeField(default=timezone.now)
    objects = OpenRunnerUsersQuerySet.as_manager()

    # Meta
    class Meta:
        verbose_name = "Données d'utilisateurs OpenRunner"
        verbose_name_plural = "Données d'utilisateurs OpenRunner"
        app_label = 'carto'
