from django.db import models

"""
Ajout des Api dans les nom des models car Poi existe deja dans les parcours
"""


class PoiCategorieApi(models.Model):
    """ Modèle destiné à lister la totalité des informations des POI que la plateforme supporte, il définit les
    différentes catégories disponibles pour les POI """
    id = models.CharField(max_length=100, verbose_name="Id de la catégorie utilisé par l'éditeur", primary_key=True)
    title_i18n = models.CharField(max_length=100, verbose_name="Nom de la catégorie")

    def __str__(self):
        return self.title_i18n


class PoiApi(models.Model):
    """ Modèle destiné à lister la totalité des informations des POI que la plateforme supporte, il définit les
    différents POI disponible """
    id = models.CharField(max_length=100, primary_key=True)
    categorie = models.ForeignKey(PoiCategorieApi, on_delete=models.SET_DEFAULT, default="CATICO-1", related_name="symbols")
    # Champs poi
    type = models.CharField(max_length=100, verbose_name="type de POI")
    def_svg_symbol_id = models.CharField(max_length=100, null=True, blank=True, verbose_name="symbol pour utilisation svg (obsolete)")
    html_code = models.CharField(max_length=100, null=True, blank=True, verbose_name="html code du POI")
    classes = models.CharField(max_length=100, null=True, blank=True, verbose_name="classes du POI")
    i18n = models.CharField(max_length=100, verbose_name="Nom du POI")
    description = models.CharField(max_length=255, null=True, blank=True, verbose_name="Description")

    def __str__(self):
        return self.i18n
