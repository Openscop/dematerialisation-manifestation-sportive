from django.contrib.gis.db import models


class CommuneGeo(models.Model):
    """
    class contenant la géographie d'une commune
    """
    commune = models.OneToOneField("administrative_division.Commune", null=True, blank=True, related_name='geographie', verbose_name="Commune rattachée", on_delete=models.SET_NULL)
    insee = models.CharField(default="", verbose_name="numéro insee", max_length=10)
    geom = models.MultiPolygonField(srid=4365, blank=True, null=True)

    def __str__(self):
        return f"Géographie de la commune {str(self.commune)}"


class ArrondissementsGeo(models.Model):
    """
    Géographie d'un arrondissement
    """
    arrondissement = models.OneToOneField("administrative_division.Arrondissement", null=True, blank=True, related_name="geographie", verbose_name="Arrondissement rattachée", on_delete=models.SET_NULL)
    insee = models.CharField(default="", verbose_name="numéro insee", max_length=10)
    geom = models.MultiPolygonField(srid=4365, blank=True, null=True)

    def __str__(self):
        return f"Géographie de l'arrondissement :  {str(self.arrondissement)}"


class DepartementGeo(models.Model):
    """
    Géographie d'un département
    """
    departement = models.OneToOneField("administrative_division.Departement", null=True, blank=True, related_name="geographie", verbose_name="Departement rattaché", on_delete=models.SET_NULL)
    insee = models.CharField(default="", verbose_name="numéro insee", max_length=10)
    geom = models.MultiPolygonField(srid=4365, blank=True, null=True)

    def __str__(self):
        return f"Géographie du département : {str(self.departement)}"


class NK2000Geo(models.Model):
    """
    Géographie d'une zone natura 2000
    """
    n2k = models.OneToOneField("evaluation_incidence.N2kSite", null=True, blank=True, related_name="geographie", verbose_name="Site rattaché", on_delete=models.SET_NULL)
    geom = models.MultiPolygonField(srid=4365, blank=True, null=True)

    def __str__(self):
        return f"Géographie de la zone Natura 2000 :  {str(self.n2k)}"


class RnrZoneGeo(models.Model):
    """
    Géographie d'une zone rnr
    """
    rnr = models.OneToOneField("evaluation_incidence.RnrZone", null=True, blank=True, related_name="geographie", verbose_name="Site rnr rattaché", on_delete=models.SET_NULL)
    geom = models.MultiPolygonField(srid=4365, blank=True, null=True)

    def __str__(self):
        return f"Géographie de la zone Rnr : {str(self.rnr)}"
