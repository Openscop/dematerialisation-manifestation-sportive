import os
import time
from django.contrib.contenttypes.models import ContentType
from django.test import tag, override_settings
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

from core.models import ConfigurationGlobale
from evenements.tests.test_base_selenium import SeleniumCommunClass
from evenements.factories import DnmFactory
from instructions.factories import InstructionFactory
from carto.factories import ParcoursFactory


class CartoNouveauParcoursTests(SeleniumCommunClass):
    """
    Test de l'onglet Carto
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ Nouveau parcours (Sel) =============')
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        SeleniumCommunClass.init_setup(cls)
        ConfigurationGlobale.objects.create(pk=1, mail_action=True, mail_suivi=True, mail_tracabilite=True, mail_actualite=True)
        cls.manifestation.nom = "La grosse course"
        cls.manifestation.save()

        cls.manif = DnmFactory.create(ville_depart=cls.commune, structure_organisatrice_fk=cls.structure_organisatrice, activite=cls.activ,
                                      nom='Manifestation_Test')
        ParcoursFactory.create(manif=cls.manif)
        InstructionFactory.create(manif=cls.manif)

        super().setUpClass()

    @tag('selenium')
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
    @override_settings(DEBUG=True)
    def test_Carto_New(self):
        """
        Test de création d'un nouveau parcours
        """

        print('**** test 0 finalisation de la manifestation ****')
        self.connexion('organisateur')
        # Tester la présence de la manifestation
        self.presence_avis('atraiter')
        self.vue_detail()
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.XPATH, "//button[contains(.,'Cartographie')]").click()
        time.sleep(self.DELAY)
        print('**** test 1 Test des boutons et carte ****')
        # Bouton Plein écran
        self.assertIn('Plein écran', self.selenium.page_source)
        self.selenium.find_element(By.ID, 'btn-editor-fullscreen')
        # Bouton Nouveau parcours
        self.assertIn('Créer un nouveau parcours', self.selenium.page_source)
        self.selenium.find_element(By.ID, 'btn-selecteur-add-parcours')
        print('**** test 2 Test de l\'affichage de la carte ****')
        # Tester le nombre de tuiles chargées
        time.sleep(self.DELAY)
        tuiles = self.selenium.find_elements(By.CLASS_NAME, "leaflet-tile-loaded")
        # time.sleep(10)
        self.assertEqual(10, len(tuiles))
        # Tester le canvas
        self.selenium.find_element(By.XPATH, '//div[contains(@class, "leaflet-zoom-animated")]')
        print('**** test 3.5 Test du chargement du parcours ****')
        # Tester le nombre d'icônes

        self.selenium.find_element(By.XPATH, '//div[contains(@id, "icon-show-parcours")]').click()
        time.sleep(self.DELAY * 5)
        icons = self.selenium.find_elements(By.CLASS_NAME, "leaflet-marker-icon")
        self.assertEqual(8, len(icons))
        time.sleep(self.DELAY)
        # Vérifier parcours enregistrés -> deux icônes download
        dload_icons = self.selenium.find_elements(By.XPATH, '//div[contains(@id, "icon-download-gpx-")]')
        self.assertEqual(1, len(dload_icons))
        print('**** test 6 Test de téléchargement de trace gpx ****')
        time.sleep(self.DELAY)
        dload_icons[0].click()
        time.sleep(self.DELAY * 5)
        self.assertTrue(os.path.exists('/tmp/Parcours_0.gpx'))
        self.selenium.find_element(By.XPATH, '//div[contains(@id, "icon-unload-")]').click()
        time.sleep(self.DELAY)
        print('**** test 3 Test du menu de droite ****')
        # Menu intégré à droite
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element(By.XPATH, '//a[@class="fak fa-or-expand-screen"]')
        self.selenium.find_element(By.XPATH, '//a[@class="fal fa-plus"]')
        self.selenium.find_element(By.XPATH, '//a[@class="fal fa-minus"]')
        self.selenium.find_element(By.XPATH, '//a[@class="fal fa-layer-group"]')
        self.selenium.find_element(By.XPATH, '//a[@class="fas fa-layer-plus"]')
        self.selenium.find_element(By.XPATH, '//a[@class="fal fa-sliders-v"]')
        self.selenium.find_element(By.XPATH, '//a[@class="fal fa-print"]')
        self.selenium.find_element(By.XPATH, '//a[@class="fal fa-street-view"]')
        print('**** test 4 Test du menu de gauche ****')
        # Créer parcours
        self.selenium.find_element(By.ID, 'btn-selecteur-add-parcours').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn-create-new-parcours').click()
        time.sleep(self.DELAY)
        # Menu de gauche
        self.selenium.find_element(By.XPATH, '//input[@placeholder="Centrer sur"]')

        self.assertIn('Tracer un parcours', self.selenium.page_source)
        self.assertIn('Mode de suivi des voies', self.selenium.page_source)
        self.assertIn('Cyclisme sur route', self.selenium.page_source)
        self.assertIn('Prochain point : auto', self.selenium.page_source)
        self.assertIn('Affichage des points', self.selenium.page_source)
        self.assertIn('Rejoindre le départ', self.selenium.page_source)
        self.assertIn('Inverser le tracé', self.selenium.page_source)
        self.assertIn('Faire demi-tour', self.selenium.page_source)
        self.assertIn('Aspect du tracé', self.selenium.page_source)

        self.assertIn('Enrichir le tracé', self.selenium.page_source)
        self.assertIn('Ajouter un POI', self.selenium.page_source)
        self.assertIn('Créer une zone', self.selenium.page_source)

        self.selenium.find_element(By.XPATH, '//div[@data-menu="routing"]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//div[@data-mode="NONE"]')
        self.selenium.find_element(By.XPATH, '//div[@data-mode="WALK"]')
        self.selenium.find_element(By.XPATH, '//div[@data-mode="BIKE"]')
        self.selenium.find_element(By.XPATH, '//div[@data-mode="GRAVEL-BIKE"]')
        self.selenium.find_element(By.XPATH, '//div[@data-mode="CAR"]').click()
        self.assertIn('Mode de suivi des voies', self.selenium.page_source)
        self.assertIn('Voiture', self.selenium.page_source)

        self.selenium.find_element(By.XPATH, '//div[@data-menu="pointType"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Prochain point : manuel', self.selenium.page_source)

        self.selenium.find_element(By.XPATH, '//div[@data-menu="options"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Épaisseur du tracé', self.selenium.page_source)
        self.assertIn('Opacité du tracé', self.selenium.page_source)
        self.assertIn('Couleur du tracé', self.selenium.page_source)
        self.selenium.find_element(By.XPATH, '//div[@data-menu="options"]').click()

        self.selenium.find_element(By.XPATH, '//div[@data-menu="pois"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Choisissez la couleur et la forme du POI, puis glissez-le sur la carte', self.selenium.page_source)
        self.assertIn('Couleur', self.selenium.page_source)
        self.assertIn('Organisation de course', self.selenium.page_source)
        self.assertIn('Signalisation routière', self.selenium.page_source)
        self.assertIn('Informations touristiques', self.selenium.page_source)
        self.assertIn('Séparateur de zone', self.selenium.page_source)
        self.assertIn('Numéro et texte', self.selenium.page_source)
        self.assertIn('VTM', self.selenium.page_source)
        self.selenium.find_element(By.XPATH, '//div[@data-menu="pois"]').click()

        # self.selenium.find_element(By.XPATH, '//div[@data-menu="poisColor"]').click()
        # time.sleep(self.DELAY)
        # self.assertIn('Couleur des POIs', self.selenium.page_source)
        # self.selenium.find_element(By.XPATH, '//div[@data-menu="poisColor"]').click()
        print('**** test 5 Test d\'enregistrement de parcours ****')
        # Enregistrer le parcours
        self.selenium.find_element(By.XPATH, '//div[contains(@id, "icon-save-parcours--")]').click()
        time.sleep(self.DELAY)
        nom = self.selenium.find_element(By.XPATH, '//input[@id="save-parcours-name"]')
        nom.send_keys(Keys.CONTROL, 'a')
        nom.send_keys(Keys.DELETE)
        nom.send_keys("Parcours test")
        time.sleep(self.DELAY)
        nom = self.selenium.find_element(By.XPATH, '//input[@id="save-parcours-description"]')
        nom.send_keys("Description parcours test")
        self.selenium.find_element(By.XPATH, '//button[@id="btn-save-parcours"]').click()
        time.sleep(self.DELAY)

        print('**** test 7 Test de suppression de parcours ****')
        # Suprimer un parcours
        self.selenium.find_element(By.ID, 'btn-selecteur-add-parcours').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn-create-new-parcours').click()
        time.sleep(self.DELAY)
        del_icons = self.selenium.find_elements(By.XPATH, '//div[contains(@aria-label, "Supprimer le parcours")]')
        self.assertEqual(2, len(del_icons))
        self.selenium.find_element(By.XPATH, '//div[contains(@id, "icon-delete-parcours--")]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@id="btn-delete-parcours"]').click()
        # Vérifier un parcours
        selecteurs = self.selenium.find_elements(By.XPATH, '//div[contains(@class, "selecteur-element")]')
        self.assertEqual(2, len(selecteurs))
        del_icons = self.selenium.find_elements(By.XPATH, '//div[contains(@aria-label, "Supprimer le parcours")]')
        self.assertEqual(1, len(del_icons))
        print('**** test 8 Test de duplication de parcours ****')
        # Dupliquer un parcours
        self.selenium.find_element(By.ID, 'btn-selecteur-add-parcours').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn-open-duplicate-parcours').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//input[@placeholder="Sélectionnez ou tapez le nom d\'un parcours"]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Parcours test")]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn-duplicate-parcours').click()
        time.sleep(self.DELAY)
        # self.assertTrue(False)
        del_icons = self.selenium.find_elements(By.XPATH, '//div[contains(@aria-label, "Supprimer le parcours")]')
        self.assertEqual(2, len(del_icons))
        # Tester le nombre d'icônes
        # icons = self.selenium.find_elements(By.CLASS_NAME, "leaflet-marker-icon")
        # self.assertEqual(10, len(icons))
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//div[contains(@id, "icon-delete-parcours-4")]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@id="btn-delete-parcours"]').click()
        print('**** test 9 Test de chargement de parcours ****')
        # Charger un parcours existant
        self.selenium.find_element(By.ID, 'btn-selecteur-add-parcours').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn-open-search-parcours').click()
        time.sleep(self.DELAY)
        chercher = self.selenium.find_element(By.XPATH, '//input[@placeholder="Sélectionnez ou tapez le nom d\'un parcours"]')
        chercher.click()
        chercher.send_keys('par')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Parcours_2")]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn-load-parcours-search').click()
        time.sleep(self.DELAY*5)
        del_icons = self.selenium.find_elements(By.XPATH, '//div[contains(@aria-label, "Supprimer le parcours")]')
        self.assertEqual(2, len(del_icons))
        # Tester le nombre d'icônes
        icons = self.selenium.find_elements(By.CLASS_NAME, "leaflet-marker-icon")
        # self.assertEqual(7, len(icons))
        self.selenium.find_element(By.XPATH, '//div[contains(@id, "icon-delete-parcours--")]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@id="btn-delete-parcours"]').click()
        print('**** test 10 Test de chargement de fichier gpx ****')
        # Charger un fichie gpx
        self.selenium.find_element(By.ID, 'btn-selecteur-add-parcours').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn-open-import-gpx').click()
        time.sleep(self.DELAY)
        fileinput = self.selenium.find_element(By.ID, 'fileinput')
        fileinput.send_keys('/tmp/Parcours_0.gpx')
        self.selenium.find_element(By.XPATH, '//button[@id="btn-gpx-to-geojson"]').click()
        time.sleep(self.DELAY)
        del_icons = self.selenium.find_elements(By.XPATH, '//div[contains(@aria-label, "Supprimer le parcours")]')
        self.assertEqual(2, len(del_icons))
        # self.selenium.find_element(By.XPATH, '//div[contains(@id, "icon-delete-parcours--")]').click()
        # time.sleep(self.DELAY)
        # self.selenium.find_element(By.XPATH, '//button[@id="btn-delete-parcours"]').click()
        print('**** test 11 Test de fusion de parcours ****')
        # Fusionner deux parcours
        # Charger le parcours 2
        self.selenium.find_element(By.ID, 'btn-selecteur-add-parcours').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn-open-search-parcours').click()
        time.sleep(self.DELAY)
        chercher = self.selenium.find_element(By.XPATH, '//input[@placeholder="Sélectionnez ou tapez le nom d\'un parcours"]')
        chercher.click()
        chercher.send_keys('par')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Parcours_2")]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn-load-parcours-search').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn-selecteur-add-parcours').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn-open-fusion-parcours').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//input[@placeholder="Choissisez des parcours a fusionner"]').click()
        self.selenium.find_element(By.XPATH, '//li/div/span[contains(text(), "Parcours_2")]').click()
        time.sleep(self.DELAY)

        self.selenium.find_element(By.XPATH, '//input[@placeholder="Choissisez des parcours a fusionner"]').click()
        self.selenium.find_element(By.XPATH, '//li/div/span[contains(text(), "1 - Parcours_0")]').click()
        self.selenium.find_element(By.ID, 'btn-fusion-parcours').click()
        time.sleep(self.DELAY)
        del_icons = self.selenium.find_elements(By.XPATH, '//div[contains(@aria-label, "Supprimer le parcours")]')
        self.assertEqual(4, len(del_icons))
        # Tester le nombre d'icônes
        icons = self.selenium.find_elements(By.CLASS_NAME, "leaflet-marker-icon")
        self.assertEqual(24, len(icons))

        del_icons[0].click()

        self.selenium.find_element(By.XPATH, '//button[@id="btn-delete-parcours"]').click()
        time.sleep(self.DELAY)
        self.deconnexion()
        os.remove("/tmp/Parcours_0.gpx")
