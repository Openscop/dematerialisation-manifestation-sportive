# coding: utf-8
from django.urls import path, re_path, include
from rest_framework import routers
from carto.views.parcours import ParcoursViewSet, ExportParcoursView
from carto.views.print import Impression
from carto.views.historique import Historique
from carto.views.conflit import Conflit
from carto.views.pageAjax import PageAjax


router = routers.DefaultRouter()
router.register(r'parcours', ParcoursViewSet)

app_name = 'carto'
urlpatterns = [

    # Redirections
    re_path('route/print/', Impression.as_view(), name='route_print'),
    # Redirections
    re_path('route/historique/', Historique.as_view(), name='route_historique'),
    # Redirections
    re_path('route/view/', PageAjax.as_view(), name='carto_ajax'),
    # Redirections
    re_path('route/conflit/(?P<pk>\d+)/', Conflit.as_view(), name='conflit'),

    # parcours interne
    re_path('export/(?P<pk>\d+)/', ExportParcoursView.as_view(), name="export_parcours"),

    path('', include(router.urls)),
]
