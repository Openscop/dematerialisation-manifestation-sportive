# Generated by Django 4.0.6 on 2022-10-21 08:04

from django.db import migrations, models
import django.db.models.deletion

from carto.services.migrationHelper import createPoisData, removePoisData


class Migration(migrations.Migration):

    dependencies = [
        ('carto', '0002_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PoiCategorieApi',
            fields=[
                ('id', models.CharField(max_length=100, primary_key=True, verbose_name="Id de la catégorie utilisé par l'éditeur")),
                ('title_i18n', models.CharField(max_length=100, verbose_name='Nom de la catégorie')),
            ],
        ),
        migrations.CreateModel(
            name='PoiApi',
            fields=[
                ('id', models.CharField(max_length=100, primary_key=True)),
                ('type', models.CharField(max_length=100, verbose_name='type de POI')),
                ('def_svg_symbol_id', models.CharField(blank=True, max_length=100, null=True, verbose_name='symbol pour utilisation svg (obsolete)')),
                ('html_code', models.CharField(blank=True, max_length=100, null=True, verbose_name='html code du POI')),
                ('classes', models.CharField(blank=True, max_length=100, null=True, verbose_name='classes du POI')),
                ('i18n', models.CharField(max_length=100, verbose_name='Nom du POI')),
                ('categorie', models.ForeignKey(default='CATICO-1', on_delete=django.db.models.deletion.SET_DEFAULT, related_name='symbols', to='carto.poicategorieapi')),
            ],
        ),
        migrations.RunPython(createPoisData, removePoisData)
    ]
