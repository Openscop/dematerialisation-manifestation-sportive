# class AnalyseCartographie:

def poi_manquant(manif):
    """Renvoie False si aucun POI manquant
        Ou un dictionnaire détaillant l'erreur"""

    poi_secours_manquant, poi_signaleur_manquant = True, True
    for parcour in manif.parcours.all():
        for poi in parcour.pois.all():
            # Test si un POI secours est présent
            if poi.properties['icon']['id'] == "2":
                poi_secours_manquant = False
            if manif.get_type_manif() in ['dcnm', 'dcnmc']:
                # Test si un POI signaleur est présent
                if poi.properties['icon']['id'] == "5":
                    poi_signaleur_manquant = False
            else:
                poi_signaleur_manquant = False

    if manif.get_type_manif() in ['dcnm', 'dcnmc']:
        if poi_signaleur_manquant and poi_secours_manquant:
            return {
                'bloquant': False,
                'code_erreur': 1,
                'type': "Cartographie incomplète",

                'message': "Nous n'avons pas détecté de POI secours et signaleurs avec permis. "
                           " Ces éléments sont requis par la réglementation pour le dépôt de votre dossier."
            }

    if poi_secours_manquant:
        return {
            'bloquant': False,
            'code_erreur': 3,
            'type': "Cartographie incomplète",
            'message': "Nous n'avons pas détecté de POI secours. "
                       "Ces éléments sont requis par la réglementation pour le dépôt de votre dossier."
        }

    elif poi_signaleur_manquant:
        return {
            'bloquant': False,
            'code_erreur': 2,
            'type': "Cartographie incomplète",

            'message': "Nous n'avons pas détecté de POI signaleurs avec permis. "
                       "Ces éléments sont requis par la réglementation pour le dépôt de votre dossier."
        }

    else:
        return False


def ligne_manquante(manif):
    """Renvoie False si un parcours contient au moins une ligne
       Ou un dictionnaire détaillant l'erreur"""

    for parcour in manif.parcours.all():
        if hasattr(parcour.shape, 'line_waypoint') and parcour.shape.line_waypoint:
            return False

        return {
            'bloquant': True,
            'code_erreur': 4,
            'type': "Cartographie incomplète",
            'message': "Nous n'avons pas détecté de ligne au sein de vos parcours. "
                       "Cet élément est requis pour le dépôt de votre dossier."
            }
