# coding=utf-8
from django.apps import AppConfig


class CartoConfig(AppConfig):
    """ Configuration de l'application """

    # Configuration
    name = 'carto'

    def ready(self):
        """ L'application est prête """
        """ Installer les récepteurs de signaux (listeners) """
        pass


default_app_config = 'carto.CartoConfig'
