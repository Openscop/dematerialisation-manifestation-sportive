# coding: utf-8
""" Classes abstraites ajoutant des champs et fonctionnalités aux modèles """
from django.db import models

from core.util.admin import set_admin_info


class DepartementableBase(models.Model):
    """ Ajoute un champ ForeignKey vers un département à un modèle """

    @set_admin_info(admin_order_field='departement', short_description="Département")
    def get_departement(self):
        """ Renvoie le département de l'objet """
        return self.departement

    @set_admin_info(short_description="Instance")
    def get_instance(self):
        """
        Renvoyer l'instance du département attaché

        :returns: l'instance du département attaché, ou instance master si aucun département
        """
        if self.get_departement():
            return self.get_departement().get_instance()
        else:
            from core.models import Instance
            return Instance.objects.get_master()

    def natural_key(self):
        return (self.pk,) + self.departement.natural_key()

    class Meta:
        abstract = True


class ManyPerDepartementMixin(DepartementableBase):
    """ Ajoute un champ ForeignKey vers un département à un modèle """

    departement = models.ForeignKey('administrative_division.departement', verbose_name="département", on_delete=models.CASCADE)

    class Meta:
        abstract = True


class DepartementableMixin(DepartementableBase):
    """ Ajoute un champ ForeignKey vers un département à un modèle """

    departement = models.ForeignKey('administrative_division.departement', null=True, blank=True, verbose_name="département", on_delete=models.SET_NULL)

    class Meta:
        abstract = True


class ManyPerArrondissementMixin(models.Model):
    """ Ajoute un champ OneToOne vers un arrondissement à un modèle """

    arrondissement = models.ForeignKey('administrative_division.arrondissement', verbose_name="Arrondissement",
                                       on_delete=models.CASCADE)

    @set_admin_info(admin_order_field='arrondissement', short_description="Arrondissement")
    def get_arrondissement(self):
        """ Renvoie la commune de l'objet """
        return self.arrondissement

    @set_admin_info(admin_order_field='arrondissement__departement', short_description="Département")
    def get_departement(self):
        """ Renvoie le département de l'arrondissement de l'objet """
        return self.arrondissement.departement

    class Meta:
        abstract = True
