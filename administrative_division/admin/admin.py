# coding: utf-8
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin

from carto.models import CommuneGeo, ArrondissementsGeo, DepartementGeo
from ..forms import CommuneForm, DepartementForm
from ..models import Commune, Arrondissement, Departement


class ZoneInline(admin.StackedInline):
    fields = ["geom"]
    can_delete = False
    verbose_name = "Géographie"
    extra = 0


class CommuneZoneInline(ZoneInline):
    model = CommuneGeo


@admin.register(Commune)
class CommuneAdmin(ExportActionModelAdmin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'name', 'zip_code', 'code', 'arrondissement', "email", 'get_departement', 'get_service_instructeur']
    list_filter = ['arrondissement__departement', 'arrondissement']
    inlines = [CommuneZoneInline]
    search_fields = ['name__unaccent', 'zip_code', 'code', 'arrondissement__name']
    form = CommuneForm

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        # Les utilisateurs équipe ne peuvent voir que leur département
        if not request.user.has_group('_support'):
            queryset = queryset.filter(arrondissement__departement=request.user.get_departement())
        return queryset

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            return ()
        else:
            return ('pk', 'name', 'zip_code', 'arrondissement', "code", 'get_departement', 'get_service_instructeur', "latitude", 'longitude')


class ArrondissementZoneInline(ZoneInline):
    model = ArrondissementsGeo


@admin.register(Arrondissement)
class ArrondissementAdmin(ExportActionModelAdmin):
    """ Configuration admin des départements """

    # Configuration
    list_display = ['pk', 'code', 'name', 'get_departement', 'get_service_instructeur']
    list_filter = ['departement__name']
    inlines = [ArrondissementZoneInline]
    search_fields = ['name__unaccent', 'departement__name']


class DepartementZoneInline(ZoneInline):
    model = DepartementGeo


@admin.register(Departement)
class DepartementAdmin(ExportActionModelAdmin):
    """ Configuration admin des départements """

    # Supprimer tous les boutons "ajouter un departement" dont le bouton vert "plus" à coté du champ departement des pages d'aide
    def has_add_permission(self, request):
        return False

    # Configuration
    list_display = ['pk', 'name']
    search_fields = ['name']
    inlines = [DepartementZoneInline]
    form = DepartementForm
