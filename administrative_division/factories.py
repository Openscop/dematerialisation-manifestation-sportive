# coding: utf-8
import factory
from factory.fuzzy import FuzzyDecimal

from .models import *
from structure.models import ServiceConsulte

class DepartementFactory(factory.django.DjangoModelFactory):
    """ Factory pour les départements """

    name = factory.Sequence(lambda n: "{0}".format(n))
    instance = factory.RelatedFactory('core.factories.InstanceFactory', 'departement')

    @factory.post_generation
    def service(self, create, extracted, **kwargs):
        from structure.factories.service import ServiceConsulteFactory, ServiceCDSRFactory
        if not create:
            return
        # Création des organisations obligatoires
        if not ServiceConsulte.objects.filter(precision_nom_s="Manifestation sportive").exists():
            ServiceConsulteFactory(instance_fk=self.instance, precision_nom_s="Manifestation sportive",
                                   type_service_fk__abreviation_s="Manifestation sportive",
                                   type_service_fk__nom_s="Manifestation sportive",
                                   type_service_fk__categorie_fk__nom_s="Manifestation sportive")
            ServiceConsulteFactory(instance_fk=self.instance, precision_nom_s="Support",
                                   type_service_fk__abreviation_s="Support",
                                   type_service_fk__nom_s="Support",
                                   type_service_fk__categorie_fk__nom_s="_Support")
        # Création des organisations départementales
        cd = ServiceConsulteFactory(departement_m2m=self, instance_fk=self.instance, precision_nom_s="CD",
                                    type_service_fk__abreviation_s="CD", type_service_fk__nom_s="Conseil départemental",
                                    type_service_fk__categorie_fk__nom_s="Conseil départemental",
                                    admin_service_famille_b=True, admin_utilisateurs_subalternes_b=True,
                                    porte_entree_avis_b=True, autorise_acces_consult_organisateur_b=True)
        ServiceConsulteFactory(departement_m2m=self, instance_fk=self.instance, precision_nom_s="CDSUP", service_parent_fk=cd,
                               type_service_fk__abreviation_s="CDSUP", type_service_fk__nom_s="Conseil départemental N+1",
                               type_service_fk__categorie_fk__nom_s="Conseil départemental",
                               admin_service_famille_b=True, admin_utilisateurs_subalternes_b=True,
                               autorise_rendre_avis_b=True, autorise_acces_consult_organisateur_b=True)
        ServiceConsulteFactory(departement_m2m=self, instance_fk=self.instance, precision_nom_s="DDSP",
                               type_service_fk__abreviation_s="DDSP", type_service_fk__nom_s="Direction Départementale de la Sécurité Publique",
                               type_service_fk__categorie_fk__nom_s="Police",
                               admin_service_famille_b=True, admin_utilisateurs_subalternes_b=True,
                               porte_entree_avis_b=True, autorise_rendre_avis_b=True,
                               autorise_acces_consult_organisateur_b=True)
        sdis = ServiceConsulteFactory(departement_m2m=self, instance_fk=self.instance, precision_nom_s="SDIS",
                                      type_service_fk__abreviation_s="SDIS", type_service_fk__nom_s="Service Départemental d'Incendie et de Secours",
                                      type_service_fk__categorie_fk__nom_s="Pompier",
                                      admin_service_famille_b=True, admin_utilisateurs_subalternes_b=True,
                                      porte_entree_avis_b=True, autorise_rendre_avis_b=True,
                                      autorise_acces_consult_organisateur_b=True)
        ServiceConsulteFactory(departement_m2m=self, instance_fk=self.instance, precision_nom_s="CODIS", autorise_acces_consult_organisateur_b=True,
                               type_service_fk__abreviation_s="CODIS", type_service_fk__nom_s="Centre Opérationnel Départemental d'Incendie et de Secours",
                               type_service_fk__categorie_fk__nom_s="Pompier", service_parent_fk=sdis)
        edsr = ServiceConsulteFactory(departement_m2m=self, instance_fk=self.instance, precision_nom_s="EDSR",
                                      type_service_fk__abreviation_s="EDSR", type_service_fk__nom_s="Escadron Départemental de Sécurité Routière",
                                      type_service_fk__categorie_fk__nom_s="Gendarmerie",
                                      admin_service_famille_b=True, admin_utilisateurs_subalternes_b=True,
                                      porte_entree_avis_b=True, autorise_acces_consult_organisateur_b=True)
        ServiceConsulteFactory(departement_m2m=self, instance_fk=self.instance, precision_nom_s="GGD", service_parent_fk=edsr,
                               type_service_fk__abreviation_s="GGD", type_service_fk__nom_s="Groupement de Gendarmerie Départementale",
                               type_service_fk__categorie_fk__nom_s="Gendarmerie",
                               admin_service_famille_b=True, admin_utilisateurs_subalternes_b=True,
                               autorise_rendre_avis_b=True, autorise_acces_consult_organisateur_b=True)
        cdsr = ServiceCDSRFactory(departement_m2m=self, instance_fk=self.instance, precision_nom_s="CDSR",
                                  type_service_fk__abreviation_s="CDSR", type_service_fk__nom_s="Commission Départementale de la Sécurité Routière",
                                  type_service_fk__categorie_fk__nom_s="Sécurité",
                                  admin_service_famille_b=True, admin_utilisateurs_subalternes_b=True,
                                  porte_entree_avis_b=True, autorise_acces_consult_organisateur_b=True)

    class Meta:
        model = Departement


class ArrondissementFactory(factory.django.DjangoModelFactory):
    """ Factory pour les arrondissements """

    departement = factory.SubFactory('administrative_division.factories.DepartementFactory')
    code = factory.Sequence(lambda n: '{0}'.format(n))
    name = factory.Sequence(lambda n: 'Arrondissement{0}'.format(n))
    prefecture = factory.RelatedFactory('structure.factories.service.PrefectureFactory',
                                        factory_related_name='arrondissement_m2m',
                                        instance_fk=factory.SelfAttribute('..departement.instance'))

    class Meta:
        model = Arrondissement


class CommuneFactory(factory.django.DjangoModelFactory):
    """ Factory pour les communes """

    arrondissement = factory.SubFactory('administrative_division.factories.ArrondissementFactory')
    name = factory.Sequence(lambda n: 'Commune{0}'.format(n))
    code = factory.Sequence(lambda n: '{0}'.format(n))
    zip_code = '42300'
    latitude = FuzzyDecimal(low=-90.0, high=90.0)
    longitude = FuzzyDecimal(low=-180.0, high=180.0)
    email = factory.Sequence(lambda n: 'commune{0}@example.com'.format(n))
    mairie = factory.RelatedFactory('structure.factories.service.MairieFactory',
                                    factory_related_name='commune_m2m',
                                    instance_fk=factory.SelfAttribute('..arrondissement.departement.instance'))

    class Meta:
        model = Commune
