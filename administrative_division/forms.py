# coding: utf-8
from django import forms

from administrative_division.models import Commune, Departement, Arrondissement


class CommuneForm(forms.ModelForm):
    """ Formulaire des communes """

    def clean_zip_code(self):
        zip = self.cleaned_data['zip_code']
        if not len(zip) == 5:
            raise forms.ValidationError("Ce champ doit comporter 5 caractères !")
        try:
            int(zip)
        except ValueError:
            raise forms.ValidationError("ce champ ne doit comporter que des chiffres !")
        return zip

    class Meta:
        model = Commune
        fields = '__all__'


class ArrondissementForm(forms.ModelForm):
    """ Formulaire des Arrondissements """

    class Meta:
        model = Arrondissement
        fields = '__all__'


class DepartementForm(forms.ModelForm):
    """ Formulaire des départements """

    class Meta:
        model = Departement
        fields = '__all__'
