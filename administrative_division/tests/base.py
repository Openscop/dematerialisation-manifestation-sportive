# coding: utf-8
from django.test import TestCase

from administrative_division.factories import ArrondissementFactory, CommuneFactory
from administrative_division.factories import DepartementFactory
from administrative_division.models.departement import Departement


class DepartementTests(TestCase):
    """ Tests départements """

    def test_departement_string(self):
        """ Teste que __str__ renvoie bien le nom du département """
        departement = DepartementFactory.build(name='42')
        self.assertEqual(str(departement), departement.get_name_display())

    def test_departement_methods(self):
        """ Tester certaines méthodes des départements """
        departement = Departement(name="departement")
        instance = departement.get_instance()
        self.assertIsNotNone(instance)  # la méthode get_instance ne renvoie jamais None, mais une instance Master dans le pire des cas
        self.assertTrue(instance.is_master())

    def test_bogus_name(self):
        """ Tester un nom erroné de département """
        departement = Departement(name="departement")  # Le nom "departement" n'est pas dans DEPARTEMENT_CHOICES
        self.assertEqual(departement.get_name(), departement.name)  # On le renvoie tel quel dans ce cas de figure
        departement = Departement(name="42")  # Le nom "42" est dans DEPARTEMENT_CHOICES, et correspond à "Loire"
        self.assertNotEqual(departement.get_name(), departement.name)  # Donc get_name() renvoie "Loire". name est "42".


class ArrondissementMethodTests(TestCase):
    """ Tests arrondissements """

    def test_arrondissement_string(self):
        """ Teste que __str__ renvoie bien le nom de l'arrondissement """
        arrondissement = ArrondissementFactory.build(name="Roanne")
        self.assertEqual(str(arrondissement), "Roanne")


class CommuneMethodTests(TestCase):
    """ Tests communes """

    def test_commune_string(self):
        """ Tester que __str__ renvoie bien le nom de la commune """
        commune = CommuneFactory.build(name="Roanne")
        self.assertEqual(str(commune), "Roanne")
