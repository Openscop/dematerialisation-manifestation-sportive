# coding: utf-8
from django.db import models

from administrative_division.mixins.divisions import ManyPerDepartementMixin


class ArrondissementManager(models.Manager):
    """ Queryset des arrondissements """

    # Getter
    def by_departement(self, departement):
        """ Renvoyer les arrondissements pour le département """
        return self.filter(departement=departement)

    def get_by_natural_key(self, code):
        return self.get(code=code)


class Arrondissement(ManyPerDepartementMixin):
    """ Division de niveau 3 (arrondissement, 330+ en France Métropolitaine """

    # Champs
    code = models.CharField("Code", unique=True, max_length=4)
    name = models.CharField("Nom", max_length=255)
    objects = ArrondissementManager()

    # Overrides
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.name

    def natural_key(self):
        return self.code,

    def get_service_instructeur(self):
        """ Renvoyer la préfecture de l'arrondissement """
        from structure.models import ServiceInstructeur
        service = ServiceInstructeur.objects.filter(arrondissement_m2m=self)
        if service.exists():
            return service.first()
        else:
            service = ServiceInstructeur.objects.filter(departement_m2m=self.departement)
            if service.exists():
                return service.first()
            else:
                return None
    get_service_instructeur.short_description = "préfecture"

    # Méta
    class Meta:
        verbose_name = "Arrondissement"
        verbose_name_plural = "Arrondissements"
        ordering = ['name']
        app_label = 'administrative_division'
        default_related_name = 'arrondissements'
