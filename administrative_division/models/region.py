from django.db import models


class Region(models.Model):
    nom = models.CharField(max_length=50)

    # Overrides
    def __str__(self):
        return self.nom
