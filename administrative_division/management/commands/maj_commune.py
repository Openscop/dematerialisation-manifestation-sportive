# coding: utf-8
import csv

from django.core.management.base import BaseCommand

from administrative_division.models import Commune, Arrondissement, Departement
from structure.models import Organisation, ServiceInstructeur
from evaluation_incidence.models import N2kOperateurSite, RnrAdministrateur
from evenements.models import Manif
from instructions.models import Avis


class Command(BaseCommand):
    """ Mise à jour des commune dans la BD """
    args = ''
    help = 'Mise à jour des communes. Tous les problème seront répertorié dans les fichiers erreur_arron.csv et erreur_commune.csv'

    def handle(self, *args, **options):
        dep_sans_instance = Departement.objects.filter(instance__isnull=True)
        name = ['14', '16', '17', '48', '56', '70', '80', '88']
        dep_instance = Departement.objects.filter(name__in=name)
        deps = dep_sans_instance | dep_instance
        deps_liste = [departement.name for departement in deps]
        liste_travail = ((Organisation, 'organisation', 'commune_m2m'),
                         (N2kOperateurSite, 'Opérateur N2k', 'commune'),
                         (RnrAdministrateur, 'Administrateur Rnr', 'commune'),
                         (Manif, 'Ville de départ de manifestation', 'ville_depart'),
                         (Manif, 'Ville de départ interdep de manifestation', 'ville_depart_interdep_mtm'),
                         (Manif, 'Ville traversée de manifestation', 'villes_traversees'),
                         (Avis, 'Service consulté d\'avis', 'service_consulte_fk'),
                         (Avis, 'Service consulté origine d\'avis', 'service_consulte_origine_fk'),
                         (Avis, 'Service consulté historique d\'avis', 'service_consulte_history_m2m'),
                         (Avis, 'Service demandeur d\'avis', 'service_demandeur_fk'))

        print('suppression')
        count = 0
        total_dep = len(deps)
        for dep in deps:
            count +=1
            print(f"{count}/{total_dep}")
            for arron in dep.arrondissements.all():
                for commune in arron.communes.all():
                    if commune.organisation_set.instance_of(ServiceInstructeur).exists():
                        mairie = commune.organisation_set.instance_of(ServiceInstructeur).get()
                    lien = False
                    for travail in liste_travail:
                        liste = None
                        if travail[0] == Avis:
                            liste = travail[0].objects.filter(**{travail[2]: mairie})
                        elif travail[0] == Organisation:
                            liste = travail[0].objects.filter(**{travail[2]: commune}).exclude(pk=mairie.pk)
                        else:
                            liste = travail[0].objects.filter(**{travail[2]: commune})
                        if liste:
                            lien = True
                    if not lien:
                        commune.delete()

                arrondissement = Arrondissement.objects.get(pk=arron.pk)
                if not arrondissement.communes.all():
                    arrondissement.delete()

        arron_csv = open("administrative_division/fixtures/arrondissement2021.csv", newline='')
        commune_csv = open("administrative_division/fixtures/commune2021.csv", newline='')
        arron_reader = csv.DictReader(arron_csv, delimiter=',')
        arron_tab = [row for row in arron_reader if row['DEP'] in deps_liste]
        commune_reader = csv.DictReader(commune_csv, delimiter=',')
        commune_tab = [row for row in commune_reader if row['DEP'] in deps_liste and row['TYPECOM']=="COM"]
        postal_csv = open("administrative_division/fixtures/laposte_hexasmal.csv", newline='')
        postal_reader = csv.DictReader(postal_csv, delimiter=';')
        postal_tab = [row for row in postal_reader]
        erreur_arron = []
        erreur_commune = []
        count = 0
        total_arron = len(arron_tab)
        for arron in arron_tab:
            count += 1
            print(f"{count}/{total_arron}")
            if not Arrondissement.objects.filter(code=arron['ARR']):
                dep = Departement.objects.get(name=arron['DEP'])
                new_arron = Arrondissement(name=arron['LIBELLE'], code=arron['ARR'], departement=dep)
                new_arron.save()
            else:
                old_arron = Arrondissement.objects.get(code=arron['ARR'])
                if not old_arron.name == arron['LIBELLE']:
                    erreur_arron.append([arron['ARR'], 'arrondissement existant'])

        count = 0
        total_comm = len(commune_tab)
        notification_email_json = {"quotidien_mail": True, "conversation_mail": False,
                                   "nouveaute_mail": False, "action_mail": False, "forum_mail": False,
                                   "notification_mail": False, "tracabilite_mail": False}
        for commune in commune_tab:
            count += 1
            print(f"{count}/{total_comm}")
            if not Commune.objects.filter(code=commune['COM']):
                recherche = list(filter(lambda sch: sch['Code_commune_INSEE'] == commune['COM'], postal_tab))
                if len(recherche) == 0:
                    erreur_commune.append([commune['COM'], 'erreur insee'])
                else:
                    arron = Arrondissement.objects.filter(code=commune['ARR'])
                    if not arron:
                        erreur_commune.append([commune['COM'], 'erreur arron'])
                    else:
                        arron = arron.first()
                        comm = Commune(arrondissement=arron, code=commune['COM'], name=commune['LIBELLE'],
                                       zip_code=recherche[0]['Code_postal'])
                        comm.save()
                        mairie_new = ServiceInstructeur(precision_nom_s=comm.name, email_s=comm.email,
                                                        type_service_fk__categorie_fk__nom_s="Mairie",
                                                        type_service_fk__nom_s="Mairie",
                                                        instance_fk=arron.departement.get_instance(),
                                                        admin_service_famille_b=False,
                                                        admin_utilisateurs_subalternes_b=False,
                                                        porte_entree_avis_b=True,
                                                        autorise_rendre_avis_b=True,
                                                        autorise_acces_consult_organisateur_b=True,
                                                        notification_email_json=notification_email_json)
                        mairie_new.save()
            else:
                old_comm = Commune.objects.get(code=commune['COM'])
                if not old_comm.name == commune['LIBELLE']:
                    erreur_commune.append([commune['COM'], 'commune existante'])


        if erreur_arron:
            nom = "erreur_arron.csv"
            with open(nom, 'w') as csvfile:
                writer = csv.writer(csvfile, delimiter=";", quotechar='"')
                row = ["code", "raison"]
                writer.writerow(row)
                for erreur in erreur_arron:
                    writer.writerow(erreur)

        if erreur_commune:
            nom = "erreur_commune.csv"
            with open(nom, 'w') as csvfile:
                writer = csv.writer(csvfile, delimiter=";", quotechar='"')
                row = ["code", "raison"]
                writer.writerow(row)
                for erreur in erreur_commune:
                    writer.writerow(erreur)


        print("finit,")

