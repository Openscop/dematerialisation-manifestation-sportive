# coding: utf-8
from functools import wraps
from asgiref.sync import sync_to_async

from django.shortcuts import render
from django.http import HttpResponseForbidden

from structure.models import Organisation, ServiceConsulte


def require_type_organisation_function(request, type_organisations):
    if request.user.is_anonymous or not request.session['service_pk']:
        return False
    o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
    if o_support in request.user.organisation_m2m.all():
        return True
    organisation = Organisation.objects.get(pk=request.session['service_pk'])
    for type_organisation in type_organisations:
        if type(organisation) == type_organisation:
            return True
    return False


def require_type_organisation_async(type_organisations=[]):
    """ Décorateur require_type_organisation asynchrone """
    def renderer(function):
        @wraps(function)
        async def wrapper(request, *args, **kwargs):
            if await sync_to_async(require_type_organisation_function)(request, type_organisations):
                return await function(request, *args, **kwargs)
            else:
                return await sync_to_async(render)(request, 'core/access_restricted.html', status=403)
        return wrapper
    return renderer


def require_type_organisation(type_organisations=[]):
    """
    Décorateur de protection d'une vue
    Autorise l'accès à la vue pour les types d'agents décrits dans rolename
    :param rolename: une liste de classe dérivant d'organisation autorisée.
    :returns: soit la réponse de la vue décorée, soit une page 403
    :rtype: django.http.response.HttpResponse
    """

    def renderer(function):
        @wraps(function)
        def wrapper(request, *args, **kwargs):
            if require_type_organisation_function(request, type_organisations):
                return function(request, *args, **kwargs)
            else:
                return render(request, 'core/access_restricted.html', status=403)
        return wrapper
    return renderer


def login_requis():
    """
    Décorateur alternatif à login_required qui lui, redirige vers la page de login

    :returns: soit la réponse de la vue décorée, soit une page 401 suivant l'origine de l'appel
    :rtype: django.http.response.HttpResponse
    """

    def renderer(function):
        @wraps(function)
        def wrapper(request, *args, **kwargs):
            user = request.user
            if user.is_authenticated:
                return function(request, *args, **kwargs)
            else:
                if request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest':
                    return HttpResponseForbidden(401)
                else:
                    return render(
                        request, "core/access_restricted.html",
                        {'message': "Veuillez vous authentifier pour accéder à votre information.<br>Pour cela,"
                                    " utilisez le bouton <a href=\"/accounts/login/\" class=\"font-italic\">"
                                    "Connexion</a> en haut à droite de cette page..."}, status=401)
        return wrapper
    return renderer


def login_requis_async():
    """ Décorateur login_requis asynchrone """
    def renderer(function):
        @wraps(function)
        async def wrapper(request, *args, **kwargs):
            user = request.user
            if user.is_authenticated:
                return await function(request, *args, **kwargs)
            else:
                if request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest':
                    return HttpResponseForbidden(401)
                else:
                    return await render(
                        request, "core/access_restricted.html",
                        {'message': "Veuillez vous authentifier pour accéder à votre information.<br>Pour cela,"
                                    " utilisez le bouton <a href=\"/accounts/login/\" class=\"font-italic\">"
                                    "Connexion</a> en haut à droite de cette page..."}, status=401)
        return wrapper
    return renderer
