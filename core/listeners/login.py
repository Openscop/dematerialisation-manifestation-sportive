# coding: utf-8
from django.conf import settings
from django.core.mail import send_mail
from django.dispatch.dispatcher import receiver

from django.contrib.auth.signals import user_logged_in, user_logged_out
from allauth.account.signals import email_confirmed

from core.models.user import User
from messagerie.models import CptMsg


@receiver(user_logged_in, sender=User)
def login_actions(sender, request, user, **kwargs):
    """ Traiter la connexion d'un utilisateur """
    # Si l'utilisateur se connecte, ce n'est plus la peine de le marquer comme ayant changé de login
    from messagerie.models import Message
    Message.objects.creer_et_envoyer('tracabilite', None, [user], 'Connexion à la plateforme', 'Connexion à la plateforme')
    cpt = CptMsg.objects.get(utilisateur=user)
    cpt.update_cpt()
    if user.status == User.USERNAME_CHANGED:
        user.status = User.NORMAL
        user.save()


@receiver(user_logged_out, sender=User)
def logout_actions(sender, request, user, **kwargs):
    # envoyer un msg de traçabilité à al deco
    from messagerie.models import Message
    Message.objects.creer_et_envoyer('tracabilite', None, [user], 'Déconnexion de la plateforme', 'Déconnexion de la plateforme')


@receiver(email_confirmed)
def email_confirmed_(request, email_address, **kwargs):
    user = email_address.user
    if not user.is_active:
        url = '/profile/' + str(user.pk) + '/'
        address = request.build_absolute_uri(url)
        sender_email = user.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
        adminstancelist = User.objects.get_email_admin_instance(user.default_instance)
        # Création de compte : get_service() retournera le bon service
        message_debut = f"Un nouveau compte a été créé avec une adresse mail valide : {user.username} {chr(10)}"
        message_fin = f"Pour vérifier et valider ce compte, rendez-vous à l'adresse : {chr(10)}{address}"
        if user.get_service().invitations.filter(completee_b=True, email_s=user.email).exists():
            invitation = user.get_service().invitations.get(completee_b=True, email_s=user.email)
            message_debut += f"grace à une invitation de {invitation.origine_fk} {chr(10)}"
        if adminstancelist:
            send_mail(subject='[Manifestation Sportive] Création de compte',
                      message=message_debut +
                              f"Ce compte appartient au service {user.get_service()} {chr(10)}" +
                              message_fin,
                      from_email=sender_email,
                      recipient_list=list(adminstancelist))
        else:
            techlist = User.objects.filter(groups__name__icontains='_support').values_list('email', flat=True)
            send_mail(subject='[Manifestation Sportive] Creation de compte',
                      message=message_debut +
                              "Il n'y a pas d'administrateur d'instance pour valider ce compte !" + chr(10) +
                              message_fin,
                      from_email=sender_email,
                      recipient_list=list(techlist))
