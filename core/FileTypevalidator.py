from django.core.exceptions import ValidationError
from django.conf import settings
import os
import magic

"""
 Fonction pour verifier les types de fichiers envoyer dans la plateforme.
 Le premier est un validateur via form à mettre dans le model du fichier
 Le second étant un validateur quand on utilise pas de formulaire
"""

VALID_EXTENSIONS = ['bmp', 'csv', 'doc', 'docx', 'dotx', 'eml', 'gbd', 'gif', 'jpeg', 'jpg', 'kml',
                        'kmz', 'msg', 'odg', 'odp', 'ods', 'odt', 'ott', 'pdf', 'png', 'pps', 'ppsm', 'ppsx', 'ppt',
                        'pptm', 'pptx', 'rtf', 'tiff', 'trk', 'txt', 'webp', 'xls', 'xlsm', 'xlsx', 'xml',]


CORRESPONDANCE = [['bmp', ['image/x-ms-bmp', 'image/bmp']],
                  ['csv', ["text/csv"]],
                  ['doc', ['application/msword']],
                  ['docx', ['application/vnd.openxmlformats-officedocument.wordprocessingml.document']],
                  ['dotx', ['application/vnd.openxmlformats-officedocument.wordprocessingml.template']],
                  ['eml', ['application/elm+xml', 'application/elm+json', 'message/rfc822']],
                  ['gbd', ['application/gbd']],
                  ['gif', ['image/gif']],
                  ['jpg', ['image/jpeg', 'image/x-citrix-jpeg']],
                  ['jpeg', ['image/jpeg', 'image/x-citrix-jpeg']],
                  ['jpg', ['image/jpeg', 'image/x-citrix-jpeg']],
                  ['kml', ['application/vnd.google-earth.kml+xml']],
                  ['kmz', ['application/vnd.google-earth.kmz']],
                  ['msg', ['application/vnd.ms-outlook']],
                  ['odg', ['application/vnd.oasis.opendocument.graphics']],
                  ['odp', ['application/vnd.oasis.opendocument.presentation']],
                  ['ods', ['application/vnd.oasis.opendocument.spreadsheet']],
                  ['odt', ['application/vnd.oasis.opendocument.text']],
                  ['ott', ['application/vnd.oasis.opendocument.text-template']],
                  ['pdf', ['application/pdf']],
                  ['png', ['image/png', 'image/x-citrix-png', 'image/x-png']],
                  ['pps', ['application/vnd.ms-powerpoint']],
                  ['ppsm', ['application/vnd.ms-powerpoint.slideshow.macroenabled.12']],
                  ['ppsx', ['application/vnd.openxmlformats-officedocument.presentationml.slideshow']],
                  ['ppt', ['application/vnd.ms-powerpoint']],
                  ['pptm', ['application/vnd.ms-powerpoint.presentation.macroenabled.12']],
                  ['pptx', ['application/vnd.openxmlformats-officedocument.presentationml.presentation']],
                  ['rtf', ['application/rtf', 'text.rtf']],
                  ['tiff', ['image/tiff']],
                  ['trk', ['application/tkt']],
                  ['txt', ['text/plain']],
                  ['webp', ['image/webp']],
                  ['xls', ['application/vnd.ms-excel']],
                  ['xlsm', ['application/vnd.ms-excel.sheet.macroenabled.12']],
                  ['xlsx', ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']],
                  ['xml', ['application/xml']]]


def file_type_validator(value):
    ext = value.name.split('.')[-1]
    if settings.DEBUG:
        # En dev, pas d'obligation d'avoir tous les fichiers médias présents
        mime = 'image/x-ms-bmp'
    else:
        mime = magic.from_buffer(value.read(2048), mime=True)  # 2048 pour tout fichier office > 2013
    ok = True
    if not ext.lower() in VALID_EXTENSIONS:  # verification que l'extension est valide d'abord
        ok = False
    if ok and not settings.DEBUG:
        # ensuite verification que l'extension correspond au mime
        r = list(filter(lambda x: x[0] == ext.lower(), CORRESPONDANCE))
        if not r or mime not in r[0][1]:
            ok = False
    if not ok:
        # ici on verifie si le fichier existe dans le server, dans ce cas où c'est un fichier déposé avant la création du validateur
        if not hasattr(value, 'path') or not os.path.isfile(value.path):
            raise ValidationError(u'Ce type de fichier n\'est pas pris en charge')


def file_type_valide_non_form(value):
    ext = value.name.split('.')[-1]
    mime = magic.from_buffer(value.read(2048), mime=True)  # 2048 pour tout fichier office > 2013
    ok = True
    if not ext.lower() in VALID_EXTENSIONS:  # verification que l'extension est valide d'abord
        ok = False
    if ok:  # ensuite verification que l'extension correspond au mime
        r = list(filter(lambda x: x[0] == ext.lower(), CORRESPONDANCE))
        if not r or mime not in r[0][1]:
            ok = False
    if not ok:
        # ici on verifie si le fichier existe dans le server, dans ce cas où c'est un fichier déposé avant la création du validateur
        if not hasattr(value, 'path') or not os.path.isfile(value.path):
            return False
    return True
