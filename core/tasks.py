from __future__ import absolute_import, unicode_literals

import os, sys, subprocess
import shutil

from io import StringIO
from pdfid.pdfid import PDFiD

from django.shortcuts import get_object_or_404
from django.conf import settings

from configuration.celery import app
from evenements.templatetags.thumbnail_filter import createthumbnail


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self
    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio    # free up some memory
        sys.stdout = self._stdout


def scan_antivirus(manif=None, path_prefix=None, post=None, path_predefinit=None):
    if manif:
        departement_number = manif.get_instance().get_departement_name()
        path = settings.MEDIA_ROOT + departement_number + "/" + str(manif.pk)
    elif path_predefinit:
        path = path_predefinit.replace(path_predefinit.split("/")[-1], "")
    else:
        path = settings.MEDIA_ROOT + settings.MACHINA_ATTACHMENT_FILE_UPLOAD_TO + "/" + str(post.pk)
        print(path)
    if path_prefix:
        path += "/" + path_prefix
    result = subprocess.run(["clamscan", "--infected", path], capture_output=True)

    if result.returncode == 0:
        return []
    list_str = result.stdout.decode('utf-8').split('----------- SCAN SUMMARY -----------')[0]
    tableau_positif = list_str.split('FOUND')
    tableau_path = []
    [tableau_path.append(positif.split(':')[0]) for positif in tableau_positif]
    [print(a) for a in tableau_path]
    return tableau_path


def scan_pdf(file):
    with Capturing() as output:
        PDFiD(file, disarm=True)
    print(output)
    detect = False
    if output and "/JS -> /js" in output:
        detect = True
        try:
            shutil.move(file, f"{file}.exclude_by_pdfid")
            shutil.move(f"{file.split('.pdf')[0]}.disarmed.pdf", file)
        except:
            if not os.path.exists(file):
                shutil.move(f"{file}.exclude_by_pdfid", file)
    else:
        try:
            os.remove(f"{file.split('.pdf')[0]}.disarmed.pdf")
        except:
            pass
    return detect


@app.task(bind=True, queue="default")
def set_instructeur(self, instance_pk):
    from core.models import Instance
    from instructions.models import Instruction
    import time
    time.sleep(2)
    instance = Instance.objects.get(pk=instance_pk)
    for i in Instruction.objects.filter(instance=instance):
        i.set_service_instructeurs()


@app.task(bind=True, queue="fichier")
def pj_forum(self, post_pk, pj_pk):
    from forum.forum_conversation.models import Post
    from messagerie.models import Message
    post = get_object_or_404(Post, pk=post_pk)
    for attachment in post.attachments.all():
        if attachment.file.name.split('.')[-1] in ['pdf', "PDF"]:
            file = settings.MEDIA_ROOT + attachment.file.name
            nom_pj = attachment.file.name
            result = scan_pdf(file)
            if result:
                contenu = "<p>Bonjour <br>" \
                          " Le pdf " + nom_pj.split('/')[-1] + " a été traité afin d'en exclure du code informatique non conforme."
                Message.objects.creer_et_envoyer('action', None, [post.poster],
                                                 "PDF invalide",
                                                 contenu)
    a = scan_antivirus(post=post)
    print(a)
    for file in a:
        print('log')
        print(file)
        if file and settings.MEDIA_ROOT in file:
            nom = file.split(settings.MEDIA_ROOT)[1]
            attachements = post.attachments.filter(file=nom)
            attachements.delete()
            os.remove(file) if os.path.exists(file) else ""
            os.system(f"touch {file}.clamav")
            contenu = f"<p>Bonjour <br>" \
                      f"votre dernier post forum contenait une pièce jointe {nom.split('/')[-1]}, contenant un virus. <br>" + \
                      f"La pièce jointe a donc été supprimée de nos serveurs.</p>"
            Message.objects.creer_et_envoyer('forum', None, [post.poster],
                                             "Pièce jointe de message infectée", contenu)
    for attachment in post.attachments.all():
        attachment.scan_antivirus = True
        attachment.save()
        if attachment.file:
            createthumbnail(attachment.file)


@app.task(bind=True, queue="fichier")
def pj_messagerie(self, pk_manif, pk_msg, cdsr=False):
    from messagerie.models import Message
    from evenements.models import Manif
    from core.models import User
    message = Message.objects.get(pk=pk_msg)
    if pk_manif:
        manif = Manif.objects.get(pk=pk_manif)
    else:
        manif = None
    if message.fichier:
        if message.fichier.name.split('.')[-1] in ['pdf', "PDF"]:
            file = settings.MEDIA_ROOT + message.fichier.name
            nom_pj = message.fichier.name
            result = scan_pdf(file)
            if result:
                contenu = "<p>Bonjour <br>" \
                          " Le pdf " + nom_pj.split('/')[
                              -1] + " a été traité afin d'en exclure du code informatique non conforme."
                Message.objects.creer_et_envoyer('action', None, [message.message_enveloppe.first().expediteur_id],
                                                 "PDF invalide",
                                                 contenu, manifestation_liee=manif, reponse_a_pk=message.message_enveloppe.first().pk)
        if cdsr:
            a = scan_antivirus(None, path_predefinit=settings.MEDIA_ROOT + message.fichier.name)
        else:
            a = scan_antivirus(manif, path_prefix="pj_msg")
        print(a)
        for file in a:
            print('log')
            print(file)
            if file and settings.MEDIA_ROOT in file:
                url = file.split(settings.MEDIA_ROOT)[1]
                try:
                    message = Message.objects.get(fichier=url)
                    message.fichier = ""
                    message.save()
                except:
                    pass
                os.remove(file) if os.path.exists(file) else ""
                os.system(f"touch {file}.clamav")
                contenu = "<p>Bonjour <br>" \
                          "Le message ci-dessus contenait un virus. <br>" + \
                          "La pièce jointe a donc été supprimée de nos serveurs.</p>"
                Message.objects.creer_et_envoyer('action', None, [message.message_enveloppe.first().expediteur_id],
                                                 "Pièce jointe de message infectée",
                                                 contenu, manifestation_liee=manif, reponse_a_pk=message.message_enveloppe.first().pk)
                # message admin instance
                admin = User.objects.filter(default_instance=manif.get_instance()).filter(
                    groups__name="Administrateurs d'instance")
                contenu = "<p>Bonjour, <br>" \
                          "L'utilisateur : " + message.message_enveloppe.first().expediteur_id.username + \
                          " a tenté d'envoyer un message avec une pièce jointe contenant un virus. <br>" + \
                          "La pièce jointe a été supprimée de nos serveurs.</p>"
                Message.objects.creer_et_envoyer('tracabilite', None, admin, "Pièce jointe infectée", contenu,
                                                 manifestation_liee=manif)
    message.scan_antivirus = True
    message.save()
    if message.fichier:
        createthumbnail(message.fichier)


@app.task(bind=True, queue="fichier")
def gestion_fichier(self, pk_manif, pk_pj):
    from evenements.models import Manif
    from messagerie.models import Message
    from core.models import User
    manif = Manif.objects.get(pk=pk_manif)
    piece = manif.piece_jointe.get(pk=pk_pj)
    if piece.document_attache.name.split('.')[-1] in ['pdf', "PDF"]:
        file = settings.MEDIA_ROOT+piece.document_attache.name
        nom_pj = piece.document_attache.name
        result = scan_pdf(file)
        if result:
            # message organisateur
            contenu = "<p>Bonjour <br>" \
                      " Le pdf " + nom_pj.split('/')[-1] + " a été traité afin d'en exclure du code informatique non conforme."
            Message.objects.creer_et_envoyer('tracabilite', None, manif.structure_organisatrice_fk.get_utilisateurs_avec_service(),
                                             "PDF retraité",
                                             contenu, manifestation_liee=manif)
    if not piece.scan_antivirus:
        for file in scan_antivirus(manif):
            if file and settings.MEDIA_ROOT in file:
                url = file.split(settings.MEDIA_ROOT)[1]
                nom_pj = url.split('/')[-1]
                try:
                    pj = manif.piece_jointe.get(document_attache=url)
                    print(url + ' trouvée')
                    pj.document_attache = ""
                    pj.date_televersement = None
                    pj.scan_antivirus = False
                    pj.save()
                except:
                    pass
                os.remove(file) if os.path.exists(file) else ""
                os.system(f"touch {file}.clamav")
                # message organisateur
                contenu = "<p>Bonjour <br>" \
                          " La pièce jointe " + nom_pj + " contenue dans votre dossier contient un virus <br>" + \
                          "Elle a donc été supprimée de nos serveurs. <br>" + \
                          "Nous vous conseillons vivement de vérifier l'intégralité de votre ordinateur, puis renvoyer un nouveau fichier sain.</p>"
                Message.objects.creer_et_envoyer('action', None, manif.structure_organisatrice_fk.get_utilisateurs_avec_service(),
                                                 "Pièce jointe infectée", contenu, manifestation_liee=manif)
                # message admin instance
                admin = User.objects.filter(default_instance=manif.get_instance()).filter(groups__name="Administrateurs d'instance")
                contenu = "<p>Bonjour, <br>" \
                          "L'utilisateur : " + manif.structure_organisatrice_fk.get_users_list()[0].username + \
                          " a tenté d'envoyer une pièce jointe contenant un virus. <br>" + \
                          "Son nom était : " + nom_pj + "<br>" + \
                          "La pièce jointe a été supprimée de nos serveurs.</p>"
                Message.objects.creer_et_envoyer('tracabilite', None, admin, "Pièce jointe infectée", contenu, manifestation_liee=manif)

    piece.scan_antivirus = True
    piece.save()
    # creation thumbnail
    if piece.document_attache:
        createthumbnail(piece.document_attache)


@app.task(bind=True, queue="fichier")
def creation_thumbnail_doc_officiel(self, pk):
    from instructions.models import DocumentOfficiel
    from messagerie.models import Message
    pj = DocumentOfficiel.objects.get(pk=pk)
    if pj.scan_antivirus:
        return ""
    alerte = False
    referent = pj.instruction.referent
    manif = pj.instruction.manif
    file = settings.MEDIA_ROOT+pj.fichier.name
    if file.split('.')[-1] in ['pdf', "PDF"]:
        result = scan_pdf(file)
        if result:
            contenu = "<p>Bonjour <br>" \
                      " Le pdf " + file.split('/')[-1] + " a été traité afin d'en exclure du code informatique non conforme."
            Message.objects.creer_et_envoyer('action', None, [referent, pj.instruction.get_prefecture_concernee()], "PDF invalide",
                                             contenu, manifestation_liee=manif)
    a = scan_antivirus(manif=manif, path_prefix='officiels')
    print(a)
    for file in a:
        print(file)
        if file and settings.MEDIA_ROOT in file:
            alerte = True
            url = file.split(settings.MEDIA_ROOT)[1]
            doc_offi = DocumentOfficiel.objects.get(fichier=url)
            doc_offi.fichier = ""
            doc_offi.scan_antivirus = True
            doc_offi.save()
            os.remove(file) if os.path.exists(file) else ""
            os.system(f"touch {file}.clamav")
            contenu = "<p>Bonjour <br>" \
                      f"Le document officiel concernant la manif {str(manif)} contenait un virus. <br>" + \
                      "Le document officiel a donc été supprimée de nos serveurs.<br>" + \
                      "Veuillez redéposer un nouveau document officiel.</p>"
            Message.objects.creer_et_envoyer('action', None, [referent, pj.instruction.get_prefecture_concernee()],
                                             "Document officiel infecté",
                                             contenu, manifestation_liee=manif)
    if not alerte:
        pj.scan_antivirus = True
        pj.save()
        if pj.fichier:
            createthumbnail(pj.fichier)


@app.task(bind=True, queue="fichier")
def creation_thumbnail_pj_avis(self, pk):
    from instructions.models import PieceJointeAvis
    from messagerie.models import Message
    pj = PieceJointeAvis.objects.get(pk=pk)
    if pj.scan_antivirus:
        return ""
    instruction = None
    agents = []
    if pj.avis:
        instruction = pj.avis.instruction
        agents = pj.avis.get_agents_avec_service()
    alerte = False
    file = settings.MEDIA_ROOT+pj.fichier.name
    if file.split('.')[-1] in ['pdf', "PDF"]:
        result = scan_pdf(file)
        if result:
            contenu = "<p>Bonjour <br>" \
                      " Le pdf " + file.split('/')[-1] + " a été traité afin d'en exclure du code informatique non conforme."
            Message.objects.creer_et_envoyer('action', None, agents, "PDF invalide", contenu,
                                             manifestation_liee=instruction.manif)
    a = scan_antivirus(manif=instruction.manif, path_prefix='avis')
    print(a)
    for file in a:
        print(file)
        if file and settings.MEDIA_ROOT in file:
            alerte = True
            url = file.split(settings.MEDIA_ROOT)[1]
            PieceJointeAvis.objects.filter(fichier=url).delete()
            os.remove(file) if os.path.exists(file) else ""
            os.system(f"touch {file}.clamav")
            contenu = "<p>Bonjour <br>" \
                      f"La pièce jointe attaché à l'avis concernant la manif {str(instruction.manif)} contenait un virus. <br>" + \
                      "La pièce jointe a donc été supprimée de nos serveurs.</p>"
            Message.objects.creer_et_envoyer('action', None, agents,
                                             "Pièce jointe d'avis infectée",
                                             contenu, manifestation_liee=instruction.manif)
    if not alerte:
        pj.scan_antivirus = True
        pj.save()
        if pj.fichier:
            createthumbnail(pj.fichier)


@app.task(bind=True, queue="fichier")
def export_multiserver(self, pk_user, pk_organisation,  pk_manif, type=None, zip=None, avis_demande=None):
    exportation_type = type
    zip = True if zip else False
    import asyncio
    from core.models import User
    from evenements.models import Manif
    from instructions.models import Avis
    from evenements.views.exportation import Exportdossier
    from structure.models import Organisation

    user = User.objects.get(pk=pk_user)
    organisation = Organisation.objects.get(pk=pk_organisation)

    if exportation_type == 'organisateur':
        pk = pk_manif
        return asyncio.run(Exportdossier.export(user, pk, preferelezip=zip))

    elif exportation_type == 'instructeur':
        manif = Manif.objects.get(pk=pk_manif)
        pk = manif.pk
        instruction = manif.get_instruction(instance=organisation.instance_fk)
        avis = Avis.objects.filter(instruction=instruction, avis_parent_fk__isnull=True) if avis_demande else None
        return asyncio.run(Exportdossier.export(user=user, pk=pk, avis=avis, preferelezip=zip))

    elif exportation_type == 'agent':
        manif = Manif.objects.get(instruction__avis__pk=pk_manif)
        pk = manif.pk
        avis = Avis.objects.filter(pk=pk_manif) if avis_demande else None
        preavis = Avis.objects.filter(avis_parent_fk=avis.first()) if avis_demande else None
        return asyncio.run(Exportdossier.export(user, pk, avis, preavis, zip))

    elif exportation_type == 'agentlocal':
        manif = Manif.objects.get(instruction__avis__preavis__pk=pk_manif)
        pk = manif.pk
        preavis = Avis.objects.filter(pk=pk_manif) if avis_demande else None
        return asyncio.run(Exportdossier.export(user, pk, None, preavis, zip))
