# coding: utf-8
import factory
from factory.django import DjangoModelFactory

from oauth2_provider.models import Application

from administrative_division.factories import DepartementFactory
from core.models.instance import Instance


class InstanceFactory(DjangoModelFactory):
    """ Factory des instances normales """

    # Champs
    name = factory.Sequence(lambda n: "Configuration GGD Sub-EDSR {0}".format(n))
    departement = factory.SubFactory(DepartementFactory)

    # Meta
    class Meta:
        model = Instance


class MasterInstanceFactory(DjangoModelFactory):
    """ Factory des instances normales """

    # Champs
    departement = None
    name = "Configuration master"

    # Meta
    class Meta:
        model = Instance


class InstanceAFactory(DjangoModelFactory):
    """ Factory d'instance avec paramètres spécifiques """

    # Champs
    name = "Configuration GGD Sub-EDSR A"

    # Meta
    class Meta:
        model = Instance


class ApplicationFactory(DjangoModelFactory):

    # Meta
    class Meta:
        model = Application
