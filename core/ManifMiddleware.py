import re
from django.utils import timezone
from django.shortcuts import redirect, reverse
from django.contrib import messages
from django.http import HttpResponse
from django.contrib.auth import logout
from django.conf import settings

from evenements.models import Manif
from instructions.models import Instruction, Avis
from structure.models import ServiceConsulte
from structure.models.organisation import Organisation
from oauth2_provider.models import AccessToken


class ManifMiddleware:
    """
    Middleware servant à enregistrer l'heure de connexion et le dossier consulté
    pour calculer le nombre d'utilisateurs connectés et le nombre d'utilisateurs sur un dossier
    Il sert aussi de verification d'acces via l'ip des superuser
    Il enregistre aussi tout changement de service de l'utilisateur et le stock en session
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        #versionning
        request.version = settings.VERSION
        user = request.user
        request.session['service_pk'] = request.session.get('service_pk')
        organisation = Organisation.objects.get(pk=request.session.get('service_pk')) if request.session.get('service_pk') else None
        authapi = False
        if not user.is_authenticated and request.META.get('HTTP_AUTHORIZATION'):
            # ici nul besoin de verifier l'authentification vient auto qui sera faite plus tard on enregistre juste l'utilisateur pour avoir son service
            tokens = AccessToken.objects.filter(token=request.META.get('HTTP_AUTHORIZATION').replace('Bearer ', ""))
            if tokens:
                token = tokens.last()  # seul le dernier token nous interesse
                user = token.user
                authapi = True
        if user.is_authenticated or authapi:
            # Verification des ip superuser et admin d'instance
            if user.is_superuser:
                x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
                if x_forwarded_for:
                    ip = x_forwarded_for.split(',')[0]
                else:
                    ip = request.META.get('REMOTE_ADDR')
                tableau_universel = settings.ADMIN_INSTANCE_IP
                if ip not in tableau_universel:
                    logout(request)
                    messages.error(request, 'Vous avez été déconnecté automatiquement ! Cette adresse ip n\'est pas autorisée.')
                    return redirect(reverse('account_login'))
            # Enregistrement de l'heure de connexion
            user.last_visit = timezone.now()
            # Enregistrement du dossier consulté
            url = request.path.split('/')
            pk_manif = 0
            # Urls de l'appli "instructions"
            if url[1] == 'instructions':
                # pk en seconde position => instruction
                if url[2].isdecimal():
                    # Vérifier l'objet
                    if Instruction.objects.filter(pk=url[2]).exists():
                        pk_manif = Instruction.objects.get(pk=url[2]).manif.pk
                # Avis demandé et pk en troisième position
                elif url[2] == 'avis'and url[3].isdecimal():
                    # Cas particulier : transfert de pièce jointe de préavis sur un avis
                    if url[4] and request.GET == 'addfile':
                        # Vérifier l'objet
                        if Avis.objects.filter(pk=url[3]).exists():
                            pk_manif = Avis.objects.get(pk=url[3]).instruction.manif.pk
                    else:
                        # Vérifier l'objet
                        if Avis.objects.filter(pk=url[3]).exists():
                            pk_manif = Avis.objects.get(pk=url[3]).instruction.manif.pk
                elif url[2] == 'add' and url[3].isdecimal():
                    if Manif.objects.filter(pk=url[3]).exists():
                        pk_manif = url[3]
                # Récuperer le servcice de l'utiliseur en session
                elif url[2] == 'tableaudebord' and url[3].isdecimal():
                    service = Organisation.objects.get(pk=url[3])
                    request.session['service_pk'] = service.pk
            elif url[1] == "tableau-de-bord-organisateur":
                if url[2] and url[2].isdecimal():
                    service = Organisation.objects.get(pk=url[2])
                    request.session['service_pk'] = service.pk
                else:
                    request.session['service_pk'] = user.organisation_m2m.first().pk

            # Urls de l'appli "evenements" limité aux accès cerfas (Dnm, Dcnm, Avtm ...)
            elif re.match(r'^[A,D]\w+', url[1]):
                if len(url) > 2 and url[2].isdecimal():
                    manif = Manif.objects.filter(pk=url[2])
                    pk_manif = manif.first().pk if manif else None
            if pk_manif:
                user.last_manif = pk_manif
            user.save()
            if pk_manif:
                manif = Manif.objects.get(pk=pk_manif)
                service = manif.has_access(user, organisation_origine=organisation)
                if service:
                    request.session['service_pk'] = service.pk
            if user.organisation_m2m.first() and not request.session['service_pk']:
                request.session['service_pk'] = user.organisation_m2m.first().pk
            o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
            request.session['o_support'] = True if o_support in user.organisation_m2m.all() else False
            request.organisation = Organisation.objects.get(pk=request.session['service_pk'])
        else:
            # Cas d'accès aux urls quand un utilisateur a été déconnecté par la plateforme
            url = request.path.split('/')
            if url[1] == 'accounts':
                # Pour l'url de changement de password
                if not (len(url) > 2 and url[2] in ['login', 'confirm-email', 'email_confirmed', 'inactive']) and not (len(url) > 2 and url[2] == 'password' and len(url) > 3 and url[3] == "reset"):
                    messages.error(request, 'Vous avez été déconnecté automatiquement ! Veuillez vous reconnecter.')
                    return redirect(reverse('account_login'))
            if 'HTTP_X_REQUESTED_WITH' in request.META and request.META['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest':
                # Pour les appels Ajax
                if url[1] in ['messagerie', 'instructions']:
                    messages.error(request, 'Vous avez été déconnecté automatiquement ! Veuillez vous reconnecter.')
                    return HttpResponse(status=401)
        request.session.save()
        return self.get_response(request)
