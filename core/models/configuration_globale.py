from django.db import models


class ConfigurationGlobale(models.Model):
    """
    Class pour configurer des choses globalement notamment les crons
    """

    # cron
    UpdateCPT = models.BooleanField(default=True, verbose_name="UpdateCPT | Mise à jour des compteurs de messagerie")
    DeleteFichierExportation = models.BooleanField(default=True, verbose_name="DeleteFichierExportation | Suppression des fichiers temporaires d'exportation au bout d'une heure")
    DeleteFlagFile = models.BooleanField(default=True, verbose_name="DeleteFlagFile | Suppression des fichiers flag de postoffice")
    DeleteExportationFile = models.BooleanField(default=True, verbose_name="DeleteExportationFile | Suppresion des fichiers finaux d'exportations")
    RelanceScanAntivirus = models.BooleanField(default=True, verbose_name="RelanceScanAntivirus | Relance des scan antivirus des fichiers qui ont échoués")
    TraitementDossier = models.BooleanField(default=True, verbose_name="TraitementDossier | Relance des instructions en retard")
    UpdateInvitation = models.BooleanField(default=True, verbose_name="UpdateInvitation | Mise à jour des invitations notemment leur expiration")
    RelanceReunionCDSR = models.BooleanField(default=True, verbose_name="RelanceReunionCDSR | Relance des réunions CDSR")
    EmailAdresseValide = models.BooleanField(default=True, verbose_name="EmailAdresseValide | Relance des adresse email non validées")
    UpdateDelai = models.BooleanField(default=True, verbose_name="UpdateDelai | Mise à jour du delai des manifestations")
    CompletudeDossier = models.BooleanField(default=True, verbose_name="CompletudeDossier | Relance d'un dossier incomplet")
    RelanceAvis = models.BooleanField(default=True, verbose_name="RelanceAvis | Relance des avis non traités")
    RelanceAction = models.BooleanField(default=True, verbose_name="RelanceAction | Relance des actions non  lues")
    SendRecap = models.BooleanField(default=True, verbose_name="SendRecap | Envoi du mail récapitulatif")
    ErreurSMTPEmail = models.BooleanField(default=True, verbose_name="ErreurSMTPEmail | Gestion des erreurs d'envoi SMTP de postoffice")
    NotificationPbEmail = models.BooleanField(default=True, verbose_name="NotificationPbEmail | Gestion des retours de mail en erreur")
    RenvoyerConfirmationMail = models.BooleanField(default=True, verbose_name="RenvoyerConfirmationMail | Renvoi des mail de confirmation ont échoués")
    UpdateStat = models.BooleanField(default=True, verbose_name="UpdateStat | Mise à jour des indicateurs en page d'accueil")

    # config mail
    mail_conversation = models.BooleanField(default=True, verbose_name="Conversation")
    mail_action = models.BooleanField(default=False, verbose_name="Action")
    mail_suivi = models.BooleanField(default=False, verbose_name="Info suivi")
    mail_tracabilite = models.BooleanField(default=False, verbose_name="Traçabilité")
    mail_actualite = models.BooleanField(default=False, verbose_name="Actualité")
    mail_forum = models.BooleanField(default=True, verbose_name="Forum")

    # envoi recap
    recap_nbre = models.IntegerField(default=25, verbose_name="Nombre de mail envoyé à chaque envoi")
    recap_ecart = models.IntegerField(default=5, verbose_name="Ecart entre deux envoi de mail")

    # Statistiques
    stat_cache_nb_instruction = models.IntegerField(default=8012, verbose_name="Nombre d'instruction")
    stat_cache_nb_participant = models.IntegerField(default=4171256, verbose_name="Nombre de participants")
    stat_cache_nb_feuille_eco = models.IntegerField(default=793946, verbose_name="Nombre de feuille économisées")
    stat_cache_nb_km_parcouru = models.IntegerField(default=677448, verbose_name="Km de parcours")
