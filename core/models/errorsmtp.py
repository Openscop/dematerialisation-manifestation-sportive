from django.db import models

from core.models import User, Instance
from structure.models.organisation import Organisation


class ErrorSMTP(models.Model):

    ERROR_CHOICES = (
        # Anti spam
        ('SPA', 'Anti-Spam'),
        # Boite mail inexistante
        ('INC', 'Adresse email inconnue'),
        # Problème hôte
        ('HOT', 'Problème hôte adresse email'),
        # Absent
        ('ABS', 'Utilisateur absent'),
        # Boîte mail pleine
        ('PLE', 'Boîte pleine'),
        # Changement d'adresse
        ('ADR', 'Changement d\'adresse'),
    )

    email = models.EmailField(max_length=200)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    instance = models.ForeignKey(Instance, on_delete=models.CASCADE, null=True, blank=True)
    service = models.ForeignKey(Organisation, on_delete=models.SET_NULL, null=True, blank=True)
    type_erreur = models.CharField(max_length=5, choices=ERROR_CHOICES)
    contenu_email = models.TextField(max_length=5000)
    date = models.DateTimeField(auto_now_add=True)
    resolu = models.BooleanField(default=False)

    def __str__(self):
        return self.email