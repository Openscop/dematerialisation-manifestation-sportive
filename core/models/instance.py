# coding: utf-8
""" Configuration """
from datetime import timedelta
from smtplib import SMTPException
import html
from lxml.html.clean import Cleaner

import logging
from django.conf import settings
from django.core.mail import get_connection, send_mail
from django.db import models
from django.db.utils import IntegrityError

from ckeditor.fields import RichTextField

from administrative_division.models.departement import Departement
from core.util.admin import set_admin_info
from core.util.application import get_current_subdomain
from core.tasks import set_instructeur


logger = logging.getLogger('core')
mail_logger = logging.getLogger('smtp')


class InstanceManager(models.Manager):
    """ Manager pour les instances """

    # Getter
    def get_for_request(self, request):
        """ Renvoyer l'objet d'instance pour l'utilisateur """
        subdomain = get_current_subdomain(request)
        try:
            number = subdomain.strip()
            instance = self.get(departement__name=number)
            return instance
        except (ValueError, Instance.DoesNotExist):
            # Si le sous-domaine n'est pas valide, utiliser l'instance de l'utilisateur
            if hasattr(request, 'user') and request.user.is_authenticated:
                instance = request.user.get_instance()
                return instance
            return self.get_master()
        except:
            return None

    def get_master(self):
        """ Renvoyer l'instance par défaut """
        masters = self.filter(departement__isnull=True)
        if masters.exists():
            return masters.first()
        return self.create(departement=None, name="(Master)")

    def not_master(self):
        """ Renvoyer les instances non master """
        return self.exclude(departement__isnull=True).order_by("name")

    def configured(self):
        """ Renvoyer toutes les instances non master """
        return self.not_master()

    @staticmethod
    def get_for_user(user):
        """ Renvoyer l'instance de configuration utilisée par l'utilisateur """
        return user.get_instance()

    def get_by_natural_key(self, departement):
        return self.get(departement=departement)

    # Setter
    def set_for_departement(self, number):
        """ Inscrire un département : assigner à une nouvelle instance (et/ou renvoyer l'instance) """
        try:
            departement = Departement.objects.db_manager(self.db).get(name=number)
            instance = self.get_or_create(departement=departement)[0]
            if not instance.name:
                instance.name = "Configuration {num}".format(num=number)
                instance.save()
            return instance
        except IntegrityError:
            return self.get(departement__name=number)


class Instance(models.Model):
    """ Configuration de l'application par département """

    # Constantes
    INSTRUCTION_MODES = [[0, "Circuit B"], [1, "Circuit A"], [2, "Circuit D"],
                         [3, "Circuit C"], [4, "Circuit E"]]
    IM_CIRCUIT_B, IM_CIRCUIT_A, IM_CIRCUIT_D, IM_CIRCUIT_C, IM_CIRCUIT_E = 0, 1, 2, 3, 4
    AC_ARRONDISSEMENT = 0
    # TODO : V6 supp (plus de lecture seule)
    ACCES_ARRONDISSEMENT_CHOICES = [[0, "Non"], [1, "Oui en lecture seule"], [2, "Oui en Lecture ET Instruction"]]
    EMAIL_HELP = """Paramètres SMTP au format host;port;user;password. Une chaîne vide utilise les paramètres par défaut."""
    ETAT = (('ferme', 'Fermé'), ('deploiement', 'En cours de déploiement'), ('test', 'En cours de test'), ('ouvert', 'Ouvert'))

    # Champs
    name = models.CharField(max_length=96, blank=False, verbose_name="Nom")
    departement = models.OneToOneField('administrative_division.departement', null=True, blank=True, related_name='instance', verbose_name="Département", on_delete=models.SET_NULL)
    etat_s = models.CharField(default="ferme", max_length=30, choices=ETAT, verbose_name="Etat de l'instance")
    message = RichTextField(default="", blank=True, verbose_name="Message d'avertissement (celui du master sera automatiquement ajouté au dessus de celui ci)")

    # Modes d'instruction (territoire)
    instruction_mode = models.SmallIntegerField(default=IM_CIRCUIT_B, choices=INSTRUCTION_MODES, verbose_name="Mode d'instruction", help_text="L'option \"Par département\" permet à tous les instructeurs de toutes les préfecture & sous-préfectures du département d'être concernés par toutes les manifestations du département. L'option \"Par arrondissement\" oriente les manifestations vers la préfecture ou sous-préfectures de l'arrondissement.")
    acces_arrondissement = models.SmallIntegerField(default=AC_ARRONDISSEMENT, choices=ACCES_ARRONDISSEMENT_CHOICES, verbose_name="Accès aux autres arrondissements", help_text="Attention: Cet élément n'est pris en compte que si l'option \"Par arrondissement\" est sélectionnée dans le choix \"Mode d'instruction\"")
    depot_hors_delai = models.BooleanField(default=False, verbose_name="Autoriser la démarche de dépôt hors délai")

    # Délais des avis
    avis_delai_federation = models.SmallIntegerField(default=28, verbose_name="Délai de réponse des avis pour les fédérations")
    # avis_delai_service = models.SmallIntegerField(default=21, verbose_name="Délai de réponse des avis pour les services")
    avis_delai_relance = models.SmallIntegerField(default=5, verbose_name="Délai d'envoi des relances d'avis")

    action_delai_relance = models.SmallIntegerField(default=14, verbose_name="Délai d'envoi des relance aux instructeurs en cas d'action non lue")
    reunion_cdsr_delai_relance = models.SmallIntegerField(default=7, verbose_name="Délai d'envoi des relances aux services pour les avis à rendre et les confirmations de présence aux réunions CDSR")
    convocation_cdsr_delai_relance = models.SmallIntegerField(default=21, verbose_name="Délai d'envoi des relances aux instructeurs pour la convocation aux réunions CDSR")

    # Email d'expéditeur pour les notifications de manifestations
    sender_email = models.CharField(max_length=128, default="Manifestationsportive.fr <plateforme@manifestationsportive.fr>", verbose_name="Email d'expéditeur")
    email_settings = models.CharField(max_length=192, blank=True, default='', help_text=EMAIL_HELP, verbose_name="Configuration email")

    # Nouvelles fonctionnalités en test
    activer_beta = models.BooleanField(default=False, verbose_name="Autoriser les nouvelles fonctionnalités")

    objects = InstanceManager()
    # Getter
    def is_master(self):
        """
        Renvoie si cette instance sert de modèle lorsqu'une information est indisponible

        dans une instance de département.
        """
        return self.departement is None

    def get_email(self):
        """ Renvoie l'email d'expéditeur de l'instance """
        return self.sender_email

    def get_message(self):
        """Renvoi un tableau avec le message du master et celui de l'instance"""
        tab = []
        master = Instance.objects.filter(name="(Master)")
        if master and not self.is_master():
            master_msg = master.first().message
            tab.append(master_msg) if master_msg else ""
        tab.append(self.message) if self.message else ""
        return tab

    @set_admin_info(short_description="Mode d'instruction")
    def get_instruction_mode(self):
        """ Renvoyer le mode d'instruction """
        return self.instruction_mode

    def get_departement(self):
        """ Renvoyer le département """
        return self.departement

    def get_departement_name(self):
        """ Renvoyer le nom du département, ou le territoire """
        name = self.departement.name if self.departement else "Nationale"
        return name.upper()

    def get_departement_friendly_name(self):
        """ Renvoyer le nom entier du département """
        name = self.departement.get_name_display() if self.departement else "France"
        return name

    def get_email_settings(self):
        """ Renvoyer les paramètres d'email pour l'instance """
        values = self.email_settings
        if values.strip() == '' or len(values.split(';')) < 4:
            values = "{host};{port};{user};{pwd}".format(host=settings.EMAIL_HOST, port=int(settings.EMAIL_PORT), user=settings.EMAIL_HOST_USER,
                                                         pwd=settings.EMAIL_HOST_PASSWORD)
        values = values.split(';')
        config = {'host': values[0], 'port': int(values[1]), 'username': values[2], 'password': values[3]}
        return config

    # Actions
    def send_test_mail(self):
        """ Envoyer un mail de test avec les paramètres de l'instance """
        sender_email = self.get_email() or settings.DEFAULT_FROM_EMAIL
        email_config = self.get_email_settings()
        connection = get_connection(use_tls=True, **email_config)  # utiliser le EMAIL_BACKEND
        try:
            send_mail(subject="[Manifestation Sportive] Message de test Manifestationsportive.fr", message="Ceci est un message de test", from_email=sender_email,
                      recipient_list=['destinataire@aa.aa'], connection=connection)
        except SMTPException:
            mail_logger.exception("core.instance.send_test_mail: failure")
        return True

    # Override
    def save(self, *args, **kwargs):
        if self.pk is None and self.departement is None:
            # Forcer l'unicité de l'instance master
            count = Instance.objects.filter(departement=None).count()
            if count != 0:
                raise IntegrityError("Une seule instance master (number=None) peut exister")

        # nettoyage du ckeditor
        if self.message:
            cleaner = Cleaner()
            cleaner.javascript = True
            style_autorise = frozenset(['style'])
            cleaner.safe_attrs = cleaner.safe_attrs.union(style_autorise)
            self.message = cleaner.clean_html(html.unescape(self.message))
        retour = super(Instance, self).save(*args, **kwargs)
        if settings.CELERY_ENABLED:
            set_instructeur.delay(self.pk)
        else:
            set_instructeur(self.pk)
        return retour

    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        name = self.name or self.departement.get_name_display() if self.departement else "Master"
        departement_name = self.departement.name if self.departement else "Nationale"
        return "{name} ({dept})".format(name=name, dept=departement_name)

    def natural_key(self):
        return self.departement,

    # Métadonnées
    class Meta:
        verbose_name = "configuration d'instance"
        verbose_name_plural = "configurations d'instance"
        app_label = 'core'
