# coding: utf-8
import pytz

from django.contrib.auth.models import AbstractUser, UserManager as BaseUserManager
from django.db import models
from django.utils.html import format_html, mark_safe
from django.utils import timezone
from core.models.instance import Instance
from core.util.admin import set_admin_info


class UserManager(BaseUserManager):
    """ Manager des utilisateurs """

    # Getter
    def active(self):
        """ Renvoyer les utilisateurs actifs """
        return self.filter(is_active=True)

    def get_email_admin_instance(self, instance):
        """
        Renvoyer les emails des utilisateurs administrateurs d'instance

        :param instance: ex. 'instructeur'
        :type instance: obj
        """
        liste = self.filter(groups__name__icontains='Administrateurs d\'instance').filter(
            default_instance=instance, email__isnull=False).values_list('email', flat=True)
        return liste


def JSON_OPENRUNNER_OPTION():
    return {
        "preferences": {
            "map_default_center_latitude": 45.42,
            "map_default_center_longitude": 4.40,
            "map_default_center_label": "Saint-Étienne",
            "map_is_mouse_wheel_enabled": True,
            "map_should_center_on_add_point": False,
            "map_stroke_opacity": 0.7,
            "map_stroke_width": 4,
            "map_stroke_color_hex": "#b71c0c",
            }
        }


class User(AbstractUser):
    """ Modèle utilisateur """

    # Constantes
    STATUS = [[0, "Normal"], [1, "Nom d'utilisateur modifié"]]
    NORMAL, USERNAME_CHANGED = 0, 1

    # Champs
    default_instance = models.ForeignKey('core.instance', null=True, related_name='users',
                                         verbose_name="Instance de configuration", on_delete=models.SET_NULL)
    organisation_m2m = models.ManyToManyField("structure.Organisation", blank=True, verbose_name="organisation", related_name="users")
    status = models.SmallIntegerField(choices=STATUS, default=NORMAL, verbose_name="Statut")
    last_name = models.CharField(default="à renseigner", null=False, blank=False, max_length=50)
    first_name = models.CharField(default="à renseigner", null=False, blank=False, max_length=50)
    last_visit = models.DateTimeField(blank=True, null=True)
    last_manif = models.IntegerField(blank=True, null=True)
    last_change_password = models.DateTimeField(blank=True, null=True, verbose_name="Date du dernier changement du mot de passe")
    recap = models.BooleanField(default=True, verbose_name="Envoi du récapitulatif des messages")
    openrunner_option = models.JSONField(default=JSON_OPENRUNNER_OPTION, verbose_name="Json des option de l'éditeur openrunneur")
    cgu = models.BooleanField("acceptation des CGU", default=False)
    history_json = models.JSONField(verbose_name="Historique des modifications", blank=True, null=True, max_length=100000)
    objects = UserManager()

    def __str__(self):
        return self.get_full_name()

    def save(self, *args, **kwargs):
        # Première sauvegarde, pk est None
        created = self.pk
        ancien_mdp = ""
        if created:
            ancien_mdp = User.objects.get(pk=self.pk).password
        super(User, self).save(*args, **kwargs)
        nouveau_mdp = self.password

        if created:
            # Mettre à jour last_change_password si le mot de passe a été changé et tracer
            if not ancien_mdp == nouveau_mdp:
                from messagerie.models import Message
                Message.objects.creer_et_envoyer('tracabilite', None, [self], "Mot de passe modifié", "Mot de passe modifié")
                self.last_change_password = timezone.now()
                self.save()
        else:
            # Initialiser la date
            self.last_change_password = timezone.now()
            self.save()
            # Créer les options et le compteur de message avec l'utilisateur
            from core.models.optionuser import OptionUser
            option = OptionUser(user=self)
            option.save()
            from messagerie.models import CptMsg
            cpt = CptMsg(utilisateur=self)
            cpt.save()

    # Getter
    def get_instance(self):
        """ Renvoyer l'instance de configuration de workflow """
        return self.default_instance or Instance.objects.get_master()

    def get_departement(self):
        """ Renvoyer le département de l'instance de l'utilisateur """
        return self.get_instance().get_departement()

    @set_admin_info(short_description="Service")
    def get_service(self):
        """ Renvoyer le service de l'utilisateur """
        if self.organisation_m2m.last():
            return self.organisation_m2m.last()

    @set_admin_info(short_description="Service")
    def get_first_service(self):
        """ Renvoyer le premier service de l'utilisateur """
        return self.organisation_m2m.first()

    def is_agent_service_instructeur(self):
        """ Renvoyer si l'utilisateur fait partie d'un service instructeur """
        for organisation in self.organisation_m2m.all():
            if organisation.is_service_instructeur():
                return True
        return False

    def is_agent_federation(self):
        """ Renvoyer True si l'utilisateur fait partie d'un service instructeur """
        for organisation in self.organisation_m2m.all():
            if organisation.is_federation():
                return True
        return False

    def is_agent_service_consulte(self):
        """ Renvoyer si l'utilisateur fait partie d'un service de consultation"""
        for organisation in self.organisation_m2m.all():
            if organisation.is_service_consulte():
                return True
        return False

    def is_agent_mairie(self):
        """ Renvoyer si l'utilisateur fait partie d'un service de consultation"""
        for organisation in self.organisation_m2m.all():
            if organisation.is_mairie():
                return True
        return False

    def is_organisateur(self):
        """ Renvoyer si l'utilisateur fait partie d'une structure organisatrice """
        for organisation in self.organisation_m2m.all():
            if organisation.is_structure_organisatrice():
                return True
        return False

    def has_group(self, group_name):
        """ Renvoyer si l'utilisateur appartient à un groupe """
        return self.groups.filter(name=group_name).exists()

    def has_email_failed(self):
        """Retourne True si l'user à plus de deux erreurs de délivrance de mail """
        from core.models.errorsmtp import ErrorSMTP
        return ErrorSMTP.objects.filter(user=self, resolu=False).count() > 1

    def get_email_failed(self):
        """Retourne les erreurs smtp si l'user à plus de deux erreurs de délivrance de mail """
        from core.models.errorsmtp import ErrorSMTP
        if ErrorSMTP.objects.filter(user=self).count() > 1:
            error_smtp = ErrorSMTP.objects.filter(user=self, resolu=False)
            return error_smtp
        return False

    def get_email_failed_message(self):
        """Retourne un message a afficher si l'user à plus de deux erreurs de délivrance de mail """
        from core.models.errorsmtp import ErrorSMTP
        if ErrorSMTP.objects.filter(user=self).count() > 1:
            error_smtp = ErrorSMTP.objects.filter(user=self).last()
            message = f"Nous ne parvenons pas à joindre l'adresse e-mail \"{error_smtp.email}\" " \
                      f"pour la raison suivante : <strong>{error_smtp.get_type_erreur_display()}</strong>"
            return message
        return False

    def is_new(self):
        """ Retourne True si l'utilisateur est un organisateur inscrit depuis moins de 24h """
        if self.is_organisateur():
            utc = pytz.UTC
            date_inscription = self.date_joined.replace(tzinfo=utc)
            now = timezone.now().replace(tzinfo=utc)
            if now - timezone.timedelta(hours=24) <= date_inscription <= now:
                return True

    @set_admin_info(short_description="Nom complet")
    def get_full_name(self):
        """ Renvoyer le nom complet de l'utilisateur """
        full_name = "{0} {1}".format(self.first_name.title(), self.last_name.upper()).strip()
        if self.has_group("Administrateurs d'instance"):
            full_name += "  ☘"
        if self.has_group("_support"):
            full_name += "  🛡️"
        return full_name or self.username

    @set_admin_info(short_description="Nom complet avec pseudo")
    def get_full_name_and_username(self):
        """ Renvoyer le nom complet de l'utilisateur """
        full_name = "{0} {1} <spam class='pseudo-light'>{2}</span>".format(self.first_name.title(), self.last_name.upper(), self.username).strip()
        return mark_safe(full_name)

    @set_admin_info(short_description="Lien vers la fiche")
    def get_link_to_admin(self):
        """ Renvoyer un lien vers la page d'administration de l'utilisateur (Core / User)"""
        return format_html('<a href="/admin/core/user/' + str(self.pk) + '/change/">Voir la fiche</a>')

    # Setter
    def set_instance(self, instance):
        """
        Définir l'instance par défaut de l'utilisateur

        :param instance: nom du département ou instance (ex. '42') ou instance
        :type instance: core.models.Instance | str
        :return: True si l'opération a correctement été effectuée
        :rtype: bool
        """
        if isinstance(instance, str):
            instances = Instance.objects.filter(departement__name=instance)
            if instances.exists():
                self.default_instance = instances.first()
                return True
            return False
        else:
            self.default_instance = instance
            return True

    # Méta
    class Meta:
        verbose_name = "Utilisateur"
        verbose_name_plural = "Utilisateurs"
        app_label = 'core'


class OneToUserModelMixin:
    """ Mixin pour les clés naturelles des objets liés en 1:1 aux utilisateurs """

    # Overrides
    def natural_key(self):
        return self.user.username,


class OneToUserQuerySetMixin:
    """ Mixin pour les clés naturelles des objets liés en 1:1 aux utilisateurs """

    # Overrides
    def get_by_natural_key(self, username):
        return self.get(user__username=username)
