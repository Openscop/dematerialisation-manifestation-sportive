from django.shortcuts import redirect


def csrf_failure(request, reason=""):
    """
    Changer la vue csrf erreur pour renvoyer sur l'accueil
    """
    return redirect('home_page')
