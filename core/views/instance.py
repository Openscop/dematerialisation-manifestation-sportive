from django.views import View
from django.shortcuts import get_object_or_404, redirect

from post_office import mail

from core.models import Instance


class SendMailVerifInstance(View):
    """
    class pour envoyer un mail de test de la config d'instance
    """

    def get(self, request, pk):

        if request.user.has_group('Administrateurs d\'instance') or request.user.has_group('_support'):
            instance = get_object_or_404(Instance, pk=pk)
            desti_mail = "contact@openscop.fr"
            expe_mail = instance.get_email()
            email = mail.send(
                desti_mail,
                expe_mail,
                subject="Test d'envoi de mail",
                html_message="<p>Ceci est un test d'envoi de mail</p>",
            )
            return redirect('admin:post_office_email_change', email.pk)
        return redirect('accueil')
