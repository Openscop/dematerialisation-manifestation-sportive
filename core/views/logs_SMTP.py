from django.views.generic import View
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from core.models import ErrorSMTP, Instance


class ListLogUser(View):
    """
    Afficher la liste des utilisateurs qui
    ont au moins un log d'erreur
    """

    @method_decorator(login_required)
    def get(self, request):
        is_admin = bool((self.request.session['o_support']) or self.request.user.has_group("Administrateurs d\'instance"))
        # Récupérer les logs où le champ "resolu" = False et qui correspondent à l'instance de l'administrateur connecté
        if is_admin:
            logs = ErrorSMTP.objects.filter(resolu=False).filter(instance=Instance.objects.get_for_request(request)).\
                distinct('email')
            context = {'logs': logs}
            return render(request, 'core/list_logs_page.html', context)
        else:
            return render(request, 'core/access_restricted.html', status=403)


class LogsDetailView(View):
    """
    Afficher les logs d'erreur d'un utilisateur
    """

    @method_decorator(login_required)
    def get(self, request, **kwargs):

        pk_log = ErrorSMTP.objects.get(pk=self.kwargs.get('pk'))
        email = pk_log.email
        if pk_log.service:
            object = pk_log.service
            is_admin = bool((self.request.session['o_support']) or
                            (self.request.user.has_group("Administrateurs d\'instance") and
                             object.default_instance == self.request.user.default_instance)
                            )
        else:
            object = pk_log.user
            is_admin = bool((self.request.session['o_support']) or object.is_service_consulte() and
                            (self.request.user.has_group("Administrateurs d\'instance") and
                             object.instance_fk == self.request.user.default_instance)
                            # or (user_organisation in service_selected.get_family_member_qs().filter(admin_service_famille_b=True))
                            )
        if is_admin:
            # Récupérer tous les logs d'un utilisateur où le champ "resolu" = False
            logs = ErrorSMTP.objects.filter(email=email).filter(resolu=False)
            context = {'logs': logs, 'detail': True, 'is_admin': is_admin}
            return render(request, 'core/list_logs_page.html', context)
        else:
            return render(request, 'core/access_restricted.html', status=403)


class ResolutionLog(View):
    """
    Résoudre les logs
    """

    @method_decorator(login_required)
    def get(self, request, **kwargs):
        pk_log = ErrorSMTP.objects.get(pk=self.kwargs.get('pk'))
        email = pk_log.email
        if pk_log.service:
            is_admin = bool((self.request.session['o_support']) or pk_log.service.is_service_consulte() and
                            (self.request.user.has_group("Administrateurs d\'instance") and
                             pk_log.service.instance_fk == self.request.user.default_instance)
                            # or (user_organisation in service_selected.get_family_member_qs().filter(admin_service_famille_b=True))
                            )
        else:
            is_admin = bool((self.request.session['o_support']) or
                            (self.request.user.has_group("Administrateurs d\'instance") and
                             pk_log.user.default_instance == self.request.user.default_instance)
                            )
        if is_admin:
            # Récupérer tous les logs d'un utilisateur où le champ "resolu" = False
            logs = ErrorSMTP.objects.filter(email=email).filter(resolu=False)
            for log in logs:
                log.resolu = True
                log.save()
            return redirect('/list_logs/')
        else:
            return render(request, 'core/access_restricted.html', status=403)