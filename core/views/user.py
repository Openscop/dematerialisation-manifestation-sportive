# coding: utf-8
import logging
import html

from django.http import HttpResponse
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404, redirect, reverse, render
from django.contrib import messages
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout, get_user_model
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, TemplateView, View
from django.views.generic.edit import UpdateView
from django.template.loader import render_to_string
from django.core.mail import send_mail, get_connection
from django.db.models import Q
from django.conf import settings
from allauth.account.views import SignupView, PasswordChangeView, PasswordResetView, LoginView
from allauth.account.utils import send_email_confirmation
from allauth.account.forms import ResetPasswordForm

from administrative_division.models.region import Region
from core.models import User
from core.forms import UserUpdateForm, SignupAgentForm, SignupOrganisateurForm
from core.models import Instance
from messagerie.models import Message
from messagerie.models.cptmsg import CptMsg
from structure.models.service import CategorieService
from structure.models.organisation import Organisation, InvitationOrganisation

mail_logger = logging.getLogger('smtp')


class RenvoiMailConfirm(View):
    """
    Vue utilisée par l'admin pour renvoyer un mail de confirmation
    """
    def get(self, request, pk):
        if request.user.has_group('Administrateurs d\'instance') or request.user.has_group('_support'):
            user = get_object_or_404(User, pk=pk)
            send_email_confirmation(request, user)
            return redirect("admin:core_user_change", pk)
        else:
            return redirect("admin:core_user_change", pk)


class ProfileDetailView(DetailView):
    """ Vue du profil utilisateur """

    # Configuration
    model = get_user_model()

    # Overrides
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProfileDetailView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organisation = Organisation.objects.get(pk=self.request.session["service_pk"])
        if organisation.is_structure_organisatrice():
            is_admin = True if self.request.user == organisation.get_users_qs().order_by('pk').first() else False
        else:
            is_admin = bool((self.request.session['o_support'])
                            or organisation.is_service_consulte() and (self.request.user.has_group("Administrateurs d\'instance") and
                             organisation.instance_fk == self.request.user.default_instance)
                            # or (user_organisation in service_selected.get_family_member_qs().filter(admin_service_famille_b=True))
                            )
        context['user_organisation'] = organisation
        context['is_admin'] = is_admin
        context['user_is_owner'] = True
        context['is_profil'] = True

        return context


class ProfilPublicView(DetailView):
    """ Vue du profil publique utilisateur """

    model = User

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or not request.user.is_agent_service_consulte():
            return redirect('home_page')
        return super(ProfilPublicView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        context['user_organisation'] = Organisation.objects.get(pk=self.request.session["service_pk"])
        user_profile = User.objects.get(pk=self.kwargs['pk'])
        cpt_msg = CptMsg.objects.get(utilisateur=user_profile)
        is_admin = bool((self.request.session['o_support']) or
                        (self.request.user.has_group("Administrateurs d\'instance") and
                         user_profile.default_instance == self.request.user.default_instance))
        context['is_admin'] = is_admin
        context['cpt_msg'] = cpt_msg
        context['user'] = user
        context['is_profil'] = True
        return context


class PasswordChangeCustomView(PasswordChangeView):
    """
    Evite de rester sur la même vue après un changement de mot de passe
    L'url est interceptée avant transmission à allauth
    """
    success_url = reverse_lazy('profile')


class PasswordResetCustomView(PasswordResetView):
    """
    Class pour ajouter un message de traçabilité à la demande d'un nouveau mot de passe
    """

    def form_valid(self, form):
        response = super(PasswordResetCustomView, self).form_valid(form)
        if form.users:
            user = form.users[0]
            Message.objects.creer_et_envoyer(
                'tracabilite', None, [user], "Demande de nouveau mot de passe",
                "Vous avez sollicité la réinitialisation de votre mot de passe. Pour cela, un lien a été envoyé par email.<br>"
                "Veuillez vous assurer que vous êtes en capacité de recevoir des emails sur cette adresse et que "
                "<strong>le domaine manifestationsportive.fr fait partie des domaines autorisés</strong> par votre messagerie.<br>"
                "Si vous ne savez pas comment faire, consultez cette <a href='/aide/probleme-reception-email' target='_blank'>page d'aide</a>.")
        return response


class PasswordExpired(View):
    """
    Si le mot de passe est perimé on l'efface
    """
    def get(self, request):
        # le mot de passe va être purgé et on va envoyé un mail de reset
        request.user.set_unusable_password()
        request.user.save()
        Message.objects.creer_et_envoyer(
            'tracabilite', None, [request.user], 'Mot de passe expiré',
            "Votre mot de passe a expiré. Celui n'est plus opérationnel. Un nouveau mot de passe doit être défini pour réaccéder à la plateforme.")
        reset_password_form = ResetPasswordForm(data={'email': request.user.email})
        if reset_password_form.is_valid():
            reset_password_form.save(request=request)

            # bof Avertir les administrateurs d'instance
            instance = request.user.get_instance()
            admin_instance_list = User.objects.get_email_admin_instance(instance)
            if admin_instance_list:
                try:
                    sender_email = request.user.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
                    email_config = request.user.get_instance().get_email_settings()
                    connection = get_connection(use_tls=True, **email_config)
                    subject = "Mot de passe expiré"
                    msg = render_to_string('messagerie/mail/password_expired.txt', {'username': request.user.username,
                                                                                       'email': request.user.email})
                    send_mail(subject=html.unescape(subject), message=html.unescape(msg),
                              from_email=sender_email,
                              recipient_list=list(admin_instance_list), connection=connection)
                except:
                    pass
            # eof Avertir les administrateurs d'instance

            logout(request)
            return render(request, "core/password_expired.html", status=400)
        else:
            return render(request, "403.html", status=400)


class UserUpdateView(UpdateView):
    """ Modification du profil utilisateur """

    # Configuration
    model = get_user_model()
    form_class = UserUpdateForm
    success_url = '/accounts/profile/'

    # Overrides
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user


class CustomLoginView(LoginView):

    def form_valid(self, form):
        user = form.user
        if not user.organisation_m2m.first():
            messages.error(self.request,
                           'Vous ne pouvez pas vous connecter, car ce compte n\'est rattaché à aucune organisation.')
            return redirect(reverse('account_login'))
        else:
            return super().form_valid(form)


login = CustomLoginView.as_view()


class SignupAgentView(SignupView):
    form_class = SignupAgentForm
    template_name = "account/signupagent.html"
    view_name = 'signupagentview'

    def dispatch(self, request, *args, **kwargs):
        if self.request.GET.get('token'):
            invitation = InvitationOrganisation.objects.filter(token_s=self.request.GET['token'])
            if not invitation.exists():
                return render(request, "core/access_restricted.html",
                              {'message': "l'url utilisée n'est pas correcte"}, status=403)
            elif not invitation.filter(completee_b=False, expiree_b=False, revoquee_b=False).exists():
                return render(request, "core/access_restricted.html",
                              {'message': "l'invitation n'est plus valable"}, status=403)
            else:
                self.request.invitation = InvitationOrganisation.objects.get(token_s=self.request.GET['token'])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ret = super(SignupAgentView, self).get_context_data(**kwargs)
        ret.update(self.kwargs)
        categories = list(CategorieService.objects.all().values_list('pk', 'nom_s'))
        if (1, "Manifestation sportive") in categories:
            categories.remove((1, "Manifestation sportive"))
        if (2, "_Support") in categories:
            categories.remove((2, "_Support"))
        ret["categories"] = categories
        ret["instances"] = Instance.objects.configured()
        ret["regions"] = Region.objects.all()
        return ret

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if hasattr(self.request, 'invitation'):
            kwargs['initial'].update({'service': self.request.invitation.service_fk.pk})
            kwargs['initial'].update({'service_fk': self.request.invitation.service_fk})
            kwargs['initial'].update({'email': self.request.invitation.email_s})
        return kwargs

    def get_success_url(self):
        if hasattr(self, 'user') and self.user.emailaddress_set.first().verified:
            return reverse('email_confirmed')
        else:
            super().get_success_url()


signupagentview = SignupAgentView.as_view()


class SignupOrganisateurView(SignupView):
    form_class = SignupOrganisateurForm
    view_name = 'signuporganisateurview'

    def dispatch(self, request, *args, **kwargs):
        if self.request.GET.get('token'):
            invitation = InvitationOrganisation.objects.filter(token_s=self.request.GET['token'])
            if not invitation.exists():
                return render(request, "core/access_restricted.html",
                              {'message': "l'url utilisée n'est pas correcte"}, status=403)
            elif not invitation.filter(completee_b=False, expiree_b=False, revoquee_b=False).exists():
                return render(request, "core/access_restricted.html",
                              {'message': "l'invitation n'est plus valable"}, status=403)
            else:
                self.request.invitation = InvitationOrganisation.objects.get(token_s=self.request.GET['token'])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ret = super(SignupOrganisateurView, self).get_context_data(**kwargs)
        ret.update(self.kwargs)
        return ret

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if hasattr(self.request, 'invitation'):
            kwargs['initial'].update({'service_fk': self.request.invitation.service_fk})
            kwargs['initial'].update({'structure_name': self.request.invitation.service_fk.precision_nom_s})
            kwargs['initial'].update({'type_of_structure': self.request.invitation.service_fk.type_organisation_fk})
            kwargs['initial'].update({'phone': self.request.invitation.service_fk.telephone_s})
            kwargs['initial'].update({'address': self.request.invitation.service_fk.adresse_s})
            kwargs['initial'].update({'website': self.request.invitation.service_fk.site_web_s})
            kwargs['initial'].update({'departement': self.request.invitation.service_fk.commune_m2m.first().arrondissement.departement})
            kwargs['initial'].update({'commune': self.request.invitation.service_fk.commune_m2m.first()})
            kwargs['initial'].update({'email': self.request.invitation.email_s})
        return kwargs


signuporganisateurview = SignupOrganisateurView.as_view()

class EmailConfirmedView(TemplateView):
    template_name = "account/email_confirmed.html"

    def get_context_data(self, **kwargs):
        context = super(EmailConfirmedView, self).get_context_data(**kwargs)
        context['valid'] = True if self.request.GET.get('valid') == 'true' else False
        return context


@method_decorator(login_required, name='dispatch')
class NomPrenomChange(View):

    def post(self, request):
        last_name = request.POST['nom']
        first_name = request.POST['prenom']
        user = request.user
        user.last_name = last_name
        user.first_name = first_name
        user.save()
        return HttpResponse(200)