# Generated by Django 4.0.6 on 2022-09-20 08:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_configurationglobale_recap_ecart_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='configurationglobale',
            name='stat_cache_nb_feuille_eco',
            field=models.IntegerField(default=793946, verbose_name='Nombre de feuille économisées'),
        ),
        migrations.AddField(
            model_name='configurationglobale',
            name='stat_cache_nb_instruction',
            field=models.IntegerField(default=8012, verbose_name="Nombre d'instruction"),
        ),
        migrations.AddField(
            model_name='configurationglobale',
            name='stat_cache_nb_km_parcouru',
            field=models.IntegerField(default=677448, verbose_name='Km de parcours'),
        ),
        migrations.AddField(
            model_name='configurationglobale',
            name='stat_cache_nb_participant',
            field=models.IntegerField(default=4171256, verbose_name='Nombre de participants'),
        ),
    ]
