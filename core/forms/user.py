# coding: utf-8
import dns.resolver
from allauth.account.models import signals
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, HTML, Div
from django import forms
from django.conf import settings
from allauth.account.forms import SignupForm
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.urls import reverse_lazy
from django.forms.models import ModelChoiceField
from django.core.mail import send_mail
from django.core.exceptions import ValidationError

from administrative_division.models import Commune
from administrative_division.models.departement import Departement
from clever_selects.form_fields import ChainedModelChoiceField
from core.forms.base import GenericForm
from core.models import User, OptionUser
from core.util.champ import TelField
from structure.models.organisation import StructureOrganisatrice, Organisation, TypeOrganisation


class UserUpdateForm(GenericForm):
    """ Formulaire de modification de l'utiliasteur """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(UserUpdateForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', "Enregistrer"))
        self.fields['first_name'].label = "Prénom"
        self.fields['last_name'].label = "Nom"

    # Meta
    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name', 'default_instance')


class CustomUserCreateForm(UserCreationForm):

    def clean_username(self):
        uname = self.cleaned_data['username']
        if User.objects.filter(username__iexact=uname):
            raise ValidationError("Le nom d'utilisateur est déjà pris !")
        return uname


class CustomUserChangeForm(UserChangeForm):

    def clean_username(self):
        uname = self.cleaned_data['username']
        if uname != self.initial['username']:
            if User.objects.filter(username__iexact=uname):
                raise ValidationError("Le nom d'utilisateur est déjà pris !")
        return uname

    def clean(self):
        cleaned_data = super(CustomUserChangeForm, self).clean()
        if cleaned_data['is_active'] != self.initial['is_active']:
            if cleaned_data['is_active']:
                send_mail(
                    '[Manifestation Sportive] Validation d\'inscription',
                    "Mr ou Mme " + self.instance.first_name + '  ' + self.instance.last_name + chr(10) +
                    'Votre compte est maintenant validé sur la plateforme ' + Site.objects.get_current().domain + '.' + chr(10) +
                    "Nom d'utilisateur : " + self.instance.username + chr(10) +
                    "Dès à présent, vous pouvez vous connecter à la plateforme",
                    self.instance.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL,
                    [self.instance.email],
                    )


class SignupAgentForm(SignupForm):
    """ Formulaire d'inscription des agents des services"""

    def get_from_serviceliste(self, param):
        # Retrouver le titre détaillé en fonction de la sélection
        titre = ''
        for couple in self.SERVICE_LISTE:
            if param in couple:
                titre = couple[1]
        return titre

    # Champs
    first_name = forms.CharField(max_length=30, label="Prénom de l'agent")
    last_name = forms.CharField(max_length=30, label="Nom de l'agent")
    service = forms.IntegerField(required=False)
    service_fk = forms.CharField(max_length=300, label="Service pour lequel vous officierez", required=False,
                                 widget=forms.TextInput(attrs={'readonly': 'readonly'},))

    # Overrides
    def __init__(self, *args, **kwargs):
        """ Initialiser l'objet """
        super(SignupAgentForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'
        self.fields['username'].help_text = "Le nom d'utilisateur vous servira comme identifiant lors de la connexion au site"
        self.fields['username'].label = "Nom d'utilisateur <i class=\"ctx-help-choixpseudo\"></i>"
        self.fields['email'].label = 'Adresse email <i class="ctx-help-emailunique"></i>'
        self.fields['password1'].label = "Mot de passe <i class=\"ctx-help-motdepasse_requis\"></i>"
        self.fields['service'].label = "Service <i class=\"ctx-help-inscription_agent_service\"></i>"
        layout_commun = Layout(
            HTML("<div class='alert alert-warning'>{text}</div>".format(
                text="<p><strong>Cette page est dédiée à l'inscription des agents instructeurs "
                     "(préfectures ou mairies) et des agents d'un service consulté.</strong><br/><br/>"
                     "L'inscription des organisateurs s'effectue à l'aide d'un "
                     "<a href=\"/inscription/organisateur\">autre formulaire.</a></p>"
            )),
            Fieldset(
                "Création d'un compte Agent",
                HTML("<div>{text}</div>".format(
                    text="<p>Veuillez renseigner votre identité en tant qu'agent.<br/>Toutes les personnes "
                         "d'un même service qui seront amenées à accéder à un dossier doivent se créer "
                         "<strong>leur propre compte</strong>. (Le partage de compte n'est pas autorisé).</p>"
                )),
                'first_name',
                'last_name',
                'username',
                'email',
                HTML("<div class='alert alert-info offset-3 col-6'>{text}</div>".format(
                    text="<p>Veuillez vous assurer que vous êtes en capacité de recevoir des emails sur cette "
                         "adresse et que <strong>le domaine manifestationsportive.fr fait partie des domaines "
                         "autorisés</strong> par votre messagerie.<br>"
                         "Si vous ne savez pas comment faire, consultez cette "
                         "<a href='/aide/probleme-reception-email' target='_blank' "
                         "style='text-decoration: underline'>page d'aide</a>.</p>"
                )),
                'password1',
                'password2',
            ),
        )
        fieldset_complet = Fieldset(
            "Rattachement à un service",
            HTML("<div>{text}</div>".format(
                text="<p>Veuillez sélectionner le service pour lequel vous officiez :</p>"
            )),
            Div('service', css_class="d-none"),
            HTML(
                '<div class="d-flex flex-wrap"><button id="btn-modal-selecteur" type="button" '
                'class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#demandeAvisModal">{text}</button>'
                 '<div class="d-none ms-3 pt-2" id="div-service-selectione"><strong>Votre sélection : '
                '</strong><span id="span-service-selectione"></span></div></div>'.format(
                    text="Sélectionner un service")
            ),
            HTML(
                '{% include "structure/modal_selecteur_de_service.html" with categories=categories instances=instances %}'),
        )

        if 'service_fk' in self.initial:
            fieldset_invitation = Fieldset(
                "Rattachement à un service",
                Div('service', css_class="d-none"),
                'service_fk',
            )
            self.fields['email'].widget.attrs['readonly'] = 'readonly'
            self.helper.layout = Layout(layout_commun, fieldset_invitation)
        else:
            self.helper.layout = Layout(layout_commun, fieldset_complet)
        self.helper.add_input(Submit(
            'submit', "Je confirme ma demande d'inscription en tant qu'agent", css_class="disabled text-center mt-2"))

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data['service']:
            raise forms.ValidationError('Un problème est survenu dans la sélection du service.')
        return cleaned_data

    def clean_email(self):
        super().clean_email()
        domaine = self.cleaned_data['email'].split('@')[1]
        try:
            dns.resolver.query(domaine, 'MX', tcp=True)
        except:
            raise forms.ValidationError('Erreur de domaine')
        users = User.objects.filter(email=self.cleaned_data['email'])
        if users:
            raise forms.ValidationError('Adresse email déjà utilisée')
        return self.cleaned_data['email']

    def save(self, request):
        user = super(SignupAgentForm, self).save(request)
        user.is_active = False
        # Ajouter le service séléctionner dans les organisation de l'utilisateurs
        serviceobject = Organisation.objects.get(pk=self.cleaned_data['service'])
        if not serviceobject.pk in [1, 2]:
            user.organisation_m2m.add(serviceobject)
            user.default_instance = serviceobject.instance_fk
        user.save()
        # Marquer l'invitation comme réalisée et envoyer un message au nouvel utilisateur
        if hasattr(request, 'invitation'):
            request.invitation.completee_b = True
            request.invitation.save()
            # envoyer un msg de traçabilité à la création
            from messagerie.models import Message
            Message.objects.creer_et_envoyer('tracabilite', None, [user], 'invitation d\'inscription',
                                             'Bienvenue sur la plateforme ManifestationSportive.'
                                             f'Vous avez été invité par {request.invitation.origine_fk} '
                                             f'à la date du {request.invitation.date_creation}')
            # Vue qu'il s'agit d'une invitation confirmer l'adresse mail
            email_address = user.emailaddress_set.first()
            email_address.verified = True
            email_address.set_as_primary(conditional=True)
            # Important car déclenche l'envoie des mails de notification de création de compte
            signals.email_confirmed.send(
                sender=self.__class__,
                request=request,
                email_address=email_address,
            )
            email_address.save()
        # La confirmation de l'adresse mail se fait si is_active est True
        user.is_active = True
        # ajout de la table des options
        op = OptionUser.objects.filter(user=user)
        if not op:
            option = OptionUser(user=user)
            option.save()
        return user


class SignupOrganisateurForm(SignupForm):
    """ Formulaire d'inscription des organisateurs"""

    # Champs
    first_name = forms.CharField(max_length=30, label="Prénom du déclarant")
    last_name = forms.CharField(max_length=30, label="Nom du déclarant")
    cgu = forms.BooleanField(label="En cochant cette case, j'accepte les <a href='/aide/CGU'>Conditions Générales d'Utilisation</a>")

    # Structure
    structure_name = forms.CharField(
        label="Nom de la structure", max_length=200,
        help_text="Précisez le nom de la structure organisatrice (nom de l'association ou du club, de la société ou de la personne physique s'il s'agit d'un "
                  "particulier) de la manifestation"
    )
    type_of_structure = forms.CharField(widget=forms.Select(choices=[]),
                                        label="Forme juridique de la structure",
                                        help_text="Opérez votre choix parmi les statuts juridiques proposés")

    address = forms.CharField(label="Adresse", max_length=255)
    departement = ModelChoiceField(required=False, queryset=Departement.objects.all(), label="Département")
    commune = ChainedModelChoiceField('departement', reverse_lazy('administrative_division:commune_widget'), Commune, label="Commune")
    website = forms.URLField(label="Site web", max_length=200, required=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        """ Initialiser l'objet """
        super().__init__(*args, **kwargs)
        TYPE_STRUCTURE_CHOICES = [('', '-------')]
        for type in TypeOrganisation.objects.all():
            TYPE_STRUCTURE_CHOICES.append(
                (type.pk, type.nom_s)
            )
        self.fields['type_of_structure'].widget.choices = TYPE_STRUCTURE_CHOICES
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'
        self.fields['phone'] = TelField(label="Numéro de téléphone", widget=forms.TextInput(attrs={'pattern': '^0[0-9]{9}'},))
        self.fields['username'].help_text = "le nom d'utilisateur vous servira comme identifiant lors de la connexion au site"
        self.fields['username'].label = "Nom d'utilisateur <i class=\"ctx-help-choixpseudo\"></i>"
        self.fields['email'].label = 'Adresse email <i class="ctx-help-emailunique"></i>'
        self.fields['password1'].label = "Mot de passe <i class=\"ctx-help-motdepasse_requis\"></i>"
        self.helper.layout = Layout(
            HTML("<div class='alert alert-warning'>{text}</div>".format(
                text="<p><strong>Cette page est dédiée exclusivement à l'inscription des organisateurs de manifestations sportives.</strong><br><br>"
                     "Si vous êtes un agent des services consultés par la plateforme, ce formulaire ne vous est pas destiné. Dans ce cas,<br>"
                     "veuillez prendre contact avec le service instructeur de votre département afin de connaître la procédure d'inscription qui vous est dédié.</p>"
            )),
            Fieldset(
                "Structure organisatrice (personne morale)",
                HTML("<div>{text}</div>".format(
                    text="<p>Veuillez préciser les coordonnées de la <strong>structure organisatrice</strong> de manifestations sportives.<br/>"
                         "(Elles seront réutilisées automatiquement lors de la saisie des formulaires.)</p>"
                )),
                'structure_name',
                'type_of_structure',
                'address',
                'departement',
                'commune',
                'phone',
                'website',
            ),
            Fieldset(
                "Représentant légal (personne physique)",
                HTML("<div>{text}</div>".format(
                    text="<p>Veuillez renseigner votre identité en tant que "
                         "<strong>représentant légale de la structure organisatrice</strong> de manifestations sportives.</p>"
                )),
                'first_name',
                'last_name',
                'username',
                'email',
                HTML("<div class='alert alert-info offset-3 col-6'>{text}</div>".format(
                    text="<p>Veuillez vous assurer que vous êtes en capacité de recevoir des emails sur cette adresse et que "
                         "<strong>le domaine manifestationsportive.fr fait partie des domaines autorisés</strong> par votre messagerie.<br>"
                         "Si vous ne savez pas comment faire, consultez cette <a href='/aide/probleme-reception-email' target='_blank' "
                         "style='text-decoration: underline'>page d'aide</a>.</p>"
                )),
                'password1',
                'password2',
                'cgu',
            ),
        )
        if 'service_fk' in self.initial:
            self.fields['email'].widget.attrs['readonly'] = 'readonly'
            self.fields['structure_name'].widget.attrs['readonly'] = 'readonly'
            self.fields['commune'].widget.attrs['readonly'] = 'readonly'
            self.fields['phone'].widget.attrs['readonly'] = 'readonly'
            self.fields['address'].widget.attrs['readonly'] = 'readonly'
            self.fields['website'].widget.attrs['readonly'] = 'readonly'
            self.fields['type_of_structure'] = forms.CharField(max_length=300, label="Forme juridique de la structure", required=False,
                                                               widget=forms.TextInput(attrs={'readonly': 'readonly'},))
            self.fields['departement'] = forms.CharField(max_length=300, label="Département", required=False,
                                                         widget=forms.TextInput(attrs={'readonly': 'readonly'},))
            self.fields['commune'] = forms.CharField(max_length=300, label="Commune", required=False,
                                                     widget=forms.TextInput(attrs={'readonly': 'readonly'},))
        self.helper.add_input(Submit('submit', "Je confirme ma demande d'inscription en tant qu'organisateur"))

    def clean_structure_name(self):
        structure_name = self.cleaned_data['structure_name']
        if 'service_fk' not in self.initial:
            if StructureOrganisatrice.objects.filter(precision_nom_s=structure_name).exists():
                raise forms.ValidationError("Le nom \"{name}\" est déjà utilisé.".format(name=structure_name))
        return structure_name

    def clean_email(self):
        email = super().clean_email()
        domaine = email.split('@')[1]
        try:
            dns.resolver.query(domaine, 'MX', tcp=True)
        except:
            raise forms.ValidationError('Erreur de domaine')
        users = User.objects.filter(email=self.cleaned_data['email'])
        if users:
            raise forms.ValidationError('Adresse email déjà utilisée')
        return email

    def save(self, request):
        user = super(SignupOrganisateurForm, self).save(request)
        if hasattr(request, 'invitation'):
            structure = request.invitation.service_fk
        else:
            structure = StructureOrganisatrice.objects.create(
                precision_nom_s=self.cleaned_data['structure_name'],
                nom_s=self.cleaned_data['structure_name'],
                type_organisation_fk=TypeOrganisation.objects.get(pk=self.cleaned_data['type_of_structure']),
                telephone_s=self.cleaned_data['phone'],
                site_web_s=self.cleaned_data['website'],
                adresse_s=self.cleaned_data['address'],
                instance_fk=self.cleaned_data['commune'].get_instance(),
            )
            structure.commune_m2m.add(self.cleaned_data['commune'])
        user.organisation_m2m.add(structure)

        user.default_instance = structure.commune_m2m.first().get_instance()
        user.save()
        # Marquer l'invitation comme réalisée et envoyer un message au nouvel utilisateur
        if hasattr(request, 'invitation'):
            # Vue qu'il s'agit d'une invitation confirmer l'adresse mail
            email_address = user.emailaddress_set.first()
            email_address.verified = True
            email_address.set_as_primary(conditional=True)
            email_address.save()

            request.invitation.completee_b = True
            request.invitation.save()
            # envoyer un msg de traçabilité à la création
            from messagerie.models import Message
            Message.objects.creer_et_envoyer('tracabilite', None, [user], 'invitation d\'inscription',
                                             'Bienvenue sur la plateforme ManifestationSportive.'
                                             f'Vous avez été invité par {request.invitation.origine_fk} '
                                             f'à la date du {request.invitation.date_creation}')
        # ajout de la table des options
        op = OptionUser.objects.filter(user=user)
        if not op:
            option = OptionUser(user=user)
            option.save()
        return user
