from django.forms import ModelForm
from core.models.optionuser import OptionUser
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field


class OptionUserForm(ModelForm):
    """
    formulaire pour changer les options de notifications
    """
    class Meta:
        model = OptionUser
        fields = ['conversation_msg', 'nouveaute_msg', 'action_msg', 'notification_msg', 'conversation_mail', 'forum_msg',
                  'nouveaute_mail', 'action_mail', 'notification_mail', 'conversation_push', 'nouveaute_push', 'forum_mail',
                  'action_push', 'notification_push', 'tracabilite_msg', 'tracabilite_mail', 'tracabilite_push', 'forum_push']

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn-primary'))
    helper.form_method = 'POST'