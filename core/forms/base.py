# coding: utf-8
from crispy_forms.helper import FormHelper
from django import forms


class GenericForm(forms.ModelForm):
    """ Formulaire de base """

    def __init__(self, *args, **kwargs):
        """ Initialiser le formulaire """
        super(GenericForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'

    def ajouter_css_class_requis_aux_champs_requis(self):
        """ Ajoute une classe css nommée 'requis' à tous les champs obligatoires """
        for cle in self.instance.get_liste_champs_requis():
            field = self.fields[cle]
            css = field.widget.attrs.get('class', '')
            field.widget.attrs['class'] = 'requis ' + css

    def ajouter_css_class_modifier_aux_champs_demandes(self, modele="manif"):
        """
        Ajoute une classe css nommée 'modifier' à tous les champs demandés à modifier par l'instructeur
        """
        liste_champs_a_modifier = []
        manif = getattr(self.instance, 'manif', self.instance)
        for formulaire, champ, date in manif.get_cerfa().get_liste_champs_a_modifier():
            if formulaire == modele:
                if modele == "manif" and champ.startswith('ville_depart_interdep'):
                    champ = '_'.join(champ.split('_')[:-1])
                if modele == "n2k" and champ in ['pietinement', 'emissions_sonores', 'emissions_lumiere', 'pollution_eau',
                                                 'pollution_terre', 'sensibilisation', 'emprise_amenagement', 'public',
                                                 'parking', 'engins_aeriens', 'cours_eau', 'ravitaillement', 'balisage']:
                    # Inversion du champ
                    champ = champ + "_"
                liste_champs_a_modifier.append(champ)

        for cle in self.fields:
            field = self.fields[cle]
            if cle in liste_champs_a_modifier:
                css = field.widget.attrs.get('class', '')
                field.widget.attrs['class'] = 'modifier ' + css
            else:
                field.disabled = True
