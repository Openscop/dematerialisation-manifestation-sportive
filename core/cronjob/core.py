import logging
import os

from datetime import timedelta
from django_cron import CronJobBase, Schedule
from django_cron.models import CronJobLog
from django.conf import settings
from django.utils import timezone

from core.models import User, ConfigurationGlobale
from instructions.models import PieceJointeAvis, DocumentOfficiel, Instruction
from messagerie.models import Enveloppe
from evenements.models import Manif, PieceJointe
from core.tasks import gestion_fichier, creation_thumbnail_pj_avis, creation_thumbnail_doc_officiel, pj_messagerie
from post_office.models import Email
from allauth.account.models import EmailAddress

mail_logger = logging.getLogger('smtp')


class RelanceScanAntivirus(CronJobBase):
    """
    A cause d'erreur reseau certains appel de scan de fichier n'ont pas lieu.
    Le cron relancera les appels pour les pj de moins de 5 minutes (pour eviter le doublons)
    """
    RUN_EVERY_MINS = 60  # à lancer toutes les heures

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'core.relancescanantivirus'

    def do(self):
        
        if not ConfigurationGlobale.objects.get(pk=1).RelanceScanAntivirus:
            return 'Cron désactivé'
        CronJobLog.objects.filter(code="core.relancescanantivirus").filter(is_success=True).filter(
            end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()
        count = 0
        pjs = PieceJointe.objects.filter(scan_antivirus=False, date_televersement__lte=(timezone.now()-timezone.timedelta(minutes=5)))
        for pj in pjs:
            gestion_fichier.delay(pj.manif.pk, pj.pk)
            count += 1
        pja_s = PieceJointeAvis.objects.filter(scan_antivirus=False)
        for pj in pja_s:
            creation_thumbnail_pj_avis.delay(pj.pk)
            count += 1
        pjdoc_s = DocumentOfficiel.objects.filter(scan_antivirus=False)
        for pj in pjdoc_s:
            creation_thumbnail_doc_officiel.delay(pj.pk)
            count += 1
        from messagerie.models import Message
        msg = Message.objects.filter(scan_antivirus=False)
        for pj in msg:
            manif = pj.message_enveloppe.first().manifestation
            pj_messagerie.delay(manif.pk, pj.pk)
            count += 1
        return str(count)


class DeleteFlagFile(CronJobBase):
    """
    Preview Manager n'arrive parfois pas à traiter des fichiers, il laisse des fichiers .flag derrière lui
    Ce qui peut le faire planter au second passage et remplir inutilement le dossier.
    Ce cron nettoie donc le dossier media des fichiers flag qui s'y trouve.
    """
    RUN_EVERY_MINS = 60  # à lancer toutes les heures

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'core.deleteflagfile'

    def do(self):
        
        if not ConfigurationGlobale.objects.get(pk=1).DeleteFlagFile:
            return 'Cron désactivé'
        # Purger les logs
        CronJobLog.objects.filter(code="core.deleteflagfile").filter(is_success=True).filter(
            end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()
        if not os.path.exists('/tmp/verrou_deleteflagfile'):
            os.system("touch /tmp/verrou_deleteflagfile")
            os.system('cd ' + settings.MEDIA_ROOT + ' && find . -name "*.*_flag" -type f -delete')
            os.system('rm /tmp/verrou_deleteflagfile')


class DeleteExportationFile(CronJobBase):
    """
    Ici on va supprimer le dossier des fichiers temporaires de l'exportations
    """
    RUN_EVERY_MINS = 60  # à lancer toutes les heures

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'core.deleteexportationfile'

    def do(self):
        
        if not ConfigurationGlobale.objects.get(pk=1).DeleteExportationFile:
            return 'Cron désactivé'
        # Purger les logs
        CronJobLog.objects.filter(code="core.deleteexportationfile").filter(is_success=True).filter(
            end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        os.system('rm ' + settings.MEDIA_ROOT + 'exportation/* -R')


class EmailAdresseValide(CronJobBase):
    """
    Clore les comptes qui :
    - n'ont aucune adresse validée
    - sont organisateur
    - ne sont associé à aucune manif
    - n'ont aucun message
    - ce sont inscrit il y a plus de 15 jours
    """
    RUN_AT_TIME = ['6:25']  # à lancer à 6h25

    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'core.emailadressevalide'    # a unique code

    def do(self):
        
        if not ConfigurationGlobale.objects.get(pk=1).EmailAdresseValide:
            return 'Cron désactivé'
        # Purger les logs sans message
        CronJobLog.objects.filter(code="core.emailadressevalide").filter(is_success=True).filter(
            message__isnull=True).filter(end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        effacer = '' # chaine de caractères renvoyer avec la liste des comptes supprimés
        for user in User.objects.all():
            enveloppes = Enveloppe.objects.filter(destinataire_id=user).exclude(type='news')
            manif = Manif.objects.filter(declarant=user)
            email = user.emailaddress_set.filter(verified=True)
            delta = timezone.now() - timezone.timedelta(days=15)
            if not enveloppes and not manif and not email and hasattr(user, 'organisateur') and user.date_joined < delta:
                effacer += 'Username : ' + str(user.username) + ' | Prénom : ' + str(user.first_name) + ' | Nom : ' + str(user.last_name) + ' | pk : ' + str(user.pk) + chr(10)
                user.delete()
        return effacer


class RenvoyerConfirmationMail(CronJobBase):


    RUN_AT_TIME = ['5:30']
    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'core.renvoyerconfirmationmail'  # unique code

    def do(self):

        if not ConfigurationGlobale.objects.get(pk=1).RenvoyerConfirmationMail:
            return 'Cron désactivé'
        # Purger les logs sans message
        CronJobLog.objects.filter(code="core.renvoyerconfirmationmail").filter(is_success=True).filter(
            message__isnull=True).filter(end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        count = 0
        emails_failed = Email.objects.filter(created__gte=timezone.now() - timedelta(days=1), status=1,
                                             subject="[Manifestation Sportive] Confirmer l'adresse email").distinct('to')

        for email in emails_failed:  # on parcourt la liste des emails
            email_exist = EmailAddress.objects.filter(
                email=email.to[0]).exists()  # vérification que l'email existe en BDD
            if email_exist:  # si l'email existe
                email_cible = EmailAddress.objects.filter(email=email.to[0]).first()  # récupération de l'adresse mail
                if not email_cible.verified:  # dans le cas ou le mail n'est pas vérifié
                    email_cible.send_confirmation()  # renvoyer un mail de confirmation aux utilisateurs
                    count += 1  # incrémentation du compteur pour indiquer le nombre d'emails renvoyés

        return f"{count} email envoyé"


class UpdateStat(CronJobBase):
    """
    Mise à jour des compteurs de la page d'accueil
    """
    RUN_AT_TIME = ['0:00']  # à lancer toutes les heures

    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'core.updatestat'

    def do(self):

        if not ConfigurationGlobale.objects.get(pk=1).UpdateStat:
            return 'Cron désactivé'
        CronJobLog.objects.filter(code="core.updatestat").filter(is_success=True).filter(
            end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()
        C = ConfigurationGlobale.objects.get(pk=1)
        manifs = Manif.objects.filter(date_debut__gte=(timezone.now()-timezone.timedelta(days=365)))
        C.stat_cache_nb_instruction = Instruction.objects.filter(manif__in=manifs).count()
        C.stat_cache_nb_feuille_eco = 611 * len(manifs) # le 611 est la moyenne de feuille total (nombre de feuille dans l'export * nombre de services ayant eu le dossier)
        km_parcouru = 0
        participant = 0
        for manif in manifs:
            participant += manif.nb_participants
            for parcour in manif.parcours.all():
                km_parcouru += round(parcour.length_in_meter) / 1000
        C.stat_cache_nb_participant = participant
        C.stat_cache_nb_km_parcouru = km_parcouru
        C.save()
        return ""