import datetime
from django.utils.timezone import make_aware
from django_cron import CronJobBase, Schedule
from core.models import User, ConfigurationGlobale

from messagerie.models import Message
from post_office.models import Email, Log
from structure.models.organisation import Organisation
from datetime import timedelta


class NotificationPbEmail(CronJobBase):
    """
    Notifier les administrateurs d'instance
    de la non déliverabilité d'un email.
    Cette class traite les emails envoyés par
    post_office qui sont en statut "failed"

    """
    RUN_AT_TIME = ['6:25']  # à lancer à 6 h 25

    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'core.notificationpbemail'

    def do(self):

        if not ConfigurationGlobale.objects.get(pk=1).NotificationPbEmail:
            return 'Cron désactivé'
        name = None
        instance = None
        detail = None

        # Récupérer les emails qui ont le statut failed
        emails_failed = Email.objects.filter(created__gte=make_aware(datetime.datetime.now()) - timedelta(days=1))\
            .filter(status=1).distinct('to')

        if emails_failed:
            for email in emails_failed:
                user = User.objects.filter(email=email.to[0]).first()
                organisation = Organisation.objects.filter(email_s=email.to[0]).first()

                if user:
                    name = f"de {user.get_full_name()}"
                    instance = user.get_instance()
                elif organisation:
                    instance = organisation.instance_fk
                    name = f"de l'organisation : {organisation}"

                # Récupérer log pour identifier le message d'erreur
                log_email = Log.objects.filter(email=email.id).last()
                message_log = log_email.message

                # Anti spam
                if "550, b'5.7.1" in message_log or "421, b'4.7.0" in message_log:
                    detail = "L'adresse email dispose d'un anti-spam qui bloque nos mails."

                # Erreur dans l'adresse email ou nom de domaine du destinataire
                elif "535, b'5.7.8" in message_log or "550, b'5.1.2" in message_log:
                    detail = "L'adresse email ou le serveur d'hôte est introuvable."

                # L'adresse email reçoit trop de messages
                elif "450, b'4.7.1" in message_log:
                    detail = "L'adresse email reçoit du courrier à un débit qui empêche la livraison de mails " \
                             "supplémentaires."

                # Erreur avec serveur DNS
                elif "Temporary failure in name resolution" in message_log:
                    detail = "Ce problème signifie généralement un problème avec le serveur DNS."

                # Erreur avec port SMTP
                elif "Connection timed out" in message_log:
                    detail = "Ce problème signifie que l'utilisateur utilise un port smtp bloqué."

                # Erreur d'authentification
                elif "454, b'4.7.0" in message_log:
                    detail = "Ce problème se produit si le serveur Exchange ne peut pas s'authentifier auprès du " \
                             "serveur Exchange distant."

                elif "451, b'4.3.0" in message_log:
                    detail = "Le mail a temporairement été rejeté par le serveur de messagerie."

                # Remplir le contenu du message
                destinataires = User.objects.filter(groups__name="Administrateurs d'instance",
                                                    default_instance=instance)

                titre = "Demande d'action"

                contenu = f"Bonjour, \n\n" \
                          f"La boîte mail {name}, {email.to[0]}, semble rencontrer un problème :\n\n" \
                          f"{detail}\n\n" \
                          f"Les emails qui lui sont envoyés n'aboutissent pas.\n" \
                          f"Merci de faire le nécessaire afin de régler le problème.\n\n" \
                          f"Cordialement,\n\n" \
                          f"L'équipe support."

                # Envoyer le message
                Message.objects.creer_et_envoyer(type_msg='action', expediteur=None, destinataires=destinataires,
                                                 titre=titre, contenu=contenu)