import imaplib
import re
from django_cron import CronJobBase, Schedule
from core.models import User, ConfigurationGlobale
from core.models import ErrorSMTP
from core.models import Instance
from messagerie.models import Message

from structure.models.organisation import Organisation
from collections import Counter
from datetime import timedelta
from configuration.settings import MANIF_COMPTE_EMAIL_PASSWORD, MANIF_COMPTE_EMAIL
import datetime


class ErreurSMTPEmail(CronJobBase):
    """
    Notifier les administrateurs d'instance
    Contexte : Il arrive que l'envoi de nos emails
    soient correct (pas d'erreur SMTP) mais que finalement
    nous ayons un retour par email de non déliverabilité.
    Exemple : besoin de validation d'un anti-spam tel que Mailinblack
    Cette class traite tous ces cas-là en analysant les emails
    reçus dans la boîte "plateforme@manifestationsportive.fr"
    """

    RUN_AT_TIME = ['6:25']  # à lancer à 6 h 25

    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'core.erreursmtpemail'

    def do(self):

        if not ConfigurationGlobale.objects.get(pk=1).ErreurSMTPEmail:
            return 'Cron désactivé'
        if not MANIF_COMPTE_EMAIL_PASSWORD or not MANIF_COMPTE_EMAIL:
            return ''
        erreur = None
        detail = None
        date = (datetime.date.today() - timedelta(days=1)).strftime("%d-%b-%Y")

        # Connexion à la boîte mail
        boite_mail = imaplib.IMAP4_SSL('mail.gandi.net')
        boite_mail.login(MANIF_COMPTE_EMAIL, MANIF_COMPTE_EMAIL_PASSWORD)

        # Liste des dossiers et sous dossiers de la boîte mail
        list_boites = boite_mail.list()

        # Tableau vide où l'on va ajouter chaque sous dossier trouvé dans list_boites
        tab_sous_dossier = []

        for boite in list_boites[1]:
            # Convertir octets en chaines de caractères
            boite_decode = boite.decode('utf-8')

            # Correspond par exemple à "inbox/42"
            recherche_inbox = re.findall(r'INBOX/\d{1,3}', boite_decode)
            if not recherche_inbox:
                # Correspond seulement à "inbox"
                recherche_inbox = re.findall(r'INBOX$', boite_decode)

            # Si un sous dossier a été trouvé...
            if recherche_inbox:
                # ... et qu'il n'est pas déjà dans le tableau
                if recherche_inbox not in tab_sous_dossier:
                    tab_sous_dossier.append(recherche_inbox)

                    # inbox correspond au dernier sous dossier ajouté au tableau
                    inbox = tab_sous_dossier[-1][0]
                    boite_mail.select(inbox, readonly=True)

                    # Récupérer tous les mails d'un sous dossier
                    emails = boite_mail.search(None, 'all', f'(SINCE "{date}")')
                    list_emails = emails[1][0].split(b' ')

                    if len(list_emails) > 1:
                        for email_recu in list_emails:
                            typ, data = boite_mail.fetch(email_recu, '(RFC822)')
                            contenu = ''.join(data[0][1].decode('ISO-8859-1').splitlines()).replace("=", "")

                            # On exclut du contenu du mail les adresses mails non pertinentes connues
                            adresses = \
                                re.findall(r'[\w.+-]+@(?!manifestationsportive.fr)(?!invitations.mailinblack.com)'
                                           r'(?!mib\d.mailinblack.com)(?!mib\d{2}.mailinblack.com)(?!antispam.xefi.fr)'
                                           r'(?!antispam\d.xefi.fr)(?!spamenmoins.net)(?![\w-]+\.spamenmoins.net)'
                                           r'(?![\w-]+\.spamsenmoins.com)(?!opme[\w-]+\.[\w.-])(?!legalopromain.org)'
                                           r'(?!bounce.email.vimeo.com)(?!antispam.[\w-]+\.[\w.-])'
                                           r'(?!spamsenmoins.com)(?!asp\d.mailinblack.com)(?!asp\d{2}.mailinblack.com)'
                                           r'(?!mtain\d.ac-lyon.fr)(?!reseaunance.mailinblack.com)(?!smdl.evoliatis.fr)'
                                           r'(?!relay\d-d.mail.gandi.net)(?!relay\d{2}-d.mail.gandi.net)'
                                           r'(?!storec.in.ac-versailles.fr)(?!nmboxes\d{3}.sd4.0x35.net)'
                                           r'(?!relay\d.mail.gandi.net)(?!relay\d{2}.mail.gandi.net)'
                                           r'(?!interieur.gouv.fr)(?!mib.[\w-]+\.[\w.-])[\w-]+\.[\w.-]+', contenu)

                            # S'il y a au moins une adresse
                            if adresses:
                                # On compte le nombre de fois où une adresse est trouvée
                                occurence_count = Counter(adresses)

                                # L'email correspond à l'adresse qui ressort le plus
                                email = occurence_count.most_common(1)[0][0]

                                user = User.objects.get(email=email)
                                service = Organisation.objects.get(email_s=email)
                                if user:
                                    instance = Instance.objects.get_for_user(user)
                                else:
                                    instance = service.instance_fk

                                # Boîte mail inexistante ou bloquée
                                if "550 5.1.1" in contenu or "550 5.2.1" in contenu or "550 5.5.0" in contenu or \
                                        "552 5.0.0" in contenu:
                                    erreur = 'INC'
                                    detail = "L'adresse email n'existe pas ou a été bloquée."

                                # Anti spam
                                elif "550 5.7.1" in contenu or "554 5.7.1" in contenu or "spam Mailinblack" in contenu \
                                        or "spam MailInBlack" in contenu or "SpamEnMoins" in contenu or \
                                        "ad invitation" in contenu or "clean Mailbox" in contenu:
                                    erreur = 'SPA'
                                    detail = "L'adresse email dispose d'un anti-spam qui bloque nos mails."

                                # Changement d'adresse mail
                                elif "nouvelle adresse" in contenu or "changement adresse" in contenu:
                                    erreur = 'ADR'
                                    detail = "L'adresse email de cette personne n'est plus valide."

                                # Utilisateur absent
                                elif "absent" in contenu or "Absent" in contenu:
                                    erreur = 'ABS'
                                    detail = "La personne qui a reçu le mail est actuellement absente."

                                # Boîte mail pleine
                                elif "pleine" in contenu or "5.2.2" in contenu or "full" in contenu:
                                    erreur = 'PLE'
                                    detail = "La boite mail de cette personne est pleine."

                                # Problème hôte
                                elif "4.4.1" in contenu:
                                    erreur = 'HOT'
                                    detail = "Impossible de communiquer avec le serveur SMTP de destination."

                                # Création d'un log pour retrouver le problème
                                if user and service:
                                    log = ErrorSMTP(email=email, user=user, service=service,
                                                    type_erreur=erreur, contenu_email=contenu, instance=instance)
                                elif user:
                                    log = ErrorSMTP(email=email, user=user, type_erreur=erreur,
                                                    contenu_email=contenu, instance=instance)
                                elif service:
                                    log = ErrorSMTP(email=email, service=service, type_erreur=erreur,
                                                    contenu_email=contenu, instance=instance)
                                else:
                                    log = ErrorSMTP(email=email, type_erreur=erreur, contenu_email=contenu,
                                                    instance=instance)

                                # On sauvegarde le log
                                log.save()

                                # On vérifie si des logs sont enregistrés avec l'email
                                logs_email = ErrorSMTP.objects.filter(email=email)

                                # On traite les problèmes qu'au bout de la troisième apparition
                                if len(logs_email) == 3:

                                    # Remplir le contenu du message
                                    titre = "Demande d'action"

                                    if user:
                                        destinataires = User.objects.filter(groups__name="Administrateurs d'instance",
                                                                            default_instance=user.get_instance())
                                        detail_message = f"La boîte mail de {user}, {email}, semble " \
                                                         f"rencontrer un problème :\n\n" \
                                                         f"{detail}\n\n"

                                    elif service:
                                        destinataires = User.objects.filter(groups__name="Administrateurs d'instance",
                                                                            default_instance=service.instance_fk)
                                        detail_message = f"La boîte mail du service {service}, {email}, " \
                                                         f"semble rencontrer un problème :\n\n" \
                                                         f"{detail}\n\n"

                                    else:
                                        destinataires = User.objects.filter(groups__name="Administrateurs d'instance",
                                                                            default_instance=Instance.objects.get_master())
                                        detail_message = f"La boîte mail {email}, semble rencontrer un problème :\n\n" \
                                                         f"{detail}\n\n"

                                    lien = "<a href=\"https://manifestationsportive.fr/detail_log/{{ log.pk }}/\">" \
                                           "Cliquez ici</a>"

                                    message = f"Bonjour,\n\n" \
                                              f"{detail_message}" \
                                              f"Les emails qui lui sont envoyés n'aboutissent pas.\n" \
                                              f"Merci de faire le nécessaire afin de régler le problème.\n" \
                                              f"{lien} pour nous indiquer que le problème a été résolu.\n\n" \
                                              f"Cordialement,\n\n" \
                                              f"L'équipe support."

                                    # Envoyer le message
                                    Message.objects.creer_et_envoyer(type_msg='action', expediteur=None, titre=titre,
                                                                     destinataires=destinataires, contenu=message)

