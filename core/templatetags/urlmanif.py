from django import template

register = template.Library()


@register.filter()
def urlmanif(manif, service):
    """
    Ce tag nous permet de recuperer dans les templates l'url d'une manifestation selon l'utilisateur
    """
    url = manif.get_url_manif_associe(service)
    return url
