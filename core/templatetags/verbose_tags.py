# coding: utf-8
from django import template
from django.contrib.auth.models import Group

register = template.Library()


@register.filter
def verbose_name(obj):
    if obj:
        return obj._meta.verbose_name
    else:
        return ""


@register.filter
def verbose_attr(obj, attr):
    if obj:
        if hasattr(obj, attr):
            return obj._meta.get_field(attr).verbose_name
    return ""


@register.filter
def verbose_name_plural(obj):
    if obj:
        return obj._meta.verbose_name_plural
    else:
        return ""


@register.filter
def verbose_groupe(str):
    return Group.objects.get(id=str).name
