from django.template.defaulttags import register


@register.filter(is_safe=False)
def pluralizero(value, arg='s'):
    """
    Return a plural suffix if the value is not 0, '0', 1, '1', or an object of
    length 1. By default, use 's' as the suffix:

    * If value is 0, vote{{ value|pluralizero }} display "vote".
    * If value is 1, vote{{ value|pluralizero }} display "vote".
    * If value is 2, vote{{ value|pluralizero }} display "votes".

    If an argument is provided, use that string instead:

    * If value is 0, class{{ value|pluralizero:"es" }} display "classe".
    * If value is 1, class{{ value|pluralizero:"es" }} display "class".
    * If value is 2, class{{ value|pluralizero:"es" }} display "classes".

    If the provided argument contains a comma, use the text before the comma
    for the singular case and the text after the comma for the plural case:

    * If value is 0, cand{{ value|pluralizero:"y,ies" }} display "candie".
    * If value is 1, cand{{ value|pluralizero:"y,ies" }} display "candy".
    * If value is 2, cand{{ value|pluralizero:"y,ies" }} display "candies".
    """
    if ',' not in arg:
        arg = ',' + arg
    bits = arg.split(',')
    if len(bits) > 2:
        return ''
    singular_suffix, plural_suffix = bits[:2]

    try:
        return singular_suffix if float(value) < 2 else plural_suffix
    except ValueError:  # Invalid string that's not a number.
        pass
    except TypeError:  # Value isn't a string or a number; maybe it's a list?
        try:
            return singular_suffix if len(value) < 2 else plural_suffix
        except TypeError:  # len() of unsized object.
            pass
    return ''