from django import template

register = template.Library()


@register.filter
def movetocore(app_liste):
    """
    Transférer les modeles de admin et auth dans core
    Transférer les sous modeles du forum dans forum
    """
    if isinstance(app_liste, list):
        if len(app_liste) > 1:
            # Range dans Core
            models_admin, models_auth = [], []
            app_admin_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'admin'), None)
            app_auth_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'auth'), None)
            app_core_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'core'), None)
            if app_admin_index is not None:
                models_admin = app_liste[app_admin_index]['models']
            if app_auth_index is not None:
                models_auth = app_liste[app_auth_index]['models']
            if app_core_index is not None:
                app_liste[app_core_index]['models'] = sorted(app_liste[app_core_index]['models'] + models_admin + models_auth, key=lambda c: c['name'])
            if app_admin_index is not None:
                del app_liste[app_admin_index]
            app_auth_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'auth'), None)
            if app_auth_index is not None:
                del app_liste[app_auth_index]

            # Range le forum
            models_conversation, models_permissions, models_polls, models_member, models_tracking = [], [], [], [], []
            app_conversation_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'forum_conversation'), None)
            app_permission_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'forum_permission'), None)
            app_polls_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'forum_polls'), None)
            app_member_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'forum_member'), None)
            app_forum_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'forum'), None)
            app_forum_tracking = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'forum_tracking'), None)

            if app_conversation_index is not None:
                models_conversation = app_liste[app_conversation_index]['models']
            if app_permission_index is not None:
                models_permissions = app_liste[app_permission_index]['models']
            if app_polls_index is not None:
                models_polls = app_liste[app_polls_index]['models']
            if app_forum_tracking is not None:
                models_tracking = app_liste[app_forum_tracking]['models']
            if app_member_index is not None:
                models_member = app_liste[app_member_index]['models']

            if app_forum_index is not None:
                app_liste[app_forum_index]['models'] = sorted(app_liste[app_forum_index]['models']
                                                              + models_conversation
                                                              + models_polls
                                                              + models_member
                                                              + models_permissions
                                                              + models_tracking, key=lambda c: c['name'])

            if app_conversation_index is not None:
                del app_liste[app_conversation_index]
            app_permission_index = next(
                (index for (index, item) in enumerate(app_liste) if item['app_label'] == 'forum_permission'), None)
            if app_permission_index is not None:
                del app_liste[app_permission_index]
            app_member_index = next(
                (index for (index, item) in enumerate(app_liste) if item['app_label'] == 'forum_member'), None)
            if app_member_index is not None:
                del app_liste[app_member_index]
            app_forum_tracking = next(
                (index for (index, item) in enumerate(app_liste) if item['app_label'] == 'forum_tracking'), None)
            if app_forum_tracking is not None:
                del app_liste[app_forum_tracking]
            app_polls_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'forum_polls'), None)
            if app_polls_index is not None:
                del app_liste[app_polls_index]

        return app_liste
    else:
        return app_liste
