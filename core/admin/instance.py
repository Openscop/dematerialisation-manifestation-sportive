# coding: utf-8
from django.contrib import admin
from django.utils.html import format_html
from django.urls import reverse

from core.models.instance import Instance


@admin.register(Instance)
class InstanceAdmin(admin.ModelAdmin):
    """ Administration des instances de configuration """

    # Configuration
    list_display = ['departement', 'name', 'etat_s', 'instruction_mode', 'activer_beta', 'acces_arrondissement']
    list_filter = ['instruction_mode', 'etat_s', 'activer_beta']
    ordering = ('departement',)
    search_fields = ['name__unaccent']
    fields = ['name', 'departement', 'etat_s', 'activer_beta', 'message', 'instruction_mode', 'depot_hors_delai',
              'acces_arrondissement', 'sender_email', 'email_settings', "envoi_mail_test",
              'avis_delai_federation', 'avis_delai_relance',
              'reunion_cdsr_delai_relance', 'convocation_cdsr_delai_relance']
    list_per_page = 25
    readonly_fields = ("envoi_mail_test",)

    def envoi_mail_test(self, obj):
        print(obj.pk)
        if obj.pk:
            url = reverse("verif_mail_instance", args=[obj.pk])
            return format_html('<a class="btn" href="{}">Envoyer un mail de test</a>', url)
        return '-'
    envoi_mail_test.short_description = "Mail de test"
