# coding: utf-8
from smtplib import SMTPServerDisconnected
from copy import deepcopy

from django import forms
from django.core.mail.message import SafeMIMEText
from django.urls import reverse
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from django.contrib.admin.models import LogEntry
from django.contrib.flatpages.admin import FlatPageAdmin, FlatpageForm as BaseFlatpageForm
from django.contrib.flatpages.models import FlatPage

from django_cron.models import CronJobLog
from django_cron.admin import CronJobLogAdmin
from allauth.account.models import EmailAddress
from import_export.admin import ExportActionModelAdmin
from ckeditor_uploader.fields import RichTextUploadingFormField
from post_office.admin import EmailAdmin
from post_office.models import Email

from core.admin.filters import LogEntryInstanceFilter
from core.models import User, Instance, OptionUser, LogConsult, ConfigurationGlobale
from core.forms import CustomUserChangeForm, CustomUserCreateForm
from messagerie.models import CptMsg
from forum.forum_conversation.models import ForumAbonnement
from forum.forum.models import Forum
from structure.models import ServiceConsulte, ServiceInstructeur


class FlatpageForm(BaseFlatpageForm):
    """ Formulaire admin des flat pages """

    # Initialiser
    def __init__(self, *args, **kwargs):
        return super().__init__(*args, **kwargs)

    # Champs
    content = RichTextUploadingFormField()


class FlatPageAdmin(FlatPageAdmin):
    """ Administration des flatpages """

    # Configuration
    form = FlatpageForm


class OptionUserInline(admin.TabularInline):
    model = OptionUser
    can_delete = False

    fieldsets = (
        (None,
         {'fields': ('conversation_msg', 'nouveaute_msg', 'action_msg', 'forum_msg', 'notification_msg', 'tracabilite_msg',
                     'conversation_mail', 'nouveaute_mail', 'action_mail', 'forum_mail', 'notification_mail', 'tracabilite_mail',
                     'conversation_push', 'nouveaute_push', 'action_push', 'forum_push', 'notification_push', 'tracabilite_push', )}),
    )


class CPTInline(admin.TabularInline):
    model = CptMsg
    can_delete = False
    max_num = 1
    fieldsets = (
        (None,
         {'fields': ('convocation', 'action', 'info_suivi', 'conversation', 'news', 'forum', )}),
    )
    readonly_fields = ('convocation', 'action', 'info_suivi', 'conversation', 'news', 'forum', )


class EmailInline(admin.TabularInline):
    model = EmailAddress

    fieldsets = (
        (None,
         {'fields': ('email', 'verified', 'primary')}),
    )

    def get_fieldsets(self, request, obj=None):
        if request.user.has_group('Administrateurs d\'instance') or request.user.has_group('_support'):
            return ((None, {'fields': ('email', 'verified', 'primary', "renvoi_mail")}), )
        else:
            return ((None, {'fields': ('email', 'verified', 'primary')}), )

    def get_readonly_fields(self, request, obj=None):
        if request.user.has_group('Administrateurs d\'instance') or request.user.has_group('_support'):
            return ("renvoi_mail",)
        else:
            return ()

    # renvoi d'un mail de confirmation
    def renvoi_mail(self, obj):
        if obj.pk and not obj.verified:
            pk = obj.user.pk
            url = reverse("send_confirm_mail", args=[pk])
            return format_html('<a class="btn" href="{}">Renvoyer le mail de confirmation</a>', url)
        return '-'
    renvoi_mail.short_description = "Mail confirmation"

    def get_extra(self, request, obj=None, **kwargs):
        return 1


def supprimer_utilisateur_a_un_groupe(modeladmin, request, queryset):
    """ Ajoute une action pour supprimer des utilisateurs d'un groupe"""
    groupname = request.POST['nomGroupe']
    group = Group.objects.get(name=groupname)
    for user in queryset:
        group.user_set.remove(user)
        group.save()


supprimer_utilisateur_a_un_groupe.short_description = "Supprimer les utilisateurs sélectionnés d'un groupe..."


def ajouter_utilisateur_a_un_groupe(modeladmin, request, queryset):
    """ Ajoute une action pour ajouter des utilisateurs à un groupe"""
    groupname = request.POST['nomGroupe']
    group = Group.objects.get(name=groupname)
    for user in queryset:
        group.user_set.add(user)
        group.save()


ajouter_utilisateur_a_un_groupe.short_description = "Ajouter les utilisateurs sélectionnés dans un groupe..."


class UtilisateurAdmin(ExportActionModelAdmin, UserAdmin):
    """ Configuration de l'admin utilisateur """
    form = CustomUserChangeForm
    add_form = CustomUserCreateForm
    # Configuration
    list_display = ('agent_name', 'email', 'is_active', 'is_staff', 'date_joined', 'default_instance', 'get_service')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups', 'default_instance')
    readonly_fields = ('last_change_password', 'lien_enveloppe', 'list_organisation',)
    actions = ExportActionModelAdmin.actions + [ajouter_utilisateur_a_un_groupe, supprimer_utilisateur_a_un_groupe, 'export_email']
    ordering = ('-date_joined',)
    inlines = [EmailInline, OptionUserInline, CPTInline]
    fieldsets = (
        (None, {'fields': ('username', 'password', 'last_change_password',  ('default_instance'), ('list_organisation'))}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'lien_enveloppe', 'recap')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    list_per_page = 25

    def export_email(self, request, queryset):
        list_email = [p.email for p in queryset.all()]
        self.message_user(request,
                          mark_safe("%s adresses email trouvées : <br> %s" % (len(list_email), "<br>".join(list_email)))
                          )
    export_email.short_description = "Exporter les adresses email"

    def lien_enveloppe(self, user_obj):
        if user_obj:
            link = reverse("admin:messagerie_enveloppe_changelist")
            return mark_safe('<a href="' + link + '?q=' + user_obj.username + '">Voir ses messages</a>')
        return '-'
    lien_enveloppe.short_description = "Voir les messages de l'utilisateur"
    lien_enveloppe.allow_tags = True

    def list_organisation(self, obj):
        html = "<p>"
        for organisation in obj.organisation_m2m.all():
            if organisation.is_structure_organisatrice():
                html += f"Structure organisatrice : <i>{organisation.nom_s}</i></br>"
            elif organisation.is_mairie():
                html += f"Mairie : <i>{organisation.nom_s}</br>"
            elif organisation.is_service_instructeur():
                html += f"Service instructeur : <i>{organisation.nom_s}</i></br>"
            elif organisation.is_federation():
                html += f"Fédération : <i>{organisation.nom_s}</i></br>"
            elif organisation.is_service_consulte():
                html += f"Service consulté : <i>{organisation.nom_s}</i></br>"
            else:
                html += f"<i>{organisation.nom_s}</i></br>"
        html += "</p>"
        return mark_safe(html)
    list_organisation.short_description = "Organisation de l'utilisateur"
    list_organisation.allow_tags = True

    def get_actions(self, request):
        actions = super().get_actions(request)
        choices = []
        groups = Group.objects.all()
        for group in groups:
            if request.user.has_group("_support"):
                choices.append((group.name, group.name))
            elif request.user.has_group("Administrateurs d\'instance"):
                if group.name == "Instructeurs" or \
                        group.name == "Administrateur de sites protégés Réserve naturelle régionale" or \
                        group.name == "Administrateurs de sites protégés N2000":
                    choices.append((group.name, group.name))
        self.action_form.base_fields['nomGroupe'] = forms.ChoiceField(choices=choices, label='Nom du groupe :')
        return actions

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        # Les utilisateurs équipe ne peuvent voir que leur département
        if not request.user.has_group('_support'):
            queryset = queryset.filter(default_instance__departement=request.user.get_departement())
        return queryset

    def get_readonly_fields(self, request, obj=None):
        if request.user.has_group('_support'):
            return ('lien_enveloppe', 'list_organisation')
        else:
            return ('groups', 'lien_enveloppe', 'list_organisation')

    def save_model(self, request, obj, form, change):
        """ Enregistrer le modèle """
        try:
            instance = request.user.get_instance()
            if not obj.default_instance and not instance.is_master():
                obj.default_instance = instance
        except (AttributeError, Exception):
            pass
        if request.user.groups.filter(name="Administrateurs d'instance") and request.user.groups.filter(name="_support"):
            if obj == request.user and obj.default_instance != request.user.default_instance:
                old_i = ServiceInstructeur.objects.filter(instance_fk=request.user.default_instance, type_service_fk__nom_s="Préfecture")
                if not obj.default_instance.is_master():
                    new_i = ServiceInstructeur.objects.filter(instance_fk=obj.default_instance, type_service_fk__nom_s="Préfecture")
                    if old_i:
                        request.user.organisation_m2m.remove(old_i.first())
                    if new_i:
                        request.user.organisation_m2m.add(new_i.first())
                elif old_i:
                    request.user.organisation_m2m.remove(old_i.first())
        if obj.pk and "groups" in form.cleaned_data:
            group_support = Group.objects.get(name="_support")
            # Abonne les utilisateurs du groupe support à tous les sujets des forums et ajout l'user à l'organisation support
            if not obj.has_group('_support') and 'groups' in form.changed_data and group_support in form.cleaned_data["groups"]:
                forums = Forum.objects.all()
                abonnement_creer = 0
                for forum in forums:
                    abonnement, created = ForumAbonnement.objects.get_or_create(user=obj, forum=forum)
                    abonnement.tout = True
                    abonnement.save()
                    if created:
                        abonnement_creer += 1
                list(messages.get_messages(request))
                messages.add_message(request, messages.SUCCESS,
                                     '%s nouveaux abonnements crées pour cet utilisateur.' % str(abonnement_creer))
                o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
                obj.organisation_m2m.add(o_support)
            # Supprime les abonnements et l'organisation support si le groupe support est retiré
            elif obj.has_group('_support') and group_support not in form.cleaned_data["groups"]:
                forums = Forum.objects.all()
                abonnement_supprimer = 0
                for forum in forums:
                    abonnement = ForumAbonnement.objects.filter(user=obj, forum=forum, tout=True).first()
                    if abonnement:
                        abonnement.delete()
                        abonnement_supprimer += 1
                list(messages.get_messages(request))
                messages.add_message(request, messages.SUCCESS,
                                     '%s abonnements ont été supprimés.' % str(abonnement_supprimer))
                o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
                obj.organisation_m2m.remove(o_support)
            group_admin_instance = Group.objects.get(name="Administrateurs d'instance")
            # Abonnez les administrateur d'instance à tous les sujets des forums admin instance
            if not obj.has_group('Administrateurs d\'instance') and 'groups' in form.changed_data and group_admin_instance in form.cleaned_data["groups"]:
                forum_national = Forum.objects.get(pk=9)
                forum_departemental = forum_national.children.filter(departement=obj.default_instance.departement).first()

                abo_forum_national, created = ForumAbonnement.objects.get_or_create(user=obj, forum=forum_national)
                abo_forum_national.tout = True
                abo_forum_national.save()

                abo_forum_dep, created = ForumAbonnement.objects.get_or_create(user=obj, forum=forum_departemental)
                abo_forum_dep.tout = True
                abo_forum_dep.save()

                list(messages.get_messages(request))
                messages.add_message(request, messages.SUCCESS,
                                     'L\'utilisateur à été abonné aux forums d\'administrateur d\'instance.')
            # Supprime les abonnements si le groupe administrateur d'instance est retiré
            elif obj.has_group('Administrateurs d\'instance') and not group_admin_instance in form.cleaned_data["groups"]:
                forum_national = Forum.objects.get(pk=9)
                forum_departemental = forum_national.children.filter(departement=obj.get_departement()).first()
                abo_forum_national = ForumAbonnement.objects.filter(user=obj, forum=forum_national)
                abo_forum_dep = ForumAbonnement.objects.filter(user=obj, forum=forum_departemental)
                abo_forum_national.delete()
                abo_forum_dep.delete()
                list(messages.get_messages(request))
                messages.add_message(request, messages.SUCCESS,
                                     'L\'utilisateur à été désabonné des forums d\'administrateur d\'instance.')

        if obj.pk and 'is_active' in form.changed_data and not form.cleaned_data['is_active']:
            form.cleaned_data['groups'] = ""
            obj.groups.remove()
        return super().save_model(request, obj, form, change)

    def get_form(self, request, obj=None, **kwargs):
        """ Empêcher les non superuser de modifier les permissions des utilisateurs """
        form = super().get_form(request, obj, **kwargs)
        if request.user.get_departement():
            if hasattr(form.base_fields, 'default_instance'):
                form.base_fields['default_instance'].queryset = Instance.objects.filter(departement=request.user.get_departement())
                form.base_fields['default_instance'].initial = request.user.get_instance()
        return form

    def get_fieldsets(self, request, obj=None):
        """Custom override to exclude fields"""
        fieldsets = deepcopy(super().get_fieldsets(request, obj))

        # Append excludes here instead of using self.exclude.
        # When fieldsets are defined for the user admin, so self.exclude is ignored.
        exclude = ()

        if not request.user.is_superuser:
            exclude += ('user_permissions', 'is_superuser')

        # Iterate fieldsets
        for fieldset in fieldsets:
            fieldset_fields = fieldset[1]['fields']

            # Remove excluded fields from the fieldset
            for exclude_field in exclude:
                if exclude_field in fieldset_fields:
                    fieldset_fields = tuple(field for field in fieldset_fields if field != exclude_field)  # Filter
                    fieldset[1]['fields'] = fieldset_fields  # Store new tuple

        return fieldsets

    def agent_name(self, user_obj):
        return user_obj.get_full_name_and_username()
    agent_name.short_description = "Nom complet et pseudo"

    def changelist_view(self, request, extra_context=None):
        """surcharge pour ajouter un log"""
        response = super().changelist_view(request, extra_context)
        LogConsult.objects.create_from_list(response, request)
        return response

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response


class CustomCronJobLogAdmin(CronJobLogAdmin):
    list_display = ('code', 'start_time', 'end_time', 'humanize_duration', 'is_success', 'extrait')

    def extrait(self, obj):
        return obj.message[0:20]


@admin.register(OptionUser)
class OptionUserAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(LogEntry)
class LogEntryAdmin(admin.ModelAdmin):

    list_display = ('action_time', 'user', 'content_type', 'object_repr', 'action_flag', 'change_message')
    search_fields = ('user__username', 'content_type__model', 'object_repr', 'change_message')
    date_hierarchy = 'action_time'
    readonly_fields = ('action_time', 'user', 'content_type', 'object_repr', 'action_flag', 'change_message')
    fields = ('action_time', 'user', 'content_type', 'object_repr', 'action_flag', 'change_message')
    list_filter = ('action_flag', LogEntryInstanceFilter)

    def get_queryset(self, request):
        queryset = super(LogEntryAdmin, self).get_queryset(request)
        if not request.user.has_group('_support'):
            return queryset.filter(user__default_instance=request.user.get_instance())
        return queryset


@admin.register(LogConsult)
class LogConsultAdmin(admin.ModelAdmin):

    list_display = ('date', 'user', "model", 'type', "pk_affiche")
    search_fields = ('user__username', "model", 'pk_affiche')
    date_hierarchy = 'date'
    readonly_fields = ('date', 'user', "model", 'type', "pk_affiche")
    fields = ('date', 'user', "model", 'type', "pk_affiche")
    list_filter = ('type', 'model', LogEntryInstanceFilter)

    def get_queryset(self, request):
        queryset = super(LogConsultAdmin, self).get_queryset(request)
        if not request.user.has_group('_support'):
            return queryset.filter(user__default_instance=request.user.get_instance())
        return queryset


class CustomEmailAdmin(EmailAdmin):
    list_display = ['truncated_message_id', 'from_email', 'to_display', 'shortened_subject', 'status', 'last_updated',
                    'scheduled_time', 'use_template']
    list_filter = ['status', 'template__language', 'template__name', "from_email", "to", "subject"]
    
    def get_fieldsets(self, request, obj=None):
        fields = ['from_email', 'to', 'cc', 'bcc', 'priority', ('status', 'scheduled_time')]
        if obj.message_id:
            fields.insert(0, 'message_id')
        fieldsets = [(None, {'fields': fields})]
        has_plaintext_content, has_html_content = False, False
        try:
            for part in obj.email_message().message().walk():
                if not isinstance(part, SafeMIMEText):
                    continue
                content_type = part.get_content_type()
                if content_type == 'text/plain':
                    has_plaintext_content = True
                elif content_type == 'text/html':
                    has_html_content = True
        except SMTPServerDisconnected:
            fieldsets.append(
                (_("Text Email"), {'fields': ['message', 'html_message']})
            )

        if has_html_content:
            fieldsets.append(
                (_("HTML Email"), {'fields': ['render_subject', 'render_html_body']})
            )
            if has_plaintext_content:
                fieldsets.append(
                    (_("Text Email"), {'classes': ['collapse'], 'fields': ['render_plaintext_body']})
                )
        elif has_plaintext_content:
            fieldsets.append(
                (_("Text Email"), {'fields': ['render_subject', 'render_plaintext_body']})
            )

        return fieldsets


@admin.register(ConfigurationGlobale)
class ConfigurationGlobaleAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Cronjob', {'fields': (
            'UpdateCPT', 'DeleteFichierExportation', 'DeleteFlagFile', 'DeleteExportationFile',
            'RelanceScanAntivirus', 'TraitementDossier', 'RelanceReunionCDSR', 'UpdateInvitation',
            'EmailAdresseValide', 'UpdateDelai', 'CompletudeDossier', 'RelanceAvis', 'RelanceAction',
            'SendRecap', 'ErreurSMTPEmail', 'NotificationPbEmail', 'RenvoyerConfirmationMail',)}),
        ("Activation de l'envoi d'emails pour les messages de : ", {'fields': (
            ('mail_conversation', 'mail_action', 'mail_suivi', 'mail_tracabilite',
             'mail_actualite', 'mail_forum'),)}),
        ("Stat : ", {'fields': (
            ('stat_cache_nb_instruction', 'stat_cache_nb_participant',
             'stat_cache_nb_feuille_eco', 'stat_cache_nb_km_parcouru'),)}),
    )


admin.site.unregister(Email)
admin.site.register(Email, CustomEmailAdmin)

admin.site.unregister(EmailAddress)

admin.site.unregister(CronJobLog)
admin.site.register(CronJobLog, CustomCronJobLogAdmin)

admin.site.unregister(FlatPage)

admin.site.register(User, UtilisateurAdmin)
admin.site.register(FlatPage, FlatPageAdmin)
