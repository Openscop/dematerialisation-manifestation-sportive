# coding: utf-8
from django.contrib.admin.filters import SimpleListFilter

from core.models import Instance


class LogEntryInstanceFilter(SimpleListFilter):
    title = "instance de l'utilisateur"
    parameter_name = 'instance'

    def lookups(self, request, model_admin):
        if not request.user.has_group('_support'):
            return None
        else:
            objets = Instance.objects.configured()
            return [(c.pk, str(c)) for c in objets]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(user__default_instance__pk=self.value())
        return queryset
