# coding: utf-8
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db import transaction
from messagerie.models import CptMsg

from core.util.security import protect_code


class Command(BaseCommand):
    """
    Création des compteurs de nombre de msg non lu des utilisateurs
    """
    def handle(self, *args, **options):
        """ Exécuter la commande """

        with transaction.atomic():
            for user in get_user_model().objects.all():
                op=CptMsg.objects.filter(utilisateur=user)
                if not op:
                    cpt=CptMsg(utilisateur=user)
                    cpt.save()

