# coding: utf-8
import csv

from django.core.management.base import BaseCommand
from django.db import transaction
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q

from administrative_division.models import Departement, Commune, Region
from administrative_division.models.departement import LISTE_DEPARTEMENT, LISTE_REGION
from administration.models import (Prefecture, CGD, Brigade, EDSR, GGD, CG, CGService, SDIS, CODIS, Compagnie, CIS,
                                   DDSP, Commissariat, UniteSpecialisee, Service)
from core.models import Instance, User
from evenements.models import Manif
from instructions.models import Avis, PreAvis, AutorisationAcces, Instruction
from organisateurs.models import Structure
from structure.models import StructureOrganisatrice, ServiceInstructeur, TypeService, CategorieService, ServiceConsulte, ServiceConsulteInteraction, Organisation
from structure.models import Federation as FederationNew
from sports.models import Federation, Discipline


def transfert_acces(ancien, nouveau):
    """
    ancien n'importe quel service
    nouveau organisation ou derivé
    """
    ct_ancien = ContentType.objects.get_for_model(ancien)
    for a in AutorisationAcces.objects.filter(content_type=ct_ancien, object_id=ancien.pk):
        a.organisation = nouveau
        a.save()


class Command(BaseCommand):
    """
    Copie de toutes les data v5 et v6
    A cause du polymorphisme et de toutes les fonctions necessaires on va le faire en mode commande et non migrations
    """
    def handle(self, *args, **options):
        """ Exécuter la commande """
        print("Choissez la portée de transfers (default 0 = toute)")
        print("0=>Tout, 1=>Organisateur, 2=>prefécture, 3=>Mairies, 4=>Gendarmerie, 5=>CG, 6=>Pompier, 7=>Police, 8=>Service simple, 9=>federation, 10=>Instructions")
        choix = str(input())
        choix = "0" if not choix else choix
        print(choix)
        with transaction.atomic():
            notification_email_json = {"quotidien_mail": True, "conversation_mail": False, "nouveaute_mail": False,
                                       "action_mail": False, "forum_mail": False, "notification_mail": False,
                                       "tracabilite_mail": False}

            # Creation des instances et liaison departement regions
            for d in Departement.objects.all():
                if not Instance.objects.filter(departement=d):
                    i = Instance(departement=d, name=d.get_name_display().split(' - ')[1])
                    i.save()
                dep = list(filter(lambda x: x[0] == d.name, LISTE_DEPARTEMENT))[0]
                if not dep[2]:
                    # cas où c'est un departement outre mer on créer une region cope de ce dep
                    nom_r = dep[1]
                else:
                    nom_r = list(filter(lambda x: x[0] == dep[2], LISTE_REGION))
                    nom_r = nom_r[0][1]
                r = Region.objects.filter(nom=nom_r)
                if not r:
                    r = Region(nom=nom_r)
                    r.save()
                else:
                    r = r.first()
                d.region = r
                d.save()
            # Création du service consulté neutre "Manifestation sportive"
            categorie_manif, created = CategorieService.objects.get_or_create(nom_s="Manifestation sportive")
            type_manif, created = TypeService.objects.get_or_create(nom_s="Manifestation sportive", abreviation_s="Manifestation sportive", categorie_fk=categorie_manif)
            s, created = ServiceConsulte.objects.get_or_create(email_s="", type_service_fk=type_manif, instance_fk=Instance.objects.get_master(),
                                notification_email_json={"quotidien_mail": False, "conversation_mail": False, "nouveaute_mail": False,
                                       "action_mail": False, "forum_mail": False, "notification_mail": False,
                                       "tracabilite_mail": False},
                                national_b=True)

            # création de la structure support
            categorie_support = CategorieService.objects.create(nom_s="_Support")
            type_support = TypeService.objects.create(nom_s="Support", categorie_fk=categorie_support, abreviation_s="Support")
            o = ServiceConsulte(email_s="", type_service_fk=type_support, instance_fk=Instance.objects.get_master(),
                             notification_email_json={"quotidien_mail": False, "conversation_mail": False,
                                                      "nouveaute_mail": False,
                                                      "action_mail": False, "forum_mail": False,
                                                      "notification_mail": False,
                                                      "tracabilite_mail": False},
                             national_b=True
                             )
            o.save()
            for u in User.objects.filter(groups__name="_support"):
                u.organisation_m2m.add(o)

            f = open('ListeTypeService.csv')
            spamreader = csv.DictReader(f, delimiter=';', quotechar='"')
            for row in spamreader:
                print(row)
                categorie = CategorieService.objects.get_or_create(nom_s=row["Catégorie"])
                a= TypeService.objects.get_or_create(nom_s=row['Nom'], abreviation_s=row["Abréviation"],
                                                         categorie_fk=categorie[0])
                print(a, a[0].pk)

            # structure
            if choix in ['0', '1']:
                count = 0
                total = Structure.objects.count()
                for a in Structure.objects.all():
                    count += 1
                    print(f"{count}/{total} Structures")
                    b = StructureOrganisatrice(precision_nom_s=a.name, adresse_s=a.address, telephone_s=a.phone,
                                               email_s=a.organisateur.user.email, site_web_s=a.website,
                                               type_organisation_s=a.type_of_structure.type_of_structure,
                                               instance_fk=a.commune.arrondissement.departement.instance,
                                               notification_email_json=notification_email_json)
                    b.save()
                    b.commune_m2m.add(a.commune)
                    a.organisateur.user.organisation_m2m.add(b)
                    for manif in a.manifs.all():
                        manif.structure_organisatrice_fk = b
                        manif.save()
                    transfert_acces(a, b)

            # préfecture et sous-prefecture
            if choix in ['0', '2']:
                categorie_prefectoral = CategorieService.objects.get(nom_s="Services de l'État")
                type_prefecture = TypeService.objects.get(nom_s="Préfecture", categorie_fk=categorie_prefectoral)
                type_sous_prefecture = TypeService.objects.get(nom_s="Sous-Préfecture", categorie_fk=categorie_prefectoral)

                count = 0
                total = Prefecture.objects.count()
                for p in Prefecture.objects.all():
                    count += 1
                    print(f"{count}/{total} Préfectures")
                    s = ServiceInstructeur(email_s=p.email,
                                           admin_service_famille_b=False, admin_utilisateurs_subalternes_b=False,
                                           autorise_rendre_avis_b=True,
                                           autorise_acces_consult_organisateur_b=True,
                                           notification_email_json=notification_email_json)
                    s.type_service_fk = type_sous_prefecture if p.sous_prefecture else type_prefecture
                    s.type_service_instructeur_s = "prefecture" if not p.sous_prefecture else "sousprefecture"
                    s.instance_fk = p.arrondissement.departement.get_instance()
                    s.save()
                    s.arrondissement_m2m.add(p.arrondissement)
                    for u in p.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(p, s)

            #  Mairie
            if choix in ['0', '3']:
                categorie_mairie = CategorieService.objects.get(nom_s="Mairie")
                type_mairie = TypeService.objects.get(nom_s="Mairie", categorie_fk=categorie_mairie)

                total = Commune.objects.count()
                count = 0
                for c in Commune.objects.all():
                    count += 1
                    print(f"{count}/{total} Mairies")
                    s = ServiceInstructeur(precision_nom_s=c.name, email_s=c.email,
                                           type_service_fk=type_mairie,
                                           instance_fk=c.arrondissement.departement.get_instance(),
                                           admin_service_famille_b=False, admin_utilisateurs_subalternes_b=False,
                                           porte_entree_avis_b=True,
                                           autorise_rendre_avis_b=True,
                                           autorise_acces_consult_organisateur_b=True,
                                           notification_email_json=notification_email_json)
                    s.type_service_instructeur_s = 'mairie'
                    s.save()
                    s.commune_m2m.add(c)
                    for u in c.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(c, s)

                    ct_c = ContentType.objects.get_for_model(c)
                    for a in Avis.objects.filter(content_type=ct_c, object_id=c.id):
                        a.service_consulte_fk = s
                        a.service_consulte_origine_fk = s
                        a.service_demandeur_fk = a.instruction.referent.organisation_m2m.first() if a.instruction.referent else None
                        a.save()

            # gendarmerie
            if choix in ["0", "4"]:
                print('création CGD')
                categorie_gendarmerie = CategorieService.objects.get(nom_s="Sécurité")
                type_cgd = TypeService.objects.get(nom_s="Compagnie de gendarmerie départementale",
                                                      abreviation_s="CGD", categorie_fk=categorie_gendarmerie)
                for c in CGD.objects.all():
                    s = ServiceConsulte(email_s=c.email,
                                        type_service_fk=type_cgd,
                                        instance_fk=c.commune.arrondissement.departement.get_instance(),
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json,
                                        old_model_json={"name": "CGD", "pk": c.pk})
                    s.save()
                    s.commune_m2m.add(c.commune)
                    for u in c.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(c, s)

                print('création unité spécialisé')
                type_bmo = TypeService.objects.get(nom_s="Brigade motorisée", abreviation_s="Bmo",
                                                      categorie_fk=categorie_gendarmerie)
                type_pmo = TypeService.objects.get(nom_s="Peloton Motorisé", abreviation_s="Pmo",
                                                      categorie_fk=categorie_gendarmerie)
                type_pa = TypeService.objects.get(nom_s="Peloton Autoroute", abreviation_s="PA",
                                                     categorie_fk=categorie_gendarmerie)
                type_bn = TypeService.objects.get(nom_s="Brigade nautique", abreviation_s="BN",
                                                     categorie_fk=categorie_gendarmerie)
                type_pgm = TypeService.objects.get(nom_s="Peloton de gendarmerie de montagne",
                                                      abreviation_s="PGM", categorie_fk=categorie_gendarmerie)
                type_pghm = TypeService.objects.get(nom_s="Peloton de gendarmerie de haute montagne",
                                                       abreviation_s="PGHM", categorie_fk=categorie_gendarmerie)
                type_unites = [type_bmo, type_pmo, type_pa, type_bn, type_pgm, type_pghm]
                for u in UniteSpecialisee.objects.all():
                    s = ServiceConsulte(email_s=u.email,
                                        instance_fk=u.commune.arrondissement.departement.get_instance(),
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json,
                                        old_model_json={"name": "Unité", "pk": u.pk})
                    s.type_service_fk = [genre for genre in type_unites if genre.abreviation_s.upper() == u.genre.upper()][0]
                    s.save()
                    s.commune_m2m.add(u.commune)
                    for us in u.get_service_users():
                        us.organisation_m2m.add(s)
                    transfert_acces(u, s)

                print('création Brigade')
                type_cob = TypeService.objects.get(nom_s="Communautés de Brigades", abreviation_s="COB",
                                                      categorie_fk=categorie_gendarmerie)
                type_bta = TypeService.objects.get(nom_s="Brigades Territoriales Autonomes", abreviation_s="BTA",
                                                      categorie_fk=categorie_gendarmerie)
                type_btc = TypeService.objects.get(nom_s="Brigades Territoriales de Contact", abreviation_s='BTC',
                                                      categorie_fk=categorie_gendarmerie)
                for b in Brigade.objects.all():
                    s = ServiceConsulte(email_s=b.email,
                                        instance_fk=b.commune.arrondissement.departement.get_instance(),
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json,
                                        old_model_json={"name": "Brigade", "pk": b.pk})
                    if b.kind == "cob":
                        s.type_service_fk = type_cob
                    elif b.kind == "bta":
                        s.type_service_fk = type_bta
                    else:
                        s.type_service_fk = type_btc
                    s_cgd = ServiceConsulte.objects.get(type_service_fk=type_cgd, commune_m2m=b.cgd.commune)
                    s.service_parent_fk = s_cgd
                    s.save()
                    s.commune_m2m.add(b.commune)
                    for u in b.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(b, s)

                print('création EDSR')
                type_edsr = TypeService.objects.get(nom_s="Escadron Départemental de Sécurité Routière",
                                                       abreviation_s="EDSR", categorie_fk=categorie_gendarmerie)
                for e in EDSR.objects.all():
                    s = ServiceConsulte(email_s=e.email,
                                        type_service_fk=type_edsr,
                                        instance_fk=e.departement.get_instance(),
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json,
                                        old_model_json={"name": "EDSR", "pk": e.pk})
                    s.save()
                    s.departement_m2m.add(e.departement)
                    for u in e.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(e, s)

                print('création GGD')
                type_ggd = TypeService.objects.get(nom_s="Groupement de Gendarmerie Départementale",
                                                      abreviation_s="GGD", categorie_fk=categorie_gendarmerie)
                for g in GGD.objects.all():
                    s = ServiceConsulte(email_s=g.email,
                                        type_service_fk=type_ggd,
                                        instance_fk=g.departement.get_instance(),
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json,
                                        old_model_json={"name": "GGD", "pk": g.pk})
                    s.save()
                    s.departement_m2m.add(g.departement)
                    for u in g.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(g, s)

                print("mise en place des circuits gendarmerie")
                for s_ggd in ServiceConsulte.objects.filter(type_service_fk=type_ggd):
                    instance = s_ggd.instance_fk
                    print(instance)
                    s_edsr = ServiceConsulte.objects.get(type_service_fk=type_edsr, instance_fk=instance)
                    s_cgds = ServiceConsulte.objects.filter(type_service_fk=type_cgd, instance_fk=instance)
                    s_unites = ServiceConsulte.objects.filter(type_service_fk__in=type_unites, instance_fk=instance)
                    if instance.workflow_ggd == 0:
                        s_edsr.porte_entree_avis_b = True
                        s_edsr.admin_service_famille_b = False
                        s_edsr.admin_utilisateurs_subalternes_b = False
                        s_edsr.save()
                        s_ggd.service_parent_fk = s_edsr
                        s_ggd.autorise_rendre_avis_b = True
                        s_ggd.save()
                        ServiceConsulteInteraction.objects.create(service_entrant_fk=s_edsr, service_sortant_fk=s_ggd,
                                                                  type_lien_s="adresser")
                        ct_edsr = ContentType.objects.get_for_model(EDSR.objects.get(pk=s_edsr.old_model_json['pk']))
                        for a in Avis.objects.filter(content_type=ct_edsr, object_id=s_edsr.old_model_json['pk'], etat__in=['formaté', 'rendu']):
                            a.service_consulte_fk = s_ggd
                            a.service_consulte_origine_fk = s_edsr
                            a.service_demandeur_fk = a.instruction.referent.organisation_m2m.first() if a.instruction.referent else None
                            a.save()
                            a.service_consulte_history_m2m.add(s_edsr)
                        for a in Avis.objects.filter(content_type=ct_edsr, object_id=s_edsr.old_model_json['pk']).exclude(etat__in=['formaté', 'rendu']):
                            a.service_consulte_fk = s_edsr
                            a.service_consulte_origine_fk = s_edsr
                            a.service_demandeur_fk = a.instruction.referent.organisation_m2m.first() if a.instruction.referent else None
                            a.save()
                        for s_cgd in s_cgds:
                            s_cgd.service_parent_fk = s_edsr
                            s_cgd.save()
                            ServiceConsulteInteraction.objects.create(service_entrant_fk=s_edsr, service_sortant_fk=s_cgd,
                                                                      type_lien_s='interroger')
                            ct_cgd = ContentType.objects.get_for_model(CGD.objects.get(pk=s_cgd.old_model_json['pk']))
                            for p in PreAvis.objects.filter(content_type=ct_cgd, object_id=s_cgd.old_model_json['pk']).order_by('pk'):
                                a = Avis(etat=p.etat, date_demande=p.date_demande, date_reponse=p.date_reponse,
                                         favorable=p.favorable, prescriptions=p.prescriptions, instruction=p.avis.instruction,
                                         agent=p.agentlocal, service_consulte_fk=s_cgd, service_demandeur_fk=s_edsr,
                                         service_consulte_origine_fk=s_cgd, avis_parent_fk=p.avis, ancien_preavis=str(p.pk))
                                if p.preavis_origine:
                                    a.avis_origine = Avis.objects.get(ancien_preavis=str(p.preavis_origine.pk))
                                    a.raison_redemande = p.raison_redemande
                                a.save()
                        for s_unite in s_unites:
                            s_unite.service_parent_fk = s_edsr
                            s_unite.save()
                            ServiceConsulteInteraction.objects.create(service_entrant_fk=s_edsr, service_sortant_fk=s_unite,
                                                                      type_lien_s='interroger')
                            ct_unite = ContentType.objects.get_for_model(UniteSpecialisee.objects.get(pk=s_unite.old_model_json['pk']))
                            for p in PreAvis.objects.filter(content_type=ct_unite, object_id=s_unite.old_model_json['pk']).order_by('pk'):
                                a = Avis(etat=p.etat, date_demande=p.date_demande, date_reponse=p.date_reponse,
                                         favorable=p.favorable, prescriptions=p.prescriptions, instruction=p.avis.instruction,
                                         agent=p.agentlocal, service_consulte_fk=s_unite, service_demandeur_fk=s_edsr,
                                         service_consulte_origine_fk=s_unite, avis_parent_fk=p.avis, ancien_preavis=str(p.pk))
                                if p.preavis_origine:
                                    a.avis_origine = Avis.objects.get(ancien_preavis=str(p.preavis_origine.pk))
                                    a.raison_redemande = p.raison_redemande
                                a.save()

                    elif instance.workflow_ggd == 1:
                        s_ggd.porte_entree_avis_b = True
                        s_ggd.admin_service_famille_b = False
                        s_ggd.admin_utilisateurs_subalternes_b = False
                        s_ggd.autorise_rendre_avis_b = True
                        s_ggd.save()

                        ct_ggd = ContentType.objects.get_for_model(GGD.objects.get(pk=s_ggd.old_model_json['pk']))
                        for a in Avis.objects.filter(content_type=ct_ggd, object_id=s_ggd.old_model_json['pk']):
                            a.service_consulte_fk = s_ggd
                            a.service_consulte_origine_fk = s_ggd
                            a.service_demandeur_fk = a.instruction.referent.organisation_m2m.first() if a.instruction.referent else None
                            a.save()

                        s_edsr.service_parent_fk = s_ggd
                        s_edsr.save()
                        ServiceConsulteInteraction.objects.create(service_entrant_fk=s_ggd, service_sortant_fk=s_edsr,
                                                                  type_lien_s='interroger')
                        ct_edsr = ContentType.objects.get_for_model(EDSR.objects.get(pk=s_edsr.old_model_json['pk']))
                        for p in PreAvis.objects.filter(content_type=ct_edsr, object_id=s_edsr.old_model_json['pk']).order_by('pk'):
                            a = Avis(etat=p.etat, date_demande=p.date_demande, date_reponse=p.date_reponse,
                                     favorable=p.favorable, prescriptions=p.prescriptions, instruction=p.avis.instruction,
                                     agent=p.agentlocal, service_consulte_fk=s_edsr, service_demandeur_fk=s_ggd,
                                     service_consulte_origine_fk=s_edsr, avis_parent_fk=p.avis, ancien_preavis=str(p.pk))
                            if p.preavis_origine:
                                a.avis_origine = Avis.objects.get(ancien_preavis=str(p.preavis_origine.pk))
                                a.raison_redemande = p.raison_redemande
                            a.save()

                        for s_cgd in s_cgds:
                            s_cgd.service_parent_fk = s_ggd
                            s_cgd.save()
                            ServiceConsulteInteraction.objects.create(service_entrant_fk=s_ggd, service_sortant_fk=s_cgd,
                                                                      type_lien_s='interroger')
                            ct_cgd = ContentType.objects.get_for_model(CGD.objects.get(pk=s_cgd.old_model_json['pk']))
                            for p in PreAvis.objects.filter(content_type=ct_cgd, object_id=s_cgd.old_model_json['pk']).order_by('pk'):
                                a = Avis(etat=p.etat, date_demande=p.date_demande, date_reponse=p.date_reponse,
                                         favorable=p.favorable, prescriptions=p.prescriptions, instruction=p.avis.instruction,
                                         agent=p.agentlocal, service_consulte_fk=s_cgd, service_demandeur_fk=s_ggd,
                                         service_consulte_origine_fk=s_cgd, avis_parent_fk=p.avis, ancien_preavis=str(p.pk))
                                if p.preavis_origine:
                                    a.avis_origine = Avis.objects.get(ancien_preavis=str(p.preavis_origine.pk))
                                    a.raison_redemande = p.raison_redemande
                                a.save()

                        for s_unite in s_unites:
                            s_unite.service_parent_fk = s_ggd
                            s_unite.save()
                            ServiceConsulteInteraction.objects.create(service_entrant_fk=s_ggd, service_sortant_fk=s_unite,
                                                                      type_lien_s='interroger')
                            ct_unite = ContentType.objects.get_for_model(UniteSpecialisee.objects.get(pk=s_unite.old_model_json['pk']))
                            for p in PreAvis.objects.filter(content_type=ct_unite, object_id=s_unite.old_model_json['pk']).order_by('pk'):
                                a = Avis(etat=p.etat, date_demande=p.date_demande, date_reponse=p.date_reponse,
                                         favorable=p.favorable, prescriptions=p.prescriptions, instruction=p.avis.instruction,
                                         agent=p.agentlocal, service_consulte_fk=s_unite, service_demandeur_fk=s_ggd,
                                         service_consulte_origine_fk=s_unite, avis_parent_fk=p.avis, ancien_preavis=str(p.pk))
                                if p.preavis_origine:
                                    a.avis_origine = Avis.objects.get(ancien_preavis=str(p.preavis_origine.pk))
                                    a.raison_redemande = p.raison_redemande
                                a.save()

                    else:  # 2
                        s_ggd.porte_entree_avis_b = True
                        s_ggd.admin_service_famille_b = False
                        s_ggd.admin_utilisateurs_subalternes_b = False
                        s_ggd.autorise_rendre_avis_b = True
                        s_ggd.save()
                        s_edsr.service_parent_fk = s_ggd
                        s_edsr.save()

                        ct_ggd = ContentType.objects.get_for_model(GGD.objects.get(pk=s_ggd.old_model_json['pk']))
                        for a in Avis.objects.filter(content_type=ct_ggd, object_id=s_ggd.old_model_json['pk']).exclude(etat__in=['transmis', 'distribué']):
                            a.service_consulte_fk = s_ggd
                            a.service_consulte_origine_fk = s_ggd
                            a.service_demandeur_fk = a.instruction.referent.organisation_m2m.first() if a.instruction.referent else None
                            a.save()
                            if a.etat in ["formaté", 'rendu']:
                                a.service_consulte_history_m2m.add(s_edsr)

                        ServiceConsulteInteraction.objects.create(service_entrant_fk=s_ggd, service_sortant_fk=s_edsr,
                                                                  type_lien_s="mandater")
                        ServiceConsulteInteraction.objects.create(service_entrant_fk=s_edsr, service_sortant_fk=s_ggd,
                                                                  type_lien_s="adresser")
                        for a in Avis.objects.filter(content_type=ct_ggd, object_id=s_ggd.old_model_json['pk']).filter(etat__in=['transmis', 'distribué']):
                            a.service_consulte_fk = s_edsr
                            a.service_consulte_origine_fk = s_ggd
                            a.service_demandeur_fk = a.instruction.referent.organisation_m2m.first() if a.instruction.referent else None
                            a.save()
                            a.service_consulte_history_m2m.add(s_ggd)

                        for s_cgd in s_cgds:
                            s_cgd.service_parent_fk = s_edsr
                            s_cgd.save()
                            ServiceConsulteInteraction.objects.create(service_entrant_fk=s_edsr, service_sortant_fk=s_cgd,
                                                                      type_lien_s='interroger')
                            ct_cgd = ContentType.objects.get_for_model(CGD.objects.get(pk=s_cgd.old_model_json['pk']))
                            for p in PreAvis.objects.filter(content_type=ct_cgd, object_id=s_cgd.old_model_json['pk']).order_by('pk'):
                                a = Avis(etat=p.etat, date_demande=p.date_demande, date_reponse=p.date_reponse,
                                         favorable=p.favorable, prescriptions=p.prescriptions, instruction=p.avis.instruction,
                                         agent=p.agentlocal, service_consulte_fk=s_cgd, service_demandeur_fk=s_edsr,
                                         service_consulte_origine_fk=s_cgd, avis_parent_fk=p.avis, ancien_preavis=str(p.pk))
                                if p.preavis_origine:
                                    a.avis_origine = Avis.objects.get(ancien_preavis=str(p.preavis_origine.pk))
                                    a.raison_redemande = p.raison_redemande
                                a.save()
                        for s_unite in s_unites:
                            s_unite.service_parent_fk = s_edsr
                            s_unite.save()
                            ServiceConsulteInteraction.objects.create(service_entrant_fk=s_edsr, service_sortant_fk=s_unite,
                                                                      type_lien_s='interroger')
                            ct_unite = ContentType.objects.get_for_model(UniteSpecialisee.objects.get(pk=s_unite.old_model_json['pk']))
                            for p in PreAvis.objects.filter(content_type=ct_unite, object_id=s_unite.old_model_json['pk']).order_by('pk'):
                                a = Avis(etat=p.etat, date_demande=p.date_demande, date_reponse=p.date_reponse,
                                         favorable=p.favorable, prescriptions=p.prescriptions, instruction=p.avis.instruction,
                                         agent=p.agentlocal, service_consulte_fk=s_unite, service_demandeur_fk=s_edsr,
                                         service_consulte_origine_fk=s_unite, avis_parent_fk=p.avis, ancien_preavis=str(p.pk))
                                if p.preavis_origine:
                                    a.avis_origine = Avis.objects.get(ancien_preavis=str(p.preavis_origine.pk))
                                    a.raison_redemande = p.raison_redemande
                                a.save()

            # CG
            if choix in ["0", "5"]:
                print('création cg')
                categorie_cg = CategorieService.objects.get(nom_s="Conseil Départemental")
                type_cg = TypeService.objects.get(nom_s="Conseil Départemental", categorie_fk=categorie_cg)
                for c in CG.objects.all():
                    s = ServiceConsulte(email_s=c.email,
                                        type_service_fk=type_cg,
                                        instance_fk=c.departement.get_instance(),
                                        admin_service_famille_b=False, admin_utilisateurs_subalternes_b=False,
                                        porte_entree_avis_b=True,
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json,
                                        old_model_json={"name": "CG", "pk": c.pk})
                    s.save()
                    s.departement_m2m.add(c.departement)
                    for u in c.get_service_users():
                        u.organisation_m2m.add(s)
                    ct_c = ContentType.objects.get_for_model(c)
                    for a in Avis.objects.filter(content_type=ct_c, object_id=c.pk).exclude(etat__in=['formaté', 'rendu']):
                        a.service_consulte_fk = s
                        a.service_consulte_origine_fk = s
                        a.service_demandeur_fk = a.instruction.referent.organisation_m2m.first() if a.instruction.referent else None
                        a.save()
                    sn1 = ServiceConsulte(email_s=c.email, precision_nom_s="Validation N+1",
                                          type_service_fk=type_cg,
                                          instance_fk=c.departement.get_instance(),
                                          service_parent_fk=s,
                                          admin_service_famille_b=False, admin_utilisateurs_subalternes_b=False,
                                          autorise_rendre_avis_b=True,
                                          autorise_acces_consult_organisateur_b=True,
                                          notification_email_json=notification_email_json,
                                          old_model_json={"name": "CG", "pk": c.pk})
                    sn1.save()
                    sn1.departement_m2m.add(c.departement)
                    for u in c.get_service_users_superieur():
                        u.organisation_m2m.add(sn1)
                    transfert_acces(c, s)
                    for a in Avis.objects.filter(content_type=ct_c, object_id=c.pk).filter(etat__in=['formaté', 'rendu']):
                        a.service_consulte_fk = sn1
                        a.service_consulte_origine_fk = s
                        a.service_demandeur_fk = a.instruction.referent.organisation_m2m.first() if a.instruction.referent else None
                        a.save()
                        a.service_consulte_history_m2m.add(s)
                    ServiceConsulteInteraction.objects.create(service_entrant_fk=s, service_sortant_fk=sn1,
                                                              type_lien_s="adresser")
                print("création cgservice")
                # mettre type v5
                for c in CGService.objects.all():
                    print(c.pk)
                    s_parent = ServiceConsulte.objects.get(old_model_json={"name": "CG", "pk": c.cg.pk}, precision_nom_s='')
                    s = ServiceConsulte(precision_nom_s=c.precision_nom_v6, email_s=c.email,
                                        instance_fk=s_parent.instance_fk,
                                        service_parent_fk=s_parent,
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json)
                    type_service = TypeService.objects.filter(Q(abreviation_s=c.type_service_v6) | Q(nom_s=c.type_service_v6)).filter(categorie_fk=categorie_cg)
                    s.type_service_fk = type_service.first()
                    s.save()
                    s.departement_m2m.add(c.cg.departement)
                    for u in c.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(c, s)
                    ServiceConsulteInteraction.objects.create(service_entrant_fk=s_parent, service_sortant_fk=s, type_lien_s="interroger")
                    ct_c = ContentType.objects.get_for_model(c)
                    for p in PreAvis.objects.filter(content_type=ct_c, object_id=c.pk).order_by("pk"):
                        a = Avis(etat=p.etat, date_demande=p.date_demande, date_reponse=p.date_reponse,
                                 favorable=p.favorable, prescriptions=p.prescriptions, instruction=p.avis.instruction,
                                 agent=p.agentlocal, service_consulte_fk=s, service_demandeur_fk=s_parent,
                                 service_consulte_origine_fk=s, avis_parent_fk=p.avis, ancien_preavis=str(p.pk))
                        if p.preavis_origine:
                            a.avis_origine = Avis.objects.get(ancien_preavis=str(p.preavis_origine.pk))
                            a.raison_redemande = p.raison_redemande
                        a.save()
            # pompier
            if choix in ['0', '6']:
                print('création sdis')
                categorie_pompier = CategorieService.objects.get(nom_s="Secours")
                type_sdis = TypeService.objects.get(nom_s="Service Départemental d'Incendie et de Secours", abreviation_s="SDIS",
                                                       categorie_fk=categorie_pompier)
                for sdis in SDIS.objects.all():
                    s = ServiceConsulte(email_s=sdis.email,
                                        type_service_fk=type_sdis,
                                        instance_fk=sdis.departement.get_instance(),
                                        admin_service_famille_b=False, admin_utilisateurs_subalternes_b=False,
                                        porte_entree_avis_b=True,
                                        autorise_rendre_avis_b=True,
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json)
                    s.save()
                    s.departement_m2m.add(sdis.departement)
                    for u in sdis.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(sdis, s)
                    ct_sdis = ContentType.objects.get_for_model(sdis)
                    for a in Avis.objects.filter(content_type=ct_sdis, object_id=sdis.pk):
                        a.service_consulte_fk = s
                        a.service_consulte_origine_fk = s
                        a.service_demandeur_fk = a.instruction.referent.organisation_m2m.first() if a.instruction.referent else None
                        a.save()

                print('création codis')
                type_codis = TypeService.objects.get(nom_s="Centre Opérationnel Départemental d'Incendie et de Secours",
                                                        abreviation_s="CODIS", categorie_fk=categorie_pompier)
                for c in CODIS.objects.all():
                    s_parent = ServiceConsulte.objects.get(instance_fk=c.departement.get_instance(), type_service_fk=type_sdis)
                    s = ServiceConsulte(email_s=c.email,
                                        type_service_fk=type_codis,
                                        instance_fk=c.departement.get_instance(),
                                        service_parent_fk=s_parent,
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json)
                    s.save()
                    s.departement_m2m.add(c.departement)
                    for u in c.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(c, s)

                print('création compagnie')
                type_compagnie = TypeService.objects.get(nom_s="Groupement", categorie_fk=categorie_pompier)
                for c in Compagnie.objects.all():
                    s_parent = ServiceConsulte.objects.get(instance_fk=c.sdis.departement.get_instance(), type_service_fk=type_sdis)
                    s = ServiceConsulte(precision_nom_s=c.number, email_s=c.email,
                                        type_service_fk=type_compagnie,
                                        instance_fk=s_parent.instance_fk,
                                        service_parent_fk=s_parent,
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json,
                                        old_model_json={"nom": "Compagnie", "pk": c.pk})
                    s.save()
                    s.departement_m2m.add(c.sdis.departement)
                    for u in c.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(c, s)
                    ct_c = ContentType.objects.get_for_model(c)
                    for p in PreAvis.objects.filter(content_type=ct_c, object_id=c.pk):
                        a = Avis(etat=p.etat, date_demande=p.date_demande, date_reponse=p.date_reponse,
                                 favorable=p.favorable, prescriptions=p.prescriptions, instruction=p.avis.instruction,
                                 agent=p.agentlocal, service_consulte_fk=s, service_demandeur_fk=s_parent,
                                 service_consulte_origine_fk=s, avis_parent_fk=p.avis, ancien_preavis=str(p.pk))
                        if p.preavis_origine:
                            a.avis_origine = Avis.objects.get(ancien_preavis=str(p.preavis_origine.pk))
                            a.raison_redemande = p.raison_redemande
                        a.save()
                    ServiceConsulteInteraction.objects.create(service_entrant_fk=s_parent, service_sortant_fk=s,
                                                              type_lien_s="interroger")

                print("création cis")
                type_cis = TypeService.objects.get(nom_s="Centre d'Incendie et de Secours", abreviation_s="CIS",
                                                      categorie_fk=categorie_pompier)
                for c in CIS.objects.all():
                    s_parent = ServiceConsulte.objects.get(old_model_json__pk=c.compagnie.pk, type_service_fk=type_compagnie)
                    s = ServiceConsulte(precision_nom_s=c.name, email_s=c.email,
                                        type_service_fk=type_cis,
                                        instance_fk=s_parent.instance_fk,
                                        service_parent_fk=s_parent,
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json)
                    s.save()
                    s.commune_m2m.add(c.commune)
                    for u in c.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(c, s)

            # Police
            if choix in ['0', '7']:
                print('création ddsp')
                categorie_police = CategorieService.objects.get(nom_s="Sécurité")
                type_ddsp = TypeService.objects.get(nom_s="Direction Départementale de la Sécurité Publique", abreviation_s="DDSP",
                                                       categorie_fk=categorie_police)
                for d in DDSP.objects.all():
                    s = ServiceConsulte(email_s=d.email,
                                        type_service_fk=type_ddsp,
                                        instance_fk=d.departement.get_instance(),
                                        admin_service_famille_b=False, admin_utilisateurs_subalternes_b=False,
                                        porte_entree_avis_b=True,
                                        autorise_rendre_avis_b=True,
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json)
                    s.save()
                    s.departement_m2m.add(d.departement)
                    for u in d.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(d, s)
                    ct_d = ContentType.objects.get_for_model(d)
                    for a in Avis.objects.filter(content_type=ct_d, object_id=d.pk):
                        a.service_consulte_fk = s
                        a.service_consulte_origine_fk = s
                        a.service_demandeur_fk = a.instruction.referent.organisation_m2m.first() if a.instruction.referent else None
                        a.save()

                print('création commissariat')
                type_commissariat = TypeService.objects.get(nom_s="Circonscriptions de Sécurité Publique (commissariat)", abreviation_s="CSP", categorie_fk=categorie_police)
                for c in Commissariat.objects.all():
                    s_parent = ServiceConsulte.objects.get(type_service_fk=type_ddsp,
                                                           instance_fk=c.commune.arrondissement.departement.get_instance())
                    s = ServiceConsulte(precision_nom_s=c.name, email_s=c.email,
                                        type_service_fk=type_commissariat,
                                        instance_fk=s_parent.instance_fk,
                                        service_parent_fk=s_parent,
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json)
                    s.save()
                    s.commune_m2m.add(c.commune)
                    for u in c.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(c, s)
                    ct_c = ContentType.objects.get_for_model(c)
                    for p in PreAvis.objects.filter(content_type=ct_c, object_id=c.pk).order_by('pk'):
                        a = Avis(etat=p.etat, date_demande=p.date_demande, date_reponse=p.date_reponse,
                                 favorable=p.favorable, prescriptions=p.prescriptions, instruction=p.avis.instruction,
                                 agent=p.agentlocal, service_consulte_fk=s, service_demandeur_fk=s_parent,
                                 service_consulte_origine_fk=s, avis_parent_fk=p.avis, ancien_preavis=str(p.pk))
                        if p.preavis_origine:
                            a.avis_origine = Avis.objects.get(ancien_preavis=str(p.preavis_origine.pk))
                            a.raison_redemande = p.raison_redemande
                        a.save()
                    ServiceConsulteInteraction.objects.create(service_entrant_fk=s_parent, service_sortant_fk=s,
                                                              type_lien_s="interroger")

            # services simple
            if choix in ['0', '8']:
                print('création service simple')
                total = Service.objects.count()
                count = 0
                for s_old in Service.objects.all():
                    count += 1
                    print(f"{count}/{total} Services simples")
                    s = ServiceConsulte(precision_nom_s=s_old.name, email_s=s_old.email,
                                        instance_fk=s_old.departements.first().get_instance(),
                                        admin_service_famille_b=False, admin_utilisateurs_subalternes_b=False,
                                        porte_entree_avis_b=True,
                                        autorise_rendre_avis_b=True,
                                        autorise_acces_consult_organisateur_b=True,
                                        notification_email_json=notification_email_json)
                    type_service = TypeService.objects.filter(
                        Q(abreviation_s=s_old.type_service_v6) | Q(nom_s=s_old.type_service_v6))
                    s.type_service_fk = type_service.first()
                    s.save()
                    for dep in s_old.departements.all():
                        s.departement_m2m.add(dep)
                    for u in s_old.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(s_old, s)
                    ct_s_old = ContentType.objects.get_for_model(s_old)
                    for a in Avis.objects.filter(content_type=ct_s_old, object_id=s_old.pk):
                        a.service_consulte_fk = s
                        a.service_consulte_origine_fk = s
                        a.service_demandeur_fk = a.instruction.referent.organisation_m2m.first() if a.instruction.referent else None
                        a.save()

            # federation
            if choix in ['0', '9']:
                print("création fédération")
                categorie_federation = CategorieService.objects.get(nom_s="Fédération")
                # Fédération délégataire en type pour tous
                for f in Federation.objects.all():
                    type_federation = TypeService.objects.get(nom_s="Fédération délégataire", categorie_fk=categorie_federation)
                    s = FederationNew(precision_nom_s=str(f), email_s=f.email,
                                      type_service_fk=type_federation,
                                      discipline_fk=f.discipline,
                                      admin_service_famille_b=False, admin_utilisateurs_subalternes_b=False,
                                      porte_entree_avis_b=True,
                                      autorise_rendre_avis_b=True,
                                      autorise_acces_consult_organisateur_b=True,
                                      notification_email_json=notification_email_json)
                    s.save()
                    if f.level == 0:
                        s.departement_m2m.add(f.departement)
                        s.instance_fk = f.departement.get_instance()
                    elif f.level == 1:
                        r_name = list(filter(lambda x: x[0]==f.region, LISTE_REGION))[0][1]
                        r = Region.objects.get(nom=r_name)
                        s.region_m2m.add(r)
                        s.instance_fk = Instance.objects.get_master()
                    else:
                        s.national_b = True
                        s.instance_fk = Instance.objects.get_master()
                    s.save()
                    for u in f.get_service_users():
                        u.organisation_m2m.add(s)
                    transfert_acces(f, s)
                    ct_f = ContentType.objects.get_for_model(f)
                    for a in Avis.objects.filter(content_type=ct_f, object_id=f.pk):
                        a.service_consulte_fk = s
                        a.service_consulte_origine_fk = s
                        a.service_demandeur_fk = a.instruction.referent.organisation_m2m.first() if a.instruction.referent else None
                        a.save()

            # instruction
            if choix in ['0', '10']:
                total = Instruction.objects.count()
                count = 0
                for i in Instruction.objects.all():
                    count += 1
                    print(f"{count}/{total} instructions")
                    i.set_service_instructeurs()
                total = Avis.objects.count()
                count = 0
                for a in Avis.objects.all():
                    count += 1
                    print(f"{count}/{total} avis")
                    if not a.historique_circuit_json:
                        a.historique_circuit_json = []
                        a.save()
                # Transformation des avis AVTM
                for a in Manif.objects.all():
                    if a.get_type_manif() == 'avtm' and a.instance_instruites.count() > 1:
                        for i in a.instruction.filter(instruction_principale__isnull=False):
                            avis = i.avis.filter(service_concerne="prefecture").first()
                            pref = i.get_prefecture_concernee()
                            if not pref:
                                avis.service_consulte_fk = ServiceConsulte.objects.get(type_service_fk__nom_s="Manifestation sportive")
                                avis.service_consulte_origine_fk = ServiceConsulte.objects.get(type_service_fk__nom_s="Manifestation sportive")
                                avis.service_demandeur_fk = ServiceConsulte.objects.get(type_service_fk__nom_s="Manifestation sportive")
                                avis.save()
                            else:
                                avis.service_consulte_fk = ServiceConsulte.objects.get(pk=pref.pk)
                                avis.service_consulte_origine_fk = ServiceConsulte.objects.get(pk=pref.pk)
                                avis.service_demandeur_fk = ServiceConsulte.objects.get(type_service_fk__nom_s="Manifestation sportive")
                                avis.save()

