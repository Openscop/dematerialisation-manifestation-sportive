from django.core.management.base import BaseCommand
from django.db import transaction

from core.models import User
from structure.models import StructureOrganisatrice


class Command(BaseCommand):
    """
    commande pour remplir le champ instance_fk des structures organisatrices
    créées après le transfert V5 -> V6
    """
    args = ''
    help = 'Mettre à jour le champ instance_fk des structures organisatrices'

    def handle(self, *args, **options):
        with transaction.atomic():
            strucs = StructureOrganisatrice.objects.all()
            total = 0
            for struc in strucs:
                if not struc.instance_fk:
                    total += 1
                    struc.instance_fk = struc.commune_m2m.first().get_instance()
                    struc.save()
            print(f'{total} structures organisatrices modifiées')
