# coding: utf-8
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db import transaction
from core.models.optionuser import OptionUser

from core.util.security import protect_code


class Command(BaseCommand):
    """
    commande pour créer la table d'option de tous les utilisateurs.
    """
    def handle(self, *args, **options):
        """ Exécuter la commande """
        with transaction.atomic():
            count = get_user_model().objects.count()
            for user in get_user_model().objects.all():
                op=OptionUser.objects.filter(user=user)
                if not op:
                    option=OptionUser(user=user)
                    option.save()
            print("Les options de {0} utilisateurs ont été mis à jour.".format(count))