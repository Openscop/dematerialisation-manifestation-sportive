# coding: utf-8
import asyncio
from asgiref.sync import sync_to_async, async_to_sync
from django.core.management.base import BaseCommand
from func_timeout import func_timeout

from evenements.models import PieceJointe
from evenements.templatetags.thumbnail_filter import thumbnail
from core.tasks import creation_thumbnail_doc_officiel, creation_thumbnail_pj_avis,gestion_fichier
from instructions.models import DocumentOfficiel, PieceJointeAvis


class Command(BaseCommand):
    """
    commande pour regénèrer toutes les miniature manquantes
    """
    help = 'Regénère toutes les  miniatures manquante ( à lancer sur TASYNC)'

    def handle(self, *args, **options):
        print('Traitement des pj')
        total = PieceJointe.objects.count()
        count = 0
        for pj in PieceJointe.objects.all():
            count += 1
            print(f"{count}/{total}")
            if pj.document_attache and thumbnail(pj.document_attache) == "/static/portail/img/vignette.png":
                gestion_fichier.delay(pj.manif.pk, pj.pk)
        print('Traitement des Doc offi')
        total = DocumentOfficiel.objects.count()
        count = 0
        for doc in DocumentOfficiel.objects.all():
            count += 1
            print(f"{count}/{total}")
            if doc.fichier and thumbnail(doc.fichier) == "/static/portail/img/vignette.png":
                creation_thumbnail_doc_officiel.delay(doc.pk)
        print('Traitement des avis')
        total = PieceJointeAvis.objects.count()
        count = 0
        for pj_avis in PieceJointeAvis.objects.all():
            count += 1
            print(f"{count}/{total}")
            if pj_avis.fichier and thumbnail(pj_avis.fichier) == "/static/portail/img/vignette.png":
                creation_thumbnail_pj_avis.delay(pj_avis.pk)
        print('Terminé')
