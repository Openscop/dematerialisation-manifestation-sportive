# coding: utf-8
from django.core.management.base import BaseCommand
from django.db import transaction

from core.models import Instance
from structure.models import TypeService, ServiceCDSR


class Command(BaseCommand):
    """
    Création des services CDSR pour chaque instance départementale
    """

    def handle(self, *args, **options):
        """ Exécuter la commande """
        print('Création des services CDSR')
        notification_email_json = {"quotidien_mail": True, "conversation_mail": False,
                                   "nouveaute_mail": False, "action_mail": False, "forum_mail": False,
                                   "notification_mail": False, "tracabilite_mail": False}
        with transaction.atomic():
            try:
                type_service_cdsr = TypeService.objects.get(abreviation_s="CDSR")
            except:
                print("pas de type de service CDSR trouvé => ABANDON")
                return
            for instance in Instance.objects.all():
                if instance.departement:
                    cdsr = ServiceCDSR(
                        type_service_fk=type_service_cdsr,
                        porte_entree_avis_b=True,
                        autorise_rendre_avis_b=True,
                        instance_fk=instance,
                        autorise_acces_consult_organisateur_b=True,
                        notification_email_json=notification_email_json
                    )
                    cdsr.save()
                    cdsr.departement_m2m.add(instance.departement)
                    # Remplissage de nom_s
                    cdsr.save()
