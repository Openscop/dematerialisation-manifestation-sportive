import click

from django.core.management.base import BaseCommand
from messagerie.models.message import Enveloppe


class Command(BaseCommand):
    """
    Corrige un champ type mal attribué à des enveloppes
    """
    def handle(self, *args, **options):
        print("Choisissez le type de message à corriger")
        print("0 : traçabilité -> tracabilite | 1 : Choix manuelle")
        choix_type = input()
        if choix_type == "1":
            print("Entré le type de message à corriger")
            type_a_corriger = input()
            print("Entré maintenant en quoi le corriger")
            corriger_en = input()
            if not click.confirm("Vous vous appréter a transformer les message de type " + type_a_corriger
                           + " en message de type " + corriger_en + ". Continuer ? ", default=True):
                quit()
        else:
            type_a_corriger = "traçabilité"
            corriger_en = "tracabilite"
        for index, enveloppe in enumerate(Enveloppe.objects.filter(type=type_a_corriger)):
            if index % 500 == 0:
                print('+', end='')
            enveloppe.type = corriger_en
            enveloppe.save()
