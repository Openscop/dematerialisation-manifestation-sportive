# coding: utf-8
import os
import json

from django.core.management.base import BaseCommand

from administrative_division.models import Commune, Arrondissement, Departement
from administrative_division.models.departement import LISTE_DEPARTEMENT


def dep_name(item):
    return [dep[1] for dep in LISTE_DEPARTEMENT if dep[0] == item][0]


class Command(BaseCommand):
    """ Vérification des communes de la BD """
    args = ''
    help = 'Vérifier les communes de la BD par rapport à un fichier json Georef'

    def handle(self, *args, **options):
        """ Exécuter la commande """
        liste_communes = Commune.objects.all()
        print('fichier : /tmp/georef-france-commune.json')
        fichier = open(os.path.join('/tmp/georef-france-commune.json'))
        data = fichier.read()
        jsondata = json.loads(data)
        print(f"Nombre d'entrée du fichier : {len(jsondata)}")
        print(f"Nombre de communes de la BD : {len(liste_communes)}")
        comm_ok, comm_in, comm_out, comm_mod = [], [], [], []
        fail_1, fail_2 = "", ""
        print('modifier : nom commune = 1, arrondissement commune = 2, tout = 0')
        modif = input()
        for index, item in enumerate(jsondata):
            if index % 500 == 0:
                print('+', end='')
            code = item['fields']['com_code']
            if not liste_communes.filter(code=code).exists():
                comm_out.append(code)
                continue
            comm = liste_communes.get(code=code)
            comm_in.append(comm.pk)
            ok = True
            if modif in ["0", "1"]:
                if not comm.name == item['fields']['com_name']:
                    fail_1 += f"nom de la commune modifié : " \
                              f"{code} - {comm.name} -> {item['fields']['com_name']}{chr(10)}"
                    comm.name = item['fields']['com_name']
                    ok = False
            if modif in ["0", "2"]:
                if "arrdep_name" in item['fields']:
                    if not comm.arrondissement.code == item['fields']['arrdep_code']:
                        fail_2 += f"arrondissement modifié : {code} - {comm.name} - " \
                                  f"{comm.arrondissement.name} ({comm.arrondissement.code}) -> " \
                                  f"{item['fields']['arrdep_name']} ({item['fields']['arrdep_code']}){chr(10)}"
                        arron = Arrondissement.objects.get(code=item['fields']['arrdep_code'])
                        comm.arrondissement = arron
                        ok = False
            if ok:
                comm_ok.append(comm.pk)
            else:
                comm_mod.append(comm.pk)
                comm.save()
        print()
        print(f"Nombre de communes checkées : {len(comm_in)}")
        print(f"Nombre de communes ok : {len(comm_ok)}")
        print(f"Nombre de communes hors BD : {len(comm_out)}")
        print(f"Nombre de communes modifiées : {len(comm_mod)}")
        liste_fichiers = [(fail_1, "mod_result_nom_comm_diff"), (fail_2, "mod_result_arr_comm_diff")]
        for fichier in liste_fichiers:
            if fichier[0]:
                f = open(f"/tmp/{fichier[1]}.txt", "w+")
                f.write(str(fichier[0]))
                f.close()
