# coding: utf-8
import os
from time import time

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.db import connections

from core.util.diagnose import dump_compared_diagnostics
from core.util.security import protect_code
from core.util.types import make_iterable


class Command(BaseCommand):
    """
    Migrer toutes les bases configurées d'un seul coup et les fusionner

    Les bases doivent être des bases Postgres, et les utilisateurs doivent
    être en mesure de créer et supprimer les bases
    Opère quelques changements sur certaines bases avant la migration.
    """
    args = ''
    help = "Migrer toutes les instances configurées"

    def add_arguments(self, parser):
        parser.add_argument('filename', nargs='+', type=str)
        parser.add_argument('--no-input', action='store_false', dest='interactive', default=True,
                            help="Ne pas demander de validation de l'utilisateur")
        parser.add_argument('--dev', action='store_true', dest='dev', default=False,
                            help="Définir les mots de passe à 123 et valider tous les utilisateurs")
        parser.add_argument('--no-notifications', action='store_false', dest='notifications', default=True,
                            help="Désactiver l'import des Notifications (notifications.Notification)")

    def handle(self, *args, **options):
        """ Exécuter la commande """
        protect_code()

        interactive = options.get('interactive')
        development = options.get('dev')
        notifications = options.get('notifications')

        reply = ''

        if interactive is True:
            reply = input("Cette commande va supprimer toutes les bases de données configurées. Continuer ? [oui/*]\n")

        if reply.lower() in ["oui", "o"] or not interactive:
            start = time()
            databases = list(settings.DATABASES.keys())[1:]  # ne pas prendre en compte default
            filenames = make_iterable(options.get('filename', []))
            # Échouer si l'utilisateur n'a pas passé autant de fichiers de dump qu'il y a de db configurées
            if len(filenames) != len(databases):
                raise ImproperlyConfigured(
                    "Vous devez passer un chemin de fichier de dump pour chaque base configurée. ({1}/{0})".format(len(databases), len(args)))
            # Vider la base de données principale
            call_command('create_clean_base', interactive=False)
            # Supprimer les bases de données, les recréer et charger les dumps
            for ind, database in enumerate(databases):
                db_user = settings.DATABASES[database]['USER']
                db_name = settings.DATABASES[database]['NAME']
                if os.system("dropdb --if-exists -U{u} {name}".format(u=db_user, name=db_name)) != 0:
                    raise OSError("Impossible de lancer l'opération : un processus semble utiliser la base de données (pgAdmin, runserver ?)")
                os.system("createdb -U{u} {name}".format(u=db_user, name=db_name))
                os.system("psql -U{u} {name} < {file}".format(u=db_user, name=db_name, file=filenames[ind]))

            # Réparer la contrainte de clé étrangère sur la base 97-2, si configurée
            if 'import_972' in databases:
                print("Correction de contrainte de clé étrangère erronée sur la base 972...")
                cursor = connections['import_972'].cursor()
                cursor.execute("ALTER TABLE public.auth_user_user_permissions DROP CONSTRAINT auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id;")
                cursor.execute(
                    "ALTER TABLE public.auth_user_user_permissions ADD CONSTRAINT auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id FOREIGN KEY ("
                    "permission_id) REFERENCES public.auth_permission (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;")
            # Exécuter la migration sur les bases configurées
            for database in databases:
                call_command('migrate', database=database, interactive=False)
                call_command('set_instance', database=database, interactive=False, departement=settings.DATABASES[database]['NUMBER'])

            # Exécuter la fusion des bases
            call_command('import_external', interactive=False, notifications=notifications)
            # Effectuer les options de dév
            if development is True:
                call_command('set_passwords_to_123')
                call_command('set_all_verified')
            # Enregistrer un log de l'état de la base de destination après import
            dump_compared_diagnostics('post-comparison.log')
            # Afficher le récapitulatif
            elapsed = time() - start
            print("Opérations effectuées avec succès en {elapsed:.01f} secondes.".format(elapsed=elapsed))
