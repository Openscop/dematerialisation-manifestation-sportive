# coding: utf-8
"""
Effectuer un changement de commune sur tous les objets en relation avec cette commune
Commande à faire après la création de la commune de remplacement
Ne concerne pas l'ancienne réglementation
Utilisable uniquement en mode DEBUG.
"""
from django.core.management.base import BaseCommand
from django.db import transaction

from core.util.security import protect_code
from administrative_division.models import Commune
from structure.models import Organisation, ServiceInstructeur
from evaluation_incidence.models import N2kOperateurSite, RnrAdministrateur
from evenements.models import Manif
from instructions.models import Avis


class Command(BaseCommand):
    """ Changement de commune dans la BD """
    args = ''
    help = 'Réaffecter tout ce qui concerne une commune à une autre'

    def handle(self, *args, **options):
        """ Exécuter la commande """
        protect_code()
        liste_travail = ((Organisation, 'organisation', 'commune_m2m'),
                         (N2kOperateurSite, 'Opérateur N2k', 'commune'),
                         (RnrAdministrateur, 'Administrateur Rnr', 'commune'),
                         (Manif, 'Ville de départ de manifestation', 'ville_depart'),
                         (Manif, 'Ville de départ interdep de manifestation', 'ville_depart_interdep_mtm'),
                         (Manif, 'Ville traversée de manifestation', 'villes_traversees'),
                         (Avis, 'Service consulté d\'avis', 'service_consulte_fk'),
                         (Avis, 'Service consulté origine d\'avis', 'service_consulte_origine_fk'),
                         (Avis, 'Service consulté historique d\'avis', 'service_consulte_history_m2m'),
                         (Avis, 'Service demandeur d\'avis', 'service_demandeur_fk'))

        print('Entrer le code INSEE de la commune source :')
        code_old = input()
        if code_old.isdecimal():
            try:
                com_old = Commune.objects.get(code=code_old)
                print("Commune trouvée : " + com_old.get_zipcoded_name())
                mairie_old = com_old.organisation_set.instance_of(ServiceInstructeur).get()
                print("Mairie trouvée : " + mairie_old.precision_nom_s)
                print('Entrer le code INSEE de la commune de destination (si nouvelle, la crééer au préalable) : ')
                code_new = input()
                if code_new.isdecimal():
                    try:
                        notification_email_json = {"quotidien_mail": True, "conversation_mail": False,
                                                   "nouveaute_mail": False, "action_mail": False, "forum_mail": False,
                                                   "notification_mail": False, "tracabilite_mail": False}

                        com_new = Commune.objects.get(code=code_new)
                        print("Commune trouvée : " + com_new.get_zipcoded_name())
                        if com_new.organisation_set.instance_of(ServiceInstructeur).exists():
                            mairie_new = com_new.organisation_set.instance_of(ServiceInstructeur).get()
                            print("Mairie trouvée : " + mairie_new.precision_nom_s)
                        else:
                            mairie_new = ServiceInstructeur(precision_nom_s=com_new.name, email_s=com_new.email,
                                                            type_service_fk__categorie_fk__nom_s="Mairie",
                                                            type_service_fk__nom_s="Mairie",
                                                            instance_fk=com_new.arrondissement.departement.get_instance(),
                                                            admin_service_famille_b=False,
                                                            admin_utilisateurs_subalternes_b=False,
                                                            porte_entree_avis_b=True,
                                                            autorise_rendre_avis_b=True,
                                                            autorise_acces_consult_organisateur_b=True,
                                                            notification_email_json=notification_email_json)
                            mairie_new.save()
                            print("Mairie créée : " + mairie_new.precision_nom_s)
                        print()
                        print('Occurences à réaffecter :')
                        for travail in liste_travail:
                            if travail[0] == Avis:
                                liste = travail[0].objects.filter(**{travail[2]: mairie_old})
                            elif travail[0] == Organisation:
                                liste = travail[0].objects.filter(**{travail[2]: com_old}).exclude(pk=mairie_old.pk)
                            else:
                                liste = travail[0].objects.filter(**{travail[2]: com_old})
                            if liste:
                                print(str(len(liste)) + ' ' + travail[1] + ' trouvé')
                                for obj in liste:
                                    print('\t' + str(obj))

                        print('Entrez Y pour changer la commune.')
                        if input() in ['Y', 'y', 'O', 'o']:
                            with transaction.atomic():
                                for travail in liste_travail:
                                    if travail[0] == Avis:
                                        liste = travail[0].objects.filter(**{travail[2]: mairie_old})
                                    elif travail[0] == Organisation:
                                        liste = travail[0].objects.filter(**{travail[2]: com_old}).exclude(pk=mairie_old.pk)
                                    else:
                                        liste = travail[0].objects.filter(**{travail[2]: com_old})
                                    if liste:
                                        for obj in liste:
                                            if travail[2] == 'villes_traversees':
                                                obj.villes_traversees.remove(com_old)
                                                obj.villes_traversees.add(com_new)
                                            elif travail[2] == 'ville_depart_interdep_mtm':
                                                obj.ville_depart_interdep_mtm.remove(com_old)
                                                obj.ville_depart_interdep_mtm.add(com_new)
                                                if obj.ville_depart != com_new:
                                                    for idx, inter in enumerate(obj.ville_depart_interdep):
                                                        if inter['commune_pk'] == str(com_old.pk):
                                                            obj.ville_depart_interdep[idx]['commune_pk'] = str(com_new.pk)
                                                            obj.ville_depart_interdep[idx]['commune_name'] = str(com_new)
                                                    obj.save()
                                            elif travail[2] == 'commune_m2m':
                                                obj.commune_m2m.remove(com_old)
                                                obj.commune_m2m.add(com_new)
                                            elif travail[2] == 'service_consulte_history_m2m':
                                                obj.service_consulte_history_m2m.remove(mairie_old)
                                                obj.service_consulte_history_m2m.add(mairie_new)
                                            else:
                                                if travail[0] == Avis:
                                                    setattr(obj, travail[2], mairie_new)
                                                    obj.save()
                                                else:
                                                    setattr(obj, travail[2], com_new)
                                                    obj.save()
                                print('La commune source peut à présent être supprimée depuis l\'interface d\'administration.')
                        else:
                            print('Abandon')
                    except:
                        print('Pas de commune ou de mairie avec ce code !')
                else:
                    print('Le code doit être décimal !')
            except:
                print('Pas de commune ou de mairie avec ce code !')
        else:
            print('Le code doit être décimal !')
