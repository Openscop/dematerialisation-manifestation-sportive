# coding: utf-8
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
import dns.resolver, dns.name
from django.urls import reverse
from configuration.settings import MAIN_URL


class Command(BaseCommand):
    """
    Recherche les anomalies dans les adresses email des utilisateurs en se basant essentiellement sur la résolution
    des noms de domaine et l'entrée MX dans les zones DNS.
    """
    args = ''
    help = 'Recherche les anomalies dans les adresses email des utilisateurs'

    email_non_conforme = []
    email_dns_non_valide = []
    email_sans_mx = []
    email_sans_reponse_serveur = []
    email_timeout = []

    def chemin_fiche_user(self, user):
        base_url = MAIN_URL[:-1] if MAIN_URL[-1] == '/' else MAIN_URL
        return base_url + reverse('admin:core_user_change', args=(user.pk,))

    def handle(self, *args, **options):
        for user in get_user_model().objects.all():
            try:
                email = user.email
                host_str = email.split('@')[1]
                resolver = dns.resolver
                dns.resolver.query(host_str, 'MX', tcp=True)
            except IndexError:
                user_data = {'user': user, 'path': self.chemin_fiche_user(user)}
                self.email_non_conforme.append(user_data)
                print("Email non conformes : {} - {}".format(str(user), user.email))
            except dns.resolver.NoAnswer:
                user_data = {'user': user, 'path': self.chemin_fiche_user(user)}
                self.email_dns_non_valide.append(user_data)
                print("Non d'hôte inexistant : {} - {}".format(str(user), user.email))
            except dns.resolver.NoNameservers:
                user_data = {'user': user, 'path': self.chemin_fiche_user(user)}
                self.email_sans_reponse_serveur.append(user_data)
                print("Aucun serveur de nom ne répond à la requête : {} - {}".format(str(user), user.email))
            except dns.resolver.NXDOMAIN:
                user_data = {'user': user, 'path': self.chemin_fiche_user(user)}
                self.email_sans_mx.append(user_data)
                print("Pas d\'enregistrement MX : {} - {}".format(str(user), user.email))
            except dns.exception.Timeout:
                user_data = {'user': user, 'path': self.chemin_fiche_user(user)}
                self.email_timeout.append(user_data)
                print("Délai de réponse trop long pour : {} - {}".format(str(user), user.email))

        print('Nb d\'email dont le format n\'est pas valide : ', len(self.email_non_conforme))
        print([(data['user'].get_full_name(), '- Lien : ' + data['path']) for data in self.email_non_conforme])
        print('Nb d\'email dont le DNS n\'est pas valide : ', len(self.email_dns_non_valide))
        print([(data['user'].get_full_name(), '- Lien : ' + data['path']) for data in self.email_dns_non_valide])
        print('Nb d\'email dont le DNS n\'a pas d\'enregistrement MX : ', len(self.email_sans_mx))
        print([(data['user'].get_full_name(), '- Lien : ' + data['path']) for data in self.email_sans_mx])
        print('Nb d\'email pour lesquels aucun serveur de nom ne répond à la requête : ',
              len(self.email_sans_reponse_serveur))
        print([(data['user'].get_full_name(), '- Lien : ' + data['path']) for data in self.email_sans_reponse_serveur])
        print('Nb d\'email pour lesquels le délai de réponse excède 30s : ', len(self.email_timeout))
        print([(data['user'].get_full_name(), '- Lien : ' + data['path']) for data in self.email_timeout])
