# coding: utf-8
from django.core.management.base import BaseCommand

from structure.models.service import ServiceInstructeur


class Command(BaseCommand):
    """
    Commande pour reconfigurer le paramètre autorise_acces_consult_organisateur_b des mairies à True
    """
    def handle(self, *args, **options):
        for index, service in enumerate(ServiceInstructeur.objects.filter(commune_m2m__isnull=False)):
            if index % 500 == 0:
                print('+', end='')
            service.autorise_acces_consult_organisateur_b = True
            service.save(skip_instructeur=True)



