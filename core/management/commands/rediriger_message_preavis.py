from django.core.management.base import BaseCommand
from django.db import transaction

from instructions.models import Avis
from messagerie.models import Message


class Command(BaseCommand):
    """
    commande pour remplir le champ instance_fk des structures organisatrices
    créées après le transfert V5 -> V6
    """
    args = ''
    help = 'Mettre à jour le champ instance_fk des structures organisatrices'

    def handle(self, *args, **options):
        with transaction.atomic():
            index = 0
            print('')
            print('rediriger préavis : ', end='')
            for index, mess in enumerate(Message.objects.filter(content_type_id=120)):
                error = 0
                try:
                    if index % 500 == 0:
                        print('+', end='')
                    if Avis.objects.filter(ancien_preavis=str(mess.object_id)).exists():
                        nouvel_avis = Avis.objects.get(ancien_preavis=str(mess.object_id))
                        mess.content_type_id = 119
                        mess.object_id = nouvel_avis.pk
                    else:
                        mess.content_type_id = None
                        mess.object_id = None
                    mess.save()
                except:
                    if error == 10:
                        break
                    error += 1
                    print(f'Erreur sur message {str(mess.pk)}')
            print('')
            print(f'total : {str(index + 1)}')
