# coding: utf-8
"""
Verifie la précense de fichier inscrit dans la database sur le systeme de fichier
"""
import os
import json
from django.core.management.base import BaseCommand
from evenements.models import Manif
from instructions.models import PieceJointeAvis, DocumentOfficiel, Avis


class Command(BaseCommand):

    def handle(self, *args, **options):
        """
        Exécuter la commande
        Créer un fichier json qui affiche les resultats
        """
        tab_final = []
        for manif in Manif.objects.all():
            tab_manif = []
            liste_pj = manif.piece_jointe.all()
            for pj in liste_pj:
                tab_manif.append(pj.document_attache.name) if pj.document_attache and not os.path.isfile(pj.document_attache.path) else ""
            if hasattr(manif, 'instruction'):
                for doc in DocumentOfficiel.objects.filter(instruction__manif=manif):
                    tab_manif.append(doc.fichier.name) if doc.fichier and not os.path.isfile(doc.fichier.path) else ""
                for avis in Avis.objects.filter(instruction__manif=manif).exclude(avis_parent_fk__isnull=False):
                    for doc in PieceJointeAvis.objects.filter(avis=avis):
                        tab_manif.append(doc.fichier.name) if doc.fichier and not os.path.isfile(doc.fichier.path) else ""
                    for preavis in avis.filter(avis_parent_fk=avis):
                        for doc in PieceJointeAvis.objects.filter(avis=preavis):
                            tab_manif.append(doc.fichier.name) if doc.fichier and not os.path.isfile(doc.fichier.path) else ""
            if tab_manif:
                tab_final.append({"pk": manif.pk, "result": tab_manif})
        f = open("result.json", "w+")
        jsonresult = json.dumps(tab_final)
        f.write(str(jsonresult))
        f.close()
