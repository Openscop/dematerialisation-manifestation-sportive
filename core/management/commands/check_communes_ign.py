# coding: utf-8
import os
import csv

from django.core.management.base import BaseCommand

from administrative_division.models import Commune
from administrative_division.models.departement import LISTE_DEPARTEMENT


def dep_name(item):
    return [dep[1] for dep in LISTE_DEPARTEMENT if dep[0] == item][0]


class Command(BaseCommand):
    """ Vérification des communes de la BD """
    args = ''
    help = 'Vérifier les communes de la BD par rapport à un fichier csv tiré des couches IGN'

    def handle(self, *args, **options):
        """ Exécuter la commande """
        liste_communes = Commune.objects.all()
        print('fichier : /tmp/communes_2022-07.csv')
        fichier = open(os.path.join('/tmp/communes_2022-07.csv'))
        tableaucsv = csv.reader(fichier, delimiter=',')
        for index, item in enumerate(tableaucsv):
            pass
        print(f"Nombre d'entrée du fichier : {index}")
        fichier.seek(0)
        print(f"Nombre de communes de la BD : {len(liste_communes)}")
        comm_ok, comm_in, comm_out = [], [], []
        fail_1, fail_2, fail_3, fail_4 = "", "", "", ""
        print('GO ?')
        input()
        for index, item in enumerate(tableaucsv):
            if index == 0:
                # titre des colonnes
                continue
            if index % 500 == 0:
                print('+', end='')
            code = item[3].zfill(5)
            dep = item[8].zfill(2)
            if not liste_communes.filter(code=code).exists():
                fail_1 += f"Entrée inconnue dans la BD : {code} - {item[1]} ({dep}){chr(10)}"
                comm_out.append(code)
                continue
            comm = liste_communes.get(code=code)
            comm_in.append(comm.pk)
            ok = True
            if not comm.name == item[1]:
                fail_2 += f"nom de la commune différent : " \
                          f"{code} - {comm.name} -> {item[1]}{chr(10)}"
                ok = False
            if not comm.arrondissement.code == item[8] + item[7]:
                fail_3 += f"code de l'arrondissement différent : {code} - {comm.name} - " \
                          f"{comm.arrondissement.code} -> {dep + item[7]}{chr(10)}"
                ok = False
            if not comm.arrondissement.departement.name == dep:
                fail_4 += f"code du département différent : {code} - {comm.name} - " \
                          f"{comm.arrondissement.departement.name} -> {dep}{chr(10)}"
                ok = False
            if ok:
                comm_ok.append(comm.pk)
        print()
        print(f"Nombre de communes checkées : {len(comm_in)}")
        print(f"Nombre de communes ok : {len(comm_ok)}")
        print(f"Nombre de communes hors BD : {len(comm_out)}")
        comm_none = liste_communes.exclude(pk__in=comm_in)
        print(f"Nombre de communes non checkées : {len(comm_none)}")
        fail_7 = ""
        # for comm in comm_none.order_by('arrondissement__departement__name'):
        #     fail_7 += f"commune absente du fichier : {comm.code} - " \
        #               f"{comm.name} ({comm.arrondissement.departement.name}){chr(10)}"
        liste_fichiers = [(fail_1, "chk_result_code_inconnu_BD"), (fail_2, "chk_result_nom_comm_diff"),
                          (fail_3, "chk_result_code_arron_diff"), (fail_4, "chk_result_code_dep_diff"),
                          (fail_7, "chk_result_comm_absentes")]
        for fichier in liste_fichiers:
            if fichier[0]:
                f = open(f"/tmp/{fichier[1]}.txt", "a+")
                f.write(str(fichier[0]))
                f.close()
