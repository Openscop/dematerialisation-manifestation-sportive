# coding: utf-8
"""
recontruit le système de fichier
"""
import os
import shutil
import random
from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction

from evenements.models import Manif
from instructions.models import PieceJointeAvis, DocumentOfficiel, Avis



def deplacement(file, manif):

    # generation du chemin de la manif
    dossier, base_filename = os.path.split(file.path)
    departement_number = manif.get_instance().get_departement_name()
    # création du dossier s'il n'existe pas
    if not os.path.exists(settings.MEDIA_ROOT+departement_number+'/'+str(manif.pk)):
        os.makedirs(settings.MEDIA_ROOT+departement_number+'/'+str(manif.pk))
    # si le fichier existe dejà on met un numero aléatoire devant le nom
    if os.path.exists(settings.MEDIA_ROOT+departement_number+"/"+str(manif.pk)+'/'+base_filename):
        rand = str(random.randint(1, 99999))
        shutil.move(file.path, settings.MEDIA_ROOT + departement_number + "/" + str(manif.pk) + '/' + rand + base_filename)
        return departement_number+"/"+str(manif.pk)+'/'+rand+base_filename
    # sinon on le deplace
    else:
        shutil.move(file.path, settings.MEDIA_ROOT + departement_number + "/" + str(manif.pk) + '/')
        return departement_number+"/"+str(manif.pk)+'/'+base_filename


class Command(BaseCommand):

    def handle(self, *args, **options):
        """ Exécuter la commande """

        cpt_obj = 0
        with transaction.atomic():
            for manif in Manif.objects.all():

                # Vérification du path des fichiers, de la forme dep/pk/nom. Si plus long, indique un ancien chemin de fichiers
                for pj in manif.piece_jointe.all():
                    if pj.document_attache and os.path.isfile(pj.document_attache.path):
                        if len(pj.document_attache.name.split('/')) >= 4:
                            pj.document_attache.name = deplacement(pj.document_attache, manif)
                            pj.save()
                            cpt_obj += 1

                if hasattr(manif, 'instruction'):
                    for doc in DocumentOfficiel.objects.filter(instruction__manif=manif):
                        if doc.fichier and os.path.isfile(doc.fichier.path):
                            if len(doc.fichier.name.split('/')) >= 4:
                                doc.fichier.name = deplacement(doc.fichier, manif)
                                doc.save()
                                cpt_obj += 1

                    for avis in Avis.objects.filter(instruction__manif=manif).exclude(avis_parent_fk__isnull=False):
                        for doc in PieceJointeAvis.objects.filter(avis=avis):
                            if doc.fichier and os.path.isfile(doc.fichier.path):
                                if len(doc.fichier.name.split('/')) >= 4:
                                    doc.fichier.name = deplacement(doc.fichier, manif)
                                    doc.save()
                                    cpt_obj += 1
                        for preavis in avis.filter(avis_parent_fk=avis):
                            for doc in PieceJointeAvis.objects.filter(avis=preavis):
                                if doc.fichier and os.path.isfile(doc.fichier.path):
                                    if len(doc.fichier.name.split('/')) >= 4:
                                        doc.fichier.name = deplacement(doc.fichier, manif)
                                        doc.save()
                                        cpt_obj += 1
            print(str(cpt_obj) + " documents déplacés")
