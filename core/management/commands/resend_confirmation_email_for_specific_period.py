# coding: utf-8
from django.utils import timezone
from datetime import timedelta
from django.core.management.base import BaseCommand
from post_office.models import Email
from allauth.account.models import EmailAddress


class Command(BaseCommand):
    """
    Commande pour renvoyer un nouveau lien de confirmation de comptes aux personnes n'ayant pas reçu leur lien depuis X jours.
    """
    def handle(self, *args, **options):
        print("Choisissez sur combien de jours les emails devront être renvoyés")
        nombre_de_jours = input()
        count = 0
        # on récupère les emails datés entre aujourd'hui et le nombre de jours renseignés qui ont le statut 1 (failed) et le sujet "Confirmer l'adresse email' en distinguant chaque email
        emails_failed = Email.objects.filter(created__gte=timezone.now() - timedelta(days=int(nombre_de_jours)),
                                             status=1,
                                             subject="[Manifestation Sportive] Confirmer l'adresse email").distinct('to')
        for email in emails_failed:  # on parcourt la liste des emails
            email_exist = EmailAddress.objects.filter(email=email.to[0]).exists()  # vérification que l'email existe en BDD
            if email_exist:  # si l'email existe
                email_cible = EmailAddress.objects.filter(email=email.to[0]).first() # récupération de l'adresse mail
                if not email_cible.verified:  # dans le cas ou le mail n'est pas vérifié
                    email_cible.send_confirmation()  # renvoyer un mail de confirmation aux utilisateurs
                    count += 1  # incrémentation du compteur pour indiquer le nombre d'emails renvoyés
        print(f"{str(count)} emails ont été renvoyés !")
