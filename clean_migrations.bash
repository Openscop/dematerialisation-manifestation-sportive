#!/bin/bash
echo "Attention, ce script doit être exécuté dans la racine du projet. L'environnement python ne doit pas s'y trouver."
touch

python manage.py migrate
python manage.py migrate --fake core zero
python manage.py migrate --fake sports zero
python manage.py migrate --fake administrative_division zero
python manage.py migrate --fake forum zero
python manage.py migrate --fake post_office zero
python manage.py migrate --fake auth zero
python manage.py migrate --fake sessions zero
python manage.py migrate --fake sites zero
python manage.py migrate --fake django_cron zero
python manage.py migrate --fake contenttypes zero
python manage.py migrate --fake carto zero

python manage.py showmigrations

find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
find . -path "*/migrations/*.pyc"  -delete

python manage.py makemigrations
python manage.py showmigrations
sed -i 's/settings.AUTH_USER_MODEL/"core.User"/g' forum/forum_conversation/migrations/0001_initial.py

python manage.py migrate --fake core
python manage.py migrate --fake oauth2_provider
python manage.py migrate --fake sessions
python manage.py migrate --fake webpush
python manage.py migrate --fake messagerie
python manage.py migrate --fake post_office
python manage.py migrate --fake sites
python manage.py migrate --fake instructions
python manage.py migrate --fake forum
python manage.py migrate --fake django_cron
python manage.py migrate --fake


python manage.py showmigrations
