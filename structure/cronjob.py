from post_office import mail

from django_cron import CronJobBase, Schedule
from django_cron.models import CronJobLog
from django.utils import timezone
from django.db.models import Q
from django.conf import settings

from structure.models import InvitationOrganisation, ReunionCDSR, ServiceConsulte, ServiceCDSR
from core.models import ConfigurationGlobale
from messagerie.models.message import Message


class UpdateInvitation(CronJobBase):
    """
    Marquer les invitations à l'expiration de la durée de 30 jours
    Avertir 1 jour avant expiration
    """
    RUN_AT_TIME = ['6:05']  # à lancer à 6h05

    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'structure.UpdateInvitation'    # un code unique

    def do(self):
        if not ConfigurationGlobale.objects.get(pk=1).UpdateInvitation:
            return 'Cron désactivé'
        # Purger les logs
        CronJobLog.objects.filter(code="structure.UpdateInvitation").filter(is_success=True).filter(
            message__isnull=True).filter(end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        log_retour = ""
        total_warn = 0
        total_expi = 0
        date_expi = timezone.now().date() - timezone.timedelta(days=31)
        date_warn = timezone.now().date() - timezone.timedelta(days=23)
        for invit in InvitationOrganisation.objects.filter(completee_b=False, expiree_b=False, revoquee_b=False):
            if invit.date_creation == date_warn:
                html_message = 'Bonjour,<br><br>' \
                               'Votre invitation à vous inscrire sur la plateforme ManifestationSportive ' \
                               'arrive à expiration.'
                try:
                    mail.send(
                        invit.email_s,
                        settings.DEFAULT_FROM_EMAIL,
                        subject="Invitation proche de l'expiration",
                        html_message=html_message,
                    )
                except:
                    pass
                total_warn += 1
            elif invit.date_creation == date_expi:
                invit.expiree_b = True
                invit.save()
                total_expi += 1

        log_retour += str(total_warn) + " invitations à 7 jours de l'expiration" + chr(10)
        log_retour += str(total_expi) + " invitations expirées" + chr(10)
        return log_retour


class RelanceReunionCDSR(CronJobBase):
    """
    Effectuer la relance des avis concernés par la réunion CDSR,
    la relance des demandes de confirmation de présence et le rappel de la réunion
    Avertir 7 jours avant réunion
    Effectuer la relance des envois de convocations à la réunion CDSR à j-15
    """
    # RUN_EVERY_MIN = 1
    # schedule = Schedule(run_every_mins=RUN_EVERY_MIN)
    RUN_AT_TIME = ['6:00']  # à lancer à 6h00

    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'structure.RelanceReunionCDSR'    # un code unique

    def do(self):
        if not ConfigurationGlobale.objects.get(pk=1).RelanceReunionCDSR:
            return 'Cron désactivé'
        # Purger les logs
        CronJobLog.objects.filter(code="structure.RelanceReunionCDSR").filter(is_success=True).filter(
            message__isnull=True).filter(end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        log_retour = ""
        for cdsr in ServiceCDSR.objects.all():
            delai_relance = cdsr.instance_fk.reunion_cdsr_delai_relance
            delai_convocation = cdsr.instance_fk.convocation_cdsr_delai_relance
            for reunion in cdsr.reunions.filter(date=timezone.now() + timezone.timedelta(days=delai_relance)):
                log_retour += f"-- relance réunion {cdsr} du {reunion}{chr(10)}"
                confirmation = 0
                rappel = 0
                # Relance de confirmation de présence ou rappel de la réunion
                if reunion.permanents_json and reunion.convocations_envoyes:
                    for item in reunion.permanents_json:
                        if not item['confirmé']:
                            if ServiceConsulte.objects.filter(pk=item['membre']).exists():
                                confirmation += 1
                                service = ServiceConsulte.objects.get(pk=item['membre'])
                                contenu = f"<p>Bonjour<br>" \
                                          f"Vous n'avez pas encore confirmé votre présence à la réunion CDSR du {reunion}.<br>" \
                                          f"Cette action est réalisable dans un des avis associés à cette réunion.</p>"
                                Message.objects.creer_et_envoyer(
                                    'action', None, service.get_utilisateurs_avec_service(),
                                    "Attente de confirmation de présence à une réunion CDSR",
                                    contenu, objet_lie_nature="reunion", objet_lie_pk=reunion.pk)
                        else:
                            if ServiceConsulte.objects.filter(pk=item['membre']).exists():
                                rappel += 1
                                service = ServiceConsulte.objects.get(pk=item['membre'])
                                contenu = f"<p>Bonjour<br>" \
                                          f"Pour rappel, Vous êtes convié à la réunion CDSR du {reunion}.<br>" \
                                          f"Vous avez déjà confirmé votre présence.</p>"
                                Message.objects.creer_et_envoyer(
                                    'info_suivi', None, service.get_utilisateurs_avec_service(),
                                    "Rappel pour une réunion CDSR", contenu,
                                    objet_lie_nature="reunion", objet_lie_pk=reunion.pk)

                for presentation in reunion.presentations.all():
                    # Relance de confirmation de présence ou rappel de la réunion
                    if presentation.invites_json and presentation.convocations_envoyes:
                        for item in presentation.invites_json[1:]:
                            if not item['confirmé']:
                                if ServiceConsulte.objects.filter(pk=item['membre']).exists():
                                    confirmation += 1
                                    service = ServiceConsulte.objects.get(pk=item['membre'])
                                    contenu = f"<p>Bonjour<br>" \
                                              f"Vous n'avez pas encore confirmé votre présence à la réunion CDSR du {reunion}.<br>" \
                                              f'Le dossier "{presentation.instruction_fk}" est présenté à {presentation.heure.strftime("%Hh%M")}.<br>' \
                                              f"Cette action est réalisable dans l'avis associé à cette présentation.</p>"
                                    Message.objects.creer_et_envoyer(
                                        'action', None, service.get_utilisateurs_avec_service(),
                                        "Attente de confirmation de présence à une réunion CDSR",
                                        contenu, manifestation_liee=presentation.instruction_fk.manif,
                                        objet_lie_nature="presentation", objet_lie_pk=presentation.pk)
                            else:
                                if ServiceConsulte.objects.filter(pk=item['membre']).exists():
                                    rappel += 1
                                    service = ServiceConsulte.objects.get(pk=item['membre'])
                                    contenu = f"<p>Bonjour<br>" \
                                              f"Pour rappel, Vous êtes convié à la réunion CDSR du {reunion}.<br>" \
                                              f"Vous avez déjà confirmé votre présence.</p>"
                                    Message.objects.creer_et_envoyer(
                                        'info_suivi', None, service.get_utilisateurs_avec_service(),
                                        "Rappel pour une réunion CDSR",
                                        contenu, manifestation_liee=presentation.instruction_fk.manif,
                                        objet_lie_nature="presentation", objet_lie_pk=presentation.pk)
                    # Relance des avis
                    if presentation.convocations_envoyes:
                        liste_avis = presentation.instruction_fk.avis.filter(~Q(etat='rendu'))
                        for avis in liste_avis:
                            if avis.service_consulte_origine_fk in presentation.get_tous_membres():
                                avis.notifier_creation_avis(origine=avis.get_service(), agents=avis.get_agents_avec_service())
                                log_retour += f"avis {str(avis)} pour l'instruction {presentation.instruction_fk} relancé{chr(10)}"
                log_retour += f" {str(confirmation)} confirmations de présence rappelées{chr(10)}"
                log_retour += f" {str(rappel)} rappels de réunions CDSR envoyés{chr(10)}"

            for reunion in ReunionCDSR.objects.filter(date=timezone.now() + timezone.timedelta(days=delai_convocation)):
                log_retour += f"-- convocations réunion CDSR du {reunion}{chr(10)}"
                convocation = 0
                # Relance des envois de convocations aux réunions CDSR
                if not reunion.convocations_envoyes:
                    convocation += 1
                    contenu = f"<p>Bonjour<br>" \
                              f"Vous n'avez pas encore envoyé la convocation des membres permanents à la réunion CDSR du {reunion}.<br>" \
                              f"Cette action est réalisable dans l'onglet correspondant à cette réunion,<br>" \
                              f"accessible par le bouton présent dans votre tableau de bord.</p>"
                    Message.objects.creer_et_envoyer(
                        'action', None, reunion.service_cdsr.get_utilisateurs_avec_service(),
                        "Envoi de la convocation des membres permanents à une réunion CDSR",
                        contenu, objet_lie_nature="reunion", objet_lie_pk=reunion.pk)
                for presentation in reunion.presentations.all():
                    if not presentation.convocations_envoyes:
                        convocation += 1
                        contenu = f"<p>Bonjour<br>" \
                                  f"Vous n'avez pas encore envoyé la convocation des membres invités à la réunion CDSR du {reunion}.<br>" \
                                  f'Le dossier "{presentation.instruction_fk}" est présenté à {presentation.heure.strftime("%Hh%M")}.<br>' \
                                  f"Cette action est réalisable dans l'onglet CDSR de l'instruction du dossier.</p>"
                        Message.objects.creer_et_envoyer(
                            'action', None, presentation.instruction_fk.get_instructeurs_avec_services(),
                            "Envoi de la convocation des membres invités à une réunion CDSR",
                            contenu, manifestation_liee=presentation.instruction_fk.manif,
                            objet_lie_nature="presentation", objet_lie_pk=presentation.pk)
                log_retour += f" {str(convocation)} rappels de convocations à envoyer{chr(10)}"
        return log_retour
