from django.urls import path

from .views.ajax import *
from .views.administration_des_services import *
from .views.edition_organisation_graphique import EditionOrganisationGraphique, EditionOrganisationGraphiqueAjax
from .views.interaction import *
from .views.modification_champ_service import *
from .views.user import *
from .views.fusion_service import FusionServiceView
from .views.cdsr import *

app_name = 'structure'
urlpatterns = [
    path('fiche-organisation/<pk>/', FicheOrganisationView.as_view(), name='fiche_organisation'),
    path('liste/types_service/', ListeTypeServiceView.as_view(), name='liste_types_service'),
    path('administration_des_services/', AdministrationServiceView.as_view(), name='administration_des_services'),
    path('liste_user_sans_service/', UserSansServiceListView.as_view(), name='liste_user_sans_service'),
    path('comptes_dormants/', ComptesDormantsView.as_view(), name='comptes_dormants'),

    path('administration_des_services/is_admin_famille/', IsAdminFamilleAjaxView.as_view(), name="is_admin_famille"),
    path('administration_des_services/is_habilite/<pk>/', IsHabiliteAjaxView.as_view(), name="is_famille"),

    path('update_service_consulte_active/<pk>/', ServiceConsulteUpdateActiveView.as_view(), name="update_service_consulte_active"),
    path('update_service_consulte_instance/<pk>/', ServiceConsulteUpdateInstanceView.as_view(), name="update_service_consulte_instance"),
    path('update_service_consulte_site/<pk>/', ServiceConsulteUpdateSiteView.as_view(), name="update_service_consulte_site"),
    path('update_service_consulte_action/<pk>/', ServiceConsulteUpdateActionView.as_view(), name="update_service_consulte_action"),
    path('update_service_consulte_nom/<pk>/', ServiceConsulteUpdateNomView.as_view(), name="update_service_consulte_nom"),
    path('update_service_consulte_type/<pk>/', ServiceConsulteUpdateTypeView.as_view(), name="update_service_consulte_type"),
    path('update_service_consulte_parent/<pk>/', ServiceConsulteUpdateParentView.as_view(), name="update_service_consulte_parent"),
    path('update_service_consulte_secteur/<pk>/', ServiceConsulteUpdateSecteurView.as_view(), name="update_service_consulte_secteur"),
    path('update_service_consulte_adresse/<pk>/', ServiceConsulteUpdateAdresseView.as_view(), name="update_service_consulte_adresse"),
    path('update_service_consulte_email/<pk>/', ServiceConsulteUpdateEmailView.as_view(), name="update_service_consulte_email"),
    path('update_service_consulte_telephone/<pk>/', ServiceConsulteUpdateTelephoneView.as_view(), name="update_service_consulte_telephone"),
    path('update_service_consulte_delai_avis/<pk>/', ServiceConsulteUpdateDelaiAvisView.as_view(), name="update_service_consulte_delai_avis"),
    path('update_service_consulte_delai_preavis/<pk>/', ServiceConsulteUpdateDelaiPreavisView.as_view(), name="update_service_consulte_delai_preavis"),

    path('update_user_email/<pk>/', UserUpdateEmailView.as_view(), name="update_user_email"),
    path('delete_user_email/', UserDeleteEmailView.as_view(), name="delete_user_email"),
    path('update_user_active/<pk>/', UserUpdateActiveView.as_view(), name="update_user_active"),
    path('update_user_organisation/<service_id>/<pk>/', UserOrganisationnUpdateView.as_view(), name="update_user_organisation"),
    path('remove_user_from_organisation/', RemoveUserOrganisationView.as_view(), name="remove_user_from_organisation"),
    path('add_user_organisation/', AddUserOrganisationView.as_view(), name="add_user_organisation"),
    path('gerer-user-organisation/<pk>/', GererUserOrganisationView.as_view(), name="gerer-user-organisation"),
    path('gerer-membre-cdsr/<pk>/', GererUserOrganisationView.as_view(), name="gerer-membre-cdsr"),

    path('organisation/<pk>/historique/', OrganisationHistoriqueView.as_view(), name='historique_organisation'),
    path('user/<pk>/historique/', UserHistoriqueView.as_view(), name='historique_user'),

    path('create_service_consulte/', ServiceConsulteCreateView.as_view(), name="create_service_consulte"),
    path('create_federation/', FederationCreateView.as_view(), name="create_federation"),
    path('create_service_instructeur/', ServiceInstructeurCreateView.as_view(), name="create_service_instructeur"),
    path('update_structure_organisatrice/<pk>/', StructureOrganisatriceUpdateView.as_view(), name="update_structure_organisatrice"),
    path('create_structure_organisatrice/', StructureOrganisatriceCreateView.as_view(), name="create_structure_organisatrice"),
    path('create_interaction/<service_id>/', ServiceConsulteInteractionCreateView.as_view(), name="create_interaction"),
    path('update_interaction/<service_id>/<pk>/', ServiceConsulteInteractionUpdateView.as_view(), name="update_interaction"),

    path('ajax/delete_interaction/', DeleteInteraction.as_view(), name='delete_interaction'),
    # Widget AJAX
    path('ajax/servicelist/', ServiceListAJAXView.as_view(), name='service_list'),
    path('ajax/categorielist/', CategorieListAJAXView.as_view(), name='categorie_list'),
    path('ajax/agentlist/', AgentListAjaxView.as_view(), name='agent_list'),
    path('ajax/sous_service/', SousServiceListAjaxView.as_view(), name='sous_service_list'),
    path('ajax/organisation_detail/', OrganisationDetailAjaxView.as_view(), name='organisation_detail'),
    path('ajax/user_detail/', UserDetailAjaxView.as_view(), name='user_detail'),
    path('ajax/invitation/', InvitationAjaxView.as_view(), name="invitation_inscription"),

    path('fusion_service/<pk_afusion>/', FusionServiceView.as_view(), name="fusion_service"),
    path('edition_organisation/<pk_organisation>/', EditionOrganisationGraphique.as_view(), name="edition_organisation"),
    path('update_service_consulte_action/<pk>/', ServiceConsulteUpdateActionView.as_view(), name="update_service_consulte_action"),
    path('ajax/edition_organisation/<pk_organisation>/', EditionOrganisationGraphiqueAjax.as_view(), name='edition_organisation_ajax'),

    path('CDSR/liste/', ReunionCDSRListView.as_view(), name="reunionCDSR_list"),
    path('CDSR/add', ReunionCDSRCreateView.as_view(), name="reunionCDSR_create"),
    path('CDSR/<pk>/', ReunionCDSRDetailView.as_view(), name="reunionCDSR_detail"),
    path('CDSR/<pk>/update', ReunionCDSRUpdateView.as_view(), name="reunionCDSR_update"),
    path('CDSR/<pk>/delete', ReunionCDSRDeleteView.as_view(), name="reunionCDSR_delete"),
    path('CDSR/<pk>/convoquer', ReunionCDSRConvoquerView.as_view(), name="reunionCDSR_convoquer"),
    path('ServiceCDSR/<pk>/add', ServiceCDSRAddMemberAjaxView.as_view(), name="ajouter_membre_cdsr_ajax"),
    path('ServiceCDSR/<pk>/delete', ServiceCDSRDeleteMemberAjaxView.as_view(), name="retirer_membre_cdsr_ajax"),
    path('CDSR/presentation/<pk>/delete', PresentationCDSRDeleteView.as_view(), name="presentationcdsr_remove"),
    path('CDSR/presentation/<pk>/date/reset', PresentationCDSRDateResetView.as_view(), name="presentationcdsr_reset_date"),
    path('CDSR/presentation/<pk>/date/set', PresentationCDSRDateSetView.as_view(), name="presentationcdsr_set_date"),
    path('CDSR/presentation/<pk>/heure/set', PresentationCDSRHeureSetView.as_view(), name="presentationcdsr_set_heure"),
    path('CDSR/presentation/<pk>/convoquer', PresentationCDSRConvoquerView.as_view(), name="presentationcdsr_convoquer"),
    path('CDSR/presentation/<pk>/confirmer', PresentationCDSRConfirmerView.as_view(), name="presentationcdsr_confirmer"),
    path('CDSR/presentation/<pk>/invite/add', PresentationCDSRInviteAddAjaxView.as_view(), name="ajouter_invite_ajax"),
    path('CDSR/presentation/<pk>/invite/delete', PresentationCDSRInviteDeleteAjaxView.as_view(), name="retirer_invite_ajax"),
]
