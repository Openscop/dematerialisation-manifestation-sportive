from django.db import models, router
from django.conf import settings
from django.db.models.deletion import Collector
from django.utils import timezone

from polymorphic.models import PolymorphicModel
from post_office import mail


class Organisation(PolymorphicModel):
    """
    Classe générique du service d'un utilisateur
    """
    nom_s = models.CharField(max_length=500, default="", verbose_name="Nom de l'organisation (calculé automatiquement)", blank=True, null=True)
    precision_nom_s = models.CharField(max_length=500, default="", verbose_name="Précision du nom", blank=True, null=True)
    adresse_s = models.TextField(max_length=1000, default="", verbose_name="Adresse complète", blank=True, null=True)
    telephone_s = models.CharField(max_length=20, default="", verbose_name="Téléphone", blank=True)
    email_s = models.EmailField(verbose_name="Email", null=True, blank=True)
    site_web_s = models.CharField(max_length=500, default="", verbose_name="Site internet", blank=True)
    instance_fk = models.ForeignKey('core.Instance', verbose_name="Instance principale de l'organisation", null=True, blank=True, on_delete=models.SET_NULL)
    notification_email_json = models.JSONField(max_length=500, blank=True, null=True, verbose_name="Paramètre de notification d'email")
    national_b = models.BooleanField(default=False, verbose_name="Est une organisation nationale")
    region_m2m = models.ManyToManyField("administrative_division.Region", blank=True, verbose_name="Region")
    departement_m2m = models.ManyToManyField("administrative_division.Departement", blank=True, verbose_name="Département")
    arrondissement_m2m = models.ManyToManyField("administrative_division.Arrondissement", blank=True, verbose_name="Arrondissement", related_name="organisations")
    commune_m2m = models.ManyToManyField("administrative_division.Commune", blank=True, verbose_name="Commune")

    history_json = models.JSONField(verbose_name="Historique des modifications", blank=True, null=True, max_length=100000)

    # TODO a supprimer
    old_model_json = models.JSONField(max_length=500, null=True, blank=True, verbose_name="Ancien model si necessaire (name, pk)")

    def __str__(self):
        if self.nom_s:
            return self.nom_s
        else:
            return self.set_nom_s()

    def set_nom_s(self):
        if self.precision_nom_s:
            return self.precision_nom_s
        else:
            return f"{type(self)} - {str(self.pk)}"

    def save(self, *args, **kwargs):
        if self.pk:
            self.nom_s = self.set_nom_s()
        super(Organisation, self).save(*args, **kwargs)

    def delete(self, using=None, keep_parents=False):
        if self.is_service_consulte():
            from instructions.models.avis import Avis
            if Avis.objects.filter(service_consulte_history_m2m=self).exists() \
                    or Avis.objects.filter(service_consulte_fk=self).exists():
                raise ValueError(
                    "%s ne peut pas être supprimer car des avis lui ont été adressé" % self
                )
        return super().delete()

    def has_email_failed(self):
        """Retourne True si le service à plus de deux erreurs de délivrance de mail """
        from core.models.errorsmtp import ErrorSMTP
        return ErrorSMTP.objects.filter(service=self).count() > 1

    def get_email_failed(self):
        """Retourne les erreurs smtp si le service à plus de deux erreurs de délivrance de mail """
        from core.models.errorsmtp import ErrorSMTP
        if ErrorSMTP.objects.filter(service=self).count() > 1:
            error_smtp = ErrorSMTP.objects.filter(service=self, resolu=False)
            return error_smtp
        return False

    def is_structure_organisatrice(self):
        return True if type(self) == StructureOrganisatrice else False

    def is_service_consulte(self):
        from structure.models import ServiceConsulte, ServiceInstructeur
        return True if type(self) == ServiceConsulte or type(self) == ServiceInstructeur else False

    def is_service_instructeur(self):
        from structure.models import ServiceInstructeur
        return True if type(self) == ServiceInstructeur else False

    def is_service_cdsr(self):
        from structure.models import ServiceCDSR
        return True if type(self) == ServiceCDSR else False

    def is_membre_cdsr(self):
        from structure.models import ServiceCDSR
        if ServiceCDSR.objects.filter(instance_fk=self.instance_fk).exists():
            cdsr = ServiceCDSR.objects.get(instance_fk=self.instance_fk)
            return self in cdsr.membres_permanents_m2m.all()
        return False

    def is_mairie(self):
        return bool(self.is_service_instructeur() and self.commune_m2m.first())

    def is_support(self):
        if self.type_service_fk:
            return self.type_service_fk.pk == 2

    def is_federation(self):
        from structure.models import Federation
        return True if type(self) == Federation else False

    def get_type_s(self):
        if self.is_structure_organisatrice():
            return 'Structure organisatrice'
        if self.is_service_instructeur():
            return "Service instructeur"
        if self.is_federation():
            return "Fédération"
        if self.is_service_consulte():
            return "Service consulté"
        return "Organisation"

    def get_users_qs(self):
        """ Renvoye le QuerySet des utilisateurs lier au service """
        return self.users.order_by('-is_active', 'last_name')

    def get_users_actif_qs(self):
        """ Renvoye le QuerySet des utilisateurs actif lier au service """
        from structure.models import ServiceInstructeur
        if self.is_service_cdsr():
            if ServiceInstructeur.objects.filter(instance_fk=self.instance_fk, type_service_fk__nom_s="Préfecture").exists():
                pref = ServiceInstructeur.objects.filter(instance_fk=self.instance_fk, type_service_fk__nom_s="Préfecture").first()
                return pref.users.filter(is_active=True)
        return self.users.filter(is_active=True)

    def get_users_inactif_qs(self):
        """ Renvoye le QuerySet des utilisateurs inactif lier au service """
        return self.users.filter(is_active=False)

    def get_users_list(self):
        """ Renvoye la liste des utilisateurs lier au service """
        return [user for user in self.get_users_qs()]

    def get_utilisateurs_avec_service(self):
        """ Renvoye la liste des organisateurs avec le service pour l'envoi de messages """
        liste = []
        [liste.append([user, self]) for user in self.get_users_actif_qs()]
        return liste

    def get_niveau_geographique_str(self):
        """ Renvoye le niveau géographique d'une organisation"""
        if self.national_b:
            return 'National'
        if self.region_m2m.first():
            return 'Régional'
        if self.departement_m2m.first():
            return 'Départemental'
        if self.arrondissement_m2m.first():
            return 'Arrondissement'
        if self.commune_m2m.first():
            return 'Communal'

    def get_objets_geographique_qs(self):
        """ Renvoye le niveau géographique d'une organisation"""
        if self.national_b:
            return 'National'
        if self.region_m2m.first():
            return self.region_m2m.all()
        if self.departement_m2m.first():
            return self.departement_m2m.all()
        if self.arrondissement_m2m.first():
            return self.arrondissement_m2m.all()
        if self.commune_m2m.first():
            return self.commune_m2m.all()

    def get_manifs_ecriture(self):
        """
        manif à l'acces ecriture
        """
        if type(self) == StructureOrganisatrice:
            return self.manifs.all()
        from structure.models import ServiceInstructeur, ServiceConsulte, Federation
        from evenements.models import Manif
        if type(self) == ServiceInstructeur:
            manif_qs = Manif.objects.filter(instruction__in=self.instructions.all())
            manif_qs = (manif_qs | Manif.objects.filter(instruction__avis__in=(self.avis_instruis.all() | self.avis_lecture.all())))
            return manif_qs
        if type(self) == ServiceConsulte or type(self) == Federation:
            return Manif.objects.filter(instruction__avis__in=(self.avis_instruis.all() | self.avis_lecture.all()))

    def get_manifs_lecture(self):
        """
        liste des manifs à l'acces lecture
        """
        from evenements.models import Manif
        pk = []
        date = timezone.now().date()
        for a in self.autorisation_acces.filter():
            if not a.date_revocation or a.date_revocation > date:
                pk.append(a.manif.pk) if a.manif and a.manif.pk not in pk else ""
                pk.append(a.instruction.manif.pk) if a.instruction and a.instruction.manif.pk not in pk else ""
                pk.append(a.avis.instruction.manif.pk) if a.avis and a.avis.instruction.manif.pk not in pk else ""
        return Manif.objects.filter(pk__in=pk)

    def get_invitation(self):
        """
        Renvoyer la liste des invitations de l'organisation
        """
        return self.invitations.filter(completee_b=False, expiree_b=False, revoquee_b=False)


class StructureOrganisatrice(Organisation):
    """
    Organisation qui va créer des manifestations
    """
    type_organisation_fk = models.ForeignKey("structure.TypeOrganisation", null=True, blank=True, on_delete=models.CASCADE)


class TypeOrganisation(models.Model):
    """
    Type des organisations
    """
    nom_s = models.CharField(max_length=300, verbose_name="Nom du type")

    def __str__(self):
        return self.nom_s


class InvitationOrganisation(models.Model):
    """
    Invitation pour permettre l'inscription de nouveaux utilisateurs à une organisation
    """
    date_creation = models.DateField("date de création", auto_now_add=True)
    email_s = models.EmailField(verbose_name="Adresse email de l'invité")
    courriel = models.OneToOneField(mail.Email, on_delete=models.SET_NULL, null=True, blank=True)
    expiree_b = models.BooleanField(default=False, verbose_name="Invitation expirée")
    revoquee_b = models.BooleanField(default=False, verbose_name="Invitation révoquée")
    completee_b = models.BooleanField(default=False, verbose_name="Utilisateur inscrit")
    origine_fk = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, verbose_name="Créateur de l'invitation", on_delete=models.SET_NULL)
    service_fk = models.ForeignKey("structure.Organisation", verbose_name="service concerné", on_delete=models.CASCADE)
    token_s = models.CharField(max_length=250, verbose_name="jeton d'identification")

    def __str__(self):
        return f'invitation vers {self.email_s}'

    def est_delivree(self):
        if self.courriel and self.courriel.status == 0:
            return True
        return False
    est_delivree.short_description = "Invitation délivrée"
    est_delivree.boolean = True

    class Meta:
        verbose_name = "Invitation d'inscription"
        verbose_name_plural = "Invitations d'inscription"
        default_related_name = 'invitations'
        app_label = 'structure'
