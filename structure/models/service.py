from django.db import models
from django.db.models import Q
from django.conf import settings

from .organisation import Organisation
from core.tasks import set_instructeur


class ServiceConsulte(Organisation):
    """
    Service qui est consulté dans le cadre de l'instruction de la manifestation
    """
    is_active_b = models.BooleanField(default=True, verbose_name="Service actif")
    service_parent_fk = models.ForeignKey("self", null=True, blank=True, verbose_name="Service parent", on_delete=models.SET_NULL, related_name="service_enfant")
    type_service_fk = models.ForeignKey("structure.TypeService", null=True, verbose_name="Type de service", on_delete=models.SET_NULL)
    admin_service_famille_b = models.BooleanField(default=False, verbose_name="Administrer tous les services de sa famille")
    admin_utilisateurs_subalternes_b = models.BooleanField(default=False, verbose_name="Administrer ses agents subalternes")
    porte_entree_avis_b = models.BooleanField(default=False, verbose_name="Recevoir des demandes d'avis du service instructeur")
    autorise_rendre_avis_b = models.BooleanField(default=False, verbose_name="Rendre les avis au service instructeur")
    autorise_rendre_preavis_a_service_avis_b = models.BooleanField(default=False, verbose_name="Rendre les préavis directement au service en charge de l'avis")
    autorise_acces_consult_organisateur_b = models.BooleanField(default=False, verbose_name="Autorise les organisateurs à octroyer un accès en lecture à ce service")
    autorise_mandater_avis_famille_b = models.BooleanField(default=False, verbose_name="Mandater un membre de sa famille à traiter une de ses demande d'avis")
    autorise_interroge_preavis_famille_b = models.BooleanField(default=False, verbose_name="Interroger un membre de sa famille avec un préavis")
    delai_jour_avis_int = models.IntegerField(default=21, verbose_name="Définir le délai imposé pour rendre un avis en nombre de jours")
    delai_jour_preavis_int = models.IntegerField(default=21, verbose_name="Définir le délai imposé pour rendre un préavis en nombre de jours")

    def set_nom_s(self):
        territoire = ""
        nom = ""
        if self.national_b:
            territoire = f'National'
        if self.region_m2m.first():
            if self.region_m2m.all().count() > 1:
                territoire = f'({self.region_m2m.first().nom}...)'
            else:
                territoire = f'({self.region_m2m.first().nom})'
        if self.departement_m2m.first():
            territoire = '('
            if self.departement_m2m.all().count() > 4:
                for dep in self.departement_m2m.all()[:4]:
                    territoire += f'{dep.name} '
                territoire += "..."
            else:
                for dep in self.departement_m2m.all():
                    territoire += f'{dep.name} '
            territoire = territoire[:-1] + ')'
        if self.arrondissement_m2m.first():
            for arrondissement in self.arrondissement_m2m.all():
                territoire += f'{arrondissement.name} '
        if self.commune_m2m.first() and self.type_service_fk:
            for commune in self.commune_m2m.all():
                territoire += f'{commune.name} '
        if self.type_service_fk:
            if self.type_service_fk.abreviation_s:
                nom = self.type_service_fk.abreviation_s
            else:
                nom = self.type_service_fk.nom_s
        if self.precision_nom_s:
            nom = nom + " " + self.precision_nom_s
        if territoire:
            nom = nom + " " + territoire
        return nom

    def is_service_avis(self):
        if self.porte_entree_avis_b or not self.service_parent_fk:
            return True
        if self.get_interactions_entrantes().filter(type_lien_s__in=['mandater', 'adresser']).exists():
            return True
        return False

    def is_service_avis_and_preavis(self):
        if self.service_parent_fk and self.porte_entree_avis_b:
            return True
        return False

    def is_service_preavis(self):
        return bool(not self.is_service_avis_and_preavis and not self.is_service_avis)

    def get_children_qs(self):
        """Renvoyer un queryset des enfants direct du service"""
        return self.service_enfant.all()

    def get_all_children_list(self):
        """Renvoyer une list de tous les enfants d'un service"""
        enfants = [self]
        for enfant in self.get_children_qs():
            enfants.extend(enfant.get_all_children_list())
        return enfants

    def get_all_children_qs(self):
        """Renvoyer un queryset de tous les enfants d'un service"""
        enfants = self.get_children_qs()
        for enfant in enfants:
            enfants = enfants | enfant.get_all_children_qs()
        return enfants

    def get_service_parent_famille(self):
        """Renvoyer le service parent de la famille"""
        service_parent = self.service_parent_fk
        if not service_parent:
            return self
        if service_parent.service_parent_fk:
            service_parent = service_parent.get_service_parent_famille()
        return service_parent

    def get_all_service_parent_list(self):
        """Renvoyer tous les parents d'un service de plus proche au plus ancètre"""
        service = self
        services_parent = []
        while service.service_parent_fk:
            service = service.service_parent_fk
            services_parent.append(service)
        return services_parent

    def get_family_member_qs(self):
        """Renvoyer un queryset des services appartement a la même famille"""
        service_parent_famille = self.get_service_parent_famille()
        return service_parent_famille.get_all_children_qs() | ServiceConsulte.objects.filter(pk=service_parent_famille.pk)

    def get_family_hierarchie_list(self, recusion=False):
        """Renvoyer un queryset des services appartement a la même famille"""
        if not recusion:
            service_parent = self.get_service_parent_famille()
        else:
            service_parent = self
        service_list = [service.get_family_hierarchie_list(True) for service in service_parent.get_children_qs()]
        hierarchie_list = {service_parent: service_list}
        return hierarchie_list

    def get_all_agent_family_list(self):
        """Renvoyer une liste de tous les agents présents dans la famille du service"""
        agents = []
        services = self.get_family_member_qs()
        for service in services:
            for agent in service.get_users_qs():
                agents.append(agent)
        return agents

    def get_services_administrateurs(self):
        """Renvoyer le service administrateur du service"""
        return self.get_family_member_qs().filter(admin_service_famille_b=True)

    def get_services_interrogeable_qs(self):
        """Renvoyer les services interrogeables par le service"""
        if self.autorise_interroge_preavis_famille_b:
            return self.get_family_member_qs()
        else:
            return ServiceConsulte.objects.filter(interaction_sortante__service_entrant_fk=self,
                                                  interaction_sortante__type_lien_s="interroger")

    def get_subalterne_qs(self):
        """Renvoyer un queryset des services subalterne du service"""
        if self.service_parent_fk:
            return self.service_parent_fk.get_all_children_qs()
        else:
            return self.get_all_children_qs()

    def get_interactions_entrantes(self):
        """Renvoyer un queryset des interactions entrantes du service"""
        return ServiceConsulteInteraction.objects.filter(service_sortant_fk=self).order_by('type_lien_s')

    def get_interactions_sortantes(self):
        """Renvoyer un queryset des interactions sortantes du service"""
        return ServiceConsulteInteraction.objects.filter(service_entrant_fk=self).order_by('type_lien_s')

    def get_interactions(self):
        """Renvoyer un queryset de toutes les interactions du service"""
        return ServiceConsulteInteraction.objects.filter(Q(service_entrant_fk=self) | Q(service_sortant_fk=self)).distinct('pk')

    def get_nombre_avis(self):
        """Renvoyer le nombre d'avis reçus pas le service"""
        from instructions.models import Avis
        nb_avis = Avis.objects.filter(service_consulte_fk=self).count()
        return nb_avis

    def is_membre_famille(self):
        """Renvoyer true ou false selon si le service est autonome (false) ou appartient à une famille (true)"""
        return True if self.service_parent_fk or self.service_enfant.all() else False

    def verification_integrite_circuit_avis(self):
        """
        Vérification de l'integrité du circuit d'instruction d'avis de la famille
        A noter que les preavis ne peuvent être bloqués du fait que toute personne peut rendre
        A noter aussi que seule les interactions et les boolean porte d'entre et rendre avis sont pris en compte.
        Les autres booléan étant des pass droit et ne peuvent donc pas etre verifier
        return tab d'obj : [{"service_sortant_bloquant", "service_entrant_bloque", "type"}] tableau vide en cas d'aucune erreur
        """
        f = self.get_family_member_qs()
        tab_error = []
        tab_interaction = []
        for p in f.filter(porte_entree_avis_b=True):
            tab_service = []
            tab_service.append(p)
            while tab_service:
                for t in tab_service:
                    for i in ServiceConsulteInteraction.objects.filter(service_entrant_fk=t,
                                                                       type_lien_s__in=["mandater", "adresser"]):
                        tab_service.append(i.service_sortant_fk) if not i.service_sortant_fk in tab_interaction else ""
                    tab_interaction.append(t) if not t in tab_interaction else ""
                    tab_service.remove(t)
        for p in tab_interaction:
            sortie = False
            tab_service = []
            tab_service.append(p)
            while tab_service and not sortie:
                for t in tab_service:
                    if t.autorise_rendre_avis_b:
                        sortie = True
                    else:
                        for i in ServiceConsulteInteraction.objects.filter(service_entrant_fk=t,
                                                                           type_lien_s__in=["mandater", "adresser"]):
                            tab_service.append(i.service_sortant_fk)
                    tab_service.remove(t)
            if not sortie:
                if not list(filter(lambda x: x['service_sortant_bloquant'] == t, tab_error)):
                    tab_error.append(
                        {"service_sortant_bloquant": t, "service_entrant_bloque": p, "type": "Circuit avis incomplet"})
        return tab_error




    def get_edition_data(self):
        nodes, nodes_restant, edges, services = [], [], [], []
        for organisation in self.get_family_member_qs():
            # Si le service a des interactions on l'ajoute a "nodes"
            if organisation.get_interactions():
                for interaction in organisation.get_interactions_sortantes():
                    edges.append({
                        'source': str(interaction.service_entrant_fk.pk),
                        'target': str(interaction.service_sortant_fk.pk),
                        'type': interaction.type_lien_s,
                    })

                nodes.append({
                    'id': str(organisation.pk),
                    'label': organisation.nom_s,
                    'data': {
                        'admin_service_famille_b': organisation.admin_service_famille_b,
                        'admin_utilisateurs_subalternes_b': organisation.admin_utilisateurs_subalternes_b,
                        'porte_entree_avis_b': organisation.porte_entree_avis_b,
                        'autorise_rendre_avis_b': organisation.autorise_rendre_avis_b,
                        'autorise_rendre_preavis_a_service_avis_b': organisation.autorise_rendre_preavis_a_service_avis_b,
                        'autorise_acces_consult_organisateur_b': organisation.autorise_acces_consult_organisateur_b,
                        'autorise_mandater_avis_famille_b': organisation.autorise_mandater_avis_famille_b,
                        'autorise_interroge_preavis_famille_b': organisation.autorise_interroge_preavis_famille_b,
                    }
                })
            # Sinon on l'ajoute a "services"
            else:
                services.append({
                    'id': str(organisation.pk),
                    'label': organisation.nom_s,
                    'data': {
                        'admin_service_famille_b': organisation.admin_service_famille_b,
                        'admin_utilisateurs_subalternes_b': organisation.admin_utilisateurs_subalternes_b,
                        'porte_entree_avis_b': organisation.porte_entree_avis_b,
                        'autorise_rendre_avis_b': organisation.autorise_rendre_avis_b,
                        'autorise_rendre_preavis_a_service_avis_b': organisation.autorise_rendre_preavis_a_service_avis_b,
                        'autorise_acces_consult_organisateur_b': organisation.autorise_acces_consult_organisateur_b,
                        'autorise_mandater_avis_famille_b': organisation.autorise_mandater_avis_famille_b,
                        'autorise_interroge_preavis_famille_b': organisation.autorise_interroge_preavis_famille_b,
                    }
                })

        network = {'nodes': nodes, 'edges': edges}
        return {"network": network, "services": services}


class TypeService(models.Model):
    """
    Type de service
    """
    nom_s = models.CharField(max_length=150, default="", verbose_name="Nom")
    abreviation_s = models.CharField(max_length=50, default="", verbose_name="Abréviation", blank=True)
    categorie_fk = models.ForeignKey("structure.CategorieService", null=True, verbose_name="Catégorie", on_delete=models.SET_NULL)

    def __str__(self):
        if self.abreviation_s:
            return self.abreviation_s + " (" + self.nom_s + ")"
        else:
            return self.nom_s


class CategorieService(models.Model):
    """
    Catégorie du type de service
    """
    nom_s = models.CharField(max_length=150, default="", verbose_name="Nom")
    my_order = models.PositiveIntegerField(blank=False, null=False)

    def __str__(self):
        return self.nom_s

    class Meta:
        ordering = ['my_order']


class ServiceConsulteInteraction(models.Model):
    """
    Les interactions disponibles entre deux services consultés dans le cadre de la demande d'avis et de préavis
    - Mandater et adresser sont des interactions à sens unique
    - Interroger implique un retour
    """
    TYPE_LIEN = (('interroger', 'peut interroger'), ('mandater', 'peut mandater'), ('adresser', "peut adresser l'avis à"))

    service_entrant_fk = models.ForeignKey('structure.ServiceConsulte', verbose_name="service entrant", on_delete=models.CASCADE, related_name="interaction_entrante")
    service_sortant_fk = models.ForeignKey('structure.ServiceConsulte', verbose_name="service sortant", on_delete=models.CASCADE, related_name="interaction_sortante")
    type_lien_s = models.CharField(max_length=20, choices=TYPE_LIEN, null=True, verbose_name="Type d'interaction")


class ServiceInstructeur(ServiceConsulte):
    """
    Service consulté qui instruit une manifestation
    """
    TYPE_SERVICE_INSTRUCTEUR = (("prefecture", 'Préfecture'), ('sousprefecture', "Sous-Préfecture"), ('mairie', "Mairie"))

    type_service_instructeur_s = models.CharField(max_length=20, choices=TYPE_SERVICE_INSTRUCTEUR, verbose_name="Type de service instructeur", null=True, blank=True)

    def set_nom_s(self):
        if self.commune_m2m.first():
            return f'{self.commune_m2m.first().zip_code} {self.precision_nom_s}'
        if self.arrondissement_m2m.first():
            return f'{self.type_service_fk.nom_s} de {self.arrondissement_m2m.first()}'
        else:
            return "-"

    def save(self, skip_instructeur=False, *args, **kwargs):
        from structure.models import Federation
        retour = super(ServiceInstructeur, self).save(*args, **kwargs)
        if not skip_instructeur:  # pour empecher l'execution lors du script de migration
            for i in self.instructions.all():
                i.service_instructeur_m2m.remove(self)
            if settings.CELERY_ENABLED:
                set_instructeur.delay(self.instance_fk.pk)
            else:
                set_instructeur(self.instance_fk.pk)
        return retour


class FederationQuerySet(models.QuerySet):
    """ Queryset Federation """

    # Overrides
    def get_by_natural_key(self, email):
        return self.get(email=email)

    # Getter
    def get_for_departement(self, discipline, departement):
        """
        Renvoyer une fédération pour une discipline et un département

        Si une fédération n'existe pas pour le département, chercher
        pour la région du département, puis pour le pays.
        """
        federations = self.filter(departement_m2m=departement, discipline_fk=discipline)
        if federations.exists():
            return federations.first()

        if departement:
            federations = self.filter(region_m2m=departement.region, discipline_fk=discipline)
            if federations.exists():
                return federations.first()

        federations = self.filter(national_b=True, discipline_fk=discipline)
        if federations.exists():
            return federations.first()

        return None


class Federation(ServiceConsulte):
    """
    Service consulté étant une fédération
    """
    discipline_fk = models.ForeignKey('sports.discipline', null=True, blank=True, related_name='service_federation', verbose_name="Discipline", on_delete=models.SET_NULL)
    non_polymorphic_objects = FederationQuerySet.as_manager()

    class Meta:
        base_manager_name = 'non_polymorphic_objects'

    def set_nom_s(self):
        territoire = ""
        if self.national_b:
            territoire = f'National'
        if self.region_m2m.first():
            if self.region_m2m.all().count() > 1:
                territoire = f'({self.region_m2m.first().nom}...)'
            else:
                territoire = f'({self.region_m2m.first().nom})'
        if self.departement_m2m.first():
            territoire = '('
            if self.departement_m2m.all().count() > 4:
                for dep in self.departement_m2m.all()[:4]:
                    territoire += f'{dep.name} '
                territoire += "..."
            else:
                for dep in self.departement_m2m.all():
                    territoire += f'{dep.name} '
            territoire = territoire[:-1] + ')'
        if self.arrondissement_m2m.first():
            for arrondissement in self.arrondissement_m2m.all():
                territoire += f'{arrondissement.name} '
        if self.commune_m2m.first() and self.type_service_fk:
            for commune in self.commune_m2m.all():
                territoire += f'{commune.name} '

        return f"{self.type_service_fk} {territoire} - {self.discipline_fk}"

    def save(self, *args, **kwargs):
        self.porte_entree_avis_b = True
        self.autorise_rendre_avis_b = True
        if not self.pk:
            self.delai_jour_avis_int = 28
            self.delai_jour_preavis_int = 28
        super().save(*args, **kwargs)

