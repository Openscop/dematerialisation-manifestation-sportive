from django.db import models
from django.utils import timezone

from .service import ServiceConsulte
from .organisation import StructureOrganisatrice


class ServiceCDSR(ServiceConsulte):
    """
    Service spécial qui regroupe plusieurs services consultés en membres permanents
    """
    membres_permanents_m2m = models.ManyToManyField("structure.ServiceConsulte", blank=True, verbose_name="membres permanents", related_name="cdsr")


class ReunionCDSR(models.Model):
    """
    Objet qui regroupe toutes les présentations de dossier pour une même journée
    """
    nom_s = models.CharField("nom de la réunion", max_length=500, blank=True, null=True)
    service_cdsr = models.ForeignKey("structure.ServiceCDSR", blank=True, null=True, verbose_name="Service CDSR", related_name="reunions", on_delete=models.CASCADE)
    date = models.DateField("Date de tenue", blank=True, null=True)
    convocations_envoyes = models.DateTimeField("Date d'envoi des convocations", blank=True, null=True)
    # contenu du json : membre : pk de l'organisation, confirmé : bool, présent : bool
    permanents_json = models.JSONField("Membres permanents", blank=True, null=True, max_length=100000)
    nb_dossier_i = models.PositiveSmallIntegerField("Nombre de dossiers", default=0)
    history_json = models.JSONField(verbose_name="Historique des modifications", blank=True, null=True, max_length=100000)

    def __str__(self):
        if self.date:
            return self.date.strftime("%d-%b-%Y")
        elif self.nom_s:
            return self.nom_s
        else:
            return ""

    def get_membres_permanents(self):
        liste_membres = []
        if self.permanents_json:
            for item in self.permanents_json:
                pk = item['membre']
                if ServiceConsulte.objects.filter(pk=pk).exists():
                    liste_membres.append(ServiceConsulte.objects.get(pk=pk))
        return liste_membres

    def get_membres_permanents_avec_service(self):
        liste_destinataires = []
        if self.permanents_json:
            for item in self.permanents_json:
                pk = item['membre']
                if ServiceConsulte.objects.filter(pk=pk).exists():
                    service = ServiceConsulte.objects.get(pk=pk)
                    liste_destinataires += service.get_utilisateurs_avec_service()
        return liste_destinataires

    def aff_membres_permanents(self):
        liste_membres = []
        if self.permanents_json:
            for item in self.permanents_json:
                pk = item['membre']
                if ServiceConsulte.objects.filter(pk=pk).exists():
                    liste_membres.append((ServiceConsulte.objects.get(pk=pk), item["confirmé"]))
        return liste_membres

    def envoi_convocation_possible(self):
        pres_ok = all([True if pres.heure else False for pres in self.presentations.all()])
        return bool(pres_ok and self.date and not self.convocations_envoyes)

    def get_pj(self):
        if self.convocations_envoyes:
            from messagerie.models import Message
            from django.contrib.contenttypes.models import ContentType
            ct = ContentType.objects.get_for_model(self)
            mes = Message.objects.filter(content_type=ct, object_id=self.pk).last()
            return mes.fichier

    class Meta:
        ordering = ('date',)


class PresentationCDSR(models.Model):
    """
    Présentation d'un dossier à une réunion CDSR
    """
    reunion_cdsr_fk = models.ForeignKey("structure.ReunionCDSR", blank=True, null=True, verbose_name="Réunion CDSR", related_name="presentations", on_delete=models.CASCADE)
    instruction_fk = models.ForeignKey("instructions.Instruction", blank=True, verbose_name="Instruction", related_name="presentations", on_delete=models.CASCADE)
    avis_o2o = models.OneToOneField("instructions.Avis", verbose_name="avis", related_name="presentation", on_delete=models.CASCADE)
    heure = models.TimeField("Heure de présentation", null=True, blank=True)
    convocations_envoyes = models.DateTimeField("Date d'envoi des convocations", blank=True, null=True)
    # contenu du json : membre : pk de l'organisation, confirmé : bool, présent : bool
    invites_json = models.JSONField("Membres invités", blank=True, null=True, max_length=100000)
    presentation_origine_fk = models.ForeignKey("self", verbose_name="présentation parente", blank=True, null=True, related_name="presentation_enfant", on_delete=models.SET_NULL)
    raison_nouvelle_presentation = models.CharField("Raison de la nouvelle présentation", max_length=500, blank=True, null=True)

    def __str__(self):
        if self.reunion_cdsr_fk:
            if self.reunion_cdsr_fk.date:
                return self.reunion_cdsr_fk.date.strftime("%d-%b-%Y")
            else:
                return self.reunion_cdsr_fk.nom_s
        return str(self.instruction_fk)

    def get_tous_membres(self):
        if self.reunion_cdsr_fk:
            liste_membres = self.reunion_cdsr_fk.get_membres_permanents()
        else:
            cdsr = ServiceCDSR.objects.get(instance_fk=self.instruction_fk.instance)
            liste_membres = list(cdsr.membres_permanents_m2m.all())
        liste_membres += self.get_membres_invites()[1:]
        return liste_membres

    def get_membres_invites(self):
        liste_membres = []
        if self.invites_json:
            pk = self.invites_json[0]['membre']
            if StructureOrganisatrice.objects.filter(pk=pk).exists():
                liste_membres.append(StructureOrganisatrice.objects.get(pk=pk))
            for item in self.invites_json[1:]:
                pk = item['membre']
                if ServiceConsulte.objects.filter(pk=pk).exists():
                    liste_membres.append(ServiceConsulte.objects.get(pk=pk))
        return liste_membres

    def get_membres_invites_avec_service(self):
        liste_destinataires = []
        for service in self.get_membres_invites():
            liste_destinataires += service.get_utilisateurs_avec_service()
        return liste_destinataires

    def aff_membres_invites(self):
        liste_membres = []
        if self.invites_json:
            pk = self.invites_json[0]['membre']
            if StructureOrganisatrice.objects.filter(pk=pk).exists():
                liste_membres.append((StructureOrganisatrice.objects.get(pk=pk), self.invites_json[0]["confirmé"]))
            for item in self.invites_json[1:]:
                pk = item['membre']
                if ServiceConsulte.objects.filter(pk=pk).exists():
                    liste_membres.append((ServiceConsulte.objects.get(pk=pk), item["confirmé"]))
        return liste_membres

    def get_last_presentation(self):
        """Avoir la dernière présentation de la chaine de demande"""
        if not self.presentation_enfant.all() and not self.presentation_origine_fk:
            return self
        if self.presentation_origine_fk:
            return self.presentation_origine_fk.presentation_enfant.all().order_by('pk').last()
        return self.presentation_enfant.all().order_by('pk').last()

    def get_precedente_presentation(self):
        """Avoir la liste des toutes les presentations sauf celle en cours"""
        if not self.presentation_enfant.all():
            return PresentationCDSR.objects.none()
        original = PresentationCDSR.objects.filter(pk=self.pk)
        precedent = self.presentation_enfant.exclude(pk=self.get_last_presentation().pk)
        liste = (original | precedent)
        return liste

    def creer_avis_membres_permanents(self, request):
        from instructions.models import Avis
        instruction = self.instruction_fk
        for item in self.reunion_cdsr_fk.permanents_json:
            pk = item['membre']
            if ServiceConsulte.objects.filter(pk=pk).exists():
                membre = ServiceConsulte.objects.get(pk=pk)
                if not Avis.objects.filter(service_consulte_fk=membre).filter(instruction=instruction).exists():
                    historique_circuit_json = [{"entrant": request.organisation.pk,
                                                "sortant": membre.pk,
                                                "type": "creer",
                                                "date": str(timezone.now())}]
                    avis = Avis.objects.create(instruction=instruction, service_consulte_fk=membre,
                                               service_consulte_origine_fk=membre,
                                               service_demandeur_fk=request.organisation,
                                               historique_circuit_json=historique_circuit_json)
                    avis.notifier_creation_avis(origine=[request.user, request.organisation],
                                                agents=membre.get_users_list())

    def get_pj(self):
        if self.convocations_envoyes:
            from messagerie.models import Message
            from django.contrib.contenttypes.models import ContentType
            ct = ContentType.objects.get_for_model(self)
            mes = Message.objects.filter(content_type=ct, object_id=self.pk).last()
            return mes.fichier