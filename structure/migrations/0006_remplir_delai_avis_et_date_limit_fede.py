# Generated by Django 4.0.6 on 2022-08-24 08:36
from django.utils import timezone
from django.db import migrations


def remplir_delai_avis_et_date_limit_fede(apps, schema):
    Avis = apps.get_model('instructions', "Avis")
    Federation = apps.get_model('structure', 'Federation')
    for avis in Avis.objects.filter(date_reponse__isnull=True, service_consulte_fk__isnull=False):
        if avis.avis_parent_fk:
            date_limite_reponse_dt = timezone.now() + timezone.timedelta(
                avis.service_consulte_fk.delai_jour_preavis_int)
            if date_limite_reponse_dt > avis.instruction.manif.date_debut:
                date_limite_reponse_dt = avis.instruction.manif.date_debut - timezone.timedelta(days=2)
        else:
            date_limite_reponse_dt = timezone.now() + timezone.timedelta(avis.service_consulte_fk.delai_jour_avis_int)
            if date_limite_reponse_dt > avis.instruction.manif.date_debut:
                date_limite_reponse_dt = avis.instruction.manif.date_debut - timezone.timedelta(days=5)
        if date_limite_reponse_dt < timezone.now():
            date_limite_reponse_dt = timezone.now()
        avis.date_limite_reponse_dt = date_limite_reponse_dt
        avis.save()
    for fede in Federation.objects.all():
        fede.delai_jour_avis_int = 28
        fede.delai_jour_preavis_int = 28
        fede.save()


class Migration(migrations.Migration):

    dependencies = [
        ('structure', '0005_serviceconsulte_delai_jour_avis_int_and_more'),
    ]

    operations = [
        migrations.RunPython(remplir_delai_avis_et_date_limit_fede)
    ]
