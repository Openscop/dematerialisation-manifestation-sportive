# coding: utf-8
from django import template

register = template.Library()


@register.filter
def historique_json_reunionCDSR(json):
    """
    Afficher l'historique d'une reunion CDSR
    """
    html = "<ul>"
    for etape in json:
        html += f"<li> le {etape['date']}, champ {etape['champ']} modifié par {etape['user']} : {etape['olddata']} -> {etape['newdata']}</li>"
    html += "</ul>"
    return html
