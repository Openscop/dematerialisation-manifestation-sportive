import factory

from ..models.service import ServiceInstructeur, TypeService, CategorieService, ServiceConsulte, Federation
from ..models.cdsr import ServiceCDSR
from .structure import StructureBaseFactory
from sports.factories.sport import DisciplineFactory


class CategorieServiceFactory(factory.django.DjangoModelFactory):
    """ Factory """

    nom_s = "Categorie"
    my_order = factory.Sequence(lambda n: '{0}'.format(n))

    class Meta:
        model = CategorieService
        django_get_or_create = ('nom_s',)


class TypeServiceFactory(factory.django.DjangoModelFactory):
    """ Factory """

    nom_s = "TypeService"
    abreviation_s = "abr"
    categorie_fk = factory.SubFactory(CategorieServiceFactory, nom_s="Cat")

    class Meta:
        model = TypeService
        django_get_or_create = ('nom_s',)


class ServiceConsulteFactory(StructureBaseFactory):
    """ Factory """

    email_s = factory.Sequence(lambda n: 'service{0}@loire.gouv.fr'.format(n))
    type_service_fk = factory.SubFactory(TypeServiceFactory)

    @factory.post_generation
    def departement_m2m(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.departement_m2m.set([extracted])

    @factory.post_generation
    def commune_m2m(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.commune_m2m.set([extracted])

    class Meta:
        model = ServiceConsulte


class ServiceCDSRFactory(ServiceConsulteFactory):
    """ Factory """
    admin_service_famille_b = True
    admin_utilisateurs_subalternes_b = True
    autorise_rendre_avis_b = True
    autorise_acces_consult_organisateur_b = True

    class Meta:
        model = ServiceCDSR


class ServiceInstructeurFactory(ServiceConsulteFactory):
    """ Factory """
    admin_service_famille_b = True
    admin_utilisateurs_subalternes_b = True
    autorise_rendre_avis_b = True
    autorise_acces_consult_organisateur_b = True

    class Meta:
        model = ServiceInstructeur


class PrefectureFactory(ServiceInstructeurFactory):
    """ Factory """

    precision_nom_s = factory.Sequence(lambda n: "Préfecture{0}".format(n))
    email_s = factory.Sequence(lambda n: 'pref{0}@loire.gouv.fr'.format(n))
    type_service_instructeur_s = "prefecture"
    type_service_fk = factory.SubFactory(TypeServiceFactory, nom_s="Préfecture",
                                         abreviation_s="Pref",
                                         categorie_fk__nom_s="Service préfectoral")

    @factory.post_generation
    def arrondissement_m2m(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            self.arrondissement_m2m.set([extracted])

    class Meta:
        model = ServiceInstructeur


class MairieFactory(ServiceInstructeurFactory):
    """ Factory """

    precision_nom_s = factory.Sequence(lambda n: "Mairie{0}".format(n))
    email_s = factory.Sequence(lambda n: 'pref{0}@loire.gouv.fr'.format(n))
    type_service_instructeur_s = "mairie"
    porte_entree_avis_b = True
    type_service_fk = factory.SubFactory(TypeServiceFactory, nom_s="Mairie",
                                         abreviation_s="Mairie",
                                         categorie_fk__nom_s="Mairie")

    @factory.post_generation
    def commune_m2m(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.commune_m2m.set([extracted])

    class Meta:
        model = ServiceInstructeur


class FederationFactory(ServiceConsulteFactory):
    """ Factory fédération sportive """

    precision_nom_s = factory.Sequence(lambda n: "Fédération{0}".format(n))
    email_s = factory.Sequence(lambda n: 'federation{0}@loire.gouv.fr'.format(n))
    admin_service_famille_b = True
    admin_utilisateurs_subalternes_b = True
    porte_entree_avis_b = True
    autorise_rendre_avis_b = True
    autorise_acces_consult_organisateur_b = True
    discipline_fk = factory.SubFactory(DisciplineFactory)
    type_service_fk = factory.SubFactory(TypeServiceFactory, nom_s="Fédération sportive",
                                         abreviation_s="Fédé",
                                         categorie_fk__nom_s="Fédération")

    @factory.post_generation
    def departement_m2m(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.departement_m2m.set([extracted])

    @factory.post_generation
    def commune_m2m(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.commune_m2m.set([extracted])

    class Meta:
        model = Federation
