import factory

from ..models.organisation import Organisation, StructureOrganisatrice, TypeOrganisation
from core.factories.instance import InstanceFactory


class StructureBaseFactory(factory.django.DjangoModelFactory):
    """ Factory structure """

    # Champs
    precision_nom_s = factory.Sequence(lambda n: 'Structure Organisatrice {0}'.format(n))
    adresse_s = factory.Sequence(lambda n: 'address{0}'.format(n))
    telephone_s = '06 12 23 45 56'
    email_s = factory.Sequence(lambda n: 'structure{0}@example.com'.format(n))
    site_web_s = 'http://www.grs.fr'
    instance_fk = factory.SubFactory(InstanceFactory)

    # Meta
    class Meta:
        model = Organisation


class TypeOrganisationFactory(factory.django.DjangoModelFactory):
    nom_s = "Association loi 1901"

    class Meta:
        model = TypeOrganisation
        django_get_or_create = ('nom_s',)


class StructureOrganisatriceFactory(StructureBaseFactory):
    """ Factory structure organisatrice """

    # Champs
    nom_s = factory.Sequence(lambda n: 'Structure Organisatrice {0}'.format(n))
    precision_nom_s = factory.Sequence(lambda n: 'Structure Organisatrice {0}'.format(n))
    type_organisation_fk = factory.SubFactory(TypeOrganisationFactory, nom_s="Association loi 1901")

    @factory.post_generation
    def commune_m2m(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.commune_m2m.set([extracted])

    # Meta
    class Meta:
        model = StructureOrganisatrice
