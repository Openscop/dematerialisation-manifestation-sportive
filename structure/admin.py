from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe
from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin, PolymorphicChildModelFilter
from adminsortable2.admin import SortableAdminMixin

from .models import (ServiceInstructeur, ServiceConsulte, Organisation, StructureOrganisatrice,
                     ServiceCDSR, ReunionCDSR, PresentationCDSR,
                     Federation, CategorieService, TypeService, TypeOrganisation, InvitationOrganisation)
from core.models import LogConsult


class OrganisationChildAdmin(PolymorphicChildModelAdmin):
    base_model = Organisation
    show_in_index = True
    readonly_fields = ('commune_m2m', 'arrondissement_m2m', 'departement_m2m', 'region_m2m', 'fusion')
    search_fields = ('precision_nom_s', "nom_s")
    list_display = ('pk', 'nom_s', 'instance_fk')

    def fusion(self, obj):
        link = reverse("structure:fusion_service", args=[obj.id])  # model name has to be lowercase
        return mark_safe(u'<a href="%s" target="_blank">Fusionner le service</a>' % link)
    fusion.short_description = "Fusionner avec un autre service"
    fusion.allow_tags = True


@admin.register(ServiceConsulte)
class ServiceConsulteAdmin(OrganisationChildAdmin):
    base_model = ServiceConsulte
    readonly_fields = OrganisationChildAdmin.readonly_fields + ('type_service_fk', 'service_parent_fk')
    list_filter = ('instance_fk', 'type_service_fk__categorie_fk', 'type_service_fk')
    list_display = ('pk', 'nom_s')


@admin.register(ServiceCDSR)
class ServiceCDSRAdmin(ServiceConsulteAdmin):
    base_model = ServiceCDSR
    list_display = ('pk', 'nom_s', 'instance_fk', 'membres_permanents')
    list_filter = []
    ordering = ['instance_fk__departement__name']
    filter_horizontal = ("membres_permanents_m2m",)

    def membres_permanents(self, obj):
        if obj.membres_permanents_m2m.all():
            return True
        return False
    membres_permanents.short_description = "membres permanents"
    membres_permanents.boolean = True

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        # Les utilisateurs équipe ne peuvent voir que leur département
        if not request.user.has_group('_support'):
            queryset = queryset.filter(instance_fk__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if obj:
            instance = obj.instance_fk
            form.base_fields['membres_permanents_m2m'].queryset = ServiceConsulte.objects.filter(
                instance_fk=instance).order_by('-nom_s')
        return form

    def changelist_view(self, request, extra_context=None):
        """surcharge pour ajouter un log"""
        response = super().changelist_view(request, extra_context)
        LogConsult.objects.create_from_list(response, request)
        return response

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response


@admin.register(ServiceInstructeur)
class ServiceInstructeurAdmin(ServiceConsulteAdmin):
    base_model = ServiceInstructeur
    list_filter = ('type_service_fk__categorie_fk', 'instance_fk', )
    list_display = ('pk', 'nom_s', 'type_service_fk', 'instance_fk',)


@admin.register(Federation)
class FederationAdmin(ServiceConsulteAdmin):
    base_model = Federation
    list_display = ('pk', 'nom_s', 'discipline_fk')
    list_filter = ('discipline_fk', )


@admin.register(StructureOrganisatrice)
class StructureOrganisatriceAdmin(OrganisationChildAdmin):
    base_model = StructureOrganisatrice
    list_display = ('pk', 'nom_s', 'instance_fk')
    list_filter = ('instance_fk',)


@admin.register(Organisation)
class OrganisationParentAdmin(PolymorphicParentModelAdmin):
    base_model = Organisation
    child_models = (StructureOrganisatrice, Federation, ServiceConsulte, ServiceInstructeur, ServiceCDSR)
    list_display = ('pk', 'nom_s', 'instance_fk')
    search_fields = ('precision_nom_s', "nom_s")
    list_filter = (PolymorphicChildModelFilter, 'instance_fk', )


@admin.register(TypeService)
class TypeServiceAdmin(admin.ModelAdmin):
    list_display = ('nom_s', 'abreviation_s', 'categorie_fk', )
    list_filter = ('categorie_fk', )
    search_fields = ('nom_s', 'abreviation_s',)
    ordering = ('nom_s',)
    list_per_page = 250


@admin.register(CategorieService)
class CategorieServiceAdmin(SortableAdminMixin, admin.ModelAdmin):
    pass


@admin.register(TypeOrganisation)
class TypeOrganisationAdmin(admin.ModelAdmin):
    list_display = ('nom_s',)


@admin.register(InvitationOrganisation)
class InvitationOrganisationAdmin(admin.ModelAdmin):
    list_display = ('date_creation', 'email_s', 'service_fk', 'est_delivree', 'completee_b')
    list_filter = ('expiree_b', 'revoquee_b', 'completee_b')
    search_fields = ('email_s', 'origine_fk__username__unaccent', 'origine_fk__last_name__unaccent')
    fields = ('email_s', 'est_delivree', 'completee_b', 'expiree_b', 'revoquee_b', 'origine_fk', 'service_fk', 'token_s')
    readonly_fields = ('est_delivree', 'service_fk', 'origine_fk', 'token_s')
    ordering = ('date_creation',)
    date_hierarchy = 'date_creation'
    list_per_page = 250


@admin.register(ReunionCDSR)
class ReunionCDSRAdmin(admin.ModelAdmin):
    list_display = ('nom_s', 'date', 'nb_dossier_i', 'service_cdsr')
    list_filter = ('service_cdsr',)
    list_display_links = ('nom_s', 'date')
    exclude = []
    date_hierarchy = 'date'

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        # Les utilisateurs équipe ne peuvent voir que leur département
        if not request.user.has_group('_support'):
            queryset = queryset.filter(service_cdsr__instance_fk__departement=request.user.get_departement())
        return queryset


@admin.register(PresentationCDSR)
class PresentationCDSRAdmin(admin.ModelAdmin):
    list_display = ('reunion_cdsr_fk', 'heure', 'instruction_fk')
    # list_filter = ('reunion_cdsr_fk__date',)
    exclude = []
    readonly_fields = ('instruction_fk', 'avis_o2o', 'presentation_origine_fk', 'raison_nouvelle_presentation')
    ordering = ('reunion_cdsr_fk',)
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        # Les utilisateurs équipe ne peuvent voir que leur département
        if not request.user.has_group('_support'):
            queryset = queryset.filter(instruction_fk__instance__departement=request.user.get_departement())
        return queryset
