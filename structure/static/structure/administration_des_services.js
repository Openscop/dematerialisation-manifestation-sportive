function showMessage(message, state){
    $('.hearder-notification').prepend(
        `<div class="row hearder-notification">
                <div class="col-xs-12 col-md-12">
                        <div class="alert alert-${state} alert-dismissable">
                            <strong>${message}</strong>
                            <button type="button" class="btn-close float-end" data-bs-dismiss="alert" aria-hidden="true"></button>
                        </div>
                </div>  
        </div>`
    )
    AfficherIcones();
}

function deleteInteraction(object_id){
    $.get({
        url: url_delete_interaction,
        data: {
            'object_id':  object_id,
        },
        success :
            function (reponse){
                showMessage(reponse, 'success')
            }
    })
}

function removeUserOrganisation(object_id, user_id, url){
    $.get({
        url: url,
        data: {
            'organisation_id':  object_id,
            'user_id': user_id,
        },
        success :
            function (reponse){
                showMessage(reponse, 'success')
            }
    })
}

function show_form(title, url, data_service=false, ){
    $('#modal-form .modal-title').text(title)
    $("#modal-form-body").empty().append(html_chargement_en_cours())
    var myModal = new bootstrap.Modal(document.getElementById('modal-form'))
    myModal.show()
    $('.modal-backdrop').addClass('modal-rel-backdrop').appendTo($('#detail-container'));
    $('body').removeClass('modal-open');
    var height = $("#service_selectionne").height() - $("#selectionne_card_header").height()
    $('.modal-backdrop').css('height', height)
    $('body').css('overflow', 'unset')
    $("#modal-form").css('min-height', '550px')
    $.get({
        url: url,
        data: {
            'service_id':  $(this).attr('data-service'),
        },
        success :
            function (reponse){
                $(".blocs-chargement-spinner").remove()
                $("#modal-form-body").empty().append(reponse)
                $(".chosen-select").chosen({width: "100%"})
            }
    })
}

// Affiche les instruction pour ajouter un service à un utilisateur
$(document).on('click', '#btn-ajouter-service', function(){
    $(this).fadeOut()
    $("#alert-gestion-service").removeClass('d-none').fadeIn()
})

$(document).on('click', '.delete-interaction', function(){
    deleteInteraction($(this).attr('id'))
    $(this).closest('li').remove();
    $('.tooltip').tooltip("hide");
})

// Affiche les bouttons de confirmation
$(document).on('click', '.delete-user-organisation', function(){
    $('.tooltip').tooltip("hide");
    $(this).addClass('d-none');
    $(this).siblings('span').removeClass('d-none');
    AfficherIcones()
})

// Retire une organisation à un utilisateur ou à un cdsr
$(document).on('click', '.valide-delete-organisation', function(){
    if($(".user-organisation-list-item").length < 2 && $(this).attr('data-switch') === "user") {
        if (confirm('Attention, ceci est la dernière organisation de l\'utilisateur. En la retirant, son compte sera désactivé. ' +
            'L\'utilisateur ne pourra plus utiliser la plateforme')) {
            removeUserOrganisation($(this).attr('id'), $(this).attr('data-user'), $(this).attr('data-url'))
            $(this).closest('li').remove();
            $('.tooltip').tooltip("hide");
            $('#user-etat').removeClass("text-primary").addClass("text-danger").text("Inactif")
            $("#icone-user-etat").removeClass('fa-circle-check').addClass('fa-hexagon-xmark').removeClass("text-primary").addClass("text-danger");

        }
    }
    else {
        removeUserOrganisation($(this).attr('id'), $(this).attr('data-user'), $(this).attr('data-url'))
        $(this).closest('li').remove();
        $('.tooltip').tooltip("hide");
    }
})

$(document).on('click', '.dismis-delete', function(){
    $('.tooltip').tooltip("hide");
    $(this).parent().addClass('d-none');
    $(this).parent().siblings('i').removeClass('d-none');
    AfficherIcones()
})

// Affiche la modal d'historique
$(document).on('click', '.history_organisation_link', async function (e){
    e.preventDefault()
    var myModal = new bootstrap.Modal(document.getElementById('modal-historique'))
    $("#modal-historique-body").empty().append('<div id="spinnerForm" class="d-flex py-5 justify-content-center"><div style="color: #869200; width: 4rem; height: 4rem;" class="spinner-border" role="status"><span class="visually-hidden">Loading...</span></div></div>')
    myModal.show()
    $.get({
        url: $(this).attr('href'),
        data:{
            service_id: $(this).attr('data-service'),
        },
        success :
            function (reponse){
                $("#modal-historique-body").empty().append(reponse)
            }
    })
    AfficherIcones();
    $('.tooltip').tooltip("hide");
})

$(document).on('click', '.edit_user_active', function(){
    show_form('Modifier l\'état de l\'utilisateur.', $(this).attr('data-url'), $(this).attr('data-service'))
})

$(document).on('click', '.edit_service_active', function(){
    if($(this).attr('data-agent-count') === "0"){
        show_form('Modifier l\'état du service.', $(this).attr('data-url'), $(this).attr('data-service'))
    }
    else {
        $('#modal-form .modal-title').text('Modifier l\'état du service.')
        $("#modal-form-body").empty().append(html_chargement_en_cours())
        var myModal = new bootstrap.Modal(document.getElementById('modal-form'))
        myModal.show()
        $('.modal-backdrop').addClass('modal-rel-backdrop').appendTo($('#detail-container'));
        $('body').removeClass('modal-open');
        var height = $("#service_selectionne").height() - $("#selectionne_card_header").height()
        $('.modal-backdrop').css('height', height)
        $('body').css('overflow', 'unset')
        $("#modal-form").css('min-height', '550px')
        $("#modal-form-body").empty().append('<div id="modal-form-body">' +
            '<div class="modal-body">' +
            '<div class="text-danger alert alert-danger">' +
            'La structure ne pourra être inactivée qu\'après lui avoir détaché tous ses utilisateurs.' +
            '</div>' +
            '</div></div>')
    }
})

$(document).on('click', '.edit_user_email', function(){
    show_form('Modifier l\'adresse e-mail de l\'utilisateur.', $(this).attr('data-url'), $(this).attr('data-service'))
})

$(document).on('click', '.add-interaction', function(){
    show_form('Ajouter une interaction à ce service', $(this).attr('data-url'), $(this).attr('data-service'))
})

$(document).on('click', '.edit-user-organisation', function(){
    show_form('Gérer les organisations de l\'utilisateur', $(this).attr('data-url'), $(this).attr('data-service'))
})

$(document).on('click', '.edit-interaction', function(){
    show_form('Modifier cette interaction', $(this).attr('data-url'), $(this).attr('data-service'))
})

$(document).on('click', '.edit-service-parent', function(){
    show_form('Modifier le parent de ce service', $(this).attr('data-url'), $(this).attr('data-service'))
})

$(document).on('click', '.edit-service-type', function(){
    show_form('Modifier le type de ce service', $(this).attr('data-url'), $(this).attr('data-service'))
})

$(document).on('click', '.edit-service-nom', function(){
    show_form('Modifier le nom de ce service', $(this).attr('data-url'), $(this).attr('data-service'))
})
$(document).on('click', '.edit-service-email', function(){
    show_form('Modifier l\'adresse e-mail de ce service', $(this).attr('data-url'), $(this).attr('data-service'))
})
$(document).on('click', '.edit-service-telephone', function(){
    show_form('Modifier le téléphone de ce service', $(this).attr('data-url'), $(this).attr('data-service'))
})
$(document).on('click', '.edit-service-adresse', function(){
    show_form('Modifier l\'adresse de ce service', $(this).attr('data-url'), $(this).attr('data-service'))
})
$(document).on('click', '.edit-service-instance', function(){
    show_form('Modifier l\'instance de ce service', $(this).attr('data-url'), $(this).attr('data-service'))
})
$(document).on('click', '.edit-service-site', function(){
    show_form('Modifier le site internet de ce service', $(this).attr('data-url'), $(this).attr('data-service'))
})
$(document).on('click', '.edit-delai-avis', function(){
    show_form('Modifier le délai de rendu d\'avis', $(this).attr('data-url'), $(this).attr('data-service'))
})
$(document).on('click', '.edit-delai-preavis', function(){
    show_form('Modifier le délai de rendu de préavis', $(this).attr('data-url'), $(this).attr('data-service'))
})

$(document).on('click', '.gerer-secteur-service', function(){
    $.get({
        url: `/structure/update_service_consulte_secteur/${$(this).attr('data-service')}/`,
        data: {
            'service_id':  $(this).attr('data-service'),
        },
        success :
            function (reponse){
                $('#modal-form .modal-title').text('Modifier les secteurs de ce service')
                $("#modal-form-body").empty().append(reponse)
                if($("#id_is_service_regional").is(':checked')){
                    $('#div_id_commune_m2m').hide()
                    $('#div_id_arrondissement_m2m').hide()
                    $('#div_id_departement_m2m').hide(400)
                }
                if($("#id_is_service_departemental").is(':checked')){
                    $('#div_id_commune_m2m').hide()
                    $('#div_id_arrondissement_m2m').hide()
                    $('#div_id_region_m2m').hide(400)
                }
                if($("#id_is_service_arrondissement").is(':checked')){
                    $('#div_id_commune_m2m').hide()
                    $('#div_id_region_m2m').hide(400)
                    $('#div_id_departement_m2m').hide(400)
                }
                if($("#id_is_service_commune").is(':checked')){
                    $('#div_id_arrondissement_m2m').hide()
                    $('#div_id_region_m2m').hide(400)
                    $('#div_id_departement_m2m').hide(400)
                }
                var myModal = new bootstrap.Modal(document.getElementById('modal-form'))
                myModal.show()
                $('.modal-backdrop').addClass('modal-rel-backdrop').appendTo($('#detail-container'));
                $('body').removeClass('modal-open');
                var height = $("#service_selectionne").height() - $("#service_selectionne_card_header").height()
                $('.modal-backdrop').css('height', height)
                $('body').css('overflow', 'unset')
                $(".chosen-select").chosen({width: "100%"})
            }
    })
    $(".chosen-select").chosen({width: "100%"})
})

// Ouvre la modal de création de Service consulté / Fédération
$(document).on('click', '#open_create_service_form_modal', function(){
    $("#service_selectionne").empty()
    var data_categorie = $(this).attr('data-categorie')
    if(data_categorie==="structure"){
        var url = `/structure/create_structure_organisatrice/`
    }
    else if(data_categorie==="12"){
        var url = `/structure/create_federation/`
    }
    else {
        var url = `/structure/create_service_consulte/`
    }
    $.get({
        url: url,
        data: {
            'categorie':  data_categorie,
            'instance': $(this).attr('data-instance'),
        },
        success :
            function (reponse){
                $("#create_service_form_modal .modal-content").empty().append(reponse)
                $(".chosen-select").chosen({width: "100%"})
                $('#div_id_arrondissement_m2m').hide()
                $('#div_id_region_m2m').hide()
                $('#div_id_departement_m2m').hide()
                if(data_categorie!=="structure"){
                    $('#div_id_commune_m2m').hide()
                }
            }
    })
    var myModal = new bootstrap.Modal(document.getElementById('create_service_form_modal'))
    setTimeout(function (){myModal.show();
        $("#id_type_service_fk").removeAttr("style").css('position', 'absolute').css('opacity', 0).css('display', 'block !important').after($(".id_type_service_fk_chosen").container);
    }, 201)
    setTimeout( function (){
        AfficherIcones()
    }, 500)
})

// Ouvre la modal de création de service instructeur
$(document).on('click', '#open_create_service_instructeur_form_modal', function(){
    $("#service_selectionne").empty()
    var data_categorie = $(this).attr('data-categorie')
    var url = `/structure/create_service_instructeur/`
    $.get({
        url: url,
        data: {
            'categorie':  data_categorie,
            'instance': $(this).attr('data-instance'),
        },
        success :
            function (reponse){
                $("#create_service_form_modal .modal-content").empty().append(reponse)
                $(".chosen-select").chosen({width: "100%"})
                $('#div_id_arrondissement_m2m').hide()
                $('#div_id_region_m2m').hide()
                $('#div_id_departement_m2m').hide()
                $('#div_id_commune_m2m').hide()
            }
    })
    var myModal = new bootstrap.Modal(document.getElementById('create_service_form_modal'))
    setTimeout(function (){myModal.show();
    }, 200)
    setTimeout( function (){
        AfficherIcones()
    }, 500)
})

// Pour service instructeur modifie type_service_fk en fonction de type_service_instructeur_s
$(document).on('change', '#id_type_service_instructeur_s', function (e){
    if($(this).val()){
        var selected_text = $( "#id_type_service_instructeur_s option:selected" ).text()
        var selected = $('#id_type_service_fk').find(`option:contains('${selected_text}')`)
        var selected_value = selected.attr('value')
        // Todo cela va poser problème
        if(selected_text === "Préfecture"){
            selected_value = 90
        }
        $('#id_type_service_fk').val(selected_value)
        $('select').trigger("chosen:updated");
    }
})

// Vérifie les champs du formulaire de création de service
$(document).on('click', '#submit_create_service_form', function(e){
    e.preventDefault()
    // 1, pas de type de service.
    // 2, pas de sélection de secteur dans le cas d'un admin d'instance ou pour arrondissement et commune pour un groupe support.
    // 3, pas de sélection de secteur région ou département pour un groupe support
    if(!$("#id_type_service_fk").val() ||
        ($('#id_commune_m2m').get(0).selectedIndex === -1 && $('#id_arrondissement_m2m').get(0).selectedIndex === -1 &&
            ($("#id_is_service_departemental").prop('checked') === false && $("#id_is_service_regional").prop('checked') === false)) ||
        ($("#id_is_service_departemental").prop('checked') === true && $("#id_departement_m2m").length && $('#id_departement_m2m').get(0).selectedIndex === -1 ) ||
        ($("#id_is_service_regional").prop('checked') === true && $("#id_region_m2m").length && $('#id_region_m2m').get(0).selectedIndex === -1 )) {
        if($('#id_commune_m2m').get(0).selectedIndex === -1 && $('#id_arrondissement_m2m').get(0).selectedIndex === -1 &&
            ($("#id_is_service_departemental").prop('checked') === false && $("#id_is_service_regional").prop('checked') === false) ||
            ($("#id_is_service_departemental").prop('checked') === true && $("#id_departement_m2m").length && $('#id_departement_m2m').get(0).selectedIndex === -1 ) ||
            ($("#id_is_service_regional").prop('checked') === true && $("#id_region_m2m").length && $('#id_region_m2m').get(0).selectedIndex === -1 )) {
            $("#geographie-container").css('background-color', '#f8d7da')
            $("#geographie-container").css('border', '#f5c2c7 1px solid')
            $("#geographie-container").css('border-radius', '0.25rem')
            if(!$("#error-geographie").length){
                $("#geographie-container fieldset").append('<span id="error-geographie" class="m-3 text-danger"><strong>Pas de géographie renseignée.</strong></span>')
            }
            else {
                $("#error-geographie").animate({ "font-size" : "+=0.5px" }, 400, "easeOutBounce");
            }
        }

        if(!$("#id_type_service_fk").val()){
            $("#id_type_service_fk_chosen").css('border', '2px solid #f5c2c7').css('border-radius', '0.25rem')
            if(!$("#error_type_service_fk").length){
                $("#div_id_type_service_fk").append('<span id="error_type_service_fk" class="text-danger"><strong>Ce champ est obligatoire.</strong></span>')
            }
        }
    }
    else if(!$("#id_service_parent_fk").val() && $("#id_service_parent_fk").prop('required')){
        $("#id_service_parent_fk_chosen").css('border', '2px solid #f5c2c7').css('border-radius', '0.25rem')
        if(!$("#error_service_parent_fk").length) {
            $("#div_id_service_parent_fk").append('<span id="error_service_parent_fk" class="text-danger"><strong>Ce champ est obligatoire.</strong></span>')
        }
    }

    else {
        $("#create_service_form_modal form").submit()
    }
})

$(document).on('change', '.geographie_checkbox', function(){
    if($(this).prop( "checked", true )){
        if($(this).attr('id')==="id_is_service_arrondissement"){
            $('.geographie_checkbox').not($(this)).prop( "checked", false );
            $('#div_id_arrondissement_m2m').show(400)
            $('#div_id_commune_m2m').hide(400)
            $('#div_id_region_m2m').hide(400)
            $('#div_id_departement_m2m').hide(400)
            $('#id_commune_m2m').val('').trigger('chosen:updated');
            $('#id_region_m2m').val('').trigger('chosen:updated');
            $('#id_departement_m2m').val('').trigger('chosen:updated');
            $('#territoire-result-name').empty()
        }
        if($(this).attr('id')==="id_is_service_commune"){
            $('.geographie_checkbox').not($(this)).prop( "checked", false );
            $('#div_id_commune_m2m').show(400)
            $('#div_id_arrondissement_m2m').hide(400)
            $('#div_id_region_m2m').hide(400)
            $('#div_id_departement_m2m').hide(400)
            $('#id_arrondissement_m2m').val('').trigger('chosen:updated');
            $('#id_region_m2m').val('').trigger('chosen:updated');
            $('#id_departement_m2m').val('').trigger('chosen:updated');
            $('#territoire-result-name').empty()
        }
        if($(this).attr('id')==="id_is_service_departemental"){
            console.log('dep')
            console.log($(this).attr('data-dep'))
            $('.geographie_checkbox').not($(this)).prop( "checked", false );
            $('#div_id_departement_m2m').show(400)
            $('#div_id_commune_m2m').hide(400)
            $('#div_id_arrondissement_m2m').hide(400)
            $('#div_id_region_m2m').hide(400)
            $('#id_arrondissement_m2m').val('').trigger('chosen:updated');
            $('#id_commune_m2m').val('').trigger('chosen:updated');
            $('#id_region_m2m').val('').trigger('chosen:updated');
            $('#territoire-result-name').text($(this).attr('data-dep'))
        }
        if($(this).attr('id')==="id_is_service_regional"){
            $('.geographie_checkbox').not($(this)).prop( "checked", false );
            $('#div_id_region_m2m').show(400)
            $('#div_id_commune_m2m').hide(400)
            $('#div_id_arrondissement_m2m').hide(400)
            $('#div_id_departement_m2m').hide(400)
            $('#id_arrondissement_m2m').val('').trigger('chosen:updated');
            $('#id_commune_m2m').val('').trigger('chosen:updated');
            $('#id_departement_m2m').val('').trigger('chosen:updated');
            $('#territoire-result-name').text($(this).attr('data-dep'))
        }
        if ($('#error-geographie').length) {
            $('#error-geographie').remove();
            $("#geographie-container").css('background-color', "");
            $("#geographie-container").css('border', "");
        }
    }
})

// Affiche dynamiquement le nom final d'un service lors de la création et l'édition
$(document).on('change', '#id_type_service_fk', function(e){
    $('#type-result-name').text($("#id_type_service_fk_chosen .chosen-single span").text().split('(')[0])
    if ($('#error_type_service_fk').length) {
        $('#error_type_service_fk').remove();
        $("#id_type_service_fk_chosen").css('background-color', "");
        $("#id_type_service_fk_chosen").css('border', "");
    }
})

$(document).on('change', '#id_service_parent_fk', function(e){
    if ($('#error_service_parent_fk').length) {
        $('#error_service_parent_fk').remove();
        $("#id_service_parent_fk_chosen").css('background-color', "");
        $("#id_service_parent_fk_chosen").css('border', "");
    }
})

$(document).on('change', '#id_region_m2m', function(e){
    var territoire_str = ""
    setTimeout(function (){
        $("#id_region_m2m_chosen .chosen-choices li[role=\"option\"] span").each(
            function (){
                territoire_str += $(this).text() + " "
            }
        )
        $('#territoire-result-name').text(`${territoire_str}`)
    }, 100)
})

$(document).on('change', '#id_departement_m2m', function(e){
    var territoire_str = ""
    setTimeout(function (){
        $("#id_departement_m2m_chosen .chosen-choices li[role=\"option\"] span").each(
            function (){
                territoire_str += $(this).text().split('-')[0] + " "
            }
        )
        $('#territoire-result-name').text(`${territoire_str}`)
    }, 100)
})

$(document).on('change', '#id_arrondissement_m2m', function(e){
    var territoire_str = ""
    setTimeout(function (){
        $("#id_arrondissement_m2m_chosen .chosen-choices li[role=\"option\"] span").each(
            function (){
                territoire_str += $(this).text() + " "
            }
        )
        $('#territoire-result-name').text(`${territoire_str}`)
    }, 100)
})

$(document).on('change', '#id_commune_m2m', function(e){
    var territoire_str = ""
    setTimeout(function (){
        $("#id_commune_m2m_chosen .chosen-choices li[role=\"option\"] span").each(
            function (){
                territoire_str += $(this).text() + " "
            }
        )
        $('#territoire-result-name').text(`${territoire_str}`)
    }, 100)

})

$(document).on('keyup', '#id_precision_nom_s', function(e){
    if($(this).val()){
        $('#precision-result-name').text(` ${$(this).val()}`)
    }
    else {
        $('#precision-result-name').empty()
    }
})

$(document).on('click', '.service-list-item', function(e){
    e.preventDefault()
    var item = $(this)
    var href = $(this).attr('href');
    if(typeof href !== "undefined"){
        var object_id = href.slice(1)
    }
    var is_user = $(this).hasClass('organisation_user')
    if(is_user){
        var model = 'user'
    }
    else {
        var model = 'organisation'
    }
    if(typeof object_id !== "undefined") {
        $.get({
            url: `/structure/administration_des_services/is_habilite/${object_id}/`,
            data: {'secteur': secteur, 'model': model},
            success:
                function (reponse) {
                    if (reponse === "True") {
                        if (typeof href !== 'undefined' && href !== false) {
                            if (is_user) {
                                history.pushState(null, '', `?service_id=${item.attr('data-service')}&&user_id=${object_id}`);
                                var url = url_detail_user
                            } else {
                                history.pushState(null, '', `?service_id=${object_id}`);
                                var url = url_action
                                var organisation_id = object_id
                            }

                            $.get({
                                url: url,
                                data: {
                                    'object_id': object_id,
                                    'organisation_id': item.attr('data-service')
                                },
                                success:
                                    function (data) {
                                        $('#service_selectionne').empty().append(data)
                                        AfficherIcones();
                                        $(".chosen-select").chosen({width: "100%"})
                                    }
                            })
                        }
                    }
                    else{
                        console.log(reponse)
                        $('#service_selectionne').empty().append("<div class='alert alert-warning mt-5 m-3 p-3'><i class=\"fas fa-ban fa-2x  me-3\"></i>Vous n'êtes pas habilité à voir ces informations.</div>")
                    }
                }
        })
    }
})

// Supprime l'adresse e-mail d'un utilisateur inactif
$(document).on('click', "#delete_user_email", function (){
    var user_id = $(this).attr('data-user')
    var orgnanisation_id = $(this).attr('data-service')
    if (confirm('Confirmez vous vouloir supprimer l\'adresse e-amail de cet utilisateur ? \n' +
                'Une nouvelle devra lui être attribué avant de pouvoir être activé à nouveau')) {
        $.get({
                url: "/structure/delete_user_email/",
                data: {
                    'user_id':  user_id,
                    'orgnanisation_id': orgnanisation_id,
                },
                success :
                    function (reponse){
                        window.location.reload()
                    }
            })
    }
})

// Affiche les habilitations avancées
$(document).on('click', "#habilitations-avancees-link", function (){
    $("#habilitations-avancees").fadeIn()
    $(this).fadeOut()
})

// TODO A voire avec David
// $(document).on('change', '#id_type_lien_s', function(){
//     $('#id_service_sortant_fk').val('')
//     if($(this).val() && $("#id_service_entrant_fk").val()){
//         var service_id = $("#id_service_entrant_fk").val()
//         var interaction = $(this).val()
//         if(interaction==="mandater"){
//             $('#id_service_sortant_fk').val('')
//             $.get({
//                 url: "/structure/get_service_for_interaction/",
//                 data: {
//                     'service_id':  service_id,
//                     'interaction': interaction,
//                 },
//                 success :
//                     function (reponse){
//                         showMessage(reponse, 'success')
//                     }
//             })
//         }
//     }
// })