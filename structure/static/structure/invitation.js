function InvitationRevoquer() {
    // Afficher la croix de révocation
    console.log(is_admin);
    if (is_admin) {
        $('.invitation').each(function () {
            $(this).append('<span class="float-end del_invit" style="display:none;"><i class="fas fa-times"></i></span>');
            $(this).find('.del_invit').css("cursor", "pointer").attr({
                'data-bs-toggle': 'tooltip',
                'data-bs-original-title': 'Cliquez pour révoquer l\'invitation'
            });
        });
    }
}

$(document).on('click', ".ajouter-invitation", function (){
    $('.invitation_btn').toggle(100);
    $('.invitation_input').toggle(500);
})

$(document).on('click', ".invit-quit", function (event){
    event.preventDefault();
    $('#invit-email').css('border-color', '');
    $('#invit-email').val("");
    $('.invitation_input').toggle(100);
    $('.invitation_btn').toggle(500);
    $('.invitation-erreur').empty();
})

$('body').on('submit', '#invit-form', function(event){
    event.preventDefault();
    var donnees = $('#invit-form').serialize();
    var data = $('#invit-form').serializeArray();
    if($("#invit-email").val()){
        $.post({
            url: `/structure/ajax/invitation/`,
            data: donnees,
            success:
                function (reponse) {
                    $('.invitation-liste').html(reponse);
                    $('#invit-email').css('border-color', '');
                    $('#invit-email').val("");
                    $('.invitation-erreur').empty();
                    InvitationRevoquer();
                    AfficherIcones();
                },
            error:
                function (reponse) {
                    $('#invit-email').val(data[1].value);
                    $('#invit-email').css('border-color', 'red');
                    $('.invitation-erreur').html(reponse.responseText);
                },
        });
    }
    else {
        $('.invitation-erreur').html("Veuillez saisir une adresse e-mail.");
    }

});

// Afficher au survol
$('body').on('mouseover', '.invitation', function () {
    $(this).find('span[class$=del_invit]').show('fast');
});

$('body').on('mouseleave', '.invitation', function () {
    $(this).find('span[class$=del_invit]').hide('fast');
});

$('body').on('click', 'span[class$=del_invit]', function () {
    let id_invit = $(this).parent().attr('id').split('_')[1]
    let id_organisation = $(this).parent().attr('data-service')
    $.ajax({
        url: "/structure/ajax/invitation/?id=" + id_invit,
        type: 'POST',
        data: {'service_id': id_organisation},
        dataType: 'html'
    }).done(function (data) {
        $('.tooltip').empty();
        $('.invitation-liste').html(data);
        InvitationRevoquer();
        AfficherIcones();
    });
});
