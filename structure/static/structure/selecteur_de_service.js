var value_array = []
var badge_array = []
var demamde_envoye = false

$("#service-search").css('width', '92%')

// Ajoute une icone pour déplier les sous service
$(document).on('click', '.tout_deplier', function(){
    showAllSousService()
    setTimeout(function (){
        $('.tout_deplier').removeClass('tout_deplier').addClass('tout_replier').attr('data-bs-original-title', "Tout replier")
    }, 350)
})

$(document).on('click', '.tout_replier', function(){
    hideAllSousService()
    setTimeout(function (){
        $('.tout_replier').removeClass('tout_replier').addClass('tout_deplier').attr('data-bs-original-title', "Tout déplier")
    }, 350)
})

$("#service-search").focus(function () {
    showAllSousService()
    setTimeout(function (){
        $('.tout_deplier').removeClass('tout_deplier').addClass('tout_replier').attr('data-bs-original-title', "Tout replier")
    }, 350)
})

// Recherche parmis les sous service
$("#service-search").keyup(function () {
    if ($("#service-search").val() && $(".name_service").length) {
        $('.button-span').addClass('d-none')
        $(".name_service").each(function () {
            if (~$(this).text().toLowerCase().indexOf($("#service-search").val().toLowerCase())) {
                $(this).closest('.list-group-item').removeClass('d-none')
            } else {
                $(this).closest('.list-group-item').addClass('d-none')
            }
        })
    } else {
        $(".list-group-item").removeClass('d-none')
    }
})

// Replie tout les sous service
function hideAllSousService(){
    $(".hide_sous_service").each(function (index, domEle){
        $(this).click()
    })
}

// Déplie tout les sous service
function showAllSousService(){
    $(".show_sous_service").each(function (index, domEle){
        $(this).click()
    })

    setTimeout(function (){
        if($(".show_sous_service").length){
            showAllSousService()
        }
    }, 500)
}

// Affiche les sous-services
$(document).on('click', '.show_sous_service', function(){
    var service_id = $(this).attr('data-service')
    var list_parent_id = $(this).attr('data-id-liste-parent')
    var padding = parseInt($(this).closest('li').css('padding-left').replace("px", "")) + 24
    if(action==="consultation"||action==='demande_avis'){
        var col = 'col-9'
    }
    else {
        var col = 'col-4'
    }
    $(this).removeClass('fa-plus-square').removeClass('show_sous_service')
        .addClass('fa-minus-square').addClass('hide_sous_service')
    $.get({
        url: `/structure/ajax/sous_service/`,
        data: {
            'service_id':  $(this).attr('data-service'),
            'action': action,
            'show_old': show_old,
            'object_id':  object_id,
        },
        success :
            function (reponse){
                var h = 0
                for (let elements of reponse.values()) {
                    h += 55
                    $("#liste_sous_service_"+service_id).append(renderElement(elements))
                }
                $("#liste_sous_service_"+service_id).animate({height: `${h}px`}, h)
                h += $("#liste_sous_service_"+list_parent_id).height()
                $("#liste_sous_service_"+list_parent_id).css('height', `${h}px`)
                setTimeout(function (){$("#liste_sous_service_"+service_id).css('height', 'auto')
                    $("#liste_sous_service_"+list_parent_id).css('height', 'auto')}, 500)
                AfficherIcones()
            }
    })
})

// Cache les sous-services
$(document).on('click', '.hide_sous_service', function(){
    var service_id = $(this).attr('data-service')
    $(this).removeClass('fa-minus-square').removeClass('hide_sous_service')
        .addClass('fa-plus-square').addClass('show_sous_service')
    $("#liste_sous_service_"+service_id).animate({height: `0`}, 500)
    setTimeout(function (){$("#liste_sous_service_"+service_id).empty()}, 500)
})

// Affiche les bouton d'action au survol du lien
$(document).on('mouseover', '.list-item-link', function (){
    if(!$(this).hasClass('disabled')){
        $('.button-span').addClass('d-none')
        $("#button_span_" + $(this).attr('data-id')).removeClass('d-none')
        $('.categorie-hover').removeClass('categorie-hover')
        $("#item_" + $(this).attr('data-id')).addClass('categorie-hover')
        $('.dismis').click(function (){
            $('.button-span').addClass('d-none')
        })
    }
})

// Permet d'obtenir la liste des catégories
function getListCategorie(action, ajout=false){

    $.get({
        url: url_categorie,
        data: {
            'action': action,
            'acces':  ajout,
            'manif_pk': object_id,
            'secteur': secteur,
        },
        success :
            function (data){
                data = JSON.parse(data);
                $('#modal-categorie-selecteur').empty()
                for ([key, val] of Object.entries(data)) {
                    $("#modal-categorie-selecteur").append(`<li data-categorie="${val[0]}" class="categorie px-3 py-2">${val[1]}
                                                            <span id="badge_${val[0]}" class="badge float-end"></span></li>`)
                }
            }
    })
}

// Ajoute des badges comprenant le nombre de selectionné lors des selections multiples
function remplirBadgeArray(action, ajout, secteur){
    if (selecteur_multiple) {
        $.get({
            url: url_categorie,
            data: {
                'action': action,
                'acces': ajout,
                'instance': secteur,
                'manif_pk': object_id,
            },
            success:
                function (data) {
                    data = JSON.parse(data);
                    for ([key, val] of Object.entries(data)) {
                        if (!badge_array[secteur]) {
                            badge_array[secteur] = []
                            badge_array[secteur]['total'] = 0
                        }
                        if (!badge_array[secteur][val[0]]) {
                            badge_array[secteur][val[0]] = 0
                            $(`#badge_${val[0]}`).empty()
                        } else {
                            $(`#badge_${val[0]}`).text(badge_array[secteur][val[0]])
                        }
                    }
                }
        })
    }
}

function user_is_admin(categorie, secteur){
    if(categorie !=="structure"){
        $.get({
            url: "/structure/administration_des_services/is_admin_famille/",
            data: {
                'categorie': categorie,
                'secteur': secteur,
            },
            success:
                function (reponse) {
                    if(reponse === "True"){
                        is_admin = true
                    }
                    else {
                        is_admin = false
                    }
                }
        })
        return is_admin
    }
}

function addBtnCreate(categorie, secteur){
    if(categorie !=="structure"){
        $.get({
            url: "/structure/administration_des_services/is_admin_famille/",
            data: {
                'categorie': categorie,
                'secteur': secteur,
            },
            success:
                function (reponse) {
                    if(reponse === "True"){
                        // Ajoute un bouton de création de service consulté
                        $("#service-liste").append(`<button class="btn ms-auto mt-4 me-1 col-4 btn-primary" id="open_create_service_form_modal" 
                                                        data-categorie="${categorie}" data-instance="${secteur}" role="button"><i class="fas fa-plus"></i>Ajouter un service</button>`)
                    }
                    else if(reponse === "Fédération délégataire"){
                        // Affiche une alerte
                        $("#service-liste").append(`
                        <div class="alert alert-info">
                            L'ajout de fédération délégataire est géré par l'équipe Support. Veuillez les contacter via 
                            votre forum <a href="/forum/forum-administrateur-instance">"Administrateurs d'instance
                            départemental"</a>  
                        </div>`)
                    }
                    else if(reponse === "Service instructeur"){
                        // Ajoute un bouton de création de service consulté et de service instructeur
                        $("#service-liste").append(`<div class="d-flex mt-4">
                                                    <button class="btn me-1 col-4 btn-danger" id="open_create_service_instructeur_form_modal" 
                                                        data-categorie="${categorie}" data-instance="${secteur}" role="button"><i class="fas fa-plus"></i>Ajouter un service <strong>instructeur</strong></button>\`
                                                     <button class="btn ms-auto me-1 col-4 btn-primary" id="open_create_service_form_modal" 
                                                        data-categorie="${categorie}" data-instance="${secteur}" role="button"><i class="fas fa-plus"></i>Ajouter un service <strong>consulté</strong></button></div>`)
                    }
                }
        })
    }
}


// Ajoute les icones d'administration à un service
function add_icones(icones){
    var html = "<div class=\"text-center mt-2 col-4 organisation-icone-container\">"
    if(icones['admin_service_famille_b']){
        html += '<i class="admin-famille pe-2"></i>'
    }
    if(icones['admin_utilisateurs_subalternes_b']){
        html += '<i class="admin-subaltern pe-2"></i>'
    }
    if(icones['porte_entree_avis_b']){
        html += `<i class="recevoir-avis pe-2"></i>`
    }
    if(icones['autorise_rendre_avis_b']){
        html += '<i class="rendre-avis pe-2"></i>'
    }
    if(icones['autorise_rendre_preavis_a_service_avis_b']){
        html += '<i class="rendre-preavis pe-2"></i>'
    }
    if(icones['autorise_mandater_avis_famille_b']){
        html += '<i class="mandater-avis pe-2"></i>'
    }
    if(icones['autorise_interroge_preavis_famille_b']){
        html += '<i class="interroge-preavis-droit pe-2"></i>'
    }
    html += '</div>'
    return  html
}

// Ajoute le nombre d'agents actif/inactif à un service
function add_agents_count(agents){
    var html = ""
    if(Number.isInteger(parseInt($('.selected').attr('data-categorie')))){
        if ( agents['cdsr'] === 1 ) {
            var icone_class = "fa-building"
            var text_tip = "Membres permanents"
            var text_tip_0 = "Pas de membre permanent !"
        } else {
            var icone_class = "fa-user"
            var text_tip = "Agents actifs"
            var text_tip_0 = "Pas d'agent !"
        }
        if(action==="consultation"||action==='demande_avis'){
            var col = 'col-3'
        }
        else {
            var col = 'col-2'
        }
        html = `<div class="text-end ${col} agents_count mt-2">`
        // Pour les sélecteurs d'administration on affiche le nombre d'agents
        if(action==='administration' ||  action==="gestion-organisation-utilisateur" ||  action==="gestion-membre-cdsr"){
            if ( agents['agents_inactif'] ) {
                html += `<span data-bs-toggle="tooltip" data-bs-original-title="Agents inactifs" aria-label="Agents inactifs">
                                ${agents['agents_inactif']}<i class="fas ${icone_class} ms-1" style="color: #E13E1B"></i></span>`
            }
            if ( agents['agents_actif'] ) {
                html += `<span data-bs-toggle="tooltip" data-bs-original-title="${text_tip}" aria-label="${text_tip}">
                                ${agents['agents_actif']}<i class="fas ${icone_class} ms-1" style="color: #015A70"></i></span>`
            }
            if ( ! agents['agents_actif'] && ! agents['agents_inactif'] ) {
                html += `<span class="fa-stack" data-bs-toggle="tooltip" data-bs-original-title="${text_tip_0}" style="background-color: #ffff00; border-radius: 4px">
                       <i class="fa-solid ${icone_class} fa-stack-1x"></i>
                       <i class="fa-solid fa-ban fa-stack-2x" style="color:#d62900"></i>
                     </span>`
            }
        }
        // Pour les autres on affiche une alerte que si aucun agent actif
        else if(!agents['agents_actif'])  {
            html += `<span class="fa-stack me-5" data-bs-toggle="tooltip" data-bs-original-title=" ${agents['agents_inactif'] ? 'Pas d\'agent actif !' : text_tip_0}" style="background-color: #ffff00; border-radius: 4px">
                       <i class="fa-solid ${icone_class} fa-stack-1x"></i>
                       <i class="fa-solid fa-ban fa-stack-2x" style="color:#d62900"></i>
                     </span>`
        }
        html += '</div>'
    }
    return html
}

$(document).on('mouseover', '.get_agent .far', function(){
    var click = false
    $(this).removeClass('far').addClass('fas')
    $(this).click(function (){
        $(this).removeClass('far').addClass('fas')
        click = true
    })
    $(this).mouseleave(function (){
        if(!click){
            $(this).removeClass('fas').addClass('far')
        }
    })
})

$(document).on('click', '.get_agent', function(){
    var checked
    if($(`#checkbox_${$(this).attr('id')}_Organisation`).is(':checked')){

        checked = "checked disabled='disabled'"
    }
    $(this).removeClass('get_agent').addClass('hide_agent')
    var service_id =  $(this).attr('id')
    $.get({
        url: url_agent,
        data: {
            'object_id': $(this).attr('id'),
        },
        success :
            function (data){
                if(Object.keys(data).length !== 0){
                    var h = 0
                    for ([key, val] of Object.entries(data)){
                        $("#agent_"+service_id).append(
                            `<div class="d-flex my-1 agent"><input class="form-check-input checkbox item ms-3" type="checkbox" value="${key}_${val[1]}" id="${key}_${val[1]}" name="service" ${checked}>
                                                 <label class="name_sous_service name label ms-2" for="${key}_${val[1]}">${val[0]}</label></div>`)
                        h += 30
                        if(value_array.includes(`${key}_${val[1]}`) && !$(`#${key}_${val[1]}`).prop('checked', true)){
                            $(`#${key}_${val[1]}`).prop('checked', true)
                        }
                    }
                    $("#agent_"+service_id).animate({height: `${h}px`}, h*2)
                }
                else {
                    $("#agent_"+service_id).append(`<div class="d-flex ms-3 text-danger"><strong>Pas d'agent trouvé</strong></div>`).animate({height: `25px`}, 250)
                }

            }
    })
})

// Envoie une demande d'avis via ajax
function envoieDemandeAvis(item_id, item){
    var container = item.parent()
    container.html('<div id="spinner'+item_id+'" style="color: #869200;" class="spinner-border me-3" role="status"><span class="visually-hidden me-5">Loading...</span></div>')
        .css('padding-left', '6rem')

    $.get({
        url: url_action,
        data: {
            'array_id[]': item_id,
            'instruction_id':  object_id,
        },
        success :
            function (reponse){
                container.animate({paddingLeft: '0'}, 800)
                if(reponse==="True"){
                    demamde_envoye = true

                    setTimeout(function (){
                        $("#spinner"+item_id).removeClass("spinner-border me-5").html("<p style='color: #869200; font-weight: bold'>Demande envoyée</p>").animate({width: '170px'}, 300)
                    }, 1000)
                }
                else {
                    setTimeout(function (){
                        $("#spinner"+item_id).removeClass("spinner-border me-5").html("<p style='color: red; font-weight: bold' class='btn-action-selecteur'>Un problème est survenu</p>").animate({width: '170px'}, 300)
                    }, 1000)
                }
            }
    })
}

// Envoie un accès en lecture via ajax
function accesConsultation(model, item_id, item){
    var container = item.parent()
    container.html('<div id="spinner'+item_id+'" style="color: #869200;" class="spinner-border me-3" role="status"><span class="visually-hidden me-5">Loading...</span></div>')
        .css('padding-left', '5rem')
    $.post({
        url: url_action,
        data: {
            'service_id': item_id,
            'acces':  ajout,
            'model': model,
        },
        error: function(rs, e) {
            console.log(rs.responseText);
        },
        success :
            function (reponse){
                container.animate({paddingLeft: '0'}, 800)
                if(reponse==="True"){
                    demamde_envoye = true
                    setTimeout(function (){
                        $("#spinner"+item_id).removeClass("spinner-border me-5").html("<p style='color: #869200; font-weight: bold'>Accés donné</p>").animate({width: '170px'}, 300)
                    }, 1000)
                }
                else {
                    setTimeout(function (){
                        $("#spinner"+item_id).removeClass("spinner-border me-5").html("<p style='color: red; font-weight: bold' class='btn-action-selecteur'>" + reponse + "</p>").animate({width: '335px'}, 300)
                    }, 1000)
                }
            }
    })
}

function addOrganisationUser(item){
    var service_id = item.attr('id')
    item.parent().html('<div id="spinner'+service_id+'" style="color: #869200;" class="spinner-border me-3" role="status"><span class="visually-hidden me-5">Loading...</span></div>')
    if (url_action) {
        $.get({
            url: url_action,
            data: {
                'service_id':  service_id,
                'array_id[]':  service_id,
                'user_id':  user_id,
            },
            success :
                function (reponse){
                    if(reponse==="True"){
                        demamde_envoye = true
                        setTimeout(function (){
                            $("#spinner"+service_id).removeClass("spinner-border me-5").html("<p style='color: #869200; font-weight: bold'>Organisation ajoutée</p>").animate({width: '170px'}, 300)
                        }, 1000)
                    }
                    else {
                        setTimeout(function (){
                            $("#spinner"+service_id).removeClass("spinner-border me-5").html(`<p style='color: red; font-weight: bold' class='btn-action-selecteur'>${reponse}</p>`).animate({width: '170px'}, 300)
                        }, 1000)
                    }
                }
        })
    }
}

$(document).on('click', '#send_multiple', function () {
    // Si pour la messagerie on remplie le champ destinataire
    if(action==="carnet_messagerie") {
        $("#destinataire_liste_chosen .chosen-drop").addClass('d-none')
        $("#destinataire_liste").val('').change().trigger("chosen:updated")
        $("#destinataire_liste").val(value_array).change().trigger("chosen:updated")
        $('#demandeAvisModal').modal('hide');

    }
})

$(document).on('click', '.btn-action-selecteur', function () {
    // Appel une url d'action si elle est fournie
    if (url_action) {
        if (action === 'demande_avis') {
            envoieDemandeAvis($(this).attr('id'), $(this))
        } else if (action === 'consultation') {
            accesConsultation($(this).parent().attr('data-service'), $(this).attr('id'), $(this))
        } else if (action === 'ajouter_invite') {
            envoieDemandeAvis($(this).attr('id'), $(this))
        } else if(action === "gestion-organisation-utilisateur") {
            addOrganisationUser($(this))
        } else if(action ==="gestion-membre-cdsr") {
            addOrganisationUser($(this))
        }
    }
    // Sinon remplie les donné d'un formulaire
    else {
        $('#id_service').val($(this).attr('id')).change()
        $("#span-service-selectione").text($("#name_container_"+$(this).attr('id')).find('a').text())
        $('#demandeAvisModal').modal('hide');
    }
})

$('#demandeAvisModal').on('hidden.bs.modal', function (event) {
    // Si une demande a était envoyer, recharge la page pour la faire apparaitre
    if (demamde_envoye === true) {
        location.reload();
    }
    // Cache les boutons de trie et d'action
    $('#deselect-all-button').addClass('d-none')
    $('#select-all-button').addClass('d-none')
    $('#sort-list-button').addClass('d-none')
    $('#container_tout_deplier').addClass('d-none')
})

function renderElement(element){
    var html = ""
    var icone_div = ""
    var btn_action = ""
    var is_inactif_class = ""
    var liste_sous_service = ""
    var icone_sous_service = ""
    var name_container = ""
    var agent_count = ""
    // Verifie sur l'organisation est active
    if(element.hasOwnProperty('actif') && !element["actif"]){
        // Si l'utilisateur peu la voir on l'affiche en rouge
        if(action==="administration" && is_admin === true){
            is_inactif_class = "text-danger"
        }
        // Sinon on ne l'affiche pas
        else {
            return html
        }
    }
    if(element["icones"]){
        // Remplie la div d'icone
        icone_div = add_icones(element["icones"])
    }
    if(action !== "administration"){
        // Remplie la span des bouton d'action
        btn_action = `<span class="col-3 mt-2 d-none button-span div_service d-flex" id="button_span_${element["id"]}">
                                <span style="width: 60px; height: 30px; line-height: 1;" class="ms-auto btn btn-success me-2 btn-action-selecteur id d-flex justify-content-center" id="${element["id"]}">
                                    <i style="color: white" class="fas fa-check" aria-hidden="true"></i>
                                </span>
                                <span style="width: 60px; height: 30px; line-height: 1;" class="btn btn-danger me-2 dismis d-flex justify-content-center">
                                    <i style="color: white" class="fas fa-times" aria-hidden="true"></i>
                                </span>
                            </span>`
    }
    // Sinon on affiche directement les sous services
    if(show_sous_service){
        // Construit la liste des sous services
        if(Object.keys(element["data"]).length > 0 ){
            for ([key, val] of Object.entries(element["data"])){
                liste_sous_service +=`<li class="py-2 ps-4 d-flex list-group-item p-0 item item_container" id="item_${val[1][2]}">
                                                <div class="py-2" id="name_container_${val[1][2]}">
                                                <a href="#" id="${val[1][1]}_${val[1][2]}" data-id="${val[1][2]}" class="name_service my-2 service-list-item list-item-link ${val[1][3] ? 'disabled' : '' }"> ${val[1][0]} </a>
                                                </div>
                                                <span data-service="${val[1][1]}" class="ms-auto d-none button-span d-flex" id="button_span_${val[1][2]}">
                                                    <span id="${val[1][2]}" style="width: 60px; height: 30px" class="btn btn-success me-2 btn-action-selecteur d-flex justify-content-center">
                                                        <i style="color: white" class="btn-ok fas fa-check" aria-hidden="true"></i>
                                                    </span>
                                                    <span style="width: 60px; height: 30px; line-height: 1;" class="btn btn-danger me-2 dismis">
                                                        <i style="color: white"class="fas fa-times" aria-hidden="true"></i>
                                                    </span>
                                                </span>
                                              </li>`
            }
        }
    }
    // Sinon ajoute une icone pour déployer les sous services
    else {
        // Si le service a des enfant carré plus
        if(element['has_children']){
            icone_sous_service =  `<i class="fas fa-plus-square me-0 ms-2 show_sous_service action_sous_service" data-service="${element["id"]}"></i>`
        }
        // Sinon carré disabled
        else{
            icone_sous_service = `<i class="fas fa-square me-0 ms-2 disabled action_sous_service"></i>`
        }
    }
    if (action === "carnet_messagerie") {
        name_container = ` 
                            <input class="form-check-input checkbox checkbox_id item service col-2" 
                                   ${value_array.includes(element["id"]+"_"+element["service"]) ? "checked" : ""} style="margin-top: 0.8rem" type="checkbox"
                                name="service" value="${element["id"]}_${element["service"]}" id="checkbox_${element["id"]}_${element["service"]}">
                                 <div class="name_container col-9 d-flex" id="name_container_${element["id"]}">
                                    ${icone_sous_service}
                                     <label class="name label ms-2 name_service pt-2" for="checkbox_${element["id"]}_${element["service"]}">${element["name"]}</label>
                                     ${element["smtp_error"] ? '<i class="fa-solid fa-circle-small text-danger ms-2 mt-1" title="Erreur d\'adresse email" data-bs-toggle="tooltip"></i>' : '' }
                                     <b class="get_agent ms-2" style="margin-top: 0.5rem" id="${element["id"]}"><i class="far fa-user"></i></b>
                                     <div class="agent_container" id="agent_${element["id"]}"></div>
                                 </div>`
    }
    else{
        name_container = `<div class="name_container ${element["icones"] ? 'col-6' : 'col-10' }" id="name_container_${element["id"]}">
                            ${icone_sous_service}
                            ${show_old && element["deja_fait"] ? `<i class='text-success far fa-check fa-fw'></i>` : ``}
                            <a href="#${action === "administration" ? element["id"] : `` }" data-id="${element["id"]}" 
                               class="${is_inactif_class} ${show_old && element["deja_fait"] ? `disabled` : ``} ${action === "administration" ? `btn-link` : `list-item-link`} 
                               name ${action === "administration" ? 'service-list-item' : `` } name_service my-2 ms-2">${element["name"]}</a>
                               ${element["smtp_error"] ? '<i class="fa-solid fa-circle-small text-danger ms-2 mt-1" title="Erreur d\'adresse e-mail" data-bs-toggle="tooltip"></i>' : '' }
                          </div>`
    }

    agent_count = add_agents_count(element["agents_count"])


    html = `<li class="item list-group-item p-0">
                <div class="item d-flex" id="item_${element["id"]}">
                    <div class="d-flex item_container pb-3 p-1 ${action !== "administration"  && action !== "carnet_messagerie"? `col-9` : `col-12` }" id="item_container_${element["id"]}">
                         ${name_container}
                         ${icone_div}
                         ${agent_count}
                    </div>
                     ${btn_action}
                </div>
            </li>
            <div id="services">
                    <ul class="liste_sous_service ps-4 list-group-flush ${!show_sous_service ? 'sous_service_show' : ''}" id="liste_sous_service_${element["id"]}">${liste_sous_service}</ul>
            </div>`
    return html
}

function init_selecteur(categorie_selected=false){
    $('#select-instances-modal').chosen('destroy');

    if(action==="administration" || action==="gestion-organisation-utilisateur" ||  action==="gestion-membre-cdsr"){
        $("#select-instances-modal").chosen({ width: '75%' });
        $("#select-instances-modal").val(secteur).trigger("chosen:updated");
    }
    else if(action==='demande_avis' || action==='ajouter_invite'){
        $("#select-instances-modal").val(secteur_initial).trigger("chosen:updated");
    }
    else {
        $("#select-instances-modal").chosen({ width: '100%' });
    }
    getListCategorie(action, ajout)
    if(secteur !== null){
        setTimeout(function(){remplirBadgeArray(action, ajout, secteur)}, 500)
    }

    if ($("#select-instances-modal").length && $("#select-instances-modal").val()) {
        $("#modal-categorie-selecteur").removeClass('d-none')
    }


    $("#select-instances-modal").unbind()
    $('.categorie').unbind()

    // Affiche les categories quand une instance est selectionné
    $("#select-instances-modal").change(function (){

        $("#modal-categorie-selecteur").removeClass('d-none')
        // Modifie la liste des catégorie selon le secteur sélectionné
        if(action !== "consultation" && ajout !== "complet" && action !== "demande_avis" && action !== "ajouter_invite"){
            secteur = $("#select-instances-modal").val()
            if(secteur.split("_")[0] !== "instance"){
                if(secteur==="national"){
                    getListCategorie("get_national")
                }
                if(secteur.split("_")[0] === "region"){
                    getListCategorie("get_regional")
                }
            }
            else {
                getListCategorie(action, ajout)
            }
        }
        if($('.categorie').length === 1){
            $('.categorie').addClass('selected')
        }

        if($('.selected').length){
            categorie_selected = $('.selected').attr('data-categorie')
        }
        if(categorie_selected){
            // Filtre la liste si une categorie est selectionné
            $('li[data-categorie='+categorie_selected+']').addClass('selected')
            setTimeout(function(){$('li[data-categorie='+categorie_selected+']').addClass('selected')} , 500)
            setTimeout(function(){listFilter(categorie_selected)} , 600)
        }
        setTimeout(function(){remplirBadgeArray(action, ajout, secteur)}, 500)
    })
    if (selecteur_multiple && !$("#send_multiple").length){
        $("#modal-footer-selecteur").append('<button type="button" class="btn btn-primary disabled" id="send_multiple">Valider</button>')
    }
    else if (!selecteur_multiple && $("#send_multiple").length) {
        $("#modal-footer-selecteur").empty()
    }

    // Filtre le liste des service
    function listFilter(categorie){
        var items_list = ""
        // Réinitialise les function et valeurs
        $("#send_multiple").unbind()
        $('.btn-action-selecteur').unbind()
        $('.get_agent').unbind()
        $('.show_sous_service').unbind()
        $('.hide_sous_service').unbind()
        $("#no-result").addClass('d-none')
        $("#service-search").val('').removeClass("d-none")
        $('.tout_replier').removeClass('tout_replier').addClass('tout_deplier').attr('data-bs-original-title', "Tout déplier")

        // Édite le placeholder de la recherche selon la catégorie
        if(categorie==="mairie"){
            $("#service-search").attr('placeholder', 'Rechercher une mairie')
        }
        else if(categorie==="structure"){
            $("#service-search").attr('placeholder', 'Rechercher une structure')
        }
        else if (categorie.startsWith('orga')){
            $("#service-search").attr('placeholder', 'Rechercher un organisateur')
        }
        else if (categorie.startsWith('instructeur')){
            $("#service-search").attr('placeholder', 'Rechercher un instructeur')
        }
        else{
            $("#service-search").attr('placeholder', 'Rechercher un service')
        }

        $("#service-liste").empty()
        $("#service-liste").append(html_chargement_en_cours())
        if($("#select-instances-modal").length && $("#select-instances-modal").val() !== null){
            secteur = $("#select-instances-modal").val()
        }
        if(action==="gestion-organisation-utilisateur" || action==="administration" ||  action==="gestion-membre-cdsr") {
            is_admin = user_is_admin(categorie, secteur)
        }
        $.get({
            url: url_list,
            data: {
                'secteur': secteur,
                'categorie':  categorie,
                'object_id':  object_id,
                'action':  action,
                'show_agent': show_agent,
                'show_old': show_old,
                'show_sous_service': show_sous_service,
                'manif': manif
            },
            success :
                function(reponse){
                    $("#services").find(".list").empty()
                    // Parcour les elements retourner pour les ajouter a la liste
                    for (i = 0; i < reponse.length; i++) {
                        items_list += renderElement(reponse[i])
                    }
                    // Ajoute les services à la liste
                    document.querySelector('#service-liste').innerHTML = items_list

                    // Appel la fonction pour ajouter le bouton de création
                    if(action === "administration"){
                        addBtnCreate(categorie, secteur)
                    }
                    // Affiche le bouton pour déplier tout les sous-service
                    if(!show_sous_service){
                        $('#container_tout_deplier').removeClass('d-none')
                    }
                    // Affiche une alert si la liste est vide
                    if($("#service-liste").find('li').length < 1){
                        $("#no-result").removeClass('d-none')
                    }
                    $("#service-search").keyup(function (){
                        if($("#service-liste").find('li').length < 1){
                            $("#no-result").removeClass('d-none')
                        }
                        else {
                            $("#no-result").addClass('d-none')
                        }
                    })
                    // Affiche les actions de selection
                    if(selecteur_multiple){
                        $("#send_multiple").removeClass('d-none')
                        if (!$("#service-liste input:checked").length) {
                            $('#select-all-button').removeClass('d-none')
                            $('#deselect-all-button').addClass('d-none')
                        }
                        else {
                            $('#deselect-all-button').removeClass('d-none')
                            $('#select-all-button').addClass('d-none')
                        }
                    }
                },

        }).done(() => {
            // Appel AfficherIcones() une fois l'ajax terminer pour gagner en performance
            setTimeout(() =>
                AfficherIcones(), 1)
        })
    }

    $("#modal-categorie-selecteur").on('click', '.categorie', function (){
        let categorie = $(this).attr('data-categorie')
        setTimeout(function(){listFilter(categorie)}, 200)
        $('.selected').removeClass('selected')
        $(this).addClass('selected')
        if($("#id_categorie").length){
            $('#id_categorie option').removeAttr("selected");
            $('#id_categorie option[value='+$(this).text()+']').attr('selected','selected');
            $('#id_categorie').val($(this).text()).text()
        }
    })


    if(categorie_selected){
        setTimeout(function(){listFilter(categorie_selected)} , 700)
    }
}

// Trigger le click sur les checkbox du sélecteur multiple
$("#service-liste").on('change', 'input', function (){
    console.log('change')
    var categorie_selectionne = $("#modal-categorie-selecteur .selected").attr('data-categorie')
    if($(this).prop('checked')){
        console.log('prop checked')
        // Selectionne tout les agents si un service est check
        if($(this).hasClass('service')){
            console.log($(this).hasClass('service'))
            $(`#agent_`+$(this).attr('id').split('_')[1]).find('input').each(function (){
                $(this).attr('disabled', 'disabled').prop('checked', true)
                // Supprime l'agent du tableau si il était sélectionner
                if(value_array.includes($(this).val())) {
                    value_array.splice($.inArray($(this).val(), value_array), 1);
                }
            })
        }
        if(!value_array.includes($(this).val()))
        {
            // Ajoute la valeur dans le tableau
            value_array.push($(this).val())
            // Additionne le nombre de sélectionner pour les badges
            badge_array[secteur][categorie_selectionne] += 1
            badge_array[secteur]['total'] += 1
            $(`#badge_${categorie_selectionne}`).text(badge_array[secteur][categorie_selectionne])
            $(`.chosen-drop .chosen-results li[data-value='${secteur}']`).append(
                `<h1>${badge_array[secteur]['total']}</h1>`
            )
            $("#instance").trigger("chosen:updated")
        }
    }
    else {
        // Supprime la valeur dans le tableau
        value_array.splice($.inArray($(this).val(), value_array), 1);
        // Déselectionne tout les agents du service
        if($(this).hasClass('service')){
            $(`#agent_`+$(this).attr('id').split('_')[1]).find('input').removeAttr('disabled').prop('checked',false)
        }
        // Soustrait le nombre de sélectionner pour les badges
        badge_array[secteur][categorie_selectionne] -= 1
        badge_array[secteur]['total'] -= 1
        if(badge_array[secteur][categorie_selectionne] > 0){
            $(`#badge_${categorie_selectionne}`).text(badge_array[secteur][categorie_selectionne])
        }
        else {
            $(`#badge_${categorie_selectionne}`).empty()
        }

    }
    if(value_array.length > 0){
        $("#send_multiple").removeClass('disabled')
    }
    else {
        $("#send_multiple").addClass('disabled')
    }
})

// Sélectionne toutes les checkbox
$("#select-all-button").click(function (){
    $("#service-liste input").each(function (){
        if(!$(this).prop('checked')){
            $(this).trigger('click')
        }
    })
    $('#deselect-all-button').removeClass('d-none')
    $('#select-all-button').addClass('d-none')
})

// Désélectionne toutes les checkbox
$("#deselect-all-button").click(function (){
    $("#service-liste input").each(function (){
        if($(this).prop('checked')){
            $(this).trigger('click')
        }
    })
    $('#deselect-all-button').addClass('d-none')
    $('#select-all-button').removeClass('d-none')
})

$('#demandeAvisModal').on('show.bs.modal', function () {
    init_selecteur()
})

$(document).ready(function () {
    if ($("#select-instances-modal").length && !$("#select-instances-modal").val()){
        $("#modal-categorie-selecteur").addClass('d-none')
    }
    $("#no-result").text(avertissement)
    $("#modal-title-selecteur").text(modal_title)
})