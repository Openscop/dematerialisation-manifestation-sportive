# coding: utf-8
from ajax_select import register, LookupChannel
from unidecode import unidecode

from structure.models import StructureOrganisatrice


@register('structureorganisatrice')
class StructureOrganisatriceLookup(LookupChannel):
    """ Lookup AJAX des structures """

    # Configuration
    model = StructureOrganisatrice

    # Overrides
    def get_query(self, q, request):
        q = unidecode(q)
        return self.model.objects.filter(precision_nom_s__icontains=q).order_by('name')[:20]

    def format_item_display(self, item):
        return u"<span class='tag'>{name}</span>".format(name=item.precision_nom_s)

    def format_match(self, item):
        return u"<span class='tag'>{name}</span>".format(name=item.precision_nom_s)
