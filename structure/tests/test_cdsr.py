import re
import locale

from django.contrib.auth.hashers import make_password as make
from django.test import override_settings
from django.shortcuts import reverse
from django.utils import timezone
from bs4 import BeautifulSoup as Bs
from post_office.models import Email

from structure.factories.service import ServiceConsulteFactory
from instructions.factories import InstructionFactory
from core.factories import GroupFactory, UserFactory
from instructions.tests.test_base import TestCommunClass
from instructions.models import Avis
from structure.models import ServiceCDSR, PresentationCDSR, ReunionCDSR
from messagerie.models import Message


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class CDSRTests(TestCommunClass):
    """
    Test de la fonctionnalité CDSR :
        Ajouter un membre permanent.
        Supprimer un membre permanent.
        Demander une présentation CDSR.
        Annuler une présentation CDSR.
        Créer une réunion CDSR.
        Supprimer le lien avec la réunion CDSR.
        Choisir une réunion pour une présentation.
        Ajouter un membre invité.
        Retirer un membre invité.
        Editer une réunion pour fixer une date.
        Définir une heure de présentation.
        Envoyer les convocations invités
        Envoyer les convocations permanents
        Vérifier les avis générés.
        Confirmer la présence à la réunion
        Rendre l'avis CDSR
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('============ CDSR (Clt) =============')
        TestCommunClass.init_setup(cls)
        cls.sncf = ServiceConsulteFactory(departement_m2m=cls.dep, instance_fk=cls.dep.instance, precision_nom_s="SNCF",
                                          type_service_fk__abreviation_s="SNCF", type_service_fk__nom_s="Transport",
                                          type_service_fk__categorie_fk__nom_s="Autre service",
                                          admin_service_famille_b=True, admin_utilisateurs_subalternes_b=True,
                                          porte_entree_avis_b=True, autorise_rendre_avis_b=True,
                                          autorise_acces_consult_organisateur_b=True)
        cls.agent_sncf = UserFactory.create(username='agent_sncf', password=make("123"), default_instance=cls.dep.instance, email='scnf@test.fr')
        cls.agent_sncf.organisation_m2m.add(cls.sncf)
        cls.agent_sdis2 = UserFactory.create(username='agent_sdis2', password=make("123"), default_instance=cls.dep.instance, email='sdis2@test.fr')
        cls.agent_sdis2.organisation_m2m.add(cls.sdis)
        grp = GroupFactory.create(name='Administrateurs d\'instance')
        cls.agentlocal_edsr.delete()
        cls.instructeur.groups.add(grp)

    def test_cdsr(self):
        """ Test CDSR """
        print('**** test 1 administration du service CDSR ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="instructeur", password='123'))
        instance = self.prefecture.instance_fk
        service_cdsr = ServiceCDSR.objects.get(instance_fk=instance)
        # page administration des services
        url_orga_detail = reverse('structure:organisation_detail') + "?object_id=" + str(service_cdsr.pk)
        reponse = self.client.get(url_orga_detail, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "Membre permanent de la structure")
        self.assertContains(reponse, "Aucun service membre rattaché à ce service")
        # Ajouter un membre permanent au service
        ########################################
        url_gerer_cdsr = reverse('structure:gerer-membre-cdsr', kwargs={'pk': service_cdsr.pk})
        reponse = self.client.get(url_gerer_cdsr, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "Ajouter un membre permanent")
        url_cdsr_add = reverse('structure:ajouter_membre_cdsr_ajax', kwargs={'pk': service_cdsr.pk})
        url_cdsr_add += f"?array_id[]={self.ddsp.pk}"
        reponse = self.client.get(url_cdsr_add, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "True")
        reponse = self.client.get(url_orga_detail, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "Membre permanent de la structure")
        self.assertContains(reponse, "DDSP", count=2)
        # Supprimer un membre permanent au service
        ##########################################
        url_cdsr_del = reverse('structure:retirer_membre_cdsr_ajax', kwargs={'pk': service_cdsr.pk})
        url_cdsr_del += f"?organisation_id={self.ddsp.pk}"
        reponse = self.client.get(url_cdsr_del, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "Membre du CDSR supprimé")
        reponse = self.client.get(url_orga_detail, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "Membre permanent de la structure")
        self.assertContains(reponse, "Aucun service membre rattaché à ce service")
        # Contituer le service CDSR
        ###########################
        url_cdsr_add += f"&array_id[]={self.edsr.pk}&array_id[]={self.sdis.pk}"
        reponse = self.client.get(url_cdsr_add, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "True")
        reponse = self.client.get(url_orga_detail, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "Membres permanents de la structure")
        self.assertContains(reponse, "DDSP", count=2)
        self.assertContains(reponse, "EDSR", count=2)
        self.assertContains(reponse, "SDIS", count=2)

        print('**** test 2 présentation CDSR ****')
        self.instructeur.groups.remove()
        self.instruction = InstructionFactory.create(manif=self.manifestation)
        self.presence_avis('instructeur', 'nouveau')
        url_intr_list = "/instructions/tableaudebord/" + str(self.prefecture.pk) + "/list/"
        reponse = self.client.get(url_intr_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action_dispo = str(html.select("div.liste-actions:not(.border-danger)"))
        self.assertIn('Présenter en CDSR', action_dispo)
        # Demander une présentation CDSR
        ################################
        url_cdsr_avis = re.search('href="(?P<url>(/[^"]+)).+\\n.+Présenter en CDSR', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_cdsr_avis, 'group'))
        reponse = self.client.get(url_cdsr_avis.group('url'), HTTP_HOST='127.0.0.1:8000', follow=True)
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action_dispo = str(html.select("div.liste-actions:not(.border-danger)"))
        self.assertNotIn('Présenter en CDSR', action_dispo)
        self.assertContains(reponse, "Liste des avis demandés")
        self.assertContains(reponse, "CDSR (42)")
        # Annuler la présentation CDSR
        ##############################
        url_cdsr_avis_del = re.search('href="(?P<url>(/[^"]+))">Confirmer l\'annulation', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_cdsr_avis_del, 'group'))
        referer = re.search('href="(?P<url>(/[^"]+))publish/"', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(referer, 'group'))
        reponse = self.client.get(url_cdsr_avis_del.group('url'), HTTP_HOST='127.0.0.1:8000', follow=True,
                                  HTTP_REFERER=referer.group('url'))
        self.assertContains(reponse, "Liste des avis demandés")
        self.assertNotContains(reponse, "CDSR (42)")
        self.assertContains(reponse, "Présenter en CDSR")
        # Ajouter la présentation
        #########################
        reponse = self.client.get(url_cdsr_avis.group('url'), HTTP_HOST='127.0.0.1:8000', follow=True)
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        avis_cdsr = str(html.select(f"div.card[id=card_{service_cdsr.pk}]"))
        self.assertIn("CDSR (42)", avis_cdsr)
        # Liste des membres
        liste_permanent = str(html.select(f"div.membres_permanents"))
        self.assertIn("remplie automatiquement", liste_permanent)
        self.assertNotIn("DDSP (42)", liste_permanent)
        self.assertNotIn("EDSR (42)", liste_permanent)
        self.assertNotIn("SDIS (42)", liste_permanent)
        liste_invites = str(html.select(f"div.membres_invites"))
        self.assertIn(str(self.structure), liste_invites)
        # Créer une réunion CDSR
        ########################
        url_cdsr_add_reunion = re.search('data-url-form-pk="(?P<url>(/[^"]+))">', avis_cdsr)
        self.assertTrue(hasattr(url_cdsr_add_reunion, 'group'))
        reponse = self.client.post(url_cdsr_add_reunion.group('url'), data={'nom_s': 'fin de mois'},
                                   HTTP_HOST='127.0.0.1:8000', follow=True, HTTP_REFERER=referer.group('url'))
        self.assertEqual(200, reponse.status_code)
        # Rappel vue de détail
        reponse = self.client.get(referer.group('url'), HTTP_HOST='127.0.0.1:8000')
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        avis_cdsr = str(html.select(f"div.card[id=card_{service_cdsr.pk}]"))
        self.assertIn("Nom de la réunion CDSR : fin de mois", avis_cdsr)
        self.assertIn("CDSR (42)", avis_cdsr)
        # Liste des membres
        liste_permanent = str(html.select(f"div.membres_permanents"))
        self.assertIn("DDSP (42)", liste_permanent)
        self.assertIn("EDSR (42)", liste_permanent)
        self.assertIn("SDIS (42)", liste_permanent)
        liste_invites = str(html.select(f"div.membres_invites"))
        self.assertIn(str(self.structure), liste_invites)
        # Les avis générés CDSR
        avis_ddsp = str(html.select(f"div.card[id=card_{self.ddsp.pk}]"))
        self.assertIn("DDSP (42)", avis_ddsp)
        avis_edsr = str(html.select(f"div.card[id=card_{self.edsr.pk}]"))
        self.assertIn("EDSR (42)", avis_edsr)
        avis_sdis = str(html.select(f"div.card[id=card_{self.sdis.pk}]"))
        self.assertIn("SDIS (42)", avis_sdis)
        # Supprimer le lien avec la réunion CDSR
        ########################################
        url_cdsr_remove_reunion = re.search('href="(?P<url>(/[^"]+))"><i class="supprimer', avis_cdsr)
        self.assertTrue(hasattr(url_cdsr_remove_reunion, 'group'))
        reponse = self.client.get(url_cdsr_remove_reunion.group('url'), HTTP_HOST='127.0.0.1:8000',
                                  follow=True, HTTP_REFERER=referer.group('url'))
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        avis_cdsr = str(html.select(f"div.card[id=card_{service_cdsr.pk}]"))
        self.assertIn("CDSR (42)", avis_cdsr)
        # Liste des membres
        liste_permanent = str(html.select(f"div.membres_permanents"))
        self.assertIn("remplie automatiquement", liste_permanent)
        # Choisir une réunion pour une présentation
        ###########################################
        pk_sel_reunion = re.search('value="(?P<pk>\d+)">fin de mois</option>', avis_cdsr)
        self.assertTrue(hasattr(pk_sel_reunion, 'group'))
        url_sel_reunion = re.search('url_set_date_cdsr = "(?P<url>(/[^"]+))";', avis_cdsr)
        self.assertTrue(hasattr(url_sel_reunion, 'group'))
        reponse = self.client.get(url_sel_reunion.group('url') + f"?reunion={pk_sel_reunion.group('pk')}", HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(200, reponse.status_code)
        # Rappel vue de détail
        reponse = self.client.get(referer.group('url'), HTTP_HOST='127.0.0.1:8000')
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        avis_cdsr = str(html.select(f"div.card[id=card_{service_cdsr.pk}]"))
        self.assertIn("Nom de la réunion CDSR : fin de mois", avis_cdsr)
        self.assertIn("CDSR (42)", avis_cdsr)
        # Liste des membres
        liste_permanent = str(html.select(f"div.membres_permanents"))
        self.assertIn("DDSP (42)", liste_permanent)
        self.assertIn("EDSR (42)", liste_permanent)
        self.assertIn("SDIS (42)", liste_permanent)
        liste_invites = str(html.select(f"div.membres_invites"))
        self.assertIn(str(self.structure), liste_invites)
        # Ajouter un membre invité
        ##########################
        url_invite_cdsr = re.search('url_invite_cdsr = "(?P<url>(/[^"]+))";', avis_cdsr)
        self.assertTrue(hasattr(url_invite_cdsr, 'group'))
        reponse = self.client.get(url_invite_cdsr.group('url') + f"?array_id[]={self.sncf.pk}",
                                  HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(200, reponse.status_code)
        # Rappel vue de détail
        reponse = self.client.get(referer.group('url'), HTTP_HOST='127.0.0.1:8000')
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        liste_invites = str(html.select(f"div.membres_invites"))
        self.assertIn(str(self.structure), liste_invites)
        self.assertIn(str(self.sncf), liste_invites)
        # Retirer un membre invité
        ##########################
        url_invite_rem = re.search('data-url="(?P<url>(/[^"]+))" ', liste_invites)
        self.assertTrue(hasattr(url_invite_rem, 'group'))
        reponse = self.client.get(url_invite_rem.group('url') + f"?membre_id={self.sncf.pk}", HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(200, reponse.status_code)
        # Rappel vue de détail
        reponse = self.client.get(referer.group('url'), HTTP_HOST='127.0.0.1:8000')
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        liste_invites = str(html.select(f"div.membres_invites"))
        self.assertIn(str(self.structure), liste_invites)
        self.assertNotIn(str(self.sncf), liste_invites)
        # Rajouter invité
        #################
        reponse = self.client.get(url_invite_cdsr.group('url') + f"?array_id[]={self.sncf.pk}",
                                  HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(200, reponse.status_code)
        # Rappel vue de détail
        reponse = self.client.get(referer.group('url'), HTTP_HOST='127.0.0.1:8000')
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        liste_invites = str(html.select(f"div.membres_invites"))
        self.assertIn(str(self.structure), liste_invites)
        self.assertIn(str(self.sncf), liste_invites)

        print('**** test 3 réunion CDSR ****')
        # Editer une réunion pour fixer une date
        ########################################
        # appel de la vue liste
        reponse = self.client.get(reverse('structure:reunionCDSR_list'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "Liste des réunions CDSR", count=2)
        self.assertContains(reponse, "bg-warning")
        self.assertContains(reponse, "fin de mois")
        # appel de la vue détail
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        tab = str(html.select("#tab_fin-de-mois"))
        pk = re.search('pk="(?P<pk>(\d+))" ', tab)
        self.assertTrue(hasattr(pk, 'group'))
        reponse = self.client.get(reverse('structure:reunionCDSR_detail', kwargs={"pk": pk.group('pk')}), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "Manifestation_Test")
        self.assertContains(reponse, "Editer la réunion afin de définir sa date")
        self.assertContains(reponse, '<i class="editer"></i>Editer la réunion')
        self.assertNotContains(reponse, "Envoyer la convocation des membres invités")
        self.assertNotContains(reponse, "Envoyer la convocation des membres permanents")
        url_cdsr_update = re.search('data-url-form-pk="(?P<url>(/[^"]+))">', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_cdsr_update, 'group'))
        date = (timezone.now() + timezone.timedelta(days=30)).strftime('%d/%m/%Y')
        reponse = self.client.post(url_cdsr_update.group('url'), data={'date': date}, follow=True, HTTP_HOST='127.0.0.1:8000')
        reponse = self.client.get(reponse.content.decode('utf-8'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "bg-info")
        self.assertContains(reponse, date)
        self.assertContains(reponse, "champ nom_s modifié par Bob ROBERT")
        self.assertContains(reponse, "champ date modifié par Bob ROBERT")
        reponse = self.client.get(reverse('structure:reunionCDSR_detail', kwargs={"pk": pk.group('pk')}), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "Manifestation_Test")
        self.assertContains(reponse, "Définir un horaire pour la présentation")
        self.assertNotContains(reponse, "Envoyer la convocation des membres invités")
        self.assertNotContains(reponse, "Envoyer la convocation des membres permanents")
        # Définir une heure de présentation
        ###################################
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        accordion = str(html.select(".accordion-item"))
        url_cdsr_set_heure = re.search('data-url-form-pk="(?P<url>(/[^"]+))">', accordion)
        self.assertTrue(hasattr(url_cdsr_set_heure, 'group'))
        reponse = self.client.post(url_cdsr_set_heure.group('url'), data={'heure': "10:00"}, HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(200, reponse.status_code)
        reponse = self.client.get(reverse('structure:reunionCDSR_detail', kwargs={"pk": pk.group('pk')}), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "Manifestation_Test")
        self.assertContains(reponse, "10h00", count=2)
        self.assertContains(reponse, "Envoyer la convocation des membres invités")
        self.assertContains(reponse, "Envoyer la convocation des membres permanents")
        self.assertContains(reponse, "Editer la réunion")
        # Envoyer les convocations invités
        ##################################0
        Email.objects.all().delete()
        Message.objects.all().delete()
        url_convoc_invites = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la convocation des membres invités',  reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_convoc_invites, 'group'))
        with open('/tmp/visa_federation.txt') as openfile:
            data = {'precision': "précision sur la présentation", 'fichier': openfile}
            reponse = self.client.post(url_convoc_invites.group('url'), data=data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(200, reponse.status_code)
        reponse = self.client.get(reverse('structure:reunionCDSR_detail', kwargs={"pk": pk.group('pk')}), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "Manifestation_Test")
        self.assertNotContains(reponse, "Envoyer la convocation des membres invités")
        self.assertContains(reponse, "La convocation des membres invités a été envoyée")
        self.assertContains(reponse, "Envoyer la convocation des membres permanents")
        self.assertNotContains(reponse, '<i class="editer"></i>Editer la réunion')
        self.assertContains(reponse, "Ordre du jour")
        # Vérifier les messages et emails envoyés
        id_avis_sncf = Avis.objects.get(service_consulte_origine_fk=self.sncf).id
        id_presentation = PresentationCDSR.objects.last().id
        id_reunion = ReunionCDSR.objects.last().id
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        index = len(outbox)
        index_mess = len(messages)
        self.assertEqual(len(messages), 2)
        self.assertIn('convoqué à la réunion CDSR', messages[0].corps)
        self.assertIn('précision sur la présentation', messages[0].corps)
        self.assertEqual(2, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.organisateur)
        self.assertEqual(messages[0].message_enveloppe.last().destinataire_id, self.agent_sncf)
        self.assertEqual(messages[0].object_id, id_presentation)          # Présentation
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "presentation")
        arbo = f"{self.dep.name}/reunion_cdsr/{id_reunion}/presentation_cdsr/{id_presentation}/"
        self.assertEqual(messages[0].fichier.name, f"{arbo}visa_federation.txt")
        self.assertIn('Demande d\'avis à traiter', messages[1].corps)
        self.assertEqual(1, messages[1].message_enveloppe.all().count())
        self.assertEqual(messages[1].message_enveloppe.first().destinataire_id, self.agent_sncf)
        self.assertEqual(messages[1].object_id, id_avis_sncf)          # Avis SNCF
        self.assertEqual(messages[1].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.structure.email_s])
        self.assertEqual(outbox[0].subject, 'Convocation en réunion CDSR')
        self.assertEqual(outbox[0].attachments.get().name, "visa_federation.txt")
        self.assertEqual(outbox[1].to, [self.sncf.email_s])
        self.assertEqual(outbox[1].attachments.get().name, "visa_federation.txt")
        # Envoyer les convocations permanents
        #####################################
        url_convoc_permanents = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la convocation des membres permanents',  reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_convoc_permanents, 'group'))
        with open('/tmp/visa_federation.txt') as openfile:
            data = {'precision': "précision sur la réunion", 'fichier': openfile}
            reponse = self.client.post(url_convoc_permanents.group('url'), data=data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(200, reponse.status_code)
        reponse = self.client.get(reverse('structure:reunionCDSR_detail', kwargs={"pk": pk.group('pk')}), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "La convocation des membres invités a été envoyée")
        self.assertContains(reponse, "La convocation des membres permanents a été envoyée")
        self.assertContains(reponse, "Ordre du jour", count=2)
        # Vérifier les messages et emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        # for out in outbox:
        #     print(out.to, end="")
        #     print("--" + out.subject)
        # for mess in messages:
        #     for env in mess.message_enveloppe.all():
        #         print("**" + env.destinataire_txt)
        #     print("---" + str(mess.pk) + mess.corps)
        self.assertEqual(len(messages), 1)
        self.assertIn('convoqué à la réunion CDSR', messages[0].corps)
        self.assertIn('précision sur la réunion', messages[0].corps)
        self.assertEqual(4, messages[0].message_enveloppe.all().count())
        destinaires = [self.agent_ddsp, self.agent_edsr, self.agent_sdis, self.agent_sdis2]
        for env in messages[0].message_enveloppe.all():
            self.assertIn(env.destinataire_id, destinaires)
        self.assertEqual(messages[0].object_id, id_reunion)          # Réunion
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "reunion")
        arbo = f"{self.dep.name}/reunion_cdsr/{id_reunion}/"
        self.assertEqual(messages[0].fichier.name, f"{arbo}visa_federation.txt")
        self.assertEqual(len(outbox), 3)
        mailist = [self.sdis.email_s, self.ddsp.email_s, self.edsr.email_s]
        for email in outbox:
            self.assertIn(email.to[0], mailist)
            self.assertEqual(email.attachments.get().name, "visa_federation.txt")
        self.assertEqual(outbox[0].subject, 'Convocation en réunion CDSR')
        # Vérifier les avis générés
        ###########################
        reponse = self.client.get(referer.group('url'), HTTP_HOST='127.0.0.1:8000')
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        # Les avis générés CDSR
        avis_ddsp = str(html.select(f"div.card[id=card_{self.ddsp.pk}]"))
        self.assertIn("DDSP (42)", avis_ddsp)
        avis_edsr = str(html.select(f"div.card[id=card_{self.edsr.pk}]"))
        self.assertIn("EDSR (42)", avis_edsr)
        avis_sdis = str(html.select(f"div.card[id=card_{self.sdis.pk}]"))
        self.assertIn("SDIS (42)", avis_sdis)
        avis_sncf = str(html.select(f"div.card[id=card_{self.sncf.pk}]"))
        self.assertIn("SNCF (42)", avis_sncf)

        print('**** test 4 confirmation CDSR ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_ddsp', 'nouveau')
        url_list = "/instructions/tableaudebord/" + str(self.ddsp.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        self.assertContains(reponse, "À rédiger")
        locale.setlocale(locale.LC_ALL, 'fr_FR')
        self.assertContains(reponse, (timezone.now() + timezone.timedelta(days=30)).strftime('%d %B %Y'))
        self.assertContains(reponse, "cet avis doit être présenté à la réunion CDSR")
        # Confirmer la présence à la réunion
        ####################################
        reponse = self.confirmer_presence_cdsr(reponse)
        # Rédiger et rendre l'avis
        reponse = self.rediger_avis(reponse, precision="Précisions DDSP sur l'avis")
        self.rendre_avis(reponse)
        self.client.logout()

        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_sdis', 'nouveau')
        url_list = "/instructions/tableaudebord/" + str(self.sdis.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        reponse = self.vue_detail(reponse)
        reponse = self.confirmer_presence_cdsr(reponse)
        # Rédiger et rendre l'avis
        reponse = self.rediger_avis(reponse, precision="Précisions SDIS sur l'avis")
        self.rendre_avis(reponse)
        self.client.logout()

        # Instruction de l'avis par la sncf, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_sncf', 'nouveau')
        url_list = "/instructions/tableaudebord/" + str(self.sncf.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        reponse = self.vue_detail(reponse)
        reponse = self.confirmer_presence_cdsr(reponse)
        # Rédiger et rendre l'avis
        reponse = self.rediger_avis(reponse, precision="Précisions SNCF sur l'avis")
        self.rendre_avis(reponse)
        self.client.logout()

        # Instruction de l'avis par l'edsr et le ggd, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_edsr', 'nouveau')
        url_list = "/instructions/tableaudebord/" + str(self.edsr.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        reponse = self.vue_detail(reponse)
        reponse = self.confirmer_presence_cdsr(reponse)
        # Rédiger et rendre l'avis
        reponse = self.rediger_avis(reponse, precision="Précisions EDSR sur l'avis")
        self.adresser_avis(reponse, self.ggd.pk)
        self.client.logout()
        # GGD
        self.presence_avis('agent_ggd', 'nouveau')
        url_list = "/instructions/tableaudebord/" + str(self.ggd.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse, precision="Précisions GGD sur l'avis")
        self.rendre_avis(reponse)
        self.client.logout()

        print('**** test 5 rendu CDSR ****')
        self.presence_avis('instructeur', 'encours')
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        reponse = self.vue_detail(reponse)
        # Rendre l'avis CDSR
        ####################
        self.assertContains(reponse, "convocation confirmée", count=4)
        self.assertContains(reponse, "Rédiger l'avis synthétique")
        self.assertContains(reponse, "Ajouter une pièce jointe")
        rediger = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+Rédiger l\'avis synthétique', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(rediger, 'group'))
        formulaire = self.client.get(rediger.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(formulaire, "Pré-remplir avec l'ensemble<br> des avis des membres permanents")
        # Pré-remplissage de l'avis
        avis_cdsr_pk = rediger.group('url').split('/')[3]
        url_remplir_avis = reverse('instructions:avis_get_avis', kwargs={'pk': avis_cdsr_pk})
        contenu_pre_rempli = self.client.get(url_remplir_avis, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(contenu_pre_rempli, "Précisions DDSP sur l'avis")
        self.assertContains(contenu_pre_rempli, "Précisions SDIS sur l'avis")
        self.assertContains(contenu_pre_rempli, "Précisions GGD sur l'avis")
        self.assertNotContains(contenu_pre_rempli, "Précisions SNCF sur l'avis")
        data = {'prescriptions': contenu_pre_rempli, 'favorable': True}
        reponse = self.client.post(rediger.group('url'), data=data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "Valider et rendre l'avis synthétique")
        ackno = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+Valider et rendre l\'avis synthétique', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(ackno, 'group'))
        reponse = self.client.get(ackno.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, "Redemander l'avis")
        self.assertContains(reponse, "Ajouter une pièce jointe")
        self.client.logout()

        # print(reponse.content.decode('utf-8'))

