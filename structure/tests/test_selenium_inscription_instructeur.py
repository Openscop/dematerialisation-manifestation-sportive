from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.by import By
from django.core import mail
from django.test import tag, override_settings
import time

from administrative_division.factories import DepartementFactory, ArrondissementFactory
from core.models.instance import Instance
from core.models import User


class InscriptionInstructeur(StaticLiveServerTestCase):
    """
    Test de l'inscription d'un agent de préfecture
    """
    DELAY = 0.4

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
        """
        print()
        print('============ Inscription agent (Sel) =============')
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(600, 800)

        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__instruction_mode=Instance.IM_CIRCUIT_B)
        cls.arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        cls.prefecture = cls.arrondissement.organisations.first()

        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """
        cls.selenium.quit()
        super().tearDownClass()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element(By.ID, id_chosen)
        chosen_select.find_element(By.CSS_SELECTOR,  "a").click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements(By.TAG_NAME, 'li')
        for result in results:
            if value in result.text:
                result.click()

    def remplir_form(self, nom_param, prenom_param, username_param, email_param):
        """
        Remplir le formulaire avec les données transmises
        """
        prenom = self.selenium.find_element(By.ID, 'id_first_name')
        prenom.send_keys(prenom_param)
        time.sleep(self.DELAY)

        nom = self.selenium.find_element(By.ID, 'id_last_name')
        nom.send_keys(nom_param)
        time.sleep(self.DELAY)

        id_user = self.selenium.find_element(By.ID, 'id_username')
        id_user.send_keys(username_param)
        time.sleep(self.DELAY)

        email = self.selenium.find_element(By.ID, 'id_email')
        email.send_keys(email_param)
        time.sleep(self.DELAY)

        pass1 = self.selenium.find_element(By.ID, 'id_password1')
        pass1.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)

        pass2 = self.selenium.find_element(By.ID, 'id_password2')
        pass2.send_keys('azeaz5646dfzdsf')

        self.selenium.execute_script("window.scroll(0, 1200)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, 'btn-modal-selecteur').click()
        time.sleep(self.DELAY * 2)

        self.chosen_select('select_instances_modal_chosen', 'instance de test (42)')
        time.sleep(self.DELAY * 2)

        self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % "Service préfectoral").click()
        time.sleep(self.DELAY * 2)

        self.selenium.find_element(By.CLASS_NAME, 'list-item-link').click()
        time.sleep(self.DELAY)

        self.selenium.find_element(By.CLASS_NAME, 'btn-action-selecteur').click()
        time.sleep(self.DELAY)

    @tag('selenium')
    @override_settings(DEBUG=True)
    def test_inscription_instructeur(self):
        """
        Test complet de l'inscription d'un agent de préfecture
        """

        print('**** test 1 remplissage formulaire d\'inscription agent ****')

        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/agent'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Création d\'un compte Agent', self.selenium.page_source)

        self.remplir_form('Dupont', 'Jean', 'jandup', 'jandup@manifestationsportive.fr')
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        print('**** test 2 vérification ****')
        self.assertIn('Vérifiez votre adresse email', self.selenium.page_source)
        self.assertIn('E-mail de confirmation envoyé à jandup@manifestationsportive.fr', self.selenium.page_source)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, '[example.com] Confirmer l\'adresse email')
        self.assertEqual(mail.outbox[0].to, ['jandup@manifestationsportive.fr'])
        url = mail.outbox[0].body.split()[-1]
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/confirm-email/' + url.split("/")[5] + "/"))

        print('**** test 3 confirmation ****')

        self.assertIn('Confirmer une adresse email', self.selenium.page_source)
        self.assertIn('>jandup@manifestationsportive.fr</a> est bien une adresse email de jandup.', self.selenium.page_source)

        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        time.sleep(self.DELAY)

        self.assertIn('Adresse email confirmée', self.selenium.page_source)
        self.assertIn('Vous avez confirmé jandup@manifestationsportive.fr.', self.selenium.page_source)

        print('**** test 4 echec inscription ****')

        print('>>> email déjà utilisé')
        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/agent'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Création d\'un compte Agent', self.selenium.page_source)

        self.remplir_form('Dupond', 'Jeannot', 'jannotdup', 'jandup@manifestationsportive.fr')
        self.selenium.execute_script("window.scroll(0, 1000)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        self.assertIn('Un autre utilisateur utilise déjà cette adresse e-mail', self.selenium.page_source)

        print('>>> username déjà utilisé')
        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/agent'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Création d\'un compte Agent', self.selenium.page_source)

        self.remplir_form('Dupond', 'Jeannot', 'jandup', 'jannotdup@manifestationsportive.fr')
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        self.assertIn('Un utilisateur avec ce nom existe déjà', self.selenium.page_source)

        print('**** test 5 echec connexion ****')

        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element(By.NAME, "login")
        pseudo_input.send_keys('jandup')
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element(By.NAME, "password")
        password_input.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.assertIn('Compte inactif', self.selenium.page_source)

        print('**** test 6 connexion réussie ****')

        user = User.objects.get()
        user.is_active = True
        user.save()

        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element(By.NAME, "login")
        pseudo_input.send_keys('jandup')
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element(By.NAME, "password")
        password_input.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.assertIn('Tableau de bord des instructions', self.selenium.page_source)
        self.assertIn('Connexion avec jandup réussie.', self.selenium.page_source)
