import time
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.core import mail
from django.test import tag
from django.contrib.contenttypes.models import ContentType

from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.by import By

from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from core.models.instance import Instance
from structure.factories.structure import StructureOrganisatriceFactory

class InscriptionOrganisateur(StaticLiveServerTestCase):
    """
    Test de l'inscription d'un agent de préfecture
    """
    DELAY = 0.4

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
        """
        print()
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        print('============ Inscription organisateur (Sel) =============')
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(600, 1000)

        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__instruction_mode=Instance.IM_CIRCUIT_B)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.structure = StructureOrganisatriceFactory.create(instance_fk=cls.dep.instance)

        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """
        cls.selenium.quit()
        super().tearDownClass()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element(By.ID, id_chosen)
        chosen_select.find_element(By.CSS_SELECTOR,  "a").click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements(By.TAG_NAME, 'li')
        for result in results:
            if value in result.text:
                result.click()

    def remplir_form(self, struc_param, nom_param, prenom_param, username_param, email_param):
        """
        Remplir le formulaire avec les données transmises
        """
        self.selenium.find_element(By.ID, 'confirm_oui').click()
        time.sleep(self.DELAY)
        struc = self.selenium.find_element(By.ID, 'id_structure_name')
        struc.send_keys(struc_param)
        time.sleep(self.DELAY)

        self.chosen_select('id_type_of_structure_chosen', self.structure.type_organisation_fk.nom_s)
        time.sleep(self.DELAY*2)

        address = self.selenium.find_element(By.ID, 'id_address')
        address.send_keys('6 rue vauban')
        time.sleep(self.DELAY * 2)
        self.chosen_select('id_departement_chosen', '42')
        time.sleep(self.DELAY)

        self.chosen_select('id_commune_chosen', 'Bard')
        time.sleep(self.DELAY)

        tel = self.selenium.find_element(By.ID, 'id_phone')
        tel.send_keys('0655555555')
        time.sleep(self.DELAY)

        prenom = self.selenium.find_element(By.ID, 'id_first_name')
        prenom.send_keys(prenom_param)
        time.sleep(self.DELAY)

        nom = self.selenium.find_element(By.ID, 'id_last_name')
        nom.send_keys(nom_param)
        time.sleep(self.DELAY)

        id_user = self.selenium.find_element(By.ID, 'id_username')
        id_user.send_keys(username_param)
        time.sleep(self.DELAY)

        email = self.selenium.find_element(By.ID, 'id_email')
        email.send_keys(email_param)
        time.sleep(self.DELAY)

        pass1 = self.selenium.find_element(By.ID, 'id_password1')
        pass1.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)

        pass2 = self.selenium.find_element(By.ID, 'id_password2')
        pass2.send_keys('azeaz5646dfzdsf')

        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, "//span[contains(text(),'En cochant cette case')]").click()
        time.sleep(self.DELAY)


    @tag('selenium')
    def test_inscription_organisateur(self):
        """
        Test complet de l'inscription d'un agent de préfecture
        """

        print('**** test 1 remplissage formulaire d\'inscription organisateur ****')

        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/organisateur'))

        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Structure organisatrice', self.selenium.page_source)

        self.remplir_form('structure_test', 'Dupont', 'Jean', 'jandup', 'jandup@manifestationsportive.fr')
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        print('**** test 2 vérification ****')

        self.assertIn('Vérifiez votre adresse email', self.selenium.page_source)
        self.assertIn('E-mail de confirmation envoyé à jandup@manifestationsportive.fr', self.selenium.page_source)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, '[example.com] Confirmer l\'adresse email')
        self.assertEqual(mail.outbox[0].to, ['jandup@manifestationsportive.fr'])
        url = mail.outbox[0].body.split()[-1]
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/confirm-email/' + url.split("/")[5] + "/"))

        print('**** test 3 confirmation ****')

        self.assertIn('Confirmer une adresse email', self.selenium.page_source)
        self.assertIn('>jandup@manifestationsportive.fr</a> est bien une adresse email de jandup.', self.selenium.page_source)

        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        time.sleep(self.DELAY)

        self.assertIn('Adresse email confirmée', self.selenium.page_source)
        self.assertIn('Vous avez confirmé jandup@manifestationsportive.fr.', self.selenium.page_source)

        print('**** test 4 echec inscription ****')

        print('>>> email déjà utilisé')
        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/organisateur'))

        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Structure organisatrice', self.selenium.page_source)

        self.remplir_form('structure_test_1', 'Dupond', 'Jeannot', 'jannotdup', 'jandup@manifestationsportive.fr')
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        self.assertIn('Un autre utilisateur utilise déjà cette adresse e-mail', self.selenium.page_source)

        print('>>> username déjà utilisé')
        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/organisateur'))

        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Structure organisatrice', self.selenium.page_source)

        self.remplir_form('structure_test_2', 'Dupond', 'Jeannot', 'jandup', 'jannotdup@manifestationsportive.fr')
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        self.assertIn('Un utilisateur avec ce nom existe déjà', self.selenium.page_source)

        print('**** test 5 connexion réussie ****')

        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element(By.NAME, "login")
        pseudo_input.send_keys('jandup')
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element(By.NAME, "password")
        password_input.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.assertIn('Tableau de bord organisateur', self.selenium.page_source)
        self.assertIn('Connexion avec jandup réussie.', self.selenium.page_source)
