from django.test import TestCase
from django.contrib.contenttypes.models import ContentType

from core.models import Instance
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from sports.factories import ActiviteFactory

from structure.factories.service import ServiceConsulteFactory, TypeServiceFactory, CategorieServiceFactory, FederationFactory

class SelecteurServiceTests(TestCase):

    @classmethod
    def setUpTestData(cls):

        print()
        print('========== Selecteur Service ===========')

        # Création des objets sur le 42
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme

        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__instruction_mode=Instance.IM_CIRCUIT_D)

        dep2 = DepartementFactory.create(name='43',
                                         instance__name="instance de test 2",
                                         instance__instruction_mode=Instance.IM_CIRCUIT_D)
        arrond_1 = ArrondissementFactory.create(name='Montbrison', code='421', departement=dep)
        arrond_2 = ArrondissementFactory.create(name='Roanne', code='422', departement=dep)
        arrond_3 = ArrondissementFactory.create(name='ST Etienne', code='420', departement=dep)
        cls.dep = dep
        cls.dep2 = dep2
        cls.instance = dep.instance
        cls.prefecture1 = arrond_1.organisations.first()
        cls.prefecture2 = arrond_2.organisations.first()
        cls.prefecture3 = arrond_3.organisations.first()
        cls.commune1 = CommuneFactory(name='Bard', arrondissement=arrond_1)
        cls.autrecommune1 = CommuneFactory(name='Roche', arrondissement=arrond_1)
        cls.commune2 = CommuneFactory(name='Bully', arrondissement=arrond_2)
        cls.autrecommune2 = CommuneFactory(name='Régny', arrondissement=arrond_2)
        cls.commune3 = CommuneFactory(name='Villars', arrondissement=arrond_3)

        # Mairie
        cls.marie_commune_1 = cls.commune1.organisation_set.first()
        cls.marie_autrecommune_1 = cls.autrecommune1.organisation_set.first()
        cls.marie_commune_2 = cls.commune2.organisation_set.first()
        cls.marie_autrecommune_2 = cls.autrecommune2.organisation_set.first()
        cls.marie_commune_3 = cls.commune3.organisation_set.first()



        # Service
        cls.edsr = cls.dep.organisation_set.get(precision_nom_s="EDSR")
        cls.ggd = cls.dep.organisation_set.get(precision_nom_s="GGD")
        cls.ddsp = cls.dep.organisation_set.get(precision_nom_s="DDSP")
        cls.codis = cls.dep.organisation_set.get(precision_nom_s="CODIS")

        cls.brigade1 = ServiceConsulteFactory(departement_m2m=cls.dep, instance_fk=cls.dep.instance, precision_nom_s="",
                                              type_service_fk__abreviation_s="BTA",
                                              type_service_fk__nom_s="Brigade",
                                              type_service_fk__categorie_fk__nom_s="Gendarmerie",
                                              service_parent_fk=cls.edsr)

        # Fédération
        activ = ActiviteFactory.create()
        categorie_fede = CategorieServiceFactory(nom_s="Fédération")
        cls.type_fede = TypeServiceFactory(abreviation_s="fede", nom_s="federation", categorie_fk=categorie_fede)
        cls.fede = FederationFactory.create(discipline_fk=activ.discipline, type_service_fk=cls.type_fede,
                                            autorise_rendre_avis_b=True, porte_entree_avis_b=True,
                                            instance_fk=cls.dep.instance)



    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def test_selecteur(self):
        print("==Prefecture==")
        retour = self.client.get('/structure/ajax/servicelist/?secteur=instance_' + str(self.dep.instance.pk) +
                                 '&categorie='+str(self.prefecture1.type_service_fk.categorie_fk.pk)+'&action=selection', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"id": self.prefecture1.pk,
              "name": str(self.prefecture1),
              "data": {},
              'actif': True,
              "service": "Organisation",
              'smtp_error': False,
              'agents_count': {'agents_actif': 0, 'agents_inactif': 0, "cdsr": 0},
              },

             {"id": self.prefecture2.pk,
              "name": str(self.prefecture2),
              "data": {},
              'actif': True,
              "service": "Organisation",
              'smtp_error': False,
              'agents_count': {'agents_actif': 0, 'agents_inactif': 0, "cdsr": 0}
              },
             {"id": self.prefecture3.pk,
              "name": str(self.prefecture3),
              "data": {},
              'actif': True,
              "service": "Organisation",
              'smtp_error': False,
              'agents_count': {'agents_actif': 0, 'agents_inactif': 0, "cdsr": 0},
              }]
        )

        print("==Mairie==")
        retour = self.client.get('/structure/ajax/servicelist/?secteur=instance_' + str(self.dep.instance.pk)
                                 + '&categorie=' + str(self.marie_commune_1.type_service_fk.categorie_fk.pk)+'&action=selection',
                                 HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"id": self.marie_commune_1.pk,
              "name": f"{self.marie_commune_1.precision_nom_s} {self.marie_commune_1.commune_m2m.first().zip_code}",
              "data": {},
              'actif': True,
              "service": "Organisation",
              'smtp_error': False,
              'agents_count': {'agents_actif': 0, 'agents_inactif': 0, "cdsr": 0},
              },
             {"id": self.marie_autrecommune_1.pk,
              "name": f"{self.marie_autrecommune_1.precision_nom_s} {self.marie_autrecommune_1.commune_m2m.first().zip_code}",
              "data": {},
              'actif': True,
              "service": "Organisation",
              'smtp_error': False,
              'agents_count': {'agents_actif': 0, 'agents_inactif': 0, "cdsr": 0},
              },
             {"id": self.marie_commune_2.pk,
              "name": f"{self.marie_commune_2.precision_nom_s} {self.marie_commune_2.commune_m2m.first().zip_code}",
              "data": {},
              'actif': True,
              "service": "Organisation",
              'smtp_error': False,
              'agents_count': {'agents_actif': 0, 'agents_inactif': 0, "cdsr": 0}},
             {"id": self.marie_autrecommune_2.pk,
              "name": f"{self.marie_autrecommune_2.precision_nom_s} {self.marie_autrecommune_2.commune_m2m.first().zip_code}",
              "data": {},
              'actif': True,
              "service": "Organisation",
              'smtp_error': False,
              'agents_count': {'agents_actif': 0, 'agents_inactif': 0, "cdsr": 0}},
             {"id": self.marie_commune_3.pk,
              "name": f"{self.marie_commune_3.precision_nom_s} {self.marie_commune_3.commune_m2m.first().zip_code}",
              "data": {},
              'actif': True,
              "service": "Organisation",
              'smtp_error': False,
              'agents_count': {'agents_actif': 0, 'agents_inactif': 0, "cdsr": 0},
              }]
        )

        print("==Gendarmerie==")
        retour = self.client.get('/structure/ajax/servicelist/?secteur=instance_' + str(self.dep.instance.pk)
                                 + '&categorie=' + str(self.edsr.type_service_fk.categorie_fk.pk)+'&action=selection',
                                 HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"id": self.edsr.pk,
              "name": self.edsr.__str__(),
              "data": {},
              'actif': True,
              "service": "Organisation",
              'smtp_error': False,
              'agents_count': {'agents_actif': 0, 'agents_inactif': 0, "cdsr": 0}
              }
             ]
        )

        print("show sous service")
        retour = self.client.get('/structure/ajax/servicelist/?secteur=instance_' + str(self.dep.instance.pk)
                                 + '&show_sous_service=true&categorie=' + str(self.edsr.type_service_fk.categorie_fk.pk)+'&action=selection',
                                 HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"id": self.edsr.pk,
              "name": self.edsr.__str__(),
              'actif': True,
              "data": [[self.brigade1.pk, [self.brigade1.__str__(), "Organisation", self.brigade1.pk, False]],
                       [self.ggd.pk, [self.ggd.__str__(), "Organisation", self.ggd.pk, False]]],
              "service": "Organisation",
              'smtp_error': False,
              'agents_count': {'agents_actif': 0, 'agents_inactif': 0, "cdsr": 0}},

             ]
        )

        print("==Police==")
        retour = self.client.get('/structure/ajax/servicelist/?secteur=instance_' + str(self.dep.instance.pk)
                                 + '&categorie=' + str(self.ddsp.type_service_fk.categorie_fk.pk)+'&action=selection',
                                 HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"id": self.ddsp.pk,
              "name": self.ddsp.__str__(),
              "data": {},
              'actif': True,
              "service": "Organisation",
              'smtp_error': False,
              'agents_count': {'agents_actif': 0, 'agents_inactif': 0, "cdsr": 0}
              },

             ]
        )

        print("==Fédération==")
        retour = self.client.get('/structure/ajax/servicelist/?secteur=instance_' + str(self.dep.instance.pk)
                                 + '&categorie=' + str(self.fede.type_service_fk.categorie_fk.pk)+'&action=selection',
                                 HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"id": self.fede.pk,
              "name": self.fede.__str__(),
              "data": {},
              'actif': True,
              "service": "Organisation",
              'smtp_error': False,
              'agents_count': {'agents_actif': 0, 'agents_inactif': 0, "cdsr": 0}
              }]
        )

