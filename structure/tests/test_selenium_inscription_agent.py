from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.by import By
from django.core import mail
from django.test import tag
import time

from core.models.instance import Instance
from core.models import User
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from sports.factories import ActiviteFactory

from structure.factories.service import TypeServiceFactory, CategorieServiceFactory, FederationFactory, ServiceConsulteFactory


class InscriptionInstructeur(StaticLiveServerTestCase):
    """
    Test l'inscription d'agent a des services et sous services
    """
    DELAY = 0.4

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
        """
        print()
        print('============ Inscription agent service et sous service (Sel) =============')
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(600, 800)
        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__instruction_mode=Instance.IM_CIRCUIT_B)
        cls.arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        cls.commune = CommuneFactory(name='Bard', arrondissement=cls.arrondissement)

        cls.prefecture = cls.arrondissement.organisations.first()
        cls.cg = cls.dep.organisation_set.get(precision_nom_s="CD")
        cls.ddsp = cls.dep.organisation_set.get(precision_nom_s="DDSP")
        cls.edsr = cls.dep.organisation_set.get(precision_nom_s="EDSR")
        cls.codis = cls.dep.organisation_set.get(precision_nom_s="CODIS")

        cls.brigade1 = ServiceConsulteFactory(departement_m2m=cls.dep, instance_fk=cls.dep.instance, precision_nom_s="",
                               type_service_fk__abreviation_s="BTA",
                               type_service_fk__nom_s="Brigade",
                               type_service_fk__categorie_fk__nom_s="Gendarmerie")
        cls.cgd = ServiceConsulteFactory(departement_m2m=cls.dep, instance_fk=cls.dep.instance,
                                              precision_nom_s="",
                                              type_service_fk__abreviation_s="CGD",
                                              type_service_fk__nom_s="Compagnie de gendarmerie départementale",
                                              type_service_fk__categorie_fk__nom_s="Gendarmerie")



        categorie_fede = CategorieServiceFactory(nom_s="Fédération")
        cls.type_fede = TypeServiceFactory(abreviation_s="fede", nom_s="federation", categorie_fk=categorie_fede)
        activ = ActiviteFactory.create()
        cls.fede = FederationFactory.create(discipline_fk=activ.discipline, type_service_fk=cls.type_fede,
                                             autorise_rendre_avis_b=True, porte_entree_avis_b=True, instance_fk=cls.dep.instance)


        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """
        cls.selenium.quit()
        super().tearDownClass()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element(By.ID, id_chosen)
        chosen_select.find_element(By.CSS_SELECTOR,  "a").click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements(By.TAG_NAME, 'li')
        for result in results:
            if value in result.text:
                result.click()

    def remplir_form(self, nom_param, prenom_param, username_param, email_param, categorie, element, id, sous_service=None):
        """
        Remplir le formulaire avec les données transmises
        """
        prenom = self.selenium.find_element(By.ID, 'id_first_name')
        prenom.send_keys(prenom_param)
        time.sleep(self.DELAY)

        nom = self.selenium.find_element(By.ID, 'id_last_name')
        nom.send_keys(nom_param)
        time.sleep(self.DELAY)

        id_user = self.selenium.find_element(By.ID, 'id_username')
        id_user.send_keys(username_param)
        time.sleep(self.DELAY)

        email = self.selenium.find_element(By.ID, 'id_email')
        email.send_keys(email_param)
        time.sleep(self.DELAY)

        pass1 = self.selenium.find_element(By.ID, 'id_password1')
        pass1.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)

        pass2 = self.selenium.find_element(By.ID, 'id_password2')
        pass2.send_keys('azeaz5646dfzdsf')
        self.selenium.execute_script("window.scroll(0, 1200)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, 'btn-modal-selecteur').click()
        time.sleep(self.DELAY * 2)

        self.chosen_select('select_instances_modal_chosen', 'instance de test (42)')
        time.sleep(self.DELAY * 2)

        self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % categorie).click()
        time.sleep(self.DELAY * 2)
        if sous_service:
            self.selenium.find_elements(By.CLASS_NAME, "name_service")[element].click()
        else:
            self.selenium.find_elements(By.CLASS_NAME, "name_service")[element].click()
        self.selenium.find_element(By.ID, id).click()
        time.sleep(self.DELAY)


    @tag('selenium')
    def test_inscription_agent(self):
        """
        Test l'inscription d'agent à des services et sous services
        """

        print('**** test 1 remplissage formulaire d\'inscription agent EDSR ****')

        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/agent'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Création d\'un compte Agent', self.selenium.page_source)

        self.remplir_form('Dupont', 'Jean', 'jandup', 'jandup@manifestationsportive.fr', 'Gendarmerie', 2, self.edsr.pk, False)
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        print('**** test 2 vérification ****')

        self.assertIn('Vérifiez votre adresse email', self.selenium.page_source)
        self.assertIn('E-mail de confirmation envoyé à jandup@manifestationsportive.fr', self.selenium.page_source)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, '[example.com] Confirmer l\'adresse email')
        self.assertEqual(mail.outbox[0].to, ['jandup@manifestationsportive.fr'])
        url = mail.outbox[0].body.split()[-1]
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/confirm-email/' + url.split("/")[5] + "/"))
        last_user = User.objects.latest('id')
        self.assertEqual(last_user.get_first_service().__str__(), str(self.edsr))

        print('**** test 3 remplissage formulaire d\'inscription agent CODIS ****')

        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/agent'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Création d\'un compte Agent', self.selenium.page_source)

        self.remplir_form('Dupont', 'Jean', 'jandupcodis', 'jandupcodis@manifestationsportive.fr', 'Pompier', 1, self.codis.pk, True)
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        print('**** test 4 vérification ****')

        self.assertIn('Vérifiez votre adresse email', self.selenium.page_source)
        self.assertIn('E-mail de confirmation envoyé à jandupcodis@manifestationsportive.fr', self.selenium.page_source)

        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[1].subject, '[example.com] Confirmer l\'adresse email')
        self.assertEqual(mail.outbox[1].to, ['jandupcodis@manifestationsportive.fr'])
        url = mail.outbox[0].body.split()[-1]
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/confirm-email/' + url.split("/")[5] + "/"))
        last_user = User.objects.latest('id')
        self.assertEqual(last_user.get_first_service().__str__(), str(self.codis))

        print('**** test 5 remplissage formulaire d\'inscription agent CG n+1****')

        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/agent'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Création d\'un compte Agent', self.selenium.page_source)

        self.remplir_form('Dupont', 'Jean', 'jandupcd', 'jandupcd@manifestationsportive.fr', 'Conseil départemental', 0, self.cg.pk, False)
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY * 5)
        btn_submit = self.selenium.find_element(By.XPATH, '//input[@type="submit"]')
        time.sleep(self.DELAY * 5)
        btn_submit.click()
        time.sleep(self.DELAY)

        print('**** test 6 vérification ****')

        self.assertIn('Vérifiez votre adresse email', self.selenium.page_source)
        self.assertIn('E-mail de confirmation envoyé à jandupcd@manifestationsportive.fr', self.selenium.page_source)

        self.assertEqual(len(mail.outbox), 3)
        self.assertEqual(mail.outbox[2].subject, '[example.com] Confirmer l\'adresse email')
        self.assertEqual(mail.outbox[2].to, ['jandupcd@manifestationsportive.fr'])
        url = mail.outbox[0].body.split()[-1]
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/confirm-email/' + url.split("/")[5] + "/"))
        last_user = User.objects.latest('id')

        self.assertEqual(last_user.get_first_service().__str__(), str(self.cg))
        print('**** test 7 remplissage formulaire d\'inscription agent brigade ****')

        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/agent'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Création d\'un compte Agent', self.selenium.page_source)
        self.remplir_form('Dupont', 'Jean', 'jandupbta', 'jandupbta@manifestationsportive.fr', 'Gendarmerie', 0, self.brigade1.pk, False)

        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        print('**** test 8 vérification ****')

        self.assertIn('Vérifiez votre adresse email', self.selenium.page_source)
        self.assertIn('E-mail de confirmation envoyé à jandupbta@manifestationsportive.fr', self.selenium.page_source)

        self.assertEqual(len(mail.outbox), 4)
        self.assertEqual(mail.outbox[3].subject, '[example.com] Confirmer l\'adresse email')
        self.assertEqual(mail.outbox[3].to, ['jandupbta@manifestationsportive.fr'])
        url = mail.outbox[0].body.split()[-1]
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/confirm-email/' + url.split("/")[5] + "/"))
        last_user = User.objects.latest('id')
        self.assertEqual(last_user.get_first_service().__str__(), str(self.brigade1))

        print('**** test 9 remplissage formulaire d\'inscription agent Fédération départemental ****')

        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/agent'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Création d\'un compte Agent', self.selenium.page_source)

        self.remplir_form('Dupont', 'Jean', 'jandupFedeNat', 'jandupFedeNat@manifestationsportive.fr', 'Fédération', 0, self.fede.pk, False)

        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        print('**** test 10 vérification ****')

        self.assertIn('Vérifiez votre adresse email', self.selenium.page_source)
        self.assertIn('E-mail de confirmation envoyé à jandupFedeNat@manifestationsportive.fr',
                      self.selenium.page_source)
        self.assertEqual(len(mail.outbox), 5)
        self.assertEqual(mail.outbox[4].subject, '[example.com] Confirmer l\'adresse email')
        self.assertEqual(mail.outbox[4].to, ['jandupFedeNat@manifestationsportive.fr'])
        url = mail.outbox[0].body.split()[-1]
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/confirm-email/' + url.split("/")[5] + "/"))
        last_user = User.objects.latest('id')
        self.assertEqual(last_user.get_first_service(), self.fede)






