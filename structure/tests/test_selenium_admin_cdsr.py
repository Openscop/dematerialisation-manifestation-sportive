import re, time

from django.test import tag, override_settings
from django.contrib.auth.hashers import make_password as make

from selenium.webdriver.common.by import By

from structure.factories.service import ServiceConsulteFactory
from instructions.tests.test_base_selenium import SeleniumCommunClass
from core.factories import GroupFactory, UserFactory


class CDSR_SelTests(SeleniumCommunClass):
    """
    Test de l'administration du service CDSR (voir test 1 client)
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ CDSR (Sel) =============')
        # ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        SeleniumCommunClass.init_setup(cls)
        cls.sncf = ServiceConsulteFactory(departement_m2m=cls.dep, instance_fk=cls.dep.instance, precision_nom_s="SNCF",
                                          type_service_fk__abreviation_s="SNCF", type_service_fk__nom_s="Transport",
                                          type_service_fk__categorie_fk__nom_s="Autre service",
                                          admin_service_famille_b=True, admin_utilisateurs_subalternes_b=True,
                                          porte_entree_avis_b=True, autorise_rendre_avis_b=True,
                                          autorise_acces_consult_organisateur_b=True)
        cls.agent_sncf = UserFactory.create(username='agent_sncf', password=make("123"), default_instance=cls.dep.instance, email='scnf@test.fr')
        cls.agent_sncf.organisation_m2m.add(cls.sncf)
        cls.agent_sdis2 = UserFactory.create(username='agent_sdis2', password=make("123"), default_instance=cls.dep.instance, email='sdis2@test.fr')
        cls.agent_sdis2.organisation_m2m.add(cls.sdis)
        grp = GroupFactory.create(name='Administrateurs d\'instance')
        cls.agentlocal_edsr.delete()
        cls.instructeur.groups.add(grp)
        super().setUpClass()

    @tag('selenium')
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/', DEBUG=True)
    def test_CDSR_admin_Sel(self):
        """
        Test CDSR administration
        """

        print('**** test 1 administration du service CDSR ****')
        self.connexion('instructeur')
        self.selenium.get('%s%s' % (self.live_server_url, '/structure/administration_des_services/'))
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Sécurité")]').click()
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'CDSR').click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Membres').click()
        self.assertIn('Aucun service membre rattaché à ce service', self.selenium.page_source)
        self.assertIn('Ajouter un membre permanent', self.selenium.page_source)
        self.selenium.find_element(By.ID, 'btn-ajouter-service').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Gestion des services').click()
        time.sleep(3)
        self.selenium.switch_to.window(window_name=self.selenium.window_handles[1])
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Police")]').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'DDSP').click()
        self.selenium.find_element(By.ID, str(self.ddsp.pk)).click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Pompier")]').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'SDIS').click()
        self.selenium.find_element(By.ID, str(self.sdis.pk)).click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Gendarmerie")]').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'EDSR').click()
        self.selenium.find_element(By.ID, str(self.edsr.pk)).click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Conseil")]').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'CD').click()
        self.selenium.find_element(By.ID, str(self.cg.pk)).click()
        time.sleep(self.DELAY * 5)
        self.selenium.close()
        time.sleep(self.DELAY * 5)
        self.selenium.switch_to.window(window_name=self.selenium.window_handles[0])
        self.selenium.refresh()
        time.sleep(self.DELAY * 3)
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Membres').click()
        time.sleep(self.DELAY * 2)
        croix = self.selenium.find_elements(By.CLASS_NAME, 'delete-user-organisation')
        self.assertEqual(4, len(croix))
        croix[0].click()
        self.selenium.find_element(By.ID, str(self.cg.pk)).click()
        time.sleep(self.DELAY * 2)
        croix = self.selenium.find_elements(By.CLASS_NAME, 'delete-user-organisation')
        self.assertEqual(3, len(croix))
        self.deconnexion()
