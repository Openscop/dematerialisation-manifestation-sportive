import re, time

from django.test import tag, override_settings
from django.contrib.auth.hashers import make_password as make
from django.utils import timezone
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from allauth.account.models import EmailAddress

from structure.factories.service import ServiceConsulteFactory
from instructions.tests.test_base_selenium import SeleniumCommunClass
from core.factories import UserFactory


class CDSR_SelTests(SeleniumCommunClass):
    """
    Test de la fonctionnalité CDSR (voir test client)
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ CDSR (Sel) =============')
        # ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        SeleniumCommunClass.init_setup(cls)
        cls.sncf = ServiceConsulteFactory(departement_m2m=cls.dep, instance_fk=cls.dep.instance, precision_nom_s="SNCF",
                                          type_service_fk__abreviation_s="SNCF", type_service_fk__nom_s="Transport",
                                          type_service_fk__categorie_fk__nom_s="Autre service",
                                          admin_service_famille_b=True, admin_utilisateurs_subalternes_b=True,
                                          porte_entree_avis_b=True, autorise_rendre_avis_b=True,
                                          autorise_acces_consult_organisateur_b=True)
        cls.agent_sncf = UserFactory.create(username='agent_sncf', password=make("123"), default_instance=cls.dep.instance, email='scnf@test.fr')
        cls.agent_sncf.organisation_m2m.add(cls.sncf)
        EmailAddress.objects.create(user=cls.agent_sncf, email=f'{cls.agent_sncf}{cls.agent_sncf.pk}@example.com',
                                    primary=True, verified=True)
        cls.agent_sdis2 = UserFactory.create(username='agent_sdis2', password=make("123"), default_instance=cls.dep.instance, email='sdis2@test.fr')
        cls.agent_sdis2.organisation_m2m.add(cls.sdis)
        cls.cdsr.membres_permanents_m2m.add(cls.ddsp)
        cls.cdsr.membres_permanents_m2m.add(cls.edsr)
        cls.cdsr.membres_permanents_m2m.add(cls.sdis)
        cls.agentlocal_edsr.delete()
        super().setUpClass()

    @tag('selenium')
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/', DEBUG=True)
    def test_CDSR_Sel(self):
        """
        Test CDSR
        """

        print('**** test 0 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 1 présentation CDSR ****')
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        self.assertNotIn("Réunions CDSR", self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        time.sleep(self.DELAY * 2)
        # Créer la présentation
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Présenter en CDSR').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('CDSR (42)', self.selenium.page_source)
        # Annuler la présentation
        # Avis CDSR
        self.selenium.find_element(By.ID, f"card_{self.cdsr.pk}").click()
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Annuler la présentation').click()
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Confirmer l\'annulation').click()
        time.sleep(self.DELAY * 2)
        self.assertNotIn('CDSR (42)', self.selenium.page_source)
        # Recréer la présentation
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Présenter en CDSR').click()
        time.sleep(self.DELAY * 2)
        # Avis CDSR
        self.selenium.find_element(By.ID, f"card_{self.cdsr.pk}").click()
        # Ajouter des membres invités
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 300)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.CLASS_NAME, 'btn-modal-presentation-cdsr').click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Autre service")]').click()
        # self.selenium.find_elements(By.CLASS_NAME, 'categorie')[9].click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'SNCF').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, str(self.sncf.pk)).click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Conseil")]').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'CD').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, str(self.cg.pk)).click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, "btn-close-modal-avis").click()
        time.sleep(self.DELAY * 5)
        # Retirer un membre invité
        self.selenium.find_element(By.ID, f"card_{self.cdsr.pk}").click()
        croix = self.selenium.find_elements(By.CLASS_NAME, 'delete-member-cdsr')
        self.assertEqual(2, len(croix))
        croix[1].click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, str(self.cg.pk)).click()
        time.sleep(self.DELAY * 2)
        croix = self.selenium.find_elements(By.CLASS_NAME, 'delete-member-cdsr')
        self.assertEqual(1, len(croix))
        print('**** test 2 réunion CDSR ****')
        # Créer une réunion cdsr
        self.selenium.find_element(By.CLASS_NAME, "btn-date-form").click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, 'id_nom_s').send_keys('prochaine réunion')
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY * 6)
        self.selenium.find_element(By.ID, f"card_{self.cdsr.pk}").click()
        time.sleep(self.DELAY * 2)
        self.assertIn('DDSP (42)', self.selenium.page_source)
        self.assertIn('EDSR (42)', self.selenium.page_source)
        self.assertIn('SDIS (42)', self.selenium.page_source)
        # Supprimer le lien avec la réunion CDSR
        self.selenium.find_element(By.CLASS_NAME, "supprimer").click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, f"card_{self.cdsr.pk}").click()
        time.sleep(self.DELAY * 2)
        self.assertIn('DDSP (42)', self.selenium.page_source)
        self.assertIn('EDSR (42)', self.selenium.page_source)
        self.assertIn('SDIS (42)', self.selenium.page_source)
        # Choisir une réunion pour une présentation
        self.chosen_select('reunion_chosen', 'prochaine réunion')
        time.sleep(self.DELAY * 2)
        self.assertIn("Fédération", self.selenium.page_source)
        self.selenium.find_element(By.CLASS_NAME, "cdsr").click()
        time.sleep(self.DELAY * 2)
        self.assertFalse(self.selenium.find_element(By.ID, f"card_{self.fede.pk}").is_displayed())
        self.selenium.find_element(By.ID, f"card_{self.cdsr.pk}").click()
        time.sleep(self.DELAY * 2)
        self.assertNotIn("Envoyer la convocation des membres invités", self.selenium.page_source)
        self.assertNotIn("Envoyer la convocation des membres permanents", self.selenium.page_source)
        print('**** test 3 gestion réunion CDSR ****')
        # Gestion des réunions CDSR
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Tableau de bord').click()
        time.sleep(self.DELAY*2)
        self.assertIn("Réunions CDSR", self.selenium.page_source)
        bloc = self.selenium.find_element(By.XPATH, "//a[starts-with(@id, 'btn_reunion')]")
        self.assertIn('prochaine réunion', bloc.text)
        self.assertEqual('1', bloc.find_element(By.TAG_NAME, 'span').text)
        bloc.click()
        time.sleep(self.DELAY * 5)
        self.selenium.switch_to.window(window_name=self.selenium.window_handles[1])
        time.sleep(self.DELAY * 5)
        self.assertIn("Liste des réunions CDSR", self.selenium.page_source)
        self.assertIn("prochaine réunion", self.selenium.page_source)
        self.assertIn("Manifestation_Test", self.selenium.page_source)
        self.assertIn("Ajouter une réunion", self.selenium.page_source)
        self.assertIn("Editer la réunion", self.selenium.page_source)
        self.assertTrue(self.selenium.find_element(By.CLASS_NAME, "fa-construction").is_displayed())
        # Créer une autre réunion
        self.selenium.find_element(By.XPATH, '//a[contains(@class, "btn-date-form")]').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, 'id_nom_s').send_keys('réunion suivante')
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY * 6)
        self.assertIn("réunion suivante", self.selenium.page_source)
        self.assertIn("Pas de dossier à présenter pour cette réunion", self.selenium.page_source)
        self.assertIn("Supprimer la réunion", self.selenium.page_source)
        # Supprimer la réunion
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Supprimer la réunion').click()
        time.sleep(self.DELAY * 3)
        self.assertNotIn("réunion suivante", self.selenium.page_source)
        # Editer une réunion pour fixer une date
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Editer la réunion').click()
        time.sleep(self.DELAY * 3)
        element = self.selenium.find_element(By.CLASS_NAME, 'datetime_picker_ar')
        element.click()
        element.send_keys(Keys.CONTROL, "a")  # Sélectionner tout son contenu
        element.send_keys(Keys.BACKSPACE)  # Supprimer tout
        date = (timezone.now() + timezone.timedelta(days=30)).strftime('%d/%m/%y')
        element.send_keys(date)  # on met la date dans le datetimepicker
        element.send_keys(Keys.ESCAPE)  # on réduit le calendrier
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.assertNotIn("fa-construction", self.selenium.page_source)
        self.assertIn("fa-pen", self.selenium.page_source)
        self.selenium.find_element(By.XPATH, "//div[starts-with(@id, 'heading')]").click()
        time.sleep(self.DELAY * 2)
        self.assertNotIn("Envoyer la convocation des membres invités", self.selenium.page_source)
        self.assertNotIn("Envoyer la convocation des membres permanents", self.selenium.page_source)
        # Définir une heure de présentation
        self.selenium.find_element(By.XPATH, '//span[contains(@class, "btn-date-form")]').click()
        time.sleep(self.DELAY * 2)
        element = self.selenium.find_element(By.CLASS_NAME, 'datetime_picker_ar')
        element.click()
        element.send_keys(Keys.CONTROL, "a")  # Sélectionner tout son contenu
        element.send_keys(Keys.BACKSPACE)  # Supprimer tout
        element.send_keys("10:00")  # on met l'heure dans le datetimepicker
        element.send_keys(Keys.ESCAPE)  # on réduit le calendrier
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.selenium.find_element(By.XPATH, "//div[starts-with(@id, 'heading')]").click()
        time.sleep(self.DELAY * 2)
        self.assertIn("Envoyer la convocation des membres invités", self.selenium.page_source)
        self.assertIn("Envoyer la convocation des membres permanents", self.selenium.page_source)
        print('**** test 4 convocation des membres permanents ****')
        # Envoyer les convocations aux membres permanents
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Envoyer la convocation des membres permanents').click()
        time.sleep(self.DELAY * 2)
        precision = self.selenium.find_element(By.ID, 'id_precision')
        precision.send_keys('Précision sur la présentation CDSR')
        time.sleep(self.DELAY * 2)
        fileinput = self.selenium.find_element(By.XPATH, '//input[@type="file"]')
        fileinput.send_keys('/tmp/pj_1.txt')
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.selenium.find_element(By.XPATH, "//div[starts-with(@id, 'heading')]").click()
        time.sleep(self.DELAY * 2)
        self.assertIn("Envoyer la convocation des membres invités", self.selenium.page_source)
        self.assertNotIn("Envoyer la convocation des membres permanents", self.selenium.page_source)
        self.assertNotIn("Editer la réunion", self.selenium.page_source)
        self.assertIn("La convocation des membres permanents a été envoyée", self.selenium.page_source)
        self.assertIn("Ordre du jour", self.selenium.page_source)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Ordre du jour').click()
        time.sleep(self.DELAY * 5)
        self.selenium.switch_to.window(window_name=self.selenium.window_handles[2])
        self.assertIn("Document Manifestation SportiveTest", self.selenium.page_source)
        self.selenium.close()
        time.sleep(self.DELAY * 2)
        self.selenium.switch_to.window(window_name=self.selenium.window_handles[1])
        self.selenium.close()
        time.sleep(self.DELAY * 2)
        self.selenium.switch_to.window(window_name=self.selenium.window_handles[0])
        self.deconnexion()
        print('**** test 5 confirmation de présence à la réunion ****')
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        time.sleep(self.DELAY * 3)
        self.assertIn("Veuillez confirmer votre présence à cette réunion", self.selenium.page_source)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Confirmer votre présence').click()
        time.sleep(self.DELAY * 3)
        self.assertIn("Vous avez confirmer votre présence à la réunion CDSR", self.selenium.page_source)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Rédiger l\'avis').click()
        contenu = self.selenium.find_element(By.ID, 'id_prescriptions')
        contenu.send_keys('Précision sur l\'avis DDSP pour la  présentation CDSR')
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Enregistrer').click()
        time.sleep(self.DELAY * 7)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Valider et rendre l\'avis').click()
        time.sleep(self.DELAY * 6)
        self.selenium.find_element(By.CLASS_NAME, 'test-btn-valider').click()
        time.sleep(self.DELAY * 2)
        self.deconnexion()
        self.connexion('agent_sncf')
        self.presence_avis('agent_sncf', 'none')
        self.deconnexion()
        print('**** test 6 convocation des membres invités ****')
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Avis CDSR
        self.selenium.find_element(By.ID, f"card_{self.cdsr.pk}").click()
        self.assertIn("Envoyer la convocation des membres invités", self.selenium.page_source)
        self.assertNotIn("Envoyer la convocation des membres permanents", self.selenium.page_source)
        self.assertNotIn("Rédiger l'avis synthétique", self.selenium.page_source)
        self.assertNotIn("Ajouter une pièce jointe", self.selenium.page_source)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Envoyer la convocation des membres invités').click()
        time.sleep(self.DELAY * 2)
        precision = self.selenium.find_element(By.ID, 'id_precision')
        precision.send_keys('Précision sur la présentation CDSR')
        time.sleep(self.DELAY * 2)
        fileinput = self.selenium.find_element(By.XPATH, '//input[@type="file"]')
        fileinput.send_keys('/tmp/pj_1.txt')
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.assertIn("La convocation des membres invités a été envoyée", self.selenium.page_source)
        self.assertIn("Rédiger l'avis synthétique", self.selenium.page_source)
        self.assertIn("Ajouter une pièce jointe", self.selenium.page_source)
        self.deconnexion()
        print('**** test 7 confirmation de présence à la réunion ****')
        self.connexion('agent_sncf')
        self.presence_avis('agent_sncf', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        time.sleep(self.DELAY * 3)
        self.assertIn("Veuillez confirmer votre présence à cette réunion", self.selenium.page_source)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Confirmer votre présence').click()
        time.sleep(self.DELAY * 3)
        self.assertIn("Vous avez confirmer votre présence à la réunion CDSR", self.selenium.page_source)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Rédiger l\'avis').click()
        contenu = self.selenium.find_element(By.ID, 'id_prescriptions')
        contenu.send_keys('Précision sur l\'avis SNCF pour la  présentation CDSR')
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Enregistrer').click()
        time.sleep(self.DELAY * 7)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Valider et rendre l\'avis').click()
        time.sleep(self.DELAY * 6)
        self.selenium.find_element(By.CLASS_NAME, 'test-btn-valider').click()
        time.sleep(self.DELAY * 2)
        self.deconnexion()
        print('**** test 8 avis CDSR ****')
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Avis CDSR
        self.selenium.find_element(By.ID, f"card_{self.cdsr.pk}").click()
        self.assertIn("Rédiger l'avis synthétique", self.selenium.page_source)
        self.assertIn("Ajouter une pièce jointe", self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 300)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Rédiger l\'avis synthétique').click()
        time.sleep(self.DELAY * 5)
        self.assertIn("Pré-remplir", self.selenium.page_source)
        self.selenium.find_element(By.ID, 'getavis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Enregistrer').click()
        time.sleep(self.DELAY * 10)
        avis_cdsr = self.selenium.find_element(By.ID, f"card_{self.cdsr.pk}")
        avis_cdsr.click()
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY * 10)
        self.assertIn("Précision sur l'avis DDSP", avis_cdsr.text)
        self.assertNotIn("Précision sur l'avis SNCF", avis_cdsr.text)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Valider et rendre l\'avis').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, f"card_{self.cdsr.pk}").click()
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY * 2)
        self.assertIn("Redemander l'avis", self.selenium.page_source)
        self.deconnexion()
