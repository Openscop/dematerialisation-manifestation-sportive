import time

from django.test import tag, override_settings
from django.contrib.auth.hashers import make_password as make
from django.conf import settings

from allauth.account.models import EmailAddress
from selenium.webdriver.common.by import By

from instructions.tests.test_base_selenium import SeleniumCommunClass
from core.models import Instance
from core.factories import UserFactory, GroupFactory
from administrative_division.factories import CommuneFactory, DepartementFactory, ArrondissementFactory
from structure.factories.service import PrefectureFactory
from structure.models.service import ServiceConsulte


class AdministrationStructureTests(SeleniumCommunClass):
    """
    Test des fonctions de l'administration des structures
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print('============ Administration des structures (Sel) =============')
        SeleniumCommunClass.init_setup(cls)
        cls.avis_nb = 2
        super().setUpClass()
        cls.dep43 = DepartementFactory.create(name='43',
                                              instance__name="instance de test",
                                              instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                              instance__etat_s="ouvert")
        arrondissement43 = ArrondissementFactory.create(name='Yssingeaux', code='433', departement=cls.dep43)
        cls.prefecture43 = PrefectureFactory(arrondissement_m2m=arrondissement43,
                                            instance_fk=cls.dep43.instance)
        cls.commune43 = CommuneFactory(name='Yssingeaux', arrondissement=arrondissement43)

        cls.instructeur43 = UserFactory.create(username='instructeur43', password=make("123"),
                                               default_instance=cls.dep43.instance, email='inst43@test.fr')
        EmailAddress.objects.create(user=cls.instructeur43, email='inst43@test.fr', primary=True,
                                    verified=True)

        cls.instructeur43.optionuser.notification_mail = True
        cls.instructeur43.optionuser.action_mail = True
        cls.instructeur43.optionuser.save()
        cls.instructeur43.organisation_m2m.add(cls.prefecture43)

        # Admin instance
        cls.admin_instance = UserFactory.create(username='admin_instance', password=make("123"),
                                                 default_instance=cls.dep.instance)
        group_admin_instance = GroupFactory.create(name='Administrateurs d\'instance')
        cls.admin_instance.groups.add(group_admin_instance)
        EmailAddress.objects.create(user=cls.admin_instance, email='admin_instance@example.com', primary=True,
                                    verified=True)
        cls.admin_instance.organisation_m2m.add(cls.prefecture)

        # Utilisateur Support
        cls.user_support = UserFactory.create(username='user_support', password=make("123"),
                                                default_instance=cls.dep.instance)
        EmailAddress.objects.create(user=cls.user_support, email='user_support3@test.fr', primary=True,
                                    verified=True)
        o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
        cls.user_support.organisation_m2m.add(o_support)
        cls.user_support.save()


    @tag('selenium')
    @override_settings(DEBUG=True)
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
    def test_AdminService(self):
        assert settings.DEBUG
        print('**** test 1 administrateur d\'instance ****')
        self.connexion("admin_instance")
        time.sleep(self.DELAY)
        self.selenium.get('%s%s' % (self.live_server_url, '/structure/administration_des_services/'))
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Police")]').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, 'open_create_service_form_modal').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, 'div_id_is_service_regional').click()
        time.sleep(self.DELAY * 2)
        self.chosen_select('id_type_service_fk_chosen', str(self.ddsp.type_service_fk))
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, 'submit_create_service_form').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('Service crée avec succès', self.selenium.page_source)
        self.selenium.find_element(By.CLASS_NAME, 'show_sous_service').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'abr').click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, 'habilitations-avancees-link').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, 'div_id_autorise_mandater_avis_famille_b').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'btn-form').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('Action du service édités avec succès', self.selenium.page_source)
        self.assertIn('DDSP DDSP (42)', self.selenium.page_source)
        self.assertIn('abr Commissariat Bard ', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.CLASS_NAME, 'add-interaction').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, "//select[@id='id_service_entrant_fk']/option[3]").click()
        self.selenium.find_element(By.XPATH, "//select[@id='id_type_lien_s']/option[3]").click()
        self.selenium.find_element(By.XPATH, "//select[@id='id_service_sortant_fk']/option[2]").click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, 'submit-inter').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('Interaction crée avec succès', self.selenium.page_source)
        self.assertIn('Les avis envoyés au service abr', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 5)
        self.selenium.find_elements(By.CLASS_NAME, "delete-interaction")[1].click()
        time.sleep(self.DELAY * 5)
        self.assertIn('Interaction supprimée avec succès', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Informations').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'edit_service_active').click()
        self.assertIn('La structure ne pourra être inactivée', self.selenium.page_source)
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, 'close-modal-interaction').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.CLASS_NAME, 'gerer-secteur-service').click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.ID, 'div_id_is_service_departemental').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'btn-form').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('Secteurs du service éditées avec succès', self.selenium.page_source)
        self.assertIn('42 - Loire', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.CLASS_NAME, 'historique').click()
        time.sleep(self.DELAY * 2)
        self.assertEqual(3, len(self.selenium.find_elements(By.CLASS_NAME, 'ligne-historique')))
        self.selenium.find_element(By.ID, 'close-modal-historique').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Agents').click()
        self.assertIn('Ajouter une invitation', self.selenium.page_source)
        self.selenium.find_element(By.CLASS_NAME, 'organisation_user').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'edit_user_email').click()
        self.selenium.find_element(By.ID, 'id_email').clear()
        self.selenium.find_element(By.ID, 'id_email').send_keys('comm2@test.fr')
        self.selenium.find_element(By.CLASS_NAME, 'btn-form').click()
        email = EmailAddress.objects.last()
        email.verified = True
        email.save()
        time.sleep(self.DELAY * 2)
        self.assertIn('Addresse e-amil de l\'utilisateur édité avec succès', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.CLASS_NAME, 'delete-user-organisation').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, '21').click()
        time.sleep(self.DELAY * 5)
        self.selenium.switch_to.alert.accept()
        time.sleep(self.DELAY * 2)
        self.assertIn('Inactif', self.selenium.page_source)
        self.selenium.find_element(By.ID, 'btn-ajouter-service').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Gestion des services').click()
        time.sleep(3)
        self.selenium.switch_to.window(window_name=self.selenium.window_handles[1])
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Police")]').click()
        self.selenium.find_element(By.CLASS_NAME, 'show_sous_service').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('fa-ban', self.selenium.page_source)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'abr Commissariat').click()
        self.selenium.find_element(By.ID, '21').click()
        time.sleep(self.DELAY * 5)
        self.assertIn('Organisation ajoutée', self.selenium.page_source)
        self.selenium.close()
        time.sleep(self.DELAY * 5)
        self.selenium.switch_to.window(window_name=self.selenium.window_handles[0])
        self.selenium.refresh()
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 5)
        self.assertIn('abr', self.selenium.page_source)
        self.assertIn('Supprimer l\'adresse e-mail', self.selenium.page_source)
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.CLASS_NAME, 'edit_user_active').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.CLASS_NAME, 'form-check-label').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'btn-form').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('État de l\'utilisateur édité avec succès', self.selenium.page_source)
        time.sleep(self.DELAY * 2)
        self.chosen_select('select_instances_modal_chosen', 'instance de test (43)')
        time.sleep(self.DELAY * 4)
        self.selenium.find_element(By.CLASS_NAME, 'service-list-item').click()
        time.sleep(self.DELAY * 2)
        self.assertNotIn('Vous n\'êtes pas habilité à voir ces informations.', self.selenium.page_source)
        self.deconnexion()

        print('**** test 2 équipe support ****')
        self.connexion("user_support")
        time.sleep(self.DELAY)
        self.selenium.get('%s%s' % (self.live_server_url, '/structure/administration_des_services/'))
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Police")]').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.CLASS_NAME, 'show_sous_service').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'abr').click()
        self.assertNotIn('Vous n\'êtes pas habilité à voir ces informations.', self.selenium.page_source)
        self.assertEqual(14, len(self.selenium.find_elements(By.CLASS_NAME, 'fa-pen')))
        time.sleep(self.DELAY * 5)
        self.chosen_select('select_instances_modal_chosen', 'instance de test (43)')
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.CLASS_NAME, 'service-list-item').click()
        self.assertNotIn('Vous n\'êtes pas habilité à voir ces informations.', self.selenium.page_source)
        self.assertEqual(14, len(self.selenium.find_elements(By.CLASS_NAME, 'fa-pen')))
        self.deconnexion()

        print('**** test 3 agent service consulte non admin ****')
        self.connexion(self.agent_commiss.username)
        self.selenium.get('%s%s' % (self.live_server_url, '/structure/administration_des_services/'))
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Police")]').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, '(Region test)').click()
        self.assertIn('Vous n\'êtes pas habilité à voir ces informations.', self.selenium.page_source)
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'DDSP (42)').click()
        self.assertEqual(0, len(self.selenium.find_elements(By.CLASS_NAME, 'fa-pen')))
        self.deconnexion()

        print('**** test 4 agent service consulte admin familly ****')
        self.connexion(self.agent_edsr.username)
        self.selenium.get('%s%s' % (self.live_server_url, '/structure/administration_des_services/'))
        self.selenium.find_element(By.XPATH, '//li[contains(text(), "Gendarmerie")]').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'show_sous_service').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'CGD1').click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, 'habilitations-avancees-link').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, 'div_id_admin_utilisateurs_subalternes_b').click()
        self.selenium.find_element(By.CLASS_NAME, 'btn-form').click()






