from django.test import TestCase
from django.contrib.auth.hashers import make_password as make
from django.contrib.auth.models import Group
from django.core import mail
from bs4 import BeautifulSoup as Bs

from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from structure.factories.service import ServiceConsulteFactory, FederationFactory
from structure.factories.structure import StructureOrganisatriceFactory
from structure.models.organisation import InvitationOrganisation
from core.factories import UserFactory
from core.models.instance import Instance
from core.models import User, ConfigurationGlobale


class InscriptionAgentTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        """
        Création des objets nécéssaires aux tests
        :return:
        """
        print()
        print('============ Inscription Utilisateurs (Clt) =============')
        ConfigurationGlobale.objects.create(pk=1, mail_action=True, mail_suivi=True, mail_tracabilite=True,
                                            mail_actualite=True)
        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__instruction_mode=Instance.IM_CIRCUIT_B)
        cls.dep07 = DepartementFactory.create(name='07',
                                              instance__name="instance 07 de test",
                                              instance__instruction_mode=Instance.IM_CIRCUIT_B)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        cls.commune = CommuneFactory.create(name='Bard', arrondissement=arrondissement)
        cls.prefecture = arrondissement.organisations.first()
        cls.mairie = cls.commune.organisation_set.first()
        cls.federation = FederationFactory.create(instance_fk=cls.dep.instance, departement_m2m=cls.dep)
        cls.cg = cls.dep.organisation_set.get(precision_nom_s="CD")
        cls.cgserv = ServiceConsulteFactory.create(instance_fk=cls.dep.instance,
                                                   precision_nom_s="Service technique départemental",
                                                   type_service_fk__categorie_fk__nom_s="Conseil départemental",
                                                   type_service_fk__nom_s="Service technique départemental",
                                                   departement_m2m=cls.dep)
        cls.organisateur = UserFactory.create(username='organisateur', password=make("123"),
                                              default_instance=cls.dep.instance, email='orga@test.fr')
        # Création d'une structure
        cls.structure_organisatrice = StructureOrganisatriceFactory.create(precision_nom_s="structure_test",
                                                                           commune_m2m=cls.commune, instance_fk=cls.dep.instance)
        cls.organisateur.organisation_m2m.add(cls.structure_organisatrice)
        cls.groupA = Group.objects.create(name='Administrateurs d\'instance')
        cls.admin = UserFactory.create(username='admin', password=make("123"),
                                       default_instance=cls.dep.instance, email='admin@test.fr')
        cls.admin.organisation_m2m.add(cls.prefecture)


    def Inscription(self, instance, service, sous_service, username='jandup', password='azeaz5646dfzdsf'):
        """
        Test de l'inscription d'un agent
        """
        print('\t===> Inscription')
        reponse = self.client.get('/inscription/agent')
        self.assertContains(reponse, 'Inscription')
        self.assertContains(reponse, 'Création d\'un compte Agent')
        agent_superieur = False
        if service == "CGn+1":
            service = "CG"
            agent_superieur = True
        reponse = self.client.post('/inscription/agent',
                                   {'first_name': 'Jean',
                                    'last_name': 'Dupont',
                                    'username': username,
                                    'email': 'ne-pas-repondre-42@manifestationsportive.fr',
                                    'password1': password,
                                    'password2': password,
                                    'instance': str(instance),
                                    'model': service,
                                    'service': sous_service,
                                    'agent_superieur': agent_superieur,
                                    },
                                   follow=True,
                                   HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content)
        return reponse

    def InscriptionOrga(self, username='jandup', password='azeaz5646dfzdsf'):
        """
        Test de l'inscription d'un organisateur
        """
        print('\t===> Inscription')
        reponse = self.client.get('/inscription/organisateur')
        self.assertContains(reponse, 'Inscription')
        self.assertContains(reponse, 'Confirmez-vous être un <strong>organisateur</strong>')
        self.assertContains(reponse, 'Structure organisatrice (personne morale)')
        self.assertContains(reponse, 'Représentant légal (personne physique)')
        reponse = self.client.post('/inscription/organisateur',
                                   {'first_name': 'Jean',
                                    'last_name': 'Dupont',
                                    'username': username,
                                    'email': 'ne-pas-repondre-42@manifestationsportive.fr',
                                    'password1': password,
                                    'password2': password,
                                    'cgu': True,
                                    'structure_name': 'MonAssoc',
                                    'type_of_structure': self.structure_organisatrice.type_organisation_fk.pk,
                                    'address': '1 rue de la république',
                                    'departement': self.dep.pk,
                                    'commune': self.commune.pk,
                                    'phone': '0655555555',
                                    },
                                   follow=True,
                                   HTTP_HOST='127.0.0.1:8000')
        return reponse

    def Verification(self, reponse):
        print('\t===> Vérification')
        self.assertContains(reponse, 'Vérifiez votre adresse email', count=2)
        self.assertContains(reponse, 'E-mail de confirmation envoyé à ne-pas-repondre-42@manifestationsportive.fr')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, '[example.com] Confirmer l\'adresse email')
        self.assertEqual(mail.outbox[0].to, ['ne-pas-repondre-42@manifestationsportive.fr'])
        url = mail.outbox[0].body.split()[-1]
        return url

    def Confirmation(self, url):
        print('\t===> Confirmation')
        reponse = self.client.get(url)
        self.assertContains(reponse, 'Confirmer une adresse email', count=2)
        self.assertContains(reponse, '>ne-pas-repondre-42@manifestationsportive.fr</a> est bien une adresse email de jandup.')
        reponse = self.client.post(url, follow=True,)
        self.assertContains(reponse, 'Adresse email confirmée', count=2)
        self.assertContains(reponse, 'Vous avez confirmé ne-pas-repondre-42@manifestationsportive.fr.')

    def ConnexionFail(self):
        print('\t===> echec connexion')
        reponse = self.client.get('/accounts/login/')
        self.assertContains(reponse, 'Connexion', count=3)
        reponse = self.client.post('/accounts/login/',
                                   {'login': 'jandup',
                                    'password': 'azeaz5646dfzdsf',
                                    },
                                   follow=True)
        self.assertContains(reponse, 'Compte inactif', count=2)

    def ConnexionPass(self, service=None):
        print('\t===> connexion réussie')
        user = User.objects.get(username="jandup")
        if service:
            user.is_active = True
            user.save()

        reponse = self.client.get('/accounts/login/')
        self.assertContains(reponse, 'Connexion', count=3)
        reponse = self.client.post('/accounts/login/',
                                   {'login': 'jandup',
                                    'password': 'azeaz5646dfzdsf',
                                    },
                                   follow=True)
        self.assertContains(reponse, 'Connexion avec jandup réussie.')
        url = reponse.request['PATH_INFO'].split('/')[1]
        if service:
            self.assertEqual(url, 'instructions')
            if service == self.prefecture:
                self.assertEqual(2, len(service.get_users_list()))
            else:
                self.assertEqual(1, len(service.get_users_list()))
            self.assertEqual(user, service.get_users_list()[0])
        else:
            self.assertEqual(url, 'tableau-de-bord-organisateur')

    def Deconnexion(self):
        print('\t===> déconnexion')
        self.client.post('/accounts/logout/', follow=True)
        User.objects.get(username="jandup").delete()
        del mail.outbox[0]

    def test_Inscription_organisateur(self):
        """
        Test de l'inscription d'un organisateur
        """
        print('**** test mauvaises inscriptions organisateur ****')
        reponse = self.InscriptionOrga(username='ja')
        self.assertContains(reponse, "doit contenir au moins 3 (trois) caractères")
        reponse = self.InscriptionOrga(password='aze14#')
        self.assertContains(reponse, "Ce mot de passe est trop court. Il doit contenir au minimum 9 caractères")
        reponse = self.InscriptionOrga(password='144568521')
        self.assertContains(reponse, "Ce mot de passe est entièrement numérique")
        reponse = self.InscriptionOrga(password='password0')
        self.assertContains(reponse, "Ce mot de passe est trop courant")
        reponse = self.InscriptionOrga(password='jeandupont')
        self.assertContains(reponse, "Le mot de passe est trop semblable au champ")
        # print(reponse.content.decode('utf-8'))

        print('**** test Inscription organisateur ****')
        reponse = self.InscriptionOrga()
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionPass()
        self.assertTrue(User.objects.get(username="jandup").is_organisateur())
        self.assertTrue(User.objects.get(username="jandup").organisation_m2m.get())
        self.Deconnexion()

        print('**** test Invitation organisateur ****')
        print('\t===> connexion')
        self.assertTrue(self.client.login(username='organisateur', password='123'))
        reponse = self.client.get('/accounts/profile/')
        self.assertContains(reponse, 'Profil de Bob ROBERT')
        self.assertContains(reponse, 'Utilisateur de la structure')
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        utilisateur = str(html.select("ul li a.organisation_user"))
        self.assertIn('Bob ROBERT', utilisateur)
        self.assertContains(reponse, 'Invitations d\'inscription')
        self.assertContains(reponse, 'Ajouter une invitation')
        print('\t===> invitation')
        reponse = self.client.post('/structure/ajax/invitation/',
                                   {'service_id': self.structure_organisatrice.id, 'email': 'invite@manifestationsportive.fr'},
                                   HTTP_HOST='127.0.0.1:8000')
        invit = InvitationOrganisation.objects.get()
        self.assertContains(reponse, "invitation vers invite@manifestationsportive.fr")
        self.assertContains(reponse, f"id=\"invit_{invit.id}")
        self.assertContains(reponse, f"data-service=\"{self.structure_organisatrice.id}")
        self.client.post('/accounts/logout/', follow=True)
        print('\t===> inscription')
        reponse = self.client.get(f'/inscription/organisateur?token={invit.token_s}')
        self.assertContains(reponse, 'Inscription')
        self.assertContains(reponse, 'Confirmez-vous être un <strong>organisateur</strong>')
        self.assertContains(reponse, 'Structure organisatrice (personne morale)')
        self.assertContains(reponse, 'Représentant légal (personne physique)')
        self.assertContains(reponse, 'structure_test')
        self.assertContains(reponse, 'readonly="readonly"', count=8)
        reponse = self.client.post(f'/inscription/organisateur?token={invit.token_s}',
                                   {'first_name': 'Jean',
                                    'last_name': 'Dupont',
                                    'username': 'jandup',
                                    'email': invit.email_s,
                                    'password1': 'azeaz5646dfzdsf',
                                    'password2': 'azeaz5646dfzdsf',
                                    'cgu': True,
                                    'structure_name': self.structure_organisatrice.precision_nom_s,
                                    'type_of_structure': self.structure_organisatrice.type_organisation_fk.pk,
                                    'address': self.structure_organisatrice.adresse_s,
                                    'departement': self.dep.pk,
                                    'commune': self.commune.pk,
                                    'phone': self.structure_organisatrice.telephone_s,
                                    },
                                   follow=True,
                                   HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Profil de Jean DUPONT')
        self.assertContains(reponse, 'Adresse validée')
        self.assertContains(reponse, 'Utilisateurs de la structure')
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        utilisateur = str(html.select("ul li a.organisation_user"))
        self.assertIn('Jean DUPONT', utilisateur)
        self.assertIn('Bob ROBERT', utilisateur)
        self.assertContains(reponse, 'Invitations d\'inscription')
        self.assertNotContains(reponse, 'Ajouter une invitation')
        self.assertContains(reponse, 'Aucune invitation proposée')

    def test_Inscriptions_Agents(self):
        """
        Test des inscriptions d'agents
        """
        print('**** test mauvaises inscriptions agents ****')
        reponse = self.Inscription(instance=self.dep.pk, service='Prefecture', sous_service=self.prefecture.pk, username='ja')
        self.assertContains(reponse, "doit contenir au moins 3 (trois) caractères")
        reponse = self.Inscription(instance=self.dep.pk, service='Prefecture', sous_service=self.prefecture.pk, password='aze14#')
        self.assertContains(reponse, "Ce mot de passe est trop court. Il doit contenir au minimum 9 caractères")
        reponse = self.Inscription(instance=self.dep.pk, service='Prefecture', sous_service=self.prefecture.pk, password='144568521')
        self.assertContains(reponse, "Ce mot de passe est entièrement numérique")
        reponse = self.Inscription(instance=self.dep.pk, service='Prefecture', sous_service=self.prefecture.pk, password='password0')
        self.assertContains(reponse, "Ce mot de passe est trop courant")
        reponse = self.Inscription(instance=self.dep.pk, service='Prefecture', sous_service=self.prefecture.pk, password='jeandupont')
        self.assertContains(reponse, "Le mot de passe est trop semblable au champ")
        # print(reponse.content.decode('utf-8'))

        print('**** test Inscription instructeur ****')
        reponse = self.Inscription(instance=self.dep.pk, service='Prefecture', sous_service=self.prefecture.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass(self.prefecture)
        self.assertTrue(self.prefecture.is_service_instructeur())
        self.assertTrue(User.objects.get(username="jandup").is_agent_service_instructeur())
        self.Deconnexion()

        print('**** test Inscription agent mairie ****')
        reponse = self.Inscription(instance=self.dep.pk, service='Commune', sous_service=self.mairie.pk)
        # print(reponse.content.decode('utf-8'))
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass(self.mairie)
        self.assertTrue(self.mairie.is_service_instructeur())
        self.assertTrue(self.mairie.is_mairie())
        self.assertTrue(User.objects.get(username="jandup").is_agent_service_instructeur())
        self.assertTrue(User.objects.get(username="jandup").is_agent_mairie())
        self.Deconnexion()

        print('**** test Inscription agent fédération ****')
        reponse = self.Inscription(instance=self.dep.pk, service='Federation', sous_service=self.federation.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass(self.federation)
        self.assertTrue(self.federation.is_federation())
        self.Deconnexion()

        print('**** test Inscription agent CD ****')
        reponse = self.Inscription(instance=self.dep.pk, service='CG', sous_service=self.cg.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass(self.cg)
        self.assertTrue(self.cg.is_service_consulte())
        self.assertTrue(User.objects.get(username="jandup").is_agent_service_consulte())
        self.Deconnexion()

        print('**** test Inscription agent local CD ****')
        reponse = self.Inscription(instance=self.dep.pk, service='CGService', sous_service=self.cgserv.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass(self.cgserv)
        self.assertTrue(self.cgserv.is_service_consulte())
        self.assertTrue(User.objects.get(username="jandup").is_agent_service_consulte())
        self.Deconnexion()

        print('**** test Invitation agent ****')
        print('\t===> connexion')
        self.assertTrue(self.client.login(username='admin', password='123'))
        reponse = self.client.get('/accounts/profile/')
        self.assertContains(reponse, 'Profil de Bob ROBERT')
        self.assertContains(reponse, 'Agent du service')
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        utilisateur = str(html.select("ul li a.organisation_user"))
        self.assertIn('Bob ROBERT', utilisateur)
        self.assertContains(reponse, 'Invitations d\'inscription')
        self.assertNotContains(reponse, 'Ajouter une invitation')
        self.assertContains(reponse, 'Aucune invitation proposée')
        self.client.post('/accounts/logout/', follow=True)
        self.admin.groups.add(self.groupA)
        self.assertTrue(self.client.login(username='admin', password='123'))
        reponse = self.client.get('/accounts/profile/')
        self.assertContains(reponse, 'Administrer la famille')
        reponse = self.client.get(f'/structure/administration_des_services/?service_id={self.prefecture.id}')
        self.assertContains(reponse, 'Administration des services')
        self.assertContains(reponse, 'Agent du service')
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        utilisateur = str(html.select("ul li a.organisation_user"))
        self.assertIn('Bob ROBERT', utilisateur)
        self.assertContains(reponse, 'Ajouter une invitation')
        print('\t===> invitation')
        reponse = self.client.post('/structure/ajax/invitation/',
                                   {'service_id': self.prefecture.id, 'email': 'invite@manifestationsportive.fr'},
                                   HTTP_HOST='127.0.0.1:8000')
        invit = InvitationOrganisation.objects.get()
        self.assertContains(reponse, "invitation vers invite@manifestationsportive.fr")
        self.assertContains(reponse, f"id=\"invit_{invit.id}")
        self.assertContains(reponse, f"data-service=\"{self.prefecture.id}")
        self.client.post('/accounts/logout/', follow=True)
        print('\t===> inscription')
        reponse = self.client.get(f'/inscription/agent?token={invit.token_s}')
        self.assertContains(reponse, 'Inscription')
        self.assertContains(reponse, 'Création d\'un compte Agent')
        self.assertContains(reponse, 'Préfecture de Montbrison')
        self.assertContains(reponse, 'readonly="readonly"', count=2)
        reponse = self.client.post(f'/inscription/agent?token={invit.token_s}',
                                   {'first_name': 'Jean',
                                    'last_name': 'Dupont',
                                    'username': 'jandup',
                                    'email': invit.email_s,
                                    'password1': 'azeaz5646dfzdsf',
                                    'password2': 'azeaz5646dfzdsf',
                                    'instance': self.dep.pk,
                                    'model': 'Prefecture',
                                    'service': self.prefecture.pk,
                                    'agent_superieur': False,
                                    },
                                   follow=True,
                                   HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Adresse email confirmée')
        self.assertContains(reponse, 'Votre adresse email est maintenant confirmée')
        self.assertContains(reponse, 'Un administrateur va bientôt valider votre compte')
        self.assertTrue(self.client.login(username='admin', password='123'))
        reponse = self.client.get(f'/structure/administration_des_services/?service_id={self.prefecture.id}')
        self.assertContains(reponse, 'Administration des services')
        self.assertContains(reponse, 'Agents du service')
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        utilisateur = str(html.select("ul li a.organisation_user"))
        self.assertIn('Jean DUPONT', utilisateur)
        self.assertIn('Bob ROBERT', utilisateur)
        utilisateur = str(html.select("ul li a.organisation_user.text-danger"))
        self.assertIn('Jean DUPONT', utilisateur)
        self.assertContains(reponse, 'Invitations d\'inscription')
        self.assertContains(reponse, 'Ajouter une invitation')
        self.assertContains(reponse, 'Aucune invitation proposée')
        # print(reponse.content.decode('utf-8'))
