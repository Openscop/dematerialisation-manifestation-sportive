import {registerServiceNode} from "./g6/nodes";
import {registerBehaviors} from "./g6/behaviors";
import G6, {Graph, IItemBase, ModelConfig} from "@antv/g6";
import {modes} from "./g6/modes";
import {getToolbars} from "./g6/toolbars";
import {registerEdges} from "./g6/edges";
import {createServiceItemList} from "./utils/renderer";
import {TreeGraphData} from "@antv/g6-core/lib/types";
import {serviceSort} from "./utils/Sort";
import {deletedElements} from "./utils/DeletedElements";
import {debounce} from "lodash-es";
import {tooltip} from "./g6/tooltips";

export type networkData = {
    nodes: TreeGraphData[],
    edges: ModelConfig[],
}
export type data = {
    network: networkData,
    services: ModelConfig[],
}

export type NetworkOptions = {
    network?: {
        width?: number,
        height?: number,
    },
    layout?: string,
}

export class Network {
    _data
    private readonly _selecteur
    _options: NetworkOptions = {}

    _network: Graph | undefined
    private _container: HTMLElement | undefined
    private _networkContainer: HTMLDivElement | undefined
    private _graphContainer: HTMLDivElement | undefined
    private _servicesContainer: HTMLDivElement | undefined

    constructor(selecteur: string, data: data | null = null, options: NetworkOptions = {}) {
        this._selecteur = selecteur
        if (data) {
            this._data = data
        } else {
            this._data = {
                network: {
                    nodes: [],
                    edges: []
                },
                services: []
            }
        }

        this._options = Object.assign({}, this._options, options)

        this._initLayout()
        this._initNetwork()
        this.refreshServices()
    }

    get selecteur() {
        return this._selecteur;
    }

    get options() {
        return this._options
    }

    get container(): HTMLElement | undefined {
        return this._container
    }

    get graphContainer(): HTMLDivElement | undefined {
        return this._graphContainer
    }

    get networkData() {
        return this._data.network
    }

    get availableServices(): ModelConfig[] {
        return this._data.services
    }

    set availableServices(data: ModelConfig[]) {
        this._data.services = data
    }

    get graph(): Graph | undefined {
        return this._network
    }

    private static _getLayoutConfig(layout: string | undefined) {
        let config
        switch (layout) {
            case 'force':
                config = {
                    type: 'force',
                    preventOverlap: true,
                    nodeSpacing: 100,
                    damping: 1,
                };
                break;
            default:
                config = {
                    begin: [0, 0],
                    type: 'dagre',
                    rankdir: 'TB',
                    nodesep: 15,
                    ranksep: 30,
                    controlPoints: true,
                }
        }
        return config
    }

    private static _getAppContainer() {
        const graphContainer: HTMLElement | null = document.querySelector('#graph')
        if (!graphContainer) {
            throw new Error('network container not found')
        }
        return graphContainer
    }

    private static _getAppSize(container: HTMLElement) {
        return container.getBoundingClientRect()
    }

    _initLayout() {
        const container: HTMLElement | null = document.querySelector(this._selecteur)
        if (!container) {
            throw new Error('container not found')
        }
        this._container = container

        this._networkContainer = document.createElement('div')
        this._networkContainer.id = 'network'

        this._graphContainer = document.createElement('div')
        this._graphContainer.id = 'graph'
        this._servicesContainer = document.createElement('div')
        this._servicesContainer.id = 'services'

        this._networkContainer.append(this._servicesContainer)
        this._networkContainer.append(this._graphContainer)
        container.append(this._networkContainer)
    }

    _initNetwork() {
        registerEdges()
        registerServiceNode()
        registerBehaviors(this)
        const toolbars = getToolbars(this)

        const graphContainer = Network._getAppContainer()
        const size = Network._getAppSize(graphContainer)
        const layout = Network._getLayoutConfig(this.options.layout)
        window.addEventListener('resize', debounce(this.resize.bind(this), 500))

        this._network = new G6.Graph({
            container: graphContainer, // String | HTMLElement, required, the id of DOM element or an HTML node
            width: size.width, // Number, required, the width of the graph
            height: size.height, // Number, required, the height of the graph
            defaultNode: {
                type: 'service',
                size: [300, 60],
                fill: '#fff',
                opacity: 1,
            },
            layout,
            modes: modes,
            animate: true,
            plugins: [...toolbars, tooltip],
            fitView: true,
            fitCenter: true,
        });
        this.graph?.data(this.networkData)
        this.graph?.render()
        this.graph?.on('aftermodechange', this.afterModeChanged.bind(this))
        this.graph?.on('afterremoveitem', this.afterItemRemoved.bind(this))
        this.graph?.on('afteradditem', this.afterItemAdded.bind(this))
    }

    refreshNetwork() {
        this.graph?.refresh()
    }

    refreshServices() {
        if (this._servicesContainer) {
            this._servicesContainer.innerHTML = ""

            const servicesHeaders = document.createElement('div')
            servicesHeaders.classList.add('services-header')
            servicesHeaders.innerHTML = `<span class="fas fa-circle-1 me-2"></span> Glissez/déposez un service sur l’espace central`
            this._servicesContainer.append(servicesHeaders)

            if(this.availableServices?.length > 0) {
                this.availableServices?.sort(serviceSort).forEach((service) => {
                    this._servicesContainer?.append(createServiceItemList(service))
                })
            } else {
                const noService = document.createElement('div')
                noService.classList.add('no-service')
                noService.innerText = 'Tous les services sont déjà sur l\'espace central'
                this._servicesContainer.append(noService)
            }
        }
    }

    updateNetworkData(networkData: networkData) {
        this._data.network = networkData
        this.graph?.clear(true)
        networkData.nodes.forEach((node) => {
            this.graph?.addItem('node', node)
        })

        networkData.edges.forEach((node) => {
            this.graph?.addItem('edge', node)
        })
        this.refreshNetwork()
        this.graph?.layout()
    }

    updateServicesData(serviceData: ModelConfig[]) {
        this._data.services = serviceData
        this.refreshServices()
    }

    addService(services: ModelConfig | ModelConfig[]) {
        if (Array.isArray(services)) {
            this._data.services.concat(services)
        } else {
            this._data.services.push(services)
        }
        this.refreshServices()
    }

    afterModeChanged(event: any) {
        if (['default', 'deleteElements'].includes(event.mode)) {
            this.graph?.getNodes().forEach(node => {
                if (node.hasState('selected:add-edge')) this.graph?.clearItemStates(node, 'selected:add-edge')
            })

        }
    }

    afterItemRemoved(event: any) {
        if (event.type === 'edge') {
            deletedElements.add(event.item)
        }
    }

    afterItemAdded(event: any) {
        const item: IItemBase = event.item
        if (item.getType() === 'edge') {
            deletedElements.remove(item.getModel())
        }
    }

    resize() {
        const size = Network._getAppSize(Network._getAppContainer())
        this.graph?.changeSize(size.width, size.height)
    }
}