import {ModelConfig} from "@antv/g6";

export function serviceSort(serviceA: ModelConfig, serviceB: ModelConfig) {
    if((serviceA.label as string) > (serviceB.label as string)) return 1
    if((serviceA.label as string) < (serviceB.label as string)) return -1
    return 0
}