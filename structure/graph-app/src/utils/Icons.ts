import {color} from "./colors";

export const iconsCorrespondances = {
    admin_service_famille_b: [
        {
            label: 'Administrer tous les services de la famille',
            icon: 'fas fa-house-building',
            text: "\ue1b1",
            fill: color.noir,
        }
    ],
    admin_utilisateurs_subalternes_b: [
        {
            label: 'Administrer ses agents subalternes',
            icon: 'fas fa-people-roof',
            text: "\ue537",
            fill: color.noir,
        }
    ],
    porte_entree_avis_b: [
        {
            label: 'Recevoir des demandes d\'avis',
            icon: 'fas fa-sign-in-alt',
            text: "\uf2f6",
            fill: color.violet,
        }
    ],
    autorise_rendre_avis_b: [
        {
            label: 'Rendre les avis au service instructeur',
            icon: 'fas fa-sign-out-alt',
            text: "\uf2f5",
            fill: color.violet,
        }
    ],
    autorise_rendre_preavis_a_service_avis_b: [
        {
            label: 'Rendre les préavis directement au service en charge de l\'avis',
            icon: 'fas fa-sign-out-alt',
            text: "\uf2f5",
            fill: color.orange,
        }
    ],
    // autorise_acces_consult_organisateur_b: [
    //     {
    //         label: 'Autoriser les organisateurs à octroyer un accès en lecture à ce service',
    //         icon: 'fa fa-user-ninja',
    //         text: "\uf504",
    //         fill: "#198754",
    //     }
    // ],
    autorise_mandater_avis_famille_b: [
        {
            label: 'Mandater un membre de la famille à traiter une demande d\'avis',
            icon: 'fas fa-arrow-alt-square-right',
            text: "\uf352",
            fill: color.violet,
        },
    ],
    autorise_interroge_preavis_famille_b: [
        {
            label: 'Interroger un membre de la famille avec un préavis',
            icon: 'fas fa-chevron-circle-right',
            text: "\uf138",
            fill: color.orange,
        },
    ],
}