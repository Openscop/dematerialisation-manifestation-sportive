import {EdgeConfig} from "@antv/g6-core/lib/types";

class DeletedElements {
    elements: EdgeConfig[] = []

    add(edge: EdgeConfig) {
        this.elements.push(edge)
    }

    remove(deletedEdge: EdgeConfig) {
        this.elements = this.elements.filter((edge: EdgeConfig) => {
            return edge.source === deletedEdge.source && edge.target === deletedEdge.target && edge.type === deletedEdge.type
        })
    }

    get() {
        return this.elements
    }
}

export const deletedElements = new DeletedElements()