function getCookie(name: string): string | null {
    let cookieValue = null;
    if (document.cookie && document.cookie != '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

export const query = (url: string, method: string, body: any = {}, headersData: any = {}) => {
    const csrftoken = getCookie('csrftoken');
    const headers = new Headers()
    if(headersData) {
        // @ts-ignore
        Object.entries(headersData).forEach(([key, value]) => headers.append(key, value))
    }
    headers.append("X-CSRFToken", "" + csrftoken)
    const options = {
        method,
        headers,
    }
    if(method === 'POST') {
        // @ts-ignore
        if(['application/x-www-form-urlencoded', 'multipart/form-data'].includes(headers.get('Content-type'))) {
            // @ts-ignore
            options.body = body
        } else {
            // @ts-ignore
            options.body = JSON.stringify(body)
        }
    }
    return fetch(url, options)
        .then(checkResponse)
}

const checkResponse = (response: Response) => {
    if (response.ok) {
        if(response?.headers?.get("content-type")?.includes('json')) return response.json()
        else return response.text()
    } else {
        throw new Error('' + response.status)
    }
}