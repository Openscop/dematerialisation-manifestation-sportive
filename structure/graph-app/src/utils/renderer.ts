import { ModelConfig } from "@antv/g6"

export function createServiceItemList(service: ModelConfig) {
    const container: HTMLDivElement = document.createElement('div')
    container.classList.add('item-service')
    container.setAttribute('draggable', 'true')

    const name: HTMLDivElement = document.createElement('div')
    name.classList.add('item-service-name')
    name.innerText = (service.label as string)

    container.append(name)

    container.dataset.id = (service.id as string)

    container.addEventListener('dragstart', (event) => {
        if(event.dataTransfer && event.target) {
            (event.target as HTMLDivElement).style.opacity = "0.4"
            event.dataTransfer.effectAllowed = 'move';
            event.dataTransfer.setData('id', (service.id as string));
        }
    })
    container.addEventListener('dragend', (event) => {
        if(event.target) {
            (event.target as HTMLDivElement).style.opacity = "1"
        }
    })

    return container
}

export function loading() {
    return `
        <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
    `
}