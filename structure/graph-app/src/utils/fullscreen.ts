// @ts-nocheck

export function toggleFullscreen(elem: HTMLElement) {
    if(document.fullscreenElement) {
        stopFullscreen(elem)
    } else {
        enableFullscreen(elem)
    }
}

function enableFullscreen(elem: HTMLElement) {
    if (elem.requestFullscreen) {
        elem.requestFullscreen()
    } else if (elem.mozRequestFullScreen) {
        elem.mozRequestFullScreen()
    } else if (elem.webkitRequestFullScreen) {
        elem.webkitRequestFullScreen()
    } else if (elem.msRequestFullscreen) {
        elem.msRequestFullscreen()
    }
}

function stopFullscreen(elem: HTMLElement) {
    if (document.exitFullscreen) {
        if (document.fullscreenElement !== null) {
            document.exitFullscreen()
        }
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen()
    } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen()
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen()
    }
}