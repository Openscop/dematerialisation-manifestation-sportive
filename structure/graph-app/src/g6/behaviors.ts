import G6, {Graph, IG6GraphEvent, ModelConfig} from "@antv/g6";
import {Network} from "../Network";

export function registerBehaviors(network: Network) {
    G6.registerBehavior('add-edges', {
        getDefaultCfg() {
            return {
                'edge-type': 'adresser'
            };
        },
        getEvents() {
            return {
                'node:click': 'onNodeClick',
                'canvas:click': 'onCanvasClick'
            };
        },
        onNodeClick(e: IG6GraphEvent) {
            const graph = (this.graph as Graph)
            const item = e.item;
            if(!item) {
                throw new Error('add edge item missing')
            }
            if (item.hasState(`selected:add-edge`)) {
                graph.clearItemStates(item, ['selected:add-edge']);
                return;
            }
            const elements = graph.findAllByState('node', 'selected:add-edge')
            if(elements.length === 1) {
                const edge: ModelConfig = {
                    source: elements[0].getContainer().cfg.id,
                    target: item.getContainer().cfg.id,
                    type: (this['edge-type'] as string),
                }
                // c'est le deuxieme noeud on ajoute le edge :
                graph.addItem('edge', edge)
                // @ts-ignore
                this.removeNodesState()
            } else {
                // Set the 'active' state of the clicked node to be true
                graph.setItemState(item, 'selected', 'add-edge');
            }
        },
        onCanvasClick() {
            // @ts-ignore
            this.removeNodesState();
        },
        removeNodesState() {
            const graph = (this.graph as Graph)
            graph.findAllByState('node', 'selected:add-edge').forEach(node => {
                graph.clearItemStates(node, ['selected:add-edge']);
            });
        }
    });
    G6.registerBehavior('delete-elements', {
        getDefaultCfg() {
            return {};
        },
        getEvents() {
            return {
                'edge:click': 'onEdgeClick',
                'node:click': 'onEdgeClick',
            };
        },
        onEdgeClick(e: IG6GraphEvent) {
            const graph = (this.graph as Graph)
            const item = e.item;
            if(!item) {
                throw new Error('click on unknown element error')
            }
            if(item.getType() === 'node') {
                network.addService(item.getModel())
            }
            graph.removeItem(item)
        },
    });
    G6.registerBehavior('drop-service', {
        getDefaultCfg() {
            return {};
        },
        getEvents() {
            return {
                'canvas:drop': 'onDrop',
            };
        },
        onDrop(event: IG6GraphEvent) {
            const id = (event.originalEvent as DragEvent)?.dataTransfer?.getData('id')
            const serviceToLoad = network.availableServices.find((service) => service.id === id)
            if(serviceToLoad) {
                serviceToLoad.x = event.x - 150
                serviceToLoad.y =  event.y - 30
                const updatedServices = network.availableServices.filter((service) => service.id !== id)
                network.graph?.addItem('node', serviceToLoad, true)
                network.updateServicesData(updatedServices)
                network.refreshNetwork()
            }
        },
    });
}