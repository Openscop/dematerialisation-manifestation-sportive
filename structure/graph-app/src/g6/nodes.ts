import G6, {IGroup, ModelConfig} from "@antv/g6";
import {iconsCorrespondances} from "../utils/Icons";
import {ShapeDefine, ShapeOptions} from "@antv/g6-core/lib/interface/shape";

export type serviceData = {
    admin_service_famille_b: boolean
    admin_utilisateurs_subalternes_b: boolean
    porte_entree_avis_b: boolean
    autorise_rendre_avis_b: boolean
    autorise_rendre_preavis_b: boolean
    autorise_acces_consult_organisateur_b: boolean
    autorise_mandater_avis_famille_b: boolean
    autorise_interroge_preavis_famille_b: boolean
}

export function registerServiceNode() {
    const customNode: ShapeOptions | ShapeDefine = {
        draw(cfg?: ModelConfig, group?: IGroup) {
            if (!cfg || !group) {
                throw new Error('error registering node')
            }

            let width: number = 0
            let height: number = 0

            if (cfg.size) {
                width = (cfg.size as number[])[0]
                height = (cfg.size as number[])[1]
            }

            const rect = group.addShape('rect', {
                attrs: {
                    width: width + 10,
                    height: height + 10,
                },
                draggable: true,
                name: 'rect-intention',
            });

            group.addShape('rect', {
                attrs: {
                    x: 5,
                    y: 5,
                    width: width,
                    height: height,
                    lineWidth: (cfg.lineWidth as number),
                    opacity: (cfg.fillOpacity as number),
                    stroke: 'rgba(0,0,0,0.2)',
                    fill: (cfg.fill as string),
                },
                draggable: true,
                name: 'rect-intention-2',
            });
            // title
            group.addShape('text', {
                attrs: {
                    x: 300 / 2,
                    y: 25,
                    text: (cfg.label as string).substring(0, 35),
                    fontSize: 14,
                    fill: '#000',
                    fontWeight: 'bold',
                    textAlign: 'center'
                },
                draggable: true,
                name: 'rect-title',
            });
            // time
            const data = (cfg.data as serviceData);

            const iconesList: any[] = [];

            (Object.keys(data) as Array<keyof serviceData>).forEach((key: keyof serviceData) => {
                if (data[key]) {
                    // @ts-ignore
                    iconsCorrespondances[key]?.forEach((iconSettings) => {
                        iconesList.push({
                            attrs: {
                                ...iconSettings,
                                y: 50,
                                fontSize: 16,
                                fontFamily: 'openscop-icon',
                            },
                            draggable: true,
                            name: 'actions-icon',
                        })
                    })
                }
            })

            const nbIcons = iconesList.length

            iconesList.forEach((cfg, idx) => {
                cfg.attrs.x = (300 / 2 - (30*nbIcons) / 2) + 30 * idx
                group.addShape('text', cfg)
            })

            return rect;
        },
        // update: undefined pour forcer le call a draw lors d'une mise a jour
        update: undefined,
        setState(name, value, item) {
            // on defini les style manuellement sinon les icones bug...
            if(item) {
                const group = item.getContainer();
                const shape = group.get('children')[0]; // Find the first graphics shape of the node. It is determined by the order of being added
                if (name === 'selected') {
                    switch (value) {
                        case 'add-edge':
                            shape.attr('fill', '#ddd');
                            break
                        default:
                            shape.attr('fill', '#fff')
                    }
                }
                if(name === 'selected:add-edge') {
                    shape.attr('fill', '#fff')
                }
            }
        },
        getAnchorPoints() {
            return [
                [0, 0], // angle en haut a gauche
                [0, 0.5], // milieu de la barre du haut
                [0, 1], // angle en haut a droite
                [0.5, 0], // milieu de la barre de gauche
                [0.5, 1], // milieu de la barre de droite
                [1, 0], // angle en bas a gauche
                [1, 0.5], // milieu de la barre du bas
                [1, 1], // angle en bas a droite
            ];
        },
    }
    G6.registerNode('service', customNode, 'single-node')
}