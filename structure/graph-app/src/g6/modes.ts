export const modes = {
    default: ['drag-canvas', 'zoom-canvas', 'drag-node', 'drop-service'],
    addAddresserEdge: ['drag-canvas', 'zoom-canvas', { type: 'add-edges', 'edge-type': 'adresser'}, 'drop-service'],
    addInterrogerEdge: ['drag-canvas', 'zoom-canvas', { type: 'add-edges', 'edge-type': 'interroger'}, 'drop-service'],
    addMandaterEdge: ['drag-canvas', 'zoom-canvas', { type: 'add-edges', 'edge-type': 'mandater'}, 'drop-service'],
    deleteElements: ['drag-canvas', 'zoom-canvas', 'delete-elements', 'drop-service'],
}