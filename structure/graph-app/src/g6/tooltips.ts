import G6, {Graph} from "@antv/g6";
import {IG6GraphEvent} from "@antv/g6-core";
import {loading} from "../utils/renderer";
import {query} from "../utils/Request";

export const tooltip = new G6.Tooltip({
    offsetX: 0,
    offsetY: 0,
    trigger: "click",
    className: 'openscop-tooltip',
    shouldBegin(e: IG6GraphEvent | undefined) {
        const graph = (e?.currentTarget as Graph)
        return graph.getCurrentMode() === 'default'
    },
    getContent(e: IG6GraphEvent | undefined) {
        const graph = (e?.currentTarget as Graph)
        const item = e?.item
        const id = item?.getModel().id
        if (id) {
            const content = document.createElement('div')
            content.classList.add('tooltip-content')

            const closeIcon = document.createElement("div")
            closeIcon.classList.add('tooltip-close')
            closeIcon.innerHTML = `<span class="fas fa-times"></span>`
            closeIcon.addEventListener('click', (e) => {
                e.stopPropagation()
                e.preventDefault()
                tooltip.hide()
            })

            const tooltipContent = document.createElement('div')
            content.append(tooltipContent)

            const render = () => (`${loading()}`)
            const displayForm = (body: string) => {
                tooltipContent.innerHTML = body
                        // @ts-ignore
                        AfficherIcones()
                        const form = tooltipContent.querySelector('form')
                        const scriptImport = document.createElement('script')
                        scriptImport.src = '/static/aide/contextHelp.jquery.js'
                        scriptImport.type = 'text/javascript'
                        if (form) {
                            form.appendChild(scriptImport)
                            form.addEventListener('submit', (e) => {
                                e.preventDefault()
                                if (e.target) {
                                    submitForm(form)
                                }
                            })
                        }
            }
            const submitForm = (form: HTMLFormElement) => {
                const data = new FormData(form)
                // @ts-ignore
                query(form.action, 'POST', new URLSearchParams(data), {'content-type': 'application/x-www-form-urlencoded'})
                    .then(() => updateNode(data))
                    .catch(err => console.debug('error : ', err))
            }
            const renderForm = () => {
                query(`/structure/update_service_consulte_action/${id}/`, 'GET')
                    .then(displayForm)
                    .catch(err => console.debug('errror : ', err))
            }
            const updateNode = (form: FormData) => {
                const newModel:any = {...item.getModel()}
                Object.keys(newModel.data).forEach(key => {
                    newModel.data[key] = form.get(key) === 'on'
                })
                graph.updateItem(item, newModel)
                tooltip.hide()
            }
            renderForm()
            tooltipContent.innerHTML = render()
            content.append(closeIcon)

            return content
        }
        return 'erreur'
    },
    itemTypes: ['node']
});