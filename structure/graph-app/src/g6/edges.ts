import G6, {IGroup, ModelConfig } from "@antv/g6";
import {color} from "../utils/colors";

type point = {
    x: number;
    y: number;
}

export function registerEdges() {
    registerAdresser()
    registerInterroger()
    registerMandater()
}

function sqr(value: number) {
    return value * value;
}

function getAngleDegrees(fromX: number, fromY: number, toX: number, toY: number) {
    let deltaX = toX - fromX;
    let deltaY = toY - fromY;
    return Math.atan2(deltaY, deltaX)
}

function computedArrowParts(startPoint: point, endPoint: point, distance: number, length = 10) {
    const radians = getAngleDegrees(startPoint.x, startPoint.y, endPoint.x, endPoint.y)
    const startFleche = {
        x: startPoint.x + distance * Math.cos(radians),
        y: startPoint.y + distance * Math.sin(radians),
    }

    const flecheLigne1 = {
        x: startFleche.x - length * Math.cos(-Math.PI / 4 - radians),
        y: startFleche.y + length * Math.sin(-Math.PI / 4 - radians),
    }
    const flecheLigne2 = {
        x: startFleche.x - length * Math.cos(Math.PI / 4 - radians),
        y: startFleche.y + length * Math.sin(Math.PI / 4 - radians),
    }

    let arrondi
    if (radians > 0) {
        if (Math.PI / 2 > radians) {
            arrondi = ['C', startFleche.x, startFleche.y, startFleche.x + .01, startFleche.y + .01, startFleche.x, startFleche.y]
        } else {
            arrondi = ['C', startFleche.x, startFleche.y, startFleche.x - .01, startFleche.y + .01, startFleche.x, startFleche.y]
        }
    } else {
        if (Math.PI / 2 > Math.abs(radians)) {
            arrondi = ['C', startFleche.x, startFleche.y, startFleche.x + .01, startFleche.y - .01, startFleche.x, startFleche.y]
        } else {
            arrondi = ['C', startFleche.x, startFleche.y, startFleche.x - .01, startFleche.y - .01, startFleche.x, startFleche.y]
        }
    }
    return {
        startFleche,
        flecheLigne1,
        flecheLigne2,
        arrondi,
    }
}

function createArrow(startPoint: point, endPoint:  point, distance: number, length = 10, filled = false) {
    const {
        startFleche,
        flecheLigne1,
        flecheLigne2,
        arrondi
    } = computedArrowParts(startPoint, endPoint, distance, length)

    const svg = [
        ['M', startFleche.x, startFleche.y],
        arrondi,
        ['M', startFleche.x, startFleche.y],
        ['L', flecheLigne1.x, flecheLigne1.y],
        ['M', startFleche.x, startFleche.y],
        ['L', flecheLigne2.x, flecheLigne2.y],
    ]
    if (filled) {
        svg.push(['L', flecheLigne1.x, flecheLigne1.y])
    }

    return svg
}

function getDistance(x1: number, y1: number, x2: number, y2: number) {
    return Math.sqrt(sqr(y2 - y1) + sqr(x2 - x1));
}

function registerAdresser() {
// addresser avis
    G6.registerEdge('adresser', {
        draw(cfg?: ModelConfig, group?: IGroup) {
            if(!cfg || !group) {
                throw new Error('error registering edge')
            }
            const startPoint = cfg.startPoint;
            const endPoint = cfg.endPoint;

            if(!endPoint || !startPoint) {
                throw new Error('error registering edge')
            }
            const longeurTotal = getDistance(startPoint.x, startPoint.y, endPoint.x, endPoint.y)

            const espacement = 10
            const arrows = []
            for (let i = espacement; i < longeurTotal; i += espacement) {
                if (i + espacement > longeurTotal) {
                    arrows.push(...createArrow(startPoint, endPoint, i, 30))
                } else {
                    arrows.push(...createArrow(startPoint, endPoint, i))
                }
            }

            return group.addShape('path', {
                attrs: {
                    lineAppendWidth: 15,
                    stroke: color.violet,
                    lineWidth: 3,
                    path: [
                        ['M', startPoint.x, startPoint.y],
                        ...arrows
                    ],
                },
                // must be assigned in G6 3.3 and later versions. it can be any value you want
                name: 'path-shape',
            });
        },
    });
}

function registerInterroger() {
// interoger
    G6.registerEdge('interroger', {
        draw(cfg?: ModelConfig, group?: IGroup) {
            if(!cfg || !group) {
                throw new Error('error registering edge')
            }
            const startPoint = cfg.startPoint;
            const endPoint = cfg.endPoint;

            if(!endPoint || !startPoint) {
                throw new Error('error registering edge')
            }

            return group.addShape('path', {
                attrs: {
                    lineAppendWidth: 15,
                    stroke: color.orange,
                    fill: color.orange,
                    lineWidth: 3,
                    path: [
                        ['M', startPoint.x, startPoint.y],
                        ['L', endPoint.x, endPoint.y],
                    ],
                    endArrow: {
                        // The custom arrow is a path points at (0, 0), and its tail points to the positive direction of x-axis
                        path: 'M 0,0 L 20,10 M 0,0 L 20,-10',
                        // the offset of the arrow, nagtive value means the arrow is moved alone the positive direction of x-axis
                        // d: -10
                        // styles are supported after v3.4.1
                        stroke: color.orange,
                        opacity: 0.8,
                        // ...
                    },
                },
                // must be assigned in G6 3.3 and later versions. it can be any value you want
                name: 'path-shape',
            });
        }
    });
}

function registerMandater() {
// mandater
    G6.registerEdge('mandater', {
        draw(cfg?: ModelConfig, group?: IGroup) {
            if(!cfg || !group) {
                throw new Error('error registering edge')
            }
            const startPoint = cfg.startPoint;
            const endPoint = cfg.endPoint;

            if(!endPoint || !startPoint) {
                throw new Error('error registering edge')
            }

            const longeurTotal = getDistance(startPoint.x, startPoint.y, endPoint.x, endPoint.y)

            const espacement = 15
            const arrows = []
            for (let i = espacement; i < longeurTotal; i += espacement) {
                if (i + espacement > longeurTotal) {
                    arrows.push(...createArrow(startPoint, endPoint, i, 20, true))
                } else {
                    arrows.push(...createArrow(startPoint, endPoint, i, 10, true))
                }
            }
            return group.addShape('path', {
                attrs: {
                    lineAppendWidth: 15,
                    stroke: color.violet,
                    lineWidth: 0,
                    fill: color.violet,
                    path: [
                        ['M', startPoint.x, startPoint.y],
                        ...arrows
                    ],

                },
                // must be assigned in G6 3.3 and later versions. it can be any value you want
                name: 'path-shape',
            });
        },
    });
}