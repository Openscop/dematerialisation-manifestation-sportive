import G6, {IAbstractGraph} from "@antv/g6";
import {deletedElements} from "../utils/DeletedElements";
import {query} from "../utils/Request";
import Toastify from 'toastify-js'
import "toastify-js/src/toastify.css"
import addresser from '../assets/Avis - adresser.png'
import mandater from '../assets/Avis - mandater.png'
import interroger from '../assets/Préavis - interroger.png'
import {toggleFullscreen} from "../utils/fullscreen";
import {Network} from "../Network";

const handleSave = (graph: IAbstractGraph) => {
    const loading = Toastify({
        text: "Enregistrement...",
        className: "info",
        duration: 10000,
    })
    loading.showToast()
    const toSave = {
        ...graph.save(),
        deleted: deletedElements.get(),
    }
    // @ts-ignore
    query(api, 'POST', toSave)
        .then(body => {
            loading.hideToast()
            if (body.success) {
                Toastify({
                    text: "Sauvegarde réussit",
                    className: "success",
                    duration: 3000
                }).showToast();
            } else {
                Toastify({
                    text: "Sauvegarde échoué",
                    className: "warning",
                    duration: 3000
                }).showToast();
            }
        })
        .catch((err) => {
            loading.hideToast()
            Toastify({
                text: err.message,
                className: "danger",
                duration: 3000
            }).showToast();
        })
}
const handleMode = (mode: string, graph: IAbstractGraph) => {
    document.querySelector(`li.toolbar-item.active`)?.classList.remove('active')
    if (graph.getCurrentMode() !== mode) {
        document.querySelector(`li[code=${mode}]`)?.classList.add('active')
        graph.setMode(mode)
    } else {
        document.querySelector(`li[code=${mode}]`)?.classList.remove('active')
        graph.setMode('default')
    }
}

export function getToolbars(app: Network) {
    return [
        new G6.ToolBar({
            getContent: () => {
                return `
<div>
    <div class="toolbar-element">
    <span class="toolbar-label"><span class="fas fa-circle-2 me-2"></span> Sélectionnez l’interation à créer :</span>
        <ul class="toolbar-actions images">
            <li class="toolbar-item" code='addAddresserEdge' title="Interactions de type adresser">
                <img src="${addresser}" alt="Interactions de type adresser">
            </li>
            <li class="toolbar-item" code='addInterrogerEdge' title="Interactions de type interroger">
                <img src="${interroger}" alt="Interactions de type interroger">
            </li>
            <li class="toolbar-item" code='addMandaterEdge' title="Interactions de type mandater">
                <img src="${mandater}" alt="Interactions de type mandater">
            </li>
        </ul>
    </div>
    <div class="toolbar-element">
        <span class="toolbar-label">
            <span class="fas fa-circle-3 me-2"></span>
            Sélectionnez un service puis un second<br>pour créer l’interaction.
        </span>
    </div>
    <div class="toolbar-element">
        <span class="toolbar-label flex">
            <span class="fas fa-circle-4 me-2"></span>
            Enregistrez :
            <ul class="toolbar-actions icones">
                <li class="toolbar-item m-0 ml-2" code='save' title="Enregistrer les modifications">
                    <span class="fas fa-floppy-disk"></span>
                </li>
            </ul>
        </span>
        <ul class="toolbar-actions icones">
            <li class="toolbar-item m-0" code='deleteElements' title="Mode suppression d’élèments">
                <span class="fas fa-eraser"></span>
            </li>
            <li class="toolbar-item m-0 ml-2" code='fullscreen' title="Plein écran">
                <span class="fas fa-arrows-maximize"></span>
            </li>
        </ul>
    </div>  
</div>          
                `
            },
            handleClick: (code: string, graph: IAbstractGraph) => {
                if (code) {
                    if (code === 'save') {
                        handleSave(graph)
                    } else if (code === 'fullscreen') {
                        if (app.container) toggleFullscreen(app.container)
                    } else {
                        handleMode(code, graph)
                    }
                }
            },
            className: 'toolbar edges'
        })
    ]
}