import {defineConfig} from 'vite'
import analyze from 'rollup-plugin-visualizer'
import * as path from 'path'

export default defineConfig({
    plugins: [
        analyze()
    ],
    build: {
        outDir: path.resolve(__dirname, '..', 'static', 'graph'),
        lib: {
            entry: path.resolve(__dirname, 'src', 'main.ts'),
            name: 'openscopNetwork',
            filename: (format) => `openscop-network.${format}.js`
        },
        rollupOptions: {
            // make sure to externalize deps that shouldn't be bundled
            // into your library
            external: [],
            output: {
                // Provide global variables to use in the UMD build
                // for externalized deps
                globals: {}
            }
        }
    },
})