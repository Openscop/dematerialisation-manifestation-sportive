# coding: utf-8
from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.utils.http import urlencode
from django.shortcuts import render, redirect
from django.views.generic import CreateView, UpdateView, View
from django.contrib import messages

from core.util.permissions import require_type_organisation
from structure.models.service import ServiceConsulte, ServiceConsulteInteraction, ServiceInstructeur, Federation
from structure.models.organisation import Organisation
from structure.form.serviceconsulte import ServiceConsulteInteractionForm


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteInteractionCreateView(CreateView):
    """Crée une interraction"""

    model = ServiceConsulteInteraction
    template_name = 'structure/serviceconsulte_form.html'
    form_class = ServiceConsulteInteractionForm

    def dispatch(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=kwargs["service_id"])
        is_admin = bool((self.request.session['o_support'])
                        or (self.request.organisation in organisation.get_family_member_qs().filter(
            admin_service_famille_b=True))
                        or (self.request.user.has_group(
            "Administrateurs d\'instance") and organisation.instance_fk == self.request.user.default_instance))
        if not is_admin:
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(self.kwargs)
        return kwargs

    def redirect_params(self, url, params=None):
        response = redirect(url)
        if params:
            query_string = urlencode({'service_id': params})
            response['Location'] += '?' + query_string
        return response

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Interaction crée avec succès')
        return self.redirect_params('structure:administration_des_services', self.kwargs['service_id']).url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteInteractionUpdateView(UpdateView):
    """Édit une interaction"""

    model = ServiceConsulteInteraction
    template_name = 'structure/serviceconsulte_form.html'
    form_class = ServiceConsulteInteractionForm

    def dispatch(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=kwargs["service_id"])
        is_admin = bool((self.request.session['o_support'])
                        or (self.request.organisation in organisation.get_family_member_qs().filter(
            admin_service_famille_b=True))
                        or (self.request.user.has_group(
            "Administrateurs d\'instance") and organisation.instance_fk == self.request.user.default_instance))
        if not is_admin:
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(self.kwargs)
        return kwargs

    def redirect_params(self, url, params=None):
        response = redirect(url)
        if params:
            query_string = urlencode({'service_id': params})
            response['Location'] += '?' + query_string
        return response

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Interaction éditée avec succès')
        return self.redirect_params('structure:administration_des_services', self.kwargs['service_id']).url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class DeleteInteraction(View):
    """Supprime une interaction"""

    def dispatch(self, request, *args, **kwargs):
        object_id = self.request.GET.get('object_id', None)
        interaction = ServiceConsulteInteraction.objects.get(pk=object_id)
        organisation = interaction.service_entrant_fk
        is_admin = bool((self.request.session['o_support'])
                        or (self.request.organisation in organisation.get_family_member_qs().filter(
            admin_service_famille_b=True))
                        or (self.request.user.has_group(
            "Administrateurs d\'instance") and organisation.instance_fk == self.request.user.default_instance))
        if not is_admin:
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):

        object_id = self.request.GET.get('object_id', None)
        interaction = ServiceConsulteInteraction.objects.get(pk=object_id)
        interaction.delete()
        message = "Interaction supprimée avec succès"
        return HttpResponse(message)
