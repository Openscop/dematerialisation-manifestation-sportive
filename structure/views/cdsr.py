import os

from django.views.generic import DetailView, CreateView, UpdateView, ListView, View
from django.views.generic.detail import SingleObjectMixin
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.http.response import HttpResponse
from django.conf import settings

from ..models.cdsr import ReunionCDSR, PresentationCDSR
from ..form.cdsr import DateReunionForm, HeurePresentationForm, ConvocationReunionForm, ConvocationPresentationForm
from structure.models import ServiceInstructeur, ServiceConsulte, ServiceCDSR
from instructions.models import Instruction, Avis
from instructions.decorators import verifier_secteur_instruction, verifier_service_avis
from core.tasks import pj_messagerie
from core.util.permissions import require_type_organisation
from core.util.permissions import login_requis
from messagerie.models import Message


@method_decorator(require_type_organisation([ServiceInstructeur]), name='dispatch')
class ServiceCDSRAddMemberAjaxView(SingleObjectMixin, View):
    """ Ajouter un membre permanent à un service CDSR """

    model = ServiceCDSR

    @method_decorator(login_requis())
    def dispatch(self, *args, **kwargs):
        if not self.request.user.has_group('_support'):
            if not self.request.user.groups.filter(name="Administrateurs d'instance").exists():
                return render(self.request, 'core/access_restricted.html', status=403)
            cdsr = self.get_object()
            if cdsr.instance_fk != self.request.user.get_instance():
                return render(self.request, 'core/access_restricted.html', status=403)
        return super().dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        cdsr = self.get_object()
        array_id = self.request.GET.getlist('array_id[]', None)
        for pk in array_id:
            service = ServiceConsulte.objects.get(pk=pk)
            if not service.porte_entree_avis_b:
                return HttpResponse("Ce service ne reçoit pas d'avis")
            cdsr.membres_permanents_m2m.add(service)
            history_json = cdsr.history_json
            if not history_json:
                history_json = []
            date_changement = timezone.now()
            old_data = ""
            new_data = str(service)
            history_json.append({"champ": "membres_permanents_m2m",
                                 "champ_verbose": "membres permanents",
                                 "user": self.request.user.__str__(),
                                 "olddata": old_data,
                                 "newdata": new_data,
                                 "date": str(date_changement),
                                 "date_timestamp": str(date_changement.timestamp()),
                                 "type": "str"})
            cdsr.history_json = history_json
            cdsr.save()
        return HttpResponse(True)


@method_decorator(require_type_organisation([ServiceInstructeur]), name='dispatch')
class ServiceCDSRDeleteMemberAjaxView(SingleObjectMixin, View):
    """ Retirer un membre permanent à un service CDSR """

    model = ServiceCDSR

    @method_decorator(login_requis())
    def dispatch(self, *args, **kwargs):
        if not self.request.user.has_group('_support'):
            if not self.request.user.groups.filter(name="Administrateurs d'instance").exists():
                return render(self.request, 'core/access_restricted.html', status=403)
            cdsr = self.get_object()
            if cdsr.instance_fk != self.request.user.get_instance():
                return render(self.request, 'core/access_restricted.html', status=403)
        return super().dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        cdsr = self.get_object()
        membre_id = self.request.GET.get('organisation_id', None)
        service = ServiceConsulte.objects.get(pk=membre_id)
        cdsr.membres_permanents_m2m.remove(service)
        history_json = cdsr.history_json
        if not history_json:
            history_json = []
        date_changement = timezone.now()
        new_data = ""
        old_data = str(service)
        history_json.append({"champ": "membres_permanents_m2m",
                             "champ_verbose": "membres permanents",
                             "user": self.request.user.__str__(),
                             "olddata": old_data,
                             "newdata": new_data,
                             "date": str(date_changement),
                             "date_timestamp": str(date_changement.timestamp()),
                             "type": "str"})
        cdsr.history_json = history_json
        cdsr.save()
        return HttpResponse("Membre du CDSR supprimé")


class ReunionCDSRListView(ListView):
    """ Vue de la liste des réunions CDSR """

    model = ReunionCDSR
    template_name = "structure/reunionCDSR_list.html"

    @method_decorator(require_type_organisation([ServiceInstructeur]))
    def dispatch(self, *args, **kwargs):
        if self.request.organisation and self.request.organisation.is_mairie():
            return render(self.request, 'core/access_restricted.html', status=403)
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        instance = self.request.organisation.instance_fk
        reunions_avenir = ReunionCDSR.objects.filter(date__gte=timezone.now().date(), service_cdsr__instance_fk=instance).order_by('date')
        for reunion in reunions_avenir:
            reunion.color = "info"
        derniere_reunion_passee = None
        if ReunionCDSR.objects.filter(date__lt=timezone.now().date(), service_cdsr__instance_fk=instance).exists():
            derniere_reunion_passee = ReunionCDSR.objects.filter(
                date__lt=timezone.now().date(), service_cdsr__instance_fk=instance).order_by('date').last()
            derniere_reunion_passee.color = "secondary"
        reunions_adefinir = ReunionCDSR.objects.filter(date__isnull=True, service_cdsr__instance_fk=instance).order_by('pk')
        for reunion in reunions_adefinir:
            reunion.color = "warning"
        reunions = [derniere_reunion_passee] if derniere_reunion_passee else []
        reunions += list(reunions_avenir) + list(reunions_adefinir)
        context['reunions'] = reunions
        if self.request.GET.get("reunion_id"):
            context['activ_tab'] = int(self.request.GET["reunion_id"])
        if "activ_tab" not in context and len(reunions) > 1:
            context['activ_tab'] = reunions[1].pk
        return context


class ReunionCDSRDetailView(DetailView):
    """ Vue de détail d'une réunion CDSR """

    model = ReunionCDSR
    template_name = "structure/reunionCDSR_detail.html"

    @method_decorator(require_type_organisation([ServiceInstructeur]))
    def dispatch(self, *args, **kwargs):
        if self.request.organisation and self.request.organisation.is_mairie():
            return render(self.request, 'core/access_restricted.html', status=403)
        reunion = self.get_object()
        if reunion.service_cdsr.instance_fk != self.request.user.get_instance():
            return render(self.request, 'core/access_restricted.html', status=403)
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        reunion = self.get_object()
        reunion.editer = True
        if reunion.convocations_envoyes:
            reunion.editer = False
        else:
            for presentation in reunion.presentations.all():
                if presentation.convocations_envoyes:
                    reunion.editer = False
                    break
        presentations = reunion.presentations.all().order_by('heure')
        context['reunion'] = reunion
        context['presentations'] = presentations
        return context


class ReunionCDSRCreateView(CreateView):
    """ Vue de création d'une réunion CDSR """

    model = ReunionCDSR
    form_class = DateReunionForm
    template_name = 'instructions/date_reponse_form.html'

    @method_decorator(require_type_organisation([ServiceInstructeur]))
    def dispatch(self, *args, **kwargs):
        if self.request.organisation and self.request.organisation.is_mairie():
            return render(self.request, 'core/access_restricted.html', status=403)
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        reunion = form.save(commit=False)
        instruction_id = 0
        if self.request.META['HTTP_REFERER'].split('/')[-2].isdecimal():
            retour = ""
            instruction_id = int(self.request.META['HTTP_REFERER'].split('/')[-2])
            if not Instruction.objects.filter(pk=instruction_id).exists():
                erreur = "Instruction introuvable"
                return HttpResponse(erreur, status=400)
            instruction = Instruction.objects.get(pk=instruction_id)
            if reunion.date and reunion.date > instruction.manif.date_debut.date():
                erreur = "La date sélectionnée dépasse la date de début de la manifestation"
                return HttpResponse(erreur, status=400)
        else:
            retour = reverse('structure:reunionCDSR_list')
        instance = self.request.organisation.instance_fk
        service_cdsr = ServiceCDSR.objects.get(instance_fk=instance)
        reunion.service_cdsr = service_cdsr
        permanents_json = []
        for membre in service_cdsr.membres_permanents_m2m.all():
            permanents_json.append({'membre': membre.pk, 'confirmé': False})
        reunion.permanents_json = permanents_json
        if instruction_id:
            reunion.nb_dossier_i = 1
        reunion.save()
        if instruction_id:
            reunion.refresh_from_db()
            presentation = instruction.presentations.last()
            presentation.reunion_cdsr_fk = reunion
            presentation.creer_avis_membres_permanents(self.request)
            presentation.save()
        return HttpResponse(retour)

    def form_invalid(self, form):
        erreur = form.errors['date'][0]
        return HttpResponse(erreur, status=400)


class ReunionCDSRUpdateView(UpdateView):
    """ Vue de modification d'une réunion CDSR - date ou nom_s """

    model = ReunionCDSR
    form_class = DateReunionForm
    template_name = 'instructions/date_reponse_form.html'

    @method_decorator(require_type_organisation([ServiceInstructeur]))
    def dispatch(self, *args, **kwargs):
        if self.request.organisation and self.request.organisation.is_mairie():
            return render(self.request, 'core/access_restricted.html', status=403)
        reunion = self.get_object()
        if reunion.service_cdsr.instance_fk != self.request.user.get_instance():
            return render(self.request, 'core/access_restricted.html', status=403)
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        reunion = form.save(commit=False)
        if reunion.date:
            if reunion.convocations_envoyes:
                erreur = f"La convocation a été envoyé pour la réunion !"
                return HttpResponse(erreur, status=400)
            if reunion.presentations.all():
                for presentation in reunion.presentations.all():
                    if presentation.convocations_envoyes:
                        erreur = f"Les convocations ont été envoyées pour la manifestation {presentation.instruction_fk}"
                        return HttpResponse(erreur, status=400)
                    if reunion.date > presentation.instruction_fk.manif.date_debut.date():
                        erreur = f"La date sélectionnée dépasse la date de début de la manifestation {presentation.instruction_fk}"
                        return HttpResponse(erreur, status=400)
        history_json = reunion.history_json
        if not history_json:
            history_json = []
        if form.changed_data:
            for changed_data in form.changed_data:
                old_data = form.initial.get(changed_data)
                new_data = form.cleaned_data[changed_data]
                date_changement = timezone.now().strftime('%d/%m/%Y %H:%M')
                if str(type(new_data).__name__) == "date":
                    old_data = old_data.strftime("%d/%m/%Y") if old_data else ""
                    new_data = new_data.strftime("%d/%m/%Y") if new_data else ""
                else:
                    old_data = str(old_data)
                    new_data = str(new_data)
                history_json.append({"champ": changed_data, "olddata": old_data, "newdata": new_data, "date": date_changement,
                                     "user": self.request.user.__str__()})
        reunion.history_json = history_json
        reunion.save()
        return HttpResponse(reverse('structure:reunionCDSR_list') + f'?reunion_id={str(reunion.pk)}')

    def form_invalid(self, form):
        erreur = form.errors['date'][0]
        return HttpResponse(erreur, status=400)


@method_decorator(require_type_organisation([ServiceInstructeur]), name='dispatch')
class ReunionCDSRConvoquerView(UpdateView):
    """ Convoquer les membres permanents d'une réunion CDSR """

    model = ReunionCDSR
    form_class = ConvocationReunionForm
    template_name = 'structure/cdsr_form.html'

    @method_decorator(require_type_organisation([ServiceInstructeur]))
    def dispatch(self, *args, **kwargs):
        if self.request.organisation and self.request.organisation.is_mairie():
            return render(self.request, 'core/access_restricted.html', status=403)
        reunion = self.get_object()
        if reunion.service_cdsr.instance_fk != self.request.user.get_instance():
            return render(self.request, 'core/access_restricted.html', status=403)
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        reunion = form.instance
        fichier_joint = form.cleaned_data['fichier']
        if fichier_joint:
            if not os.path.exists(settings.MEDIA_ROOT + 'tmp'):
                os.makedirs(settings.MEDIA_ROOT + "tmp")
            from django.core.files.storage import FileSystemStorage
            fs = FileSystemStorage(location=f'{settings.MEDIA_ROOT}tmp/')
            nom_fichier = f"{settings.MEDIA_ROOT}tmp/{fs.save(fichier_joint.name, fichier_joint)}"
        else:
            nom_fichier = ""
        if reunion.presentations.all() and reunion.envoi_convocation_possible:
            # Envoyer convocation
            message = Message.objects.creer_et_envoyer(
                'convocation', [self.request.user, self.request.organisation],
                reunion.get_membres_permanents_avec_service(), 'Convocation en réunion CDSR',
                'Bonjour,<br><br>'
                f'Vous êtes convoqué à la réunion CDSR du {reunion.date.strftime("%d/%m/%Y")} '
                f"pour la présentation des dossiers d'instruction.<br>{form.cleaned_data['precision']}",
                objet_lie_nature="reunion", objet_lie_pk=reunion.pk, fichier=nom_fichier)
            if fichier_joint:
                pj_messagerie.delay(pk_manif=None, pk_msg=message.pk, cdsr=True)
            # Modifier les dates limites des avis des membres permanents
            for presentation in reunion.presentations.all():
                instruction = presentation.instruction_fk
                date_limite = timezone.make_aware(timezone.now().combine(presentation.reunion_cdsr_fk.date, presentation.heure))
                for service in reunion.get_membres_permanents():
                    avis = instruction.avis.get(service_consulte_fk=service)
                    avis.date_limite_reponse_dt = date_limite
                    avis.save()
            reunion.convocations_envoyes = timezone.now()
            reunion.save()
            return redirect(reverse('structure:reunionCDSR_list') + f'?reunion_id={str(reunion.pk)}')
        else:
            return render(self.request, "core/access_restricted.html",
                          {'message': "L'envoi des convocations n'est pas possible !"},
                          status=403)


@method_decorator(require_type_organisation([ServiceInstructeur]), name='dispatch')
class ReunionCDSRDeleteView(SingleObjectMixin, View):
    """ Suppression d'une réunion CDSR """

    model = ReunionCDSR

    @method_decorator(require_type_organisation([ServiceInstructeur]))
    def dispatch(self, *args, **kwargs):
        if self.request.organisation and self.request.organisation.is_mairie():
            return render(self.request, 'core/access_restricted.html', status=403)
        reunion = self.get_object()
        if reunion.service_cdsr.instance_fk != self.request.user.get_instance():
            return render(self.request, 'core/access_restricted.html', status=403)
        return super().dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        reunion = self.get_object()
        if not reunion.presentations.all():
            reunion.delete()
            return redirect(reverse('structure:reunionCDSR_list'))
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Vous ne pouvez pas supprimer cette réunion. Des présentation existent !"},
                          status=403)


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class PresentationCDSRDateSetView(SingleObjectMixin, View):
    """ Ajouter le lien de la présentation avec une réunion """
    model = PresentationCDSR

    def get(self, request, *args, **kwargs):
        presentation = self.get_object()
        reunion = get_object_or_404(ReunionCDSR, pk=request.GET.get('reunion'))
        if not presentation.convocations_envoyes:
            reunion.nb_dossier_i += 1
            reunion.save()
            presentation.reunion_cdsr_fk = reunion
            presentation.creer_avis_membres_permanents(request)
            presentation.save()
            return HttpResponse()
        else:
            erreur = "Vous ne pouvez plus changer la date de cette présentation. Les convocations sont envoyées !"
            return HttpResponse(erreur, status=400)


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class PresentationCDSRDateResetView(SingleObjectMixin, View):
    """ Supprimer le lien de la présentation avec une réunion """
    model = PresentationCDSR

    def get(self, request, *args, **kwargs):
        presentation = self.get_object()
        reunion = presentation.reunion_cdsr_fk
        reunion.nb_dossier_i -= 1
        reunion.save()
        instruction = presentation.instruction_fk
        if not presentation.convocations_envoyes:
            presentation.reunion_cdsr_fk = None
            presentation.heure = None
            presentation.save()
            if self.request.META['HTTP_REFERER'].split('/')[-2] == "liste":
                return redirect(reverse('structure:reunionCDSR_list'))
            return redirect(instruction.get_absolute_url())
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Vous ne pouvez plus changer la date de cette présentation. Les convocations sont envoyées !"},
                          status=403)


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class PresentationCDSRConvoquerView(UpdateView):
    """ Envoyer la convocation des membres invités à la présentation du dossier pour la réunion CDSR """

    model = PresentationCDSR
    form_class = ConvocationPresentationForm
    template_name = 'structure/cdsr_form.html'

    def form_valid(self, form):
        presentation = form.instance
        instruction = presentation.instruction_fk
        fichier_joint = form.cleaned_data['fichier']
        if fichier_joint:
            if not os.path.exists(settings.MEDIA_ROOT + 'tmp'):
                os.makedirs(settings.MEDIA_ROOT + "tmp")
            from django.core.files.storage import FileSystemStorage
            fs = FileSystemStorage(location=f'{settings.MEDIA_ROOT}tmp/')
            nom_fichier = f"{settings.MEDIA_ROOT}tmp/{fs.save(fichier_joint.name, fichier_joint)}"
        else:
            nom_fichier = ""
        if not presentation.convocations_envoyes and presentation.heure and presentation.reunion_cdsr_fk.date:
            date_limite = timezone.make_aware(timezone.now().combine(presentation.reunion_cdsr_fk.date, presentation.heure))
            # Envoyer convocation
            message = Message.objects.creer_et_envoyer(
                'convocation', [self.request.user, self.request.organisation],
                presentation.get_membres_invites_avec_service(), 'Convocation en réunion CDSR',
                'Bonjour,<br><br>'
                f'Vous êtes convoqué à la réunion CDSR du {presentation.reunion_cdsr_fk.date.strftime("%d/%m/%Y")}'
                f' pour la présentation du dossier {presentation.instruction_fk} à {presentation.heure.strftime("%Hh%M")}.'
                f"<br>{form.cleaned_data['precision']}",
                manifestation_liee=instruction.manif, fichier=nom_fichier,
                objet_lie_nature="presentation", objet_lie_pk=presentation.pk)
            if fichier_joint:
                pj_messagerie.delay(pk_manif=None, pk_msg=message.pk, cdsr=True)
            # Creer les avis des membres invités avec date limite à la date de la réunion
            for service in presentation.get_membres_invites():
                if service.is_structure_organisatrice():
                    continue
                if not Avis.objects.filter(service_consulte_fk=service).filter(instruction=instruction).exists():
                    historique_circuit_json = [{"entrant": self.request.organisation.pk,
                                                "sortant": service.pk,
                                                "type": "creer",
                                                "date": str(timezone.now())}]
                    avis = Avis.objects.create(instruction=instruction, service_consulte_fk=service,
                                               service_consulte_origine_fk=service, service_demandeur_fk=self.request.organisation,
                                               historique_circuit_json=historique_circuit_json)
                    # Modification de la date générée automatiquement
                    avis.date_limite_reponse_dt = date_limite
                    avis.save()
                    avis.notifier_creation_avis(origine=[self.request.user, self.request.organisation], agents=service.get_users_list())
            presentation.convocations_envoyes = timezone.now()
            presentation.save()
            if "instruction" in self.request.GET.get('path'):
                return redirect(instruction.get_absolute_url())
            return redirect(reverse('structure:reunionCDSR_list') + f'?reunion_id={str(presentation.reunion_cdsr_fk.pk)}')
        return render(self.request, "core/access_restricted.html",
                      {'message': "Les convocations ne peuvent pas être envoyées, "
                                  "la date de la réunion ou l'heure de la présentation ne sont pas connus!"},
                      status=403)


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class PresentationCDSRDeleteView(SingleObjectMixin, View):
    """ Suppression d'une présentation CDSR """

    model = PresentationCDSR

    def get(self, request, *args, **kwargs):
        presentation = self.get_object()
        if presentation.reunion_cdsr_fk:
            presentation.reunion_cdsr_fk.nb_dossier_i -= 1
            presentation.reunion_cdsr_fk.save()
        instruction = presentation.instruction_fk
        if not presentation.convocations_envoyes:
            presentation.avis_o2o.delete()
            presentation.delete()
            if self.request.META['HTTP_REFERER'].split('/')[-2] == "liste":
                return redirect(reverse('structure:reunionCDSR_list'))
            return redirect(instruction.get_absolute_url())
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Vous ne pouvez plus supprimer cette présentation. Les convocations sont envoyées !"},
                          status=403)


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class PresentationCDSRInviteAddAjaxView(SingleObjectMixin, View):
    """ Ajouter un membre invité à une présentation """

    model = PresentationCDSR

    def get(self, request, *args, **kwargs):
        presentation = self.get_object()
        array_id = self.request.GET.getlist('array_id[]', None)
        if not presentation.convocations_envoyes:
            for pk in array_id:
                service = ServiceConsulte.objects.get(pk=pk)
                presentation.invites_json.append({'membre': service.pk, 'confirmé': False})
            presentation.save()
            return HttpResponse(True)
        else:
            return HttpResponse(False)


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class PresentationCDSRInviteDeleteAjaxView(SingleObjectMixin, View):
    """ Retirer un membre invité à une présentation """

    model = PresentationCDSR

    def get(self, request, *args, **kwargs):
        presentation = self.get_object()
        membre_id = int(self.request.GET.get('membre_id'))
        if not presentation.convocations_envoyes:
            for idx, item in enumerate(presentation.invites_json):
                if presentation.invites_json[idx]['membre'] == membre_id:
                    presentation.invites_json.pop(idx)
                    break
            presentation.save()
            return HttpResponse(True)
        else:
            return HttpResponse(False)


@method_decorator(verifier_service_avis(), name='dispatch')
class PresentationCDSRConfirmerView(SingleObjectMixin, View):
    """ Confirmer sa présence à une présentation CDSR """

    model = PresentationCDSR

    def get(self, request, *args, **kwargs):
        presentation = self.get_object()
        membre_id = int(self.request.GET.get('membre'))
        invit_json = presentation.invites_json
        if any([True if item['membre'] == membre_id else False for item in invit_json]):
            if presentation.convocations_envoyes:
                for idx, item in enumerate(invit_json):
                    if invit_json[idx]['membre'] == membre_id:
                        invit_json[idx]['confirmé'] = True
                presentation.save()
                return redirect(request.META['HTTP_REFERER'])
        if presentation.reunion_cdsr_fk:
            perm_json = presentation.reunion_cdsr_fk.permanents_json
            if any([True if item['membre'] == membre_id else False for item in perm_json]):
                if presentation.reunion_cdsr_fk.convocations_envoyes:
                    for idx, item in enumerate(perm_json):
                        if perm_json[idx]['membre'] == membre_id:
                            perm_json[idx]['confirmé'] = True
                    presentation.reunion_cdsr_fk.save()
                    return redirect(request.META['HTTP_REFERER'])
        return render(request, "core/access_restricted.html",
                      {'message': "Vous ne pouvez pas confirmer votre présence car les convocations ne sont pas envoyées !"},
                      status=403)


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class PresentationCDSRHeureSetView(UpdateView):
    """ Vue de création d'une réunion CDSR """

    model = PresentationCDSR
    form_class = HeurePresentationForm
    template_name = 'instructions/date_reponse_form.html'

    def form_valid(self, form):
        presentation = form.save(commit=False)
        presentation.save()
        return HttpResponse()

    def form_invalid(self, form):
        erreur = form.errors['heure'][0]
        return HttpResponse(erreur, status=400)
