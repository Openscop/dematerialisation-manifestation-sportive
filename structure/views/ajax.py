# coding: utf-8
import json
import dns.resolver
from hashlib import sha256
import unidecode
from datetime import timedelta

from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.shortcuts import get_object_or_404, render
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.conf import settings

from clever_selects.views import View
from rest_framework import status
from post_office import mail


from administrative_division.models.region import Region
from evenements.models import Manif
from instructions.models import Avis, Instruction, AutorisationAcces
from core.models import Instance, User
from core.util.permissions import login_requis
from structure.models.service import CategorieService, ServiceConsulte, ServiceInstructeur
from structure.models.organisation import Organisation, StructureOrganisatrice, InvitationOrganisation
from structure.models.cdsr import ServiceCDSR


def demande_deja_envoyer(instruction, action, model=None, acces=[], user=None):
    """Renvoie True si un model à déja fait l'objet d'une demande d'avis ou d'accès"""
    if action == "consultation":
        return model in [acces.organisation for acces in acces]
    elif instruction:
        return Avis.objects.filter(service_consulte_fk=model).filter(instruction=instruction).exists()
    elif action == "gestion-organisation-utilisateur":
        return model in user.organisation_m2m.all()
    elif action == "gestion-membre-cdsr":
        return model in user.membres_permanents_m2m.all()
    return False


class CategorieListAJAXView(View):
    """Retourne les listes des catégories selon l'action du sélecteur de service"""

    def get(self, request, *args,  **kwargs):
        action = self.request.GET.get('action', None)
        secteur = self.request.GET.get('secteur', None)
        if action == "selection":
            categories = list(CategorieService.objects.all().values_list('pk', 'nom_s'))
            if (1, "Manifestation sportive") in categories:
                categories.remove((1, "Manifestation sportive"))
            if (2, "_Support") in categories:
                categories.remove((2, "_Support"))
            return HttpResponse(json.dumps(categories))
        elif self.request.user.is_authenticated:
            user_organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
            o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
            if secteur and secteur.split('_')[0] == "instance":
                instance = Instance.objects.filter(pk=secteur.split('_')[1]).first()
            categories = []
            # Si national ou régional on prend seulement les catégories ayant des services
            if action == "get_national":
                services_national = ServiceConsulte.objects.filter(national_b=True)
                for service_national in services_national:
                    if (service_national.type_service_fk.categorie_fk.pk, service_national.type_service_fk.categorie_fk.nom_s) not in categories:
                        categories += [(service_national.type_service_fk.categorie_fk.pk, service_national.type_service_fk.categorie_fk.nom_s)]
            elif action == "get_regional" and secteur.split('_')[0] == "region":
                region = Region.objects.get(pk=secteur.split('_')[1])
                services_regional = ServiceConsulte.objects.filter(region_m2m=region)
                for service_regional in services_regional:
                    if (service_regional.type_service_fk.categorie_fk.pk, service_regional.type_service_fk.categorie_fk.nom_s) not in categories:
                        categories += [(service_regional.type_service_fk.categorie_fk.pk, service_regional.type_service_fk.categorie_fk.nom_s)]
            # Si c'est un accés en lecteur complet on prend la catégorie de l'organisation de l'utilisateur
            elif action == "consultation" and self.request.GET.get('acces') and self.request.GET.get('acces') == "complet":
                categories = [(user_organisation.type_service_fk.categorie_fk.pk, user_organisation.type_service_fk.categorie_fk.nom_s)]
                return HttpResponse(json.dumps(categories))
            elif action == "carnet_messagerie" and self.request.GET.get('manif_pk'):
                manif_pk = self.request.GET.get('manif_pk')
                admin_tech = request.user.groups.filter(name="Administrateurs techniques").exists()
                admin_instance = request.user.groups.filter(name="Administrateurs d'instance").exists()
                if not user_organisation.is_service_instructeur() and not admin_instance and not admin_tech:
                    categories = [(f'instructeurs_{manif_pk}', 'Instructeurs de cette manifestation')]
                    categories += [(f'orga_{manif_pk}', 'Organisateur de cette manifestation')]
                else:
                    categories = [(f'orga_{manif_pk}', 'Organisateur de cette manifestation')]
                categories += list(CategorieService.objects.all().values_list('pk', 'nom_s'))
            elif action == "carnet_messagerie":
                categories = [
                    ("orga_new", "Organisateurs actifs depuis un an"),
                    ("orga_old", "Organisateurs inactifs depuis un an"),
                ]
                categories += list(CategorieService.objects.all().values_list('pk', 'nom_s'))
            # Si c'est une demande d'avis on prend les services qui sont portes d'entrés
            elif action == "demande_avis":
                services_porte_entree_qs = ServiceConsulte.objects.filter(
                    porte_entree_avis_b=True, instance_fk=instance).order_by('type_service_fk__categorie_fk') | \
                                           ServiceConsulte.objects.filter(
                   porte_entree_avis_b=True, departement_m2m=instance.departement).order_by('type_service_fk__categorie_fk') | \
                                           ServiceConsulte.objects.filter(
                   porte_entree_avis_b=True, region_m2m=instance.departement.region).order_by('type_service_fk__categorie_fk')
                for service in services_porte_entree_qs:
                    if (service.type_service_fk.categorie_fk.pk, service.type_service_fk.categorie_fk.nom_s) not in categories:
                        categories += [(service.type_service_fk.categorie_fk.pk, service.type_service_fk.categorie_fk.nom_s)]
            elif action == "gestion-organisation-utilisateur":
                # Si admin d'instance ou support on prend toutes les catégories
                if self.request.user.has_group("Administrateurs d\'instance") or o_support in request.user.organisation_m2m.all():
                    categories = list(CategorieService.objects.all().values_list('pk', 'nom_s'))
                    if o_support in request.user.organisation_m2m.all():
                        categories.append(("structure", "Organisateurs"))
                # Sinon on prend la catégorie de l'organisation de l'utilisateur
                else:
                    categories.append(
                        (user_organisation.type_service_fk.categorie_fk.pk, user_organisation.type_service_fk.categorie_fk.nom_s))
            elif action == "administration" and (self.request.session['o_support'] or self.request.user.has_group("Administrateurs d\'instance")):
                categories = list(CategorieService.objects.all().values_list('pk', 'nom_s'))
                categories.append(("structure", "Organisateurs"))
                categories.append(("0", "Tous les services"))
            else:
                categories = list(CategorieService.objects.all().values_list('pk', 'nom_s'))
                categories.append(("structure", "Organisateurs"))
            # Retirer les categories support et manif sport de la liste
            if (1, "Manifestation sportive") in categories:
                categories.remove((1, "Manifestation sportive"))
            if (2, "_Support") in categories:
                categories.remove((2, "_Support"))
            return HttpResponse(json.dumps(categories))
        else:
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)


@method_decorator(login_requis(), name='dispatch')
class AgentListAjaxView(View):
    """Retourne la liste des agents d'une organisation"""
    def get(self, request):

        data = {}
        object_id = self.request.GET.get('object_id', None)
        organisation = Organisation.objects.get(pk=object_id)
        for user in organisation.get_users_qs():
            data[user.id] = (user.get_full_name(), organisation.pk)

        return JsonResponse(data, safe=False)


@method_decorator(login_requis(), name='dispatch')
class SousServiceListAjaxView(View):

    """Retourne la liste des sous-services d'un service"""
    def get(self, request):
        data = []
        service_id = self.request.GET.get('service_id', None)
        object_id = self.request.GET.get('object_id', None)
        action = self.request.GET.get('action', None)
        service = Organisation.objects.get(pk=service_id)
        show_old = self.request.GET.get('show_old', None)
        user_organisation = Organisation.objects.filter(pk=request.session['service_pk']).first()
        instruction, user, acces = None, None, []
        if action == "demande_avis":
            # Recupére l'instruction si un pk est donné
            instruction = Instruction.objects.filter(pk=object_id).first()

        elif action == "consultation":
            if user_organisation.is_structure_organisatrice():
                manif = get_object_or_404(Manif, pk=object_id)
                acces = AutorisationAcces.objects.filter(manif=manif, date_revocation__isnull=True)
            else:
                instruction = Instruction.objects.filter(pk=object_id).first()
                acces = AutorisationAcces.objects.filter(manif=instruction.manif, date_revocation__isnull=True)

        elif action == "gestion-organisation-utilisateur":
            user = User.objects.filter(pk=object_id).first()
        elif action == "gestion-membre-cdsr":
            user = ServiceCDSR.objects.filter(pk=object_id).first()

        if action == "demande_avis":
            sous_services = service.get_all_children_qs().filter(
                Q(porte_entree_avis_b=True, service_parent_fk=service) |
                Q(service_parent_fk__porte_entree_avis_b=False, porte_entree_avis_b=True))
        elif action == "consultation" and user_organisation.is_structure_organisatrice():
            sous_services = service.get_all_children_qs()\
                                   .filter(Q(autorise_acces_consult_organisateur_b=True, service_parent_fk=service) |
                                           Q(service_parent_fk__autorise_acces_consult_organisateur_b=False,
                                             autorise_acces_consult_organisateur_b=True))
        else:
            sous_services = service.get_children_qs()
        for sous_service in sous_services:
            if action == "demande_avis":
                has_children = sous_service.get_all_children_qs().filter(porte_entree_avis_b=True).exists()
            else:
                has_children = sous_service.get_children_qs().count() > 0
            service = {
                'id': sous_service.id,
                'name': sous_service.__str__(),
                "service": "Organisation",
                "actif": sous_service.is_active_b,
                "icones":
                    {
                        'admin_service_famille_b': sous_service.admin_service_famille_b,
                        'admin_utilisateurs_subalternes_b': sous_service.admin_utilisateurs_subalternes_b,
                        'porte_entree_avis_b': sous_service.porte_entree_avis_b,
                        'autorise_rendre_avis_b': sous_service.autorise_rendre_avis_b,
                        'autorise_rendre_preavis_a_service_avis_b': sous_service.autorise_rendre_preavis_a_service_avis_b,
                        'autorise_acces_consult_organisateur_b': sous_service.autorise_acces_consult_organisateur_b,
                        'autorise_mandater_avis_famille_b': sous_service.autorise_mandater_avis_famille_b,
                        'autorise_interroge_preavis_famille_b': sous_service.autorise_interroge_preavis_famille_b,
                    },
                "agents_count":
                    {
                        'agents_actif': sous_service.get_users_qs().filter(is_active=True).count(),
                        'agents_inactif': sous_service.get_users_qs().filter(is_active=False).count(),
                    },
                "has_children": has_children
            }
            if show_old == 'true' and demande_deja_envoyer(instruction=instruction, action=action, model=sous_service, acces=acces, user=user):
                service['deja_fait'] = True
            data.append(service)
        return JsonResponse(data, safe=False)


class ServiceListAJAXView(View):

    def get(self, request):
        """ Renvoyer la liste des services selon une catégorie sélectionnée"""

        secteur = self.request.GET.get('secteur', None)
        categorie = self.request.GET.get('categorie', None)
        object_id = self.request.GET.get('object_id', None)
        action = self.request.GET.get('action', None)
        show_old = self.request.GET.get('show_old', None)
        show_sous_service = self.request.GET.get('show_sous_service', None)
        manif_pk = self.request.GET.get('manif', None)
        user_organisation = Organisation.objects.filter(pk=request.session['service_pk']).first()
        # Retourne une erreur si une de ses variables n'est pas rensseigner
        if not secteur or not categorie or not action:
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        instance = None
        filters = {}

        if secteur.split('_')[0] == "instance":
            # Crée une liste de filtre pour tous les niveaux géographique
            instance = Instance.objects.filter(pk=secteur.split('_')[1]).first()
            filters = [
                {'instance_fk': instance},
                {'commune_m2m__arrondissement__departement': instance.departement},
                {'arrondissement_m2m__departement': instance.departement},
                {'departement_m2m': instance.departement},
                {'region_m2m__departement': instance.departement},
                {'national_b': True},
            ]
        elif secteur.split('_')[0] == "region":
            region = Region.objects.filter(pk=secteur.split('_')[1]).first()
            filters = {
                'region_m2m': region
            }
        elif secteur == "national":
            filters = {
                'national_b': True
            }

        categorie_list, result, orgas = [], [], []
        if filters:
            # Si on veut une list d'instructeur
            if categorie.split('_')[0] == 'instructeurs':
                service = Organisation.objects.filter(pk=self.request.session['service_pk']).first()
                manif = get_object_or_404(Manif, pk=manif_pk)
                avis_encours = Avis.objects.filter(instruction__manif=manif)
                acces_encours_bool = AutorisationAcces.objects.filter(manif=manif,
                                                                      date_revocation__isnull=True,
                                                                      organisation=service).exists()
                is_concerne = bool(manif.instance == service.instance_fk
                                   or service in [avis.service_consulte_fk for avis in avis_encours]
                                   or service in [avis.service_consulte_history_m2m for avis in avis_encours]
                                   or service in manif.get_instruction().service_instructeur_m2m.all()
                                   or acces_encours_bool)
                if is_concerne:
                    liste = manif.get_instructeurs_manif()
                    for recipient in liste:
                        if isinstance(recipient, list):
                            if isinstance(recipient[0], User):
                                result.append({"id": recipient[0].pk,
                                               "service": recipient[1].pk,
                                               "name": str(recipient[1]) + " - " + recipient[0].get_full_name(),
                                               "data": ""})
                    return JsonResponse(result, safe=False)
            # Si on veut une liste d'organisateur
            elif categorie.split('_')[0] == 'orga':
                if categorie.split('_')[1] == "new" or categorie.split('_')[1] == "old" or categorie.split('_')[1] == "all":
                    structures_organisatrice = StructureOrganisatrice.objects.all()
                    # Si plusieur filtre selectionner
                    if type(filters) is list:
                        structures_organisatrice = structures_organisatrice.filter(
                            Q(**filters[0]) | Q(**filters[1]) | Q(**filters[2]) | Q(**filters[3])).distinct()
                    else:
                        structures_organisatrice.structures_organisatrice(**filters)
                    if categorie.split('_')[1] == "new" or categorie.split('_')[1] == "old":
                        last_year = timezone.now() - timedelta(days=360)
                        for structure in structures_organisatrice:
                            if categorie.split('_')[1] == "new":
                                orgas += structure.get_users_qs().filter(last_login__gte=last_year)
                            if categorie.split('_')[1] == "old":
                                orgas += structure.get_users_qs().filter(last_login__lt=last_year)
                    elif categorie.split('_')[1] == "all":
                        orgas = User.objects.filter(organisation_m2m__in=structures_organisatrice)
                # Sinon on veut les organisateurs d'une manifestation
                else:
                    manif = get_object_or_404(Manif, pk=manif_pk)
                    orgas = manif.structure_organisatrice_fk.get_users_list()
                for user in orgas:
                    if user.is_active:
                        result.append({"id": user.pk,
                                       "service": user.organisation_m2m.first().pk,
                                       "name": str(user.organisation_m2m.first().precision_nom_s) + ' - ' + user.get_full_name(),
                                       "data": "",
                                       })
                return JsonResponse(result, safe=False)

            elif categorie == "structure":
                result = StructureOrganisatrice.objects.all()
            elif categorie == "0" and action in ["administration", "gestion-membre-cdsr"]:
                result = ServiceConsulte.objects.filter(service_parent_fk=None).exclude(type_service_fk__nom_s='Mairie')
            else:
                categorie_objet = CategorieService.objects.get(pk=categorie)
                if action == "demande_avis":
                    result = ServiceConsulte.objects.filter(Q(type_service_fk__categorie_fk=categorie_objet,
                                                              porte_entree_avis_b=True,
                                                              service_parent_fk=None,) |
                                                            Q(type_service_fk__categorie_fk=categorie_objet,
                                                              service_parent_fk__porte_entree_avis_b=False,
                                                              porte_entree_avis_b=True,)).exclude(pk=user_organisation.pk)
                elif action == "selection":
                    result = ServiceConsulte.objects.filter(type_service_fk__categorie_fk=categorie_objet,
                                                            service_parent_fk=None, is_active_b=True)
                elif action == "consultation" and user_organisation.is_structure_organisatrice():
                    manif = get_object_or_404(Manif, pk=manif_pk)
                    result = ServiceConsulte.objects.filter(Q(type_service_fk__categorie_fk=categorie_objet,
                                                              service_parent_fk=None,
                                                              autorise_acces_consult_organisateur_b=True) |
                                                            Q(type_service_fk__categorie_fk=categorie_objet,
                                                              service_parent_fk__autorise_acces_consult_organisateur_b=False,
                                                              autorise_acces_consult_organisateur_b=True))
                    # Dans le cardre d'une manifesation hors delay on autorise l'ajout d'accès en lecture aux services instructeurs
                    if (manif.get_instance().depot_hors_delai and manif.delai_depasse() and
                            not manif.date_depassee() and not manif.en_cours_instruction()):
                        if categorie_objet.nom_s == "Services de l'État":
                            result = ServiceInstructeur.objects.filter(type_service_fk__categorie_fk=categorie_objet)
                else:
                    result = ServiceConsulte.objects.filter(type_service_fk__categorie_fk=categorie_objet,
                                                            service_parent_fk=None)
                if not action == "administration":
                    result = result.exclude(pk__in=ServiceCDSR.objects.all().values_list('id'))
            # Si plusieur filtre selectionner
            if type(filters) is list:
                if categorie == "12":
                    # Pour les fédérations délégataire on ne recupère pas les régional et national
                    result = result.filter(Q(**filters[0]) | Q(**filters[1]) | Q(**filters[2]) | Q(**filters[3])).distinct()
                else:
                    # Pour les autres on filtre sur tous les niveaux géographiques
                    result = result.filter(Q(**filters[0]) | Q(**filters[1]) | Q(**filters[2]) | Q(**filters[3]) | Q(**filters[4]) | Q(**filters[5])).distinct()
            else:
                result = result.filter(**filters)

            instruction, user, acces = None, None, []
            if action == "demande_avis":
                # Recupére l'instruction si un pk est donné
                instruction = Instruction.objects.filter(pk=object_id).first()
            elif action == "consultation":
                if user_organisation.is_structure_organisatrice():
                    manif = get_object_or_404(Manif, pk=manif_pk)
                    acces = AutorisationAcces.objects.filter(manif=manif, date_revocation__isnull=True)
                else:
                    instruction = Instruction.objects.filter(pk=object_id).first()
                    acces = AutorisationAcces.objects.filter(manif=instruction.manif, date_revocation__isnull=True)
            elif action == "gestion-organisation-utilisateur":
                user = User.objects.filter(pk=object_id).first()
            elif action == "gestion-membre-cdsr":
                user = ServiceCDSR.objects.filter(pk=object_id).first()
            for model in result:
                liste_sous_service = {}
                if hasattr(model, 'get_all_children_list') and show_sous_service == "true":
                    for sous_service in model.get_all_children_qs().filter(is_active_b=True):
                        deja_fait = False
                        if show_old == 'true' and demande_deja_envoyer(instruction, action, sous_service, acces, user):
                            deja_fait = True
                        if sous_service != model:
                            liste_sous_service[sous_service.id] = [sous_service.__str__(), 'Organisation', sous_service.pk, deja_fait]
                    liste_sous_service = sorted(liste_sous_service.items(), key=lambda x: x[1])
                if not demande_deja_envoyer(instruction, action, model, acces, user) or show_old == 'true':
                    if model.is_service_consulte() and model.type_service_fk.nom_s == 'Mairie':
                        model_name = f"{model.precision_nom_s} {model.commune_m2m.first().zip_code}"
                    else:
                        model_name = model.__str__()
                    new_model = {
                        "id": model.id,
                        "name": model_name,
                        "data": liste_sous_service,
                        "service": "Organisation",
                        "smtp_error": model.has_email_failed(),
                    }
                    if model.is_service_consulte() or model.is_service_cdsr() or model.is_federation():
                        new_model.update({"actif": model.is_active_b})
                    if hasattr(model, 'get_all_children_list') and show_sous_service == "false":
                        if action in ["administration", "gestion-organisation-utilisateur", "gestion-membre-cdsr"]:
                            if model.is_service_consulte() or model.is_service_cdsr():
                                icones = {
                                    "icones":
                                        {
                                            'admin_service_famille_b': model.admin_service_famille_b,
                                            'admin_utilisateurs_subalternes_b': model.admin_utilisateurs_subalternes_b,
                                            'porte_entree_avis_b': model.porte_entree_avis_b,
                                            'autorise_rendre_avis_b': model.autorise_rendre_avis_b,
                                            'autorise_rendre_preavis_a_service_avis_b': model.autorise_rendre_preavis_a_service_avis_b,
                                            'autorise_acces_consult_organisateur_b': model.autorise_acces_consult_organisateur_b,
                                            'autorise_mandater_avis_famille_b': model.autorise_mandater_avis_famille_b,
                                            'autorise_interroge_preavis_famille_b': model.autorise_interroge_preavis_famille_b,
                                        }
                                }
                                new_model.update(icones)
                        if action == "demande_avis":
                            has_children = {"has_children": False}
                            for service in model.get_all_children_qs():
                                if service.porte_entree_avis_b:
                                    has_children = {"has_children": True}
                        else:
                            has_children = {"has_children": bool(model.get_children_qs().count() > 0)}

                        new_model.update(has_children)
                    if model.is_service_cdsr():
                        agents = {
                            "agents_count":
                                {
                                    'cdsr': 1,
                                    'agents_actif': model.membres_permanents_m2m.all().count(),
                                    'agents_inactif': 0,
                                }
                        }
                    else:
                        agents = {
                            "agents_count":
                                {
                                    'cdsr': 0,
                                    'agents_actif': model.get_users_actif_qs().count(),
                                    'agents_inactif': model.get_users_inactif_qs().count(),
                                }
                        }

                    new_model.update(agents)

                    if show_old == 'true' and demande_deja_envoyer(instruction, action, model, acces, user):
                        new_model['deja_fait'] = True
                    # Dans le cas d'une demande d'avis ou d'un accès en consultation donné par un organisateur :
                    if action == "demande_avis" or action == "consultation" \
                            and user_organisation.is_structure_organisatrice() \
                            and hasattr(model, 'get_all_children_list'):
                        parent_in_list = False
                        # On vérifie qu'aucun de ses parents n'est présent dans la liste afin de respecter le classement hiérarchique
                        for service_parent in model.get_all_service_parent_list():
                            if service_parent in result:
                                parent_in_list = True
                        if not parent_in_list:
                            categorie_list.insert(0, new_model)
                    else:
                        categorie_list.insert(0, new_model)
                    categorie_list.sort(key=lambda x: unidecode.unidecode(x['name']), reverse=False)
            return JsonResponse(categorie_list, safe=False)

        return HttpResponse("Liste de service non trouvée", status=404)


@method_decorator(csrf_exempt, name='dispatch')
class InvitationAjaxView(View):
    """
    Vue ajax pour la création d'une invitation d'inscription en POST
    pour donner la liste des invitations d'une organisation en GET
    """
    def get(self, request):
        service = get_object_or_404(Organisation, pk=request.GET.get('service_id'))
        return render(request, 'structure/panneaux/invitation.html', {'organisation': service})

    def post(self, request):
        service = get_object_or_404(Organisation, pk=request.POST.get('service_id'))
        go = False
        if request.organisation.is_structure_organisatrice():
            if request.user == request.organisation.get_users_qs().first():
                go = True
        else:
            if not service.pk == 2:
                if (request.session['o_support'] or
                        (request.organisation.is_service_consulte() and
                            (request.user.has_group("Administrateurs d\'instance") and
                             service.instance_fk == self.request.user.default_instance)) or
                             (self.request.organisation in service.get_services_administrateurs())):
                    go = True
        if not go:
            return HttpResponse('Accès restreint', status=400)
        if request.GET.get('id'):
            # Révocation d'une invitation
            if service.invitations.filter(pk=int(request.GET['id'])).exists():
                invitation = service.invitations.get(pk=int(request.GET['id']))
                invitation.revoquee_b = True
                invitation.save()
            return render(request, 'structure/panneaux/invitation.html', {'organisation': service})
        email = request.POST.get('email')
        if User.objects.filter(email=email).exists() or Organisation.objects.filter(email_s=email) or \
                service.invitations.filter(email_s=email, revoquee_b=False, expiree_b=False).exists():
            return HttpResponse('Adresse email déjà utilisée', status=400)
        if service.get_invitation().count() >= 10:
            return HttpResponse('Nombre limite d\'invitations atteint', status=400)
        try:
            dns.resolver.query(email.split('@')[1], 'MX', tcp=True)
        except:
            return HttpResponse('Erreur de domaine', status=400)
        token = sha256((str(timezone.datetime.timestamp(timezone.now())) + email).encode()).hexdigest()
        if type(service) == StructureOrganisatrice:
            url = reverse('inscription_organisateur')
        else:
            url = reverse('inscription_agent')
        address = request.build_absolute_uri(url)
        # Envoyer l'invitation
        message = (f'Bonjour,{chr(10)}'
                   f'Vous êtes invité à vous inscrire sur la plateforme ManifestationSportive.{chr(10)}'
                   f'Suivez ce lien pour votre inscription :{chr(10)}'
                   f'{address}?token={token}')
        try:
            courriel = mail.send(
                email,
                settings.DEFAULT_FROM_EMAIL,
                subject="Invitation à l'inscription sur la plateforme ManifestationSportive",
                message=message,
            )
        except:
            return HttpResponse('Problème d\'envoi d\'email', status=400)
        InvitationOrganisation.objects.create(email_s=email, origine_fk=request.user,
                                              service_fk=service, token_s=token, courriel=courriel)
        # envoyer un msg de traçabilité à la création
        from messagerie.models import Message
        Message.objects.creer_et_envoyer('tracabilite', None, [request.user], 'invitation d\'inscription',
                                         f'Vous avez invité la personne possedant l\'adresse mail {email} '
                                         f'à rejoindre le service {service}')
        return render(request, 'structure/panneaux/invitation.html', {'organisation': service})
