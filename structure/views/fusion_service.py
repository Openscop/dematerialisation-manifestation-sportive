from django.shortcuts import render
from django.views import View

from core.tasks import set_instructeur
from ..models import Organisation, ServiceInstructeur, ServiceConsulte, StructureOrganisatrice, Federation


class FusionServiceView(View):

    def dispatch(self, request, *args, **kwargs):
        if not request.session['o_support']:
            return render(request, 'core/access_restricted.html', status=403)
        return super(FusionServiceView, self).dispatch(request, *args, **kwargs)

    def get(self, request, pk_afusion):
        pk_cible = request.GET.get('pk_cible')
        organisation_afusion = Organisation.objects.get(pk=pk_afusion)
        if not pk_cible:
            return render(request, 'structure/fusion_service.html', {"organisation_afusion": organisation_afusion})
        organisation_cible = Organisation.objects.get(pk=pk_cible)
        context = {
            "organisation_afusion": organisation_afusion,
            "organisation_cible": organisation_cible,
        }
        incoherence = ""

        if type(organisation_afusion) == StructureOrganisatrice:
            if not type(organisation_cible) == StructureOrganisatrice:
                incoherence = "Une structure organisatrice ne peut être fusionnée que dans une autre structure organisatrice."
        if type(organisation_afusion) in [ServiceInstructeur, ServiceConsulte, Federation]:
            if type(organisation_cible) == StructureOrganisatrice:
                incoherence = "Un service consulté ne peut être pas fusionné dans une structure organisatrice."
            if organisation_afusion.service_enfant.all():
                incoherence = "Ce service possède des services enfants, il ne peut donc aps être supprimé."
        if type(organisation_afusion) == ServiceInstructeur:
            incoherence = "Un service instructeur ne peut être supprimé"
        if type(organisation_afusion) == Federation:
            if not type(organisation_cible) == Federation:
                incoherence = "Une fédération ne peut être fusionné que dans une autre fédération."
        if organisation_afusion.pk in [1, 2] or organisation_cible.pk in [1, 2]:
            incoherence = "Ces organisations ne peuvent être modifiées"
        if incoherence:
            context['incoherence'] = incoherence
            return render(request, 'structure/fusion_service.html', context)
        if type(organisation_afusion) == StructureOrganisatrice:
            context['manifs'] = organisation_afusion.manifs.all()
        if type(organisation_afusion) in [ServiceInstructeur, ServiceConsulte, Federation]:
            context['avis'] = organisation_afusion.avis_lecture.all() | organisation_afusion.avis_instruis.all()
            context['avis_demande'] = organisation_afusion.avis_demande.all()
        context['users'] = organisation_afusion.users.all()
        context['acces'] = organisation_afusion.autorisation_acces.all()
        context['acces_donne'] = organisation_afusion.creation_acces.all()
        return render(request, 'structure/fusion_service.html', context)

    def post(self, request, pk_afusion):
        pk_cible = request.GET.get('pk_cible')
        organisation_afusion = Organisation.objects.get(pk=pk_afusion)
        if not pk_cible:
            return render(request, 'structure/fusion_service.html', {"organisation_afusion": organisation_afusion})
        organisation_cible = Organisation.objects.get(pk=pk_cible)
        context = {
            "organisation_afusion": organisation_afusion,
            "organisation_cible": organisation_cible,
        }
        incoherence = ""
        if type(organisation_afusion) == StructureOrganisatrice:
            if not type(organisation_cible) == StructureOrganisatrice:
                incoherence = "Une structure organisatrice ne peut être fusionnée que dans une autre structure organisatrice."
        if type(organisation_afusion) in [ServiceInstructeur, ServiceConsulte, Federation]:
            if type(organisation_cible) == StructureOrganisatrice:
                incoherence = "Un service consulté ne peut être fusionné dans une structure organisatrice."
            if organisation_afusion.service_enfant.all():
                incoherence = "Ce service possède des services enfants, il ne peut donc pas être supprimé."
        if type(organisation_afusion) == ServiceInstructeur:
            incoherence = "Un service instructeur ne peut être supprimé"
        if type(organisation_afusion) == Federation:
            if not type(organisation_cible) == Federation:
                incoherence = "Une fédération ne peut être fusionné que dans une autre fédération."
        if organisation_afusion.pk in [1, 2] or organisation_cible.pk in [1, 2]:
            incoherence = "Ces organisations ne peuvent être modifiées"
        if incoherence:
            context['incoherence'] = incoherence
            return render(request, 'structure/fusion_service.html', context)
        if type(organisation_afusion) == StructureOrganisatrice:
            for manif in organisation_afusion.manifs.all():
                manif.structure_organisatrice_fk = organisation_cible
                manif.save()
        if type(organisation_afusion) in [ServiceInstructeur, ServiceConsulte, Federation]:
            for avis in organisation_afusion.avis_lecture.all():
                avis.service_consulte_history_m2m.remove(organisation_afusion)
                avis.service_consulte_history_m2m.add(organisation_cible)
            for avis in organisation_afusion.avis_instruis.all():
                avis.service_consulte_fk = organisation_cible
                avis.save()
            for avis in organisation_afusion.avis_instruis_origine.all():
                avis.service_consulte_origine_fk = organisation_cible
                avis.save()
            for avis in organisation_afusion.avis_demande.all():
                avis.service_demandeur_fk = organisation_cible
                avis.save()
        for user in organisation_afusion.users.all():
            user.organisation_m2m.remove(organisation_afusion)
            user.organisation_m2m.add(organisation_cible)
        for acces in organisation_afusion.autorisation_acces.all():
            acces.organisation = organisation_cible
            acces.save()
        for acces in organisation_afusion.creation_acces.all():
            acces.organisation_origine_fk = organisation_cible
            acces.save()
        organisation_afusion.delete()
        context['success'] = True
        return render(request, 'structure/fusion_service.html', context)
