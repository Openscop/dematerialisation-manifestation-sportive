import json

from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.views import View

from structure.models.service import ServiceConsulteInteraction
from structure.models import Organisation, ServiceConsulte


class EditionOrganisationGraphique(View):

    def dispatch(self, request, *args, **kwargs):
        organisation = ServiceConsulte.objects.get(pk=kwargs["pk_organisation"])
        is_admin = bool((self.request.session['o_support']) or
                        (self.request.organisation in organisation.get_family_member_qs().filter(admin_service_famille_b=True)) or
                        (self.request.user.has_group("Administrateurs d\'instance") and
                         organisation.instance_fk == self.request.user.default_instance))
        if not is_admin:
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk_organisation):
        organisation_selected = ServiceConsulte.objects.get(pk=pk_organisation)
        context = {
            'data': json.dumps(organisation_selected.get_edition_data()),
            'api': reverse("structure:edition_organisation_ajax", kwargs={'pk_organisation': pk_organisation}),
            'nom_famille': organisation_selected.get_service_parent_famille().nom_s,

        }
        return render(request, 'structure/edition_organisation_graphique.html', context)


class EditionOrganisationGraphiqueAjax(View):

    def dispatch(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=kwargs["pk_organisation"])
        is_admin = bool((self.request.session['o_support']) or
                        (self.request.organisation in organisation.get_family_member_qs().filter(admin_service_famille_b=True)) or
                        (self.request.user.has_group("Administrateurs d\'instance") and
                         organisation.instance_fk == self.request.user.default_instance))
        if not is_admin:
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, pk_organisation):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        for interaction in body['deleted']:
            deleted_interraction = ServiceConsulteInteraction.objects.filter(service_sortant_fk_id=int(interaction['target']),
                                                                             service_entrant_fk_id=int(interaction['source']),
                                                                             type_lien_s=interaction['type']).first()
            if deleted_interraction:
                deleted_interraction.delete()

        for interaction in body['edges']:
            new_interraction = ServiceConsulteInteraction.objects.get_or_create(service_sortant_fk_id=int(interaction['target']),
                                                                                service_entrant_fk_id=int(interaction['source']),
                                                                                type_lien_s=interaction['type'])
        for node in body['nodes']:
            service = ServiceConsulte.objects.get(pk=int(node['id']))
            if node['data']['porte_entree_avis_b'] and not service.porte_entree_avis_b:
                service.porte_entree_avis_b = True
                service.save()
            elif not node['data']['porte_entree_avis_b'] and service.porte_entree_avis_b:
                service.porte_entree_avis_b = False
                service.save()
            if node['data']['autorise_rendre_avis_b'] and not service.autorise_rendre_avis_b:
                service.autorise_rendre_avis_b = True
                service.save()
            elif not node['data']['autorise_rendre_avis_b'] and service.autorise_rendre_avis_b:
                service.autorise_rendre_avis_b = False
                service.save()
        return JsonResponse({'success': True})
