# coding: utf-8
from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.utils.http import urlencode
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import CreateView, UpdateView, View, ListView
from django.contrib import messages
from django.utils import timezone

from administrative_division.models.region import Region
from core.models.user import User
from core.models.instance import Instance
from core.util.permissions import require_type_organisation
from messagerie.models.cptmsg import CptMsg
from structure.models.service import ServiceConsulte, ServiceInstructeur, Federation, CategorieService
from structure.models.organisation import Organisation, StructureOrganisatrice
from structure.form.structureorganisatrice import StructureOrganisatriceForm
from structure.form.serviceconsulte import (ServiceConsulteForm, ServiceConsulteActionForm, FederationForm)
from structure.form.service_instructeur import ServiceInstructeurForm


@method_decorator(require_type_organisation([StructureOrganisatrice, ServiceConsulte, ServiceInstructeur, FederationForm]), name='dispatch')
class AdministrationServiceView(View):
    """Affiche la page d'administration des services"""

    def get(self, request):
        user_organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        service_selected_id = self.request.GET.get('service_id', None)
        user_selected_id = self.request.GET.get('user_id', None)
        tab = self.request.GET.get('tab', None)
        o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
        is_admin_instance = self.request.user.has_group("Administrateurs d\'instance")
        if o_support in request.user.organisation_m2m.all():
            is_support = True
        else:
            is_support = False
        is_admin = False
        is_admin_subalterne = False
        if user_selected_id:
            user_selected = User.objects.get(pk=user_selected_id)
            is_family = False
            is_admin = (is_admin_instance and
                        user_selected.default_instance == self.request.user.default_instance)
            for organisation in user_selected.organisation_m2m.all():
                if organisation in user_organisation.get_family_member_qs():
                    is_family = True
            cpt_msg = CptMsg.objects.get(utilisateur=user_selected)
            if not is_family and not is_admin and not is_admin_subalterne and not is_support and not is_admin_instance:
                user_selected = None
                cpt_msg = None
        else:
            user_selected = None
            cpt_msg = None

        if service_selected_id:
            service_selected = Organisation.objects.get(pk=service_selected_id)
            if service_selected.is_structure_organisatrice():
                categorie_selected = "structure"
                service_edit_action_form = None
            else:
                categorie_selected = service_selected.type_service_fk.categorie_fk.pk
                service_edit_action_form = ServiceConsulteActionForm(instance=service_selected)
            # TODO A décommenter pour activer les droits administrateur
            is_admin = bool((self.request.session['o_support']) or user_organisation.is_service_consulte() and
                            (self.request.user.has_group("Administrateurs d\'instance") and
                             service_selected.instance_fk == self.request.user.default_instance)
                            or (service_selected.is_service_consulte() and user_organisation in service_selected.get_services_administrateurs())
                            )
            if service_selected.is_service_consulte() and user_organisation.is_service_consulte():
                is_admin_subalterne = bool(user_organisation.admin_utilisateurs_subalternes_b and service_selected in user_organisation.get_all_children_qs())
            if service_selected.is_service_consulte() and hasattr(user_organisation, 'get_family_member_qs'):
                is_family = service_selected in user_organisation.get_family_member_qs()
            else:
                is_family = False

            if not is_family and not is_admin and not is_support and not is_admin_subalterne and not is_admin_instance:
                service_selected = None
                service_edit_action_form = None
                categorie_selected = self.request.GET.get('categorie_selected', None)

        else:
            service_selected = None
            service_edit_action_form = None
            categorie_selected = self.request.GET.get('categorie_selected', None)

        context = {
            'user_organisation': user_organisation,
            'service_selected': service_selected,
            'categorie_selected': categorie_selected,
            'user_selected': user_selected,
            'instances': Instance.objects.all().order_by('departement__name'),
            'regions': Region.objects.order_by('nom'),
            "service_edit_action_form": service_edit_action_form,
            "cpt_msg": cpt_msg,
            "is_admin": is_admin,
            "is_support": is_support,
            "is_admin_subalterne": is_admin_subalterne,
            "tab": tab,
            "is_admin_instance": is_admin_instance
        }
        return render(request, 'structure/administration_des_services.html', context)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class OrganisationDetailAjaxView(View):
    """Retourne une organisation et son formulaire lié"""

    def get(self, request):

        object_id = self.request.GET.get('object_id', None)
        user_organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        organisation = Organisation.objects.get(pk=object_id)
        is_admin_instance = self.request.user.has_group("Administrateurs d\'instance")
        if organisation.is_structure_organisatrice():
            service_edit_action_form = None
        else:
            service_edit_action_form = ServiceConsulteActionForm(instance=organisation)
        o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
        if o_support in request.user.organisation_m2m.all():
            is_support = True
        else:
            is_support = False
        is_admin_subalterne = False
        # TODO A décommenter pour activer les droits administrateur
        is_admin = bool((self.request.session['o_support']) or user_organisation.is_service_consulte() and
                        (is_admin_instance and
                         organisation.instance_fk == self.request.user.default_instance)
                        or (organisation.is_service_consulte() and user_organisation in organisation.get_services_administrateurs())
                        )
        if organisation.is_service_consulte() and user_organisation.is_service_consulte():
            is_admin_subalterne = bool(
                user_organisation.admin_utilisateurs_subalternes_b and organisation in user_organisation.get_all_children_qs())
        is_family = organisation in user_organisation.get_family_member_qs()
        if not is_family and not is_admin and not is_support and not is_admin_subalterne and not is_admin_instance:
            return render(request, 'core/access_restricted.html', status=403)

        context = {
            'organisation': organisation,
            'service_edit_action_form': service_edit_action_form,
            'is_admin': is_admin,
            'is_support': is_support,
            'is_admin_subalterne': is_admin_subalterne,
            'user_organisation': user_organisation,
        }

        return render(request, 'structure/organisation_detail.html', context)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteCreateView(CreateView):
    """Crée un service"""
    form_class = ServiceConsulteForm
    model = ServiceConsulte
    template_name = 'structure/serviceconsulte_form.html'

    def dispatch(self, request, *args, **kwargs):
        admin = False
        for orga in self.request.user.organisation_m2m.all():
            if hasattr(orga, 'admin_service_famille_b') and orga.admin_service_famille_b:
                admin = True
        # TODO verifier avec un parametre get (get en post) la validité de la verification des admin service et admin d'instance
        droit = bool((self.request.session['o_support']) or (admin) or (self.request.user.has_group("Administrateurs d\'instance")))
        if not droit:
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Service crée avec succès')
        return redirect('structure:administration_des_services')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['categorie'] = self.request.GET.get('categorie')
        instance = self.request.GET.get('instance', None)
        if instance:
            instance = Instance.objects.get(pk=instance.split("_")[1])
            kwargs['instance_id'] = instance.pk
        if self.request.session['o_support']:
            kwargs['is_support'] = True
        if self.request.user.has_group("Administrateurs d\'instance") and self.request.user.default_instance == instance:
            kwargs['is_admin'] = True
        kwargs['user_organisation'] = self.request.organisation
        kwargs.update(self.kwargs)
        return kwargs

    def form_valid(self, form):
        object = form.save(commit=False)
        form.save(commit=False)
        object.is_active_b = True
        object.autorise_acces_consult_organisateur_b = True
        if not object.service_parent_fk:
            object.porte_entree_avis_b = True
            object.autorise_rendre_avis_b = True
        object.save()
        form.save_m2m()
        object.save()  # faire le save du nom qui ne peut être fait qu'après les m2m
        return redirect(self.get_success_url().url+"?categorie_selected="+str(object.type_service_fk.categorie_fk.pk))

    def form_invalid(self, form):
        messages.error(self.request, form.errors)
        type_service = form.cleaned_data["type_service_fk"]
        return redirect("/structure/administration_des_services/?categorie_selected=" + str(type_service.categorie_fk.pk))


class FederationCreateView(ServiceConsulteCreateView):

    form_class = FederationForm
    model = Federation
    template_name = 'structure/serviceconsulte_form.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.session['o_support']:
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Fédération crée avec succès')
        return redirect('structure:administration_des_services')

    def form_valid(self, form):
        retour = super(FederationCreateView, self).form_valid(form)
        form.instance.save()  # faire le save du nom qui ne peut être fait qu'après les m2m
        return retour


class ServiceInstructeurCreateView(ServiceConsulteCreateView):

    form_class = ServiceInstructeurForm
    model = ServiceInstructeur
    template_name = 'structure/serviceconsulte_form.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.session['o_support']:
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Service instructeur crée avec succès')
        return redirect('structure:administration_des_services')

    def form_valid(self, form):
        retour = super(ServiceInstructeurCreateView, self).form_valid(form)
        form.instance.save()  # faire le save du nom qui ne peut être fait qu'après les m2m
        return retour


@method_decorator(require_type_organisation([StructureOrganisatrice]), name='dispatch')
class StructureOrganisatriceUpdateView(UpdateView):
    """Édite une structure"""

    model = StructureOrganisatrice
    form_class = StructureOrganisatriceForm
    template_name = 'structure/structureorganisatrice_form.html'

    def dispatch(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=kwargs["pk"])
        if organisation in self.request.user.organisation_m2m.all() or self.request.session['o_support']:
            return super().dispatch(request, *args, **kwargs)
        else:
            return render(request, 'core/access_restricted.html', status=403)

    def redirect_params(self, params=None):
        if 'update_structure_organisatrice' in self.request.META['HTTP_REFERER']:
            response = redirect('profile')
        else:
            response = redirect('structure:administration_des_services')
            if params:
                query_string = urlencode({'service_id': params})
                response['Location'] += '?' + query_string
        return response

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Structure édité avec succès')
        return self.redirect_params(str(self.object.pk)).url

    def form_valid(self, form):
        history_json = self.object.history_json
        if not history_json:
            history_json = []
        if form.changed_data:
            for changed_data in form.changed_data:
                champ = getattr(StructureOrganisatrice, changed_data)
                old_data = form.initial.get(changed_data)
                new_data = form.cleaned_data[changed_data]
                date_changement = timezone.now()
                if str(type(old_data).__name__) == 'list':
                    temp = [str(x) for x in old_data]
                    if temp:
                        old_data = temp
                    else:
                        old_data = "Non renseigné"

                if str(type(new_data).__name__) == 'QuerySet':
                    if new_data.count() < 1:
                        new_data = "Non renseigné"
                    else:
                        temp = [str(x) for x in new_data]
                        new_data = temp
                history_json.append({"champ": changed_data,
                                     "champ_verbose": str(champ.field.verbose_name),
                                     "user": self.request.user.__str__(),
                                     "olddata": old_data,
                                     "newdata": str(new_data),
                                     "date": str(date_changement),
                                     "date_timestamp": str(date_changement.timestamp()),
                                     "type": str(type(old_data).__name__)})

        self.object.history_json = history_json
        return super().form_valid(form)


@method_decorator(require_type_organisation([StructureOrganisatrice, ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class StructureOrganisatriceCreateView(CreateView):
    """Crée une structure organisatrice"""
    form_class = StructureOrganisatriceForm
    model = StructureOrganisatrice
    template_name = 'structure/serviceconsulte_form.html'

    def redirect_params(self, url):
        response = redirect(url)
        query_string = urlencode({'categorie_selected': "structure"})
        response['Location'] += '?' + query_string
        return response

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Structure crée avec succès')
        return self.redirect_params('structure:administration_des_services').url

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance_id'] = self.request.GET.get('instance')
        kwargs.update(self.kwargs)
        return kwargs

    def form_invalid(self, form):
        for error in form.errors:
            messages.error(self.request, error)
        return redirect("/structure/administration_des_services/?categorie_selected=structure")

    def form_valid(self, form):
        retour = super(StructureOrganisatriceCreateView, self).form_valid(form)
        form.instance.save()  # faire le save du nom qui ne peut être fait qu'après les m2m
        return retour


@method_decorator(require_type_organisation([StructureOrganisatrice, ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class FicheOrganisationView(View):

    def get(self, request, pk):
        organisation = Organisation.objects.get(pk=pk)
        user_organisation = Organisation.objects.get(pk=self.request.session["service_pk"])

        context = {
            'organisation': organisation,
            'user_organisation': user_organisation,
        }
        return render(request, 'structure/panneaux/fiche-organisation.html', context)


class OrganisationHistoriqueView(View):

    def get(self, request, *args, **kwargs):
        organisation = get_object_or_404(Organisation, pk=kwargs.get('pk'))
        user_organisation = Organisation.objects.get(pk=self.request.session['service_pk'])

        is_admin = bool(self.request.user.is_staff or self.request.user.has_group("_support")
                        or self.request.user.has_group("Administrateurs d\'instance")
                        or (user_organisation.admin_service_famille_b
                            and organisation in user_organisation.get_family_member_qs())
                        or (user_organisation.admin_utilisateurs_subalternes_b
                            and organisation in user_organisation.get_subalterne_qs())
                        or (user_organisation.admin_service_famille_b and
                            organisation in user_organisation.get_all_children_list()))

        if not request.user.is_authenticated or not is_admin:
            return render(request, "core/access_restricted.html",
                          {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)

        data = organisation.history_json
        context = {
            "data": data,
        }
        return render(request, "structure/historique.html", context=context)


class ListeTypeServiceView(ListView):
    model = CategorieService
    template_name = "structure/types_service.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = context['object_list'].exclude(nom_s__in=["_Support", "Manifestation sportive"])
        return context
