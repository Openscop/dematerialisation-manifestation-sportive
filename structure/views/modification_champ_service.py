# coding: utf-8
from django.utils.decorators import method_decorator
from django.utils.http import urlencode
from django.shortcuts import render, redirect
from django.views.generic import UpdateView
from django.contrib import messages
from django.utils import timezone

from administrative_division.models import Departement, Region
from core.util.permissions import require_type_organisation
from structure.models.service import ServiceConsulte, ServiceInstructeur, Federation
from structure.models.organisation import Organisation, StructureOrganisatrice
from structure.form.serviceconsulte import (ServiceConsulteSecteurForm, ServiceConsulteActionForm)
from structure.form.serviceconsulte_modification_champ import (ServiceConsulteParentForm, ServiceConsulteNomForm,
                                                               ServiceConsulteTypeForm, ServiceConsulteEmailForm,
                                                               ServiceConsulteTelephoneForm,  ServiceConsulteAdresseForm,
                                                               ServiceConsulteActiveForm, ServiceConsulteInstanceForm,
                                                               ServiceConsulteSiteForm, ServiceConsulteDelaiAvisForm,
                                                               ServiceConsulteDelaiPreavisForm)


@method_decorator(require_type_organisation([StructureOrganisatrice, ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class BaseServiceConsulteUpdateView(UpdateView):
    """View de base d'édition de service"""

    model = ServiceConsulte
    template_name = 'structure/serviceconsulte_form.html'

    def dispatch(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=kwargs["pk"])
        if organisation.is_service_consulte() or organisation.is_federation():
            is_admin = bool((self.request.session['o_support']) or
                            (self.request.organisation in organisation.get_family_member_qs().filter(admin_service_famille_b=True)) or
                            (self.request.user.has_group("Administrateurs d\'instance") and
                             organisation.instance_fk == self.request.user.default_instance))
        else:
            is_admin = bool((self.request.session['o_support']) or
                            (self.request.user.has_group("Administrateurs d\'instance") and
                             organisation.instance_fk == self.request.user.default_instance))
        if not is_admin:
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def redirect_params(self, url, params=None, tab=None):
        response = redirect(url)
        if params:
            query_string = urlencode({'service_id': params})
            response['Location'] += '?' + query_string
        if tab:
            query_string = urlencode({'tab': tab})
            response['Location'] += '&' + query_string
        return response

    def form_valid(self, form):
        history_json = self.object.history_json
        if not history_json:
            history_json = []
        if form.changed_data:
            for changed_data in form.changed_data:
                if changed_data == "is_service_departemental" and not 'departement_m2m' in form.changed_data:
                    changed_data = "departement_m2m"
                if changed_data == "is_service_regional" and not 'region_m2m' in form.changed_data:
                    changed_data = "region_m2m"
                if changed_data not in ['is_service_regional', 'is_service_departemental', 'is_service_arrondissement', 'is_service_commune']:
                    champ = ""
                    if hasattr(Organisation, changed_data):
                        champ = getattr(Organisation, changed_data)
                    elif hasattr(ServiceConsulte, changed_data):
                        champ = getattr(ServiceConsulte, changed_data)
                    elif hasattr(StructureOrganisatrice, changed_data):
                        champ = getattr(StructureOrganisatrice, changed_data)
                    old_data = form.initial.get(changed_data)
                    new_data = form.cleaned_data[changed_data]
                    date_changement = timezone.now()
                    if str(type(old_data).__name__) == 'list':
                        temp = [str(x) for x in old_data]
                        if temp:
                            old_data = temp
                        else:
                            old_data = "Non renseigné"
                    elif str(type(old_data).__name__) == 'int' and hasattr(getattr(form.instance, changed_data), 'pk'):
                        pass
                    else:
                        old_data = str(old_data)
                    if str(type(new_data).__name__) == 'QuerySet':
                        if new_data.count() < 1:
                            if changed_data == "departement_m2m" and form.cleaned_data['is_service_departemental'] == True:
                                new_data = Departement.objects.filter(instance=self.object.instance_fk).first()
                            elif changed_data == "region_m2m" and form.cleaned_data['is_service_regional'] == True:
                                new_data = Region.objects.filter(departement__instance=self.object.instance_fk).first()
                            else:
                                new_data = "Non renseigné"
                        else:
                            temp = [str(x) for x in new_data]
                            new_data = temp
                    if str(type(champ.field).__name__) == 'ForeignKey' and old_data != 'None':
                        old_data = str(champ.field.related_model.objects.get(pk=int(old_data)))
                    if str(type(champ.field).__name__) == 'BooleanField':
                        if old_data == "True":
                            old_data = "Vrai"
                        elif old_data == "False":
                            old_data = "Faux"
                        if new_data:
                            new_data = "Vrai"
                        elif not new_data:
                            new_data = "Faux"
                    else:
                        if old_data == "None":
                            old_data = "Non renseigné"
                        if not new_data:
                            new_data = "Non renseigné"
                    history_json.append({"champ": changed_data,
                                         "champ_verbose": str(champ.field.verbose_name),
                                         "user": self.request.user.__str__(),
                                         "olddata": old_data,
                                         "newdata": str(new_data),
                                         "date": str(date_changement),
                                         "date_timestamp": str(date_changement.timestamp()),
                                         "type": str(type(old_data).__name__)})

        self.object.history_json = history_json
        return super().form_valid(form)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteUpdateActiveView(BaseServiceConsulteUpdateView):
    """Édite le nom d'un service"""

    form_class = ServiceConsulteActiveForm

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'État du service édités avec succès')
        return self.redirect_params('structure:administration_des_services', str(self.object.pk), 'info').url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteUpdateNomView(BaseServiceConsulteUpdateView):
    """Édite le nom d'un service"""

    form_class = ServiceConsulteNomForm

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Nom du service édités avec succès')
        return self.redirect_params('structure:administration_des_services', str(self.object.pk), 'info').url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteUpdateAdresseView(BaseServiceConsulteUpdateView):
    """Édite les actions service"""

    model = Organisation
    form_class = ServiceConsulteAdresseForm

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Adresse du service édités avec succès')
        return self.redirect_params('structure:administration_des_services', str(self.object.pk), 'info').url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteUpdateTelephoneView(BaseServiceConsulteUpdateView):
    """Édite les actions service"""

    model = Organisation
    form_class = ServiceConsulteTelephoneForm

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Téléphone du service édités avec succès')
        return self.redirect_params('structure:administration_des_services', str(self.object.pk), 'info').url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteUpdateEmailView(BaseServiceConsulteUpdateView):
    """Édite les actions service"""

    model = Organisation
    form_class = ServiceConsulteEmailForm

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Email du service édités avec succès')
        return self.redirect_params('structure:administration_des_services', str(self.object.pk), 'info').url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteUpdateDelaiAvisView(BaseServiceConsulteUpdateView):
    """Édite le delai de rendu d'avis"""

    model = ServiceConsulte
    form_class = ServiceConsulteDelaiAvisForm

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Délai de rendu d\'avis édités avec succès')
        return self.redirect_params('structure:administration_des_services', str(self.object.pk), 'info').url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteUpdateDelaiPreavisView(BaseServiceConsulteUpdateView):
    """Édite le delai de rendu de préavis"""

    model = ServiceConsulte
    form_class = ServiceConsulteDelaiPreavisForm

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Délai de rendu de préavis édités avec succès')
        return self.redirect_params('structure:administration_des_services', str(self.object.pk), 'info').url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteUpdateParentView(BaseServiceConsulteUpdateView):
    """Édite le parent d'un service consulte"""

    form_class = ServiceConsulteParentForm

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Parent du service éditées avec succès')
        return self.redirect_params('structure:administration_des_services', str(self.object.pk), 'info').url

    def form_invalid(self, form):
        messages.error(self.request, form.errors['service_parent_fk'][0])
        return self.redirect_params('structure:administration_des_services', str(self.object.pk), 'info')


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteUpdateTypeView(BaseServiceConsulteUpdateView):
    """Édite les actions service"""

    form_class = ServiceConsulteTypeForm

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Type du service édités avec succès')
        return self.redirect_params('structure:administration_des_services', str(self.object.pk), 'info').url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteUpdateActionView(BaseServiceConsulteUpdateView):
    """Édite les actions service"""

    form_class = ServiceConsulteActionForm

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Action du service édités avec succès')
        if 'administration_des_services' in self.request.META.get('HTTP_REFERER'):
            return self.redirect_params('structure:administration_des_services', str(self.object.pk)).url
        else:
            return redirect('structure:edition_organisation', self.object.pk).url



@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteUpdateInstanceView(BaseServiceConsulteUpdateView):
    """Édite l'instance par default d'une organisation"""

    model = Organisation
    form_class = ServiceConsulteInstanceForm

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Instance du service éditées avec succès')
        return self.redirect_params('structure:administration_des_services', str(self.object.pk), 'info').url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteUpdateSiteView(BaseServiceConsulteUpdateView):
    """Édite le site internet d'une organisation"""

    model = Organisation
    form_class = ServiceConsulteSiteForm

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Site internet du service éditées avec succès')
        return self.redirect_params('structure:administration_des_services', str(self.object.pk), 'info').url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class ServiceConsulteUpdateSecteurView(BaseServiceConsulteUpdateView):
    """Édite les secteurs d'un service consulte"""

    model = Organisation
    form_class = ServiceConsulteSecteurForm

    def dispatch(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=kwargs["pk"])
        if (not self.request.session['o_support'] and
                (organisation.region_m2m.all().count() > 1 or organisation.departement_m2m.all().count() > 1)):
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Secteurs du service éditées avec succès')
        return self.redirect_params('structure:administration_des_services', str(self.object.pk), 'info').url

    def form_valid(self, form):
        retour = super().form_valid(form)
        form.instance.save()  # faire le save du nom qui ne peut être fait qu'après les m2m
        return retour

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.session['o_support']:
            kwargs['is_support'] = True
        return kwargs
