# coding: utf-8
from django.db.models import Q
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.utils.http import urlencode
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import UpdateView, View
from django.contrib import messages
from django.utils import timezone

from administrative_division.models.region import Region
from core.models.user import User
from core.models.instance import Instance
from core.util.permissions import require_type_organisation
from messagerie.models.cptmsg import CptMsg
from structure.models.service import ServiceConsulte, ServiceInstructeur, Federation, CategorieService
from structure.models.organisation import Organisation
from structure.models.cdsr import ServiceCDSR
from structure.form.user import (UserOrganisationForm, UserEmailForm, UserActiveForm)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class IsHabiliteAjaxView(View):
    """ À partir d'une organisation retourne True si :
        - Équipe support
        - Administrateur d'instance
        - De la même famille que le service """

    def get(self, request, pk, *args, **kwargs):
        o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
        if o_support in request.user.organisation_m2m.all() or self.request.user.has_group("Administrateurs d\'instance"):
            return HttpResponse(True)  # Équipe support ou admin d'instance
        model = self.request.GET.get('model', None)
        user_organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        if model == "organisation":
            organisation = Organisation.objects.get(pk=pk)
            if organisation in user_organisation.get_family_member_qs():
                return HttpResponse(True)  # De la même famille que le service
        else:
            user = User.objects.get(pk=pk)
            for organisation in user.organisation_m2m.all():
                if organisation in user_organisation.get_family_member_qs():
                    return HttpResponse(True)  # De la même famille que l'utilisateur
        return HttpResponse(False)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class IsAdminFamilleAjaxView(View):
    """ À partir d'une organisation retourne True si :
            - Équipe support
            - Administrateur d'instance et bon secteur
            - Service administrateur de la famille du service """
    def get(self, request, *args, **kwargs):
        categorie = self.request.GET.get('categorie', None)
        secteur = self.request.GET.get('secteur', None)
        o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
        if o_support in request.user.organisation_m2m.all():
            if categorie in ['4', '13']:
                # Si groupe support et catégorie instruction
                return HttpResponse("Service instructeur")
            return HttpResponse(True)
        if self.request.user.has_group("_support"):
            return HttpResponse(True)
        # Si Fédération délégataire et pas groupe support
        if categorie == '12':
            return HttpResponse("Fédération délégataire")

        instance = None
        if secteur.split("_")[0] == "instance":
            instance = Instance.objects.get(pk=secteur.split("_")[1])
        # TODO a réactiver
        user_organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        # if user_organisation.is_service_consulte():
        #     if user_organisation.admin_service_famille_b \
        #             and user_organisation.instance_fk == instance \
        #             and user_organisation.type_service_fk.categorie_fk.pk == int(categorie):
        #         return HttpResponse(True)

        if self.request.user.has_group("Administrateurs d\'instance") and instance == self.request.user.default_instance:
            return HttpResponse(True)

        return HttpResponse(False)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class BaseUserUpdateView(UpdateView):
    """View de base d'édition d'utilisateur"""

    model = User
    template_name = 'structure/serviceconsulte_form.html'

    def dispatch(self, request, *args, **kwargs):
        # TODO a modifier

        user = User.objects.get(pk=kwargs["pk"])
        user_organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        is_admin = bool(self.request.session['o_support'] or self.request.user.has_group("Administrateurs d\'instance")
                        or (user_organisation.admin_service_famille_b and
                            organisation in user_organisation.get_family_member_qs() for organisation in
                            user.organisation_m2m.all())
                        or (user_organisation.admin_utilisateurs_subalternes_b and
                            organisation in user_organisation.get_all_children_qs() for organisation in
                            user.organisation_m2m.all())
                        or user == self.request.user)

        if not is_admin:
            return render(request, 'core/access_restricted.html', status=403)
        return super(BaseUserUpdateView, self).dispatch(request, *args, **kwargs)

    def redirect_params(self, url, service_id=None, user_id=None):
        if 'profile' in self.request.META['HTTP_REFERER']:
            response = redirect('profile_publique', pk=user_id)
            return response
        response = redirect(url)
        if service_id:
            query_string = urlencode({'service_id': service_id})
            response['Location'] += '?' + query_string
        if user_id:
            query_string = urlencode({'user_id': user_id})
            if service_id:
                response['Location'] += '&' + query_string
            else:
                response['Location'] += '?' + query_string
        return response

    def form_invalid(self, form):
        messages.error(self.request, form.errors['__all__'])
        if self.object.organisation_m2m.first():
            return self.redirect_params('structure:administration_des_services', self.object.organisation_m2m.first().pk, self.object.pk)
        else:
            return self.redirect_params('structure:administration_des_services', service_id=None, user_id=self.object.pk)

    def form_valid(self, form):
        history_json = self.object.history_json
        if not history_json:
            history_json = []
        if form.changed_data:
            for changed_data in form.changed_data:
                champ = getattr(User, changed_data)
                old_data = form.initial.get(changed_data)
                new_data = form.cleaned_data[changed_data]
                date_changement = timezone.now()
                if str(type(old_data).__name__) == 'list':
                    temp = [str(x) for x in old_data]
                    old_data = temp
                else:
                    old_data = str(old_data)
                if str(type(new_data).__name__) == 'QuerySet':
                    if new_data.count() < 1:
                        new_data = "Non renseigné"
                    else:
                        temp = [str(x) for x in new_data]
                        new_data = temp

                if str(type(champ.field).__name__) == 'ForeignKey':
                    old_data = str(champ.field.related_model.objects.get(pk=int(old_data)))
                history_json.append({"champ": changed_data,
                                     "champ_verbose": str(champ.field.verbose_name),
                                     "user": self.request.user.__str__(),
                                     "olddata": old_data,
                                     "newdata": str(new_data),
                                     "date": str(date_changement),
                                     "date_timestamp": str(timezone.now().timestamp()),
                                     "type": str(type(old_data).__name__)})

        self.object.history_json = history_json
        return super().form_valid(form)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class UserDetailAjaxView(View):
    """Retourne un utilisateur et son organisation liée"""

    def get(self, request):

        object_id = self.request.GET.get('object_id', None)
        user = User.objects.get(pk=object_id)
        organisation = Organisation.objects.get(pk=self.request.GET.get('organisation_id', None))
        user_organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        cpt_msg = CptMsg.objects.get(utilisateur=user)
        o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
        is_admin_instance = self.request.user.has_group("Administrateurs d\'instance")
        if o_support in request.user.organisation_m2m.all():
            is_admin = True
        else:
            is_admin = False
        is_admin_subalterne = False
        # TODO A décommenter pour activer les droits administrateur
        if user_organisation.is_service_consulte():
            is_admin = bool((self.request.session['o_support']) or
                            (self.request.user.has_group("Administrateurs d\'instance") and
                             organisation.instance_fk == self.request.user.default_instance)
                             or (organisation.is_service_consulte() and user_organisation in organisation.get_services_administrateurs())
                             )
            is_admin_subalterne = bool(
                user_organisation.admin_utilisateurs_subalternes_b and organisation in user_organisation.get_all_children_qs())

        is_family = False
        for organisation in user.organisation_m2m.all():
            if organisation in user_organisation.get_family_member_qs():
                is_family = True
        if not is_family and not is_admin and not is_admin_subalterne and not is_admin_instance:
            return render(request, 'core/access_restricted.html', status=403)

        context = {
            'user': user,
            'organisation': organisation,
            'cpt_msg': cpt_msg,
            'is_admin': is_admin,
            'is_admin_subalterne': is_admin_subalterne,
            'is_admin_instance': is_admin_instance,
        }

        return render(request, 'structure/user_detail.html', context)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class UserHistoriqueView(View):

    def get(self, request, *args, **kwargs):
        user = get_object_or_404(User, pk=kwargs.get('pk'))
        user_organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        is_admin = bool(self.request.user.is_staff or self.request.user.has_group("_support"))
        service_id = self.request.GET.get('service_id', None)
        if service_id:
            organisation = Organisation.objects.get(pk=self.request.GET.get('service_id'))
        if (user_organisation.admin_service_famille_b and organisation in user_organisation.get_family_member_qs()) \
            or (user_organisation.admin_utilisateurs_subalternes_b and organisation in user_organisation.get_all_children_qs()):
            is_admin = True

        if not request.user.is_authenticated or not is_admin:
            return render(request, "core/access_restricted.html",
                          {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)

        data = user.history_json

        context = {
            "data": data,
        }
        return render(request, "structure/historique.html", context=context)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class UserUpdateEmailView(BaseUserUpdateView):

    form_class = UserEmailForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request_user_pk'] = self.request.user.pk
        return kwargs

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Addresse e-amil de l\'utilisateur édité avec succès')
        if self.object.organisation_m2m.first():
            return self.redirect_params('structure:administration_des_services', self.object.organisation_m2m.first().pk, self.object.pk).url
        return self.redirect_params('structure:administration_des_services', service_id=None,
                                    user_id=self.object.pk).url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class UserDeleteEmailView(View):

    def dispatch(self, request, *args, **kwargs):

        user = User.objects.get(pk=kwargs["pk"])
        user_organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        is_admin = bool(self.request.session['o_support'] or self.request.user.has_group("Administrateurs d\'instance")
                        or (user_organisation.admin_service_famille_b and
                            organisation in user_organisation.get_family_member_qs() for organisation in
                            user.organisation_m2m.all())
                        or user == self.request.user)
        if not is_admin:
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        user_id = self.request.GET.get('user_id', None)
        user = User.objects.get(pk=user_id)
        user.emailaddress_set.all().delete()
        old_email = user.email
        user.email = ""
        history_json = user.history_json
        if not history_json:
            history_json = []
        history_json.append({"champ": "email",
                             "champ_verbose": "Adresse e-mail",
                             "user": self.request.user.__str__(),
                             "olddata": old_email,
                             "newdata": None,
                             "date": str(timezone.now()),
                             "date_timestamp": str(timezone.now().timestamp()),
                             "type": 'str'})
        user.history_json = history_json
        user.save()
        return HttpResponse('Adresse e-mail supprimée')


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class UserUpdateActiveView(BaseUserUpdateView):

    form_class = UserActiveForm

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'État de l\'utilisateur édité avec succès')
        if self.object.organisation_m2m.first():
            return self.redirect_params('structure:administration_des_services', self.object.organisation_m2m.first().pk, self.object.pk).url
        else:
            return self.redirect_params('structure:administration_des_services', service_id=None, user_id=self.object.pk).url

    def form_valid(self, form):
        if not form.cleaned_data['is_active']:
            history_json = form.instance.history_json
            if not history_json:
                history_json = []
            groups = []
            for group in form.instance.groups.all():
                groups.append(str(group))
            if groups:
                history_json.append({"champ": "groups",
                                     "champ_verbose": "Groupe de l'utilisateur",
                                     "user": self.request.user.__str__(),
                                     "olddata": groups,
                                     "newdata": 'Supprimé car passé inactif',
                                     "date": str(timezone.now()),
                                     "date_timestamp": str(timezone.now().timestamp()),
                                     "type": 'str'})
            form.instance.history_json = history_json
            form.instance.save()
            form.instance.groups.clear()
        return super().form_valid(form)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class UserOrganisationnUpdateView(UpdateView):
    """Édite les organisations d'un utilisateur"""

    model = User
    template_name = 'structure/serviceconsulte_form.html'
    form_class = UserOrganisationForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['service_id'] = self.request.GET.get('service_id')
        kwargs['user_id'] = self.request.GET.get('pk')
        kwargs.update(self.kwargs)
        return kwargs

    def redirect_params(self, url, service_id=None, user_id=None):
        response = redirect(url)
        if service_id:
            query_string = urlencode({'service_id': service_id})
            response['Location'] += '?' + query_string
        if user_id:
            query_string = urlencode({'user_id': user_id})
            response['Location'] += '&' + query_string
        return response

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Organisations de l\'utilisateur éditées avec succès')
        return self.redirect_params('structure:administration_des_services', self.kwargs['service_id'], self.kwargs['pk']).url


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class GererUserOrganisationView(View):
    """
    Gérer les organisations des utilisateurs
    Gérer les membres des CDSR
    suivant l'url appelée
    """

    def get(self, request, pk):
        instances = Instance.objects.all().order_by('departement__name')
        categories = []
        if "user" in request.path.split('/')[-3]:
            user = User.objects.get(pk=pk)
            context = {"user_selected": user}
        else:
            cdsr = ServiceCDSR.objects.get(pk=pk)
            context = {"cdsr_selected": cdsr}
        user_organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        is_super_admin = False
        is_admin = False
        if self.request.user.has_group("_support"):
            categories = list(CategorieService.objects.all().values_list('pk', 'nom_s'))
            is_super_admin = True
            is_admin = True
        elif self.request.user.has_group("Administrateurs d\'instance"):
            categories = list(CategorieService.objects.all().values_list('pk', 'nom_s'))
            is_admin = True
        elif user_organisation.is_service_consulte():
            if user_organisation.admin_service_famille_b:
                for user_service in self.request.user.organisation_m2m.all():
                    if user_service.admin_service_famille_b:
                        for service in user.organisation_m2m.all():
                            if service.is_service_consulte():
                                if service in user_service.get_family_member_qs():
                                    categories.append((user_service.type_service_fk.categorie_fk.pk, user_service.type_service_fk.categorie_fk.nom_s))
                                    is_admin = True
        else:
            return render(request, 'core/access_restricted.html', status=403)
        context = {**context,
                   'instances': instances,
                   'regions': Region.objects.order_by('nom'),
                   'categories': categories,
                   'is_admin': is_admin,
                   'is_super_admin': is_super_admin
                   }
        return render(request, "structure/gerer-user-organisation.html", context)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class AddUserOrganisationView(View):
    """Ajoute une organisation à un utilisateur"""

    def dispatch(self, request, *args, **kwargs):
        user_id = self.request.GET.get("user_id", None)
        service_id = self.request.GET.get("service_id")
        service = Organisation.objects.get(pk=service_id)
        if user_id and service_id:
            user = User.objects.get(pk=user_id)
            is_admin = bool(self.request.session['o_support']
                            or self.request.organisation.admin_service_famille_b
                            or self.request.user.has_group("Administrateurs d\'instance"))
            # Droit pour admin subalterne
            if self.request.organisation.admin_utilisateurs_subalternes_b:
                for organisation in user.organisation_m2m.all():
                    if organisation in self.request.organisation.get_all_children_qs() and \
                            service in self.request.organisation.get_all_children_qs():
                        is_admin = True
            if not is_admin:
                return HttpResponse("Vous n'êtes pas autorisé à ajouter un agent à ce service.")
        else:
            return render(request, '404.html', status=404)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        user_id = self.request.GET.get("user_id", None)
        service_id = self.request.GET.get("service_id")
        user_organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
        if user_id and service_id:
            user = User.objects.get(pk=user_id)
            service = Organisation.objects.get(pk=service_id)
            # On vérifie que l'utilisateur a les droits pour cette opération
            is_admin = bool(
                o_support in request.user.organisation_m2m.all() or self.request.user.has_group("Administrateurs d\'instance")
                or (user_organisation.admin_service_famille_b
                    and user in user_organisation.get_all_agent_family_list()
                    and service in user_organisation.get_family_member_qs())
            )
            # Droit pour admin subalterne
            if self.request.organisation.admin_utilisateurs_subalternes_b:
                for organisation in user.organisation_m2m.all():
                    if organisation in self.request.organisation.get_all_children_qs() and \
                            service in self.request.organisation.get_all_children_qs():
                        is_admin = True
            if is_admin:
                # On réalise l'opération et enregistre les modifications dans le history_json de l'user
                old_data, new_data, old_data_organisation, new_data_organisation = [], [], [], []
                for organisation in user.organisation_m2m.all():
                    old_data.append(organisation.nom_s)
                for old_user in service.get_users_qs():
                    old_data_organisation.append(str(old_user))
                user.organisation_m2m.add(service)
                for organisation in user.organisation_m2m.all():
                    new_data.append(organisation.nom_s)
                for new_user in service.get_users_qs():
                    new_data_organisation.append(str(new_user))
                if not new_data:
                    new_data = "Aucune"
                if not old_data:
                    old_data = "Aucune"
                if not new_data_organisation:
                    new_data_organisation = "Aucun"
                if not old_data_organisation:
                    old_data_organisation = "Aucun"

                history_json_user = user.history_json
                if not history_json_user:
                    history_json_user = []
                history_json_user.append({"champ": "organisation_m2m",
                                          "champ_verbose": "Organisation",
                                          "user": self.request.user.__str__(),
                                          "olddata": old_data,
                                          "newdata": str(new_data),
                                          "date": str(timezone.now()),
                                          "date_timestamp": str(timezone.now().timestamp()),
                                          "type": str(type(old_data).__name__)})
                user.history_json = history_json_user
                history_json_organisation = service.history_json
                if not history_json_organisation:
                    history_json_organisation = []
                history_json_organisation.append({"champ": "users",
                                                  "champ_verbose": "Utilisateurs",
                                                  "user": self.request.user.__str__(),
                                                  "olddata": old_data_organisation,
                                                  "newdata": str(new_data_organisation),
                                                  "date": str(timezone.now()),
                                                  "date_timestamp": str(timezone.now().timestamp()),
                                                  "type": str(type(old_data).__name__)})

                service.history_json = history_json_organisation
                service.save()
                user.save()
                return HttpResponse("True")
            else:
                return HttpResponse("Vous n'êtes pas autorisé à ajouter un agent à ce service. ")
        else:
            return HttpResponse("Un problème est survenue lors de la selection de l'utilisateur. "
                                "Veuillez réitérer l'opération sur la page d'administration des structures.")


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class RemoveUserOrganisationView(View):
    """Retire un utilisateur d'une organisation"""

    def dispatch(self, request, *args, **kwargs):
        user_id = self.request.GET.get("user_id", None)
        service_id = self.request.GET.get("organisation_id")
        user = User.objects.get(pk=user_id)
        service = Organisation.objects.get(pk=service_id)
        is_admin = bool(self.request.session['o_support']
                        or self.request.organisation in service.get_services_administrateurs()
                        or self.request.user.has_group("Administrateurs d\'instance"))
        # Droit pour admin subalterne
        if self.request.organisation.admin_utilisateurs_subalternes_b:
            for organisation in user.organisation_m2m.all():
                if organisation in self.request.organisation.get_all_children_qs() and \
                        service in self.request.organisation.get_all_children_qs():
                    is_admin = True
        if not is_admin:
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        user_organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
        organisation_id = self.request.GET.get('organisation_id', None)
        user_id = self.request.GET.get('user_id', None)
        service = Organisation.objects.get(pk=organisation_id)
        user = User.objects.get(pk=user_id)
        old_data, new_data = [], []
        # On vérifie que l'utilisateur a les droits pour cette opération
        is_admin = bool(
            o_support in request.user.organisation_m2m.all() or
            (self.request.user.has_group("Administrateurs d\'instance")) or
            (user_organisation.admin_service_famille_b and user in user_organisation.get_all_agent_family_list()
             and service.type_service_fk.categorie_fk == user_organisation.type_service_fk.categorie_fk)
        )
        # Droit pour admin subalterne
        if self.request.organisation.admin_utilisateurs_subalternes_b:
            for organisation in user.organisation_m2m.all():
                if organisation in self.request.organisation.get_all_children_qs() and \
                        service in self.request.organisation.get_all_children_qs():
                    is_admin = True
        if is_admin:
            # On réalise l'opération et enregistre les modifications dans le history_json de l'user
            old_data, new_data, old_data_organisation, new_data_organisation = [], [], [], []
            for organisation in user.organisation_m2m.all():
                old_data.append(organisation.nom_s)
            for old_user in service.get_users_qs():
                old_data_organisation.append(str(old_user))
            user.organisation_m2m.remove(service)
            for organisation in user.organisation_m2m.all():
                new_data.append(organisation.nom_s)
            for new_user in service.get_users_qs():
                new_data_organisation.append(str(new_user))
            if not new_data:
                new_data = "Aucune"
            if not old_data:
                old_data = "Aucune"
            if not new_data_organisation:
                new_data_organisation = "Aucun"
            if not old_data_organisation:
                old_data_organisation = "Aucun"
            if user.organisation_m2m.count() < 1:
                # Si c'étais la derniére organisation d'un utilisateur, on désactive son compte
                user.is_active = False
                user.save()
                if service.is_service_consulte():
                    message = "Utilisateur retiré du service. Son compte a été désactivé."
                else:
                    message = "Utilisateur retiré de l'organisation. Son compte a été désactivé."
            elif service.is_service_consulte():
                message = "Utilisateur retiré du service"
            else:
                message = "Utilisateur retiré de l'organisation"
            history_json = user.history_json
            if not history_json:
                history_json = []
            history_json.append({"champ": "organisation_m2m",
                                 "champ_verbose": "Organisation",
                                 "user": self.request.user.__str__(),
                                 "olddata": old_data,
                                 "newdata": str(new_data),
                                 "date": str(timezone.now()),
                                 "date_timestamp": str(timezone.now().timestamp()),
                                 "type": str(type(old_data).__name__)})
            user.history_json = history_json
            history_json_organisation = service.history_json
            if not history_json_organisation:
                history_json_organisation = []
            history_json_organisation.append({"champ": "users",
                                              "champ_verbose": "Utilisateurs",
                                              "user": self.request.user.__str__(),
                                              "olddata": old_data_organisation,
                                              "newdata": str(new_data_organisation),
                                              "date": str(timezone.now()),
                                              "date_timestamp": str(timezone.now().timestamp()),
                                              "type": str(type(old_data).__name__)})

            service.history_json = history_json_organisation
            service.save()
            user.save()
            return HttpResponse(message)
        else:
            return render(request, 'core/access_restricted.html', status=403)


class UserSansServiceListView(View):

    def dispatch(self, request, *args, **kwargs):
        is_admin = bool((self.request.session['o_support']) or (self.request.user.has_group("Administrateurs d\'instance")))
        if not is_admin:
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        instance_pk = request.GET.get('instance_pk', None)
        if instance_pk:
            instance = Instance.objects.get(pk=instance_pk)
        else:
            instance = request.user.default_instance
        if instance.pk != 2:
            user_sans_service = User.objects.filter(default_instance=instance, organisation_m2m__isnull=True).order_by('last_name')
        else:
            user_sans_service = User.objects.filter(organisation_m2m__isnull=True).order_by('last_name')
        if self.request.GET.get('request'):
            return render(request, "structure/panneaux/tableau_user_list.html", {'user_list': user_sans_service, "type_user_list": 'user_sans_service'})

        return render(request, "structure/page_user_tableau.html", {'user_list': user_sans_service,
                                                                    'instances': Instance.objects.all().order_by('departement'),
                                                                    "type_user_list": 'user_sans_service',
                                                                    "titre": "Liste des utilisateurs sans service"})


class ComptesDormantsView(View):

    def dispatch(self, request, *args, **kwargs):
        is_admin = bool((self.request.session['o_support']) or (self.request.user.has_group("Administrateurs d\'instance")))
        if not is_admin:
            return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        # définition du délai sans connection des comptes (via ajax, sinon définir à None)
        delais_sans_connexion = self.request.GET.get('delais_sans_connexion', None)
        if delais_sans_connexion:  # Si on a demandé un délai précis
            date_cle_compte_dormant = timezone.now() - timezone.timedelta(int(delais_sans_connexion))
        else:  # Sinon on prend le délai par défaut (1 an)
            date_cle_compte_dormant = timezone.now() - timezone.timedelta(365)
        # définition de l'instance (via ajax, sinon définir à user.default_instance)
        instance_pk = self.request.GET.get('instance_pk', None)
        # instance auquel la requête est rattachée
        if instance_pk:  # Si on a demandé une instance précise
            instance_selected = Instance.objects.get(pk=instance_pk)
        else:  # Sinon on prend celle de base de l'utilisateur
            instance_selected = request.user.default_instance
            # instance_selected = request.organisation.instance_fk
        # réalisation d'une requête combinée
        combined_queryset = instance_selected.users.filter(Q(last_login__lte=date_cle_compte_dormant) | Q(last_login__isnull=True))
        # réalisation d'un order by sur la requete combinée
        ordered_queryset = combined_queryset.order_by('-last_login').reverse()
        users_service_connecte = ordered_queryset
        # calcul du nombre de comptes dormants
        nombre_comptes_dormants = len(users_service_connecte)
        context = {
            "user_list": users_service_connecte,
            "nombre_comptes_dormants": nombre_comptes_dormants,
            'instances': Instance.objects.order_by('departement'),
            "type_user_list": 'compte_dormant',
            "titre": "Comptes dormants"
        }
        # S'il s'agit d'une requête ajax
        ajax = self.request.GET.get('request')
        if ajax:
            # On retourne le tableau mis à jour avec la nouvelle date clé prise en compte
            return render(request, 'structure/panneaux/tableau_user_list.html', context=context)
        # Sinon on affiche la page globale
        return render(request, 'structure/page_user_tableau.html', context=context)