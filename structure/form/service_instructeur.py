from crispy_forms.layout import Layout, Fieldset, HTML
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import StrictButton

from django import forms
from django.urls import reverse

from core.models.instance import Instance
from administrative_division.models import Commune, Departement, Arrondissement, Region
from structure.models.service import TypeService, ServiceInstructeur
from .serviceconsulte import ServiceConsulteForm


class ServiceInstructeurForm(ServiceConsulteForm):

    class Meta:
        model = ServiceInstructeur
        fields = "__all__"
        exclude = ('delai_jour_preavis_int', 'delai_jour_avis_int')

    def __init__(self, categorie=None, instance_id=None, is_support=False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        fieldset_secteur = ['Portée territoriale', 'is_service_regional', 'is_service_departemental', 'is_service_arrondissement',
                            'arrondissement_m2m', 'is_service_commune', 'commune_m2m', 'instance_fk']
        self.is_support = is_support
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:create_service_instructeur')
        self.fields['autorise_acces_consult_organisateur_b'].initial = True
        if instance_id:
            instance = Instance.objects.get(pk=instance_id)
            self.fields['is_service_regional'].widget.attrs = {'class': 'geographie_checkbox',
                                                               'data-dep': f'({instance.departement.region.nom})'}
            self.fields['is_service_departemental'].widget.attrs = {'class': 'geographie_checkbox',
                                                                    'data-dep': f'({instance.departement.name})'}
            self.fields['is_service_arrondissement'].widget.attrs = {'class': 'geographie_checkbox'}
            self.fields['is_service_commune'].widget.attrs = {'class': 'geographie_checkbox'}
            if is_support:
                self.fields['departement_m2m'] = forms.ModelMultipleChoiceField(label="Departement", required=False,
                                                                                queryset=Departement.objects.all())
                self.fields['region_m2m'] = forms.ModelMultipleChoiceField(label="Region", required=False,
                                                                           queryset=Region.objects.all())
                self.fields['departement_m2m'].widget.attrs = {'class': 'chosen-select', 'data-placeholder': "Faites votre sélection"}
                self.fields['region_m2m'].widget.attrs = {'class': 'chosen-select', 'data-placeholder': "Faites votre sélection"}
                fieldset_secteur = fieldset_secteur[:2] + ['region_m2m'] + fieldset_secteur[2:3] + ['departement_m2m'] + fieldset_secteur[3:]

            self.fields['arrondissement_m2m'] = forms.ModelMultipleChoiceField(
                label="Arrondissements", required=False,
                queryset=Arrondissement.objects.filter(departement__instance__pk=instance_id))
            self.fields['commune_m2m'] = forms.ModelMultipleChoiceField(
                label="Communes", required=False,
                queryset=Commune.objects.filter(arrondissement__departement__instance__pk=instance_id))
            self.fields['commune_m2m'].widget.attrs = {'class': 'chosen-select', 'data-placeholder': "Faites votre sélection"}
            self.fields['arrondissement_m2m'].widget.attrs = {'class': 'chosen-select', 'data-placeholder': "Faites votre sélection"}
            self.fields['type_service_fk'].widget.attrs = {'class': 'chosen-select'}
            self.fields['type_service_fk'].label = "Type de service (obligatoire)"
            self.fields['type_service_fk'].queryset = TypeService.objects.filter(categorie_fk=categorie).order_by('abreviation_s')
            self.fields['instance_fk'].initial = instance_id
            self.fields['instance_fk'].widget = forms.HiddenInput()
            self.fields['adresse_s'].widget.attrs = {'row': 4}
            self.helper.layout = Layout(
                HTML(f'<div class="modal-header">'
                     f'<h5 class="modal-title">Ajouter un service</h5>'
                     f'<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'
                     f'</div>'
                     f'<div class="modal-body" id="modal-form-body">'),

                HTML('<div id="geographie-container">'),
                Fieldset(*fieldset_secteur),
                HTML('</div>'),
                Fieldset('Type du service instructeur',
                         'type_service_instructeur_s',
                         HTML('<div class="d-flex"><div class="col-12 position-relative">'),
                         'type_service_fk',
                         HTML(
                             '<div class="position-absolute" style="top: 0; right: 0">'
                             '<i class="ctx-help-type_service ms-2"></i>'
                             '</div></div></div>'),
                         Fieldset('Coordonnées',
                                  'email_s',
                                  'telephone_s',
                                  'adresse_s',
                                  ),

                         Fieldset('<i class="fa-solid fa-network-wired fa-fw"></i>Habilitations de ce service ',
                                  HTML(
                                      '<p class="inline-help">Associés aux interactions, ces réglages affinent le rôle du service au sein d\'une famille de services.<br> '
                                      'Dans le cas où le service serait indépendant et autonome, les réglages initiaux sont recommandés.<br> '
                                      '<strong>Consultez les documentations</strong> via les icônes d\'aide<i class="ctx-help-ex_aide_context"></i>.</p>'),
                                  HTML('<div class="d-flex"><div class="col-10">'),
                                  'admin_service_famille_b',
                                  HTML(
                                      '</div><div class="col-2 text-end"><i class="admin-famille"></i><i class="ctx-help-admin_service_famille ms-2"></i></div></div>'),
                                  HTML('<div class="d-flex"><div class="col-10">'),
                                  'admin_utilisateurs_subalternes_b',
                                  HTML('</div><div class="col-2 text-end">'
                                       '<i class="admin-subaltern"></i><i class="ctx-help-admin_utilisateurs_subalternes ms-2"></i></div></div>'),
                                  HTML('<div class="d-flex"><div class="col-10">'),
                                  'porte_entree_avis_b',
                                  HTML(
                                      '</div><div class="col-2 text-end">'
                                      '<i class="recevoir-avis"></i><i class="ctx-help-porte_entree_avis ms-2"></i>'
                                      '</div></div>'),
                                  HTML('<div class="d-flex"><div class="col-10">'),
                                  'autorise_rendre_avis_b',
                                  HTML(
                                      '</div><div class="col-2 text-end">'
                                      '<i class="rendre-avis"></i><i class="ctx-help-autorise_rendre_avis ms-2"></i>'
                                      '</div></div>'),
                                  HTML('<div class="d-flex"><div class="col-10">'
                                       '<span data-bs-toggle-quick="tooltip" '
                                       'data-bs-original-title="À n\'utiliser que dans des chaînes complexes" '
                                       'aria-label="À n\'utiliser que dans des chaînes complexes" '
                                       'title="À n\'utiliser que dans des chaînes complexes">'),
                                  'autorise_rendre_preavis_a_service_avis_b',
                                  HTML(
                                      '</div><div class="col-2 text-end">'
                                      '<i class="rendre-preavis"></i><i class="ctx-help-autorise_rendre_preavis_a_service_avis ms-2"></i>'
                                      '</div></div>'),
                                  HTML('<div class="d-flex"><div class="col-10">'
                                       '<span data-bs-toggle-quick="tooltip" '
                                       'data-bs-original-title="À n\'utiliser que dans des chaînes complexes" '
                                       'aria-label="À n\'utiliser que dans des chaînes complexes" '
                                       'title="À n\'utiliser que dans des chaînes complexes">'),
                                  'autorise_mandater_avis_famille_b',
                                  HTML(
                                      '</div><div class="col-2 text-end">'
                                      '<i class="mandater-avis-droit"></i><i class="ctx-help-autorise_mandater_avis_famille ms-2"></i>'
                                      '</div></div>'),
                                  HTML('<div class="d-flex"><div class="col-10">'),
                                  'autorise_interroge_preavis_famille_b',
                                  HTML(
                                      '</div><div class="col-2 text-end">'
                                      '<i class="interroge-preavis-droit"></i><i class="ctx-help-autorise_interroge_preavis_famille ms-2"></i>'
                                      '</div></div>'),
                                  HTML('<div class="d-flex"><div class="col-10">'),
                                  'autorise_acces_consult_organisateur_b',
                                  HTML(
                                      '</div><div class="col-2 text-end">'
                                      '<i class="consultation-organisateur"></i><i class="ctx-help-autorise_acces_consult_organisateur ms-2"></i>'
                                      '</div></div>'),
                                  ),

                         HTML('</div><div class="form-actions text-end">'),
                         HTML('<script src="/static/aide/contextHelp.jquery.js" type="text/javascript"></script>'),
                         StrictButton(
                             '<div class="form-actions text-end"> </div>%s' % "Valider",
                             type='submit',
                             css_class='btn btn-primary btn-form my-3 me-3 px-3 btn',
                             id="submit_create_service_form"
                         ),
                         HTML('</div>'),
                         )
                )

    def clean(self):
        cleaned_data = super().clean()
        if self.cleaned_data['arrondissement_m2m'] and self.cleaned_data['type_service_instructeur_s']:
            if ServiceInstructeur.objects.filter(arrondissement_m2m__in=self.cleaned_data['arrondissement_m2m'], type_service_instructeur_s=self.cleaned_data['type_service_instructeur_s']).exists():
                self.add_error('type_service_instructeur_s', 'Ce type de service instructeur existe déjà pour cette arrondissement')

        if self.cleaned_data['commune_m2m'] and self.cleaned_data['type_service_instructeur_s']:
            if ServiceInstructeur.objects.filter(commune_m2m__in=self.cleaned_data['commune_m2m'], type_service_instructeur_s=self.cleaned_data['type_service_instructeur_s']).exists():
                self.add_error('type_service_instructeur_s', 'Ce type de service instructeur existe déjà pour cette commune')
        if self.cleaned_data['type_service_instructeur_s'] == "prefecture":
            if ServiceInstructeur.objects.filter(instance_fk=self.cleaned_data['instance_fk'],
                                                 type_service_instructeur_s=self.cleaned_data['type_service_instructeur_s']).exists():
                self.add_error('type_service_instructeur_s',
                               'Une préfecture existe déjà sur cette instance')
        return cleaned_data
