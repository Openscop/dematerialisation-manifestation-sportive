from crispy_forms.layout import Layout, Fieldset, HTML
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import StrictButton

from django import forms
from django.urls import reverse

from administrative_division.models import Commune
from structure.models.organisation import StructureOrganisatrice, TypeOrganisation


class StructureOrganisatriceForm(forms.ModelForm):

    class Meta:
        model = StructureOrganisatrice
        fields = ['email_s',
                  'telephone_s',
                  'type_organisation_fk',
                  'precision_nom_s',
                  'adresse_s',
                  'site_web_s',
                  'instance_fk',
                  'commune_m2m',
                  ]

    def __init__(self, instance_id=None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["precision_nom_s"] = forms.CharField(
            label="Nom de la structure", max_length=200,
            help_text="Précisez le nom de la structure organisatrice (nom de l'association ou du club, de la société) de la manifestation"
        )
        if instance_id:
            self.fields['commune_m2m'] = forms.ModelMultipleChoiceField(
                queryset=Commune.objects.filter(arrondissement__departement__instance__pk=instance_id),
                label="Communes")
        elif self.instance.instance_fk:
            self.fields['commune_m2m'] = forms.ModelMultipleChoiceField(
                queryset=Commune.objects.filter(arrondissement__departement__instance__pk=self.instance.instance_fk.pk),
                label="Communes")
        self.fields['commune_m2m'].widget.attrs = {'class': 'chosen-select'}
        self.fields['type_organisation_fk'] = forms.ModelChoiceField(queryset=TypeOrganisation.objects.all(),
                                                                     label="Forme juridique de la structure",
                                                                     help_text="Opérez votre choix parmi les statuts juridiques proposés")

        self.helper = FormHelper()
        if self.instance.pk:
            self.fields['instance_fk'].widget = forms.HiddenInput()
            self.helper.form_action = reverse('structure:update_structure_organisatrice', kwargs={'pk': self.instance.pk})
            self.helper.layout = Layout(
                Fieldset('Éditer cette structure',
                         'email_s',
                         'telephone_s',
                         'type_organisation_fk',
                         'precision_nom_s',
                         'adresse_s',
                         'site_web_s',
                         'instance_fk',
                         'commune_m2m',
                         ),
                HTML('<div class="form-actions text-end">'),
                StrictButton(
                    '<div class="form-actions text-end"> </div>%s' % "Valider",
                    type='submit',
                    css_class='btn btn-primary btn-form my-3 px-3 btn',
                    ),
                HTML('</div>'),
            )
        else:
            self.fields['instance_fk'].initial = instance_id
            self.fields['instance_fk'].widget = forms.HiddenInput()
            self.helper.form_action = reverse('structure:create_structure_organisatrice')
            self.helper.layout = Layout(
                HTML(f'<div class="modal-header">'
                     f'<h5 class="modal-title">Ajouter une structure</h5>'
                     f'<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'
                     f'</div>'
                     f'<div class="modal-body" id="modal-form-body">'),
                Fieldset('',
                         'email_s',
                         'telephone_s',
                         'precision_nom_s',
                         'adresse_s',
                         'type_organisation_fk',
                         'site_web_s',
                         'instance_fk',
                         'commune_m2m',
                         ),
                HTML('</div><div class="form-actions text-end">'),
                StrictButton(
                    '<div class="form-actions text-end"> </div>%s' % "Valider",
                    type='submit',
                    css_class='btn btn-primary btn-form my-3 me-3 px-3 btn',
                    ),
                HTML('</div>'),
            )
