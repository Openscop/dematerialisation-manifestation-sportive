from crispy_forms.layout import Layout, Fieldset, HTML, Div
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import StrictButton

from django import forms
from django.urls import reverse

from core.models.instance import Instance
from administrative_division.models import Commune, Departement, Arrondissement, Region
from structure.models.service import ServiceConsulte, ServiceConsulteInteraction, TypeService, Federation


class ServiceConsulteForm(forms.ModelForm):

    is_service_regional = forms.BooleanField(label="Service regional", initial=False, required=False)
    is_service_departemental = forms.BooleanField(label="Service départemental", initial=False, required=False)
    is_service_arrondissement = forms.BooleanField(label="Service arrondissement", initial=False, required=False)
    is_service_commune = forms.BooleanField(label="Service communal", initial=False, required=False)

    class Meta:
        model = ServiceConsulte
        fields = "__all__"

    def __init__(self, categorie=None, instance_id=None, is_admin=False, is_support=False, user_organisation=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        fieldset_secteur = ['Portée territoriale', 'is_service_regional', 'is_service_departemental', 'is_service_arrondissement',
                            'arrondissement_m2m', 'is_service_commune', 'commune_m2m', 'instance_fk']
        self.is_support = is_support
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:create_service_consulte')
        if instance_id:
            instance = Instance.objects.get(pk=instance_id)
            self.fields['is_service_regional'].widget.attrs = {'class': 'geographie_checkbox',
                                                               'data-dep': f'({instance.departement.region.nom})'}
            self.fields['is_service_departemental'].widget.attrs = {'class': 'geographie_checkbox',
                                                                    'data-dep': f'({instance.departement.name})'}
            self.fields['is_service_arrondissement'].widget.attrs = {'class': 'geographie_checkbox'}
            self.fields['is_service_commune'].widget.attrs = {'class': 'geographie_checkbox'}

            if is_admin or is_support:
                self.fields['service_parent_fk'].required = False
                self.fields['service_parent_fk'].queryset = ServiceConsulte.objects.filter(
                    type_service_fk__categorie_fk__pk=categorie,
                    instance_fk__pk=instance_id).order_by("nom_s")
            else:
                self.fields['service_parent_fk'].required = True
                self.fields['service_parent_fk'].queryset = user_organisation.get_family_member_qs()
            if is_support:
                self.fields['departement_m2m'] = forms.ModelMultipleChoiceField(label="Departement", required=False,
                                                                                queryset=Departement.objects.all())
                self.fields['region_m2m'] = forms.ModelMultipleChoiceField(label="Region", required=False,
                                                                           queryset=Region.objects.all())
                self.fields['departement_m2m'].widget.attrs = {'class': 'chosen-select', 'data-placeholder': "Faites votre sélection"}
                self.fields['region_m2m'].widget.attrs = {'class': 'chosen-select', 'data-placeholder': "Faites votre sélection"}
                fieldset_secteur = fieldset_secteur[:2] + ['region_m2m'] + fieldset_secteur[2:3] + ['departement_m2m'] + fieldset_secteur[3:]

            self.fields['arrondissement_m2m'] = forms.ModelMultipleChoiceField(
                label="Arrondissements", required=False,
                queryset=Arrondissement.objects.filter(departement__instance__pk=instance_id))
            self.fields['commune_m2m'] = forms.ModelMultipleChoiceField(
                label="Communes", required=False,
                queryset=Commune.objects.filter(arrondissement__departement__instance__pk=instance_id))
            self.fields['commune_m2m'].widget.attrs = {'class': 'chosen-select', 'data-placeholder': "Faites votre sélection"}
            self.fields['arrondissement_m2m'].widget.attrs = {'class': 'chosen-select', 'data-placeholder': "Faites votre sélection"}

            self.fields['service_parent_fk'].widget.attrs = {'class': 'chosen-select'}
            self.fields['type_service_fk'].widget.attrs = {'class': 'chosen-select'}
            self.fields['precision_nom_s'].label = "Suffix / Précision du nom (facultatif)"
            self.fields['type_service_fk'].label = "Type de service (obligatoire)"
            self.fields['type_service_fk'].queryset = TypeService.objects.filter(categorie_fk=categorie).order_by('abreviation_s')
            self.fields['instance_fk'].initial = instance_id
            self.fields['instance_fk'].widget = forms.HiddenInput()
            self.fields['adresse_s'].widget.attrs = {'row': 4}
            self.helper.layout = Layout(
                HTML(f'<div class="modal-header">'
                     f'<h5 class="modal-title">Ajouter un service</h5>'
                     f'<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'
                     f'</div>'
                     f'<div class="modal-body" id="modal-form-body">'),

                HTML('<div id="geographie-container">'),
                Fieldset(*fieldset_secteur),
                HTML('</div>'),
                Fieldset('Nom du service',
                         HTML('<div class="d-flex"><div class="col-12 position-relative">'),
                         'type_service_fk',
                         HTML(
                             '<div class="position-absolute" style="top: 0; right: 0">'
                             '<i class="ctx-help-type_service ms-2"></i>'
                             '</div></div></div>'),
                         'precision_nom_s',
                         HTML('<div class="alert alert-info" role="alert"><span>Le nom du service sera : <span id="result-name">'
                              '<span id="type-result-name"></span>'
                              '<span id="precision-result-name"></span>'
                              '<span id="territoire-result-name" class="ms-1"></span>'
                              '</span></span></div>'),
                         ),
                Fieldset('Coordonnées',
                         'email_s',
                         'telephone_s',
                         'adresse_s'
                         ),
                # champs délais
                Fieldset('Délais',
                         'delai_jour_avis_int',
                         'delai_jour_preavis_int'
                         ),
                Fieldset('Hiérarchie',
                         'service_parent_fk',
                         ),

                HTML('</div><div class="form-actions text-end">'),
                StrictButton(
                    '<div class="form-actions text-end"> </div>%s' % "Valider",
                    type='submit',
                    css_class='btn btn-primary btn-form my-3 me-3 px-3 btn',
                    id="submit_create_service_form"
                ),
                HTML('</div>'),
            )

    def clean(self):
        cleaned_data = super().clean()
        if self.is_support:
            # Chercher une géographie renseignée
            if (not self.cleaned_data['arrondissement_m2m'] and not self.cleaned_data['commune_m2m'] and
                    not self.cleaned_data['departement_m2m'] and not self.cleaned_data['region_m2m']):
                self.add_error('departement_m2m', 'Pas de géographie renseignée')
            # Chercher un champ rempli et un seul
            liste = iter([self.cleaned_data['commune_m2m'], self.cleaned_data['arrondissement_m2m'],
                          self.cleaned_data['departement_m2m'], self.cleaned_data['region_m2m']])
            if not (any(liste) and not any(liste)):
                self.add_error('departement_m2m', 'Plusieurs géographies renseignées')
        else:
            # Chercher une géographie renseignée
            if (not self.cleaned_data['arrondissement_m2m'] and not self.cleaned_data['commune_m2m'] and
                    not self.cleaned_data['is_service_departemental'] and not self.cleaned_data['is_service_regional']):
                self.add_error('departement_m2m', 'Pas de géographie renseignée')
            # Chercher un champ rempli et un seul
            liste = iter([self.cleaned_data['commune_m2m'], self.cleaned_data['arrondissement_m2m'],
                          self.cleaned_data['is_service_departemental'], self.cleaned_data['is_service_regional']])
            if not (any(liste) and not any(liste)):
                self.add_error('departement_m2m', 'Plusieurs géographies renseignées')
        return cleaned_data

    def save(self, commit=True):
        if self.cleaned_data['is_service_departemental']:
            if not self.cleaned_data['departement_m2m']:
                self.cleaned_data['departement_m2m'] = Departement.objects.filter(instance=self.cleaned_data['instance_fk'])
        if self.cleaned_data['is_service_regional']:
            if not self.cleaned_data['region_m2m']:
                self.cleaned_data['region_m2m'] = Region.objects.filter(departement__instance=self.cleaned_data['instance_fk'])
        return super().save(commit=commit)


class FederationForm(ServiceConsulteForm):

    class Meta:
        model = Federation
        fields = "__all__"
        exclude = ('delai_jour_preavis_int', 'delai_jour_avis_int')

    def __init__(self, categorie=None, instance_id=None, is_support=False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        fieldset_secteur = ['Portée territoriale', 'is_service_regional', 'is_service_departemental',
                            'is_service_arrondissement',
                            'arrondissement_m2m', 'is_service_commune', 'commune_m2m', 'instance_fk']
        self.is_support = is_support
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:create_federation')

        if instance_id:
            instance = Instance.objects.get(pk=instance_id)
            self.fields['is_service_regional'].widget.attrs = {'class': 'geographie_checkbox',
                                                               'data-dep': f'({instance.departement.region.nom})'}
            self.fields['is_service_departemental'].widget.attrs = {'class': 'geographie_checkbox',
                                                                    'data-dep': f'({instance.departement.name})'}
            self.fields['is_service_arrondissement'].widget.attrs = {'class': 'geographie_checkbox'}
            self.fields['is_service_commune'].widget.attrs = {'class': 'geographie_checkbox'}

            if is_support:
                self.fields['departement_m2m'] = forms.ModelMultipleChoiceField(label="Departement", required=False,
                                                                                queryset=Departement.objects.all())
                self.fields['region_m2m'] = forms.ModelMultipleChoiceField(label="Region", required=False,
                                                                           queryset=Region.objects.all())
                self.fields['departement_m2m'].widget.attrs = {'class': 'chosen-select',
                                                               'data-placeholder': "Faites votre sélection"}
                self.fields['region_m2m'].widget.attrs = {'class': 'chosen-select',
                                                          'data-placeholder': "Faites votre sélection"}
                fieldset_secteur = fieldset_secteur[:2] + ['region_m2m'] + fieldset_secteur[2:3] + [
                    'departement_m2m'] + fieldset_secteur[3:]

            self.fields['arrondissement_m2m'] = forms.ModelMultipleChoiceField(
                label="Arrondissements", required=False,
                queryset=Arrondissement.objects.filter(departement__instance__pk=instance_id))
            self.fields['commune_m2m'] = forms.ModelMultipleChoiceField(
                label="Communes", required=False,
                queryset=Commune.objects.filter(arrondissement__departement__instance__pk=instance_id))
            self.fields['commune_m2m'].widget.attrs = {'class': 'chosen-select',
                                                       'data-placeholder': "Faites votre sélection"}
            self.fields['arrondissement_m2m'].widget.attrs = {'class': 'chosen-select',
                                                              'data-placeholder': "Faites votre sélection"}

            self.fields['type_service_fk'].widget.attrs = {'class': 'chosen-select'}
            self.fields['precision_nom_s'].label = "Suffix / Précision du nom (facultatif)"
            self.fields['type_service_fk'].label = "Type de service (obligatoire)"
            self.fields['type_service_fk'].queryset = TypeService.objects.filter(categorie_fk=categorie).order_by(
                'abreviation_s')
            self.fields['instance_fk'].initial = instance_id
            self.fields['instance_fk'].widget = forms.HiddenInput()
            self.fields['adresse_s'].widget.attrs = {'row': 4}
            self.helper.layout = Layout(
                HTML(f'<div class="modal-header">'
                     f'<h5 class="modal-title">Ajouter un service</h5>'
                     f'<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'
                     f'</div>'
                     f'<div class="modal-body" id="modal-form-body">'),

                HTML('<div id="geographie-container">'),
                Fieldset(*fieldset_secteur),
                HTML('</div>'),
                Fieldset('Nom du service',
                         HTML('<div class="d-flex"><div class="col-12 position-relative">'),
                         'type_service_fk',
                         HTML(
                             '<div class="position-absolute" style="top: 0; right: 0">'
                             '<i class="ctx-help-type_service ms-2"></i>'
                             '</div></div></div>'),
                         'discipline_fk'
                         ),
                Fieldset('Coordonnées',
                         'email_s',
                         'telephone_s',
                         'adresse_s',
                         ),
                HTML('</div><div class="form-actions text-end">'),
                HTML('<script src="/static/aide/contextHelp.jquery.js" type="text/javascript"></script>'),
                StrictButton(
                    '<div class="form-actions text-end"> </div>%s' % "Valider",
                    type='submit',
                    css_class='btn btn-primary btn-form my-3 me-3 px-3 btn',
                    id="submit_create_service_form"
                ),
                HTML('</div>'),
            )


class ServiceConsulteActionForm(forms.ModelForm):

    class Meta:
        model = ServiceConsulte
        fields = [
            'admin_service_famille_b',
            'admin_utilisateurs_subalternes_b',
            'porte_entree_avis_b',
            'autorise_rendre_avis_b',
            'autorise_rendre_preavis_a_service_avis_b',
            'autorise_acces_consult_organisateur_b',
            'autorise_mandater_avis_famille_b',
            'autorise_interroge_preavis_famille_b',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:update_service_consulte_action', kwargs={'pk': self.instance.pk})
        champs_admin_service_famille = champs_autorise_rendre_preavis_a_service_avis_b = champs_autorise_mandater_avis_famille = champs_autorise_interroge_preavis_famille = ""
        if self.instance.is_membre_famille():
            champs_admin_service_famille = [
                HTML('<div class="d-flex"><div class="col-10">'),
                'admin_service_famille_b',
                HTML(
                    '</span></div><div class="col-2 text-end"><i class="admin-famille"></i><i class="ctx-help-admin_service_famille ms-2"></i></div></div>')
            ]
            champs_autorise_rendre_preavis_a_service_avis_b = [
                HTML('<div class="d-flex"><div class="col-10">'
                     '<span data-bs-toggle-quick="tooltip" '
                     'data-bs-original-title="À n\'utiliser que dans des chaînes complexes" '
                     'aria-label="À n\'utiliser que dans des chaînes complexes" '
                     'title="À n\'utiliser que dans des chaînes complexes">'),
                'autorise_rendre_preavis_a_service_avis_b',
                HTML(
                    '</div><div class="col-2 text-end">'
                    '<i class="rendre-preavis"></i><i class="ctx-help-autorise_rendre_preavis_a_service_avis ms-2"></i>'
                    '</div></div>'),
            ]
            champs_autorise_mandater_avis_famille = [
                HTML('<div class="d-flex"><div class="col-10">'
                     '<span data-bs-toggle-quick="tooltip" '
                     'data-bs-original-title="À n\'utiliser que dans des chaînes complexes" '
                     'aria-label="À n\'utiliser que dans des chaînes complexes" '
                     'title="À n\'utiliser que dans des chaînes complexes">'),
                'autorise_mandater_avis_famille_b',
                HTML(
                    '</div><div class="col-2 text-end">'
                    '<i class="mandater-avis-droit"></i><i class="ctx-help-autorise_mandater_avis_famille ms-2"></i>'
                    '</div></div>'),
            ]
            champs_autorise_interroge_preavis_famille = [
                HTML('<div class="d-flex"><div class="col-10">'),
                'autorise_interroge_preavis_famille_b',
                HTML(
                    '</div><div class="col-2 text-end">'
                    '<i class="interroge-preavis-droit"></i><i class="ctx-help-autorise_interroge_preavis_famille ms-2"></i>'
                    '</div></div>'),
            ]

        self.helper.layout = Layout(
            Fieldset(
                '<i class="fa-solid fa-network-wired fa-fw"></i>Habilitations de ce service ',
                HTML(
                    '<p class="inline-help">Associés aux interactions, ces réglages affinent le rôle du service au sein d\'une famille de services.<br> '
                    'Dans le cas où le service serait indépendant et autonome, les réglages initiaux sont recommandés.<br> '
                    '<strong>Consultez les documentations</strong> via les icônes d\'aide<i class="ctx-help-ex_aide_context"></i>.</p>'),
                 HTML('<div class="d-flex"><div class="col-10">'),
                 'porte_entree_avis_b',
                 HTML(
                     '</div><div class="col-2 text-end">'
                     '<i class="recevoir-avis"></i><i class="ctx-help-porte_entree_avis ms-2"></i>'
                     '</div></div>'),
                 HTML('<div class="d-flex"><div class="col-10">'),
                 'autorise_rendre_avis_b',
                 HTML(
                     '</div><div class="col-2 text-end">'
                     '<i class="rendre-avis"></i><i class="ctx-help-autorise_rendre_avis ms-2"></i>'
                     '</div></div>'),
                HTML('<div class="d-flex"><div class="col-10">'),
                'autorise_acces_consult_organisateur_b',
                HTML(
                    '</div><div class="col-2 text-end">'
                    '<i class="consultation-organisateur"></i><i class="ctx-help-autorise_acces_consult_organisateur ms-2"></i>'
                    '</div></div>'),
                ),
                HTML('<p id="habilitations-avancees-link">Afficher les habilitations avancées</p>'
                     '<div id="habilitations-avancees" style="display: none">'),
                *champs_autorise_rendre_preavis_a_service_avis_b,
                *champs_autorise_mandater_avis_famille,
                *champs_autorise_interroge_preavis_famille,
                *champs_admin_service_famille,
                HTML('<div class="d-flex"><div class="col-10">'),
                'admin_utilisateurs_subalternes_b',
                HTML('</span></div><div class="col-2 text-end">'
                     '<i class="admin-subaltern"></i><i class="ctx-help-admin_utilisateurs_subalternes ms-2"></i>'
                     '</div></div></div>'),
                HTML('<div class="form-actions text-end">'),
                StrictButton(
                    '<div class="form-actions text-end"> </div>%s' % "<i class=\"fa-regular fa-floppy-disk\"></i>Enregistrer",
                    type='submit',
                    css_class='btn btn-primary btn-form my-3 px-3 btn',
                    ),
                HTML('</div>'),
                HTML('<script src="/static/aide/contextHelp.jquery.js" type="text/javascript"></script>'),
        )


class ServiceConsulteInteractionForm(forms.ModelForm):

    class Meta:
        model = ServiceConsulteInteraction
        fields = ['service_sortant_fk', 'type_lien_s', 'service_entrant_fk']

    def __init__(self, service_id=None, pk=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        service = ServiceConsulte.objects.get(pk=service_id)
        self.helper = FormHelper()
        self.fields['service_sortant_fk'].queryset = service.get_family_member_qs().order_by("nom_s")
        self.fields['service_entrant_fk'].queryset = service.get_family_member_qs().order_by("nom_s")
        if pk:
            self.helper.form_action = reverse('structure:update_interaction', kwargs={'service_id': service_id, 'pk': pk})
        else:
            self.helper.form_action = reverse('structure:create_interaction', kwargs={'service_id': service_id})

        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            Fieldset(
                '',
                'service_entrant_fk',
                HTML('<div class="d-flex"><div class="col-12 position-relative">'),
                'type_lien_s',
                HTML(
                    '<div class="position-absolute" style="top: 0; right: 0">'
                    '<i class="ctx-help-liste_interactions ms-2"></i>'
                    '</div></div></div>'),
                'service_sortant_fk',
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
            HTML('<script src="/static/aide/contextHelp.jquery.js" type="text/javascript"></script>'),
        )


class ServiceConsulteSecteurForm(forms.ModelForm):

    is_service_regional = forms.BooleanField(label="Service regional", initial=False, required=False)
    is_service_departemental = forms.BooleanField(label="Service départemental", initial=False, required=False)
    is_service_arrondissement = forms.BooleanField(label="Service arrondissement", initial=False, required=False)
    is_service_commune = forms.BooleanField(label="Service communal", initial=False, required=False)

    class Meta:
        model = ServiceConsulte
        fields = ["region_m2m", "departement_m2m", "arrondissement_m2m", "commune_m2m"]

    def __init__(self, is_support=False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        fieldset_secteur = ['',
                HTML('<div class="d-flex form-action-field-container" id="div-is_service_regional"><div class="col-11">'),
                'is_service_regional',
                HTML('</div></div>'),
                HTML('<div class="d-flex form-action-field-container" id="div-is_service_departemental"><div class="col-11">'),
                'is_service_departemental',
                HTML('</div></div>'),
                HTML('<div class="d-flex form-action-field-container" id="div-is_service_arrondissement"><div class="col-11">'),
                'is_service_arrondissement',
                HTML('</div></div>'),
                'arrondissement_m2m',
                HTML('<div class="d-flex form-action-field-container" id="div-is_service_commune"><div class="col-11">'),
                'is_service_commune',
                HTML('</div></div>'),
                'commune_m2m']
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:update_service_consulte_secteur', kwargs={'pk': self.instance.pk})
        self.fields['arrondissement_m2m'] = forms.ModelMultipleChoiceField(
            queryset=Arrondissement.objects.filter(departement__instance=self.instance.instance_fk), required=False,
            label="Arrondissements")
        self.fields['commune_m2m'] = forms.ModelMultipleChoiceField(
            queryset=Commune.objects.filter(arrondissement__departement__instance=self.instance.instance_fk),
            required=False, label="Communes")
        self.fields['commune_m2m'].widget.attrs = {'class': 'chosen-select', 'data-placeholder': "Faites votre sélection"}
        self.fields['arrondissement_m2m'].widget.attrs = {'class': 'chosen-select', 'data-placeholder': "Faites votre sélection"}
        self.fields['departement_m2m'].widget.attrs = {'class': 'chosen-select'}
        if is_support:
            self.fields['departement_m2m'] = forms.ModelMultipleChoiceField(label="Departement", required=False,
                                                                            queryset=Departement.objects.all())
            self.fields['region_m2m'] = forms.ModelMultipleChoiceField(label="Region", required=False,
                                                                       queryset=Region.objects.all())
            self.fields['departement_m2m'].widget.attrs = {'class': 'chosen-select', 'data-placeholder': "Faites votre sélection"}
            self.fields['region_m2m'].widget.attrs = {'class': 'chosen-select', 'data-placeholder': "Faites votre sélection"}
            fieldset_secteur = fieldset_secteur[:4] + ['region_m2m'] + fieldset_secteur[4:7] + ['departement_m2m'] + fieldset_secteur[7:]

        self.fields['is_service_regional'].widget.attrs = {'class': 'geographie_checkbox'}
        self.fields['is_service_departemental'].widget.attrs = {'class': 'geographie_checkbox'}
        self.fields['is_service_arrondissement'].widget.attrs = {'class': 'geographie_checkbox'}
        self.fields['is_service_commune'].widget.attrs = {'class': 'geographie_checkbox'}

        if self.instance.get_niveau_geographique_str() == "Régional":
            self.fields['is_service_regional'].initial = True
        elif self.instance.get_niveau_geographique_str() == "Départemental":
            self.fields['is_service_departemental'].initial = True
        elif self.instance.get_niveau_geographique_str() == "Arrondissement":
            self.fields['is_service_arrondissement'].initial = True
        elif self.instance.get_niveau_geographique_str() == "Communal":
            self.fields['is_service_commune'].initial = True

        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            HTML('<div id="geographie-container">'),
            Fieldset(*fieldset_secteur),
            HTML('</div></div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )

    def save(self, commit=True):
        if self.cleaned_data['is_service_departemental']:
            if not self.cleaned_data['departement_m2m']:
                self.cleaned_data['departement_m2m'] = Departement.objects.filter(instance=self.instance.instance_fk)
        if self.cleaned_data['is_service_regional']:
            if not self.cleaned_data['region_m2m']:
                self.cleaned_data['region_m2m'] = Region.objects.filter(departement__instance=self.instance.instance_fk)
        return super().save(commit=commit)
