from crispy_forms.layout import Layout, Fieldset, HTML
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import StrictButton

from django import forms
from django.urls import reverse

from core.models.instance import Instance
from structure.models.service import ServiceConsulte, TypeService
from structure.models.organisation import Organisation


class ServiceConsulteNomForm(forms.ModelForm):

    class Meta:
        model = ServiceConsulte
        fields = ["precision_nom_s"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()

        self.helper.form_action = reverse('structure:update_service_consulte_nom', kwargs={'pk': self.instance.pk})
        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            Fieldset(
                '',
                HTML(
                    "<span>Le nom d'un service se compose de son type de service, "
                    "son secteur et son suffix, si renseigné, de la maniére suivante : <br>"
                    "'Type de service' 'suffix' 'secteur'</span>"),
                'precision_nom_s',
                HTML(
                    '<div class="alert alert-info" role="alert"><span>Le nom du service sera : <span id="result-name">'
                    '<span id="type-result-name"></span>'
                    '<span id="precision-result-name"></span>'
                    '<span id="territoire-result-name" class="ms-1"></span>'
                    '</span></span></div>'),
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )


class ServiceConsulteAdresseForm(forms.ModelForm):

    class Meta:
        model = Organisation
        fields = ["adresse_s"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:update_service_consulte_adresse', kwargs={'pk': self.instance.pk})
        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            Fieldset(
                '',
                'adresse_s',
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )


class ServiceConsulteTelephoneForm(forms.ModelForm):

    class Meta:
        model = Organisation
        fields = ["telephone_s"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:update_service_consulte_telephone', kwargs={'pk': self.instance.pk})
        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            Fieldset(
                '',
                'telephone_s',
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )


class ServiceConsulteInstanceForm(forms.ModelForm):

    class Meta:
        model = Organisation
        fields = ["instance_fk"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:update_service_consulte_instance', kwargs={'pk': self.instance.pk})
        self.fields['instance_fk'].queryset = Instance.objects.order_by('departement')
        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            Fieldset(
                '',
                'instance_fk',
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )


class ServiceConsulteSiteForm(forms.ModelForm):

    class Meta:
        model = Organisation
        fields = ["site_web_s"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:update_service_consulte_site', kwargs={'pk': self.instance.pk})
        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            Fieldset(
                '',
                'site_web_s',
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )


class ServiceConsulteEmailForm(forms.ModelForm):

    class Meta:
        model = Organisation
        fields = ["email_s"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:update_service_consulte_email', kwargs={'pk': self.instance.pk})
        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            Fieldset(
                '',
                'email_s',
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )


class ServiceConsulteDelaiAvisForm(forms.ModelForm):

    class Meta:
        model = ServiceConsulte
        fields = ["delai_jour_avis_int"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:update_service_consulte_delai_avis', kwargs={'pk': self.instance.pk})
        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            Fieldset(
                '',
                'delai_jour_avis_int',
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )


class ServiceConsulteDelaiPreavisForm(forms.ModelForm):

    class Meta:
        model = ServiceConsulte
        fields = ["delai_jour_preavis_int"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:update_service_consulte_delai_preavis', kwargs={'pk': self.instance.pk})
        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            Fieldset(
                '',
                'delai_jour_preavis_int',
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )


class ServiceConsulteTypeForm(forms.ModelForm):

    class Meta:
        model = ServiceConsulte
        fields = ["type_service_fk"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields['type_service_fk'].queryset = TypeService.objects.filter(
            categorie_fk=self.instance.type_service_fk.categorie_fk).order_by('abreviation_s')
        self.helper.form_action = reverse('structure:update_service_consulte_type', kwargs={'pk': self.instance.pk})
        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            Fieldset(
                '',
                HTML('<div class="d-flex"><div class="col-12 position-relative">'),
                'type_service_fk',
                HTML(
                    '<div class="position-absolute" style="top: 0; right: 0">'
                    '<i class="ctx-help-type_service ms-2"></i>'
                    '</div></div></div'),
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            HTML('<script src="/static/aide/contextHelp.jquery.js" type="text/javascript"></script>'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )


class ServiceConsulteParentForm(forms.ModelForm):

    class Meta:
        model = ServiceConsulte
        fields = ["service_parent_fk"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields['service_parent_fk'].queryset = ServiceConsulte.objects.filter(
            type_service_fk__categorie_fk=self.instance.type_service_fk.categorie_fk,
            instance_fk=self.instance.instance_fk).exclude(pk=self.instance.pk)
        self.fields['service_parent_fk'].widget.attrs = {'class': 'chosen-select'}
        self.helper.form_action = reverse('structure:update_service_consulte_parent', kwargs={'pk': self.instance.pk})
        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            Fieldset(
                '',
                'service_parent_fk',
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )

    def clean(self):
        cleaned_data = super().clean()
        if self.cleaned_data["service_parent_fk"]:
            service_parent = ServiceConsulte.objects.get(pk=cleaned_data["service_parent_fk"])
            if service_parent in self.instance.get_all_children_list():
                self.add_error('service_parent_fk',
                               'Un service ne peut pas avoir l\'un de ses enfants en tant que service parent.')
        return cleaned_data


class ServiceConsulteActiveForm(forms.ModelForm):

    class Meta:
        model = ServiceConsulte
        fields = ["is_active_b"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:update_service_consulte_active', kwargs={'pk': self.instance.pk})
        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            Fieldset(
                '',
                'is_active_b',
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )
