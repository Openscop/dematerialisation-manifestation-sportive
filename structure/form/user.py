import dns
from crispy_forms.layout import Layout, Fieldset, HTML
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import StrictButton

from django import forms
from django.urls import reverse

from structure.models.service import ServiceConsulte
from core.models.user import User


class UserActiveForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ["is_active"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:update_user_active', kwargs={'pk': self.instance.pk})
        alert =""
        if self.instance.is_active:
            alert = "<div class='alert alert-warning'><strong>Attention : </strong> Si vous désactivez cet utilisateur, " \
                    "tous ses groupes lui seront retirés.</div> "
        self.helper.layout = Layout(
            HTML(f'<div class="modal-body">{alert}'),
            Fieldset(
                '',
                'is_active',
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )

    def clean(self):
        super().clean()
        user = self.instance
        if not user.is_active:
            if not user.email or user.emailaddress_set.first() and not user.emailaddress_set.first().verified:
                raise forms.ValidationError('Vous devez renseigner une adresse e-mail, et la faire validée,  avant de pouvoir activé cet utilisateur')
        return self.cleaned_data


class UserEmailForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ["email"]

    def __init__(self, request_user_pk, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse('structure:update_user_email', kwargs={'pk': self.instance.pk})
        alerte = ""
        # S'il s'agit d'une modification de sa propre adresse affiche une alerte
        if self.instance.pk == request_user_pk:
            alerte = "<div class='alert alert-danger'><strong>Attention :</strong> si vous changer d'adresse e-mail" \
                      " vous devrez la valider avant votre prochaine connection.</div>"
        self.helper.layout = Layout(
            HTML(f'<div class="modal-body">{alerte}'),
            Fieldset(
                '',
                'email',
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-inter'
            ),
            HTML('</div>'),
        )

    def clean(self):
        super().clean()
        if not self.cleaned_data['email']:
            raise forms.ValidationError('Veuillez renseigner une adresse email')
        domaine = self.cleaned_data['email'].split('@')[1]
        try:
            dns.resolver.query(domaine, 'MX', tcp=True)
        except:
            raise forms.ValidationError('Erreur de domaine')
        users = User.objects.filter(email=self.cleaned_data['email'])
        if users:
            raise forms.ValidationError('Adresse email déjà utilisée')
        return self.cleaned_data

    def save(self, commit=True):
        user = super(UserEmailForm, self).save(commit=commit)
        user.emailaddress_set.all().delete()
        from allauth.account.models import EmailAddress
        email = EmailAddress(user=user, email=user.email, primary=True)
        email.save()
        email.send_confirmation()
        return user


class UserOrganisationForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['organisation_m2m']

    def __init__(self, user_id=None, service_id=None, pk=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        user = User.objects.get(pk=pk)
        self.helper = FormHelper()
        categories = []
        for organisation in user.organisation_m2m.all():
            if organisation.type_service_fk:
                categories.append(organisation.type_service_fk.categorie_fk.pk)
        self.fields['organisation_m2m'].queryset = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__pk__in=categories)
        self.fields['organisation_m2m'].widget.attrs = {'class': 'chosen-select'}
        self.helper.form_action = reverse('structure:update_user_organisation', kwargs={'pk': pk,
                                                                                        'service_id': service_id})

        self.helper.layout = Layout(
            HTML('<div class="modal-body">'),
            Fieldset(
                '',
                'organisation_m2m',
            ),
            HTML('</div>'),
            HTML('<div class="modal-footer">'),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form float-end',
                id='submit-edit-user-organisation'
            ),
            HTML('</div>'),
        )
