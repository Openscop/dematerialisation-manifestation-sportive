from django.utils import timezone
from django import forms
from django.urls import reverse
from crispy_forms.helper import FormHelper
from bootstrap_datepicker_plus import DatePickerInput, TimePickerInput

from structure.models import ReunionCDSR, PresentationCDSR
from crispy_forms.layout import Layout, Fieldset, HTML


class DateReunionForm(forms.ModelForm):

    date = forms.DateField(label='Date de la réunion', required=False, widget=DatePickerInput(
        options={"format": "DD/MM/YYYY",  'locale': 'fr'},
        attrs={"class": "datetime_picker_ar"}))

    class Meta:
        model = ReunionCDSR
        fields = ('nom_s', 'date')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'formDateReunion'
        self.helper.form_class = 'formDateModal'
        self.helper.form_action = reverse('structure:reunionCDSR_create')
        self.helper.layout = Layout(
            HTML('<div>'
                 'L\'un des deux champs est obligatoire.<br>'
                 'si la date de cette nouvelle réunion reste à définir, vous pouvez lui donner un nom à la place.<br>'
                 '</div>'),
            Fieldset('', 'nom_s', 'date'),
            HTML('<div style="display: none" id="message-erreur-date"></div>'
                 '<div class="btn-container mt-2">'
                 '<input class="btn btn-warning submit_formDate" '
                 'data="#formDateReunion" type="submit" value="Enregistrer">'
                 '<input class="btn btn-secondary" data-bs-dismiss="modal" value="Annuler">'
                 '</div>'))

    def clean_nom_s(self):
        nom = self.cleaned_data['nom_s']
        if nom and nom.find('_') != -1:
            message = "Caractère interdit '_' !"
            # erreur envoyée dans le champ "date" car ce champ est renvoyé en réponse si form.invalid
            self.add_error("date", self.error_class([message]))
        return nom

    def clean(self):
        cleaned_data = super().clean()
        nom = cleaned_data['nom_s']
        date = cleaned_data['date']
        if date:
            cleaned_data['nom_s'] = ""
            if date < timezone.now().date():
                message = "La date limite de réponse ne peut être antérieure à aujourd'hui."
                self.add_error("date", self.error_class([message]))
            if ReunionCDSR.objects.filter(date=date).exists():
                message = "La date sélectionnée correspond à une réunion CDSR"
                self.add_error("date", self.error_class([message]))
        elif nom:
            if ReunionCDSR.objects.filter(nom_s=nom).exists():
                message = "Une réunion avec ce nom existe déjà."
                self.add_error("date", self.error_class([message]))

        else:
            message = "Un des deux champs doit être rempli."
            self.add_error("date", self.error_class([message]))
        return cleaned_data


class HeurePresentationForm(forms.ModelForm):

    heure = forms.TimeField(label='', widget=TimePickerInput(
        options={"format": "HH:mm", 'locale': 'fr'},
        attrs={"class": "datetime_picker_ar"}))

    class Meta:
        model = PresentationCDSR
        fields = ('heure',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'formHeurePresentation'
        self.helper.form_class = 'formDateModal'
        self.helper.form_action = reverse('structure:presentationcdsr_set_heure', kwargs={'pk': self.instance.pk})
        self.helper.layout = Layout(
            Fieldset('', 'heure'),
            HTML('<div style="display: none" id="message-erreur-date"></div>'
                 '<div class="btn-container mt-2">'
                 '<input class="btn btn-warning submit_formDate" '
                 'data="#formHeurePresentation" type="submit" value="Enregistrer">'
                 '<input class="btn btn-secondary" data-bs-dismiss="modal" value="Annuler">'
                 '</div>'))


class ConvocationReunionForm(forms.ModelForm):

    precision = forms.CharField(max_length=500, label='Précision à apporter', widget=forms.Textarea(attrs={'rows': 3}),
                                help_text='Indiquer le lieu où la réunion CDSR doit se tenir.')
    fichier = forms.FileField(label="Ordre de jour")

    class Meta:
        model = ReunionCDSR
        fields = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'
        self.helper.layout = Layout(
            Fieldset('', 'precision', 'fichier'),
            HTML('<div class="btn-container mt-2 col-6">'
                 '<input class="btn btn-warning submit_formDate w-25" type="submit" value="Enregistrer">'
                 '</div>'))

    def clean_fichier(self):
        """ Vérifier le champ fichier """
        data = self.cleaned_data['fichier']
        if not data:
            raise forms.ValidationError("Vous devez spécifier un fichier contenant l'ordre du jour.")
        return data

    def clean_precision(self):
        """ Vérifier le champ precision """
        data = self.cleaned_data['precision']
        if not data:
            raise forms.ValidationError("Vous devez indiquer le lieu de tenue de la réunion CDSR.")
        return data


class ConvocationPresentationForm(forms.ModelForm):

    precision = forms.CharField(max_length=500, label='Précision à apporter', widget=forms.Textarea(attrs={'rows': 3}),
                                help_text='Indiquer le lieu où la réunion CDSR doit se tenir.')
    fichier = forms.FileField(label="Ordre de jour")

    class Meta:
        model = PresentationCDSR
        fields = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'
        self.helper.layout = Layout(
            Fieldset('', 'precision', 'fichier'),
            HTML('<div class="btn-container mt-2 col-6">'
                 '<input class="btn btn-warning submit_formDate w-25" type="submit" value="Enregistrer">'
                 '</div>'))

    def clean_fichier(self):
        """ Vérifier le champ fichier """
        data = self.cleaned_data['fichier']
        if not data:
            raise forms.ValidationError("Vous devez spécifier un fichier contenant l'ordre du jour.")
        return data

    def clean_precision(self):
        """ Vérifier le champ precision """
        data = self.cleaned_data['precision']
        if not data:
            raise forms.ValidationError("Vous devez indiquer le lieu de tenue de la réunion CDSR.")
        return data
