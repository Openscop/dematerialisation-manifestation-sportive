from rest_framework import viewsets

from carto.models.poi import PoiApi
from carto.serializers import PoiApiSerializer
from rest_framework.permissions import AllowAny


class PoiView(viewsets.ModelViewSet):
    """
    Retourne la liste des POIS supporté par la plateforme

    Pour prévisualiser les POIS [voir ici][ref].

    [ref]: https://manifestationsportive.fr/aide/pois
    """
    queryset = PoiApi.objects.all()
    serializer_class = PoiApiSerializer
    permission_classes = [AllowAny]
