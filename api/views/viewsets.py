import logging
from django_filters.rest_framework import FilterSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from django_filters.rest_framework import DjangoFilterBackend, ModelChoiceFilter

from rest_framework import viewsets, filters
from drf_yasg.utils import swagger_auto_schema

from api.serializer.serializers import (StructureOrganisatriceLightSerializer, StructureOrganisatriceSerializer,
                                        SiteN2KSerializer, N2kOperateurSiteSerilizer)
from api.permissions import IsInGroupApiDefault, IsAppliEnvironnement
from administrative_division.models import Departement, Arrondissement
from structure.models import StructureOrganisatrice
from evaluation_incidence.models import N2kSite

api_logger = logging.getLogger('api')


class OrganisateurFilter(FilterSet):
    departement = ModelChoiceFilter(field_name="commune__arrondissement__departement",
                                    label='departement',
                                    queryset=Departement.objects.all())
    arrondissement = ModelChoiceFilter(field_name="commune__arrondissement",
                                       label='arrondissement',
                                       queryset=Arrondissement.objects.all())

    class Meta:
        model = StructureOrganisatrice
        fields = ('departement', 'arrondissement', 'commune_m2m')


class OrganisateurViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vue des organisateurs basée sur le modèle Structure\n
    Accessible par les utilisateurs du groupe _API_default
    Recherche possible sur les champs 'organisateur' et 'structure' avec le bouton 'filtres'
    """
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter,)
    queryset = StructureOrganisatrice.objects.all()
    serializer_class = StructureOrganisatriceLightSerializer
    permission_classes = (IsInGroupApiDefault, )
    filter_class = OrganisateurFilter
    detail_serializer_class = StructureOrganisatriceSerializer
    search_fields = ('name', 'organisateur__user__username')
    ordering_fields = ('name', 'commune')

    """
    Utilisation de la fonction finalize_response pour bénéficier des variables request et response
    dans lesquelles se trouvent les infos à logger
    """
    def finalize_response(self, request, response, *args, **kwargs):
        if 'count' in response.data:
            count = response.data['count']
        else:
            count = 'none'
        api_logger.info("Utilisateur %s | %s de %s | %s | réponse %s, %s | items : %s" %
                        (request.user, request.method, self.request.get_host(), self.request.get_full_path(),
                         response.status_code, response.status_text, count
                         ))
        return super().finalize_response(request, response, *args, **kwargs)

    def get_serializer_class(self):
        if self.action == 'retrieve':
            if hasattr(self, 'detail_serializer_class'):
                return self.detail_serializer_class

        return super().get_serializer_class()

    def retrieve(self, request, *args, **kwargs):
        """
        Détail d'une structure d'organisateur.\n
        Accessible par les utilisateurs du groupe _API_default
        """
        return super().retrieve(request, *args, **kwargs)


class SiteN2KViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vue des sites Natura 2000\n
    Accessible par les utilisateurs du groupe _API_environnement
    """
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter,)
    queryset = N2kSite.objects.all()
    serializer_class = SiteN2KSerializer
    permission_classes = (IsAuthenticated, IsAppliEnvironnement)
    # filter_class = OrganisateurFilter
    filter_fields = ('departements__instance', )
    search_fields = ('nom', 'site_type', 'index')
    ordering_fields = ('nom', 'index')

    """
    Utilisation de la fonction finalize_response pour bénéficier des variables request et response
    dans lesquelles se trouvent les infos à logger
    """
    def finalize_response(self, request, response, *args, **kwargs):
        if 'count' in response.data:
            count = response.data['count']
        else:
            count = 'none'
        api_logger.info("Utilisateur %s | %s de %s | %s | réponse %s, %s | items : %s" %
                        (request.user, request.method, self.request.get_host(), self.request.get_full_path(),
                         response.status_code, response.status_text, count
                         ))
        return super().finalize_response(request, response, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        """
        Détail d'un site Natura 2000.\n
        Accessible par les utilisateurs du groupe _API_environnement
        """
        return super().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(responses={'200': N2kOperateurSiteSerilizer})
    @action(methods=['get'], detail=True)
    def OperateurSite(self, request, pk=None, *args, **kwargs):
        """
        Détail de l'opérateur du site Natura2000\n
        {id} est le numéro du site Natura2000.
        """
        site = self.get_object()
        if not hasattr(site, 'operateursiten2k'):
            return Response({"detail": "Pas d'opérateur pour ce site."}, status=404)
        operateur = site.operateursiten2k
        serializer = N2kOperateurSiteSerilizer(operateur)
        return Response(serializer.data)
