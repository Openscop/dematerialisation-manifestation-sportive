from django.test import TestCase
from django.utils import timezone
from django.contrib.auth import hashers
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType

from core.factories import ApplicationFactory
from structure.factories.service import ServiceConsulteFactory
from structure.factories.structure import StructureOrganisatriceFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from core.factories import UserFactory
from sports.factories.sport import ActiviteFactory
from evenements.factories.manif import DcnmFactory, AvtmFactory, PieceJointeFactory
from instructions.factories import (InstructionFactory, AvisInstructionFactory, DocOfficielFactory)
from evaluation_incidence.factories import N2kEvalFactory, RNREvalFactory, N2kSiteFactory, N2kOperateurSiteFactory
from carto.factories import ParcoursFactory


class ApiTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        print()
        print('========= Test Api (Clt) ==========')

        # Création des lieux
        dep = DepartementFactory.create(name='42')
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        prefecture = arrondissement.organisations.first()
        autre_dep = DepartementFactory.create(name='43')
        autre_arrondissement = ArrondissementFactory.create(name='autre', code='99', departement=autre_dep)
        autre_prefecture = autre_arrondissement.organisations.first()
        commune1 = CommuneFactory(name='Bard', arrondissement=arrondissement)
        commune2 = CommuneFactory(name='Bully', arrondissement=arrondissement)
        cls.ddsp = dep.organisation_set.get(precision_nom_s="DDSP")
        sdis = dep.organisation_set.get(precision_nom_s="SDIS")
        commiss = ServiceConsulteFactory.create(instance_fk=dep.instance,
                                                precision_nom_s="Commissariat",
                                                type_service_fk__categorie_fk__nom_s="Police",
                                                type_service_fk__nom_s="Commissariat",
                                                departement_m2m=dep)

        # Création des utilisateurs
        user1 = UserFactory.create(username='api_def_user', password=hashers.make_password("123"),
                                   default_instance=dep.instance)
        user2 = UserFactory.create(username='api_env_user', password=hashers.make_password("123"),
                                   default_instance=dep.instance)
        cls.instructeur = instruc = UserFactory.create(username='instructeur', password=hashers.make_password("123"),
                                                       default_instance=dep.instance)
        instruc.organisation_m2m.add(prefecture)
        cls.instructeur2 = instruc2 = UserFactory.create(username='instructeur2', password=hashers.make_password("123"),
                                                         default_instance=autre_dep.instance)
        instruc2.organisation_m2m.add(autre_prefecture)
        cls.agent = agent = UserFactory.create(username='agent', password=hashers.make_password("123"),
                                               default_instance=dep.instance)
        agent.organisation_m2m.add(cls.ddsp)
        cls.autre_agent = autre_agent = UserFactory.create(username='agent2', password=hashers.make_password("123"),
                                                           default_instance=dep.instance)
        autre_agent.organisation_m2m.add(sdis)
        cls.agentlocal = agentlocal = UserFactory.create(username='agentlocal', password=hashers.make_password("123"),
                                                         default_instance=dep.instance)
        agentlocal.organisation_m2m.add(commiss)
        group1 = Group.objects.create(name='_API_default')
        group2 = Group.objects.create(name='_API_services_consultes')
        group3 = Group.objects.create(name='_API_environnement')
        user1.groups.add(group1)
        user2.groups.add(group3)
        instruc.groups.add(group2)
        instruc2.groups.add(group2)
        agent.groups.add(group2)
        autre_agent.groups.add(group2)
        agentlocal.groups.add(group2)

        ContentType.objects.clear_cache()
        # Création d'une structure
        struc = StructureOrganisatriceFactory.create(precision_nom_s="structure_test", commune_m2m=commune1, instance_fk=dep.instance)
        user1.organisation_m2m.add(struc)
        user2.organisation_m2m.add(struc)
        # Création d'une activite
        sport = ActiviteFactory.create(name='sport_test')
        # Création des manifestations
        # Manif NM sur commmune 1
        cls.manifestation1 = manif1 = DcnmFactory.create(ville_depart=commune1, structure_organisatrice_fk=struc, activite=sport,
                                                         nom='Manifestation_Test_1')
        N2kEvalFactory.create(manif=manif1)
        RNREvalFactory.create(manif=manif1)
        N2kOperateurSiteFactory.create_batch(2, departement=dep, commune=commune1)
        N2kSiteFactory.create()
        pj = manif1.piece_jointe.get(champ_cerfa="reglement_manifestation")
        pj.document_attache = "url"
        pj.save()
        PieceJointeFactory.create(manif=manif1, champ_cerfa="demande_instructeur",
                                  document_attache="document", date_demande=timezone.now())
        # Manif NM sur commmune 2
        cls.manifestation2 = manif2 = DcnmFactory.create(ville_depart=commune2, structure_organisatrice_fk=struc, activite=sport,
                                                         nom='Manifestation_Test_2')
        # Manif NM sur 2 commmunes
        cls.manifestation3 = manif3 = DcnmFactory.create(ville_depart=commune1, structure_organisatrice_fk=struc, activite=sport,
                                                         nom='Manifestation_Test_3', villes_traversees=(commune2,))
        # Manif NM sur 2 commmunes
        cls.manifestation4 = manif4 = DcnmFactory.create(ville_depart=commune2, structure_organisatrice_fk=struc, activite=sport,
                                                         nom='Manifestation_Test_4', villes_traversees=(commune1,))
        # Manif VTM sur 1 commmune
        cls.manifestation5 = manif5 = AvtmFactory.create(ville_depart=commune2, structure_organisatrice_fk=struc, activite=sport,
                                                         nom='Manifestation_Test_7')

        # Création des instructions
        cls.instruction1 = InstructionFactory.create(manif=manif1, instance=dep.instance)
        cls.instruction2 = InstructionFactory.create(manif=manif2, instance=dep.instance)
        cls.instruction3 = InstructionFactory.create(manif=manif3, instance=dep.instance)
        cls.instruction4 = InstructionFactory.create(manif=manif4, instance=dep.instance)
        cls.instruction5 = InstructionFactory.create(manif=manif5, instance=dep.instance)

        # Créations complémentaires sur instruction 1
        historique_circuit_json = [{"entrant": prefecture.pk,
                                    "sortant": cls.ddsp.pk,
                                    "type": "creer",
                                    "date": str(timezone.now())}]
        cls.avis1 = AvisInstructionFactory.create(instruction=cls.instruction1, service_consulte_fk=cls.ddsp,
                                                  service_demandeur_fk=prefecture, historique_circuit_json=historique_circuit_json)
        cls.preavis1 = AvisInstructionFactory.create(instruction=cls.instruction1, avis_parent_fk=cls.avis1, service_consulte_origine_fk=commiss, service_consulte_fk=commiss)
        DocOfficielFactory.create(instruction=cls.instruction1, nature=2, utilisateur=instruc,
                                  fichier="arrete_circulation")

        # Créations de manifestations finies pour la route Evenement_environnement
        cls.manifestation10 = DcnmFactory.create(ville_depart=commune1, structure_organisatrice_fk=struc, activite=sport,
                                                 date_debut=timezone.now() - timezone.timedelta(days=1),
                                                 date_fin=timezone.now() - timezone.timedelta(days=1),
                                                 parcours_openrunner="55,66", nom='Manifestation_Test_10')
        ParcoursFactory.create(manif=cls.manifestation10)
        cls.evalN2k = N2kEvalFactory.create(manif=cls.manifestation10)
        cls.instruction10 = InstructionFactory.create(manif=cls.manifestation10)

        cls.apps = ApplicationFactory.create(client_type="confidential", authorization_grant_type="password")

    def test01_access_anonymous(self):
        """
        Verifie si l'accès est refusé sans login
        """
        print('\t >>> Accès anonyme')
        reponse = self.client.get('/api/')
        self.assertEqual(reponse.status_code, 401)
        self.assertContains(reponse, "authentification", status_code=401)

    def test02_access_login(self):
        """
        Verifie si l'accès est ok avec login
        """
        print('\t >>> Accès Api par login')
        self.assertTrue(self.client.login(username='api_def_user', password='123'))
        reponse = self.client.get('/api/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("Calendrier", reponse.json())
        self.assertIn("Evenement", reponse.json())
        self.assertIn("Instruction", reponse.json())
        self.assertIn("Avis", reponse.json())
        self.assertIn("Organisateur", reponse.json())

    def test03_access_token(self):
        """
        Verifie si l'accès est ok avec token
        """
        print('\t >>> Accès Api par token')
        data = {
            "grant_type": "password",
            "username": "instructeur",
            "password": "123",
            "client_id": self.apps.client_id,
            "client_secret": self.apps.client_secret,
        }
        reponse = self.client.post('/o/token/', data, content_type='application/json')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(5, len(reponse.json()))
        self.assertEqual("Bearer", reponse.json()['token_type'])
        token = reponse.json()['access_token']
        refresh = reponse.json()['refresh_token']
        header = {"Authorization": "Bearer " + token}
        reponse = self.client.get('/api/', **header)
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(8, len(reponse.json()))
        data = {
            "grant_type": "refresh_token",
            "refresh_token": refresh,
            "client_id": self.apps.client_id,
            "client_secret": self.apps.client_secret,
        }
        reponse = self.client.post('/o/token/', data, content_type='application/json')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(5, len(reponse.json()))
        self.assertNotEqual(token, reponse.json()['access_token'])

    def test04_Route_Calendrier(self):
        """
        Verifie la route calendrier
        réservée a tous les groupes API
        """
        print('\t >>> Route Calendrier')
        reponse = self.client.get('/api/Calendrier/')
        self.assertContains(reponse, "authentification", status_code=401)
        self.client.logout()
        self.assertTrue(self.client.login(username='instructeur', password='123'))
        reponse = self.client.get('/api/Calendrier/')
        self.assertEqual(reponse.status_code, 200)
        self.client.logout()
        self.assertTrue(self.client.login(username='api_env_user', password='123'))
        reponse = self.client.get('/api/Calendrier/')
        self.assertEqual(reponse.status_code, 200)
        self.client.logout()
        self.assertTrue(self.client.login(username='api_def_user', password='123'))
        reponse = self.client.get('/api/Calendrier/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(reponse.json()['count'], 5)
        self.assertIn("Manifestation_Test", reponse.json()['results'][0]['nom'])
        self.assertIn("Sport_test", reponse.json()['results'][0]['activite'])
        self.assertNotIn("nb_participants", reponse.json()['results'][0])

        reponse = self.client.get('/api/Calendrier/' + str(self.manifestation3.pk) + '/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("Manifestation_Test_3", reponse.json()['nom'])
        self.assertIn("Bully", reponse.json()['villes_traversees'][0])
        self.assertIn("structure_test", reponse.json()['structure_organisatrice_fk']['precision_nom_s'])

    def test05_Route_Evenement(self):
        """
        Verifie la route Evenement"
        réservée au groupe _API_services_consultes
        """
        print('\t >>> Route Evenement')
        self.assertTrue(self.client.login(username='api_def_user', password='123'))
        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()
        self.assertTrue(self.client.login(username='api_env_user', password='123'))
        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()

        self.assertTrue(self.client.login(username='instructeur2', password='123'))
        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/')
        self.assertContains(reponse, 'Vous n\'êtes pas concerné par ce dossier', status_code=403)
        self.client.logout()

        self.assertTrue(self.client.login(username='agent2', password='123'))
        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/')
        self.assertContains(reponse, 'Vous n\'êtes pas concerné par ce dossier', status_code=403)
        self.client.logout()

        self.assertTrue(self.client.login(username='instructeur', password='123'))
        reponse = self.client.get('/api/Evenement/')
        self.assertContains(reponse, 'fournir un numéro de dossier', status_code=403)
        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("Manifestation_Test_1", reponse.json()['nom'])
        self.assertIn("Sport_test", reponse.json()['activite'])
        self.assertIn("structure_test", reponse.json()['structure_organisatrice_fk']['precision_nom_s'])
        self.assertIn("Bard", reponse.json()['ville_depart_interdep_mtm'][0])

        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/pieceJointe/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(len(reponse.json()), 9)
        LISTE_FICHIERS = ['reglement_manifestation', 'disposition_securite', 'certificat_assurance',
                          'itineraire_horaire', 'liste_signaleurs', 'presence_docteur',
                          'presence_secouriste', 'presence_ambulance', "demande_instructeur"]
        for file in reponse.json():
            self.assertIn(file['champ_cerfa'], LISTE_FICHIERS)
            if file['champ_cerfa'] == "reglement_manifestation":
                self.assertIn("https://example.com/media/url", file['document_attache'])

        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/evaluationN2k/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("impact_estime", reponse.json())
        self.assertIn("conclusion_natura_2000", reponse.json())

        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/evaluationRnr/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("impact_estime", reponse.json())
        self.assertIn("conclusion_rnr", reponse.json())

        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/docOfficiel/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("Arrêté de circulation", reponse.json()[0]['nature'])
        self.assertEqual(self.instructeur.pk, reponse.json()[0]['utilisateur'])
        self.assertIn(str(timezone.now().year), reponse.json()[0]['date_depot'])
        self.assertIn("/media/arrete_circulation", reponse.json()[0]['fichier'])

    def test06_Route_Instruction(self):
        """
        Verifie la route Instruction
        réservée au groupe _API_services_consultes
        """
        print('\t >>> Route Instruction')
        self.assertTrue(self.client.login(username='api_def_user', password='123'))
        reponse = self.client.get('/api/Instruction/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()
        self.assertTrue(self.client.login(username='api_env_user', password='123'))
        reponse = self.client.get('/api/Instruction/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()

        self.assertTrue(self.client.login(username='instructeur', password='123'))
        reponse = self.client.get('/api/Instruction/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(reponse.json()['count'], 6)

        reponse = self.client.get('/api/Instruction/' + str(self.instruction1.pk) + '/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(self.manifestation1.pk, reponse.json()['manif_id'])
        self.assertIn("demandée", reponse.json()['etat'])
        self.assertEqual(self.avis1.pk, reponse.json()['avis'][0]['id'])
        self.assertIn("demandé", reponse.json()['avis'][0]['etat'])

    def test07_Route_Avis(self):
        """
        Verifie la route Avis
        réservée au groupe _API_services_consultes
        """
        print('\t >>> Route Avis')
        self.assertTrue(self.client.login(username='api_def_user', password='123'))
        reponse = self.client.get('/api/Avis/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()
        self.assertTrue(self.client.login(username='api_env_user', password='123'))
        reponse = self.client.get('/api/Avis/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()

        self.assertTrue(self.client.login(username='agent', password='123'))
        reponse = self.client.get('/api/Avis/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(reponse.json()['count'], 1)

        reponse = self.client.get('/api/Avis/' + str(self.avis1.pk) + '/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(self.avis1.pk, reponse.json()['id'])
        self.assertIn("demandé", reponse.json()['etat'])
        self.assertIn("DDSP DDSP (42)", reponse.json()['service'])

        data = {"favorable": True, "prescriptions": "ma prescription"}
        reponse = self.client.post('/api/Avis/' + str(self.avis1.pk) + '/rendre/', data)
        print(reponse.json())
        self.assertContains(reponse, 'Avis rendu', status_code=200)

        reponse = self.client.get('/api/Avis/' + str(self.avis1.pk) + '/')
        self.assertEqual(reponse.status_code, 200)
        # print(reponse)
        # print(reponse.json())
        self.assertIn("rendu", reponse.json()['etat'])
        self.assertEqual(self.agent.pk, reponse.json()['agent'])
        self.assertIn(str(timezone.now().year), reponse.json()['date_reponse'])
        self.assertTrue(reponse.json()['favorable'])
        self.assertIn("ma prescription", reponse.json()['prescriptions'])

        data = {"favorable": True, "prescriptions": "ma prescription"}
        reponse = self.client.post('/api/Avis/' + str(self.avis1.pk) + '/rendre/', data)
        self.assertContains(reponse, 'Avis déjà rendu', status_code=406)

    def test08_Route_Organisateur(self):
        """
        Verifie la route Organisateur
        réservée au group _API_default
        """
        print('\t >>> Route Organisateur')
        # L'utilisateur n'a pas accès à cette route
        self.assertTrue(self.client.login(username='api_env_user', password='123'))
        reponse = self.client.get('/api/Organisateur/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()
        self.assertTrue(self.client.login(username='instructeur', password='123'))
        reponse = self.client.get('/api/Organisateur/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()

        # L'utilisateur a accès à cette route
        self.assertTrue(self.client.login(username='api_def_user', password='123'))
        reponse = self.client.get('/api/Organisateur/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(1, reponse.json()['count'])
        self.assertIn("Bard", reponse.json()['results'][0]['commune_m2m'][0])
        self.assertIn("structure_test", reponse.json()['results'][0]['structure'])

    def test09_Route_SiteN2K(self):
        """
        Verifie la route SiteN2K
        réservée au group _API_environnement
        """
        print('\t >>> Route SiteN2K')
        # L'utilisateur n'a pas accès à cette route
        self.assertTrue(self.client.login(username='api_def_user', password='123'))
        reponse = self.client.get('/api/SiteN2K/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()
        self.assertTrue(self.client.login(username='instructeur', password='123'))
        reponse = self.client.get('/api/SiteN2K/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()

        # L'utilisateur a accès à cette route
        self.assertTrue(self.client.login(username='api_env_user', password='123'))
        reponse = self.client.get('/api/SiteN2K/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(3, reponse.json()['count'])
        self.assertEqual(1, reponse.json()['results'][0]['id'])
        reponse = self.client.get('/api/SiteN2K/1/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(5, len(reponse.json()))
        self.assertEqual("Natura2000_0", reponse.json()['nom'])
        self.assertEqual("0123456", reponse.json()['index'])
        reponse = self.client.get('/api/SiteN2K/1/OperateurSite/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(7, len(reponse.json()))
        self.assertEqual("OpérateurN2k_0", reponse.json()['nom'])
        self.assertEqual("open2k0@example.com", reponse.json()['email'])
        reponse = self.client.get('/api/SiteN2K/3/OperateurSite/')
        self.assertEqual(reponse.status_code, 404)
        self.assertEqual("Pas d'opérateur pour ce site.", reponse.json()['detail'])

    def test12_Route_Evenement_environnement(self):
        """
        Verifie la route Evenement_environnement
        réservée au group _API_environnement
        """
        print('\t >>> Route Evenement_environnement')
        # L'utilisateur n'a pas accès à cette route
        self.assertTrue(self.client.login(username='api_def_user', password='123'))
        reponse = self.client.get('/api/Evenement_environnement/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()
        self.assertTrue(self.client.login(username='instructeur', password='123'))
        reponse = self.client.get('/api/Evenement_environnement/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()

        # L'utilisateur a accès à cette route
        self.assertTrue(self.client.login(username='api_env_user', password='123'))
        reponse = self.client.get('/api/Evenement_environnement/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(1, reponse.json()['count'])
        self.assertEqual('Manifestation_Test_10', reponse.json()['results'][0]['nom'])
        id_manif = str(reponse.json()['results'][0]['id'])
        reponse = self.client.get('/api/Evenement_environnement/' + id_manif + '/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(22, len(reponse.json()))
        self.assertEqual('demandée', reponse.json()['etat_instruction'])
        self.assertEqual(self.evalN2k.pk, reponse.json()['n2kevaluation'])
        reponse = self.client.get('/api/Evenement_environnement/' + id_manif + '/evaluationN2k/')
        self.assertEqual(47, len(reponse.json()))
        reponse = self.client.get('/api/Evenement_environnement/' + id_manif + '/carto_GPX/')
        self.assertContains(reponse, 'parcours')
        self.assertEqual(2, len(reponse.json()['parcours']))
        self.assertEqual(7, len(reponse.json()['parcours'][0].split('/')))
        reponse = self.client.get('/api/Evenement_environnement/' + id_manif + '/carto_GeoJson/')
        self.assertContains(reponse, 'parcours')
        self.assertEqual(2, len(reponse.json()['parcours']))
        self.assertEqual(8, len(reponse.json()['parcours'][0]))
        self.assertEqual(4, len(reponse.json()['parcours'][0]['shape']))
        self.assertEqual(2, len(reponse.json()['parcours'][0]['shape']['line_waypoint']["geometry"]))
        # print(reponse.json())
