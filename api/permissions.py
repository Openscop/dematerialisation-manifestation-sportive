from django.contrib.auth.models import Group
from annoying.functions import get_object_or_None

from rest_framework import permissions

from evenements.models import Manif
from structure.models import Organisation, ServiceInstructeur, ServiceConsulte


class IsInGroupApi(permissions.BasePermission):
    message = 'Vous n\'avez pas accès à ces informations. Contactez Openscop au 04 77 36 91 35.'

    def has_permission(self, request, view):
        grouplist = Group.objects.filter(name__startswith="_API_").values_list('name', flat=True)
        usergrouplist = request.user.groups.values_list('name', flat=True)
        if any([mot_cle in usergrouplist for mot_cle in grouplist]):
            return True
        return False


class IsInGroupApiDefault(permissions.BasePermission):
    message = 'Vous n\'avez pas accès à ces informations. Contactez Openscop au 04 77 36 91 35.'

    def has_permission(self, request, view):
        grouplist = request.user.groups.values_list('name', flat=True)
        if '_API_default' in grouplist:
            return True
        return False


class IsAppliServicesConsultes(permissions.BasePermission):
    message = 'Vous n\'avez pas accès à ces informations. Contactez Openscop au 04 77 36 91 35.'

    def has_permission(self, request, view):
        grouplist = request.user.groups.values_list('name', flat=True)
        if '_API_services_consultes' in grouplist:
            return True
        return False


class IsAppliEnvironnement(permissions.BasePermission):
    message = 'Vous n\'avez pas accès à ces informations. Contactez Openscop au 04 77 36 91 35.'

    def has_permission(self, request, view):
        grouplist = request.user.groups.values_list('name', flat=True)
        if '_API_environnement' in grouplist:
            return True
        return False


class IsAgentAvis(permissions.BasePermission):
    message = 'Vous n\'avez pas accès à ces informations. Contactez Openscop au 04 77 36 91 35.'

    def has_permission(self, request, view):
        if request.user.is_agent_service_consulte():
            return True
        return False


class IsInstructeur(permissions.BasePermission):
    message = 'Vous n\'avez pas accès à ces informations. Contactez Openscop au 04 77 36 91 35.'

    def has_permission(self, request, view):
        if request.user.is_agent_service_instructeur():
            return True
        return False


class IsInvolvedInInstruction(permissions.BasePermission):
    message = 'Vous n\'êtes pas concerné par ce dossier'

    def has_permission(self, request, view):
        if view.action == 'list':
            # Laisser passer pour avoir le message correct de la vue
            return True
        manif = get_object_or_None(Manif, pk=view.kwargs.get('pk'))
        if not manif:
            # Laisser passer pour avoir le message correct avec get_object
            return True
        if not hasattr(manif, 'instruction'):
            return False
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        if type(organisation) == ServiceInstructeur:
            if manif in organisation.get_manifs_ecriture():
                return True
        if type(organisation) == ServiceConsulte:
            if manif in organisation.get_manifs_ecriture():
                return True
        return False
