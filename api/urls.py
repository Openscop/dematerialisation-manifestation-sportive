from django.urls import include, path, re_path

from api.views.carto import PoiView
from api.views.viewsets import OrganisateurViewSet, SiteN2KViewSet
from api.views.manif import ManifestationViewSet, ManifPublicViewSet, ManifEnvViewSet
from api.views.instruction import InstructionViewSet
from api.views.avis import AvisView
from rest_framework import routers, permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


app_name = 'api'
router = routers.DefaultRouter()
router.register(r'Calendrier', ManifPublicViewSet, basename='Calendrier')
router.register(r'Organisateur', OrganisateurViewSet)
router.register(r'Instruction', InstructionViewSet, basename='Instruction')
router.register(r'Evenement', ManifestationViewSet, basename='Evenement')
router.register(r'Avis', AvisView, basename='Avis')
router.register(r'Evenement_environnement', ManifEnvViewSet, basename='Evenement_environnement')
router.register(r'SiteN2K', SiteN2KViewSet, basename='SiteN2K')
router.register(r'Pois', PoiView, basename='Pois')

schema_view = get_schema_view(
    openapi.Info(
        title="API de manifestationSportive.fr",
        default_version='v1',
        description="Permet l'utilisation de la plateforme à partir d'une autre application\n"
                    "Les urls affichées sont celles que vous pouvez accèder",
        contact=openapi.Contact(name="Openscop", url="https://www.openscop.fr/", email="contact@openscop.fr"),
        license=openapi.License(name="Creative Commons BY-NC-ND 4.0", url="https://creativecommons.org/licenses/by-nc-nd/4.0/")
    ),
    public=False,
    patterns=[path('api/', include(router.urls))],
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('', include(router.urls)),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

]
