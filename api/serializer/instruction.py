from rest_framework import serializers

from instructions.models import Instruction, Avis


class AvisRendreSerializer(serializers.ModelSerializer):

    class Meta:
        ref_name = None
        model = Avis
        fields = (
            'favorable',
            'prescriptions',
        )


class AvisCourtSerializer(serializers.ModelSerializer):

    service = serializers.CharField(source='service_consulte_fk')

    class Meta:
        ref_name = None
        model = Avis
        fields = (
            'id',
            'service',
            'etat',
        )


class AvisDetailSerializer(serializers.ModelSerializer):

    service = serializers.CharField(source='service_consulte_fk')
    manif_id = serializers.IntegerField(source='instruction.manif.pk')

    class Meta:
        ref_name = 'Avis'
        model = Avis
        fields = (
            'id',
            'etat',
            'service',
            'agent',
            'date_demande',
            'date_reponse',
            'favorable',
            'prescriptions',
            'manif_id',
        )


class AvisSerializer(serializers.ModelSerializer):

    service = serializers.CharField(source='service_consulte_fk')
    manif_id = serializers.IntegerField(source='instruction.manif.pk')

    class Meta:
        ref_name = None
        model = Avis
        fields = (
            'id',
            'etat',
            'service',
            'date_demande',
            'date_reponse',
            'favorable',
            'manif_id',
        )


class InstructionDetailSerializer(serializers.ModelSerializer):

    manif_id = serializers.IntegerField(source='manif.id')
    referent = serializers.StringRelatedField()
    avis = AvisSerializer(many=True)

    class Meta:
        ref_name = 'Instruction'
        model = Instruction
        fields = (
            'id',
            'etat',
            'date_creation',
            'date_consult',
            'date_derniere_action',
            'referent',
            'doc_verif',
            'manif_id',
            'avis'
        )


class InstructionSerializer(serializers.ModelSerializer):

    manif_id = serializers.IntegerField(source='manif.id')
    avis = AvisCourtSerializer(many=True)

    class Meta:
        ref_name = None
        model = Instruction
        fields = (
            'id',
            'etat',
            'date_creation',
            'manif_id',
            'avis'
        )
