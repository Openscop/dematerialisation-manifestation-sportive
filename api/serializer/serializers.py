from rest_framework import serializers

from structure.models.organisation import StructureOrganisatrice
from evaluation_incidence.models import N2kSite, N2kOperateurSite
from administrative_division.models import Commune


class CommuneField(serializers.RelatedField):
    """
    Modification de la représentation de l'objet
    """
    queryset = Commune.objects.all()
    label = 'Code INSEE et nom de la commune'

    def to_representation(self, value):
        return '%s, %s' % (value.code, value.name)


class StructureOrganisatriceLightSerializer(serializers.ModelSerializer):

    structure = serializers.CharField(source='precision_nom_s')
    commune_m2m = CommuneField(many=True)
    # une autre façon : profiter d'une fonction de la classe
    # commune = serializers.CharField(source='commune.natural_key')

    class Meta:
        ref_name = None
        model = StructureOrganisatrice
        fields = ('id', 'structure', 'commune_m2m')


class StructureOrganisatriceSerializer(serializers.ModelSerializer):

    commune_m2m = CommuneField(many=True)

    class Meta:
        ref_name = 'StructureOrganisatrice'
        model = StructureOrganisatrice
        fields = (
            'id',
            'precision_nom_s',
            'adresse_s',
            'commune_m2m',
            'telephone_s',
            'site_web_s',
        )


class SiteN2KSerializer(serializers.ModelSerializer):

    departements = serializers.StringRelatedField(many=True)
    site_type = serializers.CharField(source='get_site_type_display')

    class Meta:
        ref_name = 'Site Natura2000'
        model = N2kSite
        fields = '__all__'


class N2kOperateurSiteSerilizer(serializers.ModelSerializer):

    departement = serializers.StringRelatedField()
    commune = CommuneField()

    class Meta:
        ref_name = ' Opérateur site Natura2000'
        model = N2kOperateurSite
        fields = ('id', 'nom', 'adresse', 'commune', 'departement', 'email', 'contacts')
