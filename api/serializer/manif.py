from django.conf import settings
from django.contrib.sites.models import Site

from rest_framework import serializers

from evenements.models import Manif, PieceJointe, Dnm, Dnmc, Dcnm, Dcnmc, Dvtm,  Avtm, Dvtmcir, PieceJointeHistorique
from evaluation_incidence.models import EvaluationRnr, EvaluationN2K, N2kSite
from instructions.models import DocumentOfficiel
from administrative_division.models import Commune
from api.serializer.serializers import StructureOrganisatriceSerializer
from carto.serializers import ParcoursApiSerializer
from messagerie.models import Enveloppe


class CommuneField(serializers.RelatedField):
    """
    Modification de la représentation de l'objet
    """
    queryset = Commune.objects.all()
    label = 'Code INSEE et nom de la commune'

    def to_representation(self, value):
        return '%s, %s' % (value.code, value.name)


class ParcoursField(serializers.URLField):
    def to_representation(self, value):
        ids = [route.id for route in value.all() if route]
        url = settings.MAIN_URL + "carto/export/xx/"
        return [url.replace('xx', str(idx)) for idx in ids]


class FileUrlField(serializers.URLField):
    def to_representation(self, value):
        return 'https://' + (Site.objects.get_current().domain + settings.MEDIA_URL + str(value)) if value else None


class RnrevaluationSerilizer(serializers.ModelSerializer):

    class Meta:
        ref_name = 'Evaluation Rnr'
        model = EvaluationRnr
        exclude = ('history_json',)


class N2kevaluationSerilizer(serializers.ModelSerializer):

    class Meta:
        ref_name = 'Evaluation Natura2000'
        model = EvaluationN2K
        exclude = ('history_json',)


class ManifPublicSerializer(serializers.ModelSerializer):

    # Utilisation de la classe StringRelatedField pour afficher
    # la représentation de l'objet défini par __str__
    # departure_city = serializers.StringRelatedField()
    #
    # Utilisation de la classe définie plus haut pour afficher
    # une représentation personnalisée de l'objet
    ville_depart = CommuneField()
    villes_traversees = CommuneField(many=True)
    activite = serializers.StringRelatedField()
    structure_organisatrice_fk = StructureOrganisatriceSerializer()

    class Meta:
        ref_name = None
        model = Manif
        fields = (
            'nom',
            'description',
            'activite',
            'date_debut',
            'date_fin',
            'ville_depart',
            'villes_traversees',
            'structure_organisatrice_fk',
        )
        # depth = 2 pour voir les attributs de l'objet valeur d'un champ


class N2kSiteIndexSerializer(serializers.ModelSerializer):
    class Meta:
        model = N2kSite
        fields = ('index',)


class ManifEnvSerializer(serializers.ModelSerializer):

    instance = serializers.StringRelatedField()
    ville_depart_interdep_mtm = CommuneField(many=True)
    villes_traversees = CommuneField(many=True)
    departements_traverses = serializers.StringRelatedField(many=True)
    activite = serializers.StringRelatedField()
    emprise = serializers.CharField(source='get_emprise_display')
    sites_natura2000 = N2kSiteIndexSerializer(many=True)
    zones_rnr = serializers.StringRelatedField(many=True)
    lieux_pdesi = serializers.StringRelatedField(many=True)
    n2kevaluation = serializers.IntegerField(source='n2kevaluation.id')
    etat_instruction = serializers.SerializerMethodField()

    class Meta:
        ref_name = None
        model = Manif
        fields = (
            'id',
            'nom',
            'instance',
            'etat_instruction',
            'activite',
            'date_debut',
            'date_fin',
            'ville_depart_interdep_mtm',
            'villes_traversees',
            'departements_traverses',
            'emprise',
            'sites_natura2000',
            'zones_rnr',
            'lieux_pdesi',
            'n2kevaluation',
            'dans_calendrier',
            'nb_organisateurs',
            'nb_participants',
            'nb_spectateurs',
            'competition',
            'lucratif',
            'signature_charte_dispense_site_n2k',
        )

    def get_etat_instruction(self, obj):
        return obj.get_instruction().etat if obj.get_instruction() else ""


class ManifestationSerializer(serializers.ModelSerializer):

    instance = serializers.StringRelatedField()
    ville_depart_interdep_mtm = CommuneField(many=True)
    villes_traversees = CommuneField(many=True)
    departements_traverses = serializers.StringRelatedField(many=True)
    structure_organisatrice_fk = StructureOrganisatriceSerializer()
    activite = serializers.StringRelatedField()
    emprise = serializers.CharField(source='get_emprise_display')
    sites_natura2000 = serializers.StringRelatedField(many=True)
    zones_rnr = serializers.StringRelatedField(many=True)
    lieux_pdesi = serializers.StringRelatedField(many=True)
    rnrevaluation = serializers.IntegerField(source='rnrevaluation.id')
    n2kevaluation = serializers.IntegerField(source='n2kevaluation.id')
    etat = serializers.SerializerMethodField()
    date_derniere_action = serializers.SerializerMethodField()

    class Meta:
        ref_name = 'Manifestation'
        model = Manif
        exclude = (
            'delai_cours',
            'ville_depart_interdep',
            'instance_instruites',
            'ville_depart',
        )
        read_only_fields = ('etat',)

    def get_etat(self, obj):
        return obj.get_etat()

    def get_date_derniere_action(self, obj):
        env = Enveloppe.objects.filter(manifestation=self.instance).filter(type='action').last()
        if env:
            return env.date
        else:
            return None


class ManifCartoGpxSerializer(serializers.ModelSerializer):

    parcours = ParcoursField()

    class Meta:
        ref_name = 'Manifestation'
        model = Manif
        fields = ('parcours',)


class ManifCartoJsonSerializer(serializers.ModelSerializer):

    parcours = ParcoursApiSerializer(many=True)

    class Meta:
        ref_name = 'Manifestation'
        model = Manif
        fields = ('parcours',)


class DnmSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Dnm'
        model = Dnm
        exclude = ManifestationSerializer.Meta.exclude


class DnmcSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Dnmc'
        model = Dnmc
        exclude = ManifestationSerializer.Meta.exclude


class DcnmSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Dcnm'
        model = Dcnm
        exclude = ManifestationSerializer.Meta.exclude


class DcnmcSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Dcnmc'
        model = Dcnmc
        exclude = ManifestationSerializer.Meta.exclude


class DvtmSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Dvtm'
        model = Dvtm
        exclude = ManifestationSerializer.Meta.exclude


class AvtmSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Avtm'
        model = Avtm
        exclude = ManifestationSerializer.Meta.exclude


class DvtmcirSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Dvtmcir'
        model = Dvtmcir
        exclude = ManifestationSerializer.Meta.exclude


class PieceJointeHistoriqueSerializer(serializers.ModelSerializer):
    """
    Sérialiseur des pièces jointes d'une manifestation
    """
    document_attache = FileUrlField()

    class Meta:
        ref_name = 'Pièce jointe historique'
        model = PieceJointeHistorique
        exclude = ('pj',)


class PieceJointeSerializer(serializers.ModelSerializer):
    """
    Sérialiseur des pièces jointes d'une manifestation
    """
    document_attache = FileUrlField()
    historique = PieceJointeHistoriqueSerializer(many=True)

    class Meta:
        ref_name = 'Pièce jointe'
        model = PieceJointe
        exclude = ('manif',)


class DocOfficielSerializer(serializers.ModelSerializer):
    """
    Sérialiseur des documents officiels d'une manifestation
    """
    fichier = FileUrlField()
    nature = serializers.CharField(source='get_nature_display')

    class Meta:
        ref_name = 'Document officiel'
        model = DocumentOfficiel
        fields = (
            'id',
            'date_depot',
            'nature',
            'utilisateur',
            'fichier'
        )
