# coding: utf-8
from __future__ import absolute_import

from django import template

from sports.models.sport import Activite
from administrative_division.models.departement import LISTE_REGION

register = template.Library()


@register.filter
def label_region(value):
    if isinstance(value, str):
        for region in LISTE_REGION:
            if region[0] == value:
                return region[1]
    return ''
