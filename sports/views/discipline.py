from django.views.generic.list import ListView

from sports.models.sport import Discipline


class DisciplinesActivitesView(ListView):

    model = Discipline
    template_name = "sports/disciplinesactivites.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
