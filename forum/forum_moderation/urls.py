"""
    Forum moderation URLs
    =====================

    This module defines URL patterns associated with the django-machina's ``forum_moderation``
    application.

"""

from django.urls import path

from forum.core.urls import URLPatternsFactory
from .views import *


class ForumModerationURLPatternsFactory(URLPatternsFactory):
    """ Allows to generate the URL patterns of the ``forum_moderation`` application. """

    app_namespace = 'forum_moderation'

    def get_urlpatterns(self):
        """ Returns the URL patterns managed by the considered factory / application. """
        return [
            path(
                'topic/<str:slug>-<int:pk>/lock/',
                TopicLockView.as_view(),
                name='topic_lock',
            ),
            path(
                'topic/<str:slug>-<int:pk>/unlock/',
                TopicUnlockView.as_view(),
                name='topic_unlock',
            ),
            path(
                'topic/<str:slug>-<int:pk>/delete/',
                TopicDeleteView.as_view(),
                name='topic_delete',
            ),
            path(
                'topic/<str:slug>-<int:pk>/move/',
                TopicMoveView.as_view(),
                name='topic_move',
            ),
            path(
                'topic/<str:slug>-<int:pk>/change/topic/',
                TopicUpdateToNormalTopicView.as_view(),
                name='topic_update_to_post',
            ),
            path(
                'topic/<str:slug>-<int:pk>/change/sticky/',
                TopicUpdateToStickyTopicView.as_view(),
                name='topic_update_to_sticky',
            ),
            path(
                'topic/<str:slug>-<int:pk>/change/announce/',
                TopicUpdateToAnnounceView.as_view(),
                name='topic_update_to_announce',
            ),
            path('queue/',
                 ModerationQueueListView.as_view(),
                 name='queue'),
            path(
                'queue/<int:pk>/',
                ModerationQueueDetailView.as_view(),
                name='queued_post',
            ),
            path(
                'queue/<int:pk>/approve/',
                PostApproveView.as_view(),
                name='approve_queued_post',
            ),
            path(
                'queue/<int:pk>/disapprove/',
                PostDisapproveView.as_view(),
                name='disapprove_queued_post',
            ),
            path(
                'topic/<str:slug>-<int:pk>/clore/',
                TopicCloreView.as_view(),
                name='topic_clore',
            ),
            path(
                'topic/<str:slug>-<int:pk>/rouvrir/',
                TopicOuvrirView.as_view(),
                name='topic_ouvrir',
            )
        ]


urlpatterns_factory = ForumModerationURLPatternsFactory()
