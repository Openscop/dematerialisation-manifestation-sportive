from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.utils.translation import gettext_lazy as _
from django.shortcuts import reverse
from django.views.generic.detail import BaseDetailView, SingleObjectTemplateResponseMixin, SingleObjectMixin
from django.views.generic import DeleteView, ListView, DetailView
from django.views.generic.edit import FormMixin, ProcessFormView

from messagerie.models import Message
from forum.forum_permission.viewmixins import PermissionRequiredMixin
from .forms import TopicMoveForm
from forum.forum_conversation.models import Post, Topic


class TopicLockView(PermissionRequiredMixin, SingleObjectTemplateResponseMixin, BaseDetailView):
    """Permet aux modérateur de blocker un sujet"""
    context_object_name = 'topic'
    model = Topic
    success_message = _('Ce sujet a été rendu confidentiel')
    template_name = 'forum_moderation/topic_lock.html'

    def lock(self, request, *args, **kwargs):
        """ Locks the considered topic and retirects the user to the success URL. """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.status = Topic.TOPIC_LOCKED
        self.object.save()
        liens = self.object.get_absolute_url()
        messages.success(self.request, self.success_message)
        Message.objects.creer_et_envoyer(type_msg='forum', expediteur=None, destinataires=[self.object.poster],
                                         titre='Votre sujet a été rendu confidentiel',
                                         contenu=f"Votre sujet a été rendu confidentiel par un administrateur. Vous pouvez toujours y accéder : <a href='{liens}"
                                                 f"'><strong>{self.object.subject}</strong></a>. Mais il ne sera pas accessible pour les autres utilisateurs."
                                         )

        return HttpResponseRedirect(success_url)

    def post(self, request, *args, **kwargs):
        """ Handles POST requests. """
        return self.lock(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """ Returns the context data to provide to the template. """
        context = super().get_context_data(**kwargs)
        topic = self.get_object()
        context['forum'] = topic.forum
        return context

    def get_success_url(self):
        """ Returns the success URL to redirect the user to. """
        return reverse(
            'forum_conversation:topic',
            kwargs={
                'forum_slug': self.object.forum.slug,
                'forum_pk': self.object.forum.pk,
                'slug': self.object.slug,
                'pk': self.object.pk,
            },
        )

    def get_controlled_object(self):
        """ Returns the controlled object. """
        return self.get_object().forum

    def perform_permissions_check(self, user, obj, perms):
        """ Performs the permission check. """
        return self.request.forum_permission_handler.can_lock_topics(obj, user)


class TopicUnlockView(PermissionRequiredMixin, SingleObjectTemplateResponseMixin, BaseDetailView):
    """ Permet aux modérateurs de débloquer un sujet """

    context_object_name = 'topic'
    model = Topic
    template_name = 'forum_moderation/topic_unlock.html'
    success_message = _('Ce sujet a été rendu public')

    def unlock(self, request, *args, **kwargs):
        """ Unlocks the considered topic and retirects the user to the success URL. """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.status = Topic.TOPIC_UNLOCKED
        self.object.save()
        messages.success(self.request, self.success_message)
        liens = self.object.get_absolute_url()

        Message.objects.creer_et_envoyer(type_msg='forum', expediteur=None, destinataires=[self.object.poster],
                                         titre='Votre sujet a été rendu public',
                                         contenu=f"Votre sujet <a href='{liens}'><strong>{self.object.subject}"
                                                 f"</strong></a> a été rendu public par un administrateur."
                                         )

        return HttpResponseRedirect(success_url)

    def post(self, request, *args, **kwargs):
        """ Handles POST requests. """
        return self.unlock(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """ Returns the context data to provide to the template. """
        context = super().get_context_data(**kwargs)
        topic = self.get_object()
        context['forum'] = topic.forum
        return context

    def get_success_url(self):
        """ Returns the success URL to redirect the user to. """
        return reverse(
            'forum_conversation:topic',
            kwargs={
                'forum_slug': self.object.forum.slug,
                'forum_pk': self.object.forum.pk,
                'slug': self.object.slug,
                'pk': self.object.pk,
            },
        )

    def get_controlled_object(self):
        """ Returns the controlled object. """
        return self.get_object().forum

    def perform_permissions_check(self, user, obj, perms):
        """ Performs the permissions check. """
        return self.request.forum_permission_handler.can_lock_topics(obj, user)


class TopicDeleteView(PermissionRequiredMixin, DeleteView):
    """ Provides the ability to delete forum topics. """

    context_object_name = 'topic'
    model = Topic
    success_message = _('This topic has been deleted successfully.')
    template_name = 'forum_moderation/topic_delete.html'

    def get_context_data(self, **kwargs):
        """ Returns the context data to provide to the template. """
        context = super().get_context_data(**kwargs)
        topic = self.get_object()
        context['forum'] = topic.forum
        return context

    def get_success_url(self):
        """ Returns the success URL to redirect the user to. """
        messages.success(self.request, self.success_message)
        return reverse(
            'forum:forum', kwargs={'slug': self.object.forum.slug, 'pk': self.object.forum.pk},
        )

    def get_controlled_object(self):
        """ Returns the controlled object. """
        return self.get_object().forum

    def perform_permissions_check(self, user, obj, perms):
        """ Performs the permissions check. """
        return self.request.forum_permission_handler.can_delete_topics(obj, user)


class TopicMoveView(PermissionRequiredMixin, SingleObjectTemplateResponseMixin, FormMixin, SingleObjectMixin, ProcessFormView):
    """ Permet aux administrateurs de déplacer un sujet dans un autre forum"""

    context_object_name = 'topic'
    form_class = TopicMoveForm
    model = Topic
    success_message = _('This topic has been moved successfully.')
    template_name = 'forum_moderation/topic_move.html'

    def get(self, request, *args, **kwargs):
        """ Handles GET requests. """
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """ Handles POST requests. """
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """ Returns the context data to provide to the template. """
        context = super().get_context_data(**kwargs)
        topic = self.get_object()
        context['forum'] = topic.forum
        return context

    def get_form_kwargs(self):
        """ Returns the keyword arguments used to initialize the associated form. """
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'topic': self.object,
            'user': self.request.user,
        })
        return kwargs

    def form_valid(self, form):
        """ Handles a valid form. """
        topic = self.object
        old_forum = topic.forum
        first_post = topic.first_post

        # Move the topic
        new_forum = form.cleaned_data['forum']
        topic.forum = new_forum

        # Eventually lock the topic
        if form.cleaned_data['lock_topic']:
            topic.status = Topic.TOPIC_LOCKED
        else:
            topic.status = Topic.TOPIC_MOVED

        topic.save()
        topic_id = topic.pk

        # Copy topic
        old_topic = topic
        old_topic.pk = None
        old_topic.forum = old_forum

        first_post.pk = None
        first_post.subject = f'{topic.subject} (Déplacé)'
        first_post.topic = old_topic
        url = reverse(
            'forum_conversation:topic',
            kwargs={
                'forum_slug': new_forum.slug,
                'forum_pk': new_forum.pk,
                'slug': topic.slug,
                'pk': topic_id,
            },
        )
        content = topic.first_post.content
        first_post.content = f'{content}<br><h1>Ce sujet a été déplacé sur le  <a href="{url}">{new_forum}</a>.</h1>'

        old_topic.deplace = True
        old_topic.save()
        first_post.save()

        Message.objects.creer_et_envoyer(type_msg='forum', expediteur=None, destinataires=[self.object.poster],
                                         titre='Votre sujet a été déplacé',
                                         contenu=f"Votre sujet \"{self.object.subject}\" a été déplacé par un administrateur sur le <a href='{url}'>{new_forum}</a>."
                                         )

        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        """ Returns the success URL to redirect the user to. """
        return reverse(
            'forum_conversation:topic',
            kwargs={
                'forum_slug': self.object.forum.slug,
                'forum_pk': self.object.forum.pk,
                'slug': self.object.slug,
                'pk': self.object.pk,
            },
        )

    def get_controlled_object(self):
        """ Returns the controlled object. """
        return self.get_object().forum

    def perform_permissions_check(self, user, obj, perms):
        """ Performs the permissions check. """
        return self.request.forum_permission_handler.can_move_topics(obj, user)


class TopicUpdateTypeBaseView(
    PermissionRequiredMixin, SingleObjectTemplateResponseMixin, BaseDetailView,
):
    """ Provides the ability to change the type of forum topics: normal, sticky topic or announce.
    """

    context_object_name = 'topic'
    model = Topic
    success_message = _('This topic type has been changed successfully.')
    template_name = 'forum_moderation/topic_update_type.html'

    # The following attributes should be defined in subclasses.
    question = ''
    target_type = None

    def update_type(self, request, *args, **kwargs):
        """ Updates the type of the considered topic and retirects the user to the success URL.
        """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.type = self.target_type
        self.object.save()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(success_url)

    def post(self, request, *args, **kwargs):
        """ Handles POST requests. """
        return self.update_type(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """ Returns the context data to provide to the template. """
        context = super().get_context_data(**kwargs)
        context['question'] = self.question
        topic = self.get_object()
        context['forum'] = topic.forum
        return context

    def get_success_url(self):
        """ Returns the success URL to redirect the user to. """
        return reverse(
            'forum_conversation:topic', kwargs={
                'forum_slug': self.object.forum.slug,
                'forum_pk': self.object.forum.pk,
                'slug': self.object.slug,
                'pk': self.object.pk,
            },
        )

    def get_controlled_object(self):
        """ Returns the controlled object. """
        return self.get_object().forum


class TopicUpdateToNormalTopicView(TopicUpdateTypeBaseView):
    """ Provides the ability to switch a topic to a normal topic. """

    question = _('Would you want to change this topic to a default topic?')
    target_type = Topic.TOPIC_POST

    def perform_permissions_check(self, user, obj, perms):
        """ Performs the permissions check. """
        return self.request.forum_permission_handler.can_update_topics_to_normal_topics(obj, user)


class TopicUpdateToStickyTopicView(TopicUpdateTypeBaseView):
    """ Provides the ability to switch a topic to a sticky topic. """

    question = _('Would you want to change this topic to a sticky topic?')
    target_type = Topic.TOPIC_STICKY

    def perform_permissions_check(self, user, obj, perms):
        """ Performs the permissions check. """
        return self.request.forum_permission_handler.can_update_topics_to_sticky_topics(obj, user)


class TopicUpdateToAnnounceView(TopicUpdateTypeBaseView):
    """ Provides the ability to switch a topic to an announce. """

    question = _('Would you want to change this topic to an announce?')
    target_type = Topic.TOPIC_ANNOUNCE

    def perform_permissions_check(self, user, obj, perms):
        """ Performs the permissions check. """
        return self.request.forum_permission_handler.can_update_topics_to_announces(obj, user)


class ModerationQueueListView(PermissionRequiredMixin, ListView):
    """ Displays the moderation queue. """

    context_object_name = 'posts'
    model = Post
    template_name = 'forum_moderation/moderation_queue/list.html'

    def get_queryset(self):
        """ Returns the list of items for this view. """
        forums = self.request.forum_permission_handler.get_moderation_queue_forums(
            self.request.user,
        )
        qs = super().get_queryset()
        qs = qs.filter(topic__forum__in=forums, approved=False)
        return qs.order_by('-created')

    def perform_permissions_check(self, user, obj, perms):
        """ Performs the permissions check. """
        return self.request.forum_permission_handler.can_access_moderation_queue(user)


class ModerationQueueDetailView(PermissionRequiredMixin, DetailView):
    """ Displays the details of an item in the moderation queue. """

    context_object_name = 'post'
    model = Post
    template_name = 'forum_moderation/moderation_queue/detail.html'

    def get_context_data(self, **kwargs):
        """ Returns the context data to provide to the template. """
        context = super().get_context_data(**kwargs)

        post = self.object
        topic = post.topic

        # Handles the case when a poll is associated to the topic.
        try:
            if hasattr(topic, 'poll') and topic.poll.options.exists():
                poll = topic.poll
                context['poll'] = poll
                context['poll_options'] = poll.options.all()
        except ObjectDoesNotExist:  # pragma: no cover
            pass

        if not post.is_topic_head:
            # Add the topic review
            previous_posts = (
                topic.posts
                .filter(approved=True, created__lte=post.created)
                .select_related('poster', 'updated_by')
                .prefetch_related('attachments', 'poster__forum_profile')
                .order_by('-created')
            )
            previous_posts = previous_posts[:10]
            context['previous_posts'] = previous_posts

        return context

    def get_controlled_object(self):
        """ Returns the controlled object. """
        return self.get_object().topic.forum

    def perform_permissions_check(self, user, obj, perms):
        """ Performs the permissions check. """
        return self.request.forum_permission_handler.can_approve_posts(obj, user)


class PostApproveView(PermissionRequiredMixin, SingleObjectTemplateResponseMixin, BaseDetailView):
    """ Provides the ability to approve queued forum posts. """

    context_object_name = 'post'
    model = Post
    success_message = _('This post has been approved successfully.')
    template_name = 'forum_moderation/moderation_queue/post_approve.html'

    def approve(self, request, *args, **kwargs):
        """ Approves the considered post and retirects the user to the success URL. """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.approved = True
        self.object.save()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(success_url)

    def post(self, request, *args, **kwargs):
        """ Handles POST requests. """
        return self.approve(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """ Returns the context data to provide to the template. """
        context = super().get_context_data(**kwargs)
        context['forum'] = self.get_object().topic.forum
        return context

    def get_success_url(self):
        """ Returns the success URL to redirect the user to. """
        return reverse('forum_moderation:queue')

    def get_controlled_object(self):
        """ Returns the controlled object. """
        return self.get_object().topic.forum

    def perform_permissions_check(self, user, obj, perms):
        """ Performs the permissions check. """
        return self.request.forum_permission_handler.can_approve_posts(obj, user)


class PostDisapproveView(
    PermissionRequiredMixin, SingleObjectTemplateResponseMixin, BaseDetailView,
):
    """ Provides the ability to disapprove queued forum posts. """

    context_object_name = 'post'
    model = Post
    success_message = _('This post has been disapproved successfully.')
    template_name = 'forum_moderation/moderation_queue/post_disapprove.html'

    def disapprove(self, request, *args, **kwargs):
        """ Disapproves the considered post and retirects the user to the success URL. """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(success_url)

    def post(self, request, *args, **kwargs):
        """ Handles POST requests. """
        return self.disapprove(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """ Returns the context data to provide to the template. """
        context = super().get_context_data(**kwargs)
        context['forum'] = self.get_object().topic.forum
        return context

    def get_success_url(self):
        """ Returns the success URL to redirect the user to. """
        return reverse('forum_moderation:queue')

    def get_controlled_object(self):
        """ Returns the controlled object. """
        return self.get_object().topic.forum

    def perform_permissions_check(self, user, obj, perms):
        """ Performs the permissions check. """
        return self.request.forum_permission_handler.can_approve_posts(obj, user)


class TopicCloreView(PermissionRequiredMixin, SingleObjectTemplateResponseMixin, BaseDetailView):
    """Permet de clore un sujet """

    context_object_name = 'topic'
    model = Topic
    success_message = 'Ce sujet à été clôturé avec succès.'
    template_name = 'forum_moderation/topic_clore.html'

    def clore(self, request, *args, **kwargs):
        """ Locks the considered topic and retirects the user to the success URL. """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.status = Topic.TOPIC_RESOLVED
        self.object.save()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(success_url)

    def post(self, request, *args, **kwargs):
        """ Handles POST requests. """
        return self.clore(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """ Returns the context data to provide to the template. """
        context = super().get_context_data(**kwargs)
        topic = self.get_object()
        context['forum'] = topic.forum
        return context

    def get_success_url(self):
        """ Returns the success URL to redirect the user to. """
        return reverse(
            'forum_conversation:topic',
            kwargs={
                'forum_slug': self.object.forum.slug,
                'forum_pk': self.object.forum.pk,
                'slug': self.object.slug,
                'pk': self.object.pk,
            },
        )

    def get_controlled_object(self):
        """ Returns the controlled object. """
        return self.get_object().forum

    def perform_permissions_check(self, user, obj, perms):
        """ Performs the permission check. """
        return self.request.forum_permission_handler.can_resolve_topics(self.get_object(), user)


class TopicOuvrirView(PermissionRequiredMixin, SingleObjectTemplateResponseMixin, BaseDetailView):
    """Permet de rouvrir un sujet """

    context_object_name = 'topic'
    model = Topic
    success_message = 'Ce sujet à été rouvert avec succès.'
    template_name = 'forum_moderation/topic_ouvrir.html'

    def clore(self, request, *args, **kwargs):
        """ Locks the considered topic and retirects the user to the success URL. """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.status = Topic.TOPIC_UNLOCKED
        self.object.save()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(success_url)

    def post(self, request, *args, **kwargs):
        """ Handles POST requests. """
        return self.clore(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """ Returns the context data to provide to the template. """
        context = super().get_context_data(**kwargs)
        topic = self.get_object()
        context['forum'] = topic.forum
        return context

    def get_success_url(self):
        """ Returns the success URL to redirect the user to. """
        return reverse(
            'forum_conversation:topic',
            kwargs={
                'forum_slug': self.object.forum.slug,
                'forum_pk': self.object.forum.pk,
                'slug': self.object.slug,
                'pk': self.object.pk,
            },
        )

    def get_controlled_object(self):
        """ Returns the controlled object. """
        return self.get_object().forum

    def perform_permissions_check(self, user, obj, perms):
        """ Performs the permission check. """
        return self.request.forum_permission_handler.can_resolve_topics(self.get_object(), user)