"""
    Forum permission models
    =======================
    This module defines models provided by the ``forum_permission`` application.
"""

from .abstract_models import AbstractForumPermission, AbstractGroupForumPermission, AbstractUserForumPermission


class ForumPermission(AbstractForumPermission):
    pass


class GroupForumPermission(AbstractGroupForumPermission):
    pass


class UserForumPermission(AbstractUserForumPermission):
    pass
