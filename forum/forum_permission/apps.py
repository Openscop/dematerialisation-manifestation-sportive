from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ForumPermissionAppConfig(AppConfig):
    label = 'forum_permission'
    name = 'forum.forum_permission'
    verbose_name = _('Machina: Forum permissions')

    def ready(self):
        """ Executes whatever is necessary when the application is ready. """
        from . import receivers  # noqa: F401
