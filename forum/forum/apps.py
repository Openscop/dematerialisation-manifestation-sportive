from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ForumAppConfig(AppConfig):
    label = 'forum'
    name = 'forum.forum'
    verbose_name = _('Machina: Forum')

    def ready(self):
        """ Executes whatever is necessary when the application is ready. """
        from . import receivers  # noqa: F401
