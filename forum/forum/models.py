from django.db import models

from administrative_division.models.departement import Departement
from .abstract_models  import AbstractForum  # noqa


class Forum(AbstractForum):

    departement = models.ForeignKey(Departement, on_delete=models.SET_NULL, null=True, verbose_name='Département')
