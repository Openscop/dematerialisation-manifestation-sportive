from django.urls import path

from forum.core.urls import URLPatternsFactory

from .views import RedirectForumAdminInstance, IndexView, ForumView, SuivieSujetsNonClosView


class ForumURLPatternsFactory(URLPatternsFactory):
    """ Allows to generate the URL patterns of the ``forum`` application. """

    app_namespace = 'forum'

    def get_urlpatterns(self):
        """ Returns the URL patterns managed by the considered factory / application. """
        return [
            path('', IndexView.as_view(), name='index'),
            path('suivi/', SuivieSujetsNonClosView.as_view(), name="suivi_sujet"),
            path(
                '<str:slug>-<int:pk>/',
                ForumView.as_view(),
                name='forum',
            ),
            path('forum-administrateur-instance', RedirectForumAdminInstance.as_view(), name="forum-redirect")
        ]


urlpatterns_factory = ForumURLPatternsFactory()