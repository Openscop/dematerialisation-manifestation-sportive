from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import View, ListView
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404

from core.models.instance import Instance
from forum.conf import settings as machina_settings
from forum.forum_conversation.models import Topic, ForumAbonnement, Theme
from structure.models.organisation import Organisation
from forum.forum_permission.viewmixins import PermissionRequiredMixin
from forum.forum_tracking.handler import TrackingHandler
from .models import Forum
from .visibility import ForumVisibilityContentTree
from .signals import forum_viewed


class IndexView(ListView):
    """ Displays the top-level forums. """

    context_object_name = 'forums'
    template_name = 'forum/index.html'

    def get_queryset(self):
        """ Returns the list of items for this view. """
        return ForumVisibilityContentTree.from_forums(
            self.request.forum_permission_handler.forum_list_filter(
                Forum.objects.all(), self.request.user,
            ),
        )

    def get_context_data(self, **kwargs):
        """ Returns the context data to provide to the template. """
        context = super(IndexView, self).get_context_data(**kwargs)
        visiblity_content_tree = context['forums']

        # Computes some global values.
        context['total_posts_count'] = sum(n.posts_count for n in visiblity_content_tree.top_nodes)
        context['total_topics_count'] = sum(
            n.topics_count for n in visiblity_content_tree.top_nodes
        )

        return context

class ForumView(PermissionRequiredMixin, ListView):
    """Affiche le forum et ses topic."""

    context_object_name = 'topics'
    paginate_by = machina_settings.FORUM_TOPICS_NUMBER_PER_PAGE
    permission_required = ['can_read_forum', ]
    template_name = 'forum/forum_detail.html'
    view_signal = forum_viewed

    def get(self, request, **kwargs):
        """ Handles GET requests. """
        forum = self.get_forum()
        if forum.is_link:
            response = HttpResponseRedirect(forum.link)
        else:
            response = super(ForumView, self).get(request, **kwargs)
        self.send_signal(request, response, forum)
        return response

    def get_forum(self):
        """ Returns the forum to consider. """
        if not hasattr(self, 'forum'):
            self.forum = get_object_or_404(Forum, pk=self.kwargs['pk'])
        return self.forum

    def get_queryset(self):
        """ Returns the list of items for this view. """
        self.forum = self.get_forum()
        qs = (
            self.forum.topics
            .exclude(type=Topic.TOPIC_ANNOUNCE)
            .exclude(approved=False)
            .select_related('poster', 'last_post', 'last_post__poster')
        )
        return qs

    def get_controlled_object(self):
        """ Returns the controlled object. """
        return self.get_forum()

    def get_context_data(self, **kwargs):
        """Retourne les données à fournir au template."""
        context = super(ForumView, self).get_context_data(**kwargs)

        # Insert the considered forum into the context
        context['forum'] = self.get_forum()

        # Get the list of forums that have the current forum as parent
        context['sub_forums'] = ForumVisibilityContentTree.from_forums(
            self.request.forum_permission_handler.forum_list_filter(
                context['forum'].get_descendants(), self.request.user,
            ),
        )

        # The announces will be displayed on each page of the forum
        context['announces'] = list(
            self.get_forum()
                .topics.select_related('poster', 'last_post', 'last_post__poster')
                .filter(type=Topic.TOPIC_ANNOUNCE)
        )

        # Determines the topics that have not been read by the current user
        context['unread_topics'] = TrackingHandler(self.request).get_unread_topics(
            list(context[self.context_object_name]) + context['announces'], self.request.user,
        )
        topics = Topic.objects.filter(forum=self.get_forum().pk, approved=True)

        activites = []
        departements = []
        services = []
        departements_user = []

        for topic in topics:
            if topic.dossier and not topic.dossier.activite in activites:
                activites.append(topic.dossier.activite)
            if topic.dossier and not topic.dossier.get_instance().departement in departements:
                departements.append(topic.dossier.get_instance().departement)
            if topic.poster.get_service() and not topic.poster.is_organisateur() and not topic.poster.get_service() in services:
                services.append(topic.poster.get_service())
            if topic.poster.get_instance() and not topic.poster.get_instance() in departements_user:
                departements_user.append(topic.poster.get_instance())

        context["services"] = services
        context['activites'] = sorted(activites,  key=lambda x: x.discipline.name)
        context['departements'] = sorted(departements, key=lambda x: x.name)
        context['departements_user'] = sorted(departements_user, key=lambda x: x.name)
        context['announces'] = list(self.get_forum()
                                    .topics.select_related('poster', 'last_post', 'last_post__poster')
                                    .filter(type=Topic.TOPIC_ANNOUNCE, approved=True))
        context['themes'] = Theme.objects.all().order_by('theme')

        forum_abonnement = ForumAbonnement.objects.filter(user=self.request.user, forum=self.get_forum()).first()
        context['forum_abonnement'] = forum_abonnement
        context['service'] = Organisation.objects.get(pk=self.request.session['service_pk'])

        return context

    def send_signal(self, request, response, forum):
        """ Sends the signal associated with the view. """
        self.view_signal.send(
            sender=self, forum=forum, user=request.user, request=request, response=response,
        )


@method_decorator(login_required, name='dispatch')
class RedirectForumAdminInstance(View):

    def get(self, request):
        if self.request.user.has_group('Administrateurs d\'instance') and not self.request.user.is_superuser:
            forum = Forum.objects.filter(departement=self.request.user.get_departement())
            if forum.all().count() > 1:
                return redirect('forum:index')
            else:
                return redirect('forum:forum', forum.first().slug, forum.first().pk)
        else:
            return redirect('forum:index')


class SuivieSujetsNonClosView(PermissionRequiredMixin, View):
    """
    Cette page affiche un tableau de suivie des sujets du forum non clos
    """

    def get(self, request):
        # L'appel a forum_permission_handler permet d'avoir le qs de forum dont l'utilisateur à accès
        readable_forums = self.request.forum_permission_handler.forum_list_filter(
                Forum.objects.filter(parent=None), self.request.user,)
        forums_dict = {}
        instance_pk = self.request.GET.get('instance_pk', None)
        if instance_pk:  # Si on a demandé une instance précise
           instance_selected = Instance.objects.get(pk=instance_pk)
        else:  # Sinon on prend celle de base de l'utilisateur
           instance_selected = request.user.default_instance

        reponse_filter = self.request.GET.get('reponse_filter', None)
        if not reponse_filter:  # Si pas de demande spécifique en termes de nombre de réponses, on les prend tous
           reponse_filter = 'tout'

        for forum in readable_forums:  # On boucle sur les forums et leurs sujets ouverts pour remplir un dictionnaire
           themes_dict = {}
           if instance_selected.pk != 2:
                forum_topics_non_clos = Topic.objects.filter(forum=forum, poster__default_instance=instance_selected, approved=True).exclude(status=3)
           else:
                forum_topics_non_clos = Topic.objects.filter(forum=forum, approved=True).exclude(status=3)
           if not reponse_filter == 'tout':
               if reponse_filter == 'sans_reponse':
                   forum_topics_non_clos = forum_topics_non_clos.filter(posts_count=1)
               elif reponse_filter == 'demande_assistance':
                   topics_pk = []
                   for topic in forum_topics_non_clos:
                       for post in topic.posts.all():
                           if post.demande_assistance:
                               topics_pk.append(topic.pk)
                   forum_topics_non_clos = forum_topics_non_clos.filter(pk__in=topics_pk)
               else:
                   forum_topics_non_clos = forum_topics_non_clos.filter(posts_count__gt=1)
           total_sujet_non_clos = forum_topics_non_clos.count()
           if not self.request.GET.get('request') == 'tdb' or total_sujet_non_clos > 0:  # S'il y a au moins un sujet non clos dans le forum, on boucle sur les thèmes
               for theme in Theme.objects.all():
                   if forum_topics_non_clos.filter(theme=theme).count() > 0:  # S'il y a au moins un sujet de ce théme on l'ajoute au dictionnaire de théme
                       themes_dict.update({theme: forum_topics_non_clos.filter(theme=theme).count()})
                   forum_dict = {forum: {'themes': themes_dict, 'total_post': total_sujet_non_clos}}
                   forums_dict.update(forum_dict)

        # Si on est du groupe support, on boucle également sur les forums départementaux
        forums_departemental_dict = {}
        forum_departemental_total = 0
        if self.request.session['o_support']:
            for forum in self.request.forum_permission_handler.forum_list_filter(Forum.objects.filter(parent__isnull=False), self.request.user):
                forum_topics_non_clos = Topic.objects.filter(forum=forum,).exclude(status=3).exclude(type=1)
                if not reponse_filter == 'tout':
                    if reponse_filter == 'sans_reponse':
                        forum_topics_non_clos = forum_topics_non_clos.filter(posts_count=1)
                    elif reponse_filter == 'demande_assistance':
                        topics_pk = []
                        for topic in forum_topics_non_clos:
                            for post in topic.posts.all():
                                if post.demande_assistance:
                                    topics_pk.append(topic.pk)
                        forum_topics_non_clos = forum_topics_non_clos.filter(pk__in=topics_pk)
                    else:
                        forum_topics_non_clos = forum_topics_non_clos.filter(posts_count__gt=1)
                if forum_topics_non_clos.count() > 0:
                    forum_departemental_total += forum_topics_non_clos.count()
                    forums_departemental_dict.update({forum: forum_topics_non_clos.count()})

        context = {
           'forums_dict': forums_dict,
           'forums_departemental_dict': forums_departemental_dict,
           'forum_departemental_total': forum_departemental_total,
           'instances': Instance.objects.order_by('departement'),
           'instance_selected_pk': instance_selected.pk,
        }
        # S'il s'agit d'une requête ajax
        if self.request.GET.get('request'):
            # Pour les icônes du tableau de bord
            if self.request.GET.get('request') == 'tdb':
                return render(request, 'instructions/blocs/notification_topic_open_forum.html', context)
            # Pour rafraichir le tableau
            elif len(forums_dict) > 0:
                return render(request, 'forum/tableau_topic_non_clos.html', context)
            # Si aucun sujet non clos, retourne une alerte
            else:
                return HttpResponse('<div class="alert alert-info text-center mt-2 mx-2">Aucun sujet non clos avec les filtres sélectionnés.</div>')
        # Sinon on affiche la page
        return render(request, 'forum/page_suivi_non_clos.html', context)
