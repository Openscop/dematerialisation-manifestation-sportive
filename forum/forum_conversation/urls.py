from django.urls import include, path

from .view import PostDeleteView, PostCreateView, TopicCreateView, SignalCreateView, ForumAbonnementCreateView, \
                  ForumAbonnementUpdateView, ForumAbonnementSujetAjax, TopicView, TopicUpdateView, ListStickyTopic, \
                  ListTopicSuiviDeploiement, PostUpdateView, RechercheTopicView
from forum.core.urls import URLPatternsFactory
from forum.forum_conversation.forum_attachments.urls import urlpatterns_factory as attachments_urlpatterns_factory
from forum.forum_conversation.forum_polls.urls import urlpatterns_factory as polls_urlpatterns_factory


class BaseForumConversationURLPatternsFactory(URLPatternsFactory):
    app_namespace = 'forum_conversation'

    def get_urlpatterns(self):
        """ Returns the URL patterns managed by the considered factory / application. """

        conversation_urlpatterns = [
            path('topic/<str:slug>-<int:pk>/', TopicView.as_view(), name='topic',),
            path('topic/create/', TopicCreateView.as_view(), name='topic_create'),
            path('topic/<str:slug>-<int:pk>/update/', TopicUpdateView.as_view(), name='topic_update'),
            path('topic/<str:topic_slug>-<int:topic_pk>/post/create/', PostCreateView.as_view(), name='post_create'),
            path('topic/<str:topic_slug>-<int:topic_pk>/<int:pk>/post/update/', PostUpdateView.as_view(), name='post_update'),
            path('topic/<str:topic_slug>-<int:topic_pk>/<int:pk>/post/delete/', PostDeleteView.as_view(), name='post_delete'),
            path('topic/<str:topic_slug>-<int:topic_pk>/<int:pk>/post/signal/', SignalCreateView.as_view(), name='signal_create'),
            path('abonnement-forum/', ForumAbonnementCreateView.as_view(), name='forum_abonnement'),
            path('gerer-abonnement-forum/<pk>/<multiple>/', ForumAbonnementUpdateView.as_view(), name='gerer_forum_abonnement'),
            path('abonnement-sujet/', ForumAbonnementSujetAjax.as_view(), name='abonnement_sujet'),
            path('recherche-sujet/', RechercheTopicView.as_view(), name='recherche_sujet'),

        ]

        urlpatterns = [
            path('<str:forum_slug>-<int:forum_pk>/', include(conversation_urlpatterns),),
            path('forum/<str:forum_slug>-<int:forum_pk>/', include(conversation_urlpatterns), ),
            path('liste-sticky-topic', ListStickyTopic.as_view(), name='sujets-epingles'),
            path('liste-topic-suivi-deploiement', ListTopicSuiviDeploiement.as_view(), name='sujets-suivi-deploiement'),
        ]

        urlpatterns += super().get_urlpatterns()

        return urlpatterns


class ForumAttachmentsURLPatternsFactory(URLPatternsFactory):
    """ Allows to generate the URL patterns of the ``forum_attachments`` application. """

    attachments_urlpatterns_factory = attachments_urlpatterns_factory

    def get_urlpatterns(self):
        """ Returns the URL patterns managed by the considered factory / application. """
        return super().get_urlpatterns() + [
            path('', include(self.attachments_urlpatterns_factory.urlpatterns)),
        ]


class ForumPollsURLPatternsFactory(URLPatternsFactory):
    """ Allows to generate the URL patterns of the ``forum_attachments`` application. """

    polls_urlpatterns_factory = polls_urlpatterns_factory

    def get_urlpatterns(self):
        """ Returns the URL patterns managed by the considered factory / application. """
        return super().get_urlpatterns() + [
            path('', include(self.polls_urlpatterns_factory.urlpatterns)),
        ]


class ForumConversationURLPatternsFactory(
    BaseForumConversationURLPatternsFactory, ForumAttachmentsURLPatternsFactory,
    ForumPollsURLPatternsFactory,
):
    """ Composite class combining conversation views with polls & attachments views. """


urlpatterns_factory = ForumConversationURLPatternsFactory()

