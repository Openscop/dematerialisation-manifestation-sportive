"""
    Forum conversation forms
    ========================

    Ce module définie les formulaires fournie par l'application ``forum_conversation``.

"""
import html

from lxml.html.clean import Cleaner
from ckeditor_uploader.fields import RichTextUploadingFormField
from crispy_forms.layout import Layout, Fieldset, HTML
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import StrictButton

from django.utils import timezone
from django.contrib.auth.models import Group
from django import forms
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import F
from django.utils.translation import gettext_lazy as _
from django.forms import HiddenInput
from django.urls import reverse

from forum.conf import settings as machina_settings
from forum.forum_permission.models import UserForumPermission, GroupForumPermission
from forum.forum_permission.handler import PermissionHandler
from forum.forum_permission.shortcuts import get_anonymous_user_forum_key
from evenements.models import Manif
from .models import Signal, ForumAbonnement, Post, Topic, Theme
from .forum_polls.models import TopicPoll
from forum.forum.models import Forum


def get_dossier_choices(dossiers, organisation):
    array = [['', '-------------']]
    if organisation.is_structure_organisatrice():
        for dossier in dossiers:
            array.append([dossier.pk, dossier.nom + "  |  " + dossier.date_debut.strftime("%d-%m-%Y")])
    else:
        for dossier in dossiers:
            array.append([dossier.pk, dossier.nom + "  |  " + dossier.date_debut.strftime("%d-%m-%Y") + "  |  "
                          + str(dossier.structure_organisatrice_fk)])
    return array


class PostForm(forms.ModelForm):
    """Permet aux uttilisateurs d'ajouter des posts"""

    content = RichTextUploadingFormField('minimal', required=True, label='')

    class Meta:
        model = Post
        fields = ['subject', 'theme', 'dossier', 'content', 'update_reason', "demande_assistance"]

    def __init__(self, organisation=None, demande_assistance=None, *args, **kwargs):

        self.organisation = organisation
        self.user = kwargs.pop('user', None)
        self.forum = kwargs.pop('forum', None)
        self.topic = kwargs.pop('topic', None)
        self.perm_handler = PermissionHandler()
        self.demande_assistance = demande_assistance
        super().__init__(*args, **kwargs)
        self.dossiers = ""
        if self.organisation:
            if not self.instance.pk and self.topic and not self.topic.dossier and (self.organisation.is_service_instructeur() or self.organisation.is_support()):
                # Permet a un instructeur de rajouter un dossier a un sujet s'il n'y en avais pas de renseigner
                dossier_qs = Manif.objects.none()
                for service in self.topic.poster.organisation_m2m.all():
                    if service.is_structure_organisatrice():
                        dossier_qs = dossier_qs | Manif.objects.filter(structure_organisatrice_fk__in=self.topic.poster.organisation_m2m.all()).distinct()
                    elif service.is_service_instructeur():
                        dossier_qs = dossier_qs | Manif.objects.filter(instruction__service_instructeur_m2m=service).distinct()
                        dossier_qs = dossier_qs | Manif.objects.filter(
                            instruction__avis__in=(service.avis_instruis.all() | service.avis_lecture.all())).distinct()
                    elif service.is_service_consulte():
                        dossier_qs = dossier_qs | Manif.objects.filter(
                            instruction__avis__in=(service.avis_instruis.all() | service.avis_lecture.all())).distinct()
                self.dossiers = dossier_qs.filter(date_fin__gte=timezone.now()).order_by('nom')
            elif self.organisation.is_structure_organisatrice():
                self.dossiers = Manif.objects.filter(structure_organisatrice_fk=self.organisation).order_by('nom').distinct()
            elif self.organisation.is_service_instructeur():
                self.dossiers = Manif.objects.filter(instance=self.user.get_instance()).order_by('nom').distinct()
            self.fields['dossier'].choices = get_dossier_choices(self.dossiers, self.organisation)

        if self.user.is_superuser or self.user.has_group('_support'):
            self.fields['content'] = RichTextUploadingFormField('default', required=True, label='')

        # Ajout les input pour remplir les fields
        self.fields['subject'].widget.attrs['placeholder'] = _('Enter your subject')
        self.fields['dossier'].label = _('Dossier concerné')
        self.fields['theme'].widget.attrs = {'data-placeholder': "Sélectionnez des thèmes"}

        # Ajout d'un input text dans la séléction des dossier
        self.fields['dossier'].widget.attrs['data-live-search'] = _('True')
        self.fields['dossier'].widget.attrs['class'] = _('selectpicker')

        # Retire le required pour le champ 'dossier'
        self.fields['dossier'].required = False
        self.fields['theme'].required = False
        self.fields['update_reason'].required = True

        self.fields['demande_assistance'].widget = HiddenInput()

        # Prépare le champs sujet si le post est une reponse
        if not self.instance.pk and self.topic:
            self.fields['subject'].initial = '{} {}'.format(
                machina_settings.TOPIC_ANSWER_SUBJECT_PREFIX,
                self.topic.subject)
        if demande_assistance:
            self.fields['subject'].initial = 'Demande d\'assistance'
            self.fields['demande_assistance'].initial = True
            self.fields['subject'].widget = HiddenInput()
            self.fields['subject'].label = "Cette demande sera ajoutée au fil de la discussion."
            self.fields['content'].initial = 'Veuillez :<ul>' \
                                             '<li>Décrire tout ce qui a déjà été entrepris pour répondre au message initial</li>' \
                                             '<li>Préciser toutes les informations qui permettent de connaitre le contexte</li>' \
                                             '<li>Illustrer la demande avec une capture d\'écran</li>' \
                                             '</ul>'

        # Supprime le champs 'update_reason' si le post est original
        if not self.instance.pk:
            del self.fields['update_reason']

        # Ajoute un champ pour permettre a l'uttilisateur de rendre le sujet confidentiel s'il en a les droits
        if (
                (self.instance.pk or self.topic) and
                self.perm_handler.can_lock_topics(self.forum, self.user)
        ):
            self.fields['lock_topic'] = forms.BooleanField(
                label=_('Lock topic'), required=False,
                initial=self.topic.status == Topic.TOPIC_LOCKED,
            )

    def clean(self):
        """ Valide le Formulaire """
        if not self.instance.pk:
            # Only set user on post creation
            if not self.user.is_anonymous:
                self.instance.poster = self.user
            else:
                self.instance.anonymous_key = get_anonymous_user_forum_key(self.user)
        if not self.instance.poster.is_superuser:
            cleaner = Cleaner()
            cleaner.javascript = True
            style_autorise = frozenset(['style'])
            cleaner.safe_attrs = cleaner.safe_attrs.union(style_autorise)
            if self.cleaned_data.get("content"):
                self.instance.contenu = cleaner.clean_html(html.unescape(self.cleaned_data.get("content")))
        return super().clean()

    def save(self, commit=True):
        """ Sauvegarde l'instance """
        if self.instance.pk:
            # First handle updates
            post = super().save(commit=False)
            post.topic.dossier = self.cleaned_data['dossier']
            post.updated_by = self.user
            post.updates_count = F('updates_count') + 1
            post.topic.save()
        else:
            post = Post(
                topic=self.topic,
                subject=self.cleaned_data['subject'],
                dossier=self.cleaned_data['dossier'],
                approved=self.perm_handler.can_post_without_approval(self.forum, self.user),
                content=self.cleaned_data['content'],
                demande_assistance=self.cleaned_data['demande_assistance'])
            if not post.topic.dossier and self.cleaned_data['dossier']:
                post.topic.dossier = self.cleaned_data['dossier']
                post.topic.save()
            if not self.user.is_anonymous:
                post.poster = self.user

        # Rend le topic confidentiel si nécessaire
        lock_topic = self.cleaned_data.get('lock_topic', False)
        if lock_topic:
            self.topic.status = Topic.TOPIC_LOCKED
            self.topic.save()

        if commit:
            post.save()

        return post


class TopicForm(PostForm):
    """ Permet de creer des Topics """

    topic_type = forms.ChoiceField(
        label=_('Post topic as'), choices=Topic.TYPE_CHOICES, required=False)
    theme = forms.ModelMultipleChoiceField(label=_('Thème du sujet'), queryset=Theme.objects.all())

    def __init__(self, organisation=None, *args, **kwargs):
        self.organisation = organisation
        super().__init__(organisation, *args, **kwargs)
        # Realise quelque verifications
        self.can_add_stickies = self.perm_handler.can_add_stickies(self.forum, self.user)
        self.can_add_announcements = self.perm_handler.can_add_announcements(self.forum, self.user)
        self.can_create_polls = self.perm_handler.can_create_polls(self.forum, self.user)
        self.fields['theme'].required = True
        self.dossiers = ""
        if self.organisation.is_structure_organisatrice():
            self.dossiers = Manif.objects.filter(structure_organisatrice_fk=self.organisation).distinct()

        if self.organisation.is_service_instructeur():
            self.dossiers = Manif.objects.filter(instance=self.user.get_instance()).order_by('nom').distinct()

        self.fields['dossier'].choices = get_dossier_choices(self.dossiers, self.organisation)


        if not self.can_add_stickies:
            choices = filter(
                lambda t: t[0] != Topic.TOPIC_STICKY, self.fields['topic_type'].choices,
            )
            self.fields['topic_type'].choices = choices
        if not self.can_add_announcements:
            choices = filter(
                lambda t: t[0] != Topic.TOPIC_ANNOUNCE, self.fields['topic_type'].choices,
            )
            self.fields['topic_type'].choices = choices

        # Rajoute les champs de sondage si l'utilisateur peut en créer
        if self.can_create_polls:
            self.fields['poll_question'] = forms.CharField(
                label=_('Poll question'), required=False,
                help_text=_(
                    'Enter a question to associate a poll with the topic or leave blank to not '
                    'create a poll.'
                ),
                max_length=TopicPoll._meta.get_field('question').max_length,
            )
            self.fields['poll_max_options'] = forms.IntegerField(
                label=_('Maximum number of poll options per user'), required=False,
                help_text=_('This is the number of options each user may select when voting.'),
                validators=TopicPoll._meta.get_field('max_options').validators,
                initial=TopicPoll._meta.get_field('max_options').default,
            )
            self.fields['poll_duration'] = forms.IntegerField(
                label=_('For how many days the poll should be run?'), required=False,
                help_text=_('Enter 0 or leave blank for a never ending poll.'),
                min_value=0, initial=0,
            )
            self.fields['poll_user_changes'] = forms.BooleanField(
                label=_('Allow re-voting?'), required=False,
                help_text=_('If enabled users are able to change their vote.'),
                initial=False,
            )

        # Rentre les valeurs initiales
        try:
            if hasattr(self.instance, 'topic'):
                self.fields['topic_type'].initial = self.instance.topic.type
                self.fields['theme'].initial = self.instance.topic.theme.all()

                if self.can_create_polls and self.instance.topic.poll is not None:
                    self.fields['poll_question'].initial = self.instance.topic.poll.question
                    self.fields['poll_max_options'].initial = self.instance.topic.poll.max_options
                    self.fields['poll_duration'].initial = self.instance.topic.poll.duration
                    self.fields['poll_user_changes'].initial = self.instance.topic.poll.user_changes
        except ObjectDoesNotExist:
            pass

    def clean(self):
        """ Valide le formulaire. """
        if self.cleaned_data.get('poll_question', None) \
                and not self.cleaned_data.get('poll_max_options', None):
            self.add_error(
                'poll_max_options',
                _('You must set the maximum number of poll options per user when creating polls'),
            )

        super().clean()

    def save(self, commit=True):
        """ Sauvegarde l'instance. """
        if not self.instance.pk:
            if 'topic_type' in self.cleaned_data and len(self.cleaned_data['topic_type']):
                topic_type = self.cleaned_data['topic_type']
            else:
                topic_type = Topic.TOPIC_POST

            topic = Topic(
                forum=self.forum,
                subject=self.cleaned_data['subject'],
                dossier=self.cleaned_data['dossier'],
                type=topic_type,
                status=Topic.TOPIC_UNLOCKED,
                approved=self.perm_handler.can_post_without_approval(self.forum, self.user),
            )

            if not self.user.is_anonymous:
                topic.poster = self.user
            self.topic = topic
            if commit:
                topic.save()
                topic.theme.set(self.cleaned_data['theme'])
            else:
                topic.theme.set(self.cleaned_data['theme'])
            topic.save()
        else:
            if 'topic_type' in self.cleaned_data and len(self.cleaned_data['topic_type']):
                if self.instance.topic.type != self.cleaned_data['topic_type']:
                    self.instance.topic.type = self.cleaned_data['topic_type']
                    self.instance.topic.theme.set(self.cleaned_data['theme'])
                    self.instance.topic._simple_save()

        return super().save(commit)

    def get_who_can_see(self):
        if self.forum.name == 'Forum instructeurs':
            return "les instructeurs et les administrateurs d'instances."
        if self.forum.name == 'Forum services consultés':
            return "les instructeurs et les agents des services consultés."
        if UserForumPermission.objects.filter(forum=self.forum, anonymous_user=True, permission__codename="can_read_forum", has_perm=True).first():
            return "tout le monde."
        elif UserForumPermission.objects.filter(forum=self.forum, authenticated_user=True, permission__codename="can_read_forum", has_perm=True).first():
            return "tous les utilisateurs authentifiés sur la plateforme."
        else:
            groups_var = ""

            for group in Group.objects.all():
                if GroupForumPermission.objects.filter(forum=self.forum, group=group, permission__codename="can_read_forum").first():
                    groups_var += f"les {str.lower(group.name)}, "
            return groups_var[:-2] + "."


class SignalForm(forms.ModelForm):

    class Meta:
        model = Signal
        fields = ['raison', 'commentaire']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields['raison'].label = _('Pour quel raison voulez-vous signaler ce commentaire ?')

        self.fields['commentaire'] = forms.CharField()

        self.helper.layout = Layout(
            Fieldset(
                '', 'raison', 'commentaire'
            ),
            StrictButton(
                '<div class="form-actions"> </div>%s' % "Valider",
                type='submit',
                css_class='btn btn-primary btn-form my-3 px-3 btn float-end',
            )
        )


class ForumAbonnementForm(forms.ModelForm):

    forum_pk = forms.CharField()

    class Meta:
        model = ForumAbonnement
        fields = '__all__'
        exclude = ('user', 'forum')

    def __init__(self, forum_slug=None, pk=None, forum_pk=None, multiple=None, *args, **kwargs):

        super(ForumAbonnementForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.tout:
            collapse = ""
        else:
            collapse = "show"
        self.forum = Forum.objects.get(pk=forum_pk)

        if multiple:
            titre = f'{self.forum.name} :'
        else:
            titre = f'Sélectionner les filtres auxquels vous souhaiterez vous abonner sur le {self.forum}:'

        self.helper = FormHelper()
        if pk:
            self.helper.form_action = reverse('forum_conversation:gerer_forum_abonnement', kwargs={'forum_slug': self.forum.slug,
                                                                                                   'forum_pk': self.forum.pk,
                                                                                                   'pk': pk,
                                                                                                   'multiple': multiple
                                                                                                   })
        self.helper.form_class = 'form-horizontal'
        self.fields['tout'].label = "S'abonner à tout les sujets de ce forum"
        self.fields['tout'].widget.attrs = {'data-id': self.forum.pk}
        self.fields['departements'].label = "Département de l'auteur du sujet"
        self.fields['departements'].widget.attrs = {'data-placeholder': "Sélectionnez des départements"}
        self.fields['activites'].widget.attrs = {'data-placeholder': "Sélectionnez des activités"}
        self.fields['disciplines'].widget.attrs = {'data-placeholder': "Sélectionnez des disciplines"}
        self.fields['topics'].widget.attrs = {'data-placeholder': "Sélectionnez des topics"}
        self.fields['themes'].widget.attrs = {'data-placeholder': "Sélectionnez des thèmes"}
        self.fields['topics'].queryset = Topic.objects.filter(forum=self.forum)
        self.fields['forum_pk'].widget = HiddenInput()
        self.initial['forum_pk'] = self.forum.pk

        self.helper.layout = Layout(
            Fieldset(
                titre,
                'tout',
                HTML(f'<div class="collapse {collapse}" id="collapse_abonnements_{self.forum.pk}" aria-expanded="true">'),
                'disciplines', 'activites', 'departements', 'themes', 'topics', 'forum_pk'

            ),
            HTML(f'<div class="d-flex justify-content-end">', ),
            StrictButton(
                "Enregistrer mes abonnements",
                type='submit',
                css_class='btn btn-primary btn-form my-3 px-3 btn',
            ),
            HTML(f'</div>'),
        )

    def clean(self):
        if self.cleaned_data['tout']:
            self.cleaned_data['activites'] = ""
            self.cleaned_data['disciplines'] = ""
            self.cleaned_data['departements'] = ""
            self.cleaned_data['topics'] = ""
            self.cleaned_data['themes'] = ""
        return self.cleaned_data

