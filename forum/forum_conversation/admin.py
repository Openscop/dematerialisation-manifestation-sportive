"""
    Forum conversation model admin definitions
    ==========================================

    Ce module définie les entrés de forum conversation dans l'interface d'admin

"""
from django.contrib import admin

from forum.models.fields import MarkupTextField, MarkupTextFieldWidget
from .models import Post, Topic, Signal, ForumAbonnement, Theme
from .forum_attachments.models import Attachment


class AttachmentInline(admin.TabularInline):
    model = Attachment
    extra = 1


class SignalAdmin(admin.ModelAdmin):
    list_display = (
        'post', 'user', 'raison', 'commentaire',
    )


class ForumAbonnementAdmin(admin.ModelAdmin):
    list_display = (
        'pk', 'user'
    )
    list_filter = ('forum', 'user')


class ThemeAdmin(admin.ModelAdmin):
    list_display = (
        'pk', 'theme', 'color'
    )


class PostAdmin(admin.ModelAdmin):
    """ The Post model admin. """

    inlines = [AttachmentInline, ]
    list_display = ('__str__', 'topic', 'poster', 'updated', 'approved')
    list_filter = ('created', 'updated',)
    raw_id_fields = ('poster', 'topic',)
    search_fields = ('content',)
    list_editable = ('approved',)

    formfield_overrides = {
        MarkupTextField: {'widget': MarkupTextFieldWidget},
    }


class TopicAdmin(admin.ModelAdmin):
    """ The Topic model admin. """

    list_display = (
        'subject', 'forum', 'created', 'first_post', 'last_post', 'posts_count', 'approved',
    )
    list_filter = ('created', 'updated',)
    raw_id_fields = ('poster', 'subscribers', )
    search_fields = ('subject',)
    list_editable = ('approved',)


admin.site.register(Signal, SignalAdmin)
admin.site.register(ForumAbonnement, ForumAbonnementAdmin)
admin.site.register(Theme, ThemeAdmin)
admin.site.register(Topic, TopicAdmin)
admin.site.register(Post, PostAdmin)
