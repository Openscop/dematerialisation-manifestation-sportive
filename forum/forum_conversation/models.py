from django.urls import reverse
from django.db import models
from django.db.models import Q
from django.utils.translation import gettext_lazy as _

from sports.models.sport import Activite, Discipline
from administrative_division.models import Departement
from messagerie.models import Message
from core.models import User
from forum.forum.models import Forum
from structure.models.service import ServiceInstructeur, ServiceConsulte
from .abstract_models import AbstractTopic, AbstractPost


class Theme(models.Model):
    """Thème d'un sujet"""

    theme = models.CharField(verbose_name="Nom du thème", max_length=300, unique=True)
    color = models.CharField(verbose_name="Couleur du thème", default="#a8b6bf", max_length=10)

    def __str__(self):
        return self.theme


class Topic(AbstractTopic):

    """Represente les Topic d'un forum"""

    TOPIC_UNLOCKED, TOPIC_LOCKED, TOPIC_MOVED, TOPIC_RESOLVED = 0, 1, 2, 3
    STATUS_CHOICES = (
        (TOPIC_UNLOCKED, _('Topic unlocked')),
        (TOPIC_LOCKED, _('Topic locked')),
        (TOPIC_MOVED, _('Topic moved')),
        (TOPIC_RESOLVED, 'Sujet clôt'),
    )

    dossier = models.ForeignKey('evenements.Manif', on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Dossier')
    deplace = models.BooleanField(default=False, verbose_name='Sujet déplacer dans un autre forum')
    theme = models.ManyToManyField(Theme, verbose_name="Thème")
    slug = models.SlugField(max_length=255, verbose_name=_('Slug'), allow_unicode=True)
    status = models.PositiveIntegerField(
        choices=STATUS_CHOICES, db_index=True, verbose_name=_('Topic status'),
    )

    def get_absolute_url(self):
        """ Renvoye l'URl des topic """
        liens = reverse('forum_conversation:topic', kwargs={
            'forum_slug': self.forum.slug, 'forum_pk': self.forum.pk,
            'slug': self.slug, 'pk': self.pk})
        return liens

    def get_unsubscribe_url(self):
        """Permet aux uttilisateurs de se désabonné d'un topic"""
        liens = reverse('forum_member:topic_unsubscribe', kwargs={
            'pk': self.pk})
        return liens

    def update_trackers(self):
        """ Updates the denormalized trackers associated with the topic instance. """
        self.posts_count = self.posts.filter(approved=True).count()
        first_post = self.posts.filter(approved=True).order_by('created').first()
        last_post = self.posts.filter(approved=True).order_by('-created').first()
        self.first_post = first_post
        self.last_post = last_post
        self.last_post_on = last_post.created if last_post else None
        self._simple_save()
        # Trigger the forum-level trackers update
        self.forum.update_trackers()

    def get_destinataire_assistance(self, user):
        """ Retourne le type et le service ou utilisateur à qui envoyé une demande d'assistance"""
        if user.has_group('Administrateurs d\'instance'):  # Si l'utilisateur est du groupe support on contacte l'équipe support
            destinataire_assistance = ServiceConsulte.objects.get(type_service_fk__categorie_fk=2)
            type_destinataire = "support"
        elif self.dossier and self.dossier.get_instruction():  # Si le sujet a un dossier on contacte la préfecture concernée
            destinataire_assistance = self.dossier.get_instruction().get_prefecture_concernee()
            type_destinataire = "préfecture"
            if destinataire_assistance in user.organisation_m2m.all():  # Sauf si l'utilisateur fais déja parti de se service, alors on l'envoie à l'administrateur d'instance
                destinataire_assistance = User.objects.filter(groups__name='Administrateurs d\'instance',
                                                              default_instance=user.default_instance)
                type_destinataire = "administrateur"
        else:  # Si pas de dossier on l'envoie a l'administrateur d'instance
            destinataire_assistance = User.objects.filter(groups__name='Administrateurs d\'instance',
                                                          default_instance=user.default_instance)
            type_destinataire = "administrateur"
        return [destinataire_assistance, type_destinataire]

    def send_demande_assistance(self, user, post_pk):
        """Envoie une demande d'assistance"""
        liens = self.get_absolute_url() + "?post=" + str(post_pk) + "#" + str(post_pk)
        type_destinataire = self.get_destinataire_assistance(user)[1]
        destinataire_assistance = self.get_destinataire_assistance(user)[0]
        destinataire = []
        if type_destinataire == "préfecture" or type_destinataire == "support":
            for user in destinataire_assistance.get_users_list():
                destinataire.append([user, destinataire_assistance])
        else:
            destinataire = destinataire_assistance
        theme_str = ""
        for theme in self.theme.all():
            theme_str += str(theme) + ", "
        theme_str = theme_str[:-2] + "."
        Message.objects.creer_et_envoyer(type_msg='action', expediteur=None,
                                         destinataires=destinataire,
                                         titre=f"Demande d'assistance : {self}",
                                         contenu=f"Vous êtes sollicités pour répondre à une demande d’assistance :"
                                                 f"<br>Thème(s) : {theme_str}"
                                                 f"<br>Sujet : <a href={liens}>{self}</a>"
                                         )

    @property
    def is_clot(self):
        """ Returns ``True`` if the topic is locked. """
        return self.status == self.TOPIC_RESOLVED


class Post(AbstractPost):
    """Représente les messages d'un Topic"""

    dossier = models.ForeignKey('evenements.Manif', on_delete=models.CASCADE, null=True, verbose_name='Dossier')
    theme = models.ManyToManyField(Theme, verbose_name="Thème")
    demande_assistance = models.BooleanField(default=False, verbose_name="Demande d'assistance")

    def save(self, *args, **kwargs):
        """ Sauvegarde les posts. """
        new_post = self.pk is None
        super().save(*args, **kwargs)

        liens = self.topic.get_absolute_url() + "?post=" + str(self.pk) + "#" + str(self.pk)
        unsubscribe = self.topic.get_unsubscribe_url()
        subs = User.objects.filter(topic_subscriptions=self.topic).exclude(pk=self.poster.pk).exclude(
            pk=self.topic.poster.pk)
        abonnements_topic = ForumAbonnement.objects.filter(topics=self.topic)
        abonnements_forum = ForumAbonnement.objects.filter(forum=self.topic.forum, tout=True)
        # Si c'est une demande d'assitance envoie une demande d'action
        if self.demande_assistance:
            self.topic.send_demande_assistance(self.poster, self.pk)
        #Si nouveau sujet
        if self.topic.first_post == self and new_post:
            for abonnement_forum in abonnements_forum:
                if self.poster != abonnement_forum.user:
                    Message.objects.creer_et_envoyer(type_msg='forum', expediteur=None,
                                                     destinataires=[abonnement_forum.user],
                                                     titre=f"Un nouveau sujet a été publié dans le {self.topic.forum}",
                                                     contenu=f"Le sujet : <a href=\"{self.topic.get_absolute_url()}\">"
                                                             f"<strong>{self.subject}</strong></a> "
                                                             f"a été publié dans le {self.topic.forum}."
                                                     )
            departement = self.poster.get_instance().departement
            activite = False
            discipline = False
            if self.dossier:
                activite = self.dossier.activite
                discipline = self.dossier.activite.discipline

            abonnements = ForumAbonnement.objects.filter(forum=self.topic.forum) \
                .filter(Q(disciplines=discipline)
                        | Q(activites=activite)
                        | Q(departements=departement)
                        | Q(themes__in=self.theme.all())).distinct()

            if abonnements:
                for abo in abonnements:
                    if self.poster != abo.user:
                        gerer_abo_url = reverse('forum_conversation:gerer_forum_abonnement',
                                                kwargs={'forum_pk': self.topic.forum.pk,
                                                        'forum_slug': self.topic.forum.slug,
                                                        'pk': abo.pk,
                                                        'multiple': False})

                        Message.objects.creer_et_envoyer(type_msg='forum', expediteur=None,
                                                         destinataires=[abo.user],
                                                         titre="Un nouveau sujet correspond à l'un de vos abonnements",
                                                         contenu=f"Le sujet : <a href=\"{self.topic.get_absolute_url()}\">"
                                                                 f"<strong>{self.subject}</strong></a> "
                                                                 f"correspond à un ou plusieurs de vos abonnements.<br>"
                                                                 f"Vous pouvez gérer vos abonnements en cliquant sur ce lien "
                                                                 f"<a href=\"{gerer_abo_url}\">Modifier mes abonnements</a>")
            else:
                admin_instance = User.objects.filter(groups__name="Administrateurs d'instance",
                                                     default_instance=self.poster.default_instance)
                for admin in admin_instance:
                    if self.poster != admin:
                        Message.objects.creer_et_envoyer(type_msg='forum', expediteur=None, destinataires=[admin],
                                                         titre="Un nouveau sujet a été publié dans votre département",
                                                         contenu=f"Le sujet : <a href=\"{self.topic.get_absolute_url()}\">"
                                                                 f"<strong>{self.subject}</strong></a> "
                                                                 f"a été publié dans votre département.<br>"
                                                                 f"Vous recevez ce message, car tous les instructeurs "
                                                                 f"de votre département ont supprimé leurs notifications.")
        #Si réponse à un sujet
        elif new_post:
            for abonnement_forum in abonnements_forum:
                if self.poster != abonnement_forum.user:
                    Message.objects.creer_et_envoyer(type_msg='forum', expediteur=None,
                                                     destinataires=[abonnement_forum.user],
                                                     titre=f"Nouvelle réponse sur le {self.topic.forum}",
                                                     contenu=f"{self.poster.get_full_name()} à répondu au sujet : <a href=\"{liens}\">"
                                                             f"<strong>{self.subject}</strong></a>."
                                                     )

            for sub in subs:
                if sub != self.topic.poster and sub != self.poster and sub != self.updated_by:
                    Message.objects.creer_et_envoyer(type_msg='forum', expediteur=None, destinataires=[sub],
                                                     titre='Réponse à un sujet que vous suivez',
                                                     contenu=f"{self.poster.get_full_name()} a répondu à un sujet dans lequel vous êtes intervenu : "
                                                             f"<a href=\"{liens}\"><strong>{self.topic.subject}</strong></a> "
                                                             f"( <a href=\"{unsubscribe}\"> cliquez ici pour ne plus suivre </a> )")

            if self.poster != self.topic.poster:
                Message.objects.creer_et_envoyer(type_msg='forum', expediteur=None, destinataires=[self.topic.poster, self.topic.posts.model.poster],
                                                 titre='Une personne a répondu a votre sujet',
                                                 contenu=f"{self.poster.get_full_name()} a répondu à votre sujet : "
                                                         f"<a href=\"{liens}\"><strong>{self.topic.subject}</strong></a> "
                                                         f"( <a href=\"{unsubscribe}\"> cliquez ici pour ne plus suivre </a> )")

            for abonnement in abonnements_topic:
                if abonnement.user != self.topic.poster \
                        and abonnement.user != self.poster \
                        and abonnement.user != self.updated_by \
                        and abonnement.user not in subs:
                    Message.objects.creer_et_envoyer(type_msg='forum', expediteur=None, destinataires=[abonnement.user],
                                                     titre='Réponse à un sujet que vous suivez',
                                                     contenu=f"{self.poster.get_full_name()} a répondu à un sujet que vous suivez : "
                                                             f"<a href=\"{liens}\"><strong>{self.topic.subject}</strong></a>")

            self.topic.subscribers.add(self.poster)
            if (new_post and self.topic.first_post is None) or self.is_topic_head:
                if self.subject != self.topic.subject or self.approved != self.topic.approved:
                    self.topic.subject = self.subject
                    self.topic.approved = self.approved

        # Édition d'un post
        elif self.approved: # Vérifier qu'il ne s'agit pas d'une suppression de post
            for sub in subs:
                if sub != self.updated_by:
                    Message.objects.creer_et_envoyer(type_msg='forum', expediteur=None, destinataires=[sub],
                                                     titre='Modification d\'un sujet que vous suivez',
                                                     contenu=f"{self.updated_by.get_full_name()} a modifié un message dans un sujet où vous êtes intervenu :"
                                                             f"<a href=\"{liens}\"><strong>{self.topic.subject}</strong></a> "
                                                             f"( <a href=\"{unsubscribe}\"> cliquez ici pour ne plus suivre </a> )")
            for abonnement in abonnements_topic:
                if abonnement.user != self.updated_by and abonnement.user not in subs:
                    Message.objects.creer_et_envoyer(type_msg='forum', expediteur=None, destinataires=[abonnement.user],
                                                     titre='Modification d\'un sujet que vous suivez',
                                                     contenu=f"{self.updated_by.get_full_name()} a modifié un message dans un sujet que vous suivez :"
                                                             f"<a href=\"{liens}\"><strong>{self.topic.subject}</strong></a>")

        self.topic.update_trackers()


class Signal(models.Model):
    """Permet de signaler un post"""

    RAISON_SIGNALEMENT = (
        ("1", "Contenu inapproprié"),
        ("2", "Spam ou publicité"),
        ("3", "Menaces ou incitation à la violence"),
        ("4", "Autre raison"),
    )

    post = models.ForeignKey('Post', on_delete=models.CASCADE, verbose_name='Post')
    user = models.ForeignKey(User, on_delete=models.SET_NULL, verbose_name='Signaler par', null=True)
    raison = models.CharField(choices=RAISON_SIGNALEMENT, max_length=150, default='1')
    commentaire = models.CharField(max_length=250, null=True, verbose_name='Commentaire', blank=True)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        nb_signal = Signal.objects.filter(post=self.post).count()
        if nb_signal == 5:
            self.post.approved = False
            self.post.save()
        liens = self.post.topic.get_absolute_url()
        commentaire = ''
        if self.commentaire:
            commentaire = 'Commentaire : ' + self.commentaire

        moderateur = []
        services_instructeurs = ServiceInstructeur.objects.filter(instance_fk=self.post.poster.default_instance)
        for service in services_instructeurs:
            if not service.is_mairie():
                moderateur.append(service.get_users_list())
        moderateur.append(User.objects.filter(groups__name="Administrateurs d'instance", default_instance=self.post.poster.default_instance))
        Message.objects.creer_et_envoyer(type_msg='forum', expediteur=None, destinataires=moderateur,
                                         titre='Un commentaire mérite votre intention.',
                                         contenu=f"Un commentaire a été signalé sur le sujet : <a href='{liens}'>"
                                                 f"<strong>{self.post.topic.subject}</strong></a><br>"
                                                 f"Raison du signalement : {self.get_raison_display()}<br>{commentaire}"
                                         )


class ForumAbonnement(models.Model):
    """Permet de s'abonner à des combinaisons de filtre"""

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    forum = models.ForeignKey(Forum, on_delete=models.CASCADE)
    activites = models.ManyToManyField(Activite, blank=True)
    disciplines = models.ManyToManyField(Discipline, blank=True)
    departements = models.ManyToManyField(Departement, blank=True)
    topics = models.ManyToManyField(Topic, blank=True, verbose_name="Sujets")
    themes = models.ManyToManyField(Theme, blank=True, verbose_name="Thèmes")
    tout = models.BooleanField(default=False, verbose_name="Abonné à tout les sujets de ce forum")

