# Custom models should be declared before importing
# django-machina models
import os
from forum.conf import settings as machina_settings
from forum.core.db.models import model_factory
from forum.forum_conversation.models import Post

from django.db import models
from django.utils.translation import gettext_lazy as _

from core.FileTypevalidator import file_type_validator


def get_attachment_file_upload_to(instance, filename):
    """ Returns a valid upload path for the file of an attachment. """
    return instance.get_file_upload_to(filename)


class Attachment(models.Model):
    post = models.ForeignKey(
        Post, related_name='attachments', on_delete=models.SET_NULL,
        verbose_name=_('Post'), null=True
    )
    file = models.FileField(upload_to=get_attachment_file_upload_to, verbose_name=_('File'), validators=[file_type_validator])
    comment = models.CharField(max_length=255, verbose_name=_('Comment'), blank=True, null=True)
    scan_antivirus = models.BooleanField(default=True, verbose_name="Scan antivirus effectué")

    class Meta:
        abstract = True
        app_label = 'forum_attachments'
        verbose_name = _('Attachment')
        verbose_name_plural = _('Attachments')

    def __str__(self):
        return '{}'.format(self.post.subject)

    @property
    def filename(self):
        """ Returns the filename of the considered attachment. """
        return os.path.basename(self.file.name)

    def get_file_upload_to(self, filename):
        """ Returns the path to upload the associated file to. """
        return os.path.join(machina_settings.ATTACHMENT_FILE_UPLOAD_TO + "/" + str(self.pk), filename)


Attachment = model_factory(Attachment)