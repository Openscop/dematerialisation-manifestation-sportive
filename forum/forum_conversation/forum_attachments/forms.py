"""
    Forum attachments forms
    =======================

    This module defines forms provided by the ``forum_attachments`` application.

"""
from django.conf import settings
from django import forms
from django.forms.models import BaseModelFormSet, modelformset_factory

from forum.conf import settings as machina_settings
from core.tasks import pj_forum
from .models import Attachment


class BaseAttachmentFormset(BaseModelFormSet):
    """ Base class allowing to define forum attachment formsets. """

    def __init__(self, *args, **kwargs):
        self.post = kwargs.pop('post', None)
        super().__init__(*args, **kwargs)

    def save(self, commit=True, **kwargs):
        """ Saves the considered instances. """
        super().save(commit)

        if self.post:
            for form in self.forms:
                if form.instance.file:
                    form.instance.refresh_from_db()
                    form.instance.post = self.post
                    if settings.CELERY_ENABLED:
                        form.instance.scan_antivirus = False
                        pj_forum.delay(self.post.pk, form.instance.pk)
                        form.instance.save()


class AttachmentForm(forms.ModelForm):
    """ Allows to upload forum attachments. """

    class Meta:
        model = Attachment
        fields = ['file', 'comment', ]


AttachmentFormset = modelformset_factory(
    Attachment, AttachmentForm,
    formset=BaseAttachmentFormset,
    can_delete=True, extra=1,
    max_num=machina_settings.ATTACHMENT_MAX_FILES_PER_POST,
    validate_max=True,
)
