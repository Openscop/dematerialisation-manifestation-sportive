/* Ce script met en place la barre des filtre ainsi que la pagination */

var theme = ""
$('.chosen-container').css('width', '100%')

var options = {
    valueNames: ['activite', 'activite_icone', 'auteur', 'dossier', 'sujet', 'departement', 'vue', 'nombre_post', 'last_post_poster', "last_message", 'topic_etat',
        {name: "theme", attr: "data-theme"},{name: "service", attr: "data-service"}, {name: "departement_user", attr: "data-departement_user"},
        {name: "last_message_timestamp", attr: "data-last-message"}, {name: "statut", attr: "data-statut"}, {name: "topic_link", attr: "href"},
        {name: "last_post_link", attr: "href"}, {name: "dossier_link", attr: "href"}, {name: "pk", attr: "id"},
    ],
    item : `<li class="item list-group-item">
            <div class="row m-0 px-0 pk">
                <div class="ps-0 col-md-8 col-sm-9 col-xs-11 topic-name">
                    <div class="topic-data-table" style="width: 100%">
                        <div class="d-flex">
                            <div class="pt-1 pe-3 align-top topic-icon col-1 topic_etat"> 
                            </div>
                            <div class="align-top topic-name">
                              <h6><a class="topic_link sujet topic-name-link topic_link" ></a></h6>
                                <div>
                                <div class="topic-dossier-info">
                                        <a class="dossier_link">
                                            <span><span class="dossier"></span></span>
                                        </a>
                                        <br>
                                        <span class="activite_icone"></span><span class="activite"></span>
                                        <span class="departement"></span>
                                    <span class="statut"></span>
                                    <span class="service"></span>
                                    <span class="departement_user"></span>
                                    <span class="theme" data-theme=""></span>
                                </div>
                                </div>
                                <div class="topic-theme">
                                
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="py-2 col-md-1 d-none d-md-block text-center topic-count"><span class="nombre_post"></span></div>
                <div class="py-2 col-md-1 d-none d-md-block text-center topic-count"><span class="vue"></span></div>
                <div class="py-2 col-md-2 col-sm-3 d-none d-sm-block topic-last-post">

                                Par: <span class="last_post_poster"></span>

                            <a class="last_post_link"><i class="fas fa-arrow-circle-right "></i></a>
                        <br />
                        <small><span class="last_message last_message_timestamp" data-last-message=""></span></small>
                </div>
            </div>    
    </li>`,
    page: 30,
    pagination: [{
        item: "<li class=\"page-item\"><a class=\"page page-link btn-primary\" href=\"#\" ></a></li>",
        outerWindow: 1,
        innerWindow: 4,
    }]
};

var hackerList = new List('hacker-list', options);

var typingTimer;
var doneTypingInterval = 1000; // Interval avant de lancer la recherche

//On Keypup sur l'input recherche, débute l'interval
$("#input-search-forum").keyup(function (){
    hackerList.clear()
    $(".topiclist").find(".list-group").append(html_chargement_en_cours())
    clearTimeout(typingTimer);
    typingTimer = setTimeout(rechercheFulltext, doneTypingInterval); // Appelle la fonction de recherche une fois l'interval écouler
});

//On Keydown, reset de l'interval
$("#input-search-forum").on('keydown', function () {
    clearTimeout(typingTimer);
});

// Fonction de recherche full content
function rechercheFulltext(){
    // Reset les filtres
    $('.theme-sort').removeClass('theme_selected')
    $('.select-filter').val('').trigger("chosen:updated");

    var mots_clef = $("#input-search-forum").val()
    if(mots_clef.includes(" ")){
        mots_clef = mots_clef.split(' ').join('_')
    }
    if(window.location.hash){
        var url = window.location.href.split('?')[0] + "recherche-sujet/"
    }
    else{
        var url = window.location.href + "recherche-sujet/"
    }
    hackerList = new List('hacker-list', options);
    hackerList.page = 200
    $.get({
        url: url.replace('#', ''),
        data: {
            'recherche':  mots_clef,
        },
        success :
            function (reponse){
                hackerList.clear()
                hackerList.add(reponse)
                for (i = 0; i < reponse.length; i++){
                    var theme_container = $('#'+reponse[i]["pk"]).find('.topic-theme')
                    for (y = 0; y < reponse[i]["theme_object"].length; y++){
                        theme_container.append(`<span class="badge me-1 mb-1 badge-pill" 
                                                      style="background-color: ${reponse[i]["theme_object"][y]["color"]}; 
                                                      font-size: 12px;">${reponse[i]["theme_object"][y]["theme"]}</span>`)
                    }
                }
                setTimeout(function(){ hackerList.page = 30; hackerList.update()}, 2000)
            }
    })
}

function hasTheme(array, theme){
    var has_theme = false
    for(var i = 0;   i < array.length; i++){
        if(theme===array[i]){
            has_theme = true
        }
    }
    return has_theme
}
function listFilter(){

    if($("#no-result")){
        $("#no-result").remove()
    }
    var theme = $('.theme_selected').attr('data-theme')
    var service = $('#service').val()
    var activite = $('#activite').val()
    var departement = $('#departement').val()
    var departement_user = $('#departement_user').val()
    var statut = $('#statut').val()

    if (departement_user && service &&  departement && activite && theme && statut){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement === departement
                && item.values().activite ===activite && hasTheme(item.values().theme.split(','), theme)
                && item.values().statut ===statut);
        });
    }

    else if (departement_user && service &&  departement && activite && theme){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement === departement
                && item.values().activite ===activite && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (departement_user && service && departement && activite && statut){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement === departement
                && item.values().activite ===activite && item.values().statut ===statut);
        });
    }

    else if (departement_user && service && departement && statut && theme){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement === departement
                && item.values().statut ===statut && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (departement_user && service && statut && activite && theme){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().statut === statut
                && item.values().activite ===activite && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (departement_user && statut && departement && activite && theme){
        hackerList.filter(function(item) {
            return (item.values().statut === statut && item.values().departement === departement
                && item.values().activite ===activite && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (statut && service &&  departement && activite && theme){
        hackerList.filter(function(item) {
            return (item.values().statut === statut && item.values().departement === departement
                && item.values().activite ===activite && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (theme && service &&  departement && activite){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement === departement
                && item.values().activite ===activite  && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (theme && service &&  departement && statut){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement === departement
                && item.values().statut ===statut  && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (theme && service &&  statut && activite){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().statut === statut
                && item.values().activite ===activite  && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (theme && statut &&  departement && activite){
        hackerList.filter(function(item) {
            return (item.values().statut === statut && item.values().departement === departement
                && item.values().activite ===activite  && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (statut && service &&  departement && activite){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement === departement
                && item.values().activite ===activite  && item.values().statut === statut);
        });
    }

    else if (theme && service &&  departement_user && activite){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement_user === departement_user
                && item.values().activite ===activite  && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (theme && service &&  departement_user && statut){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement_user === departement_user
                && item.values().statut ===statut  && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (theme && service && statut && activite){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().statut === statut
                && item.values().activite ===activite  && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (theme && statut && departement_user && activite){
        hackerList.filter(function(item) {
            return (item.values().statut === statut && item.values().departement_user === departement_user
                && item.values().activite ===activite  && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (statut && service &&  departement_user && activite){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement_user === departement_user
                && item.values().activite ===activite  && item.values().statut === statut);
        });
    }

    else if (theme && service &&  departement_user && departement){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement === departement
                && item.values().activite ===activite  && hasTheme(item.values().theme.split(','), theme));
        });
    }

    else if (departement_user &&  departement && activite){
        hackerList.filter(function(item) {
            return (item.values().departement_user === departement_user && item.values().departement === departement
                && item.values().activite ===activite);
        });
    }

    else if (departement_user &&  departement && statut){
        hackerList.filter(function(item) {
            return (item.values().departement_user === departement_user && item.values().departement === departement
                && item.values().statut ===statut);
        });
    }

    else if (departement_user &&  statut && activite){
        hackerList.filter(function(item) {
            return (item.values().departement_user === departement_user && item.values().statut === statut
                && item.values().activite ===activite);
        });
    }

    else if (statut &&  departement && activite){
        hackerList.filter(function(item) {
            return (item.values().statut === statut && item.values().departement === departement
                && item.values().activite ===activite);
        });
    }

    else if (service &&  departement_user && activite){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement_user === departement_user
                && item.values().activite ===activite);
        });
    }

    else if (service &&  departement && departement_user){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement === departement
                && item.values().departement_user ===departement_user);
        });
    }

    else if (theme && departement_user && statut){
        hackerList.filter(function(item) {
            return (hasTheme(item.values().theme.split(','), theme)
                && item.values().departement_user === departement_user
                && item.values().statut === statut);
        });
    }

    else if (theme && service){
        hackerList.filter(function(item) {
            return (hasTheme(item.values().theme.split(','), theme) && item.values().service ===service);
        });
    }

    else if (theme && departement){
        hackerList.filter(function(item) {
            return (hasTheme(item.values().theme.split(','), theme) && item.values().departement ===departement);
        });
    }

    else if (theme && departement_user){
        hackerList.filter(function(item) {
            return (hasTheme(item.values().theme.split(','), theme) && item.values().departement_user ===departement_user);
        });
    }


    else if (theme && statut){
        hackerList.filter(function(item) {
            return (hasTheme(item.values().theme.split(','), theme) && item.values().statut ===statut);
        });
    }

    else if (activite && statut){
        hackerList.filter(function(item) {
            return (item.values().activite ===activite && item.values().statut ===statut);
        });
    }

    else if (departement_user && statut){
        hackerList.filter(function(item) {
            return (item.values().departement_user === departement_user && item.values().statut ===statut);
        });
    }

    else if (departement && statut){
        hackerList.filter(function(item) {
            return (item.values().departement === departement && item.values().statut ===statut);
        });
    }

    else if (service && statut){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().statut ===statut);
        });
    }

    else if (theme && activite){
        hackerList.filter(function(item) {
            return (hasTheme(item.values().theme.split(','), theme) && item.values().activite ===activite);
        });
    }

    else if (departement_user && service &&  departement && activite){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement === departement
                && item.values().activite ===activite);
        });
    }

    else if (departement_user &&  departement && activite){
        hackerList.filter(function(item) {
            return (item.values().departement_user === departement_user && item.values().departement === departement
                && item.values().activite ===activite);
        });
    }

    else if (service &&  departement_user && activite){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement_user === departement_user
                && item.values().activite ===activite);
        });
    }

    else if (service &&  departement && departement_user){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement === departement
                && item.values().departement_user ===departement_user);
        });
    }

    else if (service &&  departement && activite){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement === departement
                && item.values().activite ===activite);
        });
    }

    else if (service &&  departement){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement === departement);
        });
    }
    else if (service &&  activite){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().activite ===activite);
        });
    }
    else if (departement &&  activite){
        hackerList.filter(function(item) {
            return (item.values().departement === departement && item.values().activite === activite);
        });
    }


    else if (departement_user &&  departement){
        hackerList.filter(function(item) {
            return (item.values().departement_user === departement_user && item.values().departement === departement);
        });
    }
    else if (service &&  departement_user){
        hackerList.filter(function(item) {
            return (item.values().service === service && item.values().departement_user ===departement_user);
        });
    }
    else if (departement_user &&  activite){
        hackerList.filter(function(item) {
            return (item.values().departement_user === departement_user && item.values().activite === activite);
        });
    }


    else if (service) {
        hackerList.filter(function(item) {
            return (item.values().service === service);
        });
    }
    else if (departement) {
        hackerList.filter(function(item) {
            return (item.values().departement === departement);
        });
    }
    else if (activite) {
        hackerList.filter(function(item) {
            return (item.values().activite === activite);
        });
    }
    else if (departement_user) {
        hackerList.filter(function(item) {
            return (item.values().departement_user === departement_user);
        });
    }

    else if (statut){
        hackerList.filter(function(item) {
            return (item.values().statut == statut);
        });
    }
    else if (theme){
        hackerList.filter(function(item) {
            return (hasTheme(item.values().theme.split(','), theme))
        })
    }
    else {
        hackerList.filter();
    }

    if(hackerList.visibleItems.length < 1){
        $('#hacker-list').append("<div id=\"no-result\" class=\"alert alert-warning container text-center\">Aucun sujet trouvé avec les filtres sélectionnés. Veuillez renouveler votre recherche.</div>\n")
    }
}

$('#departement_user').change(function () {
    listFilter()
});

$('#theme').change(function () {
    listFilter()
});

$('#service').change(function () {
    listFilter()
});

$('#activite').change(function () {
    listFilter()
});

$('#departement').change(function () {
    listFilter()
});

$('#statut').change(function (){
    listFilter()
})


$('.theme-sort').click(function (){
    $('.theme-sort').removeClass('theme_selected')
    $(this).addClass('theme_selected')
    listFilter()
})

// Applique des filtres spécifiques si l'on vient de la page de suivie
if(window.location.hash) {
    var theme_id = window.location.hash.substring(1);
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    $('*[data-theme="'+theme_id+'"]').click();
    $("#departement_user").val(urlParams.get('dep')).trigger("chosen:updated");
    $("#statut").val("False").trigger("chosen:updated");
    listFilter()
}
