# """
#     Forum tracking model admin definitions
#     ======================================
#
#     This module defines admin classes used to populate the Django administration dashboard.
#
# """
#
# from django.contrib import admin
#
# from .models import ForumReadTrack, TopicReadTrack
#
#
# class ForumReadTrackAdmin(admin.ModelAdmin):
#     """ The Forum Read Track model admin. """
#
#     list_display = ('__str__', 'user', 'forum', 'mark_time',)
#     list_filter = ('mark_time',)
#
#
# class TopicReadTrackAdmin(admin.ModelAdmin):
#     """ The Topic Read Track model admin. """
#
#     list_display = ('__str__', 'user', 'topic', 'mark_time',)
#     list_filter = ('mark_time',)
#
# admin.site.register(TopicReadTrack, TopicReadTrackAdmin)
#
# admin.site.register(ForumReadTrack, ForumReadTrackAdmin)
