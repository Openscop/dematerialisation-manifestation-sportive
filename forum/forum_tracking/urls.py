"""
    Forum tracking URLs
    ===================

    This module defines URL patterns associated with the django-machina's ``forum_tracking``
    application.

"""

from django.urls import path

from forum.core.urls import URLPatternsFactory
from forum.forum_tracking.views import MarkForumsReadView, MarkTopicsReadView, UnreadTopicsView


class ForumTrackingURLPatternsFactory(URLPatternsFactory):
    """ Allows to generate the URL patterns of the ``forum_search`` application. """

    app_namespace = 'forum_tracking'

    def get_urlpatterns(self):
        """ Returns the URL patterns managed by the considered factory / application. """
        return [
            path(
                'mark/forums/',
                MarkForumsReadView.as_view(),
                name='mark_all_forums_read',
            ),
            path(
                'mark/forums/<int:pk>/',
                MarkForumsReadView.as_view(),
                name='mark_subforums_read',
            ),
            path(
                'mark/forum/<int:pk>/topics/',
                MarkTopicsReadView.as_view(),
                name='mark_topics_read',
            ),
            path(
                'unread-topics/',
                UnreadTopicsView.as_view(),
                name='unread_topics',
            ),
        ]


urlpatterns_factory = ForumTrackingURLPatternsFactory()
