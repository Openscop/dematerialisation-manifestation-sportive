"""
    Forum tracking models
    =====================

    This module defines models provided by the ``forum_tracking`` application.

"""

from .abstract_models import AbstractForumReadTrack, AbstractTopicReadTrack


class ForumReadTrack(AbstractForumReadTrack):
    pass


class TopicReadTrack(AbstractTopicReadTrack):
    pass

