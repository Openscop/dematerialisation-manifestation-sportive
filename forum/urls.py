"""
    Machina URLs
    ============

    This module imports all the URLs defined by the forum-related applications.

"""

from django.urls import include, re_path

from forum.core.urls import URLPatternsFactory
from forum.forum.urls import urlpatterns_factory as forum_urlpatterns_factory
from forum.forum_conversation.urls import urlpatterns_factory as conversation_urlpatterns_factory
from forum.forum_tracking.urls import urlpatterns_factory as tracking_urlpatterns_factory
from forum.forum_moderation.urls import urlpatterns_factory as moderation_urlpatterns_factory
from forum.forum_feeds.urls import urlpatterns_factory as feeds_urlpatterns_factory
from forum.forum_member.urls import urlpatterns_factory as member_urlpatterns_factory

class BoardURLPatternsFactory(URLPatternsFactory):
    """ Allows to generate the URL patterns of the whole forum application. """

    # forum_urlpatterns_factory = get_class('forum.urls', 'urlpatterns_factory')
    # conversation_urlpatterns_factory = get_class('forum_conversation.urls', 'urlpatterns_factory')
    # feeds_urlpatterns_factory = get_class('forum_feeds.urls', 'urlpatterns_factory')
    # member_urlpatterns_factory = get_class('forum_member.urls', 'urlpatterns_factory')
    # moderation_urlpatterns_factory = get_class('forum_moderation.urls', 'urlpatterns_factory')
    # search_urlpatterns_factory = get_class('forum_search.urls', 'urlpatterns_factory')
    # tracking_urlpatterns_factory = get_class('forum_tracking.urls', 'urlpatterns_factory')

    def get_urlpatterns(self):
        """ Returns the URL patterns managed by the considered factory / application. """
        return [
            re_path(r'', include(forum_urlpatterns_factory.urlpatterns)),
            re_path(r'', include(conversation_urlpatterns_factory.urlpatterns)),
            re_path(r'^feeds/', include(feeds_urlpatterns_factory.urlpatterns)),
            re_path(r'^member/', include(member_urlpatterns_factory.urlpatterns)),
            re_path(r'^moderation/', include(moderation_urlpatterns_factory.urlpatterns)),
            # re_path(r'^search/', include(self.search_urlpatterns_factory.urlpatterns)),
            re_path(r'^tracking/', include(tracking_urlpatterns_factory.urlpatterns)),
        ]


urlpatterns_factory = BoardURLPatternsFactory()
urlpatterns = urlpatterns_factory.get_urlpatterns()
