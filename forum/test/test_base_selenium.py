import time
from django.utils import timezone
from django.contrib.auth.hashers import make_password as make
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.contenttypes.models import ContentType

from forum.forum_permission.models import GroupForumPermission, UserForumPermission
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.by import By
from allauth.account.models import EmailAddress
from forum.factories import PostFactory,  create_forum, create_topic


from core.models import Instance, ConfigurationGlobale
from core.factories import UserFactory, GroupFactory
from evenements.factories import DcnmFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from sports.factories import ActiviteFactory
from forum.forum_permission.shortcuts import assign_perm
from forum.forum_permission.handler import PermissionHandler
from forum.forum_conversation.models import Theme
from structure.factories.structure import StructureOrganisatriceFactory
from structure.factories.service import PrefectureFactory


class SeleniumCommunClass(StaticLiveServerTestCase):

    DELAY = 0.35

    ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme

    def init_setup(self):
        """
        Préparation du test
        """
        self.port = 12345
        self.selenium = WebDriver()
        ConfigurationGlobale.objects.create(pk=1, mail_action=True, mail_suivi=True, mail_tracabilite=True, mail_actualite=True)
        self.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        self.selenium.set_window_size(800, 1000)

        # Création des objets sur le 42

        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                        instance__etat_s="ouvert")
        self.dep = dep
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        self.prefecture = arrondissement.organisations.first()
        self.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        self.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)


        autre_dep = DepartementFactory.create(name='43',
                                        instance__name="instance de test 2",
                                        instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                        instance__etat_s="ouvert")
        self.autre_dep = autre_dep
        arrondissement_2 = ArrondissementFactory.create(name='Le Puy‑en‑Velay', code='57', departement=autre_dep)
        self.prefecture_2 = PrefectureFactory(arrondissement_m2m=arrondissement_2, instance_fk=autre_dep.instance)
        self.commune = CommuneFactory(name='Le Puy‑en‑Velay', arrondissement=arrondissement)



        # Création des utilisateurs
        yesterday = timezone.now() - timezone.timedelta(days=2)
        self.organisateur = UserFactory.create(username='organisateur', password=make("123"),
                                               default_instance=dep.instance, date_joined=yesterday)
        EmailAddress.objects.create(user=self.organisateur, email='organisateur@example.com', primary=True,
                                    verified=True)
        self.structure_organisatrice = StructureOrganisatriceFactory.create(instance_fk=dep.instance)
        self.structure_organisatrice.commune_m2m.add(self.commune)
        self.organisateur.organisation_m2m.add(self.structure_organisatrice)

        self.instructeur = UserFactory.create(username='instructeur', password=make("123"),
                                              default_instance=dep.instance)
        EmailAddress.objects.create(user=self.instructeur, email='instructeur@example.com', primary=True,
                                    verified=True)
        self.instructeur.organisation_m2m.add(self.prefecture)


        #Admin instance
        self.admin_instance = UserFactory.create(username='admin_instance', password=make("123"),
                                               default_instance=dep.instance, date_joined=yesterday)
        group_admin_instance = GroupFactory.create(name='Administrateurs d\'instance')
        self.admin_instance.groups.add(group_admin_instance)
        EmailAddress.objects.create(user=self.admin_instance, email='admin_instance@example.com', primary=True,
                                    verified=True)
        self.admin_instance.organisation_m2m.add(self.prefecture)


        #Admin Technique
        self.admin_technique = UserFactory.create(username='admin_technique', password=make("123"),
                                               default_instance=dep.instance, date_joined=yesterday)

        group_admin_technique = GroupFactory.create(name='_support')
        self.admin_technique.groups.add(group_admin_technique)
        EmailAddress.objects.create(user=self.admin_technique, email='admin_technique@example.com', primary=True,
                                    verified=True)
        self.admin_technique.organisation_m2m.add(self.prefecture)

        # Création Discipline et Manifestation
        activ = ActiviteFactory.create()
        self.manifestation = DcnmFactory.create(ville_depart=self.commune, structure_organisatrice_fk=self.structure_organisatrice,
                                                nom='Manifestation_Test',
                                                activite=activ)
        self.manifestation.nb_participants = 1
        self.manifestation.description = 'une course qui fait courir'
        self.manifestation.nb_spectateurs = 100
        self.manifestation.nb_signaleurs = 1
        self.manifestation.nb_vehicules_accompagnement = 0
        self.manifestation.nom_contact = 'durand'
        self.manifestation.prenom_contact = 'joseph'
        self.manifestation.tel_contact = '0605555555'
        self.manifestation.save()

        # Création des models du forum
        self.perm_handler = PermissionHandler()
        self.top_level_forum = create_forum()
        self.top_level_forum.name = "Forum entraide"
        self.top_level_forum.save()
        self.topic = create_topic(forum=self.top_level_forum, poster=self.organisateur)
        self.post = PostFactory.create(topic=self.topic, poster=self.organisateur)

        self.forum_national = create_forum()
        self.forum_national.name = "Forum national des administrateurs d'instance"
        self.forum_national.save()

        self.forum_departemantal = create_forum()
        self.forum_departemantal.name = f"Forum administrateur d'instance {self.dep}"
        self.forum_departemantal.departement = self.dep
        self.forum_departemantal.parent_id = self.forum_national.pk
        self.forum_departemantal.save()

        self.autre_forum_departemantal = create_forum()
        self.autre_forum_departemantal.name = f"Forum administrateur d'instance {self.autre_dep}"
        self.autre_forum_departemantal.departement = self.autre_dep
        self.autre_forum_departemantal.parent_id = self.forum_national.pk
        self.autre_forum_departemantal.save()

        self.theme = Theme(theme="autre")
        self.theme.save()

        # Donne des permissions
        self.perm_handler = PermissionHandler()
        assign_perm('can_read_forum', self.organisateur, self.top_level_forum)
        assign_perm('can_see_forum', self.organisateur, self.top_level_forum)
        assign_perm('can_reply_to_topics', self.organisateur, self.top_level_forum)
        assign_perm('can_start_new_topics', self.organisateur, self.top_level_forum)
        assign_perm('can_post_without_approval', self.organisateur, self.top_level_forum)
        assign_perm('can_edit_own_posts', self.organisateur, self.top_level_forum)
        assign_perm('can_delete_own_posts', self.organisateur, self.top_level_forum)
        assign_perm('can_vote_in_polls', self.organisateur, self.top_level_forum)
        assign_perm('can_attach_file', self.organisateur, self.top_level_forum)
        assign_perm('can_download_file', self.organisateur, self.top_level_forum)

        assign_perm('can_start_new_topics', self.instructeur, self.top_level_forum)
        assign_perm('can_reply_to_topics', self.instructeur, self.top_level_forum)
        assign_perm('can_read_forum', self.instructeur, self.top_level_forum)
        assign_perm('can_see_forum', self.instructeur, self.top_level_forum)
        assign_perm('can_post_without_approval', self.instructeur, self.top_level_forum)
        assign_perm('can_post_stickies', self.instructeur, self.top_level_forum)
        assign_perm('can_post_announcements', self.instructeur, self.top_level_forum)
        assign_perm('can_create_polls', self.instructeur, self.top_level_forum)
        assign_perm('can_edit_posts', self.instructeur, self.top_level_forum)
        assign_perm('can_delete_posts', self.instructeur, self.top_level_forum)
        assign_perm('can_lock_topics', self.instructeur, self.top_level_forum)
        assign_perm('can_vote_in_polls', self.instructeur, self.top_level_forum)
        assign_perm('can_attach_file', self.instructeur, self.top_level_forum)
        assign_perm('can_download_file', self.instructeur, self.top_level_forum)

        assign_perm('can_read_forum', self.admin_instance, self.forum_national)
        assign_perm('can_see_forum', self.admin_instance, self.forum_national)
        assign_perm('can_reply_to_topics', self.admin_instance, self.forum_national)
        assign_perm('can_start_new_topics', self.admin_instance, self.forum_national)
        assign_perm('can_post_without_approval', self.admin_instance, self.forum_national)
        assign_perm('can_edit_own_posts', self.admin_instance, self.forum_national)
        assign_perm('can_delete_own_posts', self.admin_instance, self.forum_national)
        assign_perm('can_vote_in_polls', self.admin_instance, self.forum_national)
        assign_perm('can_attach_file', self.admin_instance, self.forum_national)
        assign_perm('can_download_file', self.admin_instance, self.forum_national)

        assign_perm('can_start_new_topics', self.admin_technique, self.forum_national)
        assign_perm('can_reply_to_topics', self.admin_technique, self.forum_national)
        assign_perm('can_read_forum', self.admin_technique, self.forum_national)
        assign_perm('can_see_forum', self.admin_technique, self.forum_national)
        assign_perm('can_post_without_approval', self.admin_technique, self.forum_national)
        assign_perm('can_post_stickies', self.admin_technique, self.forum_national)
        assign_perm('can_post_announcements', self.admin_technique, self.forum_national)
        assign_perm('can_create_polls', self.admin_technique, self.forum_national)
        assign_perm('can_edit_posts', self.admin_technique, self.forum_national)
        assign_perm('can_delete_posts', self.admin_technique, self.forum_national)
        assign_perm('can_lock_topics', self.admin_technique, self.forum_national)
        assign_perm('can_vote_in_polls', self.admin_technique, self.forum_national)
        assign_perm('can_attach_file', self.admin_technique, self.forum_national)
        assign_perm('can_download_file', self.admin_technique, self.forum_national)
        assign_perm('can_move_topics', self.admin_technique, self.forum_national)

        permissions = []
        permissions += [permission for permission in GroupForumPermission.objects.filter(forum=self.forum_national)]
        permissions += [permission for permission in UserForumPermission.objects.filter(forum=self.forum_national)]
        forum_departemental = self.forum_national.children.all()
        for forum in forum_departemental:
            for permission in permissions:
                new_permission = permission
                new_permission.pk = None
                new_permission.forum = forum
                new_permission.save()


        mon_fichier = open("/tmp/test.txt", "w")
        mon_fichier.write("Document Manifestation Sportive")
        mon_fichier.write("")
        mon_fichier.write("Test Sélénium")
        mon_fichier.close()

    def connexion(self, username):
        """ Connexion de l'utilisateur 'username' """
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element(By.NAME, "login")
        pseudo_input.send_keys(username)
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element(By.NAME, "password")
        password_input.send_keys('123')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.assertIn('Connexion avec ' + username + ' réussie', self.selenium.page_source)

    def deconnexion(self):
        """ Deconnexion de l'utilisateur """
        time.sleep(self.DELAY)
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.CLASS_NAME, 'deconnecter').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        time.sleep(self.DELAY)

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element(By.ID, id_chosen)
        chosen_select.click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements(By.TAG_NAME, 'li')
        for result in results:
            if value in result.text:
                result.click()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """
        cls.selenium.quit()
        super().tearDownClass()
