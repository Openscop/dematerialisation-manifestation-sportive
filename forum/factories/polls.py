import factory
import factory.django
from faker import Faker

from forum.factories.auth import UserFactory
from forum.factories.conversation import TopicFactory
from forum.forum_conversation.forum_polls.models import TopicPoll, TopicPollVote, TopicPollOption

faker = Faker()


class TopicPollFactory(factory.django.DjangoModelFactory):
    topic = factory.SubFactory(TopicFactory)
    question = faker.text(max_nb_chars=200)

    class Meta:
        model = TopicPoll


class TopicPollOptionFactory(factory.django.DjangoModelFactory):
    poll = factory.SubFactory(TopicPollFactory)
    text = faker.text(max_nb_chars=100)

    class Meta:
        model = TopicPollOption


class TopicPollVoteFactory(factory.django.DjangoModelFactory):
    poll_option = factory.SubFactory(TopicPollOptionFactory)
    voter = factory.SubFactory(UserFactory)

    class Meta:
        model = TopicPollVote
