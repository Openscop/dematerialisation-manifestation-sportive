import factory
import factory.django
from faker import Faker

from forum.factories.conversation import PostFactory
from forum.forum_conversation.forum_attachments.models import Attachment

faker = Faker()


class AttachmentFactory(factory.django.DjangoModelFactory):
    post = factory.SubFactory(PostFactory)
    comment = faker.text(max_nb_chars=255)
    file = factory.django.FileField()

    class Meta:
        model = Attachment
