import factory.django

from forum.factories.auth import UserFactory
from forum.factories.conversation import TopicFactory
from forum.factories.forum import ForumFactory

from forum.forum_tracking.models import ForumReadTrack, TopicReadTrack


class ForumReadTrackFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    forum = factory.SubFactory(ForumFactory)

    class Meta:
        model = ForumReadTrack


class TopicReadTrackFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    topic = factory.SubFactory(TopicFactory)

    class Meta:
        model = TopicReadTrack
