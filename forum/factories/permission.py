import factory
import factory.django

from forum.forum_permission.models import ForumPermission, UserForumPermission, GroupForumPermission


class ForumPermissionFactory(factory.django.DjangoModelFactory):
    codename = factory.Sequence(lambda n: 'perm-{}'.format(n))

    class Meta:
        model = ForumPermission


class UserForumPermissionFactory(factory.django.DjangoModelFactory):
    permission = factory.SubFactory(ForumPermissionFactory)

    class Meta:
        model = UserForumPermission


class GroupForumPermissionFactory(factory.django.DjangoModelFactory):
    permission = factory.SubFactory(ForumPermissionFactory)

    class Meta:
        model = GroupForumPermission
