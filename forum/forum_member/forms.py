# """
#     Forum member forms
#     ==================
#
#     This module defines forms provided by the ``forum_member`` application.
#
# """
# #TODO voire a supprimer
# from django import forms
#
# from forum.conf import settings as machina_settings
# from .models import ForumProfile
#
#
# class ForumProfileForm(forms.ModelForm):
#
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         if not machina_settings.PROFILE_AVATARS_ENABLED:
#             del self.fields["avatar"]
#
#     class Meta:
#         model = ForumProfile
#         fields = ['avatar', 'signature', ]
