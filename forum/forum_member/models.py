"""
    Forum member models
    ===================

    This module defines models provided by the ``forum_member`` application.

"""
#TODO voire a supprimer
from .abstract_models import AbstractForumProfile


class ForumProfile(AbstractForumProfile):
    pass
