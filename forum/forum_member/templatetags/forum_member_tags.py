from django import template

from forum.forum_member.shortcuts import get_forum_member_display_name


register = template.Library()


@register.filter
def forum_member_display_name(user):
    """ Returns the forum member display name to use for a given user.

    Usage::

        {{ user|forum_member_display_name }}

    """
    return get_forum_member_display_name(user)
