from django.views import View
from django.shortcuts import HttpResponse, redirect
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.generic.detail import SingleObjectMixin

from instructions.models import Avis, Instruction, AutorisationAcces
from instructions.decorators import verifier_secteur_instruction
from messagerie.models import Message
from structure.models.service import ServiceConsulte
from structure.models.cdsr import PresentationCDSR


class CreateAvisAJAXView(SingleObjectMixin, View):
    """Créé un avis via AJAX"""

    model = Instruction

    @method_decorator(verifier_secteur_instruction(), name='dispatch')
    def get(self, request, *args, **kwargs):

        instruction = self.get_object()
        instruction.referent = self.request.user
        service_demandeur = ServiceConsulte.objects.get(pk=self.request.session['service_pk'])
        all_mairie = self.request.GET.get('all_mairies', None)
        # S'il s'agit d'un envoie à toutes les mairies remplie le tableau avec les id des mairies de son instance
        if all_mairie:
            array_id = []
            for commune in instruction.manif.villes_traversees.all():
                for service in commune.organisation_set.all():
                    if service.is_mairie() and service.instance_fk == request.user.default_instance:
                        array_id.append(service.pk)
        # Sinon récupére les id envoyés
        else:
            array_id = self.request.GET.getlist('array_id[]', None)
        avis = None
        avis_envoye = False
        for id in array_id:
            service = ServiceConsulte.objects.get(pk=id)
            acces = AutorisationAcces.objects.filter(organisation=service, manif=instruction.manif).first()
            # Si il y a un accès on le révoque
            if acces:
                acces.date_revocation = timezone.now()
                acces.save()
                titre = "Accès en lecture révoqué"
                message = f"L'accès en lecture qui vous avait été permis pour le dossier {acces.manif}, vient d'être révoqué. " \
                          f"Vous faites maintenant l'objet d'une demande d'avis."
                destinataires = acces.organisation.get_utilisateurs_avec_service()
                Message.objects.creer_et_envoyer('info_suivi', [request.user, request.organisation], destinataires, titre, message,
                                                 manifestation_liee=acces.manif, objet_lie_nature="dossier")
                Message.objects.creer_et_envoyer('tracabilite', None, [request.user, request.organisation],
                                                 'Accès en lecture révoqué suite à une demande d\'avis',
                                                 f'Accès en lecture révoqué pour le dossier {acces.manif}')

            if not Avis.objects.filter(service_consulte_fk=service).filter(instruction=instruction).exists():
                historique_circuit_json = [{"entrant": service_demandeur.pk,
                                            "sortant": service.pk,
                                            "type": "creer",
                                            "date": str(timezone.now())}]
                avis = Avis.objects.create(instruction=instruction, service_consulte_fk=service,
                                           service_consulte_origine_fk=service, service_demandeur_fk=service_demandeur,
                                           historique_circuit_json=historique_circuit_json)
                avis.notifier_creation_avis(origine=[self.request.user, self.request.organisation], agents=service.get_users_list())
                avis_envoye = True

        if avis_envoye:
            instruction.envoyerDemandeAvis(service_demandeur)
            instruction.save()

        if request.path.split('/')[-2] == "cdsr" and avis_envoye:
            presentation = PresentationCDSR.objects.create(instruction_fk=instruction, avis_o2o=avis)
            presentation.invites_json = [{'membre': instruction.manif.structure_organisatrice_fk.pk, 'confirmé': False}]
            presentation.save()
            return redirect(instruction.get_absolute_url())

        return HttpResponse(avis_envoye)
