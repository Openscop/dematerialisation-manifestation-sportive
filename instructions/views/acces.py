# coding: utf-8
from django.http import HttpResponse, HttpResponseForbidden
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404
from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin

from evenements.models import Manif
from instructions.models import Instruction, Avis, AutorisationAcces
from core.util.permissions import login_requis
from messagerie.models import Message
from structure.models.organisation import Organisation


class AutorisationAccesRemove(SingleObjectMixin, View):
    """ Supprimer une autorisation d'accès """

    model = AutorisationAcces
    # Appel Ajax, le retour est donné par la fonction

    # Overrides
    def dispatch(self, *args, **kwargs):
        acces = self.get_object()
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        if acces:
            if (acces.createur == self.request.user or
                    (organisation.is_service_instructeur() and
                     self.request.user in acces.manif.get_instruction(instance=self.request.user.get_instance()).get_instructeurs())):
                return super().dispatch(*args, **kwargs)
        return HttpResponseForbidden("Requête rejetée")

    def post(self, request, *args, **kwargs):
        acces = self.get_object()
        acces.date_revocation = timezone.now()
        acces.save()
        titre = "Accès en lecture révoqué"
        message = f"L'accès en lecture qui vous avait été permis pour le dossier {acces.manif}, vient d'être révoqué."
        destinataires = acces.organisation.get_utilisateurs_avec_service()
        Message.objects.creer_et_envoyer('info_suivi', [request.user, request.organisation], destinataires, titre, message,
                                         manifestation_liee=acces.manif, objet_lie_nature="dossier")
        Message.objects.creer_et_envoyer('tracabilite', None, [request.user, request.organisation], 'Accès en lecture révoqué',
                                         f'Accès en lecture révoqué pour le dossier {acces.manif}')
        return HttpResponse("done")


@method_decorator(login_requis(), 'dispatch')
class AutorisationAccesCreate(View):
    """ class de création des autorisation d'accès """

    def post(self, request, *args,  **kwargs):
        manif, instruction, avis = None, None, None
        service_demandeur = Organisation.objects.get(pk=self.request.session['service_pk'])
        organisation_object = Organisation.objects.get(pk=self.request.POST.get('service_id'))
        if all([mot_cle in self.request.META['HTTP_REFERER'] for mot_cle in ['avis', 'instructions']]):
            avis = get_object_or_404(Avis, pk=self.kwargs.get('pk'))
            if service_demandeur != avis.service_consulte_fk and service_demandeur not in avis.service_consulte_history_m2m.all():
                return HttpResponse("Vous n'êtes pas concerné par cet avis")
            manif = avis.instruction.manif
            avis.agent = self.request.user
            avis.save()
        elif 'instructions' in self.request.META['HTTP_REFERER']:
            instruction = get_object_or_404(Instruction, pk=self.kwargs.get('pk'))
            instructeurs = []
            for service in instruction.service_instructeur_m2m.all():
                for user in service.get_users_list():
                    instructeurs.append(user)
            if self.request.user not in instructeurs:
                return HttpResponse("Vous n'êtes pas concerné par cette instruction")
            manif = instruction.manif
            instruction.referent = self.request.user
            instruction.save()
        else:
            manif = get_object_or_404(Manif, pk=self.kwargs.get('pk'))
        if manif:
            if manif.get_instruction():
                if organisation_object in manif.get_instruction().service_instructeur_m2m.all():
                    return HttpResponse("Cette manifestation est déjà en cours d'instruction par ce service.")
            if organisation_object == service_demandeur:
                return HttpResponse("Vous ne pouvez pas octroyer un accès en lecture à votre propre service !")
            elif organisation_object == manif.structure_organisatrice_fk:
                return HttpResponse("Vous ne pouvez pas octroyer un accès en lecture à l'organisateur de l'événement.")
            acces = AutorisationAcces(date_creation=timezone.now(), createur=self.request.user,
                                      organisation_origine_fk=service_demandeur, manif=manif)
            if self.request.POST.get('acces') == "complet":
                if instruction:
                    if not organisation_object.is_service_instructeur:
                        return HttpResponse("Vous ne pouvez pas octroyer un accès en lecture à ce service.")
                    acces.instruction = instruction
                else:
                    if organisation_object.type_service_fk.categorie_fk != service_demandeur.type_service_fk.categorie_fk:
                        return HttpResponse("Vous ne pouvez pas octroyer un accès en lecture à ce service.")
                if avis:
                    acces.avis = avis
            acces.organisation = organisation_object
            avis_old = Avis.objects.filter(instruction__manif=manif)
            for avis in avis_old:
                if organisation_object in avis.get_tout_services_concerne():
                    return HttpResponse(
                        "Vous ne pouvez pas octroyer un accès en lecture à un service déjà consulté via une demande d'avis.")
            acces_old = AutorisationAcces.objects.filter(manif=manif, date_revocation__isnull=True)
            if organisation_object in [acces.organisation for acces in acces_old]:
                return HttpResponse("Un accès en lecture existe déjà pour ce service.")
            else:
                acces.save()
            titre = "Accès en lecture"
            message = f"Un accès en lecture {self.request.POST.get('acces')} vient de vous être octroyer pour le dossier {manif}."
            destinataires = acces.organisation.get_utilisateurs_avec_service()
            Message.objects.creer_et_envoyer('info_suivi', [self.request.user, self.request.organisation], destinataires, titre, message,
                                             manifestation_liee=acces.manif, objet_lie_nature="dossier")
            Message.objects.creer_et_envoyer('tracabilite', None, [self.request.user, self.request.organisation], 'Accès en lecture',
                                             f'Accès en lecture {self.request.GET.get("acces")} créé pour le dossier {manif}')
        return HttpResponse('True')
