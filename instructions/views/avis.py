# coding: utf-8

from django.utils import timezone
from django.contrib import messages
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import UpdateView
from django.conf import settings

from administrative_division.models.region import Region
from instructions.decorators import verifier_service_avis, verifier_secteur_instruction, verifier_secteur_instruction_function
from core.util.permissions import require_type_organisation
from core.models.instance import Instance
from ..forms.date_limite_reponse import DateLimiteForm
from ..models import Avis, PieceJointeAvis
from ..forms.avis import (AvisInterrogerForm, AvisAdresserForm, AvisMandaterForm,
                          PreAvisRedigerForm, AvisRedigerForm)
from messagerie.models import Message
from structure.models import ServiceConsulte, Organisation, ServiceInstructeur, Federation, PresentationCDSR


@method_decorator(verifier_service_avis(acces=True), name='dispatch')
class AvisDetail(DetailView):
    """
    Vue de détail d'un avis
    La vue est protégée par le décorateur "verifier_service_avis" qui intervient dans la méthode "dispatch"
    """

    # Configuration
    model = Avis

    def dispatch(self, request, *args, **kwargs):
        avis = Avis.objects.get(pk=kwargs['pk'])
        if avis.avis_origine:
            return redirect(reverse('instructions:avis_detail', kwargs={"pk": avis.avis_origine.pk}))
        return super(AvisDetail, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        avis = super(AvisDetail, self).get_object(queryset)
        return avis.get_last_avis()

    # Overrides
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        manif = self.object.instruction.manif
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        is_preavi = True if self.object.avis_parent_fk else False
        context['str_avis'] = {"avis": "avis", "de_l": "de l'avis", "d": "d'avis", "l": "l'avis"} if not is_preavi else {"avis": "préavis", "de_l": "du préavis", "d": "de préavis", "l": "le préavis"}
        if self.object.acces.filter(organisation=organisation, date_revocation__isnull=True).exists():
            acces = self.object.acces.filter(organisation=organisation, date_revocation__isnull=True).first()
            acces.date_derniere_consult = timezone.now()
            acces.save()
            context['consult'] = acces
        else:
            context['consult'] = None
        if organisation.service_parent_fk == None:
            context['acces_consult'] = [acces for acces in manif.acces.filter(date_revocation__isnull=True) if
                                        acces.get_categorie() == self.object.get_categorie()]
        else:
            context['acces_consult'] = manif.acces.filter(createur=self.request.user, date_revocation__isnull=True)
        if self.object.instruction.presentations.all():
            presentation = self.object.instruction.presentations.last()
            if presentation.reunion_cdsr_fk and self.object.service_consulte_fk in presentation.get_tous_membres():
                membre_id = self.object.service_consulte_fk.pk
                perm_json = presentation.reunion_cdsr_fk.permanents_json
                invit_json = presentation.invites_json
                if any([True if item['membre'] == membre_id else False for item in perm_json]):
                    context['confirmation_cdsr'] = [item['confirmé'] for item in perm_json if item['membre'] == membre_id][0]
                    if presentation.reunion_cdsr_fk.convocations_envoyes:
                        context['presentation'] = presentation
                if any([True if item['membre'] == membre_id else False for item in invit_json]):
                    context['confirmation_cdsr'] = [item['confirmé'] for item in invit_json if item['membre'] == membre_id][0]
                    if presentation.convocations_envoyes:
                        context['presentation'] = presentation
        cerfa = manif.get_cerfa()
        context['confirmation'] = ""
        if type(organisation) == Federation:
            if hasattr(cerfa, "avec_convention_fede") and cerfa.avec_convention_fede and cerfa.avec_convention_fede_confirme is None:
                context['confirmation'] = "convention"
            elif manif.dans_calendrier and manif.dans_calendrier_confirme is None:
                context['confirmation'] = "calendrier"
        context['type'] = manif.get_type_manif()
        context['sans_avis_fede'] = manif.get_context_fede()
        context['last_action'] = manif.message_manifestation.filter(type='action').last()
        if not manif.date_depassee():
            context['rendu_message'] = bool(self.object.get_etat() == 'rendu')
            # Si l'avis n'est pas encore rendu
            context['historique'] = bool(not organisation == self.object.get_last_avis().service_consulte_fk)
            if not context['rendu_message'] and self.object.get_last_avis().service_consulte_fk == organisation:
                context['rendre_avis'] = bool(organisation.autorise_rendre_avis_b and not self.object.get_last_avis().avis_parent_fk and
                                              not context['confirmation'] and not is_preavi)
                context['adresser_avis'] = bool(organisation.interaction_entrante.filter(type_lien_s="adresser") and not is_preavi)
                context['mandater_avis'] = bool((organisation.autorise_mandater_avis_famille_b or
                                                organisation.interaction_entrante.filter(type_lien_s="mandater"))
                                                and not is_preavi)
                context['interroger_avis'] = bool(organisation.autorise_interroge_preavis_famille_b or
                                                  organisation.interaction_entrante.filter(type_lien_s="interroger"))
                context['rendre_preavis'] = bool(self.object.get_last_avis().avis_parent_fk)
                context['notifier'] = bool(organisation.type_service_fk.categorie_fk.nom_s in ['Sécurité', "Secours"] and self.object.get_last_avis().avis_parent_fk)
                context["preavis_envoyes"] = self.object.avis_enfant.all()
                context["tous_preavis_rendu"] = bool(context["preavis_envoyes"]
                                                     and self.object.avis_enfant.filter(etat="rendu").count() == self.object.avis_enfant.all().count())
        else:
            context['depasse'] = True
        # Afficher l'instructeur
        if manif.en_cours_instruction() and manif.get_instruction().referent:
            context['referent'] = manif.get_instruction().referent.get_full_name()
        else:
            context['referent'] = ''

        manif = self.object.instruction.manif.get_cerfa()
        if not manif.dossier_complet():
            if hasattr(manif, 'n2kevaluation'):
                if not manif.n2kevaluation.formulaire_n2k_complet():
                    context['n2k_manquant'] = True
            if manif.afficher_panneau_eval_n2000():
                context['n2k_manquant'] = True
            if hasattr(manif, 'rnrevaluation'):
                if not manif.rnrevaluation.formulaire_rnr_complet():
                    context['rnr_manquant'] = True
            if not manif.formulaire_complet():
                context['formulaire_manquant'] = True
            if not manif.carto_complet():
                context['carto_manquant'] = True
            if manif.liste_manquants():
                context['manquants'] = True
        context["instances"] = Instance.objects.configured()
        context["regions"] = Region.objects.all()
        context['service'] = organisation
        context['is_preavi'] = is_preavi
        # Gérer l'affichage de l'alerte "Traitement pas terminé"
        context['alerte_avis_pas_termine'] = True if (
                organisation == self.object.service_consulte_fk and
                (self.object.etat == "brouillon" or self.object.etat == "distribuée")) else False
        return context


@method_decorator(verifier_service_avis(acces=False), name='dispatch')
class AvisMandater(UpdateView):
    """ Vue pour mandater l'instruction d'un avis """

    # Configuration
    model = Avis
    form_class = AvisMandaterForm
    template_name = 'instructions/service_concerne_form.html'

    # Overrides
    def dispatch(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        avis = Avis.objects.get(pk=kwargs['pk'])
        if (not organisation.autorise_mandater_avis_famille_b and
                not organisation.interaction_entrante.filter(type_lien_s="mandater")) or avis.avis_parent_fk:
            return render(request, 'core/access_restricted.html', status=403)
        return super(AvisMandater, self).dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        """ Renvoyer le formulaire """
        form = super().get_form(form_class=form_class)
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        services_interroge = ServiceConsulte.objects.none()
        if organisation.autorise_mandater_avis_famille_b:
            services_interroge = organisation.get_family_member_qs().distinct()
        services_adresses = services_interroge | ServiceConsulte.objects.filter(interaction_sortante__service_entrant_fk=organisation,
                                                                                interaction_sortante__type_lien_s='mandater').distinct()
        form.fields['service_consulte_fk'].queryset = services_adresses
        return form

    def form_valid(self, form):
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        service = form.cleaned_data['service_consulte_fk']
        form.instance.agent = None  # le but est de faire en sorte de mettre l'avis dans la colonne à traiter du tableau de bord
        form.instance.mandater_avis(self.request.user, organisation, service)
        return super().form_valid(form)


@method_decorator(verifier_service_avis(acces=False), name='dispatch')
class AvisInterroger(UpdateView):
    """ Vue pour interroger un service consulté (préavis) """

    # Configuration
    model = Avis
    form_class = AvisInterrogerForm
    template_name = 'instructions/service_concerne_form.html'

    # Overrides
    def dispatch(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        if not organisation.autorise_interroge_preavis_famille_b and not organisation.interaction_entrante.filter(
                type_lien_s="interroger"):
            return render(request, 'core/access_restricted.html', status=403)
        return super(AvisInterroger, self).dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        """ Renvoyer le formulaire """
        form = super().get_form(form_class=form_class)
        # Filtrer les choix à ceux possibles selon la manifestation de l'avis
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        services_interroge = ServiceConsulte.objects.none()
        if organisation.autorise_interroge_preavis_famille_b:
            services_interroge = organisation.get_children_qs()
        services_interroge = services_interroge | ServiceConsulte.objects.filter(interaction_sortante__service_entrant_fk=organisation,
                                                                                 interaction_sortante__type_lien_s='interroger')
        form.fields['services_concernes'].queryset = services_interroge
        return form

    def form_valid(self, form):
        """ Le formulaire est valide, gérer la réponse HTTP """

        # Création des préavis
        for service in form.cleaned_data['services_concernes']:
            if not Avis.objects.filter(service_consulte_fk=service,
                                       service_demandeur_fk=self.object.service_consulte_fk,
                                       avis_parent_fk=self.object,
                                       instruction=self.object.instruction).exists():
                historique_circuit_json = [{"entrant": self.object.service_consulte_fk.pk,
                                            "sortant": service.pk,
                                            "type": "interroger",
                                            "date": str(timezone.now())}]
                preavis = Avis.objects.create(service_consulte_fk=service, service_consulte_origine_fk=service,
                                              service_demandeur_fk=self.object.service_consulte_fk,
                                              avis_parent_fk=self.object, historique_circuit_json=historique_circuit_json,
                                              instruction=self.object.instruction)
                # preavis.set_date_limite_reponse()
                # preavis.save()
                preavis.notifier_creation_avis(origine=[self.request.user, self.request.organisation],
                                               agents=preavis.get_agents_avec_service())
            else:
                pass
        form.instance.agent = self.request.user
        form.instance.log_distribution(self.request.user)
        return super().form_valid(form)


@method_decorator(verifier_service_avis(acces=False), name='dispatch')
class AvisAdresser(UpdateView):
    """
    Vue de soumission de l'avis
    Accès filtré par le décorateur aux seuls cgagent et edsragent
    """

    # Configuration
    model = Avis
    form_class = AvisAdresserForm
    template_name = 'instructions/service_concerne_form.html'

    # Overrides
    def dispatch(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        if not organisation.interaction_entrante.filter(type_lien_s="adresser"):
            return render(request, 'core/access_restricted.html', status=403)
        return super(AvisAdresser, self).dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        form = super().get_form(form_class=form_class)
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        services_adresses = ServiceConsulte.objects.filter(interaction_sortante__service_entrant_fk=organisation, interaction_sortante__type_lien_s='adresser')
        form.fields['services_concerne'].queryset = services_adresses
        return form

    def form_valid(self, form):
        """ Le formulaire est valide, gérer la réponse HTTP """
        user = self.request.user
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        form.instance.adresserAvis(user, organisation, form.cleaned_data['services_concerne'])
        return super().form_valid(form)


@method_decorator(verifier_service_avis(acces=False), name='dispatch')
class AvisRediger(UpdateView):
    """ Vue de redaction d'avis """

    # Configuration
    model = Avis
    form_class = AvisRedigerForm

    # # Overrides
    # def dispatch(self, request, *args, **kwargs):
    #     organisation = Organisation.objects.get(pk=request.session['service_pk'])
    #     avis = Avis.objects.get(pk=kwargs['pk'])
    #     if not (organisation.autorise_rendre_avis_b and not avis.avis_parent_fk) and not \
    #             (avis.avis_parent_fk):
    #         return render(request, 'core/access_restricted.html', status=403)
    #     return super(AvisRediger, self).dispatch(request, *args, **kwargs)

    def get_form_class(self):
        if self.object.avis_parent_fk:
            self.form_class = PreAvisRedigerForm
        return super(AvisRediger, self).get_form_class()

    def form_valid(self, form):
        """ Le formulaire est valide, gérer la réponse HTTP """
        if form.instance.etat == 'rendu':
            messages.error(self.request, "Aucune action effectuée : cet avis a déjà été validé.")
            return HttpResponseRedirect(self.get_success_url())
        else:
            form.instance.agent = self.request.user
            form.instance.etat = "brouillon"
            return super().form_valid(form)

    def get_initial(self):
        """ Renvoyer les valeurs de base du formulaire """
        initial = super().get_initial()
        initial['favorable'] = True
        return initial

    def get_context_data(self, **kwargs):
        if self.object.avis_parent_fk:
            preavis = True
        else:
            preavis = False
        context = super().get_context_data(**kwargs)
        if self.object.is_avis_cdsr():
            instruction = self.object.presentation.instruction_fk
            liste_perm = self.object.presentation.reunion_cdsr_fk.get_membres_permanents()
            for service in liste_perm:
                if Avis.objects.filter(instruction=instruction, service_consulte_fk=service).exists():
                    avis = Avis.objects.filter(instruction=instruction, service_consulte_fk=service).order_by('pk').last()
                    if avis.etat == "rendu":
                        context['remplir_avis'] = True
                        break
        else:
            context['remplir_preavis'] = bool(self.object.get_nb_preavis() != 0)
        context['preavis'] = preavis
        return context

    def get_success_url(self):
        if self.object.service_consulte_fk.is_service_cdsr():
            return reverse('instructions:instruction_detail', kwargs={'pk': self.object.instruction.pk})
        return super().get_success_url()


@method_decorator(verifier_service_avis(acces=False), name='dispatch')
class AvisRendre(View):
    """ Vue de rendu d'avis """

    # Overrides
    def dispatch(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        avis = Avis.objects.get(pk=kwargs['pk'])
        if not (organisation.autorise_rendre_avis_b and not avis.avis_parent_fk) and not avis.avis_parent_fk:
            return render(request, 'core/access_restricted.html', status=403)
        return super(AvisRendre, self).dispatch(request, *args, **kwargs)

    def get(self, request, pk, initiateur=False, *args, **kwargs):
        avis = Avis.objects.get(pk=pk)
        if avis.etat == 'rendu':
            messages.error(self.request, "Aucune action effectuée : cet avis a déjà été validé.")
            return redirect('instructions:avis_detail',  avis.pk)
        else:
            if initiateur == "True" and avis.avis_parent_fk:
                avis_parent = avis.avis_parent_fk
                while avis_parent.avis_parent_fk:
                    avis_parent = avis.avis_parent_fk
                avis.avis_parent_fk = avis_parent
            avis.rendreAvis(request.user)
            avis.date_reponse = timezone.now().date()
            avis.save()
            messages.success(self.request, "Avis rendu")
            if avis.service_consulte_fk.is_service_cdsr():
                return redirect('instructions:instruction_detail', avis.instruction.pk)
            return redirect('instructions:avis_detail', avis.pk)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class AvisResend(SingleObjectMixin, View):
    """ Vue de renvoi des demandes d'avis """

    # Configuration
    model = Avis

    def dispatch(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        avis = Avis.objects.get(pk=kwargs['pk'])
        if organisation.is_service_instructeur():
            if not verifier_secteur_instruction_function(request, *args, **kwargs):
                return super(AvisResend, self).dispatch(request, *args, **kwargs)
        elif organisation == avis.service_demandeur_fk:
            return super(AvisResend, self).dispatch(request, *args, **kwargs)
        return render(request, 'core/access_restricted.html', status=403)

    def get(self, request, *args, **kwargs):
        """ Vue pour la méthode GET """
        instance = self.get_object()
        instance.notifier_creation_avis(origine=[request.user, request.organisation],
                                        agents=instance.get_agents_avec_service())
        messages.success(request, "Demande d'avis relancée avec succès")
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        if organisation.is_service_instructeur():
            return redirect(reverse('instructions:instruction_detail', kwargs={'pk': instance.instruction.id}))
        elif organisation.is_service_consulte():
            return redirect(reverse('instructions:avis_detail', kwargs={'pk': instance.avis_parent_fk.pk}))


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class RedemandeAvis(SingleObjectMixin, View):
    """View pour redemander un avis"""
    # Configuration
    model = Avis

    def dispatch(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        avis = Avis.objects.get(pk=request.GET['pk_avis'])
        if organisation.is_service_instructeur():
            if verifier_secteur_instruction_function(request, *args, **kwargs):
                return verifier_secteur_instruction_function(request, *args, **kwargs)
            else:
                return super(RedemandeAvis, self).dispatch(request, *args, **kwargs)
        # Pour les avis aux fédérations délégataires -> accepter si l'organisation est un service instructeur
        # Pour les autres accepter si l'organisation est le service demandeur
        elif (organisation == avis.service_demandeur_fk or request.session['o_support'] or
                organisation.is_service_instructeur() and
                avis.service_consulte_origine_fk.type_service_fk.categorie_fk.pk == 12):
            return super(RedemandeAvis, self).dispatch(request, *args, **kwargs)
        return render(request, 'core/access_restricted.html', status=403)

    def get(self, request, pk):
        avis = Avis.objects.get(pk=request.GET['pk_avis'])
        if avis and avis.get_etat() == "rendu":
            context = {
                "manif": avis.instruction.manif,
                "is_preavis": True if avis.avis_parent_fk else False,
            }
            return render(request, 'instructions/redemande_avis_preavis.html', context=context)
        return redirect(reverse('instructions:instruction_detail', kwargs={'pk': pk}))

    def post(self, request, pk):
        raison = request.POST.get('raison')
        avis = Avis.objects.get(pk=request.GET['pk_avis'])
        instruction = avis.instruction
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        if avis and avis.get_etat() == "rendu":
            if avis.service_consulte_fk.is_service_cdsr():
                historique_circuit_json = [{"entrant": organisation.pk,
                                            "sortant": avis.service_consulte_origine_fk.pk,
                                            "type": "redemande",
                                            "date": str(timezone.now())}]
                reavis = Avis.objects.create(instruction=instruction, service_consulte_fk=avis.service_consulte_origine_fk,
                                             service_consulte_origine_fk=avis.service_consulte_origine_fk, service_demandeur_fk=organisation,
                                             historique_circuit_json=historique_circuit_json)
                reavis.notifier_creation_avis(origine=[request.user, request.organisation], agents=avis.service_consulte_origine_fk.get_users_list())
                origine = avis.presentation.presentation_origine_fk if avis.presentation.presentation_origine_fk else avis.presentation
                PresentationCDSR.objects.create(
                    instruction_fk=instruction, avis_o2o=reavis, invites_json=avis.presentation.invites_json,
                    presentation_origine_fk=origine, raison_nouvelle_presentation=raison)
            else:
                historique_circuit_json = [
                    {"entrant": organisation.pk, "sortant": avis.service_consulte_origine_fk.pk,
                     "type": "redemande"}]
                origine = avis.avis_origine if avis.avis_origine else avis
                avis_relance = Avis(service_consulte_fk=avis.service_consulte_origine_fk, service_demandeur_fk=organisation,
                                    instruction=avis.instruction, service_consulte_origine_fk=avis.service_consulte_origine_fk,
                                    avis_origine=origine, raison_redemande=raison, avis_parent_fk=avis.avis_parent_fk,
                                    historique_circuit_json=historique_circuit_json)
                avis_relance.save()
                titre = "Nouvelle demande d'avis à traiter"
                message = f"{titre} pour la manifestation {avis.instruction.manif.nom}. Cette dernière remplace la demande d'avis précédente."
                Message.objects.creer_et_envoyer('action', [request.user, request.organisation],
                                                 avis.service_consulte_origine_fk.get_utilisateurs_avec_service(),
                                                 titre, message,  manifestation_liee=avis.instruction.manif,
                                                 objet_lie_nature="avis", objet_lie_pk=avis_relance.pk)
                messages.success(self.request, "La demande d'avis a été redemandé.")
        if organisation.is_service_instructeur() or (request.session['o_support'] and not avis.avis_parent_fk):
            return redirect(reverse('instructions:instruction_detail', kwargs={'pk': instruction.pk}))
        else:
            return redirect(reverse("instructions:avis_detail", kwargs={'pk': avis.avis_parent_fk.pk}))


@method_decorator(verifier_service_avis(acces=False), name='dispatch')
class AvisGetAvisAjax(SingleObjectMixin, View):
    """
    Renvoyer la synthèse des avis des membres permanents CDSR pour pré-remplir la prescription de l'avis CDSR
    """
    # Configuration
    model = Avis

    # Ajouter une pièce jointe
    def get(self, request, *args, **kwargs):
        avis_cdsr = self.get_object()
        instruction = avis_cdsr.presentation.instruction_fk
        liste_perm = avis_cdsr.presentation.reunion_cdsr_fk.get_membres_permanents()
        liste_avis = []
        for service in liste_perm:
            if Avis.objects.filter(instruction=instruction, service_consulte_origine_fk=service).exists():
                avis = Avis.objects.filter(instruction=instruction, service_consulte_origine_fk=service).last()
                avis = avis.get_last_avis()
                if avis.etat == "rendu":
                    liste_avis.append(avis)
        return render(request, "instructions/avis_prescription.html", {'liste_avis': liste_avis, 'avis_cdsr': avis_cdsr})


@method_decorator(verifier_service_avis(acces=False), name='dispatch')
class AvisGetPreavisAjax(SingleObjectMixin, View):
    """
    Renvoyer la synthèse des préavis pour pré-remplir la prescription de l'avis
    """
    # Configuration
    model = Avis

    def get(self, request, *args, **kwargs):
        avis = self.get_object()
        liste_preavis = avis.avis_enfant.filter(etat="rendu")
        return render(request, "instructions/avis_prescription.html", {'liste_preavis': liste_preavis})


@method_decorator(verifier_service_avis(acces=True), name='dispatch')
class AvisAddFile(SingleObjectMixin, View):

    """ class d'ajout des avis """

    # Configuration
    model = Avis

    # Ajouter une pièce jointe
    def post(self, request, *args, **kwargs):
        avis = self.get_object()
        if 'fichier' in request.FILES and request.FILES['fichier']:
            from core.FileTypevalidator import file_type_valide_non_form
            if file_type_valide_non_form(request.FILES['fichier']):
                scan = False if settings.CELERY_ENABLED else True
                pj = PieceJointeAvis.objects.create(
                    fichier=request.FILES['fichier'], avis=avis, date_depot=timezone.now(), scan_antivirus=scan)

            else:
                return render(request, "core/access_restricted.html",
                              {'message': "Données POST incorrectes !"},
                              status=403)
            if avis.etat == 'rendu':
                origine = [request.user, request.organisation]
                avis.notifier_ajout_pj(origine, pj.fichier.name)
            if avis.service_consulte_fk.is_service_cdsr():
                return redirect('instructions:instruction_detail', avis.instruction.pk)
            return redirect('instructions:avis_detail', pk=avis.pk)
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Données POST incorrectes !"},
                          status=403)

    # Transférer une pièce jointe sur un avis
    def get(self, request, *args, **kwargs):
        preavis = Avis.objects.get(id=kwargs.get('pk', ''))
        avis = preavis.avis_parent_fk
        if 'doc' in request.GET and request.GET['doc']:
            pj_origine = PieceJointeAvis.objects.get(id=request.GET['doc'])
            pj = PieceJointeAvis.objects.create(
                fichier=pj_origine.fichier, avis=avis, date_depot=timezone.now(), scan_antivirus=True)
            if avis.etat == 'rendu':
                origine = [request.user, request.organisation]
                avis.notifier_ajout_pj(origine, pj.fichier.name)
            return redirect('instructions:avis_detail', pk=avis.pk)
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Données GET incorrectes !"},
                          status=403)


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class AvisPrefAjoutPJ(SingleObjectMixin, View):
    """ class d'ajout des avis """

    # Configuration
    model = Avis

    # Transférer une pièce jointe sur l'avis préfecture
    def get(self, request, *args, **kwargs):
        avis = Avis.objects.get(id=kwargs.get('pk', ''))
        instruction = avis.instruction
        avis_pref = instruction.get_avis_prefecture()
        if 'doc' in request.GET and request.GET['doc']:
            pj = PieceJointeAvis.objects.get(id=request.GET['doc'])
            pj.avis = avis_pref
            pj.save()
            titre = f"Pièce jointe ajoutée par la Préfecture {str(instruction.instance.departement)}"
            message = f"Pièce jointe ajoutée à son avis par la Préfecture du département {str(instruction.instance.departement)}."
            instruction.referent = request.user
            instruction.save()
            if instruction.instruction_principale.referent:
                destinataires = [instruction.instruction_principale.referent, instruction.instruction_principale.get_prefecture_concernee()]
            else:
                destinataires = instruction.instruction_principale.get_instructeurs_avec_services()
            Message.objects.creer_et_envoyer('action', [request.user, request.organisation], destinataires, titre, message,
                                             manifestation_liee=instruction.manif)
            return redirect('instructions:instruction_detail', pk=instruction.pk)
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Données GET incorrectes !"},
                          status=403)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class AvisRemove(SingleObjectMixin, View):
    """ Vue d'annulation d'une demande d'avis """

    # Configuration
    model = Avis

    def dispatch(self, request, *args, **kwargs):
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        avis = Avis.objects.get(pk=kwargs['pk'])
        if organisation.is_service_instructeur():
            if verifier_secteur_instruction_function(request, *args, **kwargs):
                return verifier_secteur_instruction_function(request, *args, **kwargs)
            else:
                return super(AvisRemove, self).dispatch(request, *args, **kwargs)
        elif organisation == avis.service_demandeur_fk:
            return super(AvisRemove, self).dispatch(request, *args, **kwargs)
        return render(request, 'core/access_restricted.html', status=403)

    # Overrides
    def get(self, request, *args, **kwargs):
        avis = self.get_object()
        instruction = avis.instruction
        if avis.etat == 'demandé':

            avis.notifier_suppression_avis(origine=[request.user, request.organisation],
                                           agents=avis.service_consulte_fk.get_users_list())

            avis.delete()
            if not instruction.avis.all():
                instruction.etat = 'demandée'
            instruction.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Vous ne pouvez plus supprimer cet avis. Il n'est plus dans l'état initial !"},
                          status=403)


@method_decorator(verifier_service_avis(acces=False), name='dispatch')
class AvisRedemande(View):

    def get(self, request, pk):
        avis = get_object_or_404(Avis, pk=pk)
        context = {
            "avis": avis,
            "is_preavis": True if avis.avis_parent_fk else False,
            "manif": avis.instruction.manif
        }
        return render(request, 'instructions/redemande_avis_preavis.html', context=context)

    def post(self, request, pk):
        avis = get_object_or_404(Avis, pk=pk)
        if not avis.get_etat() == "rendu":
            # l'avis est en cours il n'est pas redemandable
            return redirect(reverse('instructions:avis_detail', kwargs={"pk": avis.pk}))
        origine = avis.avis_origine if avis.avis_origine else avis
        raison = request.POST.get('raison')
        historique_circuit_json = [{"entrant": avis.service_consulte_origine_fk.pk,
                                    "sortant": avis.service_demandeur_fk.pk,
                                    "type": "relance",
                                    "date": str(timezone.now())}]
        avis_relance = Avis(service_consulte_fk=avis.service_consulte_origine_fk, instruction=avis.instruction,
                            avis_origine=origine, raison_redemande=raison, service_demandeur_fk=avis.service_demandeur_fk,
                            service_consulte_origine_fk=avis.service_consulte_origine_fk, avis_parent_fk=avis.avis_parent_fk,
                            historique_circuit_json=historique_circuit_json)
        avis_relance.save()
        if avis.avis_parent_fk:
            destinataires = avis.service_demandeur_fk.get_utilisateurs_avec_service()
        else:
            destinataires = [avis.instruction.referent, avis.instruction.get_prefecture_concernee()]
        titre = "Nouvel avis initié par le service consulté"
        message = f"Le service consulté a créé et s'apprête à rendre un nouvel avis." \
                  f"Vous pouvez suivre son avancement dans le détail de l'instruction"
        Message.objects.creer_et_envoyer('info_suivi', [request.user, request.organisation], destinataires,
                                         titre, message, manifestation_liee=avis.instruction.manif,
                                         objet_lie_nature="avis", objet_lie_pk=avis_relance.pk)
        return redirect(reverse('instructions:avis_detail', kwargs={"pk": avis.pk}))


@method_decorator(verifier_service_avis(acces=False), name='dispatch')
class AvisConfirme(View):

    def get(self, request, pk):
        avis = get_object_or_404(Avis, pk=pk)
        if avis.etat == 'rendu':
            messages.error(request, "Aucune action effectuée : cet avis a déjà été validé.")
            return redirect(reverse('instructions:avis_detail', kwargs={"pk": avis.pk}))
        manif = avis.instruction.manif.get_cerfa()
        attrib = request.GET.get('obj')
        confirme = request.GET.get('confirme')
        prescription = ""
        if attrib == "calendrier" and manif.dans_calendrier:
            if confirme:
                manif.dans_calendrier_confirme = True
                prescription = "Non concerné : l'inscription dans le calendrier de la fédération est confirmée"
            else:
                manif.dans_calendrier_confirme = False
            manif.save()
        if attrib == "convention" and manif.avec_convention_fede:
            if confirme:
                manif.avec_convention_fede_confirme = True
                prescription = "Non concerné : la convention avec la fédération est confirmée"
            else:
                manif.avec_convention_fede_confirme = False
            manif.save()
        if prescription:
            avis.favorable = True
            avis.prescriptions = prescription
            avis.date_reponse = timezone.now().date()
            avis.agent = request.user
            avis.rendreAvis(request.user)
            avis.historique_circuit_json = avis.historique_circuit_json + [{"entrant": avis.service_consulte_fk.pk,
                                                                            "sortant": avis.service_demandeur_fk.pk,
                                                                            "type": "rendre",
                                                                            "date": str(timezone.now())}]
            avis.save()
        return redirect(reverse('instructions:avis_detail', kwargs={"pk": avis.pk}))


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class DateLimiteReponse(UpdateView):

    model = Avis
    form_class = DateLimiteForm
    template_name = 'instructions/date_reponse_form.html'

    def dispatch(self, request, *args, **kwargs):
        avis = Avis.objects.get(pk=kwargs['pk'])
        if request.organisation.is_service_instructeur():
            if not verifier_secteur_instruction_function(request, *args, **kwargs):
                return super(DateLimiteReponse, self).dispatch(request, *args, **kwargs)
        elif request.organisation == avis.service_demandeur_fk:
            return super(DateLimiteReponse, self).dispatch(request, *args, **kwargs)
        return render(request, 'core/access_restricted.html', status=403)

    def post(self, request, *args, **kwargs):
        avis = get_object_or_404(Avis, pk=kwargs['pk'])
        form = DateLimiteForm(request.POST, instance=avis)
        if form.is_valid():
            form.save()
            return HttpResponse()
        else:
            erreur = form.errors['date_limite_reponse_dt'][0]
            return HttpResponse(erreur, status=400)
