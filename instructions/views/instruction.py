# coding: utf-8
import logging
import json
import html

from smtplib import SMTPRecipientsRefused, SMTPException
from post_office import mail
from django.contrib import messages
from django.db.models import Q
from django.conf import settings
from django.http.response import HttpResponseRedirect, HttpResponse
from django import forms
from django.urls import reverse
from django.shortcuts import get_object_or_404, render, redirect
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, UpdateView

from administrative_division.models.region import Region
from core.models.user import User
from core.models import Instance
from core.util.permissions import require_type_organisation
from core.tasks import creation_thumbnail_pj_avis
from core.FileTypevalidator import file_type_valide_non_form
from instructions.decorators import verifier_secteur_instruction
from evenements.models import Manif, AbstractInstructionConfig, PieceJointe
from instructions.forms.instruction import (InstructionForm, InstructionPublishForm,
                                            InstructionPublierForm, InstructionAvisForm)
from instructions.models.instruction import Instruction, DocumentOfficiel
from instructions.models.avis import Avis, PieceJointeAvis
from messagerie.models import Message
from structure.models import (Organisation, StructureOrganisatrice,
                              ServiceInstructeur, ServiceConsulte, Federation,
                              ServiceCDSR, ReunionCDSR)

@method_decorator(require_type_organisation([StructureOrganisatrice, ServiceInstructeur, ServiceConsulte, Federation]), name='dispatch')
class AssignationDossier(View):
    """
    Avant toute action un dossier (instruction ou avis) doit être assigné à une personne de l'organisation responsable.
    vue AJAX
    """

    def get(self, request, pk):
        type_dossier = request.GET.get("type_dossier")
        if not type_dossier:
            return HttpResponse()
        if type_dossier == "instruction":
            instruction = get_object_or_404(Instruction, pk=pk)
            tab = []
            for service in instruction.service_instructeur_m2m.all():
                tab += [[str(user.pk), str(user.get_full_name()), service.nom_s] for user in service.users.all()]
        else:  # avis
            avis = get_object_or_404(Avis, pk=pk)
            avis = avis.get_last_avis()
            service = avis.service_consulte_fk
            tab = [[str(user.pk), str(user.get_full_name()), service.nom_s] for user in service.users.all()]
        return HttpResponse(json.dumps(tab))

    def post(self, request, pk):
        type_dossier = request.GET.get("type_dossier")
        pk_user = request.GET.get('pk_user')
        if not type_dossier or not pk_user:
            return HttpResponse()
        if type_dossier == "instruction":
            instruction = get_object_or_404(Instruction, pk=pk)
            user = get_object_or_404(User, pk=pk_user)
            instruction.referent = user
            instruction.save()
        else:
            avis = get_object_or_404(Avis, pk=pk)
            avis = avis.get_last_avis()
            user = get_object_or_404(User, pk=pk_user)
            avis.agent = user
            avis.save()
        return HttpResponse('ok')


class InstructionCreateView(CreateView):

    # Configuration
    model = Instruction
    form_class = InstructionForm

    # Overrides
    @method_decorator(require_type_organisation([StructureOrganisatrice,]))
    def dispatch(self, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        self.manif = manif
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if Instruction.objects.filter(manif=manif).exists():
            return redirect('evenements:manif_detail', pk=manif.pk)
        if manif.structure_organisatrice_fk == organisation:
            return super().dispatch(*args, **kwargs)
        return render(self.request, 'core/access_restricted.html', status=403)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        return form

    def form_valid(self, form):

        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        manif.gestion_instance_instruites()
        manif.declarant = self.request.user
        manif.save()
        # si le dossier est imcomplet on redirige vers une mage d'erreur
        if manif.etape_en_cours() != "etape 0" and not manif.autorisation_hors_delai_json:
            return render(self.request, '400.html', context={"message": "Dossier hors délai réglementaire"}, status=403)
        if not manif.cerfa.dossier_complet():
            return render(self.request, '400.html', context={"message": "Dossier incomplet"}, status=403)
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if Instruction.objects.filter(manif=manif).exists():
            return redirect(self.get_success_url())
        # Sinon enregister la nouvelle déclaration
        form.instance.manif = manif
        form.instance.instance = manif.instance
        response = super().form_valid(form)
        form.instance.refresh_from_db()
        form.instance.set_service_instructeurs()
        form.instance.envoi_avis_fede()
        form.instance.notifier_creation()
        # Enregistrer la date de soumission d'une évaluation N2000
        if hasattr(manif, 'n2kevaluation'):
            evaln2k = manif.n2kevaluation
            evaln2k.date_soumission = timezone.now()
            evaln2k.save()
        # Révoquer tous les accès en lecture des services intructeurs pour débuter l'instruction
        for acces in form.instance.manif.acces.filter(date_revocation__isnull=True):
            if type(acces.organisation) == ServiceInstructeur:
                acces.date_revocation = timezone.now()
                acces.save()
        # interdepartementalité
        if manif.instance_instruites.all().count() > 1:
            for instance in manif.instance_instruites.exclude(pk=manif.instance.pk):
                instruction = Instruction(manif=manif, instance=instance, instruction_principale=form.instance)
                instruction.save()
                instruction.set_service_instructeurs()
                instruction.refresh_from_db()
                instruction.notifier_creation()
                if not manif.get_type_manif() in ['avtm']:
                    instruction.envoi_avis_fede()
                else:
                    instruction.generer_avis_prefecture()
        if not manif.est_instructible_mairie():
            admin = User.objects.filter(default_instance=form.instance.instance).filter(groups__name="Administrateurs d'instance")
            cause = "pas de service instructeur."
            if Instruction.get_service_mairie_qs(manif).exists():
                service = Instruction.get_service_mairie_qs(manif).first()
                if not service.get_users_actif_qs() and service.get_users_qs():
                    cause = "Compte agent inactif."
                else:
                    cause = "Pas de compte agent créé."
                url = reverse('structure:administration_des_services') + f"?service_id={ str(service.pk) }"
                address = self.request.build_absolute_uri(url)
                if service.email_s:
                    cause += f"<br>L'adresse email de la mairie est : {service.email_s}"
                cause += f"<br>Veuillez prendre contact auprès d'eux.<br>Lien vers l'administration du service :<br><a href='{address}' target='_blank'>{address}</a>"
            contenu = "<p>Bonjour, <br>" \
                      f"Un dossier a été déposé auprès d'une mairie qui n'est pas en capacité de le traiter pour la raison suivante :<br>" \
                      f" {cause}"
            Message.objects.creer_et_envoyer('action', None, admin, "Instruction en mairie impossible", contenu, manifestation_liee=manif)

        # envoi de mail de confirmation
        subject = "Accusé de réception : "+manif.nom
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        data = {
            "date": timezone.now(),
            "structure": organisation,
            "user": self.request.user,
            "manif": manif,
            "instru": form.instance,
            "url": self.request.META['HTTP_HOST']+self.get_success_url(),
        }
        message = render_to_string('messagerie/mail/message_organisateur_envoi_manif.txt', data)
        recipient = self.request.user
        mail_logger = logging.getLogger('smtp')
        if hasattr(recipient, 'email'):
            try:
                sender_email = manif.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL  # utiliser le EMAIL_BACKEND
                mail.send(recipient.email, sender_email,  subject=html.unescape(subject), message=html.unescape(message), html_message=html.unescape(message))
                message = render_to_string('messagerie/mail/message_organisateur_envoi_manif_message.txt', data)
                Message.objects.creer_et_envoyer("info_suivi", None, [self.request.user, self.request.organisation],
                                                 "Accusé de réception (dépôt de dossier)", message, manifestation_liee=manif)
            except SMTPRecipientsRefused:
                # En cas d'échec où l'adresse n'existe pas (ou similaire), notifier les instructeurs du dossier
                # À noter que l'échec SMTP ne peut se produire qu'en situation de production
                # donc à surveiller.
                mail_logger.exception(
                    "notifications.notification.notify_and_mail: SMTP Recipients refused, trying to send to instructors")
            except SMTPException:
                # Si le serveur SMTP pose un autre type de problème, logger l'erreur
                exception_text = """
                notifications.notification.notify_and_mail: Une exception SMTP non gérée vient de se produire
                instance: {instance}
                destinataires: {recipients}
                manifestation: {manifestation}
                cible: {target}
                """
                mail_logger.exception(exception_text.format(instance=manif.get_instance(),
                                                            recipients=recipient, manifestation=manif,
                                                            target=manif.structure_organisatrice_fk))
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['creer'] = True
        poi_secours = 0
        poi_signaleur_permi = 0
        federation_html = ""

        if self.manif.get_type_manif() in ['avtm', 'dcnm', 'dcnmc']:
            for parcours in self.manif.parcours.all():
                for pois in parcours.pois.all():
                    if pois.properties['icon']['id'] == "2":  # poi secours
                        poi_secours += 1
        else:
            poi_secours = -1

        if self.manif.get_type_manif() in ['dcnm', 'dcnmc']:
            for parcours in self.manif.parcours.all():
                for pois in parcours.pois.all():
                    if pois.properties['icon']['id'] == "5":  # poi signaleur avec permis
                        poi_signaleur_permi += 1
        else:
            poi_signaleur_permi = -1

        if self.manif.get_cerfa().consultFederation:
            # Les cerfa qui ont "consultfederation" True ont aussi le champ "avec_convention_fede"
            fede = self.manif.get_federation()
            if not fede:
                federation_html = ('<div class="alert alert-danger" role="alert">'
                         'La plateforme ne peut pas transmettre votre dossier à la fédération délégataire.<br>'
                         'Veuillez leur transmettre manuellement votre dossier afin de respecter le code du sport.'
                         '</div>')
            else:
                if not self.manif.dans_calendrier and not self.manif.get_cerfa().avec_convention_fede:
                    federation_html = '<div><p>Une demande d\'avis va être automatiquement envoyée à '
                else:
                    federation_html = "<div><p>La plateforme va vérifier votre déclaration auprès de "
                federation_html += f"votre fédération délégataire "
                if fede.departement_m2m.first():
                    federation_html += "départementale :"
                elif fede.region_m2m.first():
                    federation_html += "régionale :"
                else:
                    federation_html += "nationale :"
                federation_html += f"<i class='ctx-help-federation_definie'></i><br><span class='ms-3 me-2'>&#8250;</span>{str(fede)}</span> </p></div>"

        context['poi_secours'] = poi_secours
        context['poi_signaleur_permi'] = poi_signaleur_permi
        context['federation'] = federation_html
        context['manif'] = self.manif
        liste_pjs = []
        liste = self.manif.get_cerfa().LISTE_FICHIERS_ETAPE_1 + self.manif.get_cerfa().LISTE_FICHIERS_ETAPE_2
        for fichier in liste:
            pjs = PieceJointe.objects.filter(manif=self.manif, champ_cerfa__icontains=fichier)
            [liste_pjs.append(pj) if pj else "" for pj in pjs]
        context['pjs'] = liste_pjs
        context['instructible_mairie'] = self.manif.est_instructible_mairie()
        return context

    def get_success_url(self):
        """ Renvoyer l'URL de retour lorsque le formulaire est valide """

        type_name = self.object.manif.get_type_manif()

        if type_name == 'dnm':
            return reverse('evenements:dnm_detail', kwargs={'pk': self.object.manif.pk})
        elif type_name == 'dnmc':
            return reverse('evenements:dnmc_detail', kwargs={'pk': self.object.manif.pk})
        elif type_name == 'dcnm':
            return reverse('evenements:dcnm_detail', kwargs={'pk': self.object.manif.pk})
        elif type_name == 'dcnmc':
            return reverse('evenements:dcnmc_detail', kwargs={'pk': self.object.manif.pk})
        elif type_name == 'dvtm':
            return reverse('evenements:dvtm_detail', kwargs={'pk': self.object.manif.pk})
        elif type_name == 'avtm':
            return reverse('evenements:avtm_detail', kwargs={'pk': self.object.manif.pk})
        elif type_name == 'dvtmcir':
            return reverse('evenements:dvtmcir_detail', kwargs={'pk': self.object.manif.pk})
        else:
            return reverse('portail:home_page')


@method_decorator(verifier_secteur_instruction(acces=True), name='dispatch')
class InstructionDetailView(DetailView):

    # Configuration
    model = Instruction

    # Overrides
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])

        if self.object.acces.filter(organisation=organisation, date_revocation__isnull=True).exists() and not self.object.manif in organisation.get_manifs_ecriture():
            acces = self.object.acces.get(organisation=organisation, date_revocation__isnull=True)
            acces.date_derniere_consult = timezone.now()
            acces.save()
            context['consult'] = acces
        else:
            context['consult'] = None
        liste_acces = self.object.manif.acces.filter(date_revocation__isnull=True)
        context['acces_consult'] = liste_acces
        context['user_is_instructeur'] = self.request.organisation.is_service_instructeur()   # test inutile car en principe, seuls les instructeurs peuvent passer dans cette view
        context['sans_avis_fede'] = self.object.manif.get_context_fede()
        context['last_action'] = self.object.manif.message_manifestation.filter(type='action').last()
        # Pour afficher l'instructeur
        if self.object.manif.en_cours_instruction() and self.object.referent:
            context['referent'] = self.object.referent.get_full_name()
        else:
            context['referent'] = ''
        if self.object.manif.get_type_manif() in ['avtm'] and self.object.instruction_principale:
            context['instruction_avis'] = True
        context['champ'] = json.dumps(self.object.manif.get_cerfa().get_liste_champs_a_modifier())


        avis = self.object.get_tous_avis_sans_cdsr()
        service_categories = []
        [service_categories.append(avi.service_consulte_fk.type_service_fk.categorie_fk) if avi.service_consulte_fk and avi.service_consulte_fk.type_service_fk.categorie_fk not in service_categories else "" for avi in avis]
        for acces in liste_acces:
            if acces.get_categorie() not in service_categories:
                service_categories.append(acces.get_categorie())

        if not self.object.referent:
            context['assigner'] = "aucun"
        elif not self.request.user == self.object.referent:
            context['assigner'] = "autre"
        else:
            context['assigner'] = False
        manif = self.object.manif.get_cerfa()
        if not manif.dossier_complet():
            if hasattr(manif, 'n2kevaluation'):
                if not manif.n2kevaluation.formulaire_n2k_complet() and self.object.instance == manif.instance:
                    context['n2k_manquant'] = True
            if manif.afficher_panneau_eval_n2000() and self.object.instance == manif.instance:
                context['n2k_manquant'] = True
            if hasattr(manif, 'rnrevaluation'):
                if not manif.rnrevaluation.formulaire_rnr_complet() and self.object.instance == manif.instance:
                    context['rnr_manquant'] = True
            if not manif.formulaire_complet():
                context['formulaire_manquant'] = True
            if not manif.carto_complet():
                context['carto_manquant'] = True
            if manif.liste_manquants():
                context['manquants'] = True
        context['dossier_incomplet'] = True if context.get('n2k_manquant') or context.get('rnr_manquant') or \
                                                context.get('formulaire_manquant') or context.get('carto_manquant') or \
                                                context.get('manquants') else False
        context["service_categories"] = service_categories
        context["instances"] = Instance.objects.configured().order_by('departement__name')
        cdsr = ServiceCDSR.objects.get(instance_fk=self.object.instance)
        if self.object.instance.activer_beta:
            if Avis.objects.filter(service_consulte_fk=cdsr).filter(instruction=self.object).exists():
                presenter_cdsr = False
            elif self.object.etat in ["autorisée", "annulée"]:
                presenter_cdsr = False
            else:
                presenter_cdsr = True
        else:
            presenter_cdsr = False
        cdsr_avenir = ReunionCDSR.objects.filter(date__gt=timezone.now().date(), date__lt=manif.date_debut.date())
        cdsr_adefinir = ReunionCDSR.objects.filter(date__isnull=True)
        liste_cdsr = cdsr_avenir | cdsr_adefinir
        context['cdsr'] = cdsr
        context['presenter_cdsr'] = presenter_cdsr
        context['liste_cdsr'] = liste_cdsr
        context["regions"] = Region.objects.all()
        context['service'] = organisation
        context['acces_pref'] = any([True if acces.get_categorie() == "Service préfectoral" else False for acces in liste_acces if acces.get_categorie() in ["prefecture", "structure"]])
        context['acces_struc'] = any([True if acces.get_categorie() == "structure" else False for acces in liste_acces if acces.get_categorie() in ["prefecture", "structure"]])
        context['autoriser_transfert_pj'] = True
        return context


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class InstructionPublishBylawView(UpdateView):
    """Ajoute un document officiel à une instruction"""

    # Configuration
    model = Instruction
    form_class = InstructionPublishForm

    # Overrides
    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['user'] = forms.IntegerField(initial=self.request.user.id, required=False)
        return form

    def form_valid(self, form):
        if form.cleaned_data['fichier']:
            nature = int(form.cleaned_data['nature'])
            user = User.objects.get(id=form.fields['user'].initial)
            if not file_type_valide_non_form(form.cleaned_data['fichier']):
                form.add_error('fichier', 'Format de fichier incorrect')
                return super().form_invalid(form)
            if nature == 1:
                if form.instance.manif.get_cerfa().consultServices == AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation obligatoire']:
                    # selectionner la catégorie fédération avec le pk
                    nb_avis = form.instance.get_tous_avis().exclude(
                        service_consulte_fk__type_service_fk__categorie_fk__pk=12).exclude(
                        service_consulte_fk__type_service_fk__categorie_fk__nom_s="Services de l'État").count()
                    if nb_avis < 1:
                        messages.error(self.request, "Aucune action effectuée : Consultation obligatoire, "
                                                     "le nombre d'avis émis est insuffisant.")
                        return HttpResponseRedirect(self.get_success_url())
            scan = False if settings.CELERY_ENABLED else True
            DocumentOfficiel.objects.create(fichier=form.cleaned_data['fichier'], nature=nature,
                                            instruction=form.instance, utilisateur=user, scan_antivirus=scan)
            form.instance.referent = self.request.user
            if nature == 0:
                form.instance.ajouter_interdiction(user)
            elif nature == 4:
                form.instance.ajouter_annulation(user)
            elif nature in [2, 5, 6]:
                form.instance.notifier_ajout_document_officiel(self.request.user)
            else:
                form.instance.ajouter_autorisation(user)
        return super().form_valid(form)

    def get_success_url(self):
        """ Retourne l'URL de destination lorsque le formulaire est validé """
        return self.object.get_absolute_url()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['publier'] = True
        return context


class InstructionTransfertPjVersDocOfficiel(UpdateView):
    # Configuration
    model = Instruction
    form_class = InstructionPublierForm
    template_name = 'instructions/instruction_form_transf_pj_vers_doc_officiel_ajax.html'

    # Overrides
    @method_decorator(verifier_secteur_instruction())
    def dispatch(self, *args, **kwargs):
        self.kwargs['doc'] = doc = get_object_or_404(PieceJointeAvis, pk=self.request.GET.get('pj'))
        # Si un document officiel existe déjà pour la pièce jointe, erreur
        if DocumentOfficiel.objects.filter(fichier=doc.fichier).exists():
            return render(self.request, 'core/access_restricted.html', status=403)
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['doc'] = self.kwargs['doc']
        return context

    def form_valid(self, form):
        nature = int(form.cleaned_data['nature'])
        user = self.request.user
        doc = self.kwargs['doc']
        if nature == 1:
            if form.instance.manif.get_cerfa().consultServices == AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation obligatoire']:
                # selectionner la catégorie fédération avec le pk
                nb_avis = form.instance.get_tous_avis().exclude(
                    service_consulte_fk__type_service_fk__categorie_fk__pk=12).exclude(
                    service_consulte_fk__type_service_fk__categorie_fk__nom_s="Services de l'État").count()
                if nb_avis < 1:
                    messages.error(self.request, "Aucune action effectuée : Consultation obligatoire, "
                                                 "le nombre d'avis émis est insuffisant.")
                    return HttpResponseRedirect(self.get_success_url())
        DocumentOfficiel.objects.create(fichier=doc.fichier, nature=nature, instruction=form.instance, utilisateur=user)
        form.instance.referent = user
        if nature == 0:
            form.instance.ajouter_interdiction(user)
        elif nature == 4:
            form.instance.ajouter_annulation(user)
        elif nature in [2, 5, 6]:
            form.instance.notifier_ajout_document_officiel(self.request.user)
        else:
            form.instance.ajouter_autorisation(user)
        return super().form_valid(form)


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class RelanceAvis(View):
    """Vue pour relancer les avis non envoyé"""
    def get(self, request, pk):
        liste_avis = Avis.objects.filter(instruction__pk=pk).filter(~Q(etat="rendu")).order_by('-pk')
        for avis in liste_avis:
            avis.notifier_creation_avis(origine=[self.request.user, self.request.organisation], agents=avis.get_agents_avec_service())
        return redirect(reverse('instructions:instruction_detail', kwargs={'pk': pk}))


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class AvisPrefecture(View):

    def get(self, request, pk):
        instruction = get_object_or_404(Instruction, pk=pk)
        if instruction.etat not in ["demandée", "distribuée"]:
            return redirect('instructions:instruction_detail', pk=instruction.pk)
        avis_pref = instruction.avis.filter(service_consulte_fk=instruction.get_prefecture_concernee()).first()
        form = InstructionAvisForm(instance=avis_pref)
        return render(request, 'instructions/avis_prefecture_form.html', {"form": form, 'object': instruction})

    def post(self, request, pk):
        instruction = get_object_or_404(Instruction, pk=pk)
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        if instruction.etat not in ["demandée", "distribuée"]:
            return redirect('instructions:instruction_detail', pk=instruction.pk)
        avis_pref = instruction.avis.filter(service_consulte_fk=instruction.get_prefecture_concernee()).first()
        if not avis_pref:
            for avis in instruction.avis.all():
                if avis.service_consulte_fk.pk == organisation.pk:
                    avis_pref = avis
        avis_pref.date_reponse = timezone.now()
        avis_pref.etat = 'rendu'
        form = InstructionAvisForm(request.POST, request.FILES, instance=avis_pref)
        if form.is_valid():
            form.save()
            if form.instance.favorable:
                instruction.etat = "autorisée"
                titre = f"Avis favorable de la Préfecture {str(instruction.instance.departement)}"
                message = f"L'avis de la Préfecture du département {str(instruction.instance.departement)} a été rendu.<br>" \
                          f"Il est favorable, le détail se trouve dans le suivi de l'instruction du dossier."
            else:
                instruction.etat = 'interdite'
                titre = f"Avis défavorable de la Préfecture {str(instruction.instance.departement)}"
                message = f"L'avis de la Préfecture du département {str(instruction.instance.departement)} a été rendu.<br>" \
                          f"Il est défavorable, le détail se trouve dans le suivi de l'instruction du dossier."
            instruction.referent = request.user
            instruction.save()
            if instruction.instruction_principale.referent:
                destinataires = [instruction.instruction_principale.referent, instruction.instruction_principale.get_prefecture_concernee()]
            else:
                destinataires = instruction.instruction_principale.get_instructeurs_avec_services()
            Message.objects.creer_et_envoyer('action', [request.user, request.organisation], destinataires, titre, message,
                                             manifestation_liee=instruction.manif, objet_lie_nature="avis", objet_lie_pk=avis_pref.pk)

            return redirect('instructions:instruction_detail', pk=instruction.pk)
        else:
            return HttpResponse(form.errors, status=402)


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class AvisPrefecturePiecejointe(View):

    def post(self, request, pk):
        instruction = get_object_or_404(Instruction, pk=pk)
        avis = instruction.avis.filter(service_consulte_fk=instruction.get_prefecture_concernee()).first()
        if 'fichier' in request.FILES and request.FILES['fichier']:
            from core.FileTypevalidator import file_type_valide_non_form
            if file_type_valide_non_form(request.FILES['fichier']):
                pj = PieceJointeAvis.objects.create(
                    fichier=request.FILES['fichier'], avis=avis, date_depot=timezone.now())
            else:
                return render(request, "core/access_restricted.html",
                              {'message': "Données POST incorrectes !"},
                              status=403)
            titre = f"Pièce jointe ajoutée par la Préfecture {str(instruction.instance.departement)}"
            message = f"Pièce jointe ajoutée à son avis par la Préfecture du département {str(instruction.instance.departement)}."
            instruction.referent = request.user
            instruction.save()
            if instruction.instruction_principale.referent:
                destinataires = [instruction.instruction_principale.referent, instruction.instruction_principale.get_prefecture_concernee()]
            else:
                destinataires = instruction.instruction_principale.get_instructeurs_avec_services()
            Message.objects.creer_et_envoyer('action', [request.user, request.organisation], destinataires, titre, message,
                                             manifestation_liee=instruction.manif)
            if settings.CELERY_ENABLED:
                pj.scan_antivirus = False
                pj.save()
                creation_thumbnail_pj_avis.delay(pj.pk)
            return redirect('instructions:instruction_detail', pk=instruction.pk)
        else:
            return render(request, "core/access_restricted.html", {'message': "Données POST incorrectes !"}, status=403)
