# coding: utf-8
import json
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.db.models import Q
from django.shortcuts import render, redirect, HttpResponse

from instructions.models import Instruction, AutorisationAcces
from core.util.permissions import require_type_organisation
from core.models.instance import Instance
from administrative_division.models import Arrondissement
from structure.models import Organisation, ServiceConsulte, ServiceInstructeur, Federation, ReunionCDSR
from messagerie.models import CptMsg


def get_instructions(service, temporalite, filtre_specifique, request, annee=None, all=None):
    # bypass pour avoir les bonnes instructions directement
    if service.is_service_instructeur():
        instruction_qs = service.instructions.all()
        instruction_qs = instruction_qs | Instruction.objects.filter(avis__in=(service.avis_instruis.all() | service.avis_lecture.all()))
    else:
        instruction_qs = Instruction.objects.filter(avis__in=(service.avis_instruis.all() | service.avis_lecture.all()))
    if not all:
        if temporalite == "a_traiter":  # cas général
            instruction_qs = instruction_qs.filter(manif__date_fin__gte=timezone.now())
        elif temporalite == "en_retard":  # todo : à supprimer ou à développer
            instruction_qs = instruction_qs
        elif annee:  # Archive pour une année
            instruction_qs = instruction_qs.filter(
                manif__date_fin__lt=timezone.now()).filter(manif__date_fin__year=annee)
        elif temporalite == "archive":
            # La liste des années n'est plus calculée (trop long),
            # normalement pas d'appel à la fonction avec cette temporalité
            return []

    if service.is_service_instructeur() and not filtre_specifique == "serviceconsulte":
        # Si service est une Mairie ou Préfecture on separe les services concerne pour ne garder que les instructions
        pk = []
        for i in instruction_qs:
            if service in i.service_instructeur_m2m.all():
                pk.append(i.pk)
        instruction_qs = Instruction.objects.filter(pk__in=pk).distinct('manif')
        if (not service.instance_fk.instruction_mode == service.instance_fk.IM_CIRCUIT_A and
                service.instance_fk.acces_arrondissement and not service.is_mairie() and
                filtre_specifique not in ["tous_arrondissement", 'moi']):
            # Prefecture
            if request.GET.get('arron') and Arrondissement.objects.filter(code=request.GET.get('arron')).exists():
                # Si il y as un arrondissement dans le requet
                arrondissement_selectionne = Arrondissement.objects.get(code=request.GET.get('arron'))
            else:
                # Sinon on prend le premier du service
                arrondissement_selectionne = service.arrondissement_m2m.first()

            instruction_qs_non_pref = instruction_qs.filter(
                    manif__ville_depart_interdep_mtm__arrondissement=arrondissement_selectionne,
                    instruction_prefecture=False).filter(instance=service.instance_fk)
            arrondissement_prefecture = ServiceInstructeur.objects.get(
                instance_fk=service.instance_fk, type_service_instructeur_s="prefecture").arrondissement_m2m.first()
            if arrondissement_selectionne == arrondissement_prefecture:
                instruction_qs_pref = instruction_qs.filter(
                    instruction_prefecture=True).filter(instance=service.instance_fk)
                instruction_qs = instruction_qs_non_pref | instruction_qs_pref
            else:
                instruction_qs = instruction_qs_non_pref

        if not service.is_mairie():
            pk = []
            for i in instruction_qs:
                # Ajoute au tableau les instruction spécifique aux mairies
                if (not i.manif.villes_traversees.all() and
                        not i.manif.get_type_manif() in ['dvtm', 'avtm', 'dvtmcir'] and
                        i.manif.instance_instruites.count() == 1):
                    pk.append(i.pk)
            # ici ce ne s'applique pas sur les mairies le tri ayant était fait en cache
            if filtre_specifique == 'instructionmairie':
                # Si instruction en mairie on les séléctionne
                instruction_qs = instruction_qs.filter(pk__in=pk)
            else:
                # Sinon on les exclues
                instruction_qs = instruction_qs.exclude(pk__in=pk)
        if filtre_specifique == "moi":
            instruction_qs = instruction_qs.filter(referent=request.user)
    else:
        # Service Consulté

        instruction_qs = instruction_qs.filter(
            Q(avis__service_consulte_fk=service) | Q(avis__service_consulte_history_m2m=service)).distinct("manif")
        if service.is_service_instructeur():
            # on enleve les avis dont la prefecture ou la mairie est aussi en instruction
            pk = []
            for i in instruction_qs:
                if i.get_instructeurs_mairie():
                    communes = i.manif.ville_depart_interdep_mtm.filter(
                        arrondissement__departement=i.instance.departement)
                    service_i = ServiceInstructeur.objects.get(commune_m2m=communes.first(),
                                                               type_service_instructeur_s="mairie")
                    if service_i.pk == service.pk:
                        pk.append(i.pk)
                else:
                    avis = i.avis.filter(service_consulte_origine_fk=service)
                    if avis:
                        avis = avis.first()
                        if avis.avis_interdep_avtm_b:
                            pk.append(i.pk)
            instruction_qs = instruction_qs.exclude(pk__in=pk)

        if (not service.is_federation() and request.GET.get('dept') and
                filtre_specifique not in ["tout_departement", 'moi']):
            instruction_qs = instruction_qs.filter(instance__departement__name=request.GET.get('dept'))
        if filtre_specifique == "moi":
            instruction_qs = instruction_qs.filter(avis__agent=request.user)
    return instruction_qs.distinct('manif')


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class TableauAjax(View):

    def dispatch(self, request, *args, **kwargs):
        if self.kwargs.get('service_pk'):
            organisation = Organisation.objects.get(pk=self.kwargs.get('service_pk'))
            if not organisation in self.request.user.organisation_m2m.all():
                return render(request, 'core/access_restricted.html', status=403)
        return super(TableauAjax, self).dispatch(request, *args, **kwargs)

    def get(self, request, service_pk=None):
        user = request.user
        temporalite = self.request.GET.get('temporalite', default='a_traiter')
        filtre_specifique = self.request.GET.get('filtre_specifique', default=None)
        if service_pk:
            organisation = Organisation.objects.get(pk=service_pk)
        else:
            organisation = user.organisation_m2m.all().first()
        if filtre_specifique:
            instructions = get_instructions(organisation, temporalite, "", request)
        else:
            instructions = get_instructions(organisation, temporalite, "serviceconsulte", request)
        if isinstance(instructions, list):
            instruction_count = len(instructions)
        else:
            instruction_count = instructions.count()
        lecture_count = AutorisationAcces.objects.filter(organisation=organisation,
                                                         date_revocation__isnull=True,
                                                         manif__date_fin__gte=timezone.now()).count()

        return HttpResponse(json.dumps({
            "instruction_count": instruction_count,
            "lecture_count": lecture_count
        }))


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class TableauBody(View):

    def dispatch(self, request, *args, **kwargs):
        if self.kwargs.get('service_pk'):
            organisation = Organisation.objects.get(pk=self.kwargs.get('service_pk'))
            if not organisation in self.request.user.organisation_m2m.all():
                return render(request, 'core/access_restricted.html', status=403)
        return super(TableauBody, self).dispatch(request, *args, **kwargs)

    def get(self, request, service_pk=None):
        user = request.user

        temporalite = self.request.GET.get('temporalite', default='a_traiter')
        annee = self.request.GET.get('annee', None)
        filtre_specifique = self.request.GET.get('filtre_specifique', default=None)
        if service_pk:
            organisation = Organisation.objects.get(pk=service_pk)
        else:
            organisation = user.organisation_m2m.all()
            if organisation and organisation.count() == 1:
                return redirect('instructions:tableaudebord', service_pk=organisation.first().pk)
            elif organisation:
                return redirect("instructions:tableaudebord_choix")
            else:
                return redirect('profile')
        instance = organisation.instance_fk
        # context pour l'instruction d'arrondissement
        if (organisation.is_service_instructeur() and not organisation.is_mairie() and
                temporalite == "a_traiter" and not filtre_specifique == "serviceconsulte"):
            onglet = bool(instance.acces_arrondissement and instance.instruction_mode != Instance.IM_CIRCUIT_A and
                          not self.request.GET.get('temporalite', default=None) == 'archive')
            if organisation.arrondissement_m2m.first():
                user_arrondis = organisation.arrondissement_m2m.first().code
            else:
                user_arrondis = None
            liste_arrondis = Arrondissement.objects.filter(departement=instance.departement)

            if self.request.GET.get('arron'):
                actual_arrondis = self.request.GET.get('arron')
            else:
                actual_arrondis = user_arrondis
        else:
            onglet = False
            actual_arrondis = None
            user_arrondis = None
            liste_arrondis = None

        # context pour l'instruction departement des services simples
        if (organisation.is_service_consulte() and
                not user.get_instance().is_master() and
                not organisation.is_service_instructeur()):
            service_dept = organisation.departement_m2m.all()
            onglet_service = bool(len(service_dept) > 1)
            liste_dept = service_dept

            dept = self.request.GET.get('dept', default=user.get_departement().name)
            actual_dept = dept
        else:
            onglet_service = False
            liste_dept = None
            actual_dept = None

        if temporalite == "archive":
            instructions = Instruction.objects.none()
            liste_annee_archives = []
            annee_archives = 2017
            while annee_archives <= timezone.now().year:
                liste_annee_archives.append(str(annee_archives))
                annee_archives += 1
        else:
            # avoir la liste des instructions
            instructions = get_instructions(organisation, temporalite, filtre_specifique, request, annee=annee)
            liste_annee_archives = []

        instruction_mairie = bool(organisation.is_mairie() and
                                  self.request.GET.get('filtre_specifique', default=None) != 'serviceconsulte')

        user_is_instructeur = bool((organisation.is_service_instructeur() and not organisation.is_mairie() and
                                    filtre_specifique != "serviceconsulte") or
                                   organisation.is_mairie() and instruction_mairie)

        pref_en_instruction_mairie = bool(organisation.is_service_instructeur() and not organisation.is_mairie() and filtre_specifique == "instructionmairie")
        archive = bool(self.request.GET.get('temporalite', default=None) == 'archive')

        consult = AutorisationAcces.objects.filter(organisation=organisation,
                                                   date_revocation__isnull=True,
                                                   manif__date_fin__gte=timezone.now()).count()
        reunions = None
        if organisation.is_service_instructeur() and not organisation.is_mairie():
            reunions_avenir = ReunionCDSR.objects.filter(date__gte=timezone.now().date(), service_cdsr__instance_fk=instance).order_by('date')
            for reunion in reunions_avenir:
                reunion.color = "info"
            derniere_reunion_passee = None
            if ReunionCDSR.objects.filter(date__lt=timezone.now().date(), service_cdsr__instance_fk=instance).exists():
                derniere_reunion_passee = ReunionCDSR.objects.filter(
                    date__lt=timezone.now().date(), service_cdsr__instance_fk=instance).order_by('date').last()
                derniere_reunion_passee.color = "secondary"
            reunions_adefinir = ReunionCDSR.objects.filter(date__isnull=True, service_cdsr__instance_fk=instance)
            for reunion in reunions_adefinir:
                reunion.color = "warning"

            reunions = [derniere_reunion_passee] if derniere_reunion_passee else []
            reunions += list(reunions_avenir) + list(reunions_adefinir)
        if temporalite == "archive":
            consult = AutorisationAcces.objects.filter(organisation=organisation,
                                                       date_revocation__isnull=True,
                                                       manif__date_fin__lt=timezone.now()).count()
            if annee:
                consult = AutorisationAcces.objects.filter(organisation=organisation, date_revocation__isnull=True,
                                                           manif__date_fin__lt=timezone.now(),
                                                           manif__date_fin__year=annee).count()

        cpt = CptMsg.objects.get(utilisateur=request.user)

        # Contexte général
        context = {
            "cpt": cpt,
            "user_is_instructeur": user_is_instructeur,
            'nb_instructions': instructions.count(),
            "archive": archive,
            "instruction_mairie": instruction_mairie,
            "pref_en_instruction_mairie": pref_en_instruction_mairie,
            "liste_annee_archives": liste_annee_archives,
            "annee": annee,
            "consultation": consult,
            "reunions": reunions,
            "service": organisation,
        }

        # Détermination du cas en présence
        if user_is_instructeur:
            usertype = "instructeur"

        else:
            usertype = "avis"

        # Définition des dicts de chaque blocs
        indic = {}
        indic['rendu'], indic['autorise'], indic['annule'], indic['interdit'], indic['atraiter'], indic[
            'encours'], indic['demarre'] = 0, 0, 0, 0, 0, 0, 0

        afficher_encours = bool(usertype == "instructeur" or
                                (organisation.autorise_interroge_preavis_famille_b or
                                 organisation.interaction_entrante.filter(type_lien_s="interroger")))

        if usertype == "instructeur":

            # Remplissage des données par blocs
            for instru in instructions:
                cle = None
                # Distribution des instructions par blocs
                etat = instru.etat
                if etat == "demandée":

                    if not instru.referent:
                        cle = "atraiter"
                    else:
                        cle = "demarre"
                elif etat == "distribuée":
                    cle = "encours"
                elif etat == "interdite":
                    cle = "interdit"
                elif etat == "annulée":
                    cle = "annule"
                elif etat == "autorisée":
                    cle = "autorise"

                indic[cle] += 1

        elif usertype == "avis":

            # Remplissage des données par blocs
            for instru in instructions:
                # Distribution des instructions par blocs
                avis = instru.get_avis_service(organisation)
                if instru.etat == "interdite":
                    cle = "interdit"
                elif instru.etat == "annulée":
                    cle = "annule"
                elif instru.etat == "autorisée":
                    cle = "autorise"
                else:
                    if avis.service_consulte_fk != organisation or avis.get_etat() == "rendu":
                        cle = "rendu"
                    else:
                        if avis.agent not in organisation.get_users_list():
                            cle = "atraiter"
                        elif afficher_encours and avis.avis_enfant.all():
                            cle = "encours"
                        else:
                            cle = "demarre"

                indic[cle] += 1

        context = {**context,
                   "atraiter": indic['atraiter'],
                   "demarre": indic['demarre'],
                   "encours": indic['encours'],
                   "rendu": indic['rendu'],
                   "autorise": indic['autorise'],
                   "interdit": indic['interdit'],
                   "annule": indic['annule'],
                   "onglet": onglet,
                   "user_arrondis": user_arrondis,
                   "liste_arrondis": liste_arrondis,
                   "actual_arrondis": actual_arrondis,
                   "onglet_service": onglet_service,
                   "liste_dept": liste_dept,
                   "actual_dept": actual_dept,
                   "afficher_encours": afficher_encours,
                   }

        return render(request, "instructions/tableaudebord_body.html", context)


@method_decorator(require_type_organisation([ServiceConsulte, ServiceInstructeur, Federation]), name='dispatch')
class TableauListe(View):

    def dispatch(self, request, *args, **kwargs):
        if self.kwargs.get('service_pk'):
            organisation = Organisation.objects.get(pk=self.kwargs.get('service_pk'))
            if not organisation in self.request.user.organisation_m2m.all():
                return render(request, 'core/access_restricted.html', status=403)
        return super(TableauListe, self).dispatch(request, *args, **kwargs)

    def get(self, request, service_pk):
        service = Organisation.objects.get(pk=service_pk)
        temporalite = self.request.GET.get('temporalite', default='a_traiter')
        # possible tous_arrondissement, tout_departement, instructionmairie, moi
        filtre_specifique = self.request.GET.get('filtre_specifique', default=None)
        annee = self.request.GET.get('annee', None)
        filtre_etat = self.request.GET.get('filtre_etat', default=None)

        archive = bool(self.request.GET.get('temporalite', default=None) == 'archive')

        instruction_mairie = bool(service.is_mairie() and filtre_specifique != "serviceconsulte")

        user_is_instructeur = bool(service.is_service_instructeur() and not filtre_specifique == "serviceconsulte")

        agent_avis = bool(not user_is_instructeur and not instruction_mairie)

        # avoir la liste des instructions
        instructions = get_instructions(service, temporalite, filtre_specifique, request, annee=annee)
        instructions_liste = []
        titre = ""

        if service.is_service_instructeur() and not filtre_specifique == "serviceconsulte":
            # Pas de marquage spécial, les lignes seront colorées selon l'état
            if filtre_etat:
                # Instructions du blocs
                if filtre_etat == "atraiter":
                    titre = "Liste des manifestations à traiter"
                    instructions_liste = instructions.filter(etat="demandée").exclude(referent__isnull=False)
                elif filtre_etat == "demarre":
                    titre = "Liste des manifestations à traiter"
                    instructions_liste = []
                    for i in instructions.filter(etat="demandée").exclude(referent__isnull=True):
                        i.css = "table_encours"
                        instructions_liste.append(i)
                elif filtre_etat == "encours":
                    titre = "Liste des manifestations en cours d'instruction"
                    instructions_liste = instructions.filter(etat="distribuée")
                elif filtre_etat == "autorise":
                    titre = "Liste des manifestations autorisées"
                    instructions_liste = instructions.filter(etat="autorisée")
                elif filtre_etat == "interdit":
                    titre = "Liste des manifestations interdites"
                    instructions_liste = instructions.filter(etat="interdite")
                elif filtre_etat == "annule":
                    titre = "Liste des manifestations annulées"
                    instructions_liste = instructions.filter(etat="annulée")
            else:
                # Tout voir
                titre = "Liste des manisfestations à venir"
                instructions_liste = instructions
        else:
            # Marquer chaque instruction avec l'attribut "css" pour colorer la ligne
            if not filtre_etat:
                # Tout voir
                filtre_etat = ["atraiter", "encours", "rendu", "autorise", "interdit", "annule"]

            afficher_encours = bool(service.is_service_instructeur() or
                                    (service.autorise_interroge_preavis_famille_b or
                                     service.interaction_entrante.filter(type_lien_s="interroger")))

            for instruction in instructions:
                avis = instruction.get_avis_service(service)
                if instruction.etat not in ["autorisée", "annulée", "interdite"]:
                    if "atraiter" in filtre_etat:
                        titre = "Liste des avis à traiter"
                        if (avis.service_consulte_fk == service and avis.get_etat() != "rendu" and
                                avis.agent not in service.get_users_list()):
                            instruction.css = "table_afaire"
                            instructions_liste.append(instruction)

                    if "demarre" in filtre_etat:
                        titre = "Liste des avis demarrés"
                        if afficher_encours:
                            if (avis.service_consulte_fk == service and avis.get_etat() != "rendu" and
                                    avis.agent in service.get_users_list() and not avis.avis_enfant.all()):
                                instruction.css = "table_encours"
                                instructions_liste.append(instruction)
                        else:
                            if (avis.service_consulte_fk == service and avis.get_etat() != "rendu" and
                                    avis.agent in service.get_users_list()):
                                instruction.css = "table_encours"
                                instructions_liste.append(instruction)

                    if "encours" in filtre_etat:
                        titre = "Liste des avis en cours"
                        if (avis.service_consulte_fk == service and avis.get_etat() != "rendu" and
                                avis.agent in service.get_users_list() and avis.avis_enfant.all()):
                            instruction.css = "table_encours"
                            instructions_liste.append(instruction)

                    if "rendu" in filtre_etat:
                        titre = "Liste des avis rendus pour les manifestations en cours d'instruction"
                        if avis.service_consulte_fk != service or avis.get_etat() == "rendu":
                            instruction.css = "table_termine"
                            instructions_liste.append(instruction)
                else:
                    if "autorise" in filtre_etat:
                        titre = "Liste des avis des manifestations autorisées"
                        if instruction.etat == "autorisée":
                            instruction.css = "table_termine"
                            instructions_liste.append(instruction)
                    if "interdit" in filtre_etat:
                        titre = "Liste des avis des manifestations interdites"
                        if instruction.etat == "interdite":
                            instruction.css = "table_termine"
                            instructions_liste.append(instruction)
                    if "annule" in filtre_etat:
                        titre = "Liste des avis des manifestations annulées"
                        if instruction.etat == "annulée":
                            instruction.css = "table_termine"
                            instructions_liste.append(instruction)
            if type(filtre_etat) is list:
                titre = "Liste des avis des manifestations à venir"

        context = {
            "user_is_instructeur": user_is_instructeur,
            "archive": archive,
            "instruction_mairie": instruction_mairie,
            "instructions": instructions_liste,
            "titre": titre,
            "service": service,
            "agent_avis": agent_avis,
        }

        return render(request, "instructions/tableaudebord_liste.html", context)


class TableauChoix(View):
    """
    Vue pour choisir son tableau de bord
    """
    def get(self, request):
        return render(request, 'instructions/tableaudebord_choix.html')
