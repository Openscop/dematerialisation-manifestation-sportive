# coding: utf-8
from django.shortcuts import get_object_or_404, render, Http404
from functools import wraps
from asgiref.sync import sync_to_async

from core.util.permissions import require_type_organisation, require_type_organisation_async
from evenements.models import Manif
from instructions.models.instruction import Instruction
from instructions.models.avis import Avis
from structure.models import Organisation, ServiceInstructeur, ServiceConsulte, PresentationCDSR, ReunionCDSR


def verifier_secteur_instruction_function(request, acces=False, *args, **kwargs):
    if request.user.is_anonymous:
        return render(request, "core/access_restricted.html",
                      {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)
    o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
    if o_support in request.user.organisation_m2m.all():
        return False
    organisation = Organisation.objects.get(pk=request.session['service_pk'])
    # Dans le cas d'une demande de renvoi ou de suppression d'avis, le pk fourni est celui de l'avis
    if request.path.split('/')[-2] in ['avis_prefecture_ajout_pj', 'resend', 'del', 'datelimite'] and request.path.split('/')[-4] == 'avis':
        instruction = get_object_or_404(Instruction, avis__pk=kwargs['pk'])
    # Dans le cas d'une demande d'export pdf de la manif, le pk fourni est celui de la manif
    elif request.path.split('/')[-2] == 'instructeur' and request.path.split('/')[-3] == 'export':
        manif = get_object_or_404(Manif, pk=kwargs['pk'])
        instruction = manif.get_instruction(instance=organisation.instance_fk)
    # Dans le cas d'une modification de l'affichage dans le calendrier, le pk fourni est celui de la manif
    elif (request.path.split('/')[-2] == 'instruction' or request.path.split('/')[-2] == 'modification') and request.path.split('/')[-4] == 'evenement':
        manif = get_object_or_404(Manif, pk=kwargs['pk'])
        instruction = manif.get_instruction(instance=organisation.instance_fk)
    # Dans le cas d'une demande de complémentd'information, le pk fourni est celui de la manif
    elif request.path.split('/')[-3] == 'doc_complementaire' and request.path.split('/')[-5] == 'evenement':
        manif = get_object_or_404(Manif, pk=kwargs['manif_pk'])
        instruction = manif.get_instruction(instance=organisation.instance_fk)
    # Dans le cas d'une présentation cdsr, le pk fourni est celui de la présentation
    elif request.path.split('/')[3] == 'presentation':
        presentation = get_object_or_404(PresentationCDSR, pk=kwargs['pk'])
        instruction = presentation.instruction_fk
    # Cas général ou le pk est celui de l'instruction
    else:
        instruction = get_object_or_404(Instruction, pk=kwargs['pk'])
    if not instruction:
        return Http404('no found')
    if type(organisation) == ServiceInstructeur:
        if instruction in organisation.instructions.all():
            return False
    # Vérifier autorisation d'accès
    if request.user.is_authenticated:
        if acces and organisation in [acces.organisation for acces in instruction.acces.filter(date_revocation__isnull=True)]:
            return False
    return render(request, "core/access_restricted.html",
                  {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)


def verifier_secteur_instruction_async(function=None, acces=False):
    def decorator(view_func):
        @require_type_organisation_async([ServiceInstructeur,])
        @wraps(view_func)
        async def _wrapped_view(request, *args, **kwargs):
            erreur = await sync_to_async(verifier_secteur_instruction_function)(request, acces, *args, **kwargs)
            if erreur:
                return erreur
            return await view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def verifier_secteur_instruction(function=None, acces=False):
    """
    Décorateur : limite l'accès des instructions en fonction du secteur
    :param function: La fonction décorée
    :param acces: autorise l'examen des autorisations d'accès
    :return:
    """
    def decorator(view_func):
        @require_type_organisation([ServiceInstructeur, ServiceConsulte])
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            erreur = verifier_secteur_instruction_function(request, acces, *args, **kwargs)
            if erreur:
                return erreur
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def verifier_service_avis_function(request, acces=False, *args, **kwargs):
    if request.user.is_anonymous:
        return render(request, "core/access_restricted.html",
                      {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)
    # calcul sorti du décorateur pour pouvoir l'utiliser en async
    o_support = ServiceConsulte.objects.filter(type_service_fk__categorie_fk__nom_s="_Support").first()
    if o_support in request.user.organisation_m2m.all():
        return False
    organisation = Organisation.objects.get(pk=request.session['service_pk'])
    if request.path.split('/')[3] == 'presentation':
        presentation = get_object_or_404(PresentationCDSR, pk=kwargs['pk'])
        if organisation in presentation.get_tous_membres():
            return False
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)
    avis = get_object_or_404(Avis, pk=kwargs['pk'])
    # TODO : relecture V6 : à discuter
    if avis.service_consulte_fk == organisation or \
            avis.service_consulte_origine_fk == organisation or \
            (acces and organisation in avis.service_consulte_history_m2m.all()) or\
            (avis.avis_parent_fk in Avis.objects.filter(service_consulte_fk=organisation)):
        return False
    # Avis CDSR
    if avis.service_consulte_fk.is_service_cdsr() and organisation.is_service_instructeur() and not organisation.is_mairie():
        if avis.service_consulte_fk.instance_fk == organisation.instance_fk:
            return False
    # Vérifier autorisation d'accès
    if request.user.is_authenticated:
        if acces and organisation in [acces.organisation for acces in
                                                    avis.acces.filter(date_revocation__isnull=True)]:
            return False
        # S'il y a des avis relances vérifier leurs autorisation d'accés
        if avis.avis_relance.all():
            for avis_relance in avis.avis_relance.all():
                if acces and organisation in [acces.organisation for acces in avis_relance.acces.filter(date_revocation__isnull=True)]:
                    return False
    return render(request, "core/access_restricted.html",
                  {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)


def verifier_service_avis_async(function=None, acces=False):
    """ Décorateur : limite l'accès des avis en fonction du service """

    def decorator(view_func):
        @wraps(view_func)
        async def _wrapped_view(request, *args, **kwargs):
            erreur = await sync_to_async(verifier_service_avis_function)(request, acces, *args, **kwargs)
            if erreur:
                return erreur
            return await view_func(request, *args, **kwargs)

        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def verifier_service_avis(function=None, acces=False):
    """ Décorateur : limite l'accès des avis en fonction du service """

    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            erreur = verifier_service_avis_function(request, acces, *args, **kwargs)
            if erreur:
                return erreur
            return view_func(request, *args, **kwargs)

        return _wrapped_view

    if function:
        return decorator(function)
    return decorator
