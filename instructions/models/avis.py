# coding: utf-8
import pytz

from django.urls import reverse
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django_fsm import FSMField
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django_fsm import transition
from django.conf import settings

from configuration.directory import UploadPath
from core.tasks import creation_thumbnail_pj_avis
from core.FileTypevalidator import file_type_validator
from structure.models.service import ServiceConsulte


class AvisQuerySet(models.QuerySet):
    """ Queryset par défaut pour les avis """

    def par_instance(self, request=None, instances=None):
        """
        Renvoyer les avis pour l'utilisateur ou les instances

        :param request: requête HTTP
        :param instances: liste d'instances
        :type instances: list<core.instance>
        """
        if request and not request.user.is_anonymous:
            # Renvoyer les avis pour l'utilisateur s'il est renseigné
            return self.filter(instruction__manif__instance_id=request.user.get_instance().pk)
        elif instances:
            # Renvoyer les avis pour les instances si elles sont renseignées
            return self.filter(instruction__manif__instance__in=instances)
        else:
            return self

    def en_cours(self):
        """ Renvoyer les avis dont la manifestation n'est pas terminée """
        return self.filter(instruction__manif__date_fin__gte=timezone.now())

    def termine(self):
        """ Renvoyer les avis pour lesquels l'événement prévu est terminé """
        return self.filter(instruction__manif__date_fin__lt=timezone.now()).order_by('-instruction__manif__date_debut')

    def rendu(self):
        """
        Renvoyer les avis rendus

        L'état acknowledged indique qu'un avis est rendu.
        """
        return self.filter(etat='rendu')

    def envoye(self):
        """ Renvoyer les avis pour lesquels les demandes de préavis ont été envoyées  """
        return self.filter(etat='distribué')

    def a_soumttre(self):
        """
        Renvoyer les avis qui peuvent être soumis

        Avant d'être rendu, un agent autorisé peut soumettre un avis
        en ajoutant des informations sensibles pour l'agent en charge
        de valider l'avis ou de ne pas trancher positivement.
        """
        return self.filter(etat__in=['demandé', 'distribué'])

    def a_rendre(self):
        """
        Renvoyer les avis qui peuvent être rendus

        Les avis qui peuvent être (re)rendus sont des avis qui sont
        soit à l'état formaté (donc prêts à soumettre à l'agent probateur),
        soit à l'état rendu (dans ce cas, on peut à nouveau rendre l'avis.
        """
        return self.filter(etat__in=['formaté', 'rendu'])

    def a_envoye(self):
        """
        Renvoyer les avis qui peuvent être dispatchés

        On peut redispatcher un avis rendu, ou qui a déjà été dispatché (rappel aux agents)
        """
        return self.filter(etat__in=['rendu', 'distribué'])


class Avis(models.Model):
    """ Avis """
    # Champs
    etat = FSMField(default='demandé')  # Voir liste des états dans : documentation/Version-4_description.md
    date_demande = models.DateField("Date de demande", default=timezone.now)
    date_reponse = models.DateField("Date de retour", blank=True, null=True)
    favorable = models.BooleanField("Avis favorable ?", default=False, help_text="Si coché, l'avis rendu est considéré favorable")
    prescriptions = models.TextField("prescriptions", blank=True)

    instruction = models.ForeignKey('instructions.instruction', verbose_name="Instruction", on_delete=models.CASCADE)
    agent = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="Dernier intervenant", on_delete=models.SET_NULL, null=True)

    service_consulte_origine_fk = models.ForeignKey('structure.ServiceConsulte', null=True, on_delete=models.SET_NULL, verbose_name="Service originalement consulté", related_name="avis_instruis_origine")
    service_consulte_fk = models.ForeignKey('structure.ServiceConsulte', null=True, on_delete=models.SET_NULL, verbose_name="Service Consulté", related_name="avis_instruis")
    service_consulte_history_m2m = models.ManyToManyField('structure.ServiceConsulte', blank=True, verbose_name="Historique des services consulté", related_name="avis_lecture")
    service_demandeur_fk = models.ForeignKey('structure.ServiceConsulte', null=True, on_delete=models.SET_NULL, verbose_name="Service Consulté", related_name="avis_demande")

    avis_parent_fk = models.ForeignKey('self', verbose_name="Avis parent", related_name="avis_enfant", null=True, blank=True, on_delete=models.SET_NULL)

    avis_origine = models.ForeignKey('self', verbose_name="Avis d'origine", related_name="avis_relance", null=True, blank=True, on_delete=models.SET_NULL)
    raison_redemande = models.CharField(max_length=500, verbose_name="Raison de la redemande de l'avis", null=True, blank=True)

    historique_circuit_json = models.JSONField(max_length=1000, verbose_name="Historique du circuit emprunté par l'avis", null=True, blank=True)

    avis_interdep_avtm_b = models.BooleanField(default=False, verbose_name="Avis Préfécture dans le cadre d'une instruction avtm")

    objects = AvisQuerySet.as_manager()
    ## date limite de réponse d'un avis
    date_limite_reponse_dt = models.DateTimeField(null=True, blank=True, verbose_name="Date limite de réponse")

    # Overrides
    def __str__(self):
        if self.service_consulte_origine_fk:
            service = self.service_consulte_origine_fk.__str__().upper()
        elif self.service_consulte_fk:
            service = self.service_consulte_fk.__str__().upper()
        else:
            return '*** PAS DE SERVICE RATTACHE ***'
        return service

    def save(self, *args, **kwargs):
        if self._state.adding is True :
            self.set_date_limite_reponse()
        super().save(*args, **kwargs)

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'avis """
        return reverse('instructions:avis_detail', kwargs={'pk': self.pk})

    def get_etat(self):
        """Avoir le dernier etat de l'avis de la chaine de demande"""
        return self.get_last_avis().etat

    def get_last_avis(self):
        """Avoir le dernier avis de la chaine de demande"""
        if not self.avis_relance.all() and not self.avis_origine:
            return self
        if self.avis_origine:
            return self.avis_origine.avis_relance.all().order_by('pk').last()
        return self.avis_relance.all().order_by('pk').last()

    def get_origine_avis(self):
        return self.avis_origine if self.avis_origine else self

    def get_precedent_avis(self):
        """Avoir la liste des touts les avis sauf celui en cours"""
        if not self.avis_relance.all():
            return Avis.objects.none()
        original = Avis.objects.filter(pk=self.pk)
        precedent = self.avis_relance.exclude(pk=self.get_last_avis().pk)
        liste = (original | precedent)
        return liste

    def get_precedent_preavis(self):
        """Avoir les préavis précedent sans le encours"""
        if not self.avis_parent_fk.avis_origine and self == self.get_last_avis() and not self.avis_origine:
            return Avis.objects.none()
        preavis_redemande = Avis.objects.filter(service_consulte_origine_fk=self.service_consulte_origine_fk,
                                                instruction=self.instruction).exclude(pk=self.get_last_avis().pk)
        return preavis_redemande.order_by('pk')

    def get_service(self):
        """ Renvoyer le service concerné """
        return self.service_consulte_fk

    def get_tout_services_concerne(self):
        pk = []
        if self.service_consulte_fk:
            pk.append(self.service_consulte_fk.pk)
        [pk.append(service.pk) for service in self.service_consulte_history_m2m.all()]
        return ServiceConsulte.objects.filter(pk__in=pk)

    def get_categorie(self):
        if self.service_consulte_fk:
            return self.service_consulte_fk.type_service_fk.categorie_fk
        return "-"

    def get_agents_avec_service(self):
        """ Renvoyer les agents concernés par l'avis pour l'envoi de messages """
        return self.service_consulte_fk.get_utilisateurs_avec_service()


    def get_jours_restants_avant_limite(self):
        """ Renvoyer le nombre de jours restant avant la deadline d'instruction """
        now = pytz.UTC.localize(timezone.datetime.combine(timezone.now(), timezone.datetime.min.time()))
        return self.date_limite_reponse_dt - now

    def delai_expire(self):
        """ Renvoyer si le délai légal d'instruction a expiré """
        now = pytz.UTC.localize(timezone.datetime.combine(timezone.now(), timezone.datetime.min.time()))
        return now > self.date_limite_reponse_dt

    def get_form_date_limite(self):
        """Renvoyer le formulaire de date limite reponse"""
        from instructions.forms.date_limite_reponse import DateLimiteForm
        return DateLimiteForm(instance=self)

    def get_old_preavis_list(self):
        old_preavis = []
        if self.avis_parent_fk and self.avis_parent_fk.avis_origine and self.avis_parent_fk.avis_origine.get_precedent_avis:
            for preavis in self.avis_parent_fk.avis_origine.avis_enfant.all():
                if preavis.service_consulte_fk == self.service_consulte_fk:
                    old_preavis.append(preavis)
        return old_preavis

    def get_preavis_rendus(self):
        """ Renvoyer les préavis validés/rendus """
        return self.avis_enfant.filter(etat='rendu')

    def tous_preavis_rendus(self):
        """ Renvoyer si tous les préavis en cours sont rendus """
        validated = self.get_preavis_rendus().count()
        return validated == self.avis_enfant.all().count()

    def get_nb_preavis(self):
        """ Renvoyer le nombre de préavis générés """
        return self.avis_enfant.all().count()

    def get_nb_preavis_a_rendre(self):
        """ Renvoyer le nombre de préavis non rendus/validés """
        return self.get_nb_preavis() - self.get_preavis_rendus().count()

    def get_manifestation(self):
        """ Renvoyer l'événement sportif pour cet avis """
        return self.instruction.manif

    def get_instance(self):
        """ Renvoyer l'instance du département de la manifestation de l'instruction """
        return self.instruction.get_instance()

    def is_avis_cdsr(self):
        """ Renvoyer True si c'est un avis du service CDSR """
        return self.service_consulte_fk.is_service_cdsr()
    # Action
    @transition(field='etat', source='*', target='rendu')
    def rendreAvis(self, expediteur=None):
        """ Notifier l'avis rendu """
        # Notifier les instructeurs et les agents
        from messagerie.models.message import Message
        self.agent = expediteur  # Change l'agent assigné à l'avis
        destinataires = self.instruction.get_instructeurs_avec_services()
        self.historique_circuit_json = self.historique_circuit_json + [{
            "entrant": self.service_consulte_fk.pk,
            "sortant": self.service_demandeur_fk.pk,
            "type": "rendre",
            "date": str(timezone.now())}]
        self.save()
        type_msg = "info_suivi"
        # Si c'est un préavis qui est rendu
        if self.avis_parent_fk:
            # Envoi d'un message aux agents du service demandeur
            destinataires = []
            for agent in self.service_demandeur_fk.get_users_list():
                destinataires.append([agent, self.service_demandeur_fk])
            a_rendre = self.avis_parent_fk.get_nb_preavis_a_rendre() - 1  # Le préavis passera à l'état rendu au retour de la méthode
            if a_rendre == 0:
                titre = 'Tous les préavis sont rendus'
                contenu = titre + ' au sujet de la manifestation ' + self.get_manifestation().nom + '.'
            else:
                titre = 'Préavis rendu (' + str(a_rendre) + ' en attente)'
                contenu = titre + ' au sujet de la manifestation ' + self.get_manifestation().nom + '. L\'avis peut être soumis.'
            type_msg = 'action' if a_rendre == 0 else "info_suivi"
        # S'il s'agit d'un avis
        else:
            # Envoi d'un message aux agents des services consultés
            for service in self.service_consulte_history_m2m.all():
                for agent in service.get_users_list():
                    destinataires.append([agent, service])
            # ajout des agents local des preavis concerné
            for preavis in self.avis_enfant.all():
                for agent in preavis.service_consulte_fk.get_users_list():
                    destinataires.append([agent, preavis.service_consulte_fk])
            titre = "Avis rendu à la préfecture"
            contenu = titre + ' pour la manifestation ' + str(self.get_manifestation())
        Message.objects.creer_et_envoyer(type_msg, [self.agent, self.service_consulte_fk], destinataires, titre,
                                         contenu, manifestation_liee=self.get_manifestation(),
                                         objet_lie_nature="avis", objet_lie_pk=self.pk)

    @transition(field='etat', source=['demandé', 'transmis', 'distribué', 'brouillon'], target='distribué')
    def log_distribution(self, origine):
        """ Distribuer les préavis """
        # Méthode conservée pour la transition d'état
        pass

    def adresserAvis(self, user, service, service_cible):
        """ adresser à un service l'avis """
        self.service_consulte_fk = service_cible
        self.service_consulte_history_m2m.add(service)
        self.agent = None  # le but est de faire en sorte de mettre l'avis dans la colonne à traiter du tableau de bord
        self.historique_circuit_json = self.historique_circuit_json + [{"entrant": service.pk,
                                                                        "sortant": service_cible.pk,
                                                                        "type": "adresser",
                                                                        "date": str(timezone.now())}]
        self.save()
        service_cible = ServiceConsulte.objects.get(pk=service_cible)
        from messagerie.models.message import Message
        titre = 'Avis à valider et à envoyer'
        contenu = titre + ' pour la manifestation ' + self.get_manifestation().nom
        recipients = []
        for agent in service_cible.get_users_list():
            recipients.append([agent, service_cible])
        Message.objects.creer_et_envoyer('action', [user, service], recipients, titre,
                                         contenu, manifestation_liee=self.get_manifestation(),
                                         objet_lie_nature="avis", objet_lie_pk=self.pk)


    def mandater_avis(self, origine, service_origine, service_destinataire):
        """
        Notifier lorsque le traitement d'un avis est mandaté
        """

        self.service_consulte_history_m2m.add(service_origine)
        self.service_consulte_fk = service_destinataire
        self.historique_circuit_json = self.historique_circuit_json + [{"entrant": service_origine.pk,
                                                                        "sortant": service_destinataire.pk,
                                                                        "type": "mandater",
                                                                        "date": str(timezone.now())}]
        self.save()
        from messagerie.models.message import Message
        titre = 'Avis mandaté'
        contenu = titre + ' pour la manifestation ' + self.get_manifestation().nom
        recipients = []
        for agent in service_destinataire.get_users_list():
            recipients.append([agent, service_destinataire])
        Message.objects.creer_et_envoyer('action', [origine, service_origine], recipients, titre,
                                         contenu, manifestation_liee=self.get_manifestation(),
                                         objet_lie_nature="avis", objet_lie_pk=self.pk)

    def notifier_creation_avis(self, origine, agents):
        """
        Consigner l'événement et envoyer un message aux agents lorsque l'avis est créé (lors de la demande d'avis)

        :param origine: service initiateur
        :param agents: agents qui reçoivent le mail et sont notifiés
        :param non_agent_recipient: un objet avec un champ email, ex. Préfecture etc. qui sera notifié par mail
        """
        from messagerie.models.message import Message
        if self.avis_parent_fk:
            titre = "Demande de préavis à traiter"
        else:
            titre = "Demande d'avis à traiter"
        contenu = titre + ' pour la manifestation ' + self.get_manifestation().nom
        Message.objects.creer_et_envoyer('action', origine, agents, titre, contenu,
                                         manifestation_liee=self.get_manifestation(),
                                         objet_lie_nature="avis", objet_lie_pk=self.pk)

    def notifier_suppression_avis(self, origine, agents):
        """
        Envoyer un mail lors de la suppression d'un avis

        :param origine:
        :param agents:
        :param non_agent_recipient:
        :return:
        """
        from messagerie.models.message import Message
        if self.avis_parent_fk:
            titre = "Suppression de la demande de préavis"
        else:
            titre = "Suppression de la demande d'avis"
        contenu = titre + ' pour la manifestation ' + str(self.get_manifestation())
        Message.objects.creer_et_envoyer('info_suivi', origine, agents, titre, contenu,
                                         manifestation_liee=self.get_manifestation(),
                                         objet_lie_nature="avis")

    def notifier_ajout_pj(self, origine, pj):
        """ Notifier de l'ajout d'une pièce jointe """
        from messagerie.models.message import Message
        # Si c'est un préavis
        if self.avis_parent_fk:
            services = []
            services.append(self.avis_parent_fk.service_consulte_fk)
            [services.append(service) for service in self.avis_parent_fk.service_consulte_history_m2m.all()]
            destinataires = []
            for service in services:
                [destinataires.append([user, service]) for user in service.get_users_qs() ]
            titre = 'Pièce jointe ajoutée à un préavis'
        else:
            destinataires = self.instruction.get_instructeurs_avec_services()
            titre = "Pièce jointe ajoutée à un avis"
        contenu = f'{titre[:12]} {pj} {titre[13:]} au sujet de la manifestation {self.get_manifestation().nom}'
        Message.objects.creer_et_envoyer('info_suivi', origine, destinataires, titre, contenu,
                                         manifestation_liee=self.get_manifestation(),
                                         objet_lie_nature="avis", objet_lie_pk=self.pk)

    def set_date_limite_reponse(self):
        if self.avis_parent_fk:
            date_limite_reponse_dt = timezone.now() + timezone.timedelta(self.service_consulte_fk.delai_jour_preavis_int)
            if date_limite_reponse_dt > self.instruction.manif.date_debut:
                date_limite_reponse_dt = self.instruction.manif.date_debut - timezone.timedelta(days=2)
        else:
            date_limite_reponse_dt = timezone.now() + timezone.timedelta(self.service_consulte_fk.delai_jour_avis_int)
            if date_limite_reponse_dt > self.instruction.manif.date_debut:
                date_limite_reponse_dt = self.instruction.manif.date_debut - timezone.timedelta(days=5)
        if date_limite_reponse_dt < timezone.now():
            date_limite_reponse_dt = timezone.now()
        self.date_limite_reponse_dt = date_limite_reponse_dt

    def exist_document_officiel(self):
        from instructions.models.instruction import DocumentOfficiel
        avis = self
        if avis.document_attache and avis.etat == 'rendu':
            return DocumentOfficiel.objects.filter(fichier=avis.document_attache).exists()
        else:
            return False

    # Meta
    class Meta:
        verbose_name = "Avis"
        verbose_name_plural = "Avis"
        ordering = ["-date_reponse"]
        app_label = 'instructions'
        default_related_name = 'avis'


class PieceJointeAvis(models.Model):
    """
    Docuements complémentaires des avis
    """
    fichier = models.FileField(upload_to=UploadPath('avis'), verbose_name='Pièce jointe avis', max_length=512, validators=[file_type_validator])
    scan_antivirus = models.BooleanField('Scan antivirus effectué', default=True)
    avis = models.ForeignKey("Avis", verbose_name='avis', on_delete=models.CASCADE, blank=True, null=True)
    # auto_now_add n'est pas adapté quand des entrées existent dans la DB
    # Pour lancer makemigrations
    date_depot = models.DateTimeField("date de dépot", null=True, editable=False, blank=True)
    # date_depot = models.DateTimeField("date de dépot", auto_now_add=True)

    def __str__(self):
        if self.avis.avis_parent_fk:
            return self.fichier.name.split('/')[-1] + " déposé pour le préavis : " + self.avis.__str__()
        else:
            return self.fichier.name.split('/')[-1] + " déposé pour l'avis : " + self.avis.__str__()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # creation de la vignette du fichier
        creation_thumbnail_pj_avis.delay(self.pk)

    def get_instance(self):
        """ Renvoyer l'instance du département de la manifestation de l'instruction """
        return self.avis.get_manifestation().get_instance()

    def exist_document_officiel(self):
        from instructions.models.instruction import DocumentOfficiel
        return DocumentOfficiel.objects.filter(fichier=self.fichier).exists()

    def exist_piece_jointe(self):
        if self.avis:
            if self.avis.avis_parent_fk.docs.filter(fichier=self.fichier):
                return True
        return False


    class Meta:
        verbose_name = 'Document complémentaire d\'un avis'
        verbose_name_plural = "Documents complémentaires d\'un avis"
        app_label = 'instructions'
        default_related_name = 'docs'
