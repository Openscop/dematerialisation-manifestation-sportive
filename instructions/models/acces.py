from django.db import models
from django.conf import settings


class AutorisationAcces(models.Model):
    """
    Classe qui permet de donner l'accès aux avis pour les CIS et les Brigades notifiées et
    plus généralement, un accès aux manifestations à un service au sens général (structure comprise)

    """

    date_creation = models.DateField("Date de création", blank=True, null=True)
    date_derniere_consult = models.DateField("Date de la dernière consultation", blank=True, null=True)
    date_revocation = models.DateField("Date de révocation", blank=True, null=True)
    # User qui créer l'autorisation d'accès
    createur = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='initiateur', on_delete=models.CASCADE)
    organisation_origine_fk = models.ForeignKey('structure.Organisation', blank=True, null=True, on_delete=models.CASCADE,
                                                related_name="creation_acces", verbose_name="Organisation")
    # Service autorisé
    organisation = models.ForeignKey('structure.Organisation', blank=True, null=True, on_delete=models.CASCADE,
                                     related_name="autorisation_acces", verbose_name="Organisation")

    # Accès sur la manifestation seule ou avec une instruction, un avis ou un préavis
    manif = models.ForeignKey("evenements.manif", verbose_name="manif", on_delete=models.CASCADE)
    instruction = models.ForeignKey("instructions.instruction", verbose_name="instruction", blank=True, null=True, on_delete=models.CASCADE)
    avis = models.ForeignKey("instructions.avis", verbose_name="avis", blank=True, null=True, on_delete=models.CASCADE)

    # Overrides
    def __str__(self):
        des = "Accès à " + str(self.manif)
        if self.avis:
            des = des + " , avis : " + str(self.avis)
        return des + " pour " + str(self.organisation)

    def get_categorie(self):
        if self.organisation:
            if self.organisation.is_structure_organisatrice():
                return 'StructureOrganisatrice'
            else:
                return self.organisation.type_service_fk.categorie_fk
        return "-"

    def get_nature(self):
        if self.instruction or  self.avis:
            return "dossier avec avis"
        return "dossier uniquement"

    def save(self, *args, **kwargs):
        """ Sauvegarde les autorisations d'acces. """
        new_acces = self.pk is None
        # S'il s'agit d'une création
        if new_acces:
            # On vérifie qu'il n'y a de d'accès existant pour les mêmes manif et service
            if not AutorisationAcces.objects.filter(manif=self.manif, date_revocation__isnull=True, organisation=self.organisation).exists():
                super().save(*args, **kwargs)
        else:
            super().save(*args, **kwargs)

    # Meta
    class Meta:
        verbose_name = 'autorisation d\'accès'
        verbose_name_plural = 'autorisations d\'accès'
        default_related_name = "acces"
        app_label = "instructions"
