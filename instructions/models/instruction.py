from django.db import models
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from django_fsm import FSMField, transition
from django.utils import timezone
from django.urls import reverse
from django.conf import settings

from evenements.models.manifestation import ManifRelatedModel
from configuration.directory import UploadPath
from ..models import Avis
from core.tasks import creation_thumbnail_doc_officiel
from core.FileTypevalidator import file_type_validator
from structure.models.service import ServiceInstructeur, ServiceConsulte


class InstructionQuerySet(models.QuerySet):
    """ Queryset des instructions d'événements sportifs"""

    def termine(self):
        """ Renvoyer les instructions pour lesquelles l'événement prévu est terminé """
        return self.filter(manif__date_fin__lt=timezone.now())

    def closest_first(self):
        """ Renvoyer les instructions, triées par date de l'événement croissante """
        return self.order_by('manif__date_debut')

    def last_first(self):
        """ Renvoyer les instructions, triées par date de l'événement décroissante """
        return self.order_by('-manif__date_debut')

    def par_instance(self, request=None, instances=None):
        """
        Renvoyer les avis pour l'utilisateur ou les instances

        :param request: requête HTTP
        :param instances: liste d'instances
        :type instances: list<core.instance>
        """
        if request and not request.user.is_anonymous:
            # Renvoyer les avis pour l'utilisateur s'il est renseigné
            return self.filter(manif__instance=request.user.get_instance())
        elif instances:
            # Renvoyer les avis pour les instances si elles sont renseignées
            return self.filter(manif__instance__in=instances)


class Instruction(ManifRelatedModel):
    """
    Dossier d'instruction d'une demande de manifestation sportive
    """

    # Champs
    etat = FSMField(default='demandée', verbose_name="état de l'instruction")  # Voir liste des états dans : documentation/Version-4_description.md
    date_creation = models.DateField("date de création", auto_now_add=True)
    date_consult = models.DateField("date d'envoi des demandes d'avis", blank=True, null=True)
    date_derniere_action = models.DateTimeField('Date de dernière action', blank=True, null=True)
    manif = models.ForeignKey('evenements.manif', related_name="instruction", verbose_name="manifestation associée", on_delete=models.CASCADE)
    referent = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name="referent", verbose_name='instructeur référent', on_delete=models.SET_NULL)

    instance = models.ForeignKey('core.Instance', verbose_name="instance", null=True, blank=True, on_delete=models.SET_NULL)
    instruction_principale = models.ForeignKey("self", related_name="instruction_secondaire", null=True, blank=True, verbose_name="Instruction principale", on_delete=models.SET_NULL)

    service_instructeur_m2m = models.ManyToManyField('structure.ServiceInstructeur', verbose_name="Services Instructeur", related_name="instructions")
    instruction_prefecture = models.BooleanField(default=False, verbose_name="Instruction en préfécture (champ technique)")

    doc_verif = models.BooleanField("Documents en annexe vérifiés", default=False)

    objects = InstructionQuerySet.as_manager()

    def __str__(self):
        return self.manif.nom

    def save(self, set_instru=None, *args, **kwargs):
        instruction = super(Instruction, self).save(*args, **kwargs)
        if not set_instru:
            self.set_service_instructeurs()
        return instruction

    def set_service_instructeurs(self, service_instructeur=None):
        """
        Enregistrement en cache des services instructeurs pouvant instruire cette manifestion
        Le param service_instructeur, permet de rafraichir l'acces d'un service instructeur unique.
        S'il n'est pas renseigné le rafraichissement se fera pour l'ensemble des services de cette instruction
        """
        instructeurs_mairie_qs = self.get_service_mairie_qs()
        instructeurs_qs, instruction_prefecture = self.get_service_prefecture_list()
        self.instruction_prefecture = instruction_prefecture
        kwargs = {"set_instru": True}
        self.save(**kwargs)
        if service_instructeur:
            if service_instructeur in instructeurs_qs or service_instructeur in instructeurs_mairie_qs:
                self.service_instructeur_m2m.add(service_instructeur)
            else:
                self.service_instructeur_m2m.remove(service_instructeur)
        else:
            [self.service_instructeur_m2m.remove(x) for x in self.service_instructeur_m2m.all()]
            [self.service_instructeur_m2m.add(x) for x in instructeurs_qs]
            [self.service_instructeur_m2m.add(x) for x in instructeurs_mairie_qs]

    def get_absolute_url(self):
        """ Renvoyer l'URL de l'instruction """
        return reverse('instructions:instruction_detail', kwargs={'pk': self.pk})

    def get_tous_avis(self):
        """ Renvoyer les avis pour l'instruction """
        if self.manif.instance_instruites.count() == 1:
            return self.avis.filter(avis_parent_fk=None).exclude(avis_interdep_avtm_b=True).order_by('-pk')
        # interdep, gerer les avis federation:
        avis = self.avis.filter(avis_parent_fk=None).exclude(avis_interdep_avtm_b=True)
        # selectionner la catégorie fédération avec le pk
        if avis.filter(service_consulte_fk__type_service_fk__categorie_fk__pk=12):
            return avis.order_by('-pk')
        region = self.instance.departement.get_region_tuple()[0]
        avis_fedes = Avis.objects.filter(instruction__manif=self.manif,
                                         service_consulte_fk__type_service_fk__categorie_fk__pk=12)
        if not avis_fedes:
            return avis
        avis_region = Avis.objects.none()
        for avis_fede in avis_fedes:
            if region in avis_fede.service_consulte_fk.region_m2m.all():
                avis_region = Avis.objects.filter(pk=avis_fede.pk)
        avis_national = Avis.objects.none()
        if not avis_region:
            for avis_fede in avis_fedes:
                if avis_fede.service_consulte_fk.national_b:
                    avis_national = Avis.objects.filter(pk=avis_fede.pk)
        avis_qs = (avis | avis_region | avis_national)
        return avis_qs.order_by('-pk')

    def get_tous_avis_sans_cdsr(self):
        from structure.models import ServiceCDSR
        try:
            cdsr = ServiceCDSR.objects.get(instance_fk=self.get_instance())
        except ServiceCDSR.DoesNotExist:
            return self.get_tous_avis()
        return self.get_tous_avis().exclude(service_consulte_fk=cdsr)

    def get_avis_cdsr(self):
        from structure.models import ServiceCDSR
        try:
            cdsr = ServiceCDSR.objects.get(instance_fk=self.get_instance())
        except ServiceCDSR.DoesNotExist:
            return None
        if self.get_tous_avis().filter(service_consulte_fk=cdsr).exists():
            return self.get_tous_avis().filter(service_consulte_fk=cdsr).first()
        return None
    def get_avis_prefecture(self):
        prefecture = self.get_prefecture_concernee()
        avis = self.avis.filter(service_consulte_fk=prefecture)
        return avis.first() if avis else None

    def get_avis_service(self, service):
        """
        Renvoyer l'avis pour l'instruction et l'utilisateur
        L'instruction contient bien un avis concernant l'utilisateur
        """
        instance = service.instance_fk
        if self.avis.filter(Q(service_consulte_fk=service) | Q(service_consulte_history_m2m=service)).exists():
            # plusieurs avis sont maintenant possible (relance) on prendra le derniers avis
            return self.avis.filter(Q(service_consulte_fk=service) | Q(service_consulte_history_m2m=service)).order_by('-pk').first()
        return None

    def get_prefecture_concernee(self):
        """ Renvoyer la préfecture de la manif """
        if isinstance(self, Instruction):
            manif = self.get_manif()
        else:
            manif = self
        try:
            ville_depart = manif.ville_depart_interdep_mtm.get(arrondissement__departement=self.instance.get_departement())
        except ObjectDoesNotExist:
            ville_depart = manif.ville_depart
        return ville_depart.get_service_instructeur()

    def get_tous_agents_avec_service(self):
        """  Renvoyer tous les agents et agents locaux concernés par l'instruction pour l'envoi de messages"""
        recipients = []
        # Ajout des agents concernés
        for avis in Avis.objects.filter(instruction=self).order_by('pk'):
            services = avis.get_tout_services_concerne()
            for service in services:
                recipients += [user for user in service.get_users_qs()]
                recipients += [service]
        return recipients

    def get_service_mairie_qs(self):
        """ Renvoyer les mairies instructrices de l'instruction ou de la manif """
        # L'agent de mairie n'instruit que les dossiers sur une seule commune et non motorisés
        # La fonction peut être appelée par la manif dans le cas où elle n'est pas encore envoyée
        if isinstance(self, Instruction):
            manif = self.get_manif()
        else:
            manif = self
        services = ServiceInstructeur.objects.none()
        if manif.instruction_par_mairie():
            # Instruction par la mairie
            services = manif.ville_depart.organisation_set.instance_of(ServiceInstructeur)
        return services

    def get_instructeurs_mairie(self, message=False):
        """ Renvoyer les agents mairie instructeur de l'instruction """
        # L'agent de mairie n'instruit que les dossiers sur une seule commune et non motorisés
        # La fonction peut être appelée par la manif dans le cas où elle n'est pas encore envoyée
        if isinstance(self, Instruction):
            services = self.get_service_mairie_qs()
        else:
            services = Instruction.get_service_mairie_qs(self)
        recipients = []
        for service in services:
            if message:
                [recipients.append([user, service]) for user in service.get_users_qs()]
            else:
                [recipients.append(user) for user in service.get_users_qs()]
        return recipients

    def get_service_prefecture_list(self):
        """ Renvoyer les préfectures instructrices de l'instruction ou de la manif """
        # La fonction peut être appelée par la manif dans le cas où elle n'est pas encore envoyée
        if isinstance(self, Instruction):
            manif = self.get_manif()
            prefectures = [self.get_prefecture_concernee()]
        else:
            manif = self
            prefectures = [Instruction.get_prefecture_concernee(self)]
        instance = self.instance
        prefecture_principale = list(ServiceInstructeur.objects.filter(
            type_service_fk__nom_s="Préfecture", arrondissement_m2m__departement=instance.departement))
        instruction_prefecture = False
        if self.instance.instruction_mode == self.instance.IM_CIRCUIT_B:
            # Instruction par arrondissement : assuré par "get_prefecture_concernee"
            pass
        elif self.instance.instruction_mode == self.instance.IM_CIRCUIT_C:
            if manif.sur_plusieurs_arrondissement() or manif.instance_instruites.count() > 1:
                # Instruction par la préfecture principale
                instruction_prefecture = True
                prefectures = prefecture_principale
        elif self.instance.instruction_mode == self.instance.IM_CIRCUIT_D:
            if manif.sur_plusieurs_arrondissement() or manif.instance_instruites.count() > 1 or manif.get_type_manif() in ['dvtm', 'avtm', 'dvtmcir']:
                # Instruction par la préfecture principale
                instruction_prefecture = True
                prefectures = prefecture_principale
        elif self.instance.instruction_mode == self.instance.IM_CIRCUIT_E:
            if manif.sur_plusieurs_arrondissement() or manif.instance_instruites.count() > 1 or manif.get_type_manif() == "avtm":
                # Instruction par la préfecture principale
                instruction_prefecture = True
                prefectures = prefecture_principale
        if self.instance.acces_arrondissement or self.instance.instruction_mode == self.instance.IM_CIRCUIT_A:
            # Accès libre pour tous les services instructeurs d'arrondissement ou pour un seul en circuit A
            prefectures = list(ServiceInstructeur.objects.filter(arrondissement_m2m__departement=instance.departement))
        return prefectures, instruction_prefecture

    def get_instructeurs_prefecture(self, message=False):
        """
        Renvoyer les instructeurs de préfecture de l'instruction
        :param message: formatte la liste pour la messagerie
        """
        # La fonction peut être appelée par la manif dans le cas où elle n'est pas encore envoyée
        if isinstance(self, Instruction):
            prefectures, x = self.get_service_prefecture_list()
        else:
            prefectures, x = Instruction.get_service_prefecture_list(self)
        recipients = []
        for prefecture in prefectures:
            if message:
                [recipients.append([user, prefecture]) for user in prefecture.get_users_qs()]
            else:
                [recipients.append(user) for user in prefecture.get_users_qs()]
        return recipients

    def get_instructeurs(self):
        """ Renvoyer les instructeurs de l'instruction """
        recipients = self.get_instructeurs_mairie()
        if not recipients:
            recipients = self.get_instructeurs_prefecture()
        return recipients

    def get_instructeurs_avec_services(self):
        """ Renvoyer la liste des instructeurs de l'instruction formattée pour l'envoi de message """
        recipients = self.get_instructeurs_mairie(message=True)
        if not recipients:
            recipients = self.get_instructeurs_prefecture(message=True)
        return recipients

    def get_nb_avis_rendus(self):
        """ Renvoyer le nombre d'AVIS rendus """
        nb_avis = self.get_tous_avis().filter(etat='rendu').count()
        return nb_avis

    def get_nb_avis(self):
        """ Renvoyer le nombre d'avis """
        nb_avis = self.get_tous_avis().count()
        return nb_avis

    def avis_tous_rendus(self):
        """ Renvoyer si tous les avis sont rendus """
        return not self.get_tous_avis().exclude(etat='rendu').exists()

    def get_nb_avis_non_rendus(self):
        """ Renvoie le nombre d'avis non encore rendus """
        return self.get_nb_avis() - self.get_nb_avis_rendus()

    def get_instance(self):
        """
        Renvoyer l'instance de la manifestation

        (la méthode est aussi utilisée de façon générique pour l'upload de fichiers)
        """
        return self.instance

    def notifier_creation(self):
        """ Notifier les instructeurs et la préfecture quand l'instruction est créée """
        destinataires = self.get_instructeurs_avec_services()
        from messagerie.models.message import Message
        titre = "Nouveau dossier à instruire"
        data = {
            "manif": self.manif,
            "url": reverse("evenements:manif_url", kwargs={'pk': self.manif.pk}),
            "structure": self.manif.structure_organisatrice_fk,
            "user": self.manif.declarant,
        }
        contenu = render_to_string('messagerie/mail/message_organisateur_creation_instruction.txt', data)
        Message.objects.creer_et_envoyer('action', [self.manif.declarant, self.manif.structure_organisatrice_fk], destinataires,
                                         titre, contenu, manifestation_liee=self.manif, objet_lie_nature="dossier")

    def notifier_consultation(self, service=None):
        """ Notifier lorsque les premières demandes d'avis sont envoyées """
        destinataires = self.manif.structure_organisatrice_fk.get_utilisateurs_avec_service()
        from messagerie.models.message import Message
        titre = "Démarrage de l'instruction"
        contenu = 'Ce message vous informe que le service instructeur a débuté le traitement de votre dossier pour la manifestation ' + self.manif.nom + \
                  '.<br>Vous pouvez suivre l\'avancement dans le bandeau de résumé du dossier.'
        Message.objects.creer_et_envoyer('info_suivi', [self.referent, service], destinataires, titre,
                                         contenu, manifestation_liee=self.manif, objet_lie_nature="dossier")

    def notifier_ajout_document_officiel(self, user):
        """ Notifier lorsque un document officiel est ajouté """
        destinataires = self.manif.structure_organisatrice_fk.get_utilisateurs_avec_service()
        # Interdep
        if self.manif.get_type_manif() in ['avtm'] and self.manif.instance_instruites.count() > 1:
            for instruction in self.manif.instruction.all().order_by('pk'):
                if not instruction.instruction_principale:
                    destinataires += instruction.get_instructeurs_avec_services()
                destinataires += instruction.manif.get_lecture_user()
                destinataires += instruction.get_tous_agents_avec_service()
        else:
            destinataires += self.get_instructeurs_avec_services()
            destinataires += self.get_tous_agents_avec_service()
            destinataires += self.manif.get_lecture_user()
        for acces in self.manif.acces.all():
            destinataires += acces.organisation.get_utilisateurs_avec_service()
        if user in destinataires:
            destinataires.remove(user)
        natureDocument = self.documents.last().nature
        if natureDocument == 6:
            titre = "Document officiel publiée"
        elif natureDocument == 5:
            titre = "Notice d'information RD publiée"
        elif natureDocument == 4:
            titre = "Déclaration d'annulation publiée"
        elif natureDocument == 3:
            titre = "Récépissé de déclaration publié"
        elif natureDocument == 2:
            titre = "Arrêté de circulation publié"
        elif natureDocument == 1:
            titre = "Arrêté d'autorisation publié"
        else:  # cas 0
            titre = "Arrêté d'interdiction publié"

        from messagerie.models.message import Message
        contenu = titre + ' pour la manifestation ' + self.manif.nom
        if natureDocument == 3:
            objet_lie_nature = "recepisse"
        elif natureDocument == 5:
            objet_lie_nature = "notice_rd"
        elif natureDocument == 6:
            objet_lie_nature = "document"
        else:
            objet_lie_nature = "arrete"

        Message.objects.creer_et_envoyer('info_suivi', [self.referent, self.get_prefecture_concernee()], destinataires, titre,
                                         contenu, manifestation_liee=self.manif, objet_lie_nature=objet_lie_nature,
                                         objet_lie_pk=self.documents.last().pk)

    def generer_avis_prefecture(self):
        if self.instruction_principale:
            avis = Avis(service_consulte_fk=self.get_prefecture_concernee(), service_demandeur_fk=self.instruction_principale.get_prefecture_concernee(),
                        service_consulte_origine_fk=self.get_prefecture_concernee(),
                        instruction=self, historique_circuit_json=[], avis_interdep_avtm_b=True)
            avis.save()

    def envoi_avis_fede(self):
        from core.models import User
        from messagerie.models import Message
        if self.manif.get_cerfa().consultFederation:
            federation = self.manif.get_federation(departement=self.instance.departement)
            if not federation:
                destinataires = self.manif.structure_organisatrice_fk.get_utilisateurs_avec_service()
                titre = "Avis fédération"
                contenu = (
                    'La plateforme ne peut pas transmettre votre dossier à la fédération délégataire.<br>'
                    'Veuillez leur transmettre manuellement votre dossier afin de respecter le code du sport.')
                Message.objects.creer_et_envoyer('action', None, destinataires, titre,
                                                 contenu, manifestation_liee=self.manif,
                                                 objet_lie_nature="dossier")
                users = User.objects.filter(
                    groups__name__icontains='Administrateurs d\'instance').filter(
                    default_instance=self.instance)
                destinataires = []
                [destinataires.append((user, user.get_first_service())) for user in users]
                titre = "Absence fédération pour avis"
                contenu = (f"La plateforme n'a pas généré de demande d'avis fédération pour le dossier "
                           f"#{self.manif.pk} en instruction.<br>"
                           f"Il n'y a pas de fédération pour la discipline {self.manif.activite.discipline}")
                Message.objects.creer_et_envoyer('action', None, destinataires, titre,
                                                 contenu, manifestation_liee=self.manif,
                                                 objet_lie_nature="dossier")
            elif not Avis.objects.filter(service_consulte_fk=federation).filter(instruction__manif=self.manif):
                service_manif = ServiceConsulte.objects.get(type_service_fk__nom_s="Manifestation sportive")
                historique_circuit_json = [{"entrant": service_manif.pk,
                                            "sortant": federation.pk,
                                            "type": "creer",
                                            "date": str(timezone.now())}]
                avis = Avis.objects.create(service_consulte_fk=federation, instruction=self,
                                           service_consulte_origine_fk=federation, service_demandeur_fk=service_manif,
                                           historique_circuit_json=historique_circuit_json)
                avis.notifier_creation_avis(origine=None, agents=avis.get_agents_avec_service())

    @transition(field=etat, source=['demandée', 'distribuée', 'interdite'], target='distribuée')
    def envoyerDemandeAvis(self, service_demandeur):
        """ envoi de la première demande(s) d'avis """
        service = service_demandeur
        if service is not None:
            if self.etat == 'demandée':
                self.notifier_consultation(service)
                self.date_consult = timezone.now()
                self.save()

    @transition(field=etat, source='*', target='autorisée')
    def ajouter_autorisation(self, user):
        """ Changer l'état de l'instruction avant l'appel de la méthode générique"""
        self.notifier_ajout_document_officiel(user)

    @transition(field=etat, source='*', target='interdite')
    def ajouter_interdiction(self, user):
        """ Changer l'état de l'instruction avant l'appel de la méthode générique"""
        self.notifier_ajout_document_officiel(user)

    @transition(field=etat, source='*', target='annulée')
    def ajouter_annulation(self, user):
        """ Changer l'état de l'instruction avant l'appel de la méthode générique"""
        self.notifier_ajout_document_officiel(user)

    # Meta
    class Meta:
        verbose_name = "instruction de manifestation sportive"
        verbose_name_plural = "instructions de manifestations sportives"
        default_related_name = 'instruction'
        app_label = 'instructions'


class DocumentOfficiel(models.Model):
    """
    Documents officiels liés à l'instruction
    """
    NATURE_ICON = (
        (0, 'interdite text-danger iconex2'),
        (1, 'arrete text-sucess iconex2'),
        (2, 'arrete-circul text-warning iconex2'),
        (3, 'recepisse text-sucess iconex2'),
        (4, 'annulee text-secondary iconex2'),
        (5, 'notice_rd text-secondary iconex2'),
        (6, 'document_officiel text-secondary iconex2'),
    )

    NATURE_CHOICE = (
        (9, '--------'),
        (0, 'Arrêté d\'interdiction'),
        (1, 'Arrêté d\'autorisation'),
        (2, 'Arrêté de circulation'),
        (3, 'Récépissé de déclaration'),
        (4, 'Déclaration d\'annulation'),
        (5, 'Notice d\'information RD'),
        (6, 'Autre document'),
    )

    fichier = models.FileField(upload_to=UploadPath('officiels'), blank=True, null=True, verbose_name="document officiel", max_length=512, validators=[file_type_validator])
    scan_antivirus = models.BooleanField('Scan antivirus effectué', default=True)
    date_depot = models.DateTimeField("date de dépot", auto_now_add=True)
    utilisateur = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='utilisateur', null=True, on_delete=models.SET_NULL)
    instruction = models.ForeignKey("Instruction", verbose_name="instruction", on_delete=models.CASCADE)
    nature = models.SmallIntegerField("nature du document déposé", choices=NATURE_CHOICE)

    def __str__(self):
        return self.fichier.name.split('/')[-1]

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # creation de la vignette du fichier
        creation_thumbnail_doc_officiel.delay(self.pk)

    def get_nature_icon(self):
        return dict(self.NATURE_ICON)[self.nature]

    def get_nature_nom(self):
        return dict(self.NATURE_CHOICE)[self.nature]

    def get_instance(self):
        """
        Renvoyer l'instance du documents

        (la méthode est aussi utilisée de façon générique pour l'upload de fichiers)
        """
        return self.instruction.manif.get_instance()

    # Meta
    class Meta:
        verbose_name = "document officiel de l'instruction"
        verbose_name_plural = "documents officiels de l'instruction"
        default_related_name = 'documents'
        app_label = 'instructions'
