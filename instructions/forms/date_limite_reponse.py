from django.utils import timezone
from django import forms
from django.urls import reverse
from crispy_forms.helper import FormHelper
from bootstrap_datepicker_plus import DateTimePickerInput

from instructions.models import Avis
from crispy_forms.layout import Layout, Fieldset, HTML

# Date par défaut de limite de réponse à l'avis ou préavis : précédente date limite
DEFAULT_START = timezone.now()
# Inversion jour et mois pour datepicker4
DEFAULT_START_STR = DEFAULT_START.strftime('%m/%d/%Y 8:00')


class DateLimiteForm(forms.ModelForm):

    date_limite_reponse_dt = forms.DateTimeField(label='', widget=DateTimePickerInput(
        options={"format": "DD/MM/YYYY HH:mm", 'defaultDate': DEFAULT_START_STR, 'locale': 'fr'},
        attrs={"class": "datetime_picker_ar"}))

    class Meta:
        model = Avis
        fields = ('date_limite_reponse_dt',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'formDateLimite'
        self.helper.form_class = 'formDateModal'
        self.helper.form_action = reverse('instructions:date_limite_reponse_edit', kwargs={'pk': self.instance.pk})
        self.helper.layout = Layout(
            Fieldset('', 'date_limite_reponse_dt'),
            HTML('<div style="display: none" id="message-erreur-date"></div>'
                 '<div class="btn-container mt-2">'
                 '<input class="btn btn-warning submit_formDate" '
                 'data="#formDateLimite" type="submit" value="Enregistrer">'
                 '<input class="btn btn-secondary" data-bs-dismiss="modal" value="Annuler">'
                 '</div>'))

    def clean(self):
        cleaned_data = super().clean()
        limit_date_response = cleaned_data['date_limite_reponse_dt']
        if limit_date_response < timezone.now():
            message = "La date limite de réponse ne peut être antérieure à aujourd'hui."
            self.add_error("date_limite_reponse_dt", self.error_class([message]))
        elif self.instance.avis_parent_fk and limit_date_response > self.instance.instruction.manif.date_debut - timezone.timedelta(days=2):
            message = "La date limite de réponse d'un préavis ne peut être supérieure à 2 jours avant le début de la manifestation."
            self.add_error("date_limite_reponse_dt", self.error_class([message]))
        elif self.instance.avis_parent_fk is None and limit_date_response > self.instance.instruction.manif.date_debut - timezone.timedelta(days=5):
            message = "La date limite de réponse d'un avis ne peut être supérieure à 5 jours avant le début de la manifestation."
            self.add_error("date_limite_reponse_dt", self.error_class([message]))
