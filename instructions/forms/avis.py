# coding: utf-8
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout, HTML
from django import forms
from django.shortcuts import reverse

from instructions.models.avis import Avis
from core.forms.base import GenericForm


class AvisRedigerForm(GenericForm):
    """ Formulaire de rendu d'avis (acknowledge) """

    prescriptions = forms.CharField(
        label="Contenu", required=False,
        widget=forms.Textarea(
            attrs={'placeholder': 'Précisez ici vos commentaires, prescriptions, recommandations...'}))
    save = forms.BooleanField(widget=forms.widgets.HiddenInput(), required=False, initial=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'favorable',
            'prescriptions',
            'save',
            FormActions(
                HTML('<a class="btn btn-primary me-4", id="record", href="#"><i class="enregistrer"></i> Enregistrer</a>'),
                )
        )

    class Meta:
        model = Avis
        fields = ['favorable', 'prescriptions', 'save']


class PreAvisRedigerForm(GenericForm):
    """ Formulaire de rédaction de préavis (acknowledge) """

    prescriptions = forms.CharField(
        label="Contenu", required=False,
        widget=forms.Textarea(
            attrs={'placeholder': 'Précisez ici vos commentaires, prescriptions, recommandations...'}))
    favorable = forms.BooleanField(label="Préavis favorable ?", required=False, initial=False, help_text="Si coché, le préavis rendu est considéré favorable")
    save = forms.BooleanField(widget=forms.widgets.HiddenInput(), required=False, initial=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'favorable',
            'prescriptions',
            'save',
            FormActions(
                HTML(
                    '<a class="btn btn-primary me-4", id="record", href="#"><i class="enregistrer"></i> Enregistrer</a>'),
                )
        )

    class Meta:
        model = Avis
        fields = ['favorable', 'prescriptions', 'save']


class AvisAdresserForm(GenericForm):
    """ Formulaire de soumission d'avis """

    services_concerne = forms.ModelChoiceField(queryset=None, label="Service concerné")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.form_class = 'form-inline'
        self.helper.form_action = reverse('instructions:avis_adresser', kwargs={'pk': self.instance.pk})
        self.helper.label_class = "col-12"
        self.helper.field_class = "col-12"
        self.helper.layout = Layout(
            'services_concerne',
            FormActions(
                HTML('<div class="d-flex">'),
                HTML('<button style="width: 70px; height: 30px; line-height: 1;" type="submit"'
                     'class="btn btn-success me-2 ms-2 d-flex justify-content-center submit-inline-avis-form">'
                     '<i style="color: white" class="fas fa-check p-0 m-0" aria-hidden="true"></i>'
                     '</button>'),
                HTML('<span id="dismis_form" style="width: 70px; height: 30px; line-height: 1;" '
                     'class="btn btn-danger me-2 ms-2 d-flex justify-content-center">'
                     '<i style="color: white" class="fas fa-times p-0 m-0" aria-hidden="true"></i>'
                     '</span></div>'),
                ),

            )


    class Meta:
        model = Avis
        fields = []


class AvisInterrogerForm(GenericForm):
    """ Formulaire de dispatchage d'avis """

    services_concernes = forms.ModelMultipleChoiceField(queryset=None, label="Services concernés")

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.form_class = 'form-inline'
        self.fields['services_concernes'].widget.attrs = {
            'data-placeholder': "Choisissez un ou plusieurs services concernés", 'class': 'chosen-select'}
        self.helper.form_action = reverse('instructions:avis_interroger', kwargs={'pk': self.instance.pk})
        self.helper.label_class = "col-12"
        self.helper.field_class = "col-12"
        self.helper.layout = Layout(
            'services_concernes',
            FormActions(
                HTML('<div class="d-flex">'),
                HTML('<button style="width: 70px; height: 30px; line-height: 1;" type="submit"'
                     'class="btn btn-success me-2 ms-2 d-flex justify-content-center submit-inline-avis-form">'
                     '<i style="color: white" class="fas fa-check p-0 m-0" aria-hidden="true"></i>'
                     '</button>'),
                HTML('<span id="dismis_form" style="width: 70px; height: 30px; line-height: 1;" '
                     'class="btn btn-danger me-2 ms-2 d-flex justify-content-center">'
                     '<i style="color: white" class="fas fa-times p-0 m-0" aria-hidden="true"></i>'
                     '</span></div>'),
            ),
        )

    def clean(self):
        cleaned_data = super().clean()
        if "services_concernes" in cleaned_data:
            if not cleaned_data['services_concernes']:
                raise forms.ValidationError("Au moins un service doit être sélectionné")
        return cleaned_data

    # Meta
    class Meta:
        model = Avis
        fields = ['services_concernes']
        exclude = []


class AvisMandaterForm(GenericForm):
    """ Formulaire pour mandater un service pour qu'il fasse l'avis """

    service_consulte_fk = forms.ModelChoiceField(queryset=None, label="Service concerné")

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.form_class = 'form-inline'
        self.helper.form_action = reverse('instructions:avis_mandater', kwargs={'pk': self.instance.pk})
        self.helper.label_class = "col-12"
        self.helper.field_class = "col-12"
        self.helper.layout = Layout(
            'service_consulte_fk',
            FormActions(
                HTML('<div class="d-flex">'),
                HTML('<button style="width: 70px; height: 30px; line-height: 1;" type="submit"'
                     'class="btn btn-success me-2 ms-2 d-flex justify-content-center submit-inline-avis-form">'
                     '<i style="color: white" class="fas fa-check p-0 m-0" aria-hidden="true"></i>'
                     '</button>'),
                HTML('<span id="dismis_form" style="width: 70px; height: 30px; line-height: 1;" '
                     'class="btn btn-danger me-2 ms-2 d-flex justify-content-center">'
                     '<i style="color: white" class="fas fa-times p-0 m-0" aria-hidden="true"></i>'
                     '</span></div>'),
                ),
        )

    # Meta
    class Meta:
        model = Avis
        fields = ['service_consulte_fk']
        exclude = []
