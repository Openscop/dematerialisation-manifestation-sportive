# coding: utf-8
from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout, Fieldset, HTML

from core.forms.base import GenericForm
from instructions.models.instruction import Instruction, DocumentOfficiel, Avis


class InstructionForm(GenericForm):
    """ Formulaire de demande d'autorisation """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(FormActions(Submit('save', "Je valide mon dossier et je souhaite le transmettre au service instructeur maintenant.")))

    # Méta
    class Meta:
        model = Instruction
        fields = ()


class InstructionPublishForm(GenericForm):
    """ Formulaire d'ajout de document officiel par dépose de fichier """

    fichier = forms.FileField()
    nature = forms.ChoiceField(label="Nature du document déposé", choices=DocumentOfficiel.NATURE_CHOICE)

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset("Choisissez le fichier du document officiel", 'nature', 'fichier'),
            HTML(f"<span  class='btn btn-primary mt-2 px-5' id='btn-valider-doc'>Valider</span>"
                 "<div style='display: none' id='double-verification'>"
                 "<div class='alert alert-info mt-3'>"
                 "<div class=\"row\">"
                 "<div class=\"col-3 libelle\">Vous vous apprêter à déposer le fichier</div>"
                 "<div class=\"col-9\"><span id='file-name' class=\"text-danger\"></span></div>"
                 "</div>"    
                 "<div class=\"row\">"
                 "<div class=\"col-3 libelle\">de nature</div>"
                 "<div class=\"col-9\"><span id='file-nature' class=\"text-dark\"></span></div>"
                 "</div>"
                 "<div class=\"row\">"
                 "<div class=\"col-3 libelle\">sur le dossier</div>"
                 "<div class=\"col-9\"><span id='manif-name' class=\"text-dark\"></span></div>"
                 "</div>"
                 "<div class=\"row\">"
                 "<div class=\"col-3 libelle\">de l'organisateur</div>"
                 "<div class=\"col-9\"><span id='user-name' class=\"text-dark\"></span></div>"
                 "</div>"   
                 "<p class=\"my-3 ms-4\">"
                 "<span id='modification-etat'>L'état de la manifestation va passer de <span id='etat-courant' class='text-dark mx-1'></span> à <span id='nouveau-etat' class='text-dark mx-1'></span>.</span>"
                 "<span id='pas-modification-etat'>L'état de la manifestation ne sera pas impacté.</span>"
                 "</p>"
                 "<p class=\"mb-2\"><strong><i class=\"attention\"></i>Cette action est irréversible.</strong>"
                 "<span id='changement-action' class=\"ms-2\"> Ainsi, certaines actions ne pourront plus être réalisées.</span></p>"
                 "</div>"
                 ),
            HTML("<div class='d-flex text-center'>"),
            FormActions(Submit('save', "Ajouter le document officiel")),
            FormActions(HTML("<input value='Annuler' id='dismiss' class='btn btn-danger ms-3'></div>")),
            HTML("</div>"),
        )

    # Validation des champs
    def clean_fichier(self):
        """ Vérifier le champ de fichier d'arrêté """
        data = self.cleaned_data['fichier']
        if not data:
            raise forms.ValidationError("Vous devez spécifier un fichier contenant le document officiel.")
        return data

    def clean_nature(self):
        """ Vérifier le champ de fichier d'arrêté """
        data = self.cleaned_data['nature']
        if not data or data == '9':
            raise forms.ValidationError("Vous devez spécifier la nature du document officiel")
        return data

    # Meta
    class Meta:
        model = Instruction
        fields = ['fichier', 'nature']


class InstructionPublierForm(forms.ModelForm):
    """ Formulaire d'ajout de document officiel par transfert de PJ """

    nature = forms.ChoiceField(label="nature du document", choices=DocumentOfficiel.NATURE_CHOICE)

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-8'
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Fieldset("", 'nature'),
        )

    # Validation des champs
    def clean_nature(self):
        """ Vérifier le champ de fichier d'arrêté """
        data = self.cleaned_data['nature']
        if not data or data == '9':
            raise forms.ValidationError("Vous devez spécifier la nature du document officiel.")
        return data

    # Meta
    class Meta:
        model = Instruction
        fields = ['nature', ]


class InstructionAvisForm(GenericForm):
    """
    Formulaire pour ajouter l'avis de la cdsr dans l'instruction interdepartementale
    """

    prescriptions = forms.CharField(
        label="Contenu", required=False,
        widget=forms.Textarea(
            attrs={'placeholder': 'Précisez ici vos commentaires, prescriptions, recommandations...'}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'favorable',
            'prescriptions',
            FormActions(
                Submit('save', "rendre l'avis".capitalize())
            )
        )

    class Meta:
        model = Avis
        fields = ('favorable', 'prescriptions')
