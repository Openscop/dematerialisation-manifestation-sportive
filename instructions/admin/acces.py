# coding: utf-8
from django.contrib import admin

from import_export.admin import ExportActionModelAdmin

from instructions.models.acces import AutorisationAcces


class RevocationFilter(admin.SimpleListFilter):
    title = 'autorisation révoquée'
    parameter_name = 'date_revocation'

    def lookups(self, request, model_admin):
        return ('True', 'Oui'), ('False', 'Non')

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(date_revocation__isnull=False)
        if self.value() == 'False':
            return queryset.exclude(date_revocation__isnull=True)


@admin.register(AutorisationAcces)
class InstructionAdmin(ExportActionModelAdmin, admin.ModelAdmin):
    """ Configuration admin pour les autorisatiions d'accès """

    # configuration
    model = AutorisationAcces
    list_display = ['pk', 'date_creation', 'createur', 'service_createur', 'manif', 'organisation', 'date_derniere_consult']
    list_filter = [RevocationFilter]
    readonly_fields = ['createur', 'manif', 'instruction', 'avis']
    search_fields = ['createur__username', 'manif__nom__unaccent']
    list_per_page = 25

    def service_createur(self, obj):
        return obj.createur.get_service()
    service_createur.short_description = "du service"
