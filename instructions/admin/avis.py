# coding: utf-8
from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from instructions.models import Avis, PieceJointeAvis
from .form import AvisInlineForm
from core.models import LogConsult


class PieceJointeAvisInline(admin.StackedInline):
    """ Inline PieceJointeAvis """

    # Configuration
    model = PieceJointeAvis
    extra = 0
    view_on_site = False
    readonly_fields = ('date_depot', 'fichier', 'scan_antivirus')
    exclude = ()


# class PreAvisInline(admin.StackedInline):
#     """ Inline Préavis """
#
#     # Configuration
#     model = Avis
#     extra = 0
#     view_on_site = False
#     readonly_fields = ['date_demande', 'agentlocal_name', 'pj_avis']
#     fieldsets = (
#         (None, {'fields': ('etat', ('date_demande', 'date_reponse'), 'favorable', 'prescriptions',
#                            ('agentlocal_name', 'pj_avis'))}),
#     )
#     form = AvisInlineForm
#
#     def get_queryset(self, request):
#         queryset = super().get_queryset(request)
#         return queryset.filter(avis_parent_fk__isnull=False)
#
#     def pj_avis(self, obj):
#         if obj.pk:
#             if obj.docs.all():
#                 pj = obj.docs.last()
#                 url = reverse("admin:instructions_piecejointeavis_change", args=[pj.pk])
#                 if pj.date_depot:
#                     return format_html('<a href="{}">{}</a> déposé le {}', url, pj.fichier, pj.date_depot.date().strftime('%d/%m/%Y'))
#                 else:
#                     return format_html('<a href="{}">{}</a>', url, pj.fichier)
#         return "pas de pièce jointe"
#     pj_avis.short_description = "pièce jointe"
#
#     def has_add_permission(self, request, obj=None):
#         return False
#
#     def agentlocal_name(self, obj):
#         if obj.agent:
#             return obj.agent.get_full_name()
#         return '-'
#     agentlocal_name.short_description = "Dernier intervenant"


class AvisInline(admin.StackedInline):
    """ Inline avis """

    # Configuration
    model = Avis
    extra = 0
    view_on_site = False
    readonly_fields = ['date_demande', 'agent_name', 'edition_avis', 'pj_avis']
    fieldsets = (
        (None, {'fields': (('etat', 'edition_avis'), ('date_demande', 'date_reponse'), 'favorable',
                           'prescriptions', ('agent_name', 'pj_avis'))}),
    )
    form = AvisInlineForm

    def pj_avis(self, obj):
        if obj.pk:
            if obj.docs.all():
                pj = obj.docs.last()
                url = reverse("admin:instructions_piecejointeavis_change", args=[pj.pk])
                if pj.date_depot:
                    return format_html('<a href="{}">{}</a> déposé le {}', url, pj.fichier, pj.date_depot.date().strftime('%d/%m/%Y'))
                else:
                    return format_html('<a href="{}">{}</a>', url, pj.fichier)
        return "pas de pièce jointe"
    pj_avis.short_description = "pièce jointe"

    def edition_avis(self, obj):
        if obj.pk:
            if Avis.objects.get(pk=obj.pk).avis_enfant.all():
                url = reverse("admin:instructions_avis_change", args=[obj.pk])
                return format_html('<a href="{}" class="btn">Préavis</a>', url)
        return 'Pas de préavis'
    edition_avis.short_description = "Voir les préavis"

    def agent_name(self, obj):
        if obj.agent:
            return obj.agent.get_full_name()
        return '-'
    agent_name.short_description = "Dernier intervenant"

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(Avis)
class AvisAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des avis """
    # Configuration de base
    model = Avis
    inlines = [PieceJointeAvisInline]
    # inlines = [PreAvisInline, PieceJointeAvisInline]
    readonly_fields = ['instruction', 'agent_name']
    fieldsets = (
        (None, {'fields': ('etat', 'instruction', ('date_demande', 'date_reponse'), 'favorable',
                           'prescriptions', 'agent_name')}),
    )

    def has_module_permission(self, request):
        return False

    def agent_name(self, obj):
        if obj.agent:
            return obj.agent.get_full_name()
        return '-'
    agent_name.short_description = "Dernier intervenant"

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.has_group('_support'):
            queryset = queryset.filter(instruction__manif__ville_depart__arrondissement__departement=request.user.get_departement())
        return queryset

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response


@admin.register(PieceJointeAvis)
class PieceJointeAvisAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des pièces jointes d'avis et de préavis """
    # Configuration
    model = PieceJointeAvis
    list_display = ('date_depot', 'avis', 'fichier', 'scan_antivirus')
    readonly_fields = ('date_depot', 'avis', 'fichier', 'scan_antivirus')
    search_fields = ('avis__instruction__pk',)

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}
