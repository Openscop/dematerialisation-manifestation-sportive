from django import forms
from django.forms.fields import ChoiceField


class AvisInlineForm(forms.ModelForm):
    choix = (('demandé', 'demandé'),
             ('rendu', 'rendu'),
             ('distribué', 'distribué'),
             ('formaté', 'formaté'),
             ('transmis', 'transmis'))
    etat = ChoiceField(choices=choix)

    class Meta:
        exclude = []
