function UpdateDateReponse(submit) {
    var formId = submit.attr('data') // formId : string
    var form = $(formId) // form : objet Jquery
    $.ajax({
      type: 'POST',    //La méthode cible (POST ou GET)
      url: form.attr('action'), //Script Cible
      data: form.serialize(),
      success: function (result) {
          // console.log('AJAX OK', result)
          if (result === "") {
              window.location.reload()
          } else {
              window.location.href = result
          }
      },
      error: function (result) {
          // console.log('AJAX NOK', result)
          if (result.responseText) {
              $('#message-erreur-date').html(`<div class="alert alert-danger" role="alert" >${result.responseText}</div>`).show()
          }
      }
    });
}

$(document).on('click', '.btn-date-form', function (e) {
    e.preventDefault()
    var url_form = $(this).attr('data-url-form-pk')
    var titre_form = $(this).attr('data-titre')
    $('.modal-date-title').html(titre_form)
    $.get(url_form, function (result) {
        $('.modal-body-date').html(result).show()
        $('.formDateModal').attr('action', url_form)
    })
})

$(document).on('click', '.submit_formDate', function (e){
    e.preventDefault()
    UpdateDateReponse($(this))
})

$(".modal").on("hidden.bs.modal", function(){
    $(".modal-body").html('<i class="spinner"></i>');
    AfficherIcones();
});
