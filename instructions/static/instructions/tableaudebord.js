// ce fichier n'est utilisé que dans le tableau de bord des agents et pas dans celui des organisateurs
$(document).ready(function () {
    function ligneToDEtail(){
        // Lier la ligne vers la vue de détail
        $('body').on('click', 'tr.clickligne', function () {
            // Prendre en compte l'execution des tests qui nécessite de ne pas ouvrir les dossiers dans un nouvel onglet
            if (! TESTS_IN_PROGRESS) {
                window.open ($(this).data("href"), '_blank').focus();
            } else {
                window.location=$(this).data("href");
            }
        });
    }

    var serviceconsulte = window.location.search.includes('?filtre_specifique=serviceconsulte')
    $('.btn_etat_1, .btn_etat_2, #btn_etat_all, #btn_etat_all_arrond, #btn_etat_all_dep, #btn_etat_me').click(function (e) {
        e.preventDefault();
        let url = window.location.pathname
        let instructionmairie = window.location.search.includes('?filtre_specifique=instructionmairie')
        let id = $(this).attr('id')
        let arron = null
        window.location.search.split('&').forEach(function (value) {
            if (value.indexOf('arron') !== -1){
                arron = value.replace('?', '')
            }
        })
        let departement = null
        window.location.search.split('&').forEach(function (value) {
            if (value.indexOf('dept') !== -1){
                departement = value.replace('?', '')
            }
        })
        // Reprendre l'url de base et insérer "list"
        let list_url = url.split('/');
        list_url.splice(4,0,"list");
        url = list_url.join('/');

        // Ajouter instruction mairie si dans l'url de base
        if (serviceconsulte) {
            url = url + '?filtre_specifique=serviceconsulte';
        }
        if (instructionmairie) {
            url = url + '?filtre_specifique=instructionmairie';
        }

        let btn_etat = $(this).attr('data');
        // Ajouter le filtre du blocs
        if (typeof btn_etat!=="undefined"){
            if (url.indexOf('?')=== -1) {
                // Pas de query, on crée
                url += "?filtre_etat="+btn_etat;
            } else {
                // Une query existe, on ajoute
                url += "&filtre_etat="+btn_etat;
            }
        }
        if (id === "btn_etat_all_arrond"){
           if (url.indexOf('?')=== -1) {
                // Pas de query, on crée
                url += "?filtre_specifique=tous_arrondissement"
            } else {
                // Une query existe, on ajoute
                url += "&filtre_specifique=tous_arrondissement"
            }
        }
        if (id === "btn_etat_all_dep"){
           if (url.indexOf('?')=== -1) {
                // Pas de query, on crée
                url += "?filtre_specifique=tout_departement"
            } else {
                // Une query existe, on ajoute
                url += "&filtre_specifique=tout_departement"
            }
        }
        console.log(id)
        if (id === "btn_etat_me"){
           if (url.indexOf('?')=== -1) {
                // Pas de query, on crée
                url += "?filtre_specifique=moi"
            } else {
                // Une query existe, on ajoute
                url += "&filtre_specifique=moi"
            }
        }
        if (arron){
           if (url.indexOf('?')=== -1) {
                // Pas de query, on crée
                url += `?${arron}`
            } else {
                // Une query existe, on ajoute
                url += `&${arron}`
            }
        }
        if (departement){
           if (url.indexOf('?')=== -1) {
                // Pas de query, on crée
                url += `?${departement}`
            } else {
                // Une query existe, on ajoute
                url += `&${departement}`
            }
        }
        if (id === "btn_etat_all" || id === "btn_etat_all_arrond" || id === "btn_etat_all_dep" || id === "btn_etat_me") {
            $('#btn_etat_row').hide()
        }
        $('#block_tableaubord_body').show();
        $('#block_tableaubord_consult').hide()
        $('#block_tableaubord_liste').html(html_chargement_en_cours());
        AfficherIcones()
        $.get(url, function (data) {
            // Recupérer la liste à afficher et insérer
            $('#block_tableaubord_liste').html(data);
            ligneToDEtail()


            AfficherIcones();
        })


    });

    $('.arron').on("click", function () {
        let arron = $(this).attr('id').split('_')[1];
        let parametres = window.location.search;
        if (parametres.search('arron') !== -1) {
            const regex = /arron=\d+/gi;
            parametres = parametres.replace(regex, "arron=" + arron);
        } else {
            if (parametres.indexOf('?') !== -1) {
                parametres += "&arron=" + arron;
            } else {
                parametres += "?arron=" + arron;
            }
        }
        document.location.href = window.location.origin + window.location.pathname + parametres;
    });

    $('#annee').on('change', function (e) {
        e.preventDefault()
        let annee = $(this).val()
        let parametres = window.location.search;

        $('#annee_chosen').after('<span class="text-info ms-4 attente_chargement">Recherche des archives en cours. Merci de patienter... <i class="far fa-spinner fa-spin"></i></span>');
        let url = window.location.pathname
        let list_url = url.split('/');
        list_url.splice(4,0,"list");
        url = list_url.join('/');

        if (annee !== "") {
            if (parametres.search('annee') !== -1) {
                const regex = /annee=\d+/gi;
                parametres = parametres.replace(regex, "annee=" + annee);
            } else {
                if (parametres.indexOf('?') !== -1) {
                    parametres += "&annee=" + annee;
                } else {
                    parametres += "?annee=" + annee;
                }
            }
            $.get(url+parametres, function (d){
                 $('#block_tableaubord_liste').html(d);
                 $('.attente_chargement').remove();
                 AfficherIcones();
                 ligneToDEtail()
            })
        }

    })

    $('.dept').on('click', function (e) {
        let dept = $(this).attr('id').split('_')[1];
        let parametres = window.location.search;
        if (parametres.search('dept') !== -1) {
            const regex = /dept=\d+/gi;
            parametres = parametres.replace(regex, "dept=" + dept);
        } else {
            if (parametres.indexOf('?') !== -1) {
                parametres += "&dept=" + dept;
            } else {
                parametres += "?dept=" + dept;
            }
        }
        document.location.href = window.location.origin + window.location.pathname + parametres;
    })

    $('#btn_consult').click(function (e) {
        e.preventDefault();
        $('#block_tableaubord_body').hide();
        $('#block_tableaubord_consult').html(html_chargement_en_cours());
        $('#block_tableaubord_consult').show()
        AfficherIcones();
        let parametres = window.location.search;
        let url_complete = url_consultation_liste + parametres
        $.get(url_complete, function (data) {
            $('#block_tableaubord_consult').html(data);
            AfficherIcones();
        });
    });
    $.get(url_tableaudebordajax + window.location.search, function (data){
        data = JSON.parse(data)
        if (data.lecture_count){
            $('#btn_consult').children('span').text(data.lecture_count)
            $('#btn_consult').fadeIn(500)
        }
        if (service_is_service_instructeur) {
            if (serviceconsulte) {
                $('#btn_autre_instru').html(`<i class="instructeur"></i> Instructions <span class="badge bg-primary ms-1">${data.instruction_count}</span>`)
                $('#btn_autre_instru').attr('href', window.location.pathname)
                    .attr("data-bs-original-title", "Accéder au tableau de bord des instructions")
            } else {
                $('#btn_autre_instru').html(`<i class="agent-local"></i> Demandes d'avis <span class="badge bg-primary ms-1">${data.instruction_count}</span>`)
                $('#btn_autre_instru').attr('href', `${window.location.pathname}?filtre_specifique=serviceconsulte`)
                    .attr("data-bs-original-title", "Accéder au tableau de bord des consultations")
            }
            $('#btn_autre_instru').fadeIn(500)

            // Affiche le suivi des sujets non clos pour les instructeurs
            $.get({
                url: '/forum/suivi/',
                data: { 'request': 'tdb',},
                success:
                    function (reponse) {
                        $("#topic_open_forum").html(reponse)
                    }
            })
        }
        AfficherIcones();

    })
});

