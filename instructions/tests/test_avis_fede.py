import re

from django.test import override_settings
from bs4 import BeautifulSoup as Bs

from .test_base import TestCommunClass
from instructions.models import Instruction


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class AvisFedeTests(TestCommunClass):
    """
    Test de la confirmation par la fédération des options :
        dans_calendrier
        avec_convention_fede
    avec une manif Dcnm
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('============ Avis fédé (Clt) =============')
        TestCommunClass.init_setup(cls)
        cls.agentlocal_edsr.delete()

    def test_Avis_fede(self):
        """
        Test de la confirmation par la fédération des options :
            dans_calendrier
            avec_convention_fede
        avec une manif Dcnm
        """
        """
        def print(string="", end=None):
            # Supprimer les prints hors debug
            pass
        """
        print('**** test 1 creation manif ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('1', nb_bloc.group('nb'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        reponse = self.client.get(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Une demande d\'avis va être automatiquement envoyée à votre fédération délégataire')
        self.assertContains(reponse, str(self.fede))
        if hasattr(declar, 'group'):
            self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        self.instruction = Instruction.objects.get(manif=self.manifestation)
        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().get_instance())
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="GGD"), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="EDSR"), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="DDSP"))
        print(self.manifestation.structure_organisatrice_fk, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline, end=" ; ")
        print(self.manifestation.activite.discipline.get_federations().first())
        self.affichage_avis()
        self.assertEqual(str(self.instruction), str(self.manifestation))
        self.client.logout()

        print('**** test 2 instructeur - présence manif ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        # appel ajax pour avoir la liste
        url_intr_list = "/instructions/tableaudebord/" + str(self.prefecture.pk) + "/list/"
        reponse = self.client.get(url_intr_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action_requise = str(html.select("div.border-danger.liste-actions"))
        self.assertIn('Envoyer une demande d\'avis', action_requise)
        self.client.logout()

        print('**** test 3 avis fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_fede', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.fede.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 28 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rédiger l\'avis', count=1)
        self.client.logout()

        print('**** test 4 validation fede calendrier négative ****')
        # Positionnement du booléen
        self.manifestation.dans_calendrier = True
        self.manifestation.save()
        self.instruction.delete()
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Chargement du fichier requis
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/visa_federation.txt') as openfile:
            url_file = re.search('action="(?P<url>([^"]+)).+data-name="visa_federation', reponse.content.decode('utf-8'))
            self.assertTrue(hasattr(url_file, 'group'))
            self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')
        reponse = self.client.get(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'La plateforme va vérifier votre déclaration auprès de votre fédération délégataire')
        self.assertContains(reponse, str(self.fede))
        self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.client.logout()

        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_fede', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 28 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertNotContains(reponse, 'Rédiger l\'avis')
        self.assertContains(reponse, 'Confirmer l\'inscription dans le calendrier')
        # Infirmer l'inscription avec l'url fournie et tester la redirection
        refus1 = re.search('href="(?P<url>(/[^"]+)).+test_invalider_calendrier.+\\n.+Infirmer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(refus1, 'group'))
        reponse = self.client.get(refus1.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Rédiger l\'avis', count=1)
        self.client.logout()

        print('**** test 5 validation fede calendrier positive ****')
        # Positionnement du booléen
        self.manifestation.dans_calendrier_confirme = None
        self.manifestation.save()
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_fede', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 28 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertNotContains(reponse, 'Rédiger l\'avis')
        self.assertContains(reponse, 'Confirmer l\'inscription dans le calendrier')
        # Confirmer l'inscription avec l'url fournie et tester la redirection
        ack1 = re.search('href="(?P<url>(/[^"]+)).+test_valider_calendrier.+\\n.+Confirmer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(ack1, 'group'))
        reponse = self.client.get(ack1.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Avis rendu, aucune action nécessaire')
        self.assertContains(reponse, 'inscription dans le calendrier de la fédération est confirmée')
        self.client.logout()

        print('**** test 6 validation fede convention négative ****')
        # Positionnement du booléen
        self.manifestation.dans_calendrier_confirme = None
        self.manifestation.dans_calendrier = False
        self.manifestation.avec_convention_fede = True
        self.manifestation.save()
        Instruction.objects.get(manif=self.manifestation).delete()
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Chargement du fichier requis
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/convention_federation.txt') as openfile:
            url_file = re.search('action="(?P<url>([^"]+)).+data-name="convention_federation', reponse.content.decode('utf-8'))
            self.assertTrue(hasattr(url_file, 'group'))
            self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')
        reponse = self.client.get(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'La plateforme va vérifier votre déclaration auprès de votre fédération délégataire')
        self.assertContains(reponse, str(self.fede))
        self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.client.logout()

        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_fede', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 28 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertNotContains(reponse, 'Rédiger l\'avis')
        self.assertContains(reponse, 'Confirmer l\'existance d\'une convention')
        # Infirmer l'existance avec l'url fournie et tester la redirection
        refus2 = re.search('href="(?P<url>(/[^"]+)).+test_invalider_convention.+\\n.+Infirmer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(refus2, 'group'))
        reponse = self.client.get(refus2.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Rédiger l\'avis')
        self.client.logout()

        print('**** test 7 validation fede convention positive ****')
        # Positionnement du booléen
        self.manifestation.avec_convention_fede_confirme = None
        self.manifestation.save()
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_fede', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 28 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertNotContains(reponse, 'Rédiger l\'avis')
        self.assertContains(reponse, 'Confirmer l\'existance d\'une convention')
        # Confirmer l'existance avec l'url fournie et tester la redirection
        ack2 = re.search('href="(?P<url>(/[^"]+)).+test_valider_convention.+\\n.+Confirmer', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(ack2, 'group'))
        reponse = self.client.get(ack2.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<')
        self.assertContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Avis rendu, aucune action nécessaire')
        self.assertContains(reponse, 'Non concerné : la convention avec la fédération est confirmée')
        # Vérifier le passage en rendu
        self.presence_avis('agent_fede', 'rendu', log=False)
        self.client.logout()
