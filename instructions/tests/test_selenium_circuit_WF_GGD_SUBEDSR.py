import re, time

from django.test import tag, override_settings

from selenium.webdriver.common.by import By

from .test_base_selenium import SeleniumCommunClass
from core.models import Instance
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from structure.models.service import ServiceConsulteInteraction

class InstructionCircuit_GGD_SUBEDSRTests(SeleniumCommunClass):
    """
    Test du circuit d'instance GGD_SUBEDSR avec sélénium pour une Dcnm
        circuit_GGD : Avis GGD préavis EDSR
        instruction par departement
        openrunner false par défaut
        limité à l'avis GGD pour le temps de test (pas d'avis mairie, sdis, cg et ddsp)
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('========== WF_GGD_SUBEDSR (Sel) ===========')

        cls.dep = DepartementFactory.create(name='78',
                                            instance__name="instance de test",
                                            instance__instruction_mode=Instance.IM_CIRCUIT_A)
        arrondissement = ArrondissementFactory.create(name='Versailles', code='99', departement=cls.dep)
        cls.prefecture = arrondissement.organisations.first()
        cls.commune = CommuneFactory(name='Plaisir', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)
        cls.mairie1 = cls.commune.organisation_set.first()
        cls.mairie2 = cls.autrecommune.organisation_set.first()
        ggd = cls.dep.organisation_set.get(precision_nom_s="GGD")
        ggd.porte_entree_avis_b = True
        ggd.autorise_rendre_avis_b = True
        ggd.service_parent_fk = None
        ggd.save()
        edsr = cls.dep.organisation_set.get(precision_nom_s="EDSR")
        edsr.porte_entree_avis_b = False
        edsr.service_parent_fk = ggd
        edsr.save()

        SeleniumCommunClass.init_setup(cls)
        ServiceConsulteInteraction.objects.filter(service_entrant_fk=edsr).delete()
        ServiceConsulteInteraction.objects.create(service_entrant_fk=ggd, service_sortant_fk=cls.cgd1, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=ggd, service_sortant_fk=cls.cgd2, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=ggd, service_sortant_fk=cls.usg, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=ggd, service_sortant_fk=edsr, type_lien_s='interroger')
        cls.avis_nb = 2
        super().setUpClass()

    @tag('selenium')
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
    def test_Circuit_GGD_SUBEDSR(self):
        """
        Test des différentes étapes du circuit GGD_SUBEDSR pour une DCNM
        """

        print('**** test 0 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 1 vérification avis; 0 pour tous sauf la fédération ****')
        # Vérification des avis des divers agents
        # GGD
        self.connexion('agent_ggd')
        self.presence_avis('agent_ggd', 'none')
        self.deconnexion()
        # EDSR
        self.connexion('agentlocal_edsr')
        self.presence_avis('agentlocal_edsr', 'none')
        self.deconnexion()

        print('**** test 2 instructeur - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.distribuer_avis(['Gendarmerie'])
        time.sleep(self.DELAY*2)
        # Vérifier le passage en encours et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY*3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 3 avis fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.connexion('agent_fede')
        self.presence_avis('agent_fede', 'nouveau')
        self.assertIn('Délai : 28 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_fede', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 4 vérification avis; 0 pour EDSR local, CGD et Brigade ****')
        # Vérification des avis des divers agents
        # EDSR
        self.connexion('agentlocal_edsr')
        self.presence_avis('agentlocal_edsr', 'none')
        self.deconnexion()
        # CGD
        self.connexion(self.agent_cgd1.username)
        self.presence_avis('agent_cgd', 'none')
        self.deconnexion()
        # Brigade
        self.connexion(self.agent_brg1.username)
        self.presence_avis('agent_brg', 'none')
        self.deconnexion()

        print('**** test 5 avis ggd - distribution ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ggd')
        self.presence_avis('agent_ggd', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.distribuer_preavis('EDSR')
        self.distribuer_preavis('CGD1')
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # vérification de la présence de l'événement en encours
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ggd', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        time.sleep(self.DELAY)
        self.assertIn('2 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 6 preavis edsr ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en nouveau
        self.connexion('agentlocal_edsr')
        self.presence_avis('agentlocal_edsr', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis("préavis")
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en warning
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agentlocal_edsr', 'rendu')
        self.deconnexion()
        # Vérifier le nombre de préavis manquants dans la vue GGD
        self.connexion('agent_ggd')
        self.presence_avis('agent_ggd', 'encours')
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 7 preavis cgd ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en nouveau
        self.connexion(self.agent_cgd1.username)
        self.presence_avis('agent_cgd', 'nouveau')
        self.vue_detail()
        time.sleep(self.DELAY * 3)
        print('\t >>> Informer brigade')
        self.assertIn('Octroyer un accès en lecture', self.selenium.page_source)
        # Ouvrir l'accès en lecture
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.CLASS_NAME, 'formconsult_btn').click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.ID, 'complet').click()
        time.sleep(self.DELAY)
        self.chosen_select('select_instances_modal_chosen', self.dep.instance.__str__())
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % 'Gendarmerie').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'tout_deplier').click()
        time.sleep(self.DELAY * 4)
        self.selenium.find_element(By.ID, "service-search").send_keys('BTA1')
        time.sleep(self.DELAY * 4)
        self.selenium.find_element(By.ID, f'name_container_{self.brigade1.pk}').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, self.brigade1.pk).click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, 'btn-close-modal-avis').click()
        time.sleep(self.DELAY * 3)
        print('\t >>> Rendre préavis')
        self.envoyer_avis("préavis")
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cgd', 'rendu')
        self.deconnexion()
        print('\t >>> Vérifier brigade')
        self.connexion(self.agent_brg1.username)
        self.assertIn('Accès en lecture', self.selenium.page_source)
        self.deconnexion()

        print('**** test 8 avis ggd ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en encours
        self.connexion('agent_ggd')
        self.presence_avis('agent_ggd', 'encours')
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # vérification de la présence de l'événement en rendu
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ggd', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.deconnexion()
