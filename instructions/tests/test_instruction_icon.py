import re

from django.test import override_settings
from django.utils import timezone
from django.shortcuts import reverse
from bs4 import BeautifulSoup as Bs

from .test_base import TestCommunClass
from core.models import Instance
from instructions.models import Instruction, Avis
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class Circuit_Instruction_IconTests(TestCommunClass):
    """
    Test du déroulement de l'instruction pour une Dcnm
        circuit_GGD : Avis GGD
        instruction par département
        openrunner false par défaut
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========== Instruction Icônes (Clt) ===========')
        # Création des objets sur le 78
        cls.dep = dep = DepartementFactory.create(name='78',
                                                  instance__name="instance de test",
                                                  instance__instruction_mode=Instance.IM_CIRCUIT_A)
        arrondissement = ArrondissementFactory.create(name='Versailles', code='99', departement=dep)
        cls.prefecture = arrondissement.organisations.first()
        cls.commune = CommuneFactory(name='Plaisir', arrondissement=arrondissement)
        cls.mairie1 = cls.commune.organisation_set.first()
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)
        cls.mairie2 = cls.commune.organisation_set.first()

        TestCommunClass.init_setup(cls)
        cls.avis_nb = 3

    def test_Circuit_Instruction(self):
        """
        Test du déroulement d'une instruction
        utilisation de beautifulsoup pour parser le html
        """
        """
        def print(string="", end=None):
            # Supprimer les prints hors debug
            pass
        """

        print('**** test 1 creation manif ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.vue_detail(reponse)
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        self.assertTrue(hasattr(declar, 'group'))
        self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.client.logout()

        self.instruction = Instruction.objects.get(manif=self.manifestation)
        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().get_instance())
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="GGD"), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="EDSR"), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="DDSP"))
        print(self.manifestation.structure_organisatrice_fk, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline, end=" ; ")
        print(self.manifestation.activite.discipline.get_federations().first())
        self.affichage_avis()
        self.assertEqual(str(self.instruction), str(self.manifestation))

        print('**** test 2 instructeur ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        # appel ajax pour avoir la liste
        url_intr_list = "/instructions/tableaudebord/" + str(self.prefecture.pk) + "/list/"
        reponse_tdb = self.client.get(url_intr_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')

        print('\t >> test des icones de la manifestation dans le TdB')
        self.assertContains(reponse_tdb, '<i class="declaration', count=1)
        self.assertContains(reponse_tdb, '<i class="sans-vtm', count=1)
        self.assertContains(reponse_tdb, '<i class="hors-voie-publique', count=1)
        self.assertContains(reponse_tdb, '<i class="competition', count=1)
        self.assertContains(reponse_tdb, '<i class="delai-60j', count=1)
        self.assertContains(reponse_tdb, '<i class="demandee', count=1)
        self.assertContains(reponse_tdb, '<i class="dossier-complet-non-verifie', count=1)

        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse_tdb)
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')

        print('\t >> test des icones du bandeau résumé de la manifestation')
        bandeau = str(html.find(class_="resume"))
        self.assertIn('<i class="declaration', bandeau)
        self.assertIn('<i class="delai-60j', bandeau)
        self.assertIn('<i class="dossier-complet-non-verifie', bandeau)
        self.assertIn('<i class="demandee', bandeau)
        self.assertNotIn('<i class="instructeur', bandeau)
        self.assertIn('Dernière action : ' + timezone.now().strftime('%d'), bandeau)

        print('\t >> test des icones des onglets')
        onglet = str(html.find(class_="tab-list"))
        self.assertIn('<i class="suivi', onglet)
        self.assertIn('<i class="detail', onglet)
        self.assertIn('<i class="carto', onglet)
        self.assertIn('<i class="pj', onglet)
        self.assertIn('<i class="message', onglet)

        print('\t >> test des actions et des icones associés')
        action = str(html.find(id="panneau_droite"))
        self.assertIn('Envoyer une demande d\'avis', action)
        self.assertIn('<i class="avis', action)
        self.assertIn('Valider les pièces jointes', action)
        self.assertIn('<i class="valider', action)
        self.assertIn('Ajouter un document officiel', action)
        self.assertIn('<i class="arrete', action)
        self.assertIn('Modifier l\'affichage dans le calendrier public', action)
        self.assertIn('<i class="date', action)
        self.assertIn('Exporter cette manifestation', action)
        self.assertIn('<i class="telecharger', action)

        print('\t >> test du suivi de \'instruction')
        suivi = str(html.find(id="authorization"))
        self.assertIn('class="ctx-help-legende_icone_avis', suivi)
        self.assertIn('<i class="avis', suivi)
        self.assertIn('Fédération', suivi)
        self.assertIn('Détails du service', suivi)
        self.assertIn('<i class="attente', suivi)
        self.assertIn('<i class="recevoir-avis', suivi)
        self.assertIn('<i class="envoyer', suivi)
        self.assertIn('Relancer cette demande', suivi)
        self.assertIn('Annuler cette demande', suivi)

        print('\t >> test des PJs de la manifestation')
        pj = html.find(id="piecejointe")
        self.assertEqual(len(pj.find_all("i", class_="afficher")), 3)
        self.assertEqual(len(pj.find_all("i", class_="telecharger")), 3)
        self.assertEqual(len(pj.find_all("i", class_="pj_nonvalide")), 3)
        self.assertIn('<i class="pj', str(pj))
        self.assertIn('Ajouter une demande de pièce jointe complémentaire', str(pj))

        print('\t >> valider les PJs')
        valid_url = re.search('var url_validation = "(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(valid_url, 'group'))
        liens = pj.find_all("a", class_="validation_pj")
        for lien in liens:
            pk = lien['id'][4:]
            reponse = self.client.post(valid_url.group('url') + '?pk=' + pk + '&valider=1', follow=True, HTTP_HOST='127.0.0.1:8000')
            self.assertContains(reponse, 'ok')

        print('\t >> vérifier validation PJs')
        reponse = self.vue_detail(reponse_tdb)
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action = str(html.find(id="panneau_droite"))
        self.assertNotIn('Valider les pièces jointes', action)
        pj = html.find(id="piecejointe")
        self.assertEqual(len(pj.find_all("i", class_="pj_valide")), 3)
        # self.assertEqual(len(pj.find_all("a", string="Déverrouiller")), 4)
        bandeau = str(html.find(class_="resume"))
        self.assertIn('<i class="instructeur', bandeau)
        self.assertIn('Instructeur : Bob ROBERT', bandeau)
        self.assertIn('<i class="dossier-complet-verifie', bandeau)

        print('\t >> distribuer les demande d\'avis')
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'mairie', 'array_id[]': self.mairie1.pk}, HTTP_HOST='127.0.0.1:8000', follow=True)

        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'police', 'array_id[]': self.ddsp.pk}, HTTP_HOST='127.0.0.1:8000', follow=True)

        reponse = self.client.get(reverse('instructions:instruction_detail', kwargs={'pk': self.instruction.pk}), HTTP_HOST='127.0.0.1:8000', follow=True)

        self.assertContains(reponse, self.instruction)
        self.assertContains(reponse, 'Fédération')
        self.assertContains(reponse, 'FÉDÉ (FÉDÉRATION SPORTIVE) (78)')
        self.assertContains(reponse, 'Mairie')
        self.assertContains(reponse, '42300 MAIRIE')
        self.assertContains(reponse, 'Police')
        self.assertContains(reponse, 'DDSP DDSP (78)')
        self.assertContains(reponse, '<i class="distribue', count=1)
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.assertContains(reponse, '<i class="avis', count=6)
        self.assertContains(reponse, 'Envoyer une demande d\'avis', count=5)
        self.assertContains(reponse, '<i class="envoyer', count=4)
        self.assertContains(reponse, 'Relancer les demandes', count=1)
        self.affichage_avis()
        # Vérifier le passage en encours
        self.presence_avis('instructeur', 'encours', log=False)
        self.client.logout()

        print('**** test 3 agent fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_fede', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.fede.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 28 jours')
        self.assertContains(reponse, '<i class="demandee ico_double" data-titre="Avis demandé par l\'instructeur"', count=1)
        self.assertContains(reponse, '<i class="dossier-complet-verifie', count=1)
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rédiger l\'avis', count=1)
        self.assertContains(reponse, 'Exporter cette manifestation', count=1)
        # Valider l'événement avec l'url fournie et tester la redirection
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # Vérifier le passage en rendu
        self.presence_avis('agent_fede', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content.decode('utf-8'))
        self.assertContains(reponse, '<i class="ok-avis ico_double" data-titre="Avis favorable rendu"', count=1)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        print('**** test 4 agent ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_ddsp', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.ddsp.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.assertContains(reponse, '<i class="demandee ico_double" data-titre="Avis demandé par l\'instructeur"', count=1)
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer une demande de préavis aux services internes', count=1)
        self.assertContains(reponse, 'Exporter cette manifestation', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        reponse = self.distribuer_avis(reponse, self.commiss.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        self.assertContains(reponse, '<i class="avis', count=3)
        self.assertContains(reponse, 'ABR COMMISSARIAT PLAISIR')
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.ddsp))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérifier le passage en encours
        self.presence_avis('agent_ddsp', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.assertContains(reponse, '<i class="distribuee ico_double" data-titre="Demandes de préavis envoyées"', count=1)
        self.client.logout()

        print('**** test 5 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_commiss', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.commiss.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '<i class="demandee ico_double" data-titre="Avis demandé', count=1)
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rédiger le préavis', count=1)
        self.assertContains(reponse, 'Exporter cette manifestation', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.ddsp))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérification du passage en rendu
        self.presence_avis('agent_commiss', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '<i class="ok-avis ico_double" data-titre="Avis favorable rendu', count=1)
        self.client.logout()

        print('**** test 6 agent ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en encours
        self.presence_avis('agent_ddsp', 'encours')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.ddsp.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.assertContains(reponse, '<i class="distribuee ico_double" data-titre="Demandes de préavis envoyées"', count=1)
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # Vérifier le passage en rendu
        self.presence_avis('agent_ddsp', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '<i class="ok-avis ico_double" data-titre="Avis favorable rendu"', count=1)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        print('**** test 7 agent mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_mairie', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.mairie1.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_specifique=serviceconsulte&filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.assertContains(reponse, '<i class="demandee ico_double" data-titre="Avis demandé par l\'instructeur"', count=1)
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rédiger l\'avis', count=1)
        self.assertContains(reponse, 'Exporter cette manifestation', count=1)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # Vérifier le passage en rendu
        self.presence_avis('agent_mairie', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_specifique=serviceconsulte&filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '<i class="ok-avis ico_double" data-titre="Avis favorable rendu"', count=1)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = self.presence_avis('instructeur', 'encours')
        self.assertNotContains(reponse, 'avis manquants')
        # print(reponse.content.decode('utf-8'))
