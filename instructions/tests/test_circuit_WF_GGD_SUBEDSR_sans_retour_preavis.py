import re

from django.test import override_settings
from django.shortcuts import reverse
from bs4 import BeautifulSoup as Bs

from .test_base import TestCommunClass
from core.models import Instance
from instructions.models import Instruction
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from structure.models.service import ServiceConsulteInteraction


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class Circuit_GGD_SUBEDSR_SansPreavisTests(TestCommunClass):
    """
    Test du circuit d'instance GGD_SUBEDSR pour une Dcnm
        avec option de rendre un avis sans emettre de préavis
        circuit_GGD : Avis GGD + préavis EDSR
        instruction par département
        openrunner false par défaut
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========== WF_GGD_SUBEDSR (Clt) ===========')
        # Création des objets sur le 78
        cls.dep = dep = DepartementFactory.create(name='78',
                                                  instance__name="instance de test",
                                                  instance__instruction_mode=Instance.IM_CIRCUIT_A)
        ggd = dep.organisation_set.get(precision_nom_s="GGD")
        ggd.porte_entree_avis_b = True
        ggd.autorise_rendre_avis_b = True
        ggd.service_parent_fk = None
        ggd.save()
        edsr = dep.organisation_set.get(precision_nom_s="EDSR")
        edsr.porte_entree_avis_b = False
        edsr.service_parent_fk = ggd
        edsr.save()
        arrondissement = ArrondissementFactory.create(name='Versailles', code='99', departement=dep)
        cls.prefecture = arrondissement.organisations.first()
        cls.commune = CommuneFactory(name='Plaisir', arrondissement=arrondissement)
        cls.mairie1 = cls.commune.organisation_set.first()
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)
        cls.mairie2 = cls.autrecommune.organisation_set.first()

        TestCommunClass.init_setup(cls)

        ServiceConsulteInteraction.objects.filter(service_entrant_fk=edsr).delete()
        ServiceConsulteInteraction.objects.create(service_entrant_fk=ggd, service_sortant_fk=cls.cgd1, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=ggd, service_sortant_fk=cls.cgd2, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=ggd, service_sortant_fk=cls.usg, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=ggd, service_sortant_fk=edsr, type_lien_s='interroger')
        cls.avis_nb = 2

    def test_Circuit_GGD_SUBEDSR_SansPreavis(self):
        """
        Test des différentes étapes du circuit GGD_SUBEDSR et option sans préavis
        """

        # def print(string="", end=None):
        #     # Supprimer les prints hors debug
        #     pass

        print('**** test 1 creation manif ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('1', nb_bloc.group('nb'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))",
                               reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(declar, 'group'):
            self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.client.logout()

        self.instruction = Instruction.objects.get(manif=self.manifestation)
        # Vérification de la création de l'événement
        self.assertEqual(str(self.instruction), str(self.manifestation))

        print('**** test 2 instructeur - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        # appel ajax pour avoir la liste
        url_intr_list = "/instructions/tableaudebord/" + str(self.prefecture.pk) + "/list/"
        reponse = self.client.get(url_intr_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action_requise = str(html.select("div.border-danger.liste-actions"))
        self.assertIn('Envoyer une demande d\'avis', action_requise)
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'gendarmerie', 'array_id[]': self.ggd.pk}, HTTP_HOST='127.0.0.1:8000')

        # Vérifier le passage en encours et le nombre d'avis manquants
        self.presence_avis('instructeur', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        print('**** test 3 avis fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_fede', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.fede.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 28 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.presence_avis('agent_fede', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        print('**** test 4 avis ggd - distribution ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_ggd', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.ggd.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, [self.edsr.pk, self.cgd1.pk])
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)

        print('**** test 5 rendre avis ggd sans retour préavis ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_ggd', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '2 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # vérification de la présence de l'événement en rendu
        self.presence_avis('agent_ggd', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'avis manquants')
        self.client.logout()

        print('**** test 6 instructeur - annulation ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en warning
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'avis manquants')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Ajouter un document officiel', count=1)
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        publish_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Ajouter un document officiel', reponse.content.decode('utf-8'))
        if hasattr(publish_form, 'group'):
            with open('/tmp/recepisse_declaration.txt') as file1:
                self.client.post(publish_form.group('url'), {'nature': '4', 'fichier': file1}, follow=True, HTTP_HOST='127.0.0.1:8000')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=annule', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction)
        # Vérifier le passage en info et le nombre d'avis manquants
        self.assertContains(reponse, 'table_termine', count=1)
