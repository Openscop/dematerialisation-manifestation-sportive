import re, os

from django.test import override_settings
from django.contrib.auth.hashers import make_password as make
from django.shortcuts import reverse
from bs4 import BeautifulSoup as Bs

from .test_base import TestCommunClass
from core.models import Instance
from core.factories import UserFactory, GroupFactory
from instructions.models import Instruction
from structure.factories.structure import StructureOrganisatriceFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class Acces_MediaTests(TestCommunClass):
    """
    Test d"accès aux fichiers média déposés dans le dossier
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('============ Accès Média (Clt) =============')
        TestCommunClass.init_setup(cls)
        autredep = DepartementFactory.create(name='69',
                                             instance__name="autre instance de test",
                                             instance__instruction_mode=Instance.IM_CIRCUIT_A)
        autrearrondissement = ArrondissementFactory.create(name='Roanne', code='97', departement=cls.dep)
        autrearrondissementautredep = ArrondissementFactory.create(name='Lyon', code='95', departement=autredep)
        autreprefecture = autrearrondissement.organisations.first()
        autreprefectureautredep = autrearrondissementautredep.organisations.first()

        # Création des utilisateurs
        grp1 = GroupFactory.create(name='_support')
        grp2 = GroupFactory.create(name='Administrateurs d\'instance')
        cls.admintech = UserFactory.create(username='admintech', password=make("123"), default_instance=cls.dep.instance)
        cls.admintech.groups.add(grp1)
        # cls.support = cls.dep.organisation_set.get(precision_nom_s="Support")
        # cls.admintech.organisation_m2m.add(cls.support)
        cls.admintech.organisation_m2m.add(cls.prefecture)
        cls.admindinst = UserFactory.create(username='admindinst', password=make("123"), default_instance=cls.dep.instance)
        cls.admindinstautre = UserFactory.create(username='admindinstautre', password=make("123"), default_instance=autredep.instance)
        cls.admindinst.groups.add(grp2)
        cls.admindinst.organisation_m2m.add(cls.prefecture)
        cls.admindinstautre.groups.add(grp2)
        cls.admindinstautre.organisation_m2m.add(autreprefectureautredep)

        cls.structure2 = StructureOrganisatriceFactory.create(precision_nom_s="structure_test_2",
                                                              commune_m2m=cls.commune, instance_fk=cls.dep.instance)
        cls.autreorganisateur = UserFactory.create(username='autreorganisateur', password=make("123"), default_instance=cls.dep.instance)
        cls.autreorganisateur.organisation_m2m.add(cls.structure2)

        cls.autreinstructeur = UserFactory.create(username='autreinstructeur', password=make("123"), default_instance=cls.dep.instance)
        cls.autreinstructeur.organisation_m2m.add(autreprefecture)
        cls.autreinstructeurautredep = UserFactory.create(username='autreinstructeurautredep', password=make("123"), default_instance=autredep.instance)
        cls.autreinstructeurautredep.organisation_m2m.add(autreprefectureautredep)
        cls.agent_mairie_autre = UserFactory.create(username='agent_mairie_autre', password=make("123"), default_instance=cls.dep.instance)
        cls.agent_mairie_autre.organisation_m2m.add(cls.mairie2)

        cls.avis_nb = 6

    def test_Acces_Media(self):
        """
        Test des accès aux fichiers déposés avec les différents comptes du circuit
        """
        """
        def print(string="", end=None):
            # Supprimer les prints hors debug
            pass
        """
        def acces_fichier(url, user, acces, show=False):
            """
            Appel de l'url du fichier pour vérifier l'accès
            """
            # Connexion avec l'utilisateur
            if user:
                self.assertTrue(self.client.login(username=user, password='123'))
            # Appel de la page
            retour = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
            if show:
                print("Accès fichier : " + retour.content.decode('utf-8'))
            if acces == "ok":
                self.assertIn('ok pour voir', retour.content.decode('utf-8'))
            elif acces == "nok":
                self.assertIn('pas les droits pour voir', retour.content.decode('utf-8'))
            else:
                self.assertTrue(False, "paramètre incorrect")
            if user:
                self.client.logout()

        def presence_avis(username, state, log=True):
            """
            Appel de la dashboard de l'utilisateur pour tester la présence et l'état de l'événement
            :param username: agent considéré
            :param state: couleur de l'événement
            :param log: booléen pour connecter l'agent
            :return: retour: la réponse http
            """
            # Connexion avec l'utilisateur
            if log:
                self.assertTrue(self.client.login(username=username, password='123'))
            # Appel de la page
            retour = self.client.get('/instructions/tableaudebord/', follow=True, HTTP_HOST='127.0.0.1:8000')

            # Test du contenu
            if state == 'none':
                if username in ["agent_cis", "agent_codis", "agent_brg1", "agent_brg2"]:
                    nb_bloc = re.search('test_nb_rendu">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                else:
                    # Recherche sur "avis demandés", on sous-entend que c'est un TdB agent
                    nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('0', nb_bloc.group('nb'))
            # Test du chiffre dans la bonne case
            elif state == 'nouveau':
                nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            elif state == 'encours':
                nb_bloc = re.search('test_nb_encours">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            elif state == 'rendu':
                nb_bloc = re.search('test_nb_rendu">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            else:
                self.assertEqual('0', '1', 'paramètre inconnu')
            return retour

        def vue_detail(page):
            """
            Appel de la vue de détail de la manifestation
            :param page: réponse précédente
            :return: reponse suivante
            """
            detail = re.search('data-href=\'(?P<url>(/[^\']+))', page.content.decode('utf-8'))
            if hasattr(detail, 'group'):
                page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
            # self.assertContains(retour, 'Détail de la manifestation<', count=1)
            # print(page.content.decode('utf-8'))
            self.assertContains(page, 'Manifestation_Test')
            return page

        print('--- creation manif ---')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        # Récupération de l'url du fichier déposé
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        url_fichier = re.search('href="(?P<url>(/.+reglement_manifestation[^"]+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_fichier, 'group'))
        print(url_fichier.group('url'))

        self.client.logout()
        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().get_instance())
        print(self.manifestation.structure_organisatrice_fk, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline, end=" ; ")
        print(self.manifestation.activite.discipline.get_federations().first())

        print('**** test accès instructeur avant envoi ****')
        acces_fichier(url_fichier.group('url'), 'instructeur', 'nok')

        print('**** test accès agent avant envoi : fédération ****')
        acces_fichier(url_fichier.group('url'), 'agent_fede', 'nok')

        # Connexion avec l'utilisateur
        print('--- envoi manif ---')
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        self.assertTrue(hasattr(declar, 'group'))
        print(declar.group('url'))
        self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.client.logout()

        print('**** test accès instructeur après envoi ****')
        acces_fichier(url_fichier.group('url'), 'instructeur', 'ok')

        print('**** test accès agent après envoi : fédération ****')
        acces_fichier(url_fichier.group('url'), 'agent_fede', 'ok')

        self.instruction = Instruction.objects.get(manif=self.manifestation)
        self.assertEqual(str(self.instruction), str(self.manifestation))

        print('**** test accès anonymous ****')
        acces_fichier(url_fichier.group('url'), '', 'nok')

        print('**** test accès grp technique ****')
        acces_fichier(url_fichier.group('url'), 'admintech', 'ok')

        print('**** test accès grp instance ****')
        acces_fichier(url_fichier.group('url'), 'admindinst', 'ok')

        print('**** test accès autre grp instance ****')
        acces_fichier(url_fichier.group('url'), 'admindinstautre', 'nok')

        print('**** test accès autre organisateur ****')
        acces_fichier(url_fichier.group('url'), 'autreorganisateur', 'nok')

        print('**** test accès organisateur ****')
        acces_fichier(url_fichier.group('url'), 'organisateur', 'ok')

        print('**** test accès autre instructeur d\'arrondissement ****')
        acces_fichier(url_fichier.group('url'), 'autreinstructeur', 'nok')

        print('**** test accès autre instructeur de département ****')
        acces_fichier(url_fichier.group('url'), 'autreinstructeurautredep', 'nok')

        print('**** test accès agent avant distribution : edsr ****')
        acces_fichier(url_fichier.group('url'), 'agent_edsr', 'nok')

        print('**** test accès agent avant distribution : ddsp ****')
        acces_fichier(url_fichier.group('url'), 'agent_ddsp', 'nok')

        print('**** test accès agent avant distribution : mairie ****')
        acces_fichier(url_fichier.group('url'), 'agent_mairie', 'nok')

        print('**** test accès agent avant distribution : cg ****')
        acces_fichier(url_fichier.group('url'), 'agent_cg', 'nok')

        print('**** test accès agent avant distribution : sdis ****')
        acces_fichier(url_fichier.group('url'), 'agent_sdis', 'nok')

        print('--- distribution des avis ---')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        presence_avis('instructeur', 'nouveau')
        # appel ajax pour avoir la liste
        url_intr_list = "/instructions/tableaudebord/" + str(self.prefecture.pk) + "/list/"
        reponse = self.client.get(url_intr_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action_requise = str(html.select("div.border-danger.liste-actions"))
        self.assertIn('Envoyer une demande d\'avis', action_requise)
        # Distribuer les demandes d'avis de l'événement
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}), data={'categorie': 'police', 'array_id[]': self.ddsp.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}), data={'categorie': 'pompier', 'array_id[]': self.sdis.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}), data={'categorie': 'gendarmerie', 'array_id[]': self.edsr.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}), data={'categorie': 'conseil_departemental', 'array_id[]': self.cg.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}), data={'categorie': 'mairie', 'array_id[]': self.mairie1.pk}, HTTP_HOST='127.0.0.1:8000')

        # Vérifier le passage en info et le nombre d'avis manquants
        presence_avis('instructeur', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        print('**** test accès agent après distribution : edsr ****')
        acces_fichier(url_fichier.group('url'), 'agent_edsr', 'ok')

        print('**** test accès agent après distribution : ddsp ****')
        acces_fichier(url_fichier.group('url'), 'agent_ddsp', 'ok')

        print('**** test accès agent après distribution : mairie ****')
        acces_fichier(url_fichier.group('url'), 'agent_mairie', 'ok')

        print('**** test accès agent après distribution : cg ****')
        acces_fichier(url_fichier.group('url'), 'agent_cg', 'ok')

        print('**** test accès agent après distribution : sdis ****')
        acces_fichier(url_fichier.group('url'), 'agent_sdis', 'ok')

        print('**** test accès agent local avant distribution : cgd ****')
        acces_fichier(url_fichier.group('url'), 'agent_cgd1', 'nok')

        print('**** test accès agent avant notification : brg ****')
        acces_fichier(url_fichier.group('url'), 'agent_brg1', 'nok')

        print('--- distribution préavis edsr ---')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en nouveau
        presence_avis('agent_edsr', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.edsr.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, [self.cgd1.pk, self.usg.pk])
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Vérification du passage en encours
        presence_avis('agent_edsr', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '2 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()

        print('**** test accès agent local après distribution : cgd ****')
        acces_fichier(url_fichier.group('url'), 'agent_cgd1', 'ok')

        print('**** test accès agent local après distribution : usg ****')
        acces_fichier(url_fichier.group('url'), 'agent_usg', 'ok')

        print('--- preavis cgd ---')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en nouveau
        presence_avis('agent_cgd1', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cgd1.pk) + "/list/"
        reponse_list = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        print('\t >>> Informer brigade')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse_list)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Octroyer un accès en lecture', count=1)
        # informer les brigades de l'événement avec l'url fournie et tester la redirection
        data = {
            'acces': 'complet',
            'model': 'Brigade',
            'service_id': self.brigade1.pk,
        }
        preavis_pk = re.search('href="/instructions/avis/(?P<id>(\d+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(preavis_pk, 'group'))
        self.client.post('/instructions/acces/' + preavis_pk.group("id") + '/create/', data,
                         HTTP_HOST='127.0.0.1:8000', HTTP_REFERER='/instructions/preavis/' + preavis_pk.group("id") + '/')
        reponse = self.vue_detail(reponse_list)
        self.assertContains(reponse, 'Accès en lecture octroyé', count=1)
        self.client.logout()

        print('**** test accès agent après notification : brg ****')
        acces_fichier(url_fichier.group('url'), 'agent_brg1', 'ok')

        print('\t >>> Rendre préavis')
        # Vérification du passage en encours
        presence_avis('agent_cgd1', 'encours')
        # appel ajax pour avoir la liste
        reponse_list = self.client.get(url_list + '?filtre_etat=demarre', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        self.client.logout()

        print('--- preavis usg ---')
        # Instruction du préavis par le usg, vérification de la présence de l'événement en nouveau
        presence_avis('agent_usg', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.usg.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        self.client.logout()


        print('**** test accès agent avant soumission : ggd ****')
        acces_fichier(url_fichier.group('url'), 'agent_ggd', 'nok')

        print('--- soumission avis edsr ---')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en encours
        presence_avis('agent_edsr', 'encours')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.edsr.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.adresser_avis(reponse, self.ggd.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # vérification de la présence de l'événement en rendu
        presence_avis('agent_edsr', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()

        print('**** test accès agent après soumission : ggd ****')
        acces_fichier(url_fichier.group('url'), 'agent_ggd', 'ok')

        print('**** test accès agent local avant distribution : commissariat ****')
        acces_fichier(url_fichier.group('url'), 'agent_commiss', 'nok')

        print('--- distribution préavis ddsp ---')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        presence_avis('agent_ddsp', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.ddsp.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, self.commiss.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Vérifier le passage en encours
        presence_avis('agent_ddsp', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()

        print('**** test accès agent local après distribution : commissariat ****')
        acces_fichier(url_fichier.group('url'), 'agent_commiss', 'ok')

        print('**** test accès agent local avant distribution : CGService ****')
        acces_fichier(url_fichier.group('url'), 'agent_cgserv', 'nok')

        print('--- distribution préavis cg ---')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en nouveau
        presence_avis('agent_cg', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cg.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, self.cgserv.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Vérifier le passage en encours
        presence_avis('agent_cg', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()

        print('**** test accès agent local après distribution : CGService ****')
        acces_fichier(url_fichier.group('url'), 'agent_cgserv', 'ok')

        # Instruction du préavis par le cgservice, vérification de la présence de l'événement en nouveau
        presence_avis('agent_cgserv', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cgserv.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        self.client.logout()

        print('**** test accès agent avant soumission : CGSupérieur ****')
        acces_fichier(url_fichier.group('url'), 'agent_cgsup', 'nok')

        print('--- soumission avis cg ---')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en encours
        presence_avis('agent_cg', 'encours')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cg.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.adresser_avis(reponse, self.cgsup.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # vérification de la présence de l'événement en rendu
        presence_avis('agent_cg', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()

        print('**** test accès agent après soumission : CGSupérieur ****')
        acces_fichier(url_fichier.group('url'), 'agent_cgsup', 'ok')

        print('**** test accès agent local avant distribution : groupement ****')
        acces_fichier(url_fichier.group('url'), 'agent_group', 'nok')

        print('**** test accès agent avant rendu : codis ****')
        acces_fichier(url_fichier.group('url'), 'agent_codis', 'nok')

        print('**** test accès agent avant notification : cis ****')
        acces_fichier(url_fichier.group('url'), 'agent_cis', 'nok')

        print('--- distribution préavis sdis ---')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en nouveau
        presence_avis('agent_sdis', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.sdis.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, self.group.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Vérification du passage en encours
        presence_avis('agent_sdis', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()

        print('**** test accès agent local après distribution : groupement ****')
        acces_fichier(url_fichier.group('url'), 'agent_group', 'ok')

        # Instruction du préavis par le groupement, vérification de la présence de l'événement en nouveau
        presence_avis('agent_group', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.group.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        self.client.logout()

        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en encours
        presence_avis('agent_sdis', 'encours')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.sdis.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        self.client.logout()

        print('**** test accès agent après rendu : codis ****')
        acces_fichier(url_fichier.group('url'), 'agent_codis', 'nok')

        print('**** test 26 notification cis du groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en encours
        presence_avis('agent_group', 'rendu')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.group.pk) + "/list/"
        reponse_list = self.client.get(url_list + '?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse_list)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Octroyer un accès en lecture', count=1)
        # Informer les CIS de l'événement avec l'url fournie et tester la redirection
        data = {
            'acces': 'complet',
            'model': 'Cis',
            'service_id': self.cis.pk,
        }
        preavis_pk = re.search('href="/instructions/avis/(?P<id>(\d+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(preavis_pk, 'group'))
        retour = self.client.post('/instructions/acces/' + preavis_pk.group("id") + '/create/', data,
                         HTTP_HOST='127.0.0.1:8000', HTTP_REFERER='/instructions/preavis/' + preavis_pk.group("id") + '/')
        reponse = self.vue_detail(reponse_list)
        self.assertContains(reponse, 'Accès en lecture octroyé', count=1)
        self.client.logout()

        print('**** test accès agent après notification : cis ****')
        acces_fichier(url_fichier.group('url'), 'agent_cis', 'ok')
