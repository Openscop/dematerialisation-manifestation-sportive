import json
import re

from django.test import override_settings

from .test_base import TestCommunClass
from core.models import Instance
from administrative_division.factories import CommuneFactory, DepartementFactory, ArrondissementFactory


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class ModificationDossierInterdep(TestCommunClass):
    """
    Test de la demande de modification du dossier en interdépartementalité par l'instructeur
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('============ Modif_dossier_interdep (Clt) =============')
        TestCommunClass.init_setup(cls)
        cls.dep43 = DepartementFactory.create(name='43',
                                              instance__name="instance de test43",
                                              instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                              instance__etat_s="ouvert")
        cls.dep07 = DepartementFactory.create(name='07',
                                              instance__name="instance de test07",
                                              instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                              instance__etat_s="ouvert")
        cls.dep63 = DepartementFactory.create(name='63',
                                              instance__name="instance de test63",
                                              instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                              instance__etat_s="ouvert")
        arron43 = ArrondissementFactory.create(name='Yssingeaux', code='433', departement=cls.dep43)
        arron07 = ArrondissementFactory.create(name='Aubenas', code='435', departement=cls.dep07)
        arron63 = ArrondissementFactory.create(name='Issoire', code='437', departement=cls.dep63)

        cls.autrecommune42 = CommuneFactory(name='Jas', arrondissement=cls.commune.arrondissement)
        cls.commune431 = CommuneFactory(name='Chadrac', arrondissement=arron43)
        cls.commune432 = CommuneFactory(name='Costaros', arrondissement=arron43)
        cls.commune433 = CommuneFactory(name='Chomelix', arrondissement=arron43)
        cls.commune434 = CommuneFactory(name='Chadron', arrondissement=arron43)
        cls.commune071 = CommuneFactory(name='Devesset', arrondissement=arron07)
        cls.commune072 = CommuneFactory(name='Dornas', arrondissement=arron07)
        cls.commune073 = CommuneFactory(name='Darbres', arrondissement=arron07)
        cls.commune631 = CommuneFactory(name='Esteil', arrondissement=arron63)
        cls.commune632 = CommuneFactory(name='Espinchal', arrondissement=arron63)

    def test_modif_dossier_interdep(self):
        """
        Test de demande de modification du dossier en interdépartementalité sur plusieurs champs
        """
        print('**** test 1 creation manif ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('1', nb_bloc.group('nb'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(declar, 'group'):
            self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertIsNone(self.manifestation.ville_depart_interdep)
        self.instruction = self.manifestation.get_instruction()
        self.client.logout()

        print('**** test 2 demande modification département traversés ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        self.assertIsNone(self.manifestation.modif_json)
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=departements_traverses"
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.modif_json), 1)
        self.assertFalse(self.manifestation.modif_json[0]['done'])
        self.assertIn('manif', self.manifestation.modif_json[0])
        self.assertEqual(self.manifestation.modif_json[0]['manif'], 'departements_traverses')
        # appel ajax pour avoir la liste
        url_list = f'/instructions/tableaudebord/{str(self.prefecture.pk)}/list/?filtre_etat=atraiter'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        self.assertIn('champ', reponse.context)
        self.assertIn('departements_traverses', reponse.context['champ'])
        self.client.logout()

        print('**** test 3 modification organisateur ajout département traversé 43 ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=demande', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Modification de formulaire demandée')
        self.assertIsNone(self.manifestation.history_json)
        self.assertIn('departements_traverses', reponse.context['champ'])
        # Modification des départements traversés
        data = {}
        data['departements_traverses'] = self.dep43.pk
        data['ville_depart_interdep_' + str(self.dep43.pk)] = self.commune431.pk
        data['ville_traverse_' + str(self.dep43.pk)] = self.commune432.pk
        url_edit = "/Dcnm/" + str(self.manifestation.pk) + "/edit/?dept=42"
        reponse = self.client.post(url_edit, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Modification de formulaire demandée')
        # Villes traversées
        self.assertContains(reponse, 'Costaros,')
        self.assertContains(reponse, 'Roche.')
        # Départements traversés
        self.assertContains(reponse, '43 - Haute-Loire.')
        # Histo
        self.assertContains(reponse, ': Roche')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=villes_traversees"')
        self.assertContains(reponse, ': Non renseigné', count=2)
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=departements_traverses"')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=ville_depart_interdep_' + str(self.dep43.pk) + '"')
        self.assertEqual(reponse.context['champ'], '[]')
        # Etat des instructions
        self.assertContains(reponse, 'Instruction du département 42 - Loire')
        self.assertContains(reponse, 'Instruction du département 43 - Haute-Loire')
        self.assertContains(reponse, '<i class="demandee fa-lg"></i>', count=2)
        # Fichiers manquants, les pièces jointes ne sont pas chargées
        self.assertContains(reponse, 'Itinéraire horaire pour le département 42 - Loire')
        self.assertContains(reponse, 'Itinéraire horaire pour le département 43 - Haute-Loire')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.ville_depart_interdep), 1)
        self.assertEqual(self.manifestation.ville_depart_interdep[0]['commune_name'], 'Chadrac')
        for item in self.manifestation.history_json:
            print(item)
        idx = len(self.manifestation.history_json)
        self.assertEqual(len(self.manifestation.history_json), 3)
        self.assertEqual(self.manifestation.history_json[0]['type'], 'list')
        self.assertIn('champ', self.manifestation.history_json[0])
        self.assertEqual(self.manifestation.history_json[0]['champ'], 'departements_traverses')
        self.assertEqual(self.manifestation.history_json[1]['type'], 'str')
        self.assertIn('champ', self.manifestation.history_json[1])
        self.assertEqual(self.manifestation.history_json[1]['champ'], 'ville_depart_interdep_' + str(self.dep43.pk))
        self.assertEqual(self.manifestation.history_json[2]['type'], 'list')
        self.assertIn('champ', self.manifestation.history_json[2])
        self.assertEqual(self.manifestation.history_json[2]['champ'], 'villes_traversees')
        self.assertTrue(self.manifestation.modif_json[0]['done'])
        self.assertEqual(len(self.manifestation.instance_instruites.all()), 2)
        self.client.logout()

        print('**** test 4 demande modification villes traversées ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=villes_traversees"
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.modif_json), 2)
        self.assertFalse(self.manifestation.modif_json[1]['done'])
        self.assertIn('manif', self.manifestation.modif_json[1])
        self.assertEqual(self.manifestation.modif_json[1]['manif'], 'villes_traversees')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        self.assertIn('villes_traversees', reponse.context['champ'])
        self.client.logout()

        print('**** test 5 modification organisateur ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=demande', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Modification de formulaire demandée')
        self.assertIn('villes_traversees', reponse.context['champ'])
        # Modification des villes traversées
        data = {}
        data['ville_traverse_' + str(self.dep43.pk)] = [self.commune432.pk,  self.commune433.pk]
        # Ajout dans le post pour être conforme à la réalité
        data['ville_traverse_' + str(self.dep.pk)] = self.autrecommune.pk
        data['villes_traversees'] = [ville.pk for ville in self.manifestation.villes_traversees.all()]
        reponse = self.client.post(url_edit, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Modification de formulaire demandée')
        # Villes traversées
        self.assertContains(reponse, 'Chomelix,')
        self.assertContains(reponse, 'Costaros,')
        self.assertContains(reponse, 'Roche.')
        # 2ème histo
        self.assertContains(reponse, ': Costaros, Roche')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=villes_traversees"')
        self.assertEqual(reponse.context['champ'], '[]')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.ville_depart_interdep), 1)
        self.assertEqual(self.manifestation.ville_depart_interdep[0]['commune_name'], 'Chadrac')
        for item in self.manifestation.history_json[idx:]:
            print(item)
        idx = len(self.manifestation.history_json)
        self.assertEqual(len(self.manifestation.history_json), 4)
        self.assertEqual(self.manifestation.history_json[3]['type'], 'list')
        self.assertIn('champ', self.manifestation.history_json[3])
        self.assertEqual(self.manifestation.history_json[3]['champ'], 'villes_traversees')
        self.assertTrue(self.manifestation.modif_json[1]['done'])
        self.client.logout()

        print('**** test 6 demande modification département traversés ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=departements_traverses"
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.modif_json), 3)
        self.assertFalse(self.manifestation.modif_json[2]['done'])
        self.assertIn('manif', self.manifestation.modif_json[2])
        self.assertEqual(self.manifestation.modif_json[2]['manif'], 'departements_traverses')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        self.assertIn('departements_traverses', reponse.context['champ'])
        self.client.logout()

        print('**** test 7 modification organisateur, supprimer département traversé ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=demande', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Modification de formulaire demandée')
        self.assertIn('departements_traverses', reponse.context['champ'])
        # Modification des départements traversés
        data = {}
        url_edit = "/Dcnm/" + str(self.manifestation.pk) + "/edit/?dept=42"
        reponse = self.client.post(url_edit, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Vous ne pouvez plus supprimer un département traversé')

        print('**** test 8 modification organisateur ajout département traversé 07 ****')
        # Modification des départements traversés
        data = {}
        data['departements_traverses'] = [self.dep43.pk, self.dep07.pk]
        data['ville_depart_interdep_' + str(self.dep07.pk)] = self.commune071.pk
        data['ville_traverse_' + str(self.dep07.pk)] = self.commune072.pk
        url_edit = "/Dcnm/" + str(self.manifestation.pk) + "/edit/?dept=42"
        reponse = self.client.post(url_edit, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Modification de formulaire demandée')
        # Villes traversées
        self.assertContains(reponse, 'Chomelix,')
        self.assertContains(reponse, 'Costaros,')
        self.assertContains(reponse, 'Dornas,')
        self.assertContains(reponse, 'Roche.')
        # Départements traversés
        self.assertContains(reponse, '07 - Ardèche,')
        self.assertContains(reponse, '43 - Haute-Loire.')
        # Ville de départ
        self.assertContains(reponse, 'Chadrac')
        self.assertContains(reponse, 'Devesset')
        # 3ème histo
        self.assertContains(reponse, ': Chomelix, Costaros, Roche')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=villes_traversees"')
        self.assertContains(reponse, ': 43 - Haute-Loire')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=departements_traverses"')
        self.assertContains(reponse, ': Non renseigné', count=3)
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=ville_depart_interdep_' + str(self.dep43.pk) + '"')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=ville_depart_interdep_' + str(self.dep07.pk) + '"')
        self.assertEqual(reponse.context['champ'], '[]')
        # Etat des instructions
        self.assertContains(reponse, 'Instruction du département 42 - Loire')
        self.assertContains(reponse, 'Instruction du département 43 - Haute-Loire')
        self.assertContains(reponse, 'Instruction du département 07 - Ardèche')
        self.assertContains(reponse, '<i class="demandee fa-lg"></i>', count=3)
        # Fichiers manquants, les pièces jointes ne sont pas chargées
        self.assertContains(reponse, 'Itinéraire horaire pour le département 42 - Loire')
        self.assertContains(reponse, 'Itinéraire horaire pour le département 43 - Haute-Loire')
        self.assertContains(reponse, 'Itinéraire horaire pour le département 07 - Ardèche')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.ville_depart_interdep), 2)
        self.assertEqual(self.manifestation.ville_depart_interdep[0]['commune_name'], 'Chadrac')
        self.assertEqual(self.manifestation.ville_depart_interdep[1]['commune_name'], 'Devesset')
        for item in self.manifestation.history_json[idx:]:
            print(item)
        idx = len(self.manifestation.history_json)
        self.assertEqual(len(self.manifestation.history_json), 7)
        self.assertEqual(self.manifestation.history_json[4]['type'], 'list')
        self.assertIn('champ', self.manifestation.history_json[4])
        self.assertEqual(self.manifestation.history_json[4]['champ'], 'departements_traverses')
        self.assertEqual(self.manifestation.history_json[5]['type'], 'str')
        self.assertIn('champ', self.manifestation.history_json[5])
        self.assertEqual(self.manifestation.history_json[5]['champ'], 'ville_depart_interdep_' + str(self.dep07.pk))
        self.assertEqual(self.manifestation.history_json[6]['type'], 'list')
        self.assertIn('champ', self.manifestation.history_json[6])
        self.assertEqual(self.manifestation.history_json[6]['champ'], 'villes_traversees')
        self.assertTrue(self.manifestation.modif_json[2]['done'])
        self.assertEqual(len(self.manifestation.instance_instruites.all()), 3)
        self.client.logout()

        print('**** test 9 demande modification ville de départ département 43 ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=ville_depart_interdep_" + str(self.dep43.pk)
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.modif_json), 4)
        self.assertFalse(self.manifestation.modif_json[3]['done'])
        self.assertIn('manif', self.manifestation.modif_json[3])
        self.assertEqual(self.manifestation.modif_json[3]['manif'], 'ville_depart_interdep_' + str(self.dep43.pk))
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        self.assertIn('ville_depart_interdep_' + str(self.dep43.pk), reponse.context['champ'])
        self.client.logout()

        print('**** test 10 modification organisateur ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=demande', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Modification de formulaire demandée')
        self.assertIn('ville_depart_interdep_' + str(self.dep43.pk), reponse.context['champ'])
        # Modification de la ville de départ 43
        self.manifestation.refresh_from_db()
        data_interdep = json.dumps(self.manifestation.ville_depart_interdep)
        data = {}
        data['ville_depart_interdep_' + str(self.dep43.pk)] = self.commune434.pk
        # Ajout dans le post pour être conforme à la réalité
        data['ville_depart_interdep'] = data_interdep
        reponse = self.client.post(url_edit, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Modification de formulaire demandée')
        # Villes traversées
        self.assertContains(reponse, 'Chomelix,')
        self.assertContains(reponse, 'Costaros,')
        self.assertContains(reponse, 'Dornas,')
        self.assertContains(reponse, 'Roche.')
        # Départements traversés
        self.assertContains(reponse, '07 - Ardèche,')
        self.assertContains(reponse, '43 - Haute-Loire.')
        # Ville de départ
        self.assertContains(reponse, 'Chadron')
        self.assertContains(reponse, 'Devesset')
        # 4ème histo
        self.assertContains(reponse, ': Non renseigné', count=3)
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=ville_depart_interdep_' + str(self.dep43.pk) + '"')
        self.assertEqual(reponse.context['champ'], '[]')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.ville_depart_interdep), 2)
        self.assertEqual(self.manifestation.ville_depart_interdep[0]['commune_name'], 'Devesset')
        self.assertEqual(self.manifestation.ville_depart_interdep[1]['commune_name'], 'Chadron')
        for item in self.manifestation.history_json[idx:]:
            print(item)
        idx = len(self.manifestation.history_json)
        self.assertEqual(len(self.manifestation.history_json), 8)
        self.assertEqual(self.manifestation.history_json[7]['type'], 'str')
        self.assertIn('champ', self.manifestation.history_json[7])
        self.assertEqual(self.manifestation.history_json[7]['champ'], 'ville_depart_interdep_' + str(self.dep43.pk))
        self.assertTrue(self.manifestation.modif_json[3]['done'])
        self.client.logout()

        print('**** test 11 demande modifications multiples ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=departements_traverses"
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=villes_traversees"
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=ville_depart"
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=ville_depart_interdep_" + str(self.dep07.pk)
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.modif_json), 8)
        self.assertFalse(self.manifestation.modif_json[4]['done'])
        self.assertIn('manif', self.manifestation.modif_json[4])
        self.assertEqual(self.manifestation.modif_json[4]['manif'], 'departements_traverses')
        self.assertFalse(self.manifestation.modif_json[5]['done'])
        self.assertIn('manif', self.manifestation.modif_json[5])
        self.assertEqual(self.manifestation.modif_json[5]['manif'], 'villes_traversees')
        self.assertFalse(self.manifestation.modif_json[6]['done'])
        self.assertIn('manif', self.manifestation.modif_json[6])
        self.assertEqual(self.manifestation.modif_json[6]['manif'], 'ville_depart')
        self.assertFalse(self.manifestation.modif_json[7]['done'])
        self.assertIn('manif', self.manifestation.modif_json[7])
        self.assertEqual(self.manifestation.modif_json[7]['manif'], 'ville_depart_interdep_' + str(self.dep07.pk))
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        self.assertIn('champ', reponse.context)
        self.assertIn('departements_traverses', reponse.context['champ'])
        self.assertIn('villes_traversees', reponse.context['champ'])
        self.assertIn('ville_depart', reponse.context['champ'])
        self.assertIn('ville_depart_interdep_' + str(self.dep07.pk), reponse.context['champ'])
        self.client.logout()

        print('**** test 12 modification organisateur ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=demande', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Modification de formulaire demandée')
        self.assertIn('departements_traverses', reponse.context['champ'])
        self.assertIn('villes_traversees', reponse.context['champ'])
        self.assertIn('ville_depart', reponse.context['champ'])
        self.assertIn('ville_depart_interdep_' + str(self.dep07.pk), reponse.context['champ'])
        # Modifications demandées
        data_interdep = json.dumps(self.manifestation.ville_depart_interdep)
        data = {}
        data['departements_traverses'] = [self.dep43.pk, self.dep07.pk, self.dep63.pk]
        data['ville_depart_interdep_' + str(self.dep63.pk)] = self.commune631.pk
        data['ville_traverse_' + str(self.dep63.pk)] = self.commune632.pk
        data['ville_depart'] = self.autrecommune42.pk
        data['ville_depart_interdep_' + str(self.dep07.pk)] = self.commune073.pk
        data['ville_traverse_' + str(self.dep07.pk)] = self.commune071.pk
        # Ajout dans le post pour être conforme à la réalité
        data['ville_traverse_' + str(self.dep.pk)] = self.autrecommune.pk
        data['ville_traverse_' + str(self.dep43.pk)] = [self.commune432.pk,  self.commune433.pk]
        data['villes_traversees'] = [ville.pk for ville in self.manifestation.villes_traversees.all()]
        data['ville_depart_interdep'] = data_interdep
        reponse = self.client.post(url_edit, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Modification de formulaire demandée')
        # Villes traversées
        self.assertContains(reponse, 'Chomelix,')
        self.assertContains(reponse, 'Costaros,')
        self.assertContains(reponse, 'Devesset,')
        self.assertContains(reponse, 'Roche.')
        # Départements traversés
        self.assertContains(reponse, '07 - Ardèche,')
        self.assertContains(reponse, '43 - Haute-Loire,')
        self.assertContains(reponse, '63 - Puy-de-Dôme.')
        # Ville de départ
        self.assertContains(reponse, 'Jas')
        self.assertContains(reponse, 'Chadron')
        self.assertContains(reponse, 'Darbres')
        self.assertContains(reponse, 'Esteil')
        # 4ème histo
        self.assertContains(reponse, ': Bard')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=ville_depart"')
        self.assertContains(reponse, ': Chomelix, Costaros, Dornas, Roche')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=villes_traversees"')
        self.assertContains(reponse, ': 07 - Ardèche, 43 - Haute-Loire')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=departements_traverses"')
        self.assertContains(reponse, ': Chadrac')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=ville_depart_interdep_' + str(self.dep43.pk) + '"')
        self.assertContains(reponse, ': Devesset')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=ville_depart_interdep_' + str(self.dep07.pk) + '"')
        self.assertContains(reponse, ': Non renseigné', count=4)
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=ville_depart_interdep_' + str(self.dep63.pk) + '"')
        self.assertEqual(reponse.context['champ'], '[]')
        # Etat des instructions
        self.assertContains(reponse, 'Instruction du département 42 - Loire')
        self.assertContains(reponse, 'Instruction du département 43 - Haute-Loire')
        self.assertContains(reponse, 'Instruction du département 07 - Ardèche')
        self.assertContains(reponse, 'Instruction du département 63 - Puy-de-Dôme')
        self.assertContains(reponse, '<i class="demandee fa-lg"></i>', count=4)
        # Fichiers manquants, les pièces jointes ne sont pas chargées
        self.assertContains(reponse, 'Itinéraire horaire pour le département 42 - Loire')
        self.assertContains(reponse, 'Itinéraire horaire pour le département 43 - Haute-Loire')
        self.assertContains(reponse, 'Itinéraire horaire pour le département 07 - Ardèche')
        self.assertContains(reponse, 'Itinéraire horaire pour le département 63 - Puy-de-Dôme')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.ville_depart_interdep), 3)
        self.assertEqual(self.manifestation.ville_depart_interdep[0]['commune_name'], 'Chadron')
        self.assertEqual(self.manifestation.ville_depart_interdep[1]['commune_name'], 'Darbres')
        self.assertEqual(self.manifestation.ville_depart_interdep[2]['commune_name'], 'Esteil')
        for item in self.manifestation.history_json[idx:]:
            print(item)
        self.assertEqual(len(self.manifestation.history_json), 13)
        self.assertEqual(self.manifestation.history_json[8]['type'], 'int')
        self.assertIn('champ', self.manifestation.history_json[8])
        self.assertEqual(self.manifestation.history_json[8]['champ'], 'ville_depart')
        self.assertEqual(self.manifestation.history_json[9]['type'], 'list')
        self.assertIn('champ', self.manifestation.history_json[9])
        self.assertEqual(self.manifestation.history_json[9]['champ'], 'departements_traverses')
        self.assertEqual(self.manifestation.history_json[10]['type'], 'str')
        self.assertIn('champ', self.manifestation.history_json[10])
        self.assertEqual(self.manifestation.history_json[10]['champ'], 'ville_depart_interdep_' + str(self.dep07.pk))
        self.assertEqual(self.manifestation.history_json[11]['type'], 'str')
        self.assertIn('champ', self.manifestation.history_json[11])
        self.assertEqual(self.manifestation.history_json[11]['champ'], 'ville_depart_interdep_' + str(self.dep63.pk))
        self.assertEqual(self.manifestation.history_json[12]['type'], 'list')
        self.assertIn('champ', self.manifestation.history_json[12])
        self.assertEqual(self.manifestation.history_json[12]['champ'], 'villes_traversees')
        self.assertTrue(self.manifestation.modif_json[4]['done'])
        self.assertTrue(self.manifestation.modif_json[5]['done'])
        self.assertTrue(self.manifestation.modif_json[6]['done'])
        self.assertTrue(self.manifestation.modif_json[7]['done'])
        self.assertEqual(len(self.manifestation.instance_instruites.all()), 4)
        self.client.logout()

        # print(reponse.content.decode('utf-8'))
