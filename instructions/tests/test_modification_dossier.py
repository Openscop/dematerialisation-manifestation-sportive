import re

from django.test import override_settings
from django.utils import timezone

from .test_base import TestCommunClass
from administrative_division.factories import CommuneFactory


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class ModificationDossier(TestCommunClass):
    """
    Test de la demande de modification du dossier par l'instructeur
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('============ Modif_dossier (Clt) =============')
        TestCommunClass.init_setup(cls)

        cls.autrecommune2 = CommuneFactory(name='Boen', arrondissement=cls.commune.arrondissement)
        cls.autrecommune3 = CommuneFactory(name='Sauvain', arrondissement=cls.commune.arrondissement)

    def test_modif_dossier(self):
        """
        Test de demande de modification du dossier sur plusieurs champs
        """
        print('**** test 1 creation manif ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('1', nb_bloc.group('nb'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        self.assertTrue(hasattr(declar, 'group'))
        self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        self.instruction = self.manifestation.get_instruction()
        self.client.logout()

        print('**** test 2 demande modification date ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        self.assertIsNone(self.manifestation.modif_json)
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=date_debut"
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.modif_json), 1)
        self.assertFalse(self.manifestation.modif_json[0]['done'])
        self.assertIn('manif', self.manifestation.modif_json[0])
        self.assertEqual(self.manifestation.modif_json[0]['manif'], 'date_debut')
        # appel ajax pour avoir la liste
        url_list = f'/instructions/tableaudebord/{str(self.prefecture.pk)}/list/?filtre_etat=atraiter'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        self.assertIn('champ', reponse.context)
        self.assertIn('date_debut', reponse.context['champ'])

        print('**** test 3 annulation de demande modification date ****')
        url_annule = url + "&annule=1"
        self.client.get(url_annule, HTTP_HOST='127.0.0.1:8000')
        self.manifestation.refresh_from_db()
        self.assertEqual(self.manifestation.modif_json, [])
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        self.assertEqual(reponse.context['champ'], '[]')
        # Demande de modif date de début
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.modif_json), 1)
        self.client.logout()

        print('**** test 4 modification organisateur ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=demande', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Modification de formulaire demandée')
        self.assertIsNone(self.manifestation.history_json)
        self.assertIn('date_debut', reponse.context['champ'])
        # Modification de la date
        date = self.manifestation.date_debut + timezone.timedelta(hours=1)
        data = {}
        data['date_debut'] = date
        url_edit = "/Dcnm/" + str(self.manifestation.pk) + "/edit/?dept=42"
        reponse = self.client.post(url_edit, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Modification de formulaire demandée')
        # 1er histo
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=date_debut"')
        self.assertEqual(reponse.context['champ'], '[]')
        self.manifestation.refresh_from_db()
        for item in self.manifestation.history_json:
            print(item)
        idx = len(self.manifestation.history_json)
        self.assertEqual(len(self.manifestation.history_json), 1)
        self.assertEqual(self.manifestation.history_json[0]['type'], 'str')
        self.assertIn('champ', self.manifestation.history_json[0])
        self.assertEqual(self.manifestation.history_json[0]['champ'], 'date_debut')
        self.assertTrue(self.manifestation.modif_json[0]['done'])
        self.client.logout()

        print('**** test 5 demande modification ville de départ ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=ville_depart"
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.modif_json), 2)
        self.assertFalse(self.manifestation.modif_json[1]['done'])
        self.assertIn('manif', self.manifestation.modif_json[1])
        self.assertEqual(self.manifestation.modif_json[1]['manif'], 'ville_depart')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        self.assertIn('ville_depart', reponse.context['champ'])
        self.client.logout()

        print('**** test 6 modification organisateur ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=demande', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Modification de formulaire demandée')
        self.assertEqual(len(self.manifestation.history_json), 1)
        self.assertIn('ville_depart', reponse.context['champ'])
        # Modification de la ville de départ
        data = {}
        data['ville_depart'] = self.autrecommune2.pk
        reponse = self.client.post(url_edit, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Modification de formulaire demandée')
        # Ville de départ
        self.assertContains(reponse, 'Boen')
        # Villes traversées
        self.assertContains(reponse, 'Roche.')
        # 2ème histo
        self.assertContains(reponse, ': Bard')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=ville_depart"')
        self.assertEqual(reponse.context['champ'], '[]')
        self.manifestation.refresh_from_db()
        for item in self.manifestation.history_json[idx:]:
            print(item)
        idx = len(self.manifestation.history_json)
        self.assertEqual(len(self.manifestation.history_json), 2)
        self.assertEqual(self.manifestation.history_json[1]['type'], 'int')
        self.assertIn('champ', self.manifestation.history_json[1])
        self.assertEqual(self.manifestation.history_json[1]['champ'], 'ville_depart')
        self.assertTrue(self.manifestation.modif_json[1]['done'])
        self.assertEqual(len(self.manifestation.instance_instruites.all()), 1)
        self.client.logout()

        print('**** test 7 demande modification villes traversées ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=villes_traversees"
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.modif_json), 3)
        self.assertFalse(self.manifestation.modif_json[2]['done'])
        self.assertIn('manif', self.manifestation.modif_json[2])
        self.assertEqual(self.manifestation.modif_json[2]['manif'], 'villes_traversees')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        self.assertIn('villes_traversees', reponse.context['champ'])
        self.client.logout()

        print('**** test 8 modification organisateur ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=demande', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Modification de formulaire demandée')
        self.assertEqual(len(self.manifestation.history_json), 2)
        self.assertIn('villes_traversees', reponse.context['champ'])
        # Modification des villes traversées
        data = {}
        data['villes_traversees'] = self.autrecommune3.pk
        reponse = self.client.post(url_edit, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Modification de formulaire demandée')
        # Villes traversées
        self.assertContains(reponse, 'Sauvain.')
        # 3ème histo
        self.assertContains(reponse, ': Roche')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=villes_traversees"')
        self.assertEqual(reponse.context['champ'], '[]')
        self.manifestation.refresh_from_db()
        for item in self.manifestation.history_json[idx:]:
            print(item)
        idx = len(self.manifestation.history_json)
        self.assertEqual(len(self.manifestation.history_json), 3)
        self.assertEqual(self.manifestation.history_json[2]['type'], 'list')
        self.assertIn('champ', self.manifestation.history_json[2])
        self.assertEqual(self.manifestation.history_json[2]['champ'], 'villes_traversees')
        self.assertTrue(self.manifestation.modif_json[2]['done'])
        self.assertEqual(len(self.manifestation.instance_instruites.all()), 1)
        self.client.logout()

        print('**** test 9 demande modifications multiples ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=date_fin"
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=villes_traversees"
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        url = "/evenement/" + str(self.manifestation.pk) + "/modification/?champ=ville_depart"
        self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.manifestation.refresh_from_db()
        self.assertEqual(len(self.manifestation.modif_json), 6)
        self.assertFalse(self.manifestation.modif_json[3]['done'])
        self.assertIn('manif', self.manifestation.modif_json[3])
        self.assertEqual(self.manifestation.modif_json[3]['manif'], 'date_fin')
        self.assertFalse(self.manifestation.modif_json[4]['done'])
        self.assertIn('manif', self.manifestation.modif_json[4])
        self.assertEqual(self.manifestation.modif_json[4]['manif'], 'villes_traversees')
        self.assertFalse(self.manifestation.modif_json[5]['done'])
        self.assertIn('manif', self.manifestation.modif_json[5])
        self.assertEqual(self.manifestation.modif_json[5]['manif'], 'ville_depart')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        self.assertIn('champ', reponse.context)
        self.assertIn('date_fin', reponse.context['champ'])
        self.assertIn('villes_traversees', reponse.context['champ'])
        self.assertIn('ville_depart', reponse.context['champ'])
        self.client.logout()

        print('**** test 10 modification organisateur ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=demande', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Modification de formulaire demandée')
        self.assertIn('date_fin', reponse.context['champ'])
        self.assertIn('villes_traversees', reponse.context['champ'])
        self.assertIn('ville_depart', reponse.context['champ'])
        # Modifications demandées
        date = self.manifestation.date_fin + timezone.timedelta(hours=10)
        data = {}
        data['date_fin'] = date
        data['ville_depart'] = self.autrecommune3.pk
        data['villes_traversees'] = [self.commune.pk, self.autrecommune.pk, self.autrecommune2.pk]
        reponse = self.client.post(url_edit, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Modification de formulaire demandée')
        # Ville de départ
        self.assertContains(reponse, 'Sauvain')
        # Villes traversées
        self.assertContains(reponse, 'Bard,')
        self.assertContains(reponse, 'Boen,')
        self.assertContains(reponse, 'Roche.')
        # 4ème histo
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=date_fin"')
        self.assertContains(reponse, ': Boen')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=ville_depart"')
        self.assertContains(reponse, ': Sauvain')
        self.assertContains(reponse, '<a href="/evenement/' + str(self.manifestation.pk) + '/historique/?name=villes_traversees"')
        self.assertEqual(reponse.context['champ'], '[]')
        self.manifestation.refresh_from_db()
        for item in self.manifestation.history_json[idx:]:
            print(item)
        self.assertEqual(len(self.manifestation.history_json), 6)
        self.assertEqual(self.manifestation.history_json[3]['type'], 'str')
        self.assertEqual(self.manifestation.history_json[3]['champ'], 'date_fin')
        self.assertTrue(self.manifestation.modif_json[3]['done'])
        self.assertEqual(self.manifestation.history_json[4]['type'], 'int')
        self.assertEqual(self.manifestation.history_json[4]['champ'], 'ville_depart')
        self.assertTrue(self.manifestation.modif_json[4]['done'])
        self.assertEqual(self.manifestation.history_json[5]['type'], 'list')
        self.assertEqual(self.manifestation.history_json[5]['champ'], 'villes_traversees')
        self.assertTrue(self.manifestation.modif_json[3]['done'])
        self.assertTrue(self.manifestation.modif_json[4]['done'])
        self.assertTrue(self.manifestation.modif_json[5]['done'])
        self.assertEqual(len(self.manifestation.instance_instruites.all()), 1)
        self.client.logout()

        # print(reponse.content.decode('utf-8'))
