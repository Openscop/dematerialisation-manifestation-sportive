# coding: utf-8
import datetime
import re
from django.urls import reverse
from .test_base import TestCommunClass
from django.contrib.auth.hashers import make_password as make
from ..factories import *
from bs4 import BeautifulSoup as Bs

from core.factories import UserFactory, GroupFactory
from instructions.factories import InstructionFactory
from structure.models.service import ServiceConsulte


class AvisTests(TestCommunClass):
    """ Tests de la base des avis """

    @classmethod
    def setUpTestData(cls):
        TestCommunClass.init_setup(cls)
        cls.avis_nb = 2
        # cls.instruction = InstructionFactory.create(manif=cls.manifestation)
        # cls.avis = Avis.objects.create(service_consulte_fk=cls.fede, service_consulte_origine_fk=cls.fede,
        #                                 instruction=cls.instruction)
        grp1 = GroupFactory.create(name='Administrateurs d\'instance')
        cls.admindinst = UserFactory.create(username='admindinst', password=make("123"), default_instance=cls.dep.instance)
        cls.admindinst.groups.add(grp1)
        cls.admindinst.organisation_m2m.add(cls.prefecture)
        cls.instruction = InstructionFactory(manif=cls.manifestation)

    def test_date_limite_reponse_conflit_autorisation(self):
        print('**** test 1 instructeur - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        # appel ajax pour avoir la liste
        url_intr_list = "/instructions/tableaudebord/" + str(self.prefecture.pk) + "/list/"
        reponse = self.client.get(url_intr_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action_requise = str(html.select("div.border-danger.liste-actions"))
        self.assertIn('Envoyer une demande d\'avis', action_requise)

        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'gendarmerie', 'array_id[]': self.ggd.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'gendarmerie', 'array_id[]': self.edsr.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'federation', 'array_id[]': self.fede.pk}, HTTP_HOST='127.0.0.1:8000')
        # Vérifier le passage en encours et le nombre d'avis manquants
        self.presence_avis('instructeur', 'encours', log=False)
        self.avis_ggd = Avis.objects.filter(service_consulte_fk=self.ggd).first()
        self.avis_edsr = Avis.objects.filter(service_consulte_fk=self.edsr).first()
        self.avis_fede = Avis.objects.filter(service_consulte_fk=self.fede).first()

        print('**** test 2 instructeur - changer date_limite_reponse ****')
        # url_list = "/instructions/" + str(self.instruction.pk) + "/list/"
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        reponse = self.vue_detail(reponse)
        self.assertContains(reponse, 'btn-date-form', count=3)
        # self.assertContains(reponse, 'date_rendu_avis', count=3)
        date_limite_reponse_avant_requete = self.avis_ggd.date_limite_reponse_dt
        self.client.post(reverse('instructions:date_limite_reponse_edit', kwargs={'pk': self.avis_ggd.pk}),
                        {'date_limite_reponse_dt': date_limite_reponse_avant_requete + datetime.timedelta(days=2)}, HTTP_HOST='127.0.0.1:8000')
        self.avis_ggd = Avis.objects.get(pk=self.avis_ggd.pk)  # on recharge avis_ggd car il a été MAJ en BDD
        date_limite_reponse_apres_requete = self.avis_ggd.date_limite_reponse_dt
        self.assertNotEqual(date_limite_reponse_avant_requete, date_limite_reponse_apres_requete)
        self.client.logout()

        print('**** test 3 fede - présence bouton édition date limite ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_fede', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.fede.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail
        reponse = self.vue_detail(reponse)
        # Vérifier que le bouton d'édition de la date limite reponse n'existe pas
        self.assertContains(reponse, 'btn-date-form', count=0)
        self.client.logout()

        print('**** test 4 agent ggd - présence bouton édition date limite ****')
        # Instruction de l'avis par l'agent ggd, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_ggd', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.ggd.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        # Vérifier que le bouton d'édition de la date limite reponse n'existe pas
        self.assertContains(reponse, 'btn-date-form', count=0)

        print('**** test 5 agent edsr - présence bouton édition date limite ****')
        # Instruction de l'avis par l'agent edsr, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_edsr', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.edsr.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, [self.cgd1.pk])
        # Vérifier que le bouton d'édition de la date limite reponse existe
        self.assertContains(reponse, 'btn-date-form', count=1)

    def test_delai_jour_conflit_autorisation(self):
        print('**** test 6 organisateur - pas d\'accès a l\'édition ****')
        # Connexion avec l'organisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        url = "/structure/administration_des_services/?service_id=" + str(self.cgd1.pk)
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'fa-pen', count=0)

        print('**** test 7 admin instance - accès a l\'édition ****')
        user = self.client.login(username="admindinst", password='123')
        self.assertTrue(user)
        url = f"/structure/update_service_consulte_delai_avis/{self.cgd1.pk}/"
        self.client.post(url, {'delai_jour_avis_int': 28}, HTTP_HOST='127.0.0.1:8000')
        self.cgd1 = ServiceConsulte.objects.get(pk=self.cgd1.pk)
        self.assertEqual(28, self.cgd1.delai_jour_avis_int)
