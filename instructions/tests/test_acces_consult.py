import re, json

from django.test import override_settings
from django.contrib.auth.hashers import make_password as make
from django.shortcuts import reverse
from bs4 import BeautifulSoup as Bs

from .test_base import TestCommunClass
from core.models import Instance
from core.factories import UserFactory
from instructions.models import Instruction
from structure.factories.structure import StructureOrganisatriceFactory
from structure.factories.service import ServiceConsulteFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class AccesConsultTests(TestCommunClass):
    """
    Test des autorisations d'accès
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('============ Acces consultation (Clt) =============')
        TestCommunClass.init_setup(cls)
        cls.autre_dep = DepartementFactory.create(name='43',
                                                  instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                                  instance__etat_s="ouvert")
        cls.agentlocal_edsr.delete()
        cls.autre_structure = StructureOrganisatriceFactory.create(precision_nom_s="structure_test2",
                                                                   commune_m2m=cls.commune, instance_fk=cls.dep.instance)
        cls.autre_arrondissement = ArrondissementFactory.create(name='Roanne', code='97', departement=cls.dep)
        cls.autre_prefecture = cls.autre_arrondissement.organisations.first()
        cls.autre_instructeur = UserFactory.create(username='autre_instructeur', password=make("123"),
                                                   default_instance=cls.autre_dep.instance, email='inst@test.fr')
        cls.autre_instructeur.organisation_m2m.add(cls.autre_prefecture)
        cls.autre_commiss = ServiceConsulteFactory(instance_fk=cls.dep.instance, precision_nom_s="Commissariat",
                                                   service_parent_fk=cls.ddsp, commune_m2m=cls.autrecommune,
                                                   type_service_fk__categorie_fk__nom_s="Police", type_service_fk__nom_s="Commissariat",
                                                   autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True)
        cls.autre_agent_commiss = UserFactory.create(username='autre_agent_commiss', password=make("123"),
                                                     default_instance=cls.dep.instance, email='comm@test.fr')
        cls.autre_agent_commiss.organisation_m2m.add(cls.autre_commiss)
        cls.autre_ddsp = ServiceConsulteFactory(departement_m2m=cls.autre_dep, instance_fk=cls.autre_dep.instance, precision_nom_s="DDSP",
                                                type_service_fk__abreviation_s="DDSP",
                                                type_service_fk__nom_s="Direction Départementale de la Sécurité Publique",
                                                type_service_fk__categorie_fk__nom_s="Police",
                                                admin_service_famille_b=True, admin_utilisateurs_subalternes_b=True,
                                                porte_entree_avis_b=True, autorise_rendre_avis_b=True,
                                                autorise_acces_consult_organisateur_b=True)
        cls.autre_agent_ddsp = UserFactory.create(username='autre_agent_ddsp', password=make("123"),
                                                  default_instance=cls.autre_dep.instance, email='ddsp@test.fr')
        cls.autre_agent_ddsp.organisation_m2m.add(cls.autre_ddsp)

    def test_Acces_Consult(self):
        """
    Test des autorisations d'accès
        """
        print('**** test 1 creation manif ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('1', nb_bloc.group('nb'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(declar, 'group'))
        # Soumettre la déclaration
        reponse = self.client.get(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Une demande d\'avis va être automatiquement envoyée à votre fédération délégataire ')
        self.assertContains(reponse, str(self.fede))
        self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        self.instruction = Instruction.objects.get(manif=self.manifestation)
        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().get_instance())
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="GGD"), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="EDSR"), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="DDSP"))
        print(self.manifestation.structure_organisatrice_fk, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline, end=" ; ")
        print(self.manifestation.activite.discipline.get_federations().first())
        self.affichage_avis()
        self.assertEqual(str(self.instruction), str(self.manifestation))
        self.client.logout()

        print('**** test 2 instructeur - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        # appel ajax pour avoir la liste
        url_intr_list = "/instructions/tableaudebord/" + str(self.prefecture.pk) + "/list/"
        reponse = self.client.get(url_intr_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action_requise = str(html.select("div.border-danger.liste-actions"))
        self.assertIn('Envoyer une demande d\'avis', action_requise)
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'police', 'array_id[]': self.ddsp.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'mairie', 'array_id[]': self.mairie1.pk}, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction)
        self.affichage_avis()
        # Vérifier le passage en encours et le nombre d'avis manquants
        self.presence_avis('instructeur', 'encours', log=False)
        self.client.logout()

        print('**** test 3 avis ddsp - distribution ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_ddsp', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.ddsp.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, self.commiss.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        # Vérification du passage en encours
        self.presence_avis('agent_ddsp', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()

        print('**** test 4 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_commiss', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.commiss.pk) + "/list/"
        reponse_list = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse_list)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rédiger le préavis', count=1)
        self.assertContains(reponse, 'Octroyer un accès en lecture', count=1)
        data = {
            'acces': 'complet',
            'model': 'Commissariat',
            'service_id': self.autre_commiss.pk,
        }
        preavis_pk = re.search('href="/instructions/avis/(?P<id>(\d))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(preavis_pk, 'group'))
        self.client.post('/instructions/acces/' + preavis_pk.group("id") + '/create/', data,
                         HTTP_HOST='127.0.0.1:8000', HTTP_REFERER='/instructions/preavis/' + preavis_pk.group("id") + '/')
        reponse = self.vue_detail(reponse_list)
        self.assertContains(reponse, 'Accès en lecture octroyé', count=1)
        self.assertContains(reponse, 'abr Commissariat Roche', count=1)
        self.client.logout()

        print('**** test 5 preavis lecture commissariat ****')
        self.assertTrue(self.client.login(username='autre_agent_commiss', password='123'))
        pk = str(self.autre_commiss.pk)
        reponse = self.client.get('/instructions/tableaudebord/' + pk + '/ajax/', HTTP_HOST='127.0.0.1:8000')
        rep_json = json.loads(reponse.content.decode('utf-8'))
        reponse = self.client.get('/instructions/tableaudebord/' + pk + '/', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(rep_json['lecture_count'], 1)
        consult = re.search('url_consultation_liste = "(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(consult, 'group'))
        page = self.client.get(consult.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', page.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        self.assertIn('avis', detail.group(('url')))
        page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test')
        self.assertContains(page, 'Suivi de l\'instruction')
        self.assertContains(page, 'Aucune action possible')
        self.assertContains(page, 'Octroyé le')
        self.assertContains(page, 'par Bob ROBERT')
        self.assertContains(page, 'de abr Commissariat Bard')
        self.client.logout()

        print('**** test 6 preavis lecture révocation ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_commiss', 'encours')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.commiss.pk) + "/list/"
        reponse_list = self.client.get(url_list + '?filtre_etat=demarre', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse_list)
        acces_id = re.search('acces" id="acces_(?P<id>(\d))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(acces_id, 'group'))
        retour = self.client.post("/instructions/acces/" + acces_id.group('id') + "/remove/", HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, 'done')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse_list)
        self.assertNotContains(reponse, 'Accès en lecture octroyé')
        data = {
            'acces': 'complet',
            'model': 'DDSP',
            'service_id': self.autre_ddsp.pk,
        }
        self.client.post('/instructions/acces/' + preavis_pk.group("id") + '/create/', data,
                         HTTP_HOST='127.0.0.1:8000', HTTP_REFERER='/instructions/preavis/' + preavis_pk.group("id") + '/')
        reponse = self.vue_detail(reponse_list)
        self.assertContains(reponse, 'Accès en lecture octroyé', count=1)
        self.assertContains(reponse, 'DDSP (43)', count=1)
        self.client.logout()

        print('**** test 7 preavis consultation none ****')
        reponse = self.presence_avis('autre_agent_commiss', 'none')
        self.assertNotContains(reponse, 'en lecture (1)')
        # Accès direct
        page = self.client.get(consult.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Aucune manifestation déclarée pour le moment', count=1)
        page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, "Vous n'avez pas les droits d'accès à cette page", status_code=403)
        # print(page.content.decode('utf-8'))
        self.client.logout()

        print('**** test 8 preavis consultation DDSP ****')
        self.assertTrue(self.client.login(username='autre_agent_ddsp', password='123'))
        pk = str(self.autre_ddsp.pk)
        reponse = self.client.get('/instructions/tableaudebord/' + pk + '/ajax/', HTTP_HOST='127.0.0.1:8000')
        rep_json = json.loads(reponse.content.decode('utf-8'))
        reponse = self.client.get('/instructions/tableaudebord/' + pk + '/', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(rep_json['lecture_count'], 1)
        consult = re.search('url_consultation_liste = "(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(consult, 'group'))
        page = self.client.get(consult.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', page.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        self.assertIn('avis', detail.group('url'))
        page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test')
        self.assertContains(page, 'Suivi de l\'instruction')
        self.assertContains(page, 'Aucune action possible')
        self.assertContains(page, 'Octroyé le')
        self.assertContains(page, 'par Bob ROBERT')
        self.assertContains(page, 'de abr Commissariat Bard')
        self.client.logout()

        print('**** test 9 preavis consultation révocation ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_commiss', 'encours')
        # appel ajax pour avoir la liste
        pk = str(self.commiss.pk)
        reponse_list = self.client.get('/instructions/tableaudebord/' + pk + '/list/?filtre_etat=demarre', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse_list)
        acces_id = re.search('acces" id="acces_(?P<id>(\d))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(acces_id, 'group'))
        retour = self.client.post("/instructions/acces/" + acces_id.group('id') + "/remove/", HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, 'done')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse_list)
        self.assertNotContains(reponse, 'Le dossier est accessible en lecture')
        self.client.logout()

        print('**** test 10 preavis consultation none ****')
        reponse = self.presence_avis('autre_agent_ddsp', 'none')
        self.assertNotContains(reponse, 'en lecture (1)')
        # Accès direct
        page = self.client.get(consult.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Aucune manifestation déclarée pour le moment', count=1)
        page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, "Vous n'avez pas les droits d'accès à cette page", status_code=403)
        # print(page.content.decode('utf-8'))
        self.client.logout()

        print('**** test 11 avis DDSP ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_ddsp', 'encours')
        # appel ajax pour avoir la liste
        pk = str(self.ddsp.pk)
        reponse_list = self.client.get('/instructions/tableaudebord/' + pk + '/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse_list)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Octroyer un accès en lecture', count=1)
        data = {
            'acces': 'complet',
            'model': 'Commissariat',
            'service_id': self.autre_commiss.pk,
        }
        page = self.client.post('/instructions/acces/' + preavis_pk.group("id") + '/create/', data, follow=True,
                                HTTP_HOST='127.0.0.1:8000', HTTP_REFERER='/instructions/preavis/' + preavis_pk.group("id") + '/')
        self.assertContains(page, "pas concerné par cet avis")
        avis_pk = re.search('href="/instructions/avis/(?P<id>(\d))/rediger', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(avis_pk, 'group'))
        self.client.post('/instructions/acces/' + avis_pk.group("id") + '/create/', data,
                         HTTP_HOST='127.0.0.1:8000', HTTP_REFERER='/instructions/avis/' + avis_pk.group("id") + '/')
        reponse = self.vue_detail(reponse_list)
        self.assertContains(reponse, 'Accès en lecture octroyé', count=1)
        self.assertContains(reponse, 'abr Commissariat Roche', count=1)
        data = {
            'acces': 'complet',
            'model': 'DDSP',
            'service_id': self.autre_ddsp.pk,
        }
        self.client.post('/instructions/acces/' + avis_pk.group("id") + '/create/', data,
                         HTTP_HOST='127.0.0.1:8000', HTTP_REFERER='/instructions/avis/' + avis_pk.group("id") + '/')
        reponse = self.vue_detail(reponse_list)
        self.assertContains(reponse, 'Accès en lecture octroyé')
        self.assertContains(reponse, 'abr Commissariat Roche', count=1)
        self.assertContains(reponse, 'DDSP DDSP (43)', count=1)
        self.client.logout()

        print('**** test 12 avis consultation commissariat ****')
        self.assertTrue(self.client.login(username='autre_agent_commiss', password='123'))
        pk = str(self.autre_commiss.pk)
        reponse = self.client.get('/instructions/tableaudebord/' + pk + '/ajax/', HTTP_HOST='127.0.0.1:8000')
        rep_json = json.loads(reponse.content.decode('utf-8'))
        reponse = self.client.get('/instructions/tableaudebord/' + pk + '/', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(rep_json['lecture_count'], 1)
        consult = re.search('url_consultation_liste = "(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(consult, 'group'))
        page = self.client.get(consult.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', page.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        self.assertIn('avis', detail.group('url'))
        page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test')
        self.assertContains(page, 'Suivi de l\'instruction')
        self.assertContains(page, 'Aucune action possible')
        self.assertContains(page, 'Octroyé le')
        self.assertContains(page, 'par Bob ROBERT')
        self.assertContains(page, 'de DDSP')
        self.client.logout()

        print('**** test 13 avis consultation DDSP ****')
        self.assertTrue(self.client.login(username='autre_agent_ddsp', password='123'))
        pk = str(self.autre_ddsp.pk)
        reponse = self.client.get('/instructions/tableaudebord/' + pk + '/ajax/', HTTP_HOST='127.0.0.1:8000')
        rep_json = json.loads(reponse.content.decode('utf-8'))
        reponse = self.client.get('/instructions/tableaudebord/' + pk + '/', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(rep_json['lecture_count'], 1)
        consult = re.search('url_consultation_liste = "(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(consult, 'group'))
        page = self.client.get(consult.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', page.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        self.assertIn('avis', detail.group('url'))
        page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test')
        self.assertContains(page, 'Suivi de l\'instruction')
        self.assertContains(page, 'Aucune action possible')
        self.assertContains(page, 'Octroyé le')
        self.assertContains(page, 'par Bob ROBERT')
        self.assertContains(page, 'de DDSP')
        self.client.logout()

        print('**** test 14 instructeur ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse_list = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse_list)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Octroyer un accès en lecture', count=1)
        data = {
            'acces': 'complet',
            'model': 'DDSP',
            'service_id': self.autre_ddsp.pk,
        }
        instruc_pk = re.search('href="/instructions/(?P<id>(\d+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(instruc_pk, 'group'))
        page = self.client.post('/instructions/acces/' + instruc_pk.group("id") + '/create/', data,
                                HTTP_HOST='127.0.0.1:8000', HTTP_REFERER='/instructions/' + instruc_pk.group("id") + '/')
        self.assertContains(page, "Un accès en lecture existe déjà pour ce service")
        data = {
            'acces': 'complet',
            'model': 'Prefecture',
            'service_id': self.autre_prefecture.pk
        }
        self.client.post('/instructions/acces/' + instruc_pk.group("id") + '/create/', data,
                         HTTP_HOST='127.0.0.1:8000', HTTP_REFERER='/instructions/' + instruc_pk.group("id") + '/')
        reponse = self.vue_detail(reponse_list)
        self.assertContains(reponse, 'Accès en lecture octroyé', count=2)
        self.assertContains(reponse, 'Préfecture de Roanne', count=1)
        self.assertContains(reponse, 'abr Commissariat Roche', count=1)
        self.assertContains(reponse, 'DDSP DDSP (43)', count=1)
        self.client.logout()

        print('**** test 15 consultation instruction pref ****')
        self.assertTrue(self.client.login(username='autre_instructeur', password='123'))
        pk = str(self.autre_prefecture.pk)
        reponse = self.client.get('/instructions/tableaudebord/' + pk + '/ajax/', HTTP_HOST='127.0.0.1:8000')
        rep_json = json.loads(reponse.content.decode('utf-8'))
        reponse = self.client.get('/instructions/tableaudebord/' + pk + '/', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(rep_json['lecture_count'], 1)
        consult = re.search('url_consultation_liste = "(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(consult, 'group'))
        page = self.client.get(consult.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', page.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        self.assertIn('instruction', detail.group('url'))
        page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test')
        self.assertContains(page, 'Suivi de l\'instruction')
        self.assertContains(page, 'Accès en lecture seule')
        self.assertContains(page, 'par Bob ROBERT')
        self.assertContains(page, 'de Préfecture de Montbrison')
        self.client.logout()

        #TODO :  tester avec organisateur vers organisateur et instructeur
        # et tester messages envoyés
