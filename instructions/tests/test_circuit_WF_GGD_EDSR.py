import re

from django.test import override_settings
from django.shortcuts import reverse
from bs4 import BeautifulSoup as Bs

from post_office.models import Email

from .test_base import TestCommunClass
from core.models import Instance
from instructions.models import Instruction, Avis
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from messagerie.models import Message
from structure.models.service import ServiceConsulteInteraction


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class Circuit_GGD_EDSRTests(TestCommunClass):
    """
    Test du circuit d'instance GGD_EDSR pour une Dnm
        circuit_GGD : Avis GGD
        instruction par département
        openrunner false par défaut
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========== WF_GGD_EDSR (Clt) ===========')
        # Création des objets sur le 78
        cls.dep = dep = DepartementFactory.create(name='78',
                                                  instance__name="instance de test",
                                                  instance__instruction_mode=Instance.IM_CIRCUIT_A)
        ggd = dep.organisation_set.get(precision_nom_s="GGD")
        ggd.porte_entree_avis_b = True
        ggd.service_parent_fk = None
        ggd.save()
        edsr = dep.organisation_set.get(precision_nom_s="EDSR")
        edsr.porte_entree_avis_b = False
        edsr.service_parent_fk = ggd
        edsr.save()
        ServiceConsulteInteraction.objects.create(service_entrant_fk=ggd, service_sortant_fk=edsr, type_lien_s='mandater')
        arrondissement = ArrondissementFactory.create(name='Versailles', code='99', departement=dep)
        cls.prefecture = arrondissement.organisations.first()
        cls.commune = CommuneFactory(name='Plaisir', arrondissement=arrondissement)
        cls.mairie1 = cls.commune.organisation_set.first()
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)
        cls.mairie2 = cls.autrecommune.organisation_set.first()
        TestCommunClass.init_setup(cls)
        cls.agentlocal_edsr.delete()
        cls.avis_nb = 6

    def test_Circuit_GGD_EDSR(self):
        """
        Test des différentes étapes du circuit GGD_EDSR pour une autorisationNM
        """

        # def print(string="", end=None):
        #     # Supprimer les prints hors debug
        #     pass

        print('**** test 1 creation manif ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('1', nb_bloc.group('nb'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        self.assertTrue(hasattr(declar, 'group'))
        self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.client.logout()

        self.instruction = Instruction.objects.get(manif=self.manifestation)
        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().get_instance())
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="GGD"), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="EDSR"), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="DDSP"))
        print(self.manifestation.structure_organisatrice_fk, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline, end=" ; ")
        print(self.manifestation.activite.discipline.get_federations().first())
        self.affichage_avis()
        self.assertEqual(str(self.instruction), str(self.manifestation))
        self.id_avis_fede = Avis.objects.last().id

        print('**** test 2 vérification avis; 0 pour tous sauf la fédération ****')
        # Vérification des avis des divers agents
        # GGD
        self.presence_avis('agent_ggd', 'none')
        self.client.logout()
        # EDSR
        self.presence_avis('agent_edsr', 'none')
        self.client.logout()
        # Mairie
        self.presence_avis('agent_mairie', 'none')
        self.client.logout()
        # CG
        self.presence_avis('agent_cg', 'none')
        self.client.logout()
        # CGSuperieur
        self.presence_avis('agent_cgsup', 'none')
        self.client.logout()
        # SDIS
        self.presence_avis('agent_sdis', 'none')
        self.client.logout()

        print('**** test 3 instructeur - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        # appel ajax pour avoir la liste
        url_intr_list = "/instructions/tableaudebord/" + str(self.prefecture.pk) + "/list/"
        reponse = self.client.get(url_intr_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action_requise = str(html.select("div.border-danger.liste-actions"))
        self.assertIn('Envoyer une demande d\'avis', action_requise)
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'mairie', 'array_id[]': self.mairie1.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'conseil_departemental', 'array_id[]': self.cg.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'police', 'array_id[]': self.ddsp.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'gendarmerie', 'array_id[]': self.ggd.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'pompier', 'array_id[]': self.sdis.pk}, HTTP_HOST='127.0.0.1:8000')
        self.affichage_avis()
        # Vérifier le passage en encours et le nombre d'avis manquants
        self.presence_avis('instructeur', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        self.id_avis_ggd = Avis.objects.filter(service_consulte_fk=self.ggd).last().id
        self.id_avis_ddsp = Avis.objects.filter(service_consulte_fk=self.ddsp).last().id
        self.id_avis_cg = Avis.objects.filter(service_consulte_fk=self.cg).last().id
        self.id_avis_sdis = Avis.objects.filter(service_consulte_fk=self.sdis).last().id
        self.id_avis_mairie = Avis.objects.filter(service_consulte_fk=self.mairie1).last().id

        print('**** test 4 avis fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_fede', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.fede.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 28 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # Vérifier le passage en rendu
        self.presence_avis('agent_fede', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        print('**** test 5 vérification avis; 0 pour EDSR, CGD, USG et Brigade ****')
        # Vérification des avis des divers agents
        # EDSR
        self.presence_avis('agent_edsr', 'none')
        self.client.logout()
        # CGD
        self.presence_avis('agent_cgd1', 'none')
        self.client.logout()
        # USG
        self.presence_avis('agent_usg', 'none')
        self.client.logout()
        # BRG
        self.presence_avis('agent_brg1', 'none')
        self.client.logout()

        print('**** test 6 avis ggd - transmission ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_ggd', 'nouveau')
        Email.objects.all().delete()
        Message.objects.all().delete()
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.ggd.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.mandater_avis(reponse, self.edsr.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.affichage_avis()
        # Vérification du passage en distribue
        self.presence_avis('agent_ggd', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content.decode('utf-8'))
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(messages), 2)
        self.assertIn('Avis mandaté pour la manifestation', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_edsr)
        self.assertEqual(messages[0].object_id, self.id_avis_ggd)          # Avis Ggd
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.edsr.email_s])
        self.assertEqual(outbox[0].to, [self.agent_edsr.email])
        self.assertEqual(outbox[0].subject, 'Avis mandaté')

        print('**** test 7 vérification avis; 0 pour CGD, USG et Brigade ****')
        # Vérification des avis des divers agents
        # CGD
        self.presence_avis('agent_cgd1', 'none')
        self.client.logout()
        # USG
        self.presence_avis('agent_usg', 'none')
        self.client.logout()
        # BRG
        self.presence_avis('agent_brg1', 'none')
        self.client.logout()

        print('**** test 8 avis edsr - distribution ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_edsr', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.edsr.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, [self.cgd1.pk, self.usg.pk])
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.filter(avis_parent_fk=self.instruction.avis.get(service_consulte_origine_fk=self.ggd))
        for pr in preavis:
            print(pr, end=" ; ")
            print(pr.etat)
        self.affichage_avis()
        # Vérification du passage en encours
        self.presence_avis('agent_edsr', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '2 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()

        print('**** test 9 preavis cgd ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en danger
        self.presence_avis('agent_cgd1', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cgd1.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.filter(avis_parent_fk=self.instruction.avis.get(service_consulte_origine_fk=self.ggd))
        for pr in preavis:
            print(pr, end=" ; ")
            print(pr.etat)
        self.affichage_avis()
        # Vérification du passage en rendu
        self.presence_avis('agent_cgd', 'rendu', log=False)
        self.client.logout()

        print('**** test 10 preavis usg ****')
        # Instruction du préavis par le usg, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_usg', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.usg.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.filter(avis_parent_fk=self.instruction.avis.get(service_consulte_origine_fk=self.ggd))
        for pr in preavis:
            print(pr, end=" ; ")
            print(pr.etat)
        self.affichage_avis()
        # Vérification du passage en rendu
        self.presence_avis('agent_usg', 'rendu', log=False)
        self.client.logout()

        print('**** test 11 avis edsr - soumission ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en encours
        self.presence_avis('agent_edsr', 'encours')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.edsr.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.adresser_avis(reponse, self.ggd.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.affichage_avis()
        # vérification de la présence de l'événement en rendu
        self.presence_avis('agent_edsr', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()

        print('**** test 12 avis ggd ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_ggd', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.ggd.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # vérification de la présence de l'événement en rendu
        self.presence_avis('agent_ggd', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        print('**** test 13 vérification avis; 0 pour Commissariat ****')
        # Vérification des avis des divers agents
        # CGD
        self.presence_avis('agent_commiss', 'none')
        self.client.logout()

        print('**** test 14 agent ddsp - distribution ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_ddsp', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.ddsp.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, self.commiss.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.ddsp))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérifier le passage en encours
        self.presence_avis('agent_ddsp', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()

        print('**** test 15 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_commiss', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.commiss.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.ddsp))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérification du passage en rendu
        self.presence_avis('agent_commiss', 'rendu', log=False)
        self.client.logout()

        print('**** test 16 avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en encours
        self.presence_avis('agent_ddsp', 'encours')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.ddsp.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # Vérifier le passage en rendu
        self.presence_avis('agent_ddsp', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        print('**** test 17 avis mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_mairie', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.mairie1.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_specifique=serviceconsulte&filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # Vérifier le passage en rendu
        self.presence_avis('agent_mairie', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        print('**** test 18 vérification avis; 0 pour CGService ****')
        # Vérification des avis des divers agents
        # CGService
        self.presence_avis('agent_cgserv', 'none')
        self.client.logout()

        print('**** test 19 avis cg - distribution ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_cg', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cg.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, self.cgserv.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.cg))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérifier le passage en encours
        self.presence_avis('agent_cg', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()

        print('**** test 20 preavis cgservice ****')
        # Instruction du préavis par le cgservice, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_cgserv', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cgserv.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.cg))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérification du passage en rendu
        self.presence_avis('agent_cgserv', 'rendu', log=False)
        self.client.logout()

        print('**** test 21 vérification avis; 0 pour CGSupérieur ****')
        # Vérification des avis des divers agents
        # CGSuperieur
        self.presence_avis('agent_cgsup', 'none')
        self.client.logout()

        print('**** test 22 agent cg - soumission ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en encours
        self.presence_avis('agent_cg', 'encours')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cg.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.adresser_avis(reponse, self.cgsup.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.affichage_avis()
        # vérification de la présence de l'événement en rendu
        self.presence_avis('agent_cg', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()

        print('**** test 23 agent cgsup ****')
        # Instruction de l'avis par le cgsup, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_cgsup', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cgsup.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # Vérifier le passage en rendu
        self.presence_avis('agent_cgsup', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        print('**** test 24 vérification avis; 0 pour Groupement, codis et cis ****')
        # Vérification des avis des divers agents
        # CIS
        self.presence_avis('agent_cis', 'none')
        self.client.logout()
        # CODIS
        self.presence_avis('agent_codis', 'none')
        self.client.logout()
        # SDIS groupement
        self.presence_avis('agent_group', 'none')
        self.client.logout()

        print('**** test 25 avis sdis - distribution ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_sdis', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.sdis.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, self.group.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.sdis))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérification du passage en encours
        self.presence_avis('agent_sdis', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()

        print('**** test 26 preavis groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_group', 'nouveau')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.group.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.sdis))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérification du passage en rendu
        self.presence_avis('agent_group', 'rendu', log=False)
        self.client.logout()

        print('**** test 27 avis sdis ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en encours
        self.presence_avis('agent_sdis', 'encours')
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.sdis.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # Vérifier le passage en rendu
        self.presence_avis('agent_sdis', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'avis manquants')
        self.client.logout()

        print('**** test 31 instructeur - interdiction ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en encours
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'avis manquants')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Ajouter un document officiel', count=1)
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        publish_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Ajouter un document officiel', reponse.content.decode('utf-8'))
        if hasattr(publish_form, 'group'):
            with open('/tmp/recepisse_declaration.txt') as file1:
                self.client.post(publish_form.group('url'), {'nature': '0', 'fichier': file1}, follow=True, HTTP_HOST='127.0.0.1:8000')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=interdit', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction)
        # Vérifier le passage en info et le nombre d'avis manquants
        self.assertContains(reponse, 'table_termine', count=1)
