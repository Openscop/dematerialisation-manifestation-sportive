import re, time

from django.test import tag, override_settings

from selenium.webdriver.common.by import By

from .test_base_selenium import SeleniumCommunClass
from administrative_division.factories import ArrondissementFactory


class InstructionCircuit_SimpleTests(SeleniumCommunClass):
    """
    Test des circuits simples avec sélénium pour une Dnm
        circuit_GGD : Avis EDSR
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ Circuit Simple (Sel) =============')
        # ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        SeleniumCommunClass.init_setup(cls)
        cls.autre_arrondissement = ArrondissementFactory.create(name='Roanne', code='97', departement=cls.dep)
        cls.autre_prefecture = cls.autre_arrondissement.organisations.first()
        cls.avis_nb = 4
        super().setUpClass()

    @tag('selenium')
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/', DEBUG=True)
    def test_Circuit_Simple(self):
        """
        Test des circuits simples SDIS et DDSP pour une autorisationNM
        """

        print('**** test 0 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 2 instructeur - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Changer l'assignation").click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Me l'assigner").click()
        time.sleep(self.DELAY * 5)
        self.distribuer_avis(['Police', 'Pompier', 'Mairie'])
        self.assertIn('Pompier', self.selenium.page_source)
        self.assertIn('SDIS', self.selenium.page_source)
        self.assertIn('Police', self.selenium.page_source)
        self.assertIn('DDSP', self.selenium.page_source)
        self.assertIn('Mairie', self.selenium.page_source)

        # Vérifier le passage en encours et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1600)")
        time.sleep(self.DELAY*3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 3 avis fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.connexion('agent_fede')
        self.presence_avis('agent_fede', 'nouveau')
        self.assertIn('Délai : 28 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_fede', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 4 avis ddsp ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_service_ddsp', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 5 avis mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en nouveau
        self.connexion('agent_mairie')
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_elements(By.CLASS_NAME, 'nav-item')[1].click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_mairie', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_elements(By.CLASS_NAME, 'nav-item')[1].click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_mairie', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 6 avis sdis ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en encours
        self.connexion('agent_sdis')
        self.presence_avis('agent_sdis', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_sdis', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.deconnexion()
