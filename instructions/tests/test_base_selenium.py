import re, os, time

from django.test import override_settings
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.hashers import make_password as make
from django.contrib.contenttypes.models import ContentType

from post_office.models import EmailTemplate
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.by import By
from allauth.account.models import EmailAddress

from core.models import Instance, User, ConfigurationGlobale
from core.factories import UserFactory
from evenements.factories import DcnmFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administrative_division.models.region import Region
from sports.factories import ActiviteFactory
from structure.models.service import ServiceConsulteInteraction
from structure.factories.structure import StructureOrganisatriceFactory
from structure.factories.service import (ServiceConsulteFactory, FederationFactory)


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class SeleniumCommunClass(StaticLiveServerTestCase):
    """
    Méthodes communes aux tests Sélénium
    """
    DELAY = 0.35

    def init_setup(self):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets sur le 42
            Création des utilisateurs
            Création de l'événement
        """
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        self.selenium = WebDriver()
        self.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        self.selenium.set_window_size(800, 1000)
        ConfigurationGlobale.objects.create(pk=1, mail_action=True, mail_suivi=True, mail_tracabilite=True, mail_actualite=True)

        if not hasattr(self, 'dep'):
            dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                            instance__etat_s="ouvert", instance__activer_beta=True)
            self.dep = dep
            self.dep.region = Region.objects.create(nom='Region test')
            self.dep.save()
            arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
            self.prefecture = arrondissement.organisations.first()
            self.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
            self.mairie1 = self.commune.organisation_set.first()
            self.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)
            self.mairie2 = self.autrecommune.organisation_set.first()
        else:
            dep = self.dep

        # Création des utilisateurs
        self.structure = StructureOrganisatriceFactory.create(precision_nom_s="structure_test",
                                                              commune_m2m=self.commune, instance_fk=dep.instance)
        self.organisateur = UserFactory.create(username='organisateur', password=make("123"), default_instance=dep.instance, email='orga@test.fr')
        self.organisateur.organisation_m2m.add(self.structure)
        self.instructeur = UserFactory.create(username='instructeur', password=make("123"), default_instance=dep.instance, email='inst@test.fr')
        self.instructeur.organisation_m2m.add(self.prefecture)
        self.agent_fede = UserFactory.create(username='agent_fede', password=make("123"), default_instance=dep.instance, email='fede@test.fr')
        self.activ = ActiviteFactory.create()
        self.fede = FederationFactory.create(instance_fk=dep.instance, departement_m2m=dep, discipline_fk=self.activ.discipline)
        self.agent_fede.organisation_m2m.add(self.fede)
        self.agent_ggd = UserFactory.create(username='agent_ggd', password=make("123"), default_instance=dep.instance, email='ggd@test.fr')
        self.ggd = dep.organisation_set.get(precision_nom_s="GGD")
        self.agent_ggd.organisation_m2m.add(self.ggd)
        self.edsr = dep.organisation_set.get(precision_nom_s="EDSR")
        self.agent_edsr = UserFactory.create(username='agent_edsr', password=make("123"), default_instance=dep.instance, email='edsr@test.fr')
        self.agent_edsr.organisation_m2m.add(self.edsr)
        self.agentlocal_edsr = UserFactory.create(username='agentlocal_edsr', password=make("123"), default_instance=dep.instance, email='edsrlocal@test.fr')
        self.agentlocal_edsr.organisation_m2m.add(self.edsr)
        self.cgd1 = ServiceConsulteFactory(instance_fk=dep.instance, precision_nom_s="CGD1", service_parent_fk=self.edsr,
                                           type_service_fk__categorie_fk__nom_s="Gendarmerie", type_service_fk__nom_s="CGD",
                                           autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True, commune_m2m=self.commune)
        self.agent_cgd1 = UserFactory.create(username='agent_cgd1', password=make("123"), default_instance=dep.instance, email='cgd1@test.fr')
        self.agent_cgd1.organisation_m2m.add(self.cgd1)
        self.cgd2 = ServiceConsulteFactory(instance_fk=dep.instance, precision_nom_s="CGD2", service_parent_fk=self.edsr,
                                           type_service_fk__categorie_fk__nom_s="Gendarmerie", type_service_fk__nom_s="CGD",
                                           autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True, commune_m2m=self.autrecommune)
        self.agent_cgd2 = UserFactory.create(username='agent_cgd2', password=make("123"), default_instance=dep.instance, email='cgd2@test.fr')
        self.agent_cgd2.organisation_m2m.add(self.cgd2)
        self.usg = ServiceConsulteFactory(instance_fk=dep.instance, precision_nom_s="BMO", service_parent_fk=self.edsr,
                                          type_service_fk__categorie_fk__nom_s="Gendarmerie", type_service_fk__nom_s="BMO",
                                          autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True, commune_m2m=self.autrecommune)
        self.agent_usg = UserFactory.create(username='agent_usg', password=make("123"), default_instance=dep.instance, email='usg@test.fr')
        self.agent_usg.organisation_m2m.add(self.usg)
        self.brigade1 = ServiceConsulteFactory(instance_fk=dep.instance, precision_nom_s="BTA1", service_parent_fk=self.cgd1,
                                               type_service_fk__categorie_fk__nom_s="Gendarmerie", type_service_fk__nom_s="BTA",
                                               autorise_acces_consult_organisateur_b=True, commune_m2m=self.commune)
        self.agent_brg1 = UserFactory.create(username='agent_brg1', password=make("123"), default_instance=dep.instance, email='brg1@test.fr')
        self.agent_brg1.organisation_m2m.add(self.brigade1)
        self.brigade2 = ServiceConsulteFactory(instance_fk=dep.instance, precision_nom_s="BTA2", service_parent_fk=self.cgd2,
                                               type_service_fk__categorie_fk__nom_s="Gendarmerie", type_service_fk__nom_s="BTA",
                                               autorise_acces_consult_organisateur_b=True, commune_m2m=self.commune)
        self.agent_brg2 = UserFactory.create(username='agent_brg2', password=make("123"), default_instance=dep.instance, email='brg2@test.fr')
        self.agent_brg2.organisation_m2m.add(self.brigade2)
        self.agent_ddsp = UserFactory.create(username='agent_ddsp', password=make("123"), default_instance=dep.instance, email='ddsp@test.fr')
        self.ddsp = dep.organisation_set.get(precision_nom_s="DDSP")
        self.agent_ddsp.organisation_m2m.add(self.ddsp)
        self.commiss = ServiceConsulteFactory(instance_fk=dep.instance, precision_nom_s="Commissariat", service_parent_fk=self.ddsp,
                                              type_service_fk__categorie_fk__nom_s="Police", type_service_fk__nom_s="Commissariat",
                                              autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True, commune_m2m=self.commune)
        self.agent_commiss = UserFactory.create(username='agent_commiss', password=make("123"), default_instance=dep.instance, email='comm@test.fr')
        self.agent_commiss.organisation_m2m.add(self.commiss)
        self.agent_mairie = UserFactory.create(username='agent_mairie', password=make("123"), default_instance=dep.instance, email='mair@test.fr')
        self.agent_mairie.organisation_m2m.add(self.mairie1)
        self.agent_mairie2 = UserFactory.create(username='agent_mairie2', password=make("123"), default_instance=dep.instance, email='mair2@test.fr')
        self.agent_mairie2.organisation_m2m.add(self.mairie2)

        self.cg = dep.organisation_set.get(precision_nom_s="CD")
        self.agent_cg = UserFactory.create(username='agent_cg', password=make("123"), default_instance=dep.instance, email='cg@test.fr')
        self.agent_cg.organisation_m2m.add(self.cg)
        self.cgserv = ServiceConsulteFactory.create(instance_fk=dep.instance, precision_nom_s="STD", service_parent_fk=self.cg,
                                                    type_service_fk__categorie_fk__nom_s="Conseil départemental", type_service_fk__nom_s="STD",
                                                    autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True, departement_m2m=dep)
        self.agent_cgserv = UserFactory.create(username='agent_cgserv', password=make("123"), default_instance=dep.instance, email='cgserv@test.fr')
        self.agent_cgserv.organisation_m2m.add(self.cgserv)
        self.cgsup = dep.organisation_set.get(precision_nom_s="CDSUP")
        self.agent_cgsup = UserFactory.create(username='agent_cgsup', password=make("123"), default_instance=dep.instance, email='cgsup@test.fr')
        self.agent_cgsup.organisation_m2m.add(self.cgsup)
        self.sdis = dep.organisation_set.get(precision_nom_s="SDIS")
        self.agent_sdis = UserFactory.create(username='agent_sdis', password=make("123"), default_instance=dep.instance, email='sdis@test.fr')
        self.agent_sdis.organisation_m2m.add(self.sdis)
        self.group = ServiceConsulteFactory.create(instance_fk=dep.instance, precision_nom_s="GROUP", service_parent_fk=self.sdis,
                                                   type_service_fk__categorie_fk__nom_s="Pompier", type_service_fk__nom_s="GROUP",
                                                   autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True, departement_m2m=dep)
        self.agent_group = UserFactory.create(username='agent_group', password=make("123"), default_instance=dep.instance, email='group@test.fr')
        self.agent_group.organisation_m2m.add(self.group)
        self.codis = dep.organisation_set.get(precision_nom_s="CODIS")
        self.agent_codis = UserFactory.create(username='agent_codis', password=make("123"), default_instance=dep.instance, email='codis@test.fr')
        self.agent_codis.organisation_m2m.add(self.codis)
        self.cis = ServiceConsulteFactory.create(instance_fk=dep.instance, precision_nom_s="CIS", service_parent_fk=self.group,
                                                 type_service_fk__categorie_fk__nom_s="Pompier", type_service_fk__nom_s="CIS",
                                                 autorise_acces_consult_organisateur_b=True, commune_m2m=self.commune)
        self.agent_cis = UserFactory.create(username='agent_cis', password=make("123"), default_instance=dep.instance, email='cis@test.fr')
        self.agent_cis.organisation_m2m.add(self.cis)
        self.cdsr = dep.organisation_set.get(precision_nom_s="CDSR")

        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.ddsp, service_sortant_fk=self.commiss, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.edsr, service_sortant_fk=self.cgd1, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.edsr, service_sortant_fk=self.cgd2, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.edsr, service_sortant_fk=self.usg, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.edsr, service_sortant_fk=self.ggd, type_lien_s='adresser')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.sdis, service_sortant_fk=self.group,  type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.cg, service_sortant_fk=self.cgserv, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.cg, service_sortant_fk=self.cgsup, type_lien_s='adresser')

        #Création des boites mails:
        for user in User.objects.all():
            EmailAddress.objects.create(user=user, email=f'{user}{user.pk}@example.com', primary=True,
                                        verified=True)


        EmailTemplate.objects.create(name='new_msg')
        for user in User.objects.all():
            user.optionuser.notification_mail = True
            user.optionuser.action_mail = True
            user.optionuser.save()

        self.manifestation = DcnmFactory.create(ville_depart=self.commune, structure_organisatrice_fk=self.structure, nom='Manifestation_Test',
                                                villes_traversees=(self.autrecommune,), activite=self.activ)
        self.manifestation.nb_participants = 1
        self.manifestation.description = 'une course qui fait courir'
        self.manifestation.nb_spectateurs = 100
        self.manifestation.nb_signaleurs = 1
        self.manifestation.nb_vehicules_accompagnement = 0
        self.manifestation.nom_contact = 'durand'
        self.manifestation.prenom_contact = 'joseph'
        self.manifestation.tel_contact = '0605555555'
        self.manifestation.save()

        for file in ('reglement_manifestation', 'disposition_securite', 'itineraire_horaire',
                     'pj_1', 'pj_2', 'pj_3', 'pj_4'):
            mon_fichier = open("/tmp/"+file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test Sélénium")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """
        cls.selenium.quit()
        for file in ('reglement_manifestation', 'disposition_securite', 'itineraire_horaire',
                     'pj_1', 'pj_2', 'pj_3', 'pj_4'):
            os.remove("/tmp/"+file+".txt")

        os.system('rm -R /tmp/maniftest/media/' + cls.dep.name + '/')

        super().tearDownClass()

    def connexion(self, username):
        """ Connexion de l'utilisateur 'username' """
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element(By.NAME, "login")
        pseudo_input.send_keys(username)
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element(By.NAME, "password")
        password_input.send_keys('123')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.assertIn('Connexion avec ' + username + ' réussie', self.selenium.page_source)

    def deconnexion(self):
        """ Deconnexion de l'utilisateur """
        time.sleep(self.DELAY*4)
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.CLASS_NAME, 'deconnecter').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element(By.ID, id_chosen)
        chosen_select.click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements(By.TAG_NAME, 'li')
        for result in results:
            if value in result.text:
                result.click()

    def vue_detail(self):
        """
        Appel de la vue de détail de la manifestation
        """
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1200)")
        time.sleep(self.DELAY)
        url = self.selenium.current_url
        self.selenium.find_element(By.XPATH, f"//span[contains(text(), '%s')]" % self.manifestation.nom).click()
        # self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % 'Gendarmerie').click()

        wait = WebDriverWait(self.selenium, 15)
        wait.until(lambda driver: self.selenium.current_url != url)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        time.sleep(self.DELAY*2)

    def envoyer_avis(self, objet="avis"):
        """
        Rédiger et valider l'avis ou le préavis
        """
        # Vérifier l'action disponible
        if objet == "avis":
            self.assertIn('Rédiger l\'avis', self.selenium.page_source)
            # Valider l'événement
            time.sleep(self.DELAY)
            self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Rédiger l\'avis').click()
        if objet == "préavis":
            self.assertIn('Rédiger le préavis', self.selenium.page_source)
            # Valider l'événement
            time.sleep(self.DELAY)
            self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Rédiger le préavis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Enregistrer').click()
        time.sleep(self.DELAY * 10)
        if objet == "avis":
            self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Valider et rendre l\'avis').click()
        if objet == "préavis":
            self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Valider et rendre le préavis').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.CLASS_NAME, 'test-btn-valider').click()
        time.sleep(self.DELAY)

    def adresser_avis(self, service):
        """
        Rédiger et adresser l'avis
        """
        # Vérifier l'action disponible
        self.assertIn('Rédiger l\'avis', self.selenium.page_source)
        # Valider l'événement
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Rédiger l\'avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Enregistrer').click()
        time.sleep(self.DELAY * 10)
        # Vérifier l'action disponible
        self.assertIn('Adresser l\'avis', self.selenium.page_source)
        # Formater l'avis de l'événement
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Adresser l\'avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, f"//select[@name='services_concerne']/option[text()='{service}']").click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        time.sleep(self.DELAY)

    def distribuer_avis(self, service_list):
        """
        Distribuer les avis
        """
        # Vérifier l'action disponible
        self.assertIn("Envoyer une demande d'avis", self.selenium.page_source)
        # Distribuer les demandes d'avis de l'événement
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Envoyer une demande d'avis").click()
        time.sleep(self.DELAY * 3)
        for service in service_list:
            self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % service).click()
            time.sleep(self.DELAY * 2)
            self.selenium.find_element(By.CLASS_NAME, 'name_service').click()
            self.selenium.find_element(By.CLASS_NAME, 'btn-action-selecteur').click()
            time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, 'btn-close-modal-avis').click()
        time.sleep(self.DELAY * 2 * len(service_list))

    def distribuer_preavis(self, service):
        """
        Distribuer les préavis
        """
        # Vérifier l'action disponible
        self.assertIn('Envoyer une demande de préavis aux services internes', self.selenium.page_source)
        # self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'préavis aux services internes').click()
        time.sleep(self.DELAY)
        self.assertIn('Services concernés', self.selenium.page_source)
        self.chosen_select('id_services_concernes_chosen', service)
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        time.sleep(self.DELAY)

    def aucune_action(self):
        """
        Test auncune action affichée dans la zone action de la dashboard
        """
        action = re.search(', aucune action nécessaire', self.selenium.page_source)
        dispo = re.search('Aucune action', self.selenium.page_source)
        attente = re.search(' devez attendre ', self.selenium.page_source)
        if not action and not attente and not dispo:
            self.assertIsNotNone(action, msg="message non trouvé")

    def presence_avis(self, username, state):
        """
        Test de la présence et l'état de l'événement
        :param username: agent considéré
        :param state: couleur de l'événement
        """
        if state == 'none':
            # if username not in ["agent_codis"]:
            #     blocs = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_demande')]")
            #     nb_class = "test_nb_atraiter"
            # else:
            #     blocs = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_rendu')]")
            #     nb_class = "test_nb_rendu"
            bloc = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_demande')]")
            nb_class = "test_nb_atraiter"
            bloc.click()
            self.assertEqual('0', bloc.find_element(By.CLASS_NAME, nb_class).text)
            if username in ['agent_commiss', 'agent_cgd', 'agentlocal_edsr', 'agent_cgserv', 'agent_group', 'agent_brg',
                            'agent_cis', 'agent_codis']:
                self.assertIn('Aucune demande', self.selenium.page_source)
            else:
                self.assertIn('Aucun avis', self.selenium.page_source)
            # self.assertIn('Aucun avis', self.selenium.page_source)

        # Test du blocs concerné
        elif state == 'nouveau':
            bloc = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_demande')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element(By.CLASS_NAME, 'test_nb_atraiter').text)
            declar = self.selenium.find_element(By.TAG_NAME, 'tbody')
            self.assertIn('Manifestation_Test', declar.text)
            try:
                declar.find_element(By.CLASS_NAME, 'table_afaire')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de nouveau dossier")
        elif state == 'demarre':
            self.selenium.execute_script("window.scroll(0, 1200)")
            time.sleep(self.DELAY*4)
            bloc = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_demarre')]")
            bloc.click()
            try:
                self.assertEqual('1', bloc.find_element(By.CLASS_NAME, 'test_nb_encours').text)
            except:
                self.assertEqual('1', bloc.find_element(By.CLASS_NAME, 'test_nb_demarre').text)
            declar = self.selenium.find_element(By.TAG_NAME, 'tbody')
            self.assertIn('Manifestation_Test', declar.text)
            time.sleep(self.DELAY * 4)
            try:
                declar.find_element(By.CLASS_NAME, 'table_encours')
            except NoSuchElementException:
                declar.find_element(By.CLASS_NAME, 'table_demarre')
        elif state == 'encours':
            self.selenium.execute_script("window.scroll(0, 1200)")
            time.sleep(self.DELAY*4)
            bloc = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_encours')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element(By.CLASS_NAME, 'test_nb_encours').text)
            declar = self.selenium.find_element(By.TAG_NAME, 'tbody')
            self.assertIn('Manifestation_Test', declar.text)
            time.sleep(self.DELAY * 4)
            try:
                declar.find_element(By.CLASS_NAME, 'table_encours')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de dossier en cours")
        elif state == 'rendu':
            self.selenium.execute_script("window.scroll(0, 600)")
            time.sleep(self.DELAY)
            bloc = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_rendu')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element(By.CLASS_NAME, 'test_nb_rendu').text)
            declar = self.selenium.find_element(By.TAG_NAME, 'tbody')
            self.assertIn('Manifestation_Test', declar.text)
            try:
                declar.find_element(By.CLASS_NAME, 'table_termine')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de dossier terminé")
        elif state == 'autorise':
            self.selenium.execute_script("window.scroll(0, 600)")
            time.sleep(self.DELAY)
            bloc = self.selenium.find_element(By.XPATH, 
                "//*[starts-with(@id, 'btn')][contains(@id, '_etat_autorise')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element(By.CLASS_NAME, 'test_nb_auto').text)
            declar = self.selenium.find_element(By.TAG_NAME, 'tbody')
            self.assertIn('Manifestation_Test', declar.text)
            try:
                declar.find_element(By.CLASS_NAME, 'table_termine')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de manifestation autorisée")
        elif state == 'interdite':
            self.selenium.execute_script("window.scroll(0, 600)")
            time.sleep(self.DELAY)
            bloc = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_interdite')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element(By.CLASS_NAME, 'test_nb_inter').text)
            declar = self.selenium.find_element(By.TAG_NAME, 'tbody')
            self.assertIn('Manifestation_Test', declar.text)
            try:
                declar.find_element(By.CLASS_NAME, 'table_termine')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de manifestation interdite")
        elif state == 'annule':
            self.selenium.execute_script("window.scroll(0, 600)")
            time.sleep(self.DELAY)
            bloc = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_annule')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element(By.CLASS_NAME, 'test_nb_annul').text)
            declar = self.selenium.find_element(By.TAG_NAME, 'tbody')
            self.assertIn('Manifestation_Test', declar.text)
            try:
                declar.find_element(By.CLASS_NAME, 'table_termine')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de manifestation annulée")
        elif state == 'acces':
            self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Lecture seule').click()
            time.sleep(self.DELAY * 4)
            declar = self.selenium.find_element(By.TAG_NAME, 'tbody')
            self.assertIn('Manifestation_Test', declar.text)
            try:
                declar.find_element(By.CLASS_NAME, 'table_encours')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de dossier en accès en lecture")
        else:
            self.assertEqual('0', '1', 'paramètre inconnu')
