import re

from django.test import TestCase
from django.utils import timezone
from django.contrib.auth.hashers import make_password as make
from django.contrib.contenttypes.models import ContentType

from core.models import Instance
from evenements.factories import DnmFactory
from evenements.models import Manif
from instructions.factories import InstructionFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from structure.factories.structure import StructureOrganisatriceFactory
from core.factories import UserFactory
from sports.factories import ActiviteFactory


class ConflitPossible(TestCase):
    """
    Test de la détection de conflit entre deux manifestations
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========= DNM conflit de parcours (Clt) ==========')
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__instruction_mode=Instance.IM_CIRCUIT_B)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        cls.prefecture = arrondissement.organisations.first()
        cls.commune1 = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.mairie1 = cls.commune1.organisation_set.first()
        cls.commune2 = CommuneFactory(name='Boen', arrondissement=arrondissement)
        cls.mairie2 = cls.commune2.organisation_set.first()
        structure = StructureOrganisatriceFactory.create(precision_nom_s="structure_test",
                                                              commune_m2m=cls.commune1, instance_fk=dep.instance)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        cls.instructeur = UserFactory.create(username='instructeur', password=make("123"), default_instance=dep.instance)
        cls.instructeur.organisation_m2m.add(cls.prefecture)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make("123"), default_instance=dep.instance)
        cls.agent_mairie.organisation_m2m.add(cls.mairie1)

        # Création de l'événement 1
        cls.manifestation1 = DnmFactory.create(ville_depart=cls.commune1, structure_organisatrice_fk=structure,
                                               date_debut=timezone.now() + timezone.timedelta(days=100),
                                               nom='Manifestation_Test_1', activite=activ, instance=dep.instance)

        # Création de l'événement 2
        cls.manifestation2 = DnmFactory.create(ville_depart=cls.commune2, structure_organisatrice_fk=structure,
                                               villes_traversees=[cls.commune1, ],
                                               date_debut=timezone.now() + timezone.timedelta(days=100),
                                               nom='Manifestation_Test_2', activite=activ, instance=dep.instance)

    def test_Conflit(self):
        print('-- test conflit de parcours --')
        print()

        print('**** test 1 creation instruction ****')
        manif = Manif.objects.get(nom='Manifestation_Test_1')
        self.instruction1 = InstructionFactory.create(manif=manif, instance=manif.instance)

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation1.ville_depart, end=" ; ")
        print(self.manifestation1.ville_depart.get_departement(), end=" ; ")
        print(list(self.manifestation1.villes_traversees.all()), end=" ; ")
        print(self.manifestation1.date_debut)
        self.assertEqual(str(self.instruction1.manif), str(self.manifestation1))

        manif = Manif.objects.get(nom='Manifestation_Test_2')
        self.instruction2 = InstructionFactory.create(manif=manif, instance=manif.instance)

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation2.ville_depart, end=" ; ")
        print(self.manifestation2.ville_depart.get_departement(), end=" ; ")
        print(list(self.manifestation2.villes_traversees.all()), end=" ; ")
        print(self.manifestation2.date_debut)
        self.assertEqual(str(self.instruction2.manif), str(self.manifestation2))

        print('**** test 2 présence instructions ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur', password='123'))
        # Appel de la page
        url = f'/instructions/tableaudebord/{str(self.prefecture.pk)}/'
        reponse = self.client.get(url + '?filtre_specifique=instructionmairie', HTTP_HOST='127.0.0.1:8000')
        # Une instruction dans le TdB mairie
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        url_list = url + 'list/'
        reponse = self.client.get(url_list + '?filtre_specifique=instructionmairie&filtre_etat=atraiter',
                                  HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test_1')

        # Appel de la page
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test_2')

        print('**** test 3 conflit instruction ****')
        # Appel de la page détail
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test_2')

        # Chargement de l'onglet carto
        carto_url = re.search(r'get\("(?P<url>(/[^"]+))', page.content.decode('utf-8'))
        self.assertTrue(hasattr(carto_url, 'group'))
        carto = self.client.get(carto_url.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(carto, 'Possibles conflits de parcours')
        self.assertContains(carto, "<td>Manifestation_Test_1</td>")

        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_specifique=instructionmairie&filtre_etat=atraiter',
                                  HTTP_HOST='127.0.0.1:8000')
        # Appel de la page détail
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test_1')

        # Chargement de l'onglet carto
        carto_url = re.search(r'get\("(?P<url>(/[^"]+))', page.content.decode('utf-8'))
        self.assertTrue(hasattr(carto_url, 'group'))
        carto = self.client.get(carto_url.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(carto, 'Possibles conflits de parcours')
        self.assertContains(carto, "<td>Manifestation_Test_2</td>")
