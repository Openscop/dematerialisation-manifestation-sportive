import re

from django.test import TestCase, tag
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.hashers import make_password as make

from post_office.models import EmailTemplate, Email

from core.models import Instance, User, ConfigurationGlobale
from evenements.factories import DcnmFactory, AvtmFactory, DvtmFactory
from instructions.factories import InstructionFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from structure.factories.structure import StructureOrganisatriceFactory
from core.factories import UserFactory
from sports.factories import ActiviteFactory
from messagerie.models import Message


class InstructionCircuitC(TestCase):
    """
    Test du circuit d'instruction C
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========= Circuit C (Clt) ==========')
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        ConfigurationGlobale.objects.create(pk=1, mail_action=True, mail_suivi=True, mail_tracabilite=True, mail_actualite=True)
        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__instruction_mode=Instance.IM_CIRCUIT_C)
        arrond_1 = ArrondissementFactory.create(name='Montbrison', code='421', departement=dep)
        arrond_2 = ArrondissementFactory.create(name='Roanne', code='422', departement=dep,
                                                prefecture__type_service_fk__nom_s="Sous-Préfecture",
                                                prefecture__type_service_instructeur_s='sousprefecture')
        arrond_3 = ArrondissementFactory.create(name='ST Etienne', code='420', departement=dep,
                                                prefecture__type_service_fk__nom_s="Sous-Préfecture",
                                                prefecture__type_service_instructeur_s='sousprefecture')
        cls.instance = dep.instance
        cls.prefecture1 = arrond_1.organisations.first()
        cls.prefecture2 = arrond_2.organisations.first()
        cls.prefecture3 = arrond_3.organisations.first()
        cls.commune1 = CommuneFactory(name='Bard', arrondissement=arrond_1)
        cls.autrecommune1 = CommuneFactory(name='Roche', arrondissement=arrond_1)
        cls.commune2 = CommuneFactory(name='Bully', arrondissement=arrond_2)
        cls.autrecommune2 = CommuneFactory(name='Régny', arrondissement=arrond_2)
        cls.commune3 = CommuneFactory(name='Villars', arrondissement=arrond_3)
        structure1 = StructureOrganisatriceFactory.create(precision_nom_s="structure_test_1",
                                                          commune_m2m=cls.commune1, instance_fk=dep.instance)
        structure2 = StructureOrganisatriceFactory.create(precision_nom_s="structure_test_2",
                                                          commune_m2m=cls.commune2, instance_fk=dep.instance)
        structure3 = StructureOrganisatriceFactory.create(precision_nom_s="structure_test_3",
                                                          commune_m2m=cls.commune3, instance_fk=dep.instance)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        cls.instructeur1 = UserFactory.create(username='instructeur1', password=make('123'),
                                              default_instance=dep.instance, email='instructeur1@test.fr')
        cls.instructeur1.organisation_m2m.add(cls.prefecture1)
        cls.agent_mairie1 = UserFactory.create(username='agent_mairie1', password=make('123'),
                                               default_instance=dep.instance, email='agentmairie1@test.fr')
        cls.mairie1 = cls.commune1.organisation_set.first()
        cls.agent_mairie1.organisation_m2m.add(cls.mairie1)
        cls.instructeur2 = UserFactory.create(username='instructeur2', password=make('123'),
                                              default_instance=dep.instance, email='instructeur1@test.fr')
        cls.instructeur2.organisation_m2m.add(cls.prefecture2)
        cls.mairie2 = cls.commune2.organisation_set.first()
        cls.agent_mairie2 = UserFactory.create(username='agent_mairie2', password=make('123'),
                                               default_instance=dep.instance, email='agentmairie2@test.fr')
        cls.agent_mairie2.organisation_m2m.add(cls.mairie2)
        cls.organisateur = UserFactory.create(username='organisateur', password=make("123"),
                                              default_instance=dep.instance, email='orga@test.fr')
        cls.organisateur.organisation_m2m.add(structure1)
        cls.organisateur.organisation_m2m.add(structure2)
        cls.organisateur.organisation_m2m.add(structure3)
        for user in User.objects.all():
            user.optionuser.notification_mail = True
            user.optionuser.action_mail = True
            user.optionuser.save()

        EmailTemplate.objects.create(name='new_msg')

        # Création des événements
        # c : commune, a : arrondissement
        # Zone 1 : pref, zone 2 : sspref2, zone 3 : sspref3
        # Circuit   |    B    |    C    |    D    |    E    | n°
        # DCNM1-1c  | mairie1 | mairie1 | mairie1 | mairie1 | 1
        # DCNM2-1c  | mairie2 | mairie2 | mairie2 | mairie2 | 2
        # DCNM1-2c  | pref1   | pref1   | pref1   | pref1   | 3
        # DCNM2-2c  | sspref2 | sspref2 | sspref2 | sspref2 | 4
        # DCNM1-2a  | pref1   | pref1   | pref1   | pref1   | 5
        # DCNM2-2a  | sspref2 | pref1   | pref1   | pref1   | 6
        # AVTM1-1c  | pref1   | pref1   | pref1   | pref1   | 7
        # AVTM2-1c  | sspref2 | sspref2 | pref1   | pref1   | 8
        # DVTM1-1c  | pref1   | pref1   | pref1   | pref1   | 9
        # DVTM2-1c  | sspref2 | sspref2 | pref1   | sspref2 | 10
        # Manif NM sur 1 commmune, arrondissement de préfecture
        cls.manifestation1 = DcnmFactory.create(ville_depart=cls.commune1, structure_organisatrice_fk=structure1, activite=activ,
                                                nom='Manifestation_Test_1', declarant=cls.organisateur)
        # Manif NM sur 1 commmune, arrondissement de sous-préfecture
        cls.manifestation2 = DcnmFactory.create(ville_depart=cls.commune2, structure_organisatrice_fk=structure2, activite=activ,
                                                nom='Manifestation_Test_2', declarant=cls.organisateur)
        # Manif NM sur 2 commmunes, même arrondissement de préfecture
        cls.manifestation3 = DcnmFactory.create(ville_depart=cls.commune1, structure_organisatrice_fk=structure1, activite=activ,
                                                nom='Manifestation_Test_3', villes_traversees=(cls.autrecommune1,), declarant=cls.organisateur)
        # Manif NM sur 2 commmunes, même arrondissement de sous-préfecture
        cls.manifestation4 = DcnmFactory.create(ville_depart=cls.commune2, structure_organisatrice_fk=structure2, activite=activ,
                                                nom='Manifestation_Test_4', villes_traversees=(cls.autrecommune2,), declarant=cls.organisateur)
        # Manif NM sur 2 commmunes, arrondissements différents, départ en préfecture
        cls.manifestation5 = DcnmFactory.create(ville_depart=cls.commune1, structure_organisatrice_fk=structure1, activite=activ,
                                                nom='Manifestation_Test_5', villes_traversees=(cls.autrecommune2,), declarant=cls.organisateur)
        # Manif NM sur 2 commmunes, arrondissements différents, départ en sous-préfecture
        cls.manifestation6 = DcnmFactory.create(ville_depart=cls.commune2, structure_organisatrice_fk=structure2, activite=activ,
                                                nom='Manifestation_Test_6', villes_traversees=(cls.autrecommune1,), declarant=cls.organisateur)
        # Manif VTM sur 1 commmune, arrondissement de préfecture
        cls.manifestation7 = AvtmFactory.create(ville_depart=cls.commune1, structure_organisatrice_fk=structure1, activite=activ,
                                                nom='Manifestation_Test_7', declarant=cls.organisateur)
        # Manif VTM sur 1 commmune, arrondissement de sous-préfecture
        cls.manifestation8 = AvtmFactory.create(ville_depart=cls.commune2, structure_organisatrice_fk=structure2, activite=activ,
                                                nom='Manifestation_Test_8', declarant=cls.organisateur)
        # Manif DVTM sur 1 commmune, arrondissement de préfecture
        cls.manifestation9 = DvtmFactory.create(ville_depart=cls.commune1, structure_organisatrice_fk=structure1, activite=activ,
                                                nom='Manifestation_Test_9', declarant=cls.organisateur)
        # Manif DVTM sur 1 commmune, arrondissement de sous-préfecture
        cls.manifestation10 = DvtmFactory.create(ville_depart=cls.commune2, structure_organisatrice_fk=structure2, activite=activ,
                                                 nom='Manifestation_Test_A', declarant=cls.organisateur)
        # Manif NM sur 1 commmune, arrondissement de sous-préfecture
        cls.manifestation11 = DcnmFactory.create(ville_depart=cls.commune3, structure_organisatrice_fk=structure3, activite=activ,
                                                 nom='Manifestation_Test_B', declarant=cls.organisateur)

    @tag('circuit')
    def test_Instruction_CircuitC(self):
        print('**** test 1 vérification des notifications ****')

        print('\t >>> Manifestation 1')
        self.instruction1 = InstructionFactory.create(manif=self.manifestation1)
        self.instruction1.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.mairie1.email_s])
        self.assertEqual(outbox[0].to, [self.agent_mairie1.email])
        self.assertEqual(outbox[0].subject, 'Nouveau dossier à instruire')
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "dossier")
        self.assertEqual(messages[0].message_enveloppe.first().type, "action")
        Email.objects.all().delete()

        print('\t >>> Manifestation 2')
        self.instruction2 = InstructionFactory.create(manif=self.manifestation2)
        self.instruction2.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.mairie2.email_s])
        self.assertEqual(outbox[0].to, [self.agent_mairie2.email])
        self.assertEqual(outbox[0].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        print('\t >>> Manifestation 3')
        self.instruction3 = InstructionFactory.create(manif=self.manifestation3)
        self.instruction3.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture1.email_s])
        self.assertEqual(outbox[0].to, [self.instructeur1.email])
        self.assertEqual(outbox[0].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        print('\t >>> Manifestation 4')
        self.instruction4 = InstructionFactory.create(manif=self.manifestation4)
        self.instruction4.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture2.email_s])
        self.assertEqual(outbox[0].to, [self.instructeur2.email])
        self.assertEqual(outbox[0].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        print('\t >>> Manifestation 5')
        self.instruction5 = InstructionFactory.create(manif=self.manifestation5)
        self.instruction5.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture1.email_s])
        self.assertEqual(outbox[0].to, [self.instructeur1.email])
        self.assertEqual(outbox[0].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        print('\t >>> Manifestation 6')
        self.instruction6 = InstructionFactory.create(manif=self.manifestation6)
        self.instruction6.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture1.email_s])
        self.assertEqual(outbox[0].to, [self.instructeur1.email])
        self.assertEqual(outbox[0].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        print('\t >>> Manifestation 7')
        self.instruction7 = InstructionFactory.create(manif=self.manifestation7)
        self.instruction7.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture1.email_s])
        self.assertEqual(outbox[0].to, [self.instructeur1.email])
        self.assertEqual(outbox[0].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        print('\t >>> Manifestation 8')
        self.instruction8 = InstructionFactory.create(manif=self.manifestation8)
        self.instruction8.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture2.email_s])
        self.assertEqual(outbox[0].to, [self.instructeur2.email])
        self.assertEqual(outbox[0].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        print('\t >>> Manifestation 9')
        self.instruction9 = InstructionFactory.create(manif=self.manifestation9)
        self.instruction9.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture1.email_s])
        self.assertEqual(outbox[0].to, [self.instructeur1.email])
        self.assertEqual(outbox[0].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        print('\t >>> Manifestation 10')
        self.instruction10 = InstructionFactory.create(manif=self.manifestation10)
        self.instruction10.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture2.email_s])
        self.assertEqual(outbox[0].to, [self.instructeur2.email])
        self.assertEqual(outbox[0].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        self.instruction11 = InstructionFactory.create(manif=self.manifestation11)

        print('**** test 2 instruction mairie 1 ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='agent_mairie1', password='123'))
        # Appel de la page
        url = f'/instructions/tableaudebord/{str(self.mairie1.pk)}/?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Arrondissements')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        url = f'/instructions/tableaudebord/{str(self.mairie1.pk)}/list/?filtre_specifique=instructionmairie&filtre_etat=atraiter'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction3.manif)
        self.assertNotContains(reponse, self.instruction4.manif)
        self.assertNotContains(reponse, self.instruction5.manif)
        self.assertNotContains(reponse, self.instruction6.manif)
        self.assertNotContains(reponse, self.instruction7.manif)
        self.assertNotContains(reponse, self.instruction8.manif)
        self.assertNotContains(reponse, self.instruction9.manif)
        self.assertNotContains(reponse, self.instruction10.manif)
        self.assertNotContains(reponse, self.instruction11.manif)

        print('**** test 3 instruction mairie 2 ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='agent_mairie2', password='123'))
        # Appel de la page
        url = f'/instructions/tableaudebord/{str(self.mairie2.pk)}/?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Arrondissements')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        url = f'/instructions/tableaudebord/{str(self.mairie2.pk)}/list/?filtre_specifique=instructionmairie&filtre_etat=atraiter'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, self.instruction1.manif)
        self.assertContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction3.manif)
        self.assertNotContains(reponse, self.instruction4.manif)
        self.assertNotContains(reponse, self.instruction5.manif)
        self.assertNotContains(reponse, self.instruction6.manif)
        self.assertNotContains(reponse, self.instruction7.manif)
        self.assertNotContains(reponse, self.instruction8.manif)
        self.assertNotContains(reponse, self.instruction9.manif)
        self.assertNotContains(reponse, self.instruction10.manif)
        self.assertNotContains(reponse, self.instruction11.manif)

        print('**** test 4 instruction sous-préfecture ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur2', password='123'))
        print('\t >>> Instruction en préfecture')
        # Appel de la page
        url = f'/instructions/tableaudebord/{str(self.prefecture2.pk)}/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Arrondissements')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('3', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        url_list = f'/instructions/tableaudebord/{str(self.prefecture2.pk)}/list/?filtre_etat=atraiter'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction3.manif)
        self.assertContains(reponse, self.instruction4.manif)
        self.assertNotContains(reponse, self.instruction5.manif)
        self.assertNotContains(reponse, self.instruction6.manif)
        self.assertNotContains(reponse, self.instruction7.manif)
        self.assertContains(reponse, self.instruction8.manif)
        self.assertNotContains(reponse, self.instruction9.manif)
        self.assertContains(reponse, self.instruction10.manif)
        self.assertNotContains(reponse, self.instruction11.manif)

        print('\t >>> Instruction en mairie')
        # Appel de la page
        url = url + '?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Arrondissements')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        url_list += '&filtre_specifique=instructionmairie'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, self.instruction1.manif)
        self.assertContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction3.manif)
        self.assertNotContains(reponse, self.instruction4.manif)
        self.assertNotContains(reponse, self.instruction5.manif)
        self.assertNotContains(reponse, self.instruction6.manif)
        self.assertNotContains(reponse, self.instruction7.manif)
        self.assertNotContains(reponse, self.instruction8.manif)
        self.assertNotContains(reponse, self.instruction9.manif)
        self.assertNotContains(reponse, self.instruction10.manif)
        self.assertNotContains(reponse, self.instruction11.manif)

        print('**** test 5 instruction préfecture ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur1', password='123'))
        print('\t >>> Instruction en préfecture')
        # Appel de la page
        url = f'/instructions/tableaudebord/{str(self.prefecture1.pk)}/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Arrondissements')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('5', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        url_list = f'/instructions/tableaudebord/{str(self.prefecture1.pk)}/list/?filtre_etat=atraiter'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)
        self.assertContains(reponse, self.instruction3.manif)
        self.assertNotContains(reponse, self.instruction4.manif)
        self.assertContains(reponse, self.instruction5.manif)
        self.assertContains(reponse, self.instruction6.manif)
        self.assertContains(reponse, self.instruction7.manif)
        self.assertNotContains(reponse, self.instruction8.manif)
        self.assertContains(reponse, self.instruction9.manif)
        self.assertNotContains(reponse, self.instruction10.manif)
        self.assertNotContains(reponse, self.instruction11.manif)

        print('\t >>> Instruction en mairie')
        # Appel de la page
        url = url + '?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Arrondissements')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        url_list += '&filtre_specifique=instructionmairie'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction3.manif)
        self.assertNotContains(reponse, self.instruction4.manif)
        self.assertNotContains(reponse, self.instruction5.manif)
        self.assertNotContains(reponse, self.instruction6.manif)
        self.assertNotContains(reponse, self.instruction7.manif)
        self.assertNotContains(reponse, self.instruction8.manif)
        self.assertNotContains(reponse, self.instruction9.manif)
        self.assertNotContains(reponse, self.instruction10.manif)
        self.assertNotContains(reponse, self.instruction11.manif)

        # print(reponse.content.decode('utf-8'))
