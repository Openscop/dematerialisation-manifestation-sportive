import re, time

from django.test import tag, override_settings
from django.conf import settings

from post_office.models import Email
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

from .test_base_selenium import SeleniumCommunClass
from instructions.models import Avis
from messagerie.models import Message


class InstructionAjoutPjTests(SeleniumCommunClass):
    """
    Test d'ajout de PJ du circuit d'instance EDSR avec sélénium pour une Dcnm
        circuit_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    Test sur les services complexes avec transfert des PJ du préavis dans l'avis et
    transfert des PJ d'avis en document officiel
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ Pièces Jointes Service GGD (Sel) =============')
        SeleniumCommunClass.init_setup(cls)
        cls.agentlocal_edsr.delete()
        cls.avis_nb = 1
        super().setUpClass()

    @tag('selenium')
    @override_settings(DEBUG=True)
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
    def test_Ajout_PJ(self):
        """
        Test d'ajout de PJ pour les différentes étapes du circuit EDSR pour une autorisationNM
        """
        assert settings.DEBUG

        print('**** test 1 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 2 distribution avis ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.distribuer_avis(['Gendarmerie'])
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY*3)

        # Annuler la demande d'avis fédération
        avis = self.selenium.find_element(By.ID, f"card_{self.fede.pk}")
        time.sleep(self.DELAY * 2)
        avis.click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 400)")
        carte = self.selenium.find_element(By.ID, f"card_{self.fede.pk}")
        time.sleep(self.DELAY * 2)
        self.assertIn('Annuler cette demande', carte.text)
        carte.find_element(By.PARTIAL_LINK_TEXT, 'Annuler cette demande').click()
        carte.find_element(By.PARTIAL_LINK_TEXT, 'Confirmer').click()

        # Vérifier le passage en encours et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1200)")
        time.sleep(self.DELAY*3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 3 avis edsr ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en nouveau
        self.connexion('agent_edsr')
        self.presence_avis('agent_edsr', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.distribuer_preavis('CGD1')
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Test de la partie pièces jointes
        self.assertIn('Ajouter une pièce jointe', self.selenium.page_source)
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_edsr', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 4 preavis cgd ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en nouveau
        self.connexion(self.agent_cgd1.username)
        self.presence_avis('agent_cgd', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis("préavis")

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Aucune', self.selenium.page_source)  # Pièces jointes
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        # Ajouter une pièce jointe
        self.selenium.find_element(By.CLASS_NAME, 'btn_doc').click()
        fichier = self.selenium.find_element(By.XPATH, '//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_1.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        # Tester la présence de la pièce jointe
        pan = self.selenium.find_element(By.XPATH, "//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        # Vérification du passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cgd', 'rendu')
        self.deconnexion()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(messages), 2)
        self.assertIn('pj_1.txt ajoutée à un préavis au sujet de la manifestation Manifestation_Test', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_edsr)
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_consulte_fk=self.cgd1).id)          # Préavis CGD
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].to, [self.agent_edsr.email])
        Email.objects.all().delete()
        Message.objects.all().delete()

        print('**** test 5 avis edsr ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en encours
        self.connexion('agent_edsr')
        self.presence_avis('agent_edsr', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rédiger l\'avis', self.selenium.page_source)
        # Vérifier affichage
        pan = self.selenium.find_element(By.XPATH, "//div[@id='agreement']")
        self.assertIn('Ajouter une pièce jointe', pan.text)
        # Vérifier les icônes du préavis
        preavis = self.selenium.find_element(By.ID, f"card_{self.cgd1.pk}")
        preavis_heading = preavis.find_element(By.XPATH, "div[starts-with(@id, 'heading')]")
        icones = preavis_heading.find_elements(By.CSS_SELECTOR, 'i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            preavis_heading.find_element(By.CLASS_NAME, 'pj')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 800)")
        # Déplier le préavis
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        time.sleep(self.DELAY*2)
        # Tester la présence de la PJ dans le préavis et du bouton de transfert
        carte = self.selenium.find_element(By.ID, f"card_{self.cgd1.pk}")
        self.assertIn('pj_1', carte.text)
        lien1 = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_1')]/following-sibling::a")
        self.assertIn('Transférer le document sur', lien1.text)
        # Transférer la PJ sur l'avis
        lien1.click()
        # Tester aucune notifications
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 0)
        self.assertEqual(len(messages), 1)  # Connexion
        # Tester la présence de la PJ sur l'avis
        pan = self.selenium.find_element(By.XPATH, "//div[@id='agreement']")
        self.assertNotIn('Aucune', pan.text)
        self.assertIn('pj_1', pan.text)
        self.selenium.execute_script("window.scroll(0, 800)")
        # Déplier le préavis
        preavis = self.selenium.find_element(By.ID, f"card_{self.cgd1.pk}")
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        time.sleep(self.DELAY)
        # Tester l'absence du bouton de transfert pour PJ_1
        carte = self.selenium.find_element(By.ID, f"card_{self.cgd1.pk}")
        item = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_1')]/parent::*")
        self.assertNotIn('Transférer le document sur', item.text)
        # Ajouter une pièce jointe
        self.selenium.find_element(By.CLASS_NAME, 'btn_doc').click()
        fichier = self.selenium.find_element(By.XPATH, '//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_3.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        # Tester la présence ds PJs
        pan = self.selenium.find_element(By.XPATH, "//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_3', pan.text)

        # Formater l'avis de l'événement
        time.sleep(self.DELAY*2)
        self.adresser_avis('GGD GGD (42)')
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        # self.aucune_action()
        # vérification de la présence de l'événement en rendu
        self.selenium.execute_script("window.scroll(0, 600)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_edsr', 'rendu')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.deconnexion()

        print('**** test 6 preavis cgd ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en nouveau
        self.connexion(self.agent_cgd1.username)
        self.presence_avis('agent_cgd', 'rendu')

        Email.objects.all().delete()
        Message.objects.all().delete()

        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Ajouter une pièce jointe
        time.sleep(self.DELAY * 3)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.CLASS_NAME, 'btn_doc').click()
        fichier = self.selenium.find_element(By.XPATH, '//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_2.txt')
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 600)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        # Tester la présence des pièces jointes
        pan = self.selenium.find_element(By.XPATH, "//div[@id='agreement']")
        time.sleep(self.DELAY * 5)
        self.assertIn('pj_2', pan.text)
        self.deconnexion()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(messages), 2)
        self.assertIn('pj_2.txt ajoutée à un préavis au sujet de la manifestation Manifestation_Test', messages[0].corps)
        self.assertEqual(2, messages[0].message_enveloppe.all().count())
        destinaires = [self.agent_ggd, self.agent_edsr]
        for env in messages[0].message_enveloppe.all():
            self.assertIn(env.destinataire_id, destinaires)
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_consulte_fk=self.cgd1).id)          # Préavis CGD
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_ggd.email])
        self.assertEqual(outbox[1].to, [self.agent_edsr.email])
        Email.objects.all().delete()
        Message.objects.all().delete()

        print('**** test 7 avis ggd ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ggd')
        self.presence_avis('agent_ggd', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rédiger l\'avis', self.selenium.page_source)
        # Vérifier affichage des PJs
        pan = self.selenium.find_element(By.XPATH, "//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_3', pan.text)
        self.assertIn('Ajouter une pièce jointe', pan.text)
        # Vérifier les icônes du préavis
        preavis = self.selenium.find_element(By.ID, f"card_{self.cgd1.pk}")
        try:
            preavis.find_element(By.CLASS_NAME, 'pj')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY*2)
        # Déplier le préavis
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        time.sleep(self.DELAY)
        # Tester la présence des PJs dans le préavis et des boutons de transfert
        carte = self.selenium.find_element(By.ID, f"card_{self.cgd1.pk}")
        time.sleep(self.DELAY*2)
        # self.assertIn('pj_1', carte.text)
        item = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_1')]/parent::*")
        self.assertNotIn('Transférer le document sur', item.text)
        self.assertIn('pj_2', carte.text)
        lien2 = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_2')]/following-sibling::a")
        self.assertIn('Transférer le document sur', lien2.text)
        # Transférer la PJ sur l'avis
        lien2.click()
        # Tester aucune notifications
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(messages), 1)
        self.assertEqual(len(outbox), 0)

        # Tester la présence des PJs sur l'avis
        pan = self.selenium.find_element(By.XPATH, "//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_2', pan.text)
        self.assertIn('pj_3', pan.text)
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY * 2)
        # Déplier le préavis
        preavis = self.selenium.find_element(By.ID, f"card_{self.cgd1.pk}")
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        time.sleep(self.DELAY)
        # Tester l'absence du bouton de transfert
        carte = self.selenium.find_element(By.ID, f"card_{self.cgd1.pk}")
        item1 = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_1')]/parent::*")
        self.assertNotIn('Transférer le document sur', item1.text)
        item2 = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_2')]/parent::*")
        self.assertNotIn('Transférer le document sur', item2.text)
        self.selenium.execute_script("window.scroll(0, 200)")
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)

        Email.objects.all().delete()
        Message.objects.all().delete()

        # Ajouter une pièce jointe
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.CLASS_NAME, 'btn_doc').click()
        fichier = self.selenium.find_element(By.XPATH, '//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_4.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        # Tester la présence ds PJs
        pan = self.selenium.find_element(By.XPATH, "//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_2', pan.text)
        self.assertIn('pj_3', pan.text)
        self.assertIn('pj_4', pan.text)
        # Tester les notifications
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(len(messages), 1)
        self.assertIn('pj_4.txt ajoutée à un avis au sujet de la manifestation Manifestation_Test', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.instructeur)
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_consulte_fk=self.ggd).id)   # Avis EDSR
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        # Tester aucune action disponible
        self.avis_nb -= 1
        # vérification de la présence de l'événement en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY * 2)
        self.presence_avis('agent_ggd', 'rendu')
        self.deconnexion()

        print('**** test 8 instructeur transfert pj ****')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.vue_detail()
        # Avis EDSR
        avis = self.selenium.find_element(By.ID, f"card_{self.ggd.pk}")
        try:
            avis.find_element(By.CLASS_NAME, 'pj')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        # Déplier l'avis
        avis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        time.sleep(self.DELAY)
        # Tester la présence des PJs dans le préavis et des boutons de transfert
        carte = self.selenium.find_element(By.ID, f"card_{self.ggd.pk}")
        time.sleep(self.DELAY*2)
        self.assertIn('pj_1', carte.text)
        lien1 = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_1')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien1.text)
        self.assertIn('pj_2', carte.text)
        lien2 = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_2')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien2.text)
        self.assertIn('pj_3', carte.text)
        lien3 = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_3')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien3.text)
        self.assertIn('pj_4', carte.text)
        lien4 = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_4')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien4.text)
        pj = self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'pj_4')
        # Transférer la PJ en document officiel
        lien4.click()
        nom_pj = pj.get_attribute('text')
        self.selenium.execute_script("window.scroll(0, 1500)")
        self.selenium.find_element(By.XPATH, "//select[@name='nature']/option[text()='Récépissé de déclaration']").click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        time.sleep(self.DELAY*2)
        # Reprendre l'avis et tester plus de bouton de transfert
        self.selenium.find_element(By.ID, f"card_{self.ggd.pk}").click()
        time.sleep(self.DELAY*2)
        carte = self.selenium.find_element(By.ID, f"card_{self.ggd.pk}")
        time.sleep(self.DELAY)
        self.assertIn('pj_1', carte.text)
        lien1 = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_1')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien1.text)
        self.assertIn('pj_2', carte.text)
        lien2 = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_2')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien2.text)
        self.assertIn('pj_3', carte.text)
        lien3 = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_3')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien3.text)
        self.assertIn('pj_4', carte.text)
        item = carte.find_element(By.XPATH, "//a[contains(text(), 'pj_4')]/parent::*")
        self.assertNotIn('Transférer le document sur', item.text)
        self.selenium.execute_script("window.scroll(0, 0)")
        self.assertIn('Documents officiels (1)', self.selenium.page_source)
        self.selenium.find_element(By.XPATH, "//button[contains(.,'Documents officiels (1)')]").click()
        time.sleep(self.DELAY*2)
        pan = self.selenium.find_element(By.XPATH, "//div[@id='officialdata']")
        self.assertIn('Récépissé de déclaration', pan.text)
        self.assertIn('Déposé le', pan.text)
        self.assertIn(nom_pj, pan.text)
        # Vérifier les emails envoyés
        # outbox = Email.objects.order_by('pk')
        # messages = Message.objects.all()
        # for out in outbox:
        #     print(out.to, end="")
        #     print("--" + out.subject)
        # for mess in messages:
        #     for env in mess.message_enveloppe.all():
        #         print("**" + env.destinataire_txt)
        #     print("---" + str(mess.pk) + mess.corps)
        # Vérifier le passage en autorise
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'autorise')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.deconnexion()

