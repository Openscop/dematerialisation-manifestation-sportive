import re, os

from django.test import TestCase
from django.contrib.auth.hashers import make_password as make
from django.contrib.contenttypes.models import ContentType

from post_office.models import EmailTemplate

from core.models import Instance, User, ConfigurationGlobale
from core.factories import UserFactory
from evenements.factories import DcnmFactory
from structure.factories.structure import StructureOrganisatriceFactory
from structure.factories.service import ServiceConsulteFactory, FederationFactory
from structure.models.service import ServiceConsulteInteraction
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from sports.factories import ActiviteFactory


class TestCommunClass(TestCase):
    """
    Test du circuit d'instance EDSR pour une Dnm
        circuit_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    """

    def init_setup(self):
        """
        Préparation du test
        """
        # Création des objets sur le 42
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        ConfigurationGlobale.objects.create(pk=1, mail_action=True, mail_suivi=True, mail_tracabilite=True, mail_actualite=True)
        if not hasattr(self, 'dep'):
            dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                            instance__etat_s="ouvert", instance__activer_beta=True)
            self.dep = dep
            arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
            self.prefecture = arrondissement.organisations.first()
            self.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
            self.mairie1 = self.commune.organisation_set.first()
            self.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)
            self.mairie2 = self.autrecommune.organisation_set.first()
        else:
            dep = self.dep

        # Création des utilisateurs
        self.structure = StructureOrganisatriceFactory.create(precision_nom_s="structure_test",
                                                              commune_m2m=self.commune, instance_fk=dep.instance)
        self.organisateur = UserFactory.create(username='organisateur', password=make("123"), default_instance=dep.instance, email='orga@test.fr')
        self.organisateur.organisation_m2m.add(self.structure)
        self.instructeur = UserFactory.create(username='instructeur', password=make("123"), default_instance=dep.instance, email='inst@test.fr')
        self.instructeur.organisation_m2m.add(self.prefecture)
        self.agent_fede = UserFactory.create(username='agent_fede', password=make("123"), default_instance=dep.instance, email='fede@test.fr')
        activ = ActiviteFactory.create()
        self.fede = FederationFactory.create(instance_fk=dep.instance, departement_m2m=dep, discipline_fk=activ.discipline)
        self.agent_fede.organisation_m2m.add(self.fede)
        self.agent_ggd = UserFactory.create(username='agent_ggd', password=make("123"), default_instance=dep.instance, email='ggd@test.fr')
        self.ggd = dep.organisation_set.get(precision_nom_s="GGD")
        self.agent_ggd.organisation_m2m.add(self.ggd)
        self.edsr = dep.organisation_set.get(precision_nom_s="EDSR")
        self.agent_edsr = UserFactory.create(username='agent_edsr', password=make("123"), default_instance=dep.instance, email='edsr@test.fr')
        self.agent_edsr.organisation_m2m.add(self.edsr)
        self.agentlocal_edsr = UserFactory.create(username='agentlocal_edsr', password=make("123"), default_instance=dep.instance, email='edsrlocal@test.fr')
        self.agentlocal_edsr.organisation_m2m.add(self.edsr)
        self.cgd1 = ServiceConsulteFactory(instance_fk=dep.instance, precision_nom_s="CGD1", service_parent_fk=self.edsr,
                                           type_service_fk__categorie_fk__nom_s="Gendarmerie", type_service_fk__nom_s="CGD",
                                           autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True, commune_m2m=self.commune)
        self.agent_cgd1 = UserFactory.create(username='agent_cgd1', password=make("123"), default_instance=dep.instance, email='cgd1@test.fr')
        self.agent_cgd1.organisation_m2m.add(self.cgd1)
        self.cgd2 = ServiceConsulteFactory(instance_fk=dep.instance, precision_nom_s="CGD2", service_parent_fk=self.edsr,
                                           type_service_fk__categorie_fk__nom_s="Gendarmerie", type_service_fk__nom_s="CGD",
                                           autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True, commune_m2m=self.autrecommune)
        self.agent_cgd2 = UserFactory.create(username='agent_cgd2', password=make("123"), default_instance=dep.instance, email='cgd2@test.fr')
        self.agent_cgd2.organisation_m2m.add(self.cgd2)
        self.usg = ServiceConsulteFactory(instance_fk=dep.instance, precision_nom_s="BMO", service_parent_fk=self.edsr,
                                          type_service_fk__categorie_fk__nom_s="Gendarmerie", type_service_fk__nom_s="BMO",
                                          autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True, commune_m2m=self.autrecommune)
        self.agent_usg = UserFactory.create(username='agent_usg', password=make("123"), default_instance=dep.instance, email='usg@test.fr')
        self.agent_usg.organisation_m2m.add(self.usg)
        self.brigade1 = ServiceConsulteFactory(instance_fk=dep.instance, precision_nom_s="BTA1", service_parent_fk=self.cgd1,
                                               type_service_fk__categorie_fk__nom_s="Gendarmerie", type_service_fk__nom_s="BTA",
                                               autorise_acces_consult_organisateur_b=True, commune_m2m=self.commune)
        self.agent_brg1 = UserFactory.create(username='agent_brg1', password=make("123"), default_instance=dep.instance, email='brg1@test.fr')
        self.agent_brg1.organisation_m2m.add(self.brigade1)
        self.brigade2 = ServiceConsulteFactory(instance_fk=dep.instance, precision_nom_s="BTA2", service_parent_fk=self.cgd2,
                                               type_service_fk__categorie_fk__nom_s="Gendarmerie", type_service_fk__nom_s="BTA",
                                               autorise_acces_consult_organisateur_b=True, commune_m2m=self.commune)
        self.agent_brg2 = UserFactory.create(username='agent_brg2', password=make("123"), default_instance=dep.instance, email='brg2@test.fr')
        self.agent_brg2.organisation_m2m.add(self.brigade2)
        self.agent_ddsp = UserFactory.create(username='agent_ddsp', password=make("123"), default_instance=dep.instance, email='ddsp@test.fr')
        self.ddsp = dep.organisation_set.get(precision_nom_s="DDSP")
        self.agent_ddsp.organisation_m2m.add(self.ddsp)
        self.commiss = ServiceConsulteFactory(instance_fk=dep.instance, precision_nom_s="Commissariat", service_parent_fk=self.ddsp,
                                              type_service_fk__categorie_fk__nom_s="Police", type_service_fk__nom_s="Commissariat",
                                              autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True, commune_m2m=self.commune)
        self.agent_commiss = UserFactory.create(username='agent_commiss', password=make("123"), default_instance=dep.instance, email='comm@test.fr')
        self.agent_commiss.organisation_m2m.add(self.commiss)
        self.agent_mairie = UserFactory.create(username='agent_mairie', password=make("123"), default_instance=dep.instance, email='mair@test.fr')
        self.agent_mairie.organisation_m2m.add(self.mairie1)
        self.cg = dep.organisation_set.get(precision_nom_s="CD")
        self.agent_cg = UserFactory.create(username='agent_cg', password=make("123"), default_instance=dep.instance, email='cg@test.fr')
        self.agent_cg.organisation_m2m.add(self.cg)
        self.cgserv = ServiceConsulteFactory.create(instance_fk=dep.instance, precision_nom_s="STD", service_parent_fk=self.cg,
                                                    type_service_fk__categorie_fk__nom_s="Conseil départemental", type_service_fk__nom_s="STD",
                                                    autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True, departement_m2m=dep)
        self.agent_cgserv = UserFactory.create(username='agent_cgserv', password=make("123"), default_instance=dep.instance, email='cgserv@test.fr')
        self.agent_cgserv.organisation_m2m.add(self.cgserv)
        self.cgsup = dep.organisation_set.get(precision_nom_s="CDSUP")
        self.agent_cgsup = UserFactory.create(username='agent_cgsup', password=make("123"), default_instance=dep.instance, email='cgsup@test.fr')
        self.agent_cgsup.organisation_m2m.add(self.cgsup)
        self.sdis = dep.organisation_set.get(precision_nom_s="SDIS")
        self.agent_sdis = UserFactory.create(username='agent_sdis', password=make("123"), default_instance=dep.instance, email='sdis@test.fr')
        self.agent_sdis.organisation_m2m.add(self.sdis)
        self.group = ServiceConsulteFactory.create(instance_fk=dep.instance, precision_nom_s="GROUP", service_parent_fk=self.sdis,
                                                   type_service_fk__categorie_fk__nom_s="Pompier", type_service_fk__nom_s="GROUP",
                                                   autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True, departement_m2m=dep)
        self.agent_group = UserFactory.create(username='agent_group', password=make("123"), default_instance=dep.instance, email='group@test.fr')
        self.agent_group.organisation_m2m.add(self.group)
        self.codis = dep.organisation_set.get(precision_nom_s="CODIS")
        self.agent_codis = UserFactory.create(username='agent_codis', password=make("123"), default_instance=dep.instance, email='codis@test.fr')
        self.agent_codis.organisation_m2m.add(self.codis)
        self.cis = ServiceConsulteFactory.create(instance_fk=dep.instance, precision_nom_s="CIS", service_parent_fk=self.group,
                                                 type_service_fk__categorie_fk__nom_s="Pompier", type_service_fk__nom_s="CIS",
                                                 autorise_acces_consult_organisateur_b=True, commune_m2m=self.commune)
        self.agent_cis = UserFactory.create(username='agent_cis', password=make("123"), default_instance=dep.instance, email='cis@test.fr')
        self.agent_cis.organisation_m2m.add(self.cis)

        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.ddsp, service_sortant_fk=self.commiss, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.edsr, service_sortant_fk=self.cgd1, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.edsr, service_sortant_fk=self.cgd2, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.edsr, service_sortant_fk=self.usg, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.edsr, service_sortant_fk=self.ggd, type_lien_s='adresser')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.sdis, service_sortant_fk=self.group,  type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.cg, service_sortant_fk=self.cgserv, type_lien_s='interroger')
        ServiceConsulteInteraction.objects.create(service_entrant_fk=self.cg, service_sortant_fk=self.cgsup, type_lien_s='adresser')

        for user in User.objects.all():
            user.optionuser.notification_mail = True
            user.optionuser.action_mail = True
            user.optionuser.save()

        EmailTemplate.objects.create(name='new_msg')

        # Création de l'événement
        self.manifestation = DcnmFactory.create(ville_depart=self.commune, structure_organisatrice_fk=self.structure, activite=activ,
                                                nom='Manifestation_Test', villes_traversees=(self.autrecommune,))
        self.manifestation.description = 'une course qui fait courir'
        self.manifestation.emprise = 1
        self.manifestation.nb_participants = 1
        self.manifestation.nb_organisateurs = 10
        self.manifestation.nb_spectateurs = 100
        self.manifestation.nb_signaleurs = 1
        self.manifestation.nb_vehicules_accompagnement = 0
        self.manifestation.nom_contact = 'durand'
        self.manifestation.prenom_contact = 'joseph'
        self.manifestation.tel_contact = '0605555555'
        self.manifestation.save()
        self.manifestation.notifier_creation()

        for file in ('reglement_manifestation', 'disposition_securite',
                     'itineraire_horaire', 'recepisse_declaration', 'visa_federation', 'convention_federation'):
            mon_fichier = open("/tmp/"+file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Suppression des fichiers créés dans /tmp
        """
        for file in ('reglement_manifestation', 'disposition_securite',
                     'itineraire_horaire', 'recepisse_declaration', 'visa_federation', 'convention_federation'):
            os.remove("/tmp/"+file+".txt")

        os.system('rm -R /tmp/maniftest/media/' + cls.dep.name + '/')

        super().tearDownClass()

    def confirmer_presence_cdsr(self, page):
        """ Confirmer sa présence à la réunion CDSR """
        # Vérifier l'action disponible
        self.assertContains(page, '<i class="ok-blanc"></i>Confirmer votre présence')
        url_confirmer = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+</i>Confirmer votre présence',  page.content.decode('utf-8'))
        self.assertTrue(hasattr(url_confirmer, 'group'))
        referer = re.search('href="(?P<url>(/[^"]+))rediger/"', page.content.decode('utf-8'))
        self.assertTrue(hasattr(referer, 'group'))
        retour = self.client.get(url_confirmer.group('url'), follow=True, HTTP_REFERER=referer.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Vous avez confirmer votre présence à la réunion CDSR")
        return retour

    def rediger_avis(self, page, objet="avis", precision=""):
        """
        Rédiger l'avis ou le préavis avant le rendu
        """
        # Vérifier l'action disponible
        if objet == "avis":
            self.assertContains(page, 'Rédiger l\'avis', count=1)
            # Rediger l'avis avec l'url fournie et tester la redirection
            redige = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rédiger l\'avis', page.content.decode('utf-8'))
        if objet == "préavis":
            self.assertContains(page, 'Rédiger le préavis', count=1)
            # Rediger l'avis avec l'url fournie et tester la redirection
            redige = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rédiger le préavis', page.content.decode('utf-8'))
        self.assertTrue(hasattr(redige, 'group'))
        data = {'prescriptions': precision, 'favorable': True}
        page = self.client.post(redige.group('url'), data=data, follow=True, HTTP_HOST='127.0.0.1:8000')
        return page

    def rendre_avis(self, page, objet="avis"):
        """
        Valider et rendre l'avis ou le préavis
        """
        # Vérifier l'action disponible
        if objet == "avis":
            self.assertContains(page, 'Valider et rendre l\'avis', count=1)
        if objet == "préavis":
            self.assertContains(page, 'Valider et rendre le préavis', count=1)
        # Valider l'événement avec l'url fournie et tester la redirection
        ackno = re.search('"span_valide_rendre_avis.+\\n.+\\n.+href="(?P<url>(/[^"]+))', page.content.decode('utf-8'))
        self.assertTrue(hasattr(ackno, 'group'))
        page = self.client.get(ackno.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        return page

    def distribuer_avis(self, page, list_service_pk):
        """
        Distribuer un avis à un service interne
        """
        # Vérifier l'action disponible
        self.assertContains(page, 'Envoyer une demande de préavis aux services internes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+préavis aux services internes', page.content.decode('utf-8'))
        self.assertTrue(hasattr(disp_form, 'group'))
        page = self.client.post(disp_form.group('url'), {'services_concernes': list_service_pk},
                                follow=True, HTTP_HOST='127.0.0.1:8000')
        return page

    def adresser_avis(self, page, service_pk):
        """
        Adresser un avis à un service supérieur
        """
        # Vérifier l'action disponible
        self.assertContains(page, 'Adresser l\'avis', count=1)
        # Formater l'avis de l'événement avec l'url fournie et tester la redirection
        form_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Adresser l\'avis', page.content.decode('utf-8'))
        self.assertTrue(hasattr(form_form, 'group'))
        page = self.client.post(form_form.group('url'), {'services_concerne': service_pk}, follow=True, HTTP_HOST='127.0.0.1:8000')
        return page

    def mandater_avis(self, page, service_pk):
        """
        Mandater un avis à un service supérieur
        """
        # Vérifier l'action disponible
        self.assertContains(page, 'Mandater l\'avis', count=1)
        # Passer l'événement à l'EDSR avec l'url fournie et tester la redirection
        pass_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Mandater l\'avis', page.content.decode('utf-8'))
        self.assertTrue(hasattr(pass_form, 'group'))
        page = self.client.post(pass_form.group('url'), {'service_consulte_fk': service_pk}, follow=True, HTTP_HOST='127.0.0.1:8000')
        return page

    def vue_detail(self, page):
        """
        Appel de la vue de détail de la manifestation
        :param page: réponse précédente
        :return: reponse suivante
        """
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', page.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test')
        return page

    def affichage_avis(self):
        """
        Affichage des avis émis pour l'événement avec leur status
        """
        for avis in self.instruction.get_tous_avis():
            if avis.etat != 'rendu':
                print(avis, end=" ; ")
                print(avis.etat)

    def aucune_action(self, page):
        """
        Test auncune action affichée dans la zone action de la dashboard
        :param page: réponse précédente
        """
        action = re.search('aucune action nécessaire', page.content.decode('utf-8'))
        attente = re.search(' devez attendre ', page.content.decode('utf-8'))
        if not action and not attente:
            self.assertIsNotNone(action, msg="message non trouvé")

    def presence_avis(self, username, state, log=True):
        """
        Appel de la dashboard de l'utilisateur pour tester la présence et l'état de l'événement
        :param username: agent considéré
        :param state: couleur de l'événement
        :param log: booléen pour connecter l'agent
        :return: retour: la réponse http
        """
        # Connexion avec l'utilisateur
        if log:
            self.assertTrue(self.client.login(username=username, password='123'))
        # Appel de la page
        if 'agent_mairie' in username:
            if username.endswith("agent_mairie"):
                pk = str(self.mairie1.pk)
            else:
                pk = str(getattr(self, 'mairie' + username.replace('agent_mairie', '')).pk)
            retour = self.client.get('/instructions/tableaudebord/' + pk + '/?filtre_specifique=serviceconsulte', HTTP_HOST='127.0.0.1:8000')
        else:
            retour = self.client.get('/instructions/tableaudebord/', follow=True, HTTP_HOST='127.0.0.1:8000')
        # print(retour.content.decode('utf-8'))
        # f = open('/var/log/manifsport/test_output.html', 'w', encoding='utf-8')
        # f.write(str(retour.content.decode('utf-8')).replace('\\n',""))
        # f.close()
        # Test du contenu
        if state == 'none':
            if username in ["agent_cis", "agent_codis", "agent_brg1", "agent_brg2"]:
                nb_bloc = re.search('test_nb_rendu">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
            else:
                # Recherche sur "avis demandés", on sous-entend que c'est un TdB agent
                nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
            self.assertTrue(hasattr(nb_bloc, 'group'))
            self.assertEqual('0', nb_bloc.group('nb'))
        # Test du chiffre dans la bonne case
        elif state == 'nouveau':
            nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
            self.assertTrue(hasattr(nb_bloc, 'group'))
            self.assertEqual('1', nb_bloc.group('nb'))
        elif state == 'demarre':
            nb_bloc = re.search('test_nb_demarre">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
            self.assertTrue(hasattr(nb_bloc, 'group'))
            self.assertEqual('1', nb_bloc.group('nb'))
        elif state == 'encours':
            nb_bloc = re.search('test_nb_encours">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
            self.assertTrue(hasattr(nb_bloc, 'group'))
            self.assertEqual('1', nb_bloc.group('nb'))
        elif state == 'rendu':
            nb_bloc = re.search('test_nb_rendu">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
            self.assertTrue(hasattr(nb_bloc, 'group'))
            self.assertEqual('1', nb_bloc.group('nb'))
        else:
            self.assertEqual('0', '1', 'paramètre inconnu')
        return retour
