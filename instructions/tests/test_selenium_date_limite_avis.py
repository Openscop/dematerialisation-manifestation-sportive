import re, time
from selenium.webdriver.common.by import By
from .test_base_selenium import SeleniumCommunClass
from django.test import tag, override_settings
from datetime import date, timedelta
from selenium.webdriver.common.keys import Keys

class InstructionDateLimiteAvis(SeleniumCommunClass):
  """
  Test modification de la date_limite_reponse_dt
  """
  DELAY = 0.35

  @classmethod
  def setUpClass(cls):
    """
    Préparation du test
    """
    print('============ Date Limite Avis (Sel) =============')
    SeleniumCommunClass.init_setup(cls)
    super().setUpClass()

  @tag('selenium')
  @override_settings(MEDIA_ROOT='/tmp/maniftest/media/', DEBUG=True)
  def test_dateLimite(self, selenium=None):

        print('**** test 1 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))",
                             reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
          with open('/tmp/' + file + '.txt') as openfile:
              url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
              self.assertTrue(hasattr(url_file, 'group'))
              self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        self.assertTrue(hasattr(envoi, 'group'))
        self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 1 instructeur - changement de date limite reponse CORRECT - Fixée à demain ****')
        self.connexion('instructeur') # Instruction de l'avis par la préfecture
        self.presence_avis('instructeur', 'nouveau') # Vérification de la présence de l'événement en nouveau
        self.vue_detail() # Appel de la vue de détail et test présence manifestation
        self.selenium.find_element(By.ID, f'card_{self.fede.pk}').click() # ouvrir l'accordion avis
        self.selenium.execute_script("window.scroll(0, document.body.scrollHeight)")# scroller vers le bas de la fenêtre
        time.sleep(self.DELAY * 4)
        self.selenium.find_element(By.CLASS_NAME, 'btn-date-form').click() # ouvre la modale
        element = self.selenium.find_element(By.CLASS_NAME, 'datetime_picker_ar') # sélectionne le champ input datetimepicker
        element.click()
        element.send_keys(Keys.CONTROL, "a")  # Sélectionner tout son contenu
        element.send_keys(Keys.BACKSPACE) # Supprimer tout
        today = date.today() # on obtient la date du jour
        tomorrow = (today + timedelta(days=1)).strftime('%d/%m/%y') # on calcule la date de demain
        element.send_keys(tomorrow) # on met la date de demain dans le datetimepicker
        element.send_keys(Keys.ESCAPE) # réduire le calendrier déroulant
        message_erreur = self.selenium.find_element(By.ID, 'message-erreur-date')
        # print(message_erreur.is_displayed())
        if not message_erreur.is_displayed():
          print('************ Aucun message d\'erreur ne s\'affiche')
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click() # soumettre la nouvelle date
        time.sleep(self.DELAY * 2)

        print('**** test 2 instructeur - changement date limite réponse INCORRECT - < aujourd\'hui - Fixée à hier ****')
        self.selenium.find_element(By.ID, f'card_{self.fede.pk}').click()
        self.selenium.execute_script("window.scroll(0, document.body.scrollHeight)")
        self.selenium.find_element(By.CLASS_NAME, 'btn-date-form').click()
        element = self.selenium.find_element(By.CLASS_NAME, 'datetime_picker_ar')
        element.click()
        element.send_keys(Keys.CONTROL, "a")  # Sélectionner tout
        element.send_keys(Keys.BACKSPACE) # Supprimer tout
        today = date.today() # on obtient la date du jour
        yesterday = (today - timedelta(days=1)).strftime('%d/%m/%y') # on calcule la date d'hier
        element.send_keys(yesterday) # on met la date d'hier dans le datetimepicker
        element.send_keys(Keys.ESCAPE) # on réduit le calendrier
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click() # soumettre la nouvelle date (pour avoir le message d'erreur)
        time.sleep(self.DELAY * 4)
        message_erreur = self.selenium.find_element(By.ID, 'message-erreur-date')
        if message_erreur.is_displayed():
          print('************ Un message d\'erreur s\'affiche')
        self.selenium.find_element(By.XPATH, '//input[@value="Annuler"]').click()  # soumettre la nouvelle date (pour avoir le message d'erreur)
        time.sleep(self.DELAY * 4)

        print('**** test 3 instructeur - changement de date limite réponse INCORRECT- 5 jours ou moins avant manif - Fixée à la date début manif ****')
        self.selenium.find_element(By.ID, f'card_{self.fede.pk}').click()
        self.selenium.execute_script("window.scroll(0, document.body.scrollHeight)")
        self.selenium.find_element(By.CLASS_NAME, 'btn-date-form').click()
        time.sleep(self.DELAY * 2)
        element = self.selenium.find_element(By.CLASS_NAME, 'datetime_picker_ar')
        element.click()
        element.send_keys(Keys.CONTROL, "a")  # Sélectionner tout
        element.send_keys(Keys.BACKSPACE) # Supprimer tout
        date_debut_manif = (self.manifestation.date_debut).strftime('%d/%m/%y') # on appelle la date de début de manif
        element.send_keys(date_debut_manif) # on met la date de debut de manif dans le datetimepicker
        element.send_keys(Keys.ESCAPE) # on réduit le calendrier
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click() # soumettre la nouvelle date (pour avoir le message d'erreur)
        time.sleep(self.DELAY * 4)
        message_erreur = self.selenium.find_element(By.ID, 'message-erreur-date')
        if message_erreur.is_displayed():
          print('************ Un message d\'erreur s\'affiche')
        self.selenium.find_element(By.XPATH, '//input[@value="Annuler"]').click()  # fermer la modale et annuler la mise à jour de la date limite