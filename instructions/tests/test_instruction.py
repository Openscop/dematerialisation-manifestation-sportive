# coding: utf-8
from django.test import TestCase
from django.contrib.auth.hashers import make_password as make
from django.contrib.contenttypes.models import ContentType

from core.factories import UserFactory
from messagerie.models import Message
from instructions.factories import InstructionFactory, AvisInstructionFactory


class InstructionTests(TestCase):
    """ Tests des instructions """

    # Configuration
    def setUp(self):
        """
        Tests : Créer une manifestation ainsi que la demande d'instruction

        Créer les services administratifs nécessaires lors de la création d'une
        manifestation sportive. La manifestation créée ici est une manifestation
        sportive non motorisée.
        """
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme

        self.instruction = InstructionFactory.create()
        self.manifestation = self.instruction.manif
        self.commune = self.manifestation.ville_depart
        self.mairie = self.commune.organisation_set.first()
        self.departement = self.commune.get_departement()
        self.arrondissement = self.commune.get_arrondissement()
        self.instance = self.departement.get_instance()
        self.prefecture = self.arrondissement.organisations.first()
        self.instructeur = UserFactory.create(username='instructeur', password=make("123"), default_instance=self.instance)
        self.instructeur.organisation_m2m.add(self.prefecture)
        self.organisateur = UserFactory.create(username='organisateur', password=make("123"), default_instance=self.instance)
        self.organisateur.organisation_m2m.add(self.manifestation.structure_organisatrice_fk)
        self.manifestation.declarant = self.organisateur
        self.cg = self.departement.organisation_set.get(precision_nom_s="CD")
        self.edsr = self.departement.organisation_set.get(precision_nom_s="EDSR")
        self.ddsp = self.departement.organisation_set.get(precision_nom_s="DDSP")
        self.agent_ddsp = UserFactory.create(username='agent_ddsp', password=make("123"), default_instance=self.instance)
        self.agent_ddsp.organisation_m2m.add(self.ddsp)
        self.sdis = self.departement.organisation_set.get(precision_nom_s="SDIS")
        self.ggd = self.departement.organisation_set.get(precision_nom_s="GGD")
        self.activite = self.manifestation.activite
        self.federation = self.manifestation.get_federation()

    # Tests
    def test_par_instructeur(self):
        """ Vérifier que l'instructeur est bien en charge de la délivrance de l'instruction """
        self.assertIn(self.instructeur, self.instruction.get_instructeurs_prefecture())

    def test_methodes(self):
        """ Tester la représentation texte de l'instruction """
        self.assertEqual(str(self.instruction), str(self.manifestation))
        self.assertEqual(self.instruction.get_absolute_url(), '/instructions/' + str(self.instruction.pk) + '/')
        self.assertEqual(self.instruction.get_instance(), self.instance)

    def test_prefecture(self):
        """ Tester que la préfecture de l'instruction est correcte """
        self.assertEqual(self.instruction.get_prefecture_concernee(), self.prefecture)

    def test_aucun_avis(self):
        """ Tester la validation des avis """
        self.assertEqual(self.instruction.get_nb_avis(), 0)
        self.assertTrue(self.instruction.avis_tous_rendus())

    def test_avis(self):
        """ Tester la validation des avis """
        avis = AvisInstructionFactory.create(instruction=self.instruction, service_consulte_fk=self.ddsp,
                                             service_demandeur_fk=self.prefecture)
        self.assertIn(self.ddsp, self.instruction.get_tous_agents_avec_service())
        self.assertIn(self.agent_ddsp, self.instruction.get_tous_agents_avec_service())
        self.assertEqual(self.instruction.get_avis_service(self.ddsp), avis)
        self.assertEqual(self.instruction.get_nb_avis(), 1)
        self.assertEqual(self.instruction.get_nb_avis_rendus(), 0)
        self.assertEqual(self.instruction.get_nb_avis_non_rendus(), 1)
        self.assertFalse(self.instruction.avis_tous_rendus())  # L'avis fédération (tjs créé par défaut) n'est pas validé
        avis = self.instruction.get_tous_avis().first()  # on sait que c'est un avis federation
        avis.etat = 'rendu'
        avis.save()
        self.assertEqual(avis.etat, 'rendu')
        self.assertEqual(self.instruction.get_nb_avis(), 1)
        self.assertEqual(self.instruction.get_nb_avis_rendus(), 1)
        self.assertEqual(self.instruction.get_nb_avis_non_rendus(), 0)
        self.assertTrue(self.instruction.avis_tous_rendus())  # L'avis fédération est désormais validé

    def test_message_log(self):
        """ Tester les messages """
        self.manifestation.notifier_creation()
        notification = Message.objects.all().count()
        # un message à l'organisateur. pas de message instructeur, ni de message fédération : les agents sont créés après l'instruction
        self.assertEqual(notification, 1)
        self.instruction.notifier_creation()
        notification = Message.objects.all().count()
        self.assertEqual(notification, 2)
