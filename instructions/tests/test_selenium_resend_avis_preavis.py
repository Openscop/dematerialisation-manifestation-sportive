import re, time

from django.test import tag, override_settings
from django.conf import settings

from post_office.models import Email
from selenium.webdriver.common.by import By

from .test_base_selenium import SeleniumCommunClass
from instructions.models import Avis
from messagerie.models import Message


class InstructionAjoutPjTests(SeleniumCommunClass):
    """
    Test des boutons resend des avis et préavis dans un instruction du circuit d'instance EDSR avec sélénium pour une Dcnm
        circuit_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    Test sur un service simple et un service complexe.
    Vérifie que le bouton resend est présent et envoie bien un email quand utilisé,
    et que le bouton n'est plus présent quand l'avis ou le préavis est rendu
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ resend avis / préavis (Sel) =============')
        SeleniumCommunClass.init_setup(cls)
        cls.avis_nb = 2
        super().setUpClass()

    @tag('selenium')
    @override_settings(DEBUG=True)
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
    def test_Resend(self):
        """
        Test du resend pour avis et préavis pendant l'instruction
        """
        assert settings.DEBUG

        print('**** test 1 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 2 distribution avis ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Changer l'assignation").click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Me l'assigner").click()
        time.sleep(self.DELAY * 2)
        self.distribuer_avis(['Police'])
        time.sleep(self.DELAY)
        Email.objects.all().delete()
        Message.objects.all().delete()

        # Vérifier le boutons de resend DDSP et faire resend
        avis = self.selenium.find_element(By.XPATH, "//div[contains(string(),'DDSP')][@data-bs-toggle='collapse']")
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY * 5)
        avis.click()
        carte = self.selenium.find_element(By.ID, f"card_{str(self.ddsp.pk)}")
        self.assertIn('Relancer cette demande', carte.text)
        carte.find_element(By.PARTIAL_LINK_TEXT, 'Relancer cette demande').click()
        # Vérifier le boutons de resend Fédération
        avis = self.selenium.find_element(By.XPATH, "//div[contains(string(),'FÉDÉRATION')][@data-bs-toggle='collapse']")
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY*2)
        avis.click()
        carte = self.selenium.find_element(By.XPATH, "//div[contains(string(),'FÉDÉRATION')][@data-bs-toggle='collapse']/following::*")
        self.assertIn('Relancer cette demande', carte.text)
        carte.find_element(By.PARTIAL_LINK_TEXT, 'Relancer cette demande').click()
        # Vérifier les emails envoyés
        # TODO
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_ddsp.email])
        self.assertEqual(outbox[1].to, [self.agent_fede.email])
        self.assertIn('Demande d\'avis à traiter pour la manifestation Manifestation_Test', messages[0].corps)
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_consulte_fk=self.ddsp).id)          # Avis DDSP
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        Email.objects.all().delete()
        Message.objects.all().delete()
        # Utiliser le bouton général pour tous les avis non reçus
        self.selenium.execute_script("window.scroll(0, 1200)")
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.XPATH, "//a[@id='relance_avis_btn']").click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, "//p[@id='relance_avis_div']/a").click()
        time.sleep(self.DELAY)
        # Vérifier les emails envoyés
        # TODO
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_ddsp.email])
        self.assertEqual(outbox[1].to, [self.agent_fede.email])
        self.assertIn('Demande d\'avis à traiter pour la manifestation Manifestation_Test', messages[0].corps)
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_consulte_fk=self.ddsp).id)          # Avis DDSP
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        Email.objects.all().delete()
        Message.objects.all().delete()

        # Vérifier le passage en encours et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY*3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 3 agent fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.connexion('agent_fede')
        self.presence_avis('agent_fede', 'nouveau')
        self.assertIn('Délai : 28 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_fede', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        Email.objects.all().delete()
        Message.objects.all().delete()
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier le boutons de resend Fédération
        avis = self.selenium.find_element(By.XPATH, "//div[contains(string(),'FÉDÉRATION')][@data-bs-toggle='collapse']")
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY)
        avis.click()
        carte = self.selenium.find_element(By.XPATH, "//div[contains(string(),'FÉDÉRATION')][@data-bs-toggle='collapse']/following::*")
        self.assertNotIn('Relancer cette demande', carte.text)
        # Utiliser le bouton général pour tous les avis non reçus
        self.selenium.execute_script("window.scroll(0, 1200)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, "//a[@id='relance_avis_btn']").click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, "//p[@id='relance_avis_div']/a").click()
        time.sleep(self.DELAY)
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].to, [self.agent_ddsp.email])
        self.assertIn('Demande d\'avis à traiter pour la manifestation Manifestation_Test', messages[0].corps)
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_consulte_fk=self.ddsp).id)          # Avis DDSP
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        time.sleep(self.DELAY)
        self.deconnexion()

        print('**** test 4 avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.distribuer_preavis('abr')
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('COMMISSARIAT', self.selenium.page_source)
        Email.objects.all().delete()
        Message.objects.all().delete()
        # Tester bouton resend et faire resend
        self.selenium.execute_script("window.scroll(0, 500)")
        preavis = self.selenium.find_element(By.ID, f'card_{self.commiss.pk}')
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY)
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 600)")
        time.sleep(self.DELAY)
        carte = self.selenium.find_element(By.ID, f'card_{self.commiss.pk}')
        self.assertIn('Relancer cette demande', carte.text)
        carte.find_element(By.PARTIAL_LINK_TEXT, 'Relancer cette demande').click()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].to, [self.agent_commiss.email])
        self.assertIn('Demande de préavis à traiter pour la manifestation Manifestation_Test', messages[0].corps)
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_consulte_fk=self.commiss).id)            # Préavis DDSP
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        Email.objects.all().delete()
        Message.objects.all().delete()
        # Tester aucune action disponible
        # self.aucune_action()
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ddsp', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 5 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        self.connexion('agent_commiss')
        self.presence_avis('agent_commiss', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis("préavis")
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_commiss', 'rendu')
        self.deconnexion()

        print('**** test 6 agent ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en encours
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rédiger l\'avis', self.selenium.page_source)
        # Tester bouton resend
        self.selenium.execute_script("window.scroll(0, 500)")
        preavis = self.selenium.find_element(By.ID, f'card_{self.commiss.pk}')
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY)
        preavis.click()
        carte = self.selenium.find_element(By.ID, f'card_{self.commiss.pk}')
        self.assertNotIn('Relancer cette demande', carte.text)
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ddsp', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier le boutons de resend DDSP
        avis = self.selenium.find_element(By.ID, f'card_{self.ddsp.pk}')
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY)
        avis.click()
        carte = self.selenium.find_element(By.ID, f'card_{self.ddsp.pk}')
        self.assertNotIn('Relancer cette demande', carte.text)
        self.deconnexion()
