import re, time
import datetime

from django.test import tag, override_settings
from django.contrib.auth.hashers import make_password as make

from allauth.account.models import EmailAddress
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By

from core.models import Instance
from .test_base_selenium import SeleniumCommunClass
from core.factories import UserFactory
from administrative_division.factories import ArrondissementFactory, CommuneFactory, DepartementFactory
from evenements.factories import DcnmFactory
from sports.factories import ActiviteFactory
from instructions.factories import InstructionFactory
from structure.factories.structure import StructureOrganisatriceFactory
from structure.factories.service import FederationFactory


class Instruction_Mairie_Test(SeleniumCommunClass):
    """
    Test des instructions en mairie avec sélénium pour une Dcnm
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ Circuit Simple (Sel) =============')
        SeleniumCommunClass.init_setup(cls)
        cls.autre_dep = DepartementFactory.create(name='43',
                                                  instance__name="autre instance de test",
                                                  instance__etat_s="ouvert",
                                                  instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                                  instance__acces_arrondissement=2)
        autre_arrondissement = ArrondissementFactory.create(name='Brioude', code='100', departement=cls.autre_dep)
        cls.prefecture_2 = autre_arrondissement.organisations.first()
        cls.autre_dep_commune = CommuneFactory(name='Agnat', arrondissement=autre_arrondissement)
        activ = ActiviteFactory.create()
        cls.fede2 = FederationFactory.create(instance_fk=cls.autre_dep.instance, departement_m2m=cls.autre_dep,
                                             discipline_fk=activ.discipline)

        # Création des utilisateurs
        cls.organisateur2 = UserFactory.create(username='organisateur_2', password=make("123"),
                                               default_instance=cls.autre_dep.instance)
        cls.structure2 = StructureOrganisatriceFactory.create(precision_nom_s="structure_test",
                                                              commune_m2m=cls.autre_dep_commune, instance_fk=cls.autre_dep.instance)
        cls.organisateur2.organisation_m2m.add(cls.structure2)
        EmailAddress.objects.create(user=cls.organisateur2, email='organisateur2@example.com', primary=True,
                                    verified=True)
        cls.instructeur_2 = UserFactory.create(username='instructeur_2',
                                               email='instructeur2@example.com',
                                               password=make("123"),
                                               default_instance=cls.autre_dep.instance)

        cls.instructeur_2.organisation_m2m.add(cls.prefecture_2)
        EmailAddress.objects.create(user=cls.instructeur_2, email='instructeur-2@test.fr', primary=True, verified=True)

        # Création de l'événement
        cls.manifestation_1 = DcnmFactory.create(ville_depart=cls.autre_dep_commune, structure_organisatrice_fk=cls.structure2,
                                                 nom='Manifestation_Test_Mairie', activite=activ)

        cls.instruction1 = InstructionFactory.create(manif=cls.manifestation_1)
        cls.avis_nb = 3

        super().setUpClass()


    @tag('selenium')
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/', DEBUG=True)
    def test_Instruction_Mairie(self):

        print('**** test 1 instruction mairie ****')
        self.connexion('instructeur_2')
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'En mairie').click()
        time.sleep(self.DELAY * 2)
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY)
        url = self.selenium.current_url
        self.vue_detail()
        wait = WebDriverWait(self.selenium, 15)
        wait.until(lambda driver: self.selenium.current_url != url)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        time.sleep(self.DELAY * 2)
        # Vérifier l'action disponible
        self.assertIn("Envoyer une demande d'avis", self.selenium.page_source)
        time.sleep(self.DELAY * 2)
        # Demander pièce jointe complémentaire
        self.selenium.find_element(By.ID, "pj_instru").click()
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, "btnedit_supp").click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, "nom_pj").send_keys('Pj Supp')
        now = datetime.datetime.now()
        tday = now.strftime("%Y-%m-%d")
        datefield = self.selenium.find_element(By.ID, "date")
        datefield.click()
        time.sleep(self.DELAY * 2)
        datefield.clear()
        time.sleep(self.DELAY * 2)
        datefield.send_keys(tday)
        self.selenium.find_element(By.ID, "aide").send_keys('Test pj sup')

        self.selenium.find_element(By.ID, "btnsubmit_supp").click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 0)")
        time.sleep(self.DELAY * 2)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)

        time.sleep(self.DELAY * 5)

        self.selenium.find_element(By.ID, "pj_instru").click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY * 2)
        self.assertIn('Pj Supp', self.selenium.page_source)
        time.sleep(self.DELAY * 2)

        self.selenium.execute_script("window.scroll(0, 0)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_elements(By.CLASS_NAME, 'link_onglet')[0].click()

        time.sleep(self.DELAY)
        self.distribuer_avis(['Police', 'Pompier', 'Mairie'])
        self.assertIn('Pompier', self.selenium.page_source)
        self.assertIn('SDIS', self.selenium.page_source)
        self.assertIn('Police', self.selenium.page_source)
        self.assertIn('DDSP', self.selenium.page_source)
        self.assertIn('Mairie', self.selenium.page_source)
        time.sleep(5)
        # Vérifier le passage en encours et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1600)")
        time.sleep(self.DELAY*3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1800)")
        time.sleep(5)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'En mairie').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()