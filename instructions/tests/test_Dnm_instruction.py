import re

from django.test import TestCase
from django.contrib.auth.hashers import make_password as make
from django.contrib.contenttypes.models import ContentType

from post_office.models import EmailTemplate

from core.models import Instance
from evenements.factories import DnmFactory
from evenements.models import Manif
from instructions.factories import InstructionFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from structure.factories.structure import StructureOrganisatriceFactory
from core.factories import UserFactory
from sports.factories import ActiviteFactory


class InstructionDnm(TestCase):
    """
    Test du circuit d'instruction d'une DNM
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========= DNM 1 commune (Clt) ==========')
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__instruction_mode=Instance.IM_CIRCUIT_B)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        cls.prefecture = arrondissement.organisations.first()
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.mairie = cls.commune.organisation_set.first()
        structure = StructureOrganisatriceFactory.create(precision_nom_s="structure_test",
                                                         commune_m2m=cls.commune, instance_fk=dep.instance)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        cls.instructeur = UserFactory.create(username='instructeur', password=make("123"), default_instance=dep.instance)
        cls.instructeur.organisation_m2m.add(cls.prefecture)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make("123"), default_instance=dep.instance)
        cls.agent_mairie.organisation_m2m.add(cls.mairie)
        cls.organisateur = UserFactory.create(username='organisateur', password=make("123"), default_instance=dep.instance)
        cls.organisateur.organisation_m2m.add(structure)

        EmailTemplate.objects.create(name='new_msg')

        # Création de l'événement
        cls.manifestation = DnmFactory.create(ville_depart=cls.commune, structure_organisatrice_fk=structure,
                                              nom='Manifestation_Test', activite=activ)

    def test_Instruction_Dnm(self):
        print('**** test 1 creation manif ****')
        manif = Manif.objects.get()
        self.instruction = InstructionFactory.create(manif=manif)
        manif.declarant = self.organisateur
        manif.save()
        self.instruction.notifier_creation()

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.structure_organisatrice_fk, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline)
        self.assertEqual(str(self.instruction.manif), str(self.manifestation))

        print('**** test 2 instruction préfecture ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur', password='123'))
        # Appel de la page
        url = f'/instructions/tableaudebord/{str(self.prefecture.pk)}/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content)
        # Aucune instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc1.group('nb')) + int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        # Appel de la page
        url = url + '?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # Une instruction dans le TdB mairie
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))

        print('**** test 3 instruction mairie ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='agent_mairie', password='123'))
        url = f'/instructions/tableaudebord/{str(self.mairie.pk)}/?filtre_specifique=instructionmairie'
        # Appel de la page
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # Une instruction dans le TdB mairie
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))


class InstructionDnm2com(TestCase):
    """
    Test du circuit d'instruction d'une DNM avec 1 autre commune traversée
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========= DNM 2 communes (Clt) ==========')
        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__instruction_mode=Instance.IM_CIRCUIT_B)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        cls.prefecture = arrondissement.organisations.first()
        cls.depart = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.commune = CommuneFactory(name='Aboen', arrondissement=arrondissement)
        cls.mairie = cls.commune.organisation_set.first()
        structure = StructureOrganisatriceFactory.create(precision_nom_s="structure_test",
                                                         commune_m2m=cls.commune, instance_fk=dep.instance)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        cls.instructeur = UserFactory.create(username='instructeur', password=make("123"), default_instance=dep.instance)
        cls.instructeur.organisation_m2m.add(cls.prefecture)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make("123"), default_instance=dep.instance)
        cls.agent_mairie.organisation_m2m.add(cls.mairie)
        cls.organisateur = UserFactory.create(username='organisateur', password=make("123"), default_instance=dep.instance)
        cls.organisateur.organisation_m2m.add(structure)

        EmailTemplate.objects.create(name='new_msg')

        # Création de l'événement
        cls.manifestation = DnmFactory.create(ville_depart=cls.commune, structure_organisatrice_fk=structure, activite=activ,
                                              villes_traversees=[cls.commune, ], nom='Manifestation_Test')

    def test_Instruction_Dnm(self):
        print('**** test 1 creation manif ****')
        manif = Manif.objects.get()
        manif.declarant = self.organisateur
        manif.save()
        self.instruction = InstructionFactory.create(manif=manif)
        self.instruction.notifier_creation()

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.structure_organisatrice_fk, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline)
        self.assertEqual(str(self.instruction.manif), str(self.manifestation))

        print('**** test 2 instruction préfecture ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur', password='123'))
        # Appel de la page
        url = f'/instructions/tableaudebord/{str(self.prefecture.pk)}/?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content)
        # Aucune instruction dans le TdB mairie
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc1.group('nb')) + int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        # Appel de la page
        url = f'/instructions/tableaudebord/{str(self.prefecture.pk)}/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))

        print('**** test 3 instruction mairie ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='agent_mairie', password='123'))
        url = f'/instructions/tableaudebord/{str(self.mairie.pk)}/?filtre_specifique=instructionmairie'
        # Appel de la page
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # Aucune instruction dans le TdB mairie
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc1.group('nb')) + int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
