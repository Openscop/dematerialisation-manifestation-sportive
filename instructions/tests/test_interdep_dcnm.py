import re

from django.test import override_settings
from django.contrib.auth.hashers import make_password as make
from django.shortcuts import reverse
from post_office.models import Email
from bs4 import BeautifulSoup as Bs

from core.models import Instance
from .test_base import TestCommunClass
from evenements.factories import DvtmFactory
from instructions.models import Instruction, Avis
from sports.factories import ActiviteFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from core.factories import UserFactory
from messagerie.models import Message


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class CircuitInterdepDvtmTests(TestCommunClass):
    """
    Test de la fonction interdépartementalité
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('============ Interdep Dvtm (Clt) =============')

        TestCommunClass.init_setup(cls)
        # Création des objets
        dep43 = DepartementFactory.create(name='43',
                                          instance__name="instance de test 43",
                                          instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                          instance__etat_s="ouvert",
                                          instance__acces_arrondissement=2)
        arrondissement43 = ArrondissementFactory.create(name='Yssingeaux', code='433', departement=dep43)
        cls.prefecture43 = arrondissement43.organisations.first()
        cls.commune43 = CommuneFactory(name='Yssingeaux', arrondissement=arrondissement43)

        cls.instructeur43 = UserFactory.create(username='instructeur43', password=make("123"),
                                               default_instance=dep43.instance, email='inst43@test.fr')
        cls.instructeur43.optionuser.notification_mail = True
        cls.instructeur43.optionuser.action_mail = True
        cls.instructeur43.optionuser.save()
        cls.instructeur43.organisation_m2m.add(cls.prefecture43)
        cls.agent_mairie43 = UserFactory.create(username='agent_mairie43', password=make("123"),
                                                default_instance=dep43.instance, email='mair43@test.fr')
        cls.agent_mairie43.optionuser.notification_mail = True
        cls.agent_mairie43.optionuser.action_mail = True
        cls.agent_mairie43.optionuser.save()
        cls.mairie43 = cls.commune43.organisation_set.first()
        cls.agent_mairie43.organisation_m2m.add(cls.mairie43)

        # Création de l'événement
        activ = ActiviteFactory.create()
        cls.manif = DvtmFactory.create(ville_depart=cls.commune, structure_organisatrice_fk=cls.structure, activite=activ,
                                       nom='Manifestation_Dvtm', villes_traversees=(cls.autrecommune,))
        cls.manif.description = 'une course qui fait courir'
        cls.manif.nb_participants = 10
        cls.manif.nb_personnes_pts_rassemblement = 10
        cls.manif.nb_organisateurs = 10
        cls.manif.nb_spectateurs = 100
        cls.manif.vehicules = 1
        cls.manif.nb_vehicules_accompagnement = 0
        cls.manif.dans_calendrier = True
        cls.manif.nom_contact = 'durand'
        cls.manif.prenom_contact = 'joseph'
        cls.manif.tel_contact = '0605555555'
        cls.manif.declarant = cls.organisateur
        cls.manif.save()
        cls.manif.departements_traverses.add(dep43)
        cls.manif.ville_depart_interdep_mtm.add(cls.commune43)
        cls.manif.gestion_instance_instruites()
        cls.manif.save()
        cls.manif.notifier_creation()

    def vue_detail(self, page):
        """
        Appel de la vue de détail de la manifestation
        :param page: réponse précédente
        :return: reponse suivante
        """
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', page.content.decode('utf-8'))
        if hasattr(detail, 'group'):
            page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Dvtm')
        return page

    def affichage_avis(self, instruction):
        """
        Affichage des avis émis pour l'événement avec leur status
        """
        for avis in instruction.get_tous_avis():
            print('\t' + str(avis), end=" ; ")
            print(avis.etat)

    def test_Interdep_Dvtm(self):
        """
        Test de l'interdépartementalité avec une manif Dvtm
        """

        # def print(string="", end=None):
        #     # Supprimer les prints hors debug
        #     pass

        print('**** test 1 creation manif ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('2', nb_bloc.group('nb'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\'\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Dvtm')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = [("Réglement de la manifestation", "reglement_manifestation"),
                       ("Dispositions prises pour la sécurité", "disposition_securite"),
                       ("Itinéraire horaire pour le département 42", "itineraire_horaire"),
                       ("Itinéraire horaire pour le département 43", "itineraire_horaire")]
        for file, fichier in liste_files:
            with open('/tmp/' + fichier + '.txt') as openfile:
                # On cherche le nom du fichier qui dépend du nombre de tests effectués
                nom_fichier = re.search(file + '.*\\n.*\\n.+<i class="ctx-help-manif_form_(?P<nom>(.+)) float-end', reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(nom_fichier, 'group'), msg="pas de match pour " + file)
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + nom_fichier.group('nom'), reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'), msg="pas de match pour " + nom_fichier.group('nom'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        self.assertTrue(hasattr(declar, 'group'))
        self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        self.instruction = Instruction.objects.get(manif=self.manif, instruction_principale__isnull=True)
        # Vérification de la création de l'événement
        self.assertEqual(str(self.instruction), str(self.manif))
        self.assertEqual(2, Instruction.objects.filter(manif=self.manif).count())
        self.assertEqual(2, self.instruction.manif.instance_instruites.count())
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        index = len(outbox)
        index_mess = len(messages)
        self.assertEqual(len(messages), 6)
        self.assertIn('créer un nouveau dossier', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.organisateur)
        self.assertIn('créer un nouveau dossier', messages[1].corps)
        self.assertEqual(1, messages[1].message_enveloppe.all().count())
        self.assertEqual(messages[1].message_enveloppe.first().destinataire_id, self.organisateur)
        self.assertIn('nouveau dossier a été envoyé', messages[3].corps)
        self.assertEqual(1, messages[3].message_enveloppe.all().count())
        self.assertEqual(messages[3].message_enveloppe.first().destinataire_id, self.instructeur)
        self.assertIn('nouveau dossier a été envoyé', messages[4].corps)
        self.assertEqual(1, messages[4].message_enveloppe.all().count())
        self.assertEqual(messages[4].message_enveloppe.first().destinataire_id, self.instructeur43)
        self.assertIn('accusé de réception', messages[5].corps)
        self.assertEqual(1, messages[5].message_enveloppe.all().count())
        self.assertEqual(messages[5].message_enveloppe.first().destinataire_id, self.organisateur)
        self.assertEqual(len(outbox), 4)
        # self.assertEqual(outbox[0].to, [self.prefecture.email_s])
        # self.assertEqual(outbox[1].to, [self.prefecture43.email_s])
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[1].to, [self.instructeur43.email])
        self.assertEqual(outbox[1].subject, 'Nouveau dossier à instruire')
        self.assertEqual(outbox[2].to, [self.organisateur.email])
        self.assertEqual(outbox[2].subject, 'Accusé de réception : Manifestation_Dvtm')
        # self.assertEqual(outbox[3].to, [self.structure.email_s])
        self.assertEqual(outbox[3].to, [self.organisateur.email])
        self.assertEqual(outbox[3].subject, 'Accusé de réception (dépôt de dossier)')
        self.client.logout()

        print('**** test 2 instructeur-42 - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = f'/instructions/tableaudebord/{str(self.prefecture.pk)}/list/?filtre_etat=atraiter'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action_requise = str(html.select("div.border-danger.liste-actions"))
        self.assertIn('Envoyer une demande d\'avis', action_requise)
        self.assertContains(reponse, 'État des instructions dans tous les départements')
        self.assertContains(reponse, 'Instruction du département 42 - Loire')
        self.assertContains(reponse, 'Instruction du département 43 - Haute-Loire')
        self.assertContains(reponse, '<i class="demandee', count=3)
        self.assertContains(reponse, 'Pas d\'avis fédéral !')
        self.assertContains(reponse, 'Itinéraire horaire pour le département 42')
        # Distribuer les demandes d'avis de l'événement
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'mairie', 'array_id[]': self.mairie1.pk}, HTTP_HOST='127.0.0.1:8000')

        self.affichage_avis(self.instruction)
        # Vérifier le passage en encours et le nombre d'avis manquants
        self.presence_avis('instructeur', 'encours', log=False)
        # appel ajax pour avoir la liste
        url42_list = f'/instructions/tableaudebord/{str(self.prefecture.pk)}/list/?filtre_etat=encours'
        reponse = self.client.get(url42_list, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1&nbsp; avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        self.id_avis_mairie = Avis.objects.get(service_consulte_fk=self.mairie1).id
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 3)
        self.assertIn('Demande d\'avis à traiter', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_mairie)
        self.assertEqual(messages[0].object_id, self.id_avis_mairie)          # Avis Mairie
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertIn('débuté le traitement de votre dossier', messages[1].corps)
        self.assertEqual(1, messages[1].message_enveloppe.all().count())
        self.assertEqual(messages[1].message_enveloppe.first().destinataire_id, self.organisateur)
        self.assertEqual(len(outbox), 2)
        # self.assertEqual(outbox[0].to, [self.mairie1.email_s])
        self.assertEqual(outbox[0].to, [self.agent_mairie.email])
        self.assertEqual(outbox[0].subject, 'Demande d\'avis à traiter')
        # self.assertEqual(outbox[1].to, [self.structure.email_s])
        self.assertEqual(outbox[1].to, [self.organisateur.email])
        self.assertEqual(outbox[1].subject, 'Démarrage de l\'instruction')

        print('**** test 3 instructeur-43 - distribution ****')
        # Instruction de l'avis par la préfecture 43, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur43', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = f'/instructions/tableaudebord/{str(self.prefecture43.pk)}/list/?filtre_etat=atraiter'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action_requise = str(html.select("div.border-danger.liste-actions"))
        self.assertIn('Envoyer une demande d\'avis', action_requise)
        self.assertNotContains(reponse, 'Rendre l\'avis')
        self.assertContains(reponse, 'Ajouter un document officiel')
        self.assertNotContains(reponse, 'Aucun avis de préfécture rendu')
        self.assertContains(reponse, 'Pas d\'avis fédéral !')
        self.assertContains(reponse, 'Itinéraire horaire pour le département 43')
        instruction = Instruction.objects.get(manif=self.manif, instruction_principale__isnull=False)
        instruction_2_pk = int(self.instruction.pk) + 1
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': instruction.pk}),
                        data={'categorie': 'mairie', 'array_id[]': self.mairie43.pk}, HTTP_HOST='127.0.0.1:8000')


        self.assertContains(reponse, instruction)
        self.affichage_avis(instruction)
        # Vérifier le passage en encours et le nombre d'avis manquants
        self.presence_avis('instructeur', 'encours', log=False)
        # appel ajax pour avoir la liste
        url43_list = f'/instructions/tableaudebord/{str(self.prefecture43.pk)}/list/?filtre_etat=encours'
        reponse = self.client.get(url43_list, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1&nbsp; avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        self.id_avis_mairie43 = Avis.objects.get(service_consulte_fk=self.mairie43, instruction_id=instruction.pk).id
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 3)
        self.assertIn('Demande d\'avis à traiter', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_mairie43)
        self.assertEqual(messages[0].object_id, self.id_avis_mairie43)          # Avis Mairie
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertIn('débuté le traitement de votre dossier', messages[1].corps)
        self.assertEqual(1, messages[1].message_enveloppe.all().count())
        self.assertEqual(messages[1].message_enveloppe.first().destinataire_id, self.organisateur)
        self.assertEqual(len(outbox), 2)
        # self.assertEqual(outbox[0].to, [self.mairie1.email_s])
        self.assertEqual(outbox[0].to, [self.agent_mairie43.email])
        self.assertEqual(outbox[0].subject, 'Demande d\'avis à traiter')
        # self.assertEqual(outbox[1].to, [self.structure.email_s])
        self.assertEqual(outbox[1].to, [self.organisateur.email])
        self.assertEqual(outbox[1].subject, 'Démarrage de l\'instruction')

        print('**** test 4 avis mairie43 ****')
        # Instruction de l'avis par la mairie du 43, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_mairie43', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = '/instructions/tableaudebord/' + str(self.mairie43.pk) + '/list/?filtre_specifique=serviceconsulte&filtre_etat=atraiter'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.affichage_avis(instruction)
        # Vérifier le passage en rendu
        self.presence_avis('agent_mairie43', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur43', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url43_list, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 4)
        self.assertIn('Avis rendu à la préfecture', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.instructeur43)
        self.assertEqual(messages[0].object_id, self.id_avis_mairie43)          # Avis Mairie
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture43.email_s])
        self.assertEqual(outbox[0].to, [self.instructeur43.email])
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture')

        print('**** test 5 instructeur-43 - autorisation ****')
        # Instruction par la préfecture 43, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur43', 'encours')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse_encours = self.client.get(url43_list, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse_encours, 'avis manquants')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse_encours)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'i class="distribuee fa-lg ico_double')
        self.assertContains(reponse, 'i class="ok', count=2)
        self.assertContains(reponse, 'Valider les pièces jointes', count=1)
        self.assertContains(reponse, 'Ajouter un document officiel', count=1)
        # Valider l'itinéraire horaire du 43
        id_fichier = re.search('Itinéraire horaire.+span id="detail_(?P<id>(\d+)).+/itineraire_horaire_', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(id_fichier, 'group'))
        url_valid = f"/instructions/{instruction.pk}/valider/?pk={id_fichier.group('id')}&valider=1"
        reponse = self.client.post(url_valid, {}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'ok')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse_encours)
        self.assertContains(reponse, 'pj_valide', count=1)
        # Publier l'autorisation avec l'url fournie et tester la redirection
        publish_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Ajouter un document officiel', reponse.content.decode('utf-8'))
        if hasattr(publish_form, 'group'):
            with open('/tmp/recepisse_declaration.txt') as file1:
                self.client.post(publish_form.group('url'), {'nature': '3', 'fichier': file1}, follow=True, HTTP_HOST='127.0.0.1:8000')
        # appel ajax pour avoir la liste
        url_list = f'/instructions/tableaudebord/{str(self.prefecture43.pk)}/list/?filtre_etat=autorise'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction)
        # Vérifier le passage en info
        self.assertContains(reponse, 'table_termine', count=1)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 3)
        self.assertEqual(messages[0].corps, '<p>Une pièce jointe a été validée par l\'instructeur : Bob ROBERT</p>')
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.instructeur)
        self.assertEqual(messages[1].corps, '<p>Récépissé de déclaration publié pour la manifestation Manifestation_Dvtm</p>')
        self.assertEqual(3, messages[1].message_enveloppe.all().count())
        destinaires = [self.instructeur43, self.agent_mairie43, self.organisateur]
        for env in messages[1].message_enveloppe.all():
            self.assertIn(env.destinataire_id, destinaires)
        # self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.structure.email_s])
        # self.assertEqual(outbox[0].subject, 'Récépissé de déclaration publié')
        self.assertEqual(len(outbox), 3)
        mailist = [self.organisateur.email, self.instructeur43.email, self.agent_mairie43.email]
        for email in outbox:
            self.assertIn(email.to[0], mailist)
            self.assertEqual(email.subject, 'Récépissé de déclaration publié')

        print('**** test 6 avis mairie42 ****')
        # Instruction de l'avis par la mairie du 42, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_mairie', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = '/instructions/tableaudebord/' + str(self.mairie1.pk) + '/list/?filtre_specifique=serviceconsulte&filtre_etat=atraiter'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.affichage_avis(self.instruction)
        # Vérifier le passage en rendu
        self.presence_avis('agent_mairie', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url42_list, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 4)
        self.assertIn('Avis rendu à la préfecture', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.instructeur)
        self.assertEqual(messages[0].object_id, self.id_avis_mairie)          # Avis Mairie
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture.email_s])
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture')

        print('**** test 7 instructeur-42 - autorisation ****')
        # Instruction par la préfecture, vérification de la présence de l'événement en warning
        self.presence_avis('instructeur', 'encours')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse_encours = self.client.get(url42_list, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse_encours, 'avis manquants')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse_encours)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'i class="distribuee fa-lg ico_double')
        self.assertContains(reponse, 'i class="autorisee', count=1)
        self.assertContains(reponse, 'i class="ok', count=2)
        self.assertContains(reponse, 'Valider les pièces jointes', count=1)
        self.assertContains(reponse, 'Ajouter un document officiel', count=1)
        # Valider les PJs
        for file, fichier in liste_files[:-1]:
            nom_fichier = re.search(file + '.*\\n.*\\n.+<i class="ctx-help-manif_form_(?P<nom>(.+)) float-end', reponse.content.decode('utf-8'))
            self.assertTrue(hasattr(nom_fichier, 'group'), msg="pas de match pour " + file)
            id_fichier = re.search('_' + nom_fichier.group('nom') + '.+span id="detail_(?P<id>(\d+)).+/' + fichier, reponse.content.decode('utf-8'), re.DOTALL)
            self.assertTrue(hasattr(id_fichier, 'group'), msg="pas de match pour " + file)
            url_valid = f"/instructions/{self.instruction.pk}/valider/?pk={id_fichier.group('id')}&valider=1"
            retour = self.client.post(url_valid, {}, follow=True, HTTP_HOST='127.0.0.1:8000')
            self.assertContains(retour, 'ok')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse_encours)
        self.assertContains(reponse, 'i class="dossier-complet-verifie')
        # Publier l'autorisation avec l'url fournie et tester la redirection
        publish_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Ajouter un document officiel', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(publish_form, 'group'))
        with open('/tmp/recepisse_declaration.txt') as file1:
            self.client.post(publish_form.group('url'), {'nature': '3', 'fichier': file1}, follow=True, HTTP_HOST='127.0.0.1:8000')
        # appel ajax pour avoir la liste
        url_list = f'/instructions/tableaudebord/{str(self.prefecture.pk)}/list/?filtre_etat=autorise'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction)
        # Vérifier le passage en info
        self.assertContains(reponse, 'table_termine', count=1)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        # for out in outbox:
        #     print(out.to, end="")
        #     print("--" + out.subject)
        # for mess in messages:
        #     for env in mess.message_enveloppe.all():
        #         print("**" + env.destinataire_txt)
        #     print("---" + str(mess.pk) + mess.corps)
        self.assertEqual(len(messages), 6)
        self.assertEqual(messages[0].corps, '<p>Une pièce jointe a été validée par l\'instructeur : Bob ROBERT</p>')
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.instructeur43)
        self.assertEqual(messages[3].corps, '<p>Vos pièces jointes ont été validées par l\'instructeur</p>')
        self.assertEqual(1, messages[3].message_enveloppe.all().count())
        self.assertEqual(messages[3].message_enveloppe.first().destinataire_id, self.organisateur)
        self.assertEqual(messages[4].corps, '<p>Récépissé de déclaration publié pour la manifestation Manifestation_Dvtm</p>')
        self.assertEqual(3, messages[4].message_enveloppe.all().count())
        destinaires = [self.agent_mairie, self.organisateur, self.instructeur]
        for env in messages[4].message_enveloppe.all():
            self.assertIn(env.destinataire_id, destinaires)
        # self.assertEqual(len(outbox), 2)
        # self.assertEqual(outbox[0].to, [self.structure.email_s])
        self.assertEqual(len(outbox), 4)
        self.assertEqual(outbox[0].to, [self.organisateur.email])
        self.assertEqual(outbox[0].subject, 'Pièces jointes validées')
        # self.assertEqual(outbox[1].to, [self.structure.email_s])
        # self.assertEqual(outbox[1].subject, 'Récépissé de déclaration publié')
        mailist = [self.organisateur.email, self.agent_mairie.email, self.instructeur.email]
        for idx, email in enumerate(outbox):
            if idx > 0:
                self.assertIn(email.to[0], mailist)
                self.assertEqual(email.subject, 'Récépissé de déclaration publié')

        # print(reponse.content.decode('utf-8'))
