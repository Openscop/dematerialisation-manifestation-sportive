import re, time

from django.test import tag, override_settings

from post_office.models import Email
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

from .test_base_selenium import SeleniumCommunClass
from instructions.models import Avis
from messagerie.models import Message


class InstructionAjoutPjTests(SeleniumCommunClass):
    """
    Test d'ajout de PJ avec sélénium pour une Dcnm
        circuit_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    Test sur les services simples avec transfert des PJ en document officiel
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ Pièces Jointes Services Simples (Sel) =============')
        SeleniumCommunClass.init_setup(cls)
        cls.avis_nb = 2
        super().setUpClass()

    @tag('selenium')
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
    def test_Ajout_PJ(self):
        """
        Test d'ajout de PJ pour les différentes étapes du circuit EDSR pour une autorisationNM
        """

        print('**** test 1 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 2 distribution avis ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.distribuer_avis(['Mairie'])
        # Vérifier le passage en encours et le nombre d'avis manquants
        time.sleep(self.DELAY*2)
        self.selenium.execute_script("window.scroll(0, 1800)")
        time.sleep(self.DELAY*3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)

        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        time.sleep(self.DELAY * 2)
        self.deconnexion()

        print('**** test 3 agent fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.connexion('agent_fede')
        self.presence_avis('agent_fede', 'nouveau')
        self.assertIn('Délai : 28 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Test de la partie pièces jointes
        self.assertIn('Ajouter une pièce jointe', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        # Ajouter une pièce jointe
        self.selenium.find_element(By.CLASS_NAME, 'btn_doc').click()
        fichier = self.selenium.find_element(By.XPATH, '//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_1.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        # Tester la présence de la pièce jointe
        pj = self.selenium.find_element(By.PARTIAL_LINK_TEXT, '.txt')
        nom_pj = pj.get_attribute('text')
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_fede', 'rendu')
        self.deconnexion()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(messages), 2)
        self.assertIn('pj_1.txt ajoutée à un avis au sujet de la manifestation Manifestation_Test', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.instructeur)
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_consulte_fk=self.fede).id)          # Avis Fédération
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].to, [self.instructeur.email])

        print('**** test 4 instructeur transfert pj ****')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.vue_detail()
        # Avis de la fédération
        avis = self.selenium.find_element(By.ID, f"card_{self.fede.pk}")
        avis_heading = avis.find_element(By.XPATH, "div[starts-with(@id, 'heading')]")
        icones = avis_heading.find_elements(By.CSS_SELECTOR, 'i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            avis.find_element(By.CLASS_NAME, 'pj')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        # Déplier l'avis et transférer en document officiel
        avis.click()
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY*2)
        carte = self.selenium.find_element(By.ID, f"card_{self.fede.pk}")
        time.sleep(self.DELAY)
        # Tester le bouton de transfert
        self.assertIn('Transférer en document officiel', carte.text)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Transférer en document officiel').click()
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.selenium.find_element(By.XPATH, "//select[@name='nature']/option[text()='Arrêté de circulation']").click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        # Reprendre l'avis et tester plus de bouton de transfert
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, f"card_{self.fede.pk}").click()
        carte = self.selenium.find_element(By.ID, f"card_{self.fede.pk}")
        self.assertNotIn('Transférer en document officiel', carte.text)
        self.selenium.execute_script("window.scroll(0, 0)")
        self.assertIn('Documents officiels (1)', self.selenium.page_source)
        self.selenium.find_element(By.XPATH, "//button[contains(.,'Documents officiels (1)')]").click()
        time.sleep(self.DELAY*2)
        pan = self.selenium.find_element(By.XPATH, "//div[@id='officialdata']")
        self.assertIn('Arrêté de circulation', pan.text)
        self.assertIn('Déposé le', pan.text)
        self.assertIn(nom_pj, pan.text)
        self.deconnexion()

        print('**** test 5  agent mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en nouveau
        self.connexion('agent_mairie')
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Demande').click()
        time.sleep(self.DELAY * 3)
        self.presence_avis('agent_mairie', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Ajouter une pièce jointe', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY * 2)
        # Ajouter une pièce jointe
        self.selenium.find_element(By.CLASS_NAME, 'btn_doc').click()
        fichier = self.selenium.find_element(By.XPATH, '//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_2.txt')
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        # Tester la présence de la pièce jointe
        pj = self.selenium.find_element(By.PARTIAL_LINK_TEXT, '.txt')
        nom_pj = pj.get_attribute('text')
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Demande').click()
        time.sleep(self.DELAY * 3)
        self.presence_avis('agent_mairie', 'rendu')
        self.deconnexion()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(messages), 2)
        self.assertIn('pj_2.txt ajoutée à un avis au sujet de la manifestation Manifestation_Test', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.instructeur)
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_consulte_fk=self.mairie1).id)          # Avis Mairie
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].to, [self.instructeur.email])

        print('**** test 6 instructeur transfert pj ****')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        time.sleep(self.DELAY)
        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.vue_detail()
        # Avis de la mairie
        avis = self.selenium.find_element(By.ID, f"card_{self.mairie1.pk}")
        avis_heading = avis.find_element(By.XPATH, "div[starts-with(@id, 'heading')]")
        icones = avis_heading.find_elements(By.CSS_SELECTOR, 'i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            avis.find_element(By.CLASS_NAME, 'pj')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        # Déplier l'avis et transférer en document officiel
        avis.click()
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        carte = self.selenium.find_element(By.ID, f"card_{self.mairie1.pk}")
        # Tester le bouton de transfert
        self.assertIn('Transférer en document officiel', carte.text)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Transférer en document officiel').click()
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.selenium.find_element(By.XPATH, '//select[@name="nature"]/option[text()="Déclaration d\'annulation"]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()
        self.selenium.execute_script("window.scroll(0, 200)")
        time.sleep(self.DELAY)
        # Reprendre l'avis et tester plus de bouton de transfert
        self.selenium.find_element(By.ID, f"card_{self.mairie1.pk}").click()
        carte = self.selenium.find_element(By.ID, f"card_{self.mairie1.pk}")
        self.assertNotIn('Transférer en document officiel', carte.text)
        self.selenium.execute_script("window.scroll(0, 0)")
        self.assertIn('Documents officiels (2)', self.selenium.page_source)
        self.selenium.find_element(By.XPATH, "//button[contains(.,'Documents officiels (2)')]").click()
        time.sleep(self.DELAY)
        pan = self.selenium.find_element(By.XPATH, "//div[@id='officialdata']")
        self.assertIn('Déclaration d\'annulation', pan.text)
        self.assertIn('Déposé le', pan.text)
        self.assertIn(nom_pj, pan.text)
        # Vérifier le passage en annulation
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'annule')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.deconnexion()
