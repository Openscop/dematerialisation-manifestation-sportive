import re, time

from django.test import tag, override_settings
from django.conf import settings

from selenium.webdriver.common.by import By

from .test_base_selenium import SeleniumCommunClass


class InstructionMultiAvisTests(SeleniumCommunClass):
    """
    Test de la fonction multi avis/préavis.
        sur avis ddsp :
        distribution préavis, rendu du préavis, rendu de l'avis, nouvel avis initié par le ddsp
        distribution nouveau préavis, rendu du nouveau préavis, rendu de l'avis, vérification instructeur
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ Multi avis / préavis (Sel) =============')
        SeleniumCommunClass.init_setup(cls)
        cls.avis_nb = 2
        super().setUpClass()

    @tag('selenium')
    @override_settings(DEBUG=True)
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
    def test_MultiAvis(self):
        """
        Test du multi avis et préavis pendant l'instruction
        """
        assert settings.DEBUG

        print('**** test 1 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))",
                               reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        self.assertTrue(hasattr(envoi, 'group'))
        self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 2 distribution avis ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Changer l'assignation").click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Me l'assigner").click()
        time.sleep(self.DELAY * 2)
        self.distribuer_avis(['Police'])
        time.sleep(self.DELAY * 2)
        self.deconnexion()

        print('**** test 3 avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.distribuer_preavis('abr')
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('COMMISSARIAT', self.selenium.page_source)
        time.sleep(self.DELAY)
        self.deconnexion()

        print('**** test 4 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        self.connexion('agent_commiss')
        self.presence_avis('agent_commiss', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis("préavis")
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_commiss', 'rendu')
        self.deconnexion()

        print('**** test 5 deuxiéme avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ddsp')
        # Appel de la vue de détail et test présence manifestation
        self.presence_avis('agent_ddsp', 'encours')
        time.sleep(self.DELAY)
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Créer et rendre', self.selenium.page_source)
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Créer et rendre').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.NAME, "raison").send_keys("Nouvelles informations")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, '//input[@type="submit"]').click()
        time.sleep(self.DELAY*2)
        # Vérifier présence des deux avis
        self.assertIn('État de l\'avis', self.selenium.page_source)
        self.assertIn('Avis précédent', self.selenium.page_source)
        self.assertIn('Ré édition de l\'avis', self.selenium.page_source)
        self.distribuer_preavis('abr')
        self.deconnexion()

        print('**** test 6 deuxiéme preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        self.connexion('agent_commiss')
        time.sleep(self.DELAY)
        self.presence_avis('agent_commiss', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        time.sleep(self.DELAY)
        # Vérifier présence des deux préavis
        self.assertIn('État du préavis', self.selenium.page_source)
        self.assertIn('Préavis précédent', self.selenium.page_source)
        self.envoyer_avis("préavis")
        self.deconnexion()

        print('**** test 7 troisiéme avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ddsp')
        # Appel de la vue de détail et test présence manifestation
        self.presence_avis('agent_ddsp', 'encours')
        time.sleep(self.DELAY)
        self.vue_detail()
        self.envoyer_avis()
        self.deconnexion()

        print('**** test 8 instructeur ****')
        # Instruction de l'avis par la préfecture, vérification de la présence des deux avis
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        time.sleep(self.DELAY * 2)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        time.sleep(self.DELAY * 2)
        # Vérifier présence des deux avis
        self.selenium.find_elements(By.CLASS_NAME, 'card-header')[1].click()
        self.assertIn('Avis précédents', self.selenium.page_source)
        self.assertIn('Ré édition de l\'avis', self.selenium.page_source)
        self.deconnexion()
