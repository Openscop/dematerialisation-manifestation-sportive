import re, time

from django.test import tag, override_settings
from django.contrib.auth.hashers import make_password as make
from django.conf import settings

from selenium.webdriver.support.ui import WebDriverWait
from allauth.account.models import EmailAddress
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By

from .test_base_selenium import SeleniumCommunClass
from core.models import Instance
from core.factories import UserFactory
from administrative_division.factories import CommuneFactory, DepartementFactory, ArrondissementFactory
from structure.factories.service import PrefectureFactory

class InstructionAccesConsultTests(SeleniumCommunClass):
    """
    Test de la fonction d'ouverture d'accès en lecture
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print('============ Accès en lecture (Sel) =============')
        SeleniumCommunClass.init_setup(cls)
        cls.avis_nb = 2
        super().setUpClass()
        cls.dep43 = DepartementFactory.create(name='43',
                                              instance__name="instance de test",
                                              instance__instruction_mode=Instance.IM_CIRCUIT_B,
                                              instance__etat_s="ouvert")
        arrondissement43 = ArrondissementFactory.create(name='Yssingeaux', code='433', departement=cls.dep43)
        cls.prefecture43 = PrefectureFactory(arrondissement_m2m=arrondissement43,
                                            instance_fk=cls.dep43.instance)
        cls.commune43 = CommuneFactory(name='Yssingeaux', arrondissement=arrondissement43)

        cls.instructeur43 = UserFactory.create(username='instructeur43', password=make("123"),
                                               default_instance=cls.dep43.instance, email='inst43@test.fr')
        EmailAddress.objects.create(user=cls.instructeur43, email='inst43@test.fr', primary=True,
                                    verified=True)

        cls.instructeur43.optionuser.notification_mail = True
        cls.instructeur43.optionuser.action_mail = True
        cls.instructeur43.optionuser.save()
        cls.instructeur43.organisation_m2m.add(cls.prefecture43)

    @tag('selenium')
    @override_settings(DEBUG=True)
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
    def test_AccesConsult(self):
        """
        Test de l'ouverture en lecture
        """
        assert settings.DEBUG

        print('**** test 1 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))",
                               reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        self.assertTrue(hasattr(envoi, 'group'))
        self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 2 ouverture des accès ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Octroyer un accès en lecture', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 800)")
        # Ouvrir l'accès en lecture
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'formconsult_btn').click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.ID, 'simple').click()
        time.sleep(self.DELAY)
        self.chosen_select('select_instances_modal_chosen', 'instance de test (42)')
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % 'Organisateur').click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_elements(By.CLASS_NAME, 'name_service')[0].click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_elements(By.CLASS_NAME, 'btn-action-selecteur')[0].click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % 'Police').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.CLASS_NAME, 'name_service').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'btn-action-selecteur').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.CLASS_NAME, "show_sous_service").click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_elements(By.CLASS_NAME, "name_service")[1].click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.CLASS_NAME, 'btn-action-selecteur').click()
        self.selenium.find_element(By.ID, 'btn-close-modal-avis').click()
        time.sleep(self.DELAY * 8)
        self.assertIn('Accès en lecture', self.selenium.page_source)
        self.assertIn('Organisateur', self.selenium.page_source)
        self.assertIn('Commissariat', self.selenium.page_source)
        self.assertIn('DDSP', self.selenium.page_source)

        self.selenium.execute_script("window.scroll(0, 800)")
        # Ouvrir l'accès en lecture
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'formconsult_btn').click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.ID, 'complet').click()
        time.sleep(self.DELAY * 2)
        self.chosen_select('select_instances_modal_chosen', 'instance de test (43)')
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'name_service').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'btn-action-selecteur').click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.ID, 'btn-close-modal-avis').click()
        time.sleep(self.DELAY * 3)
        self.assertIn('Préfecture de Yssingeaux', self.selenium.page_source)
        self.deconnexion()

        print('**** test 3 instructeur 43 ****')
        self.connexion('instructeur43')
        self.assertIn('Lecture seule', self.selenium.page_source)
        self.presence_avis('instructeur43', 'acces')
        url = self.selenium.current_url
        self.selenium.find_element(By.XPATH, '//td[contains(text(), "Manifestation_Test")]').click()
        wait = WebDriverWait(self.selenium, 15)
        wait.until(lambda driver: self.selenium.current_url != url)
        time.sleep(self.DELAY * 3)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Accès en lecture seule', self.selenium.page_source)
        self.assertIn('Suivi de l\'instruction', self.selenium.page_source)
        self.deconnexion()

        print('**** test 4 ddsp ****')
        self.connexion('agent_ddsp')
        self.assertIn('Lecture seule', self.selenium.page_source)
        self.presence_avis('agent_ddsp', 'acces')
        url = self.selenium.current_url
        self.selenium.find_element(By.XPATH, '//td[contains(text(), "Manifestation_Test")]').click()
        wait = WebDriverWait(self.selenium, 15)
        wait.until(lambda driver: self.selenium.current_url != url)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Accès en lecture seule', self.selenium.page_source)
        self.assertNotIn('Suivi de l\'instruction', self.selenium.page_source)
        self.deconnexion()

        print('**** test 5 commissariat ****')
        self.connexion('agent_commiss')
        self.assertIn('Lecture seule', self.selenium.page_source)
        self.presence_avis('agent_commiss', 'acces')
        url = self.selenium.current_url
        self.selenium.find_element(By.XPATH, '//td[contains(text(), "Manifestation_Test")]').click()
        wait = WebDriverWait(self.selenium, 15)
        wait.until(lambda driver: self.selenium.current_url != url)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Accès en lecture seule', self.selenium.page_source)
        self.assertNotIn('Suivi de l\'instruction', self.selenium.page_source)
        self.deconnexion()

        print('**** test 6 révoquer des accès ****')
        self.connexion('instructeur')
        time.sleep(self.DELAY * 2)
        self.presence_avis('instructeur', 'demarre')
        self.vue_detail()
        time.sleep(self.DELAY * 2)
        div_drl_acces_btn = self.selenium.find_elements(By.CLASS_NAME, 'service')[0]
        drl_acces_btn = self.selenium.find_elements(By.CLASS_NAME, 'del_acces')[0]
        ActionChains(self.selenium).move_to_element(div_drl_acces_btn).perform()
        time.sleep(self.DELAY * 2)
        ActionChains(self.selenium).move_to_element(drl_acces_btn).perform()
        time.sleep(self.DELAY * 2)
        ActionChains(self.selenium).click(drl_acces_btn).perform()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 0)")
        time.sleep(self.DELAY * 2)
        self.deconnexion()

        print('**** test 7 vérifier  révocation ****')

        self.connexion('agent_ddsp')
        self.assertNotIn('Lecture seule', self.selenium.page_source)
        self.deconnexion()
