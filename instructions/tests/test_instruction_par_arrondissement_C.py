import re

from django.test import TestCase
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.hashers import make_password as make

from bs4 import BeautifulSoup as Bs

from core.models import Instance
from evenements.factories import DcnmFactory
from instructions.factories import InstructionFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from structure.factories.structure import StructureOrganisatriceFactory
from core.factories import UserFactory
from sports.factories import ActiviteFactory


class InstructionArrondissementC(TestCase):
    """
    Test du circuit d'instruction par arrondissement
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========= Instruction par arrondissement C (Clt) ==========')
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__instruction_mode=Instance.IM_CIRCUIT_C)
        arrond_1 = ArrondissementFactory.create(name='Montbrison', code='421', departement=dep)
        arrond_2 = ArrondissementFactory.create(name='Roanne', code='422', departement=dep,
                                                prefecture__type_service_fk__nom_s="Sous-Préfecture",
                                                prefecture__type_service_instructeur_s='sousprefecture')
        cls.instance = dep.instance
        cls.prefecture1 = arrond_1.organisations.first()
        cls.prefecture2 = arrond_2.organisations.first()
        cls.commune1 = CommuneFactory(name='Bard', arrondissement=arrond_1)
        cls.autrecommune1 = CommuneFactory(name='Roche', arrondissement=arrond_1)
        cls.commune2 = CommuneFactory(name='Bully', arrondissement=arrond_2)
        cls.autrecommune2 = CommuneFactory(name='Régny', arrondissement=arrond_2)
        structure1 = StructureOrganisatriceFactory.create(precision_nom_s="structure_test_1",
                                                          commune_m2m=cls.commune1, instance_fk=dep.instance)
        structure2 = StructureOrganisatriceFactory.create(precision_nom_s="structure_test_2",
                                                          commune_m2m=cls.commune2, instance_fk=dep.instance)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        cls.instructeur1 = UserFactory.create(username='instructeur1', password=make("123"),
                                              default_instance=dep.instance)
        cls.instructeur1.organisation_m2m.add(cls.prefecture1)
        cls.agent_mairie1 = UserFactory.create(username='agent_mairie1', password=make("123"),
                                               default_instance=dep.instance)
        cls.mairie1 = cls.commune1.organisation_set.first()
        cls.agent_mairie1.organisation_m2m.add(cls.mairie1)
        cls.instructeur2 = UserFactory.create(username='instructeur2', password=make("123"),
                                              default_instance=dep.instance)
        cls.instructeur2.organisation_m2m.add(cls.prefecture2)
        cls.agent_mairie2 = UserFactory.create(username='agent_mairie2', password=make("123"),
                                               default_instance=dep.instance)
        cls.mairie2 = cls.commune2.organisation_set.first()
        cls.agent_mairie2.organisation_m2m.add(cls.mairie2)
        cls.organisateur = UserFactory.create(username='organisateur', password=make("123"),
                                              default_instance=dep.instance, email='orga@test.fr')
        cls.organisateur.organisation_m2m.add(structure1)
        cls.organisateur.organisation_m2m.add(structure2)

        # Création des événements
        cls.manifestation1 = DcnmFactory.create(ville_depart=cls.commune1, structure_organisatrice_fk=structure1, activite=activ,
                                                nom='Manifestation_Test_1', villes_traversees=(cls.autrecommune1,))
        cls.manifestation2 = DcnmFactory.create(ville_depart=cls.commune2, structure_organisatrice_fk=structure2, activite=activ,
                                                nom='Manifestation_Test_2', villes_traversees=(cls.autrecommune2,))
        cls.manifestation3 = DcnmFactory.create(ville_depart=cls.commune1, structure_organisatrice_fk=structure1, activite=activ,
                                                nom='Manifestation_Test_3', villes_traversees=(cls.autrecommune2,))
        cls.manifestation4 = DcnmFactory.create(ville_depart=cls.commune2, structure_organisatrice_fk=structure2, activite=activ,
                                                nom='Manifestation_Test_4', villes_traversees=(cls.autrecommune1,))

    def test_Instruction_Arrond_C(self):
        print('**** test 1 creation manifs ****')
        self.instruction1 = InstructionFactory.create(manif=self.manifestation1)
        self.instruction2 = InstructionFactory.create(manif=self.manifestation2)
        self.instruction3 = InstructionFactory.create(manif=self.manifestation3)
        self.instruction4 = InstructionFactory.create(manif=self.manifestation4)

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation1.ville_depart, end=" ; ")
        print(self.manifestation1.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation1.structure_organisatrice_fk, end=" ; ")
        print(self.manifestation1.activite, end=" ; ")
        print(self.manifestation1.activite.discipline)
        self.assertEqual(str(self.instruction1.manif), str(self.manifestation1))

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation2.ville_depart, end=" ; ")
        print(self.manifestation2.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation2.structure_organisatrice_fk, end=" ; ")
        print(self.manifestation2.activite, end=" ; ")
        print(self.manifestation2.activite.discipline)
        self.assertEqual(str(self.instruction2.manif), str(self.manifestation2))

        print('**** test 2 instruction préfecture 1 ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur1', password='123'))
        # Appel de la page
        url = f'/instructions/tableaudebord/{str(self.prefecture1.pk)}/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Montbrison', count=4)
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('3', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        url_list = f'/instructions/tableaudebord/{str(self.prefecture1.pk)}/list/?filtre_etat=atraiter'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)
        self.assertContains(reponse, self.instruction3.manif)
        self.assertContains(reponse, self.instruction4.manif)

        print('**** test 3 instruction préfecture 2 ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur2', password='123'))
        # Appel de la page
        url = f'/instructions/tableaudebord/{str(self.prefecture2.pk)}/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Roanne', count=4)
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_demarre">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc5 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        self.assertTrue(hasattr(nb_bloc5, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb')) + int(nb_bloc5.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        url_list = f'/instructions/tableaudebord/{str(self.prefecture2.pk)}/list/?filtre_etat=atraiter'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, self.instruction1.manif)
        self.assertContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction3.manif)
        self.assertNotContains(reponse, self.instruction4.manif)

        print('**** test 4 instruction préfecture 1 et 2 en lecture ****')

        self.instance.acces_arrondissement = 1
        self.instance.save()

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur1', password='123'))
        # Appel de la page
        url = f'/instructions/tableaudebord/{str(self.prefecture1.pk)}/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Montbrison', count=5)
        classe_i = re.search('<label.+class="(?P<class>([^"]+)).+id="(?P<id>([^"]+)).+Montbrison', reponse.content.decode('utf-8'), re.DOTALL)
        if hasattr(classe_i, 'group'):
            self.assertEqual(classe_i.group('id'), 'arron_421')
            self.assertTrue('test_ecriture' in classe_i.group('class'))
        else:
            self.assertTrue(False, msg="pas d\'onglet Montbrison trouvé")
        self.assertContains(reponse, 'Roanne')
        classe_i = re.search('<label.+class="(?P<class>([^"]+)).+id="(?P<id>([^"]+)).+Roanne', reponse.content.decode('utf-8'), re.DOTALL)
        if hasattr(classe_i, 'group'):
            self.assertEqual(classe_i.group('id'), 'arron_422')
            self.assertTrue('test_lecture' in classe_i.group('class'))
        else:
            self.assertTrue(False, msg="pas d\'onglet Roanne trouvé")

        # appel ajax pour avoir la liste
        url_list = f'/instructions/tableaudebord/{str(self.prefecture1.pk)}/list/?filtre_etat=atraiter'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)
        self.assertContains(reponse, self.instruction3.manif)
        self.assertContains(reponse, self.instruction4.manif)
        # Appel de la vue de détail et test présence manifestation et action possible
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test_1')
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action_requise = str(html.select("div.border-danger.liste-actions"))
        self.assertIn('Envoyer une demande d\'avis', action_requise)

        # Changement d'onglet => Roanne
        url_list += "&arron=" + classe_i.group('id')[6:]
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, self.instruction1.manif)
        self.assertContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction3.manif)
        self.assertNotContains(reponse, self.instruction4.manif)

        # Appel de la vue de détail et test présence manifestation et lecture seule
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test_2')
        # Configuration supprimée, il n'y a plus de lecture seule
        # self.assertContains(reponse, 'La configuration de votre département ne permet')

        print('**** test 5 instruction préfecture 2 et 1 ****')

        self.instance.acces_arrondissement = 2
        self.instance.save()

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur2', password='123'))
        # Appel de la page
        url = f'/instructions/tableaudebord/{str(self.prefecture2.pk)}/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Roanne', count=5)
        classe_i = re.search('<label.+class="(?P<class>([^"]+)).+id="(?P<id>([^"]+)).+Roanne', reponse.content.decode('utf-8'), re.DOTALL)
        if hasattr(classe_i, 'group'):
            self.assertEqual(classe_i.group('id'), 'arron_422')
            self.assertTrue('test_ecriture' in classe_i.group('class'))
        else:
            self.assertTrue(False, msg="pas d\'onglet Roanne trouvé")
        self.assertContains(reponse, 'Montbrison')
        classe_i = re.search('<label.+class="(?P<class>([^"]+)).+id="(?P<id>([^"]+)).+Montbrison', reponse.content.decode('utf-8'), re.DOTALL)
        if hasattr(classe_i, 'group'):
            self.assertEqual(classe_i.group('id'), 'arron_421')
            self.assertTrue('test_ecriture' in classe_i.group('class'))
        else:
            self.assertTrue(False, msg="pas d\'onglet Montbrison trouvé")

        # appel ajax pour avoir la liste
        url_list = f'/instructions/tableaudebord/{str(self.prefecture2.pk)}/list/?filtre_etat=atraiter'
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction1.manif)
        # Appel de la vue de détail et test présence manifestation et action possible
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, self.instruction1.manif)
        self.assertContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction3.manif)
        self.assertNotContains(reponse, self.instruction4.manif)

        # Changement d'onglet => Montbrison
        url_list += "&arron=" + classe_i.group('id')[6:]
        reponse = self.client.get(url_list, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)
        self.assertContains(reponse, self.instruction3.manif)
        self.assertContains(reponse, self.instruction4.manif)

        # Appel de la vue de détail et test présence manifestation et lecture seule
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test_1')
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action_requise = str(html.select("div.border-danger.liste-actions"))
        self.assertIn('Envoyer une demande d\'avis', action_requise)

        # print(reponse.content.decode('utf-8'))
