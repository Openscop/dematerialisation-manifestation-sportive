import re

from django.test import override_settings
from django.shortcuts import reverse
from post_office.models import Email
from bs4 import BeautifulSoup as Bs

from .test_base import TestCommunClass
from instructions.models import Instruction, Avis
from messagerie.models import Message


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class Circuit_EDSRTests(TestCommunClass):
    """
    Test du circuit d'instance EDSR pour une Dnm
        circuit_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
        envoi des messages et des emails testé
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('============ WF_EDSR (Clt) =============')
        TestCommunClass.init_setup(cls)
        cls.agentlocal_edsr.delete()
        cls.avis_nb = 6

    def test_Circuit_EDSR(self):
        """
        Test des différentes étapes du circuit EDSR pour une autorisationNM
        """
        """
        def print(string="", end=None):
            # Supprimer les prints hors debug
            pass
        """
        print('**** test 1 creation manif ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('1', nb_bloc.group('nb'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        self.assertTrue(hasattr(declar, 'group'))
        self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        self.instruction = Instruction.objects.get(manif=self.manifestation)
        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().get_instance())
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="GGD"), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="EDSR"), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().organisation_set.get(precision_nom_s="DDSP"))
        print(self.manifestation.structure_organisatrice_fk, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline, end=" ; ")
        print(self.manifestation.activite.discipline.get_federations().first())
        self.affichage_avis()
        self.assertEqual(str(self.instruction), str(self.manifestation))
        self.id_avis_fede = Avis.objects.last().id
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        index = len(outbox)
        index_mess = len(messages)
        self.assertEqual(len(messages), 5)
        self.assertIn('créer un nouveau dossier', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.organisateur)
        self.assertIn('Demande d\'avis à traiter', messages[2].corps)
        self.assertEqual(1, messages[2].message_enveloppe.all().count())
        self.assertEqual(messages[2].message_enveloppe.first().destinataire_id, self.agent_fede)
        self.assertEqual(messages[2].object_id, self.id_avis_fede)          # Avis fédération
        self.assertEqual(messages[2].message_enveloppe.first().doc_objet, "avis")
        self.assertIn('nouveau dossier a été envoyé', messages[3].corps)
        self.assertEqual(1, messages[3].message_enveloppe.all().count())
        self.assertEqual(messages[3].message_enveloppe.first().destinataire_id, self.instructeur)
        self.assertIn('accusé de réception', messages[4].corps)
        self.assertEqual(1, messages[4].message_enveloppe.all().count())
        self.assertEqual(messages[4].message_enveloppe.first().destinataire_id, self.organisateur)
        self.assertEqual(len(outbox), 4)
        # self.assertEqual(outbox[0].to, [self.fede.email_s])
        self.assertEqual(outbox[0].to, [self.agent_fede.email])
        self.assertEqual(outbox[0].subject, 'Demande d\'avis à traiter')
        # self.assertEqual(outbox[1].to, [self.prefecture.email_s])
        self.assertEqual(outbox[1].to, [self.instructeur.email])
        self.assertEqual(outbox[1].subject, 'Nouveau dossier à instruire')
        self.assertEqual(outbox[2].to, [self.organisateur.email])
        self.assertEqual(outbox[2].subject, 'Accusé de réception : Manifestation_Test')
        # self.assertEqual(outbox[3].to, [self.structure.email_s])
        self.assertEqual(outbox[3].to, [self.organisateur.email])
        self.assertEqual(outbox[3].subject, 'Accusé de réception (dépôt de dossier)')
        self.client.logout()

        print('**** test 2 vérification avis; 0 pour tous sauf la fédération ****')
        # Vérification des avis des divers agents
        # GGD
        self.presence_avis('agent_ggd', 'none')
        self.client.logout()
        # EDSR
        self.presence_avis('agent_edsr', 'none')
        self.client.logout()
        # Mairie
        self.presence_avis('agent_mairie', 'none')
        self.client.logout()
        # CG
        self.presence_avis('agent_cg', 'none')
        self.client.logout()
        # CGSuperieur
        self.presence_avis('agent_cgsup', 'none')
        self.client.logout()
        # SDIS
        self.presence_avis('agent_sdis', 'none')
        self.client.logout()

        print('**** test 3 instructeur - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.presence_avis('instructeur', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_intr_list = "/instructions/tableaudebord/" + str(self.prefecture.pk) + "/list/"
        reponse = self.client.get(url_intr_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        html = Bs(reponse.content.decode('utf-8'), 'html.parser')
        action_requise = str(html.select("div.border-danger.liste-actions"))
        self.assertIn('Envoyer une demande d\'avis', action_requise)
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'mairie', 'array_id[]': self.mairie1.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'conseil_departemental', 'array_id[]': self.cg.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'police', 'array_id[]': self.ddsp.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'gendarmerie', 'array_id[]': self.edsr.pk}, HTTP_HOST='127.0.0.1:8000')
        self.client.get(reverse('instructions:ajouter_avis_ajax', kwargs={'pk': self.instruction.pk}),
                        data={'categorie': 'pompier', 'array_id[]': self.sdis.pk}, HTTP_HOST='127.0.0.1:8000')


        self.assertContains(reponse, self.instruction)
        self.affichage_avis()
        # Vérifier le passage en encours et le nombre d'avis manquants
        self.presence_avis('instructeur', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content.decode('utf-8'))
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        self.id_avis_edsr = Avis.objects.filter(service_consulte_fk=self.edsr).last().id
        self.id_avis_ddsp = Avis.objects.filter(service_consulte_fk=self.ddsp).last().id
        self.id_avis_cg = Avis.objects.filter(service_consulte_fk=self.cg).last().id
        self.id_avis_sdis = Avis.objects.filter(service_consulte_fk=self.sdis).last().id
        self.id_avis_mairie = Avis.objects.filter(service_consulte_fk=self.mairie1).last().id

        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 7)
        self.assertIn('Demande d\'avis à traiter', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_mairie)
        self.assertEqual(messages[0].object_id, self.id_avis_mairie)          # Avis Mairie
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertIn('débuté le traitement de votre dossier', messages[1].corps)
        self.assertEqual(1, messages[1].message_enveloppe.all().count())
        self.assertEqual(messages[1].message_enveloppe.first().destinataire_id, self.organisateur)
        self.assertIn('Demande d\'avis à traiter', messages[2].corps)
        self.assertEqual(1, messages[2].message_enveloppe.all().count())
        self.assertEqual(messages[2].message_enveloppe.first().destinataire_id, self.agent_cg)
        self.assertEqual(messages[2].object_id, self.id_avis_cg)          # Avis CG
        self.assertEqual(messages[2].message_enveloppe.first().doc_objet, "avis")
        self.assertIn('Demande d\'avis à traiter', messages[3].corps)
        self.assertEqual(1, messages[3].message_enveloppe.all().count())
        self.assertEqual(messages[3].message_enveloppe.first().destinataire_id, self.agent_ddsp)
        self.assertEqual(messages[3].object_id, self.id_avis_ddsp)          # Avis DDSP
        self.assertEqual(messages[3].message_enveloppe.first().doc_objet, "avis")
        self.assertIn('Demande d\'avis à traiter', messages[4].corps)
        self.assertEqual(1, messages[4].message_enveloppe.all().count())
        self.assertEqual(messages[4].message_enveloppe.first().destinataire_id, self.agent_edsr)
        self.assertEqual(messages[4].object_id, self.id_avis_edsr)          # Avis EDSR
        self.assertEqual(messages[4].message_enveloppe.first().doc_objet, "avis")
        self.assertIn('Demande d\'avis à traiter', messages[5].corps)
        self.assertEqual(1, messages[5].message_enveloppe.all().count())
        self.assertEqual(messages[5].message_enveloppe.first().destinataire_id, self.agent_sdis)
        self.assertEqual(messages[5].object_id, self.id_avis_sdis)          # Avis SDIS
        self.assertEqual(messages[5].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 6)
        # mailist = [self.edsr.email_s, self.sdis.email_s, self.ddsp.email_s, self.cg.email_s,
        #            self.mairie1.email_s, self.structure.email_s]
        mailist = [self.agent_edsr.email, self.agent_sdis.email, self.agent_ddsp.email, self.agent_cg.email,
                   self.agent_mairie.email, self.structure.users.first().email]
        for email in outbox:
            self.assertIn(email.to[0], mailist)
        self.assertEqual(outbox[0].subject, 'Demande d\'avis à traiter')
        self.assertEqual(outbox[1].subject, 'Démarrage de l\'instruction')

        print('**** test 4 avis fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_fede', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.fede.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 28 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # Vérifier le passage en rendu
        self.presence_avis('agent_fede', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 4)
        self.assertIn('Avis rendu à la préfecture', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.instructeur)
        self.assertEqual(messages[0].object_id, self.id_avis_fede)          # Avis fédération
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture.email_s])
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture')

        print('**** test 5 vérification avis; 0 pour GGD, CGD, USG et Brigade ****')
        # Vérification des avis des divers agents
        # EDSR
        self.presence_avis('agent_ggd', 'none')
        self.client.logout()
        # CGD
        self.presence_avis('agent_cgd1', 'none')
        self.client.logout()
        # USG
        self.presence_avis('agent_usg', 'none')
        self.client.logout()
        # BRG
        self.presence_avis('agent_brg1', 'none')
        self.client.logout()

        print('**** test 6 avis edsr - distribution ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_edsr', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.edsr.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, [self.cgd1.pk, self.cgd2.pk, self.usg.pk])
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        preavis = Avis.objects.filter(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.edsr))
        for pr in preavis:
            print(pr, end=" ; ")
            print(pr.etat)
        self.affichage_avis()
        # Vérification du passage en encours
        self.presence_avis('agent_edsr', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '3 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 4)
        self.assertIn('Demande de préavis à traiter', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_cgd1)
        self.assertIn(messages[0].object_id, preavis.values_list('id', flat=True))          # Préavis Cgd
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertIn('Demande de préavis à traiter', messages[1].corps)
        self.assertEqual(1, messages[1].message_enveloppe.all().count())
        self.assertEqual(messages[1].message_enveloppe.first().destinataire_id, self.agent_cgd2)
        self.assertIn(messages[1].object_id, preavis.values_list('id', flat=True))          # Préavis Cgd
        self.assertEqual(messages[1].message_enveloppe.first().doc_objet, "avis")
        self.assertIn('Demande de préavis à traiter', messages[2].corps)
        self.assertEqual(1, messages[2].message_enveloppe.all().count())
        self.assertEqual(messages[2].message_enveloppe.first().destinataire_id, self.agent_usg)
        self.assertIn(messages[2].object_id, preavis.values_list('id', flat=True))          # Préavis Cgd
        self.assertEqual(messages[2].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 3)
        # self.assertIn(outbox[0].to[0], [self.cgd2.email_s, self.cgd1.email_s, self.usg.email_s])
        # self.assertIn(outbox[1].to[0], [self.cgd2.email_s, self.cgd1.email_s, self.usg.email_s])
        # self.assertIn(outbox[2].to[0], [self.cgd2.email_s, self.cgd1.email_s, self.usg.email_s])
        self.assertIn(outbox[0].to[0], [self.agent_cgd2.email, self.agent_cgd1.email, self.agent_usg.email])
        self.assertIn(outbox[1].to[0], [self.agent_cgd2.email, self.agent_cgd1.email, self.agent_usg.email])
        self.assertIn(outbox[2].to[0], [self.agent_cgd2.email, self.agent_cgd1.email, self.agent_usg.email])
        self.assertEqual(outbox[0].subject, 'Demande de préavis à traiter')

        print('**** test 7 preavis cgd1 ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_cgd1', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cgd1.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.filter(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.edsr))
        for pr in preavis:
            print(pr, end=" ; ")
            print(pr.etat)
        self.affichage_avis()
        # Vérification du passage en rendu
        self.presence_avis('agent_cgd1', 'rendu', log=False)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 2)
        self.assertIn('Préavis rendu (2 en attente)', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_edsr)
        self.assertIn(messages[0].object_id, preavis.values_list('id', flat=True))          # Préavis Cgd
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.edsr.email_s])
        self.assertEqual(outbox[0].to, [self.agent_edsr.email])
        self.assertEqual(outbox[0].subject, 'Préavis rendu (2 en attente)')

        print('**** test 8 preavis cgd2 ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_cgd2', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cgd2.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.filter(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.edsr))
        for pr in preavis:
            print(pr, end=" ; ")
            print(pr.etat)
        self.affichage_avis()
        # Vérification du passage en rendu
        self.presence_avis('agent_cgd2', 'rendu', log=False)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 2)
        self.assertIn('Préavis rendu (1 en attente)', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_edsr)
        self.assertIn(messages[0].object_id, preavis.values_list('id', flat=True))          # Préavis Cgd
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.edsr.email_s])
        self.assertEqual(outbox[0].to, [self.agent_edsr.email])
        self.assertEqual(outbox[0].subject, 'Préavis rendu (1 en attente)')
        # deux connexions avant notification => message #4

        print('**** test 9 preavis usg ****')
        # Instruction du préavis par le usg, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_usg', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.usg.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.filter(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.edsr))
        for pr in preavis:
            print(pr, end=" ; ")
            print(pr.etat)
        self.affichage_avis()
        # Vérification du passage en rendu
        self.presence_avis('agent_usg', 'rendu', log=False)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 2)
        self.assertIn('Tous les préavis sont rendus', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_edsr)
        self.assertIn(messages[0].object_id, preavis.values_list('id', flat=True))          # Préavis Cgd
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.edsr.email_s])
        self.assertEqual(outbox[0].to, [self.agent_edsr.email])
        self.assertEqual(outbox[0].subject, 'Tous les préavis sont rendus')

        print('**** test 10 avis edsr - soumission ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en encours
        self.presence_avis('agent_edsr', 'encours')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.edsr.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.adresser_avis(reponse, self.ggd.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.affichage_avis()
        # vérification de la présence de l'événement en rendu
        self.presence_avis('agent_edsr', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 2)
        self.assertIn('Avis à valider et à envoyer', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_ggd)
        self.assertEqual(messages[0].object_id, self.id_avis_edsr)          # Avis Edsr
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.ggd.email_s])
        self.assertEqual(outbox[0].to, [self.agent_ggd.email])
        self.assertEqual(outbox[0].subject, 'Avis à valider et à envoyer')

        print('**** test 11 avis ggd ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_ggd', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.ggd.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # vérification de la présence de l'événement en rendu
        self.presence_avis('agent_ggd', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 4)
        self.assertIn('Avis rendu à la préfecture', messages[0].corps)
        self.assertEqual(5, messages[0].message_enveloppe.all().count())
        destinaires = [self.instructeur, self.agent_edsr, self.agent_cgd1, self.agent_cgd2, self.agent_usg]
        for env in messages[0].message_enveloppe.all():
            self.assertIn(env.destinataire_id, destinaires)
        self.assertEqual(messages[0].object_id, self.id_avis_edsr)          # Avis Edsr
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        # self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture.email_s])
        self.assertEqual(len(outbox), 5)
        mailist = [self.instructeur.email, self.agent_edsr.email, self.agent_cgd1.email, self.agent_cgd2.email, self.agent_usg.email]
        for email in outbox:
            self.assertIn(email.to[0], mailist)
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture')

        print('**** test 12 vérification avis; 0 pour Commissariat ****')
        # Vérification des avis des divers agents
        # CGD
        self.presence_avis('agent_commiss', 'none')
        self.client.logout()

        print('**** test 13 avis ddsp - distribution ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_ddsp', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.ddsp.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, self.commiss.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.ddsp))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérifier le passage en encours
        self.presence_avis('agent_ddsp', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 2)
        self.assertIn('Demande de préavis à traiter', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_commiss)
        self.assertEqual(messages[0].object_id, preavis.id)          # Préavis Commissariat
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.commiss.email_s])
        self.assertEqual(outbox[0].to, [self.agent_commiss.email])
        self.assertEqual(outbox[0].subject, 'Demande de préavis à traiter')

        print('**** test 14 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_commiss', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.commiss.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.ddsp))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérification du passage en rendu
        self.presence_avis('agent_commiss', 'rendu',log=False)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 2)
        self.assertIn('Tous les préavis sont rendus', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_ddsp)
        self.assertEqual(messages[0].object_id, preavis.id)          # Préavis Commissariat
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.ddsp.email_s])
        self.assertEqual(outbox[0].to, [self.agent_ddsp.email])
        self.assertEqual(outbox[0].subject, 'Tous les préavis sont rendus')

        print('**** test 15 avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en encours
        self.presence_avis('agent_ddsp', 'encours')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.ddsp.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # Vérifier le passage en rendu
        self.presence_avis('agent_ddsp', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 4)
        self.assertIn('Avis rendu à la préfecture', messages[0].corps)
        self.assertEqual(2, messages[0].message_enveloppe.all().count())
        destinaires = [self.instructeur, self.agent_commiss]
        for env in messages[0].message_enveloppe.all():
            self.assertIn(env.destinataire_id, destinaires)
        self.assertEqual(messages[0].object_id, self.id_avis_ddsp)          # Avis Edsr
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        # self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture.email_s])
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[1].to, [self.agent_commiss.email])
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture')

        print('**** test 16 avis mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_mairie', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.mairie1.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_specifique=serviceconsulte&filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # Vérifier le passage en rendu
        self.presence_avis('agent_mairie', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 4)
        self.assertIn('Avis rendu à la préfecture', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.instructeur)
        self.assertEqual(messages[0].object_id, self.id_avis_mairie)          # Avis Mairie
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture.email_s])
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture')

        print('**** test 17 vérification avis; 0 pour CGService ****')
        # Vérification des avis des divers agents
        # CGService
        self.presence_avis('agent_cgserv', 'none')
        self.client.logout()

        print('**** test 18 avis cg - distribution ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_cg', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cg.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, self.cgserv.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.cg))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérifier le passage en encours
        self.presence_avis('agent_cg', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 2)
        self.assertIn('Demande de préavis à traiter', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_cgserv)
        self.assertEqual(messages[0].object_id, preavis.id)          # Préavis CG
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.cgserv.email_s])
        self.assertEqual(outbox[0].to, [self.agent_cgserv.email])
        self.assertEqual(outbox[0].subject, 'Demande de préavis à traiter')

        print('**** test 19 preavis cgservice ****')
        # Instruction du préavis par le cgservice, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_cgserv', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cgserv.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.cg))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérification du passage en rendu
        self.presence_avis('agent_cgserv', 'rendu', log=False)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 2)
        self.assertIn('Tous les préavis sont rendus', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_cg)
        self.assertEqual(messages[0].object_id, preavis.id)          # Préavis CG
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.cg.email_s])
        self.assertEqual(outbox[0].to, [self.agent_cg.email])
        self.assertEqual(outbox[0].subject, 'Tous les préavis sont rendus')

        print('**** test 20 vérification avis; 0 pour CGSupérieur ****')
        # Vérification des avis des divers agents
        # CGSuperieur
        self.presence_avis('agent_cgsup', 'none')
        self.client.logout()

        print('**** test 21 avis cg - soumission ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en encours
        self.presence_avis('agent_cg', 'encours')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cg.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.adresser_avis(reponse, self.cgsup.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.affichage_avis()
        # vérification de la présence de l'événement en rendu
        self.presence_avis('agent_cg', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 2)
        self.assertIn('Avis à valider et à envoyer', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_cgsup)
        self.assertEqual(messages[0].object_id, self.id_avis_cg)          # Avis CG
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.cgsup.email_s])
        self.assertEqual(outbox[0].to, [self.agent_cgsup.email])
        self.assertEqual(outbox[0].subject, 'Avis à valider et à envoyer')

        print('**** test 22 agent cgsup ****')
        # Instruction de l'avis par le cgsup, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_cgsup', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.cgsup.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # Vérifier le passage en rendu
        self.presence_avis('agent_cgsup', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 4)
        self.assertIn('Avis rendu à la préfecture', messages[0].corps)
        self.assertEqual(3, messages[0].message_enveloppe.all().count())
        destinaires = [self.instructeur, self.agent_cg, self.agent_cgserv]
        for env in messages[0].message_enveloppe.all():
            self.assertIn(env.destinataire_id, destinaires)
        self.assertEqual(messages[0].object_id, self.id_avis_cg)          # Avis CG
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        # self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture.email_s])
        self.assertEqual(len(outbox), 3)
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[1].to, [self.agent_cg.email])
        self.assertEqual(outbox[2].to, [self.agent_cgserv.email])
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture')

        print('**** test 23 vérification avis; 0 pour Groupement, codis et cis ****')
        # Vérification des avis des divers agents
        # CIS
        self.presence_avis('agent_cis', 'none')
        self.client.logout()
        # CODIS
        self.presence_avis('agent_codis', 'none')
        self.client.logout()
        # SDIS groupement
        self.presence_avis('agent_group', 'none')
        self.client.logout()

        print('**** test 24 avis sdis - distribution ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_sdis', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.sdis.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.distribuer_avis(reponse, self.group.pk)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.sdis))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérification du passage en encours
        self.presence_avis('agent_sdis', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 2)
        self.assertIn('Demande de préavis à traiter', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_group)
        self.assertEqual(messages[0].object_id, preavis.id)          # Préavis Groupement
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.group.email_s])
        self.assertEqual(outbox[0].to, [self.agent_group.email])
        self.assertEqual(outbox[0].subject, 'Demande de préavis à traiter')

        print('**** test 25 preavis groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en nouveau
        self.presence_avis('agent_group', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.group.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse, "préavis")
        reponse = self.rendre_avis(reponse, "préavis")
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        preavis = Avis.objects.get(avis_parent_fk=self.instruction.avis.get(service_consulte_fk=self.sdis))
        print(preavis, end=" ; ")
        print(preavis.etat)
        self.affichage_avis()
        # Vérification du passage en rendu
        self.presence_avis('agent_group', 'rendu', log=False)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 2)
        self.assertIn('Tous les préavis sont rendus', messages[0].corps)
        self.assertEqual(1, messages[0].message_enveloppe.all().count())
        self.assertEqual(messages[0].message_enveloppe.first().destinataire_id, self.agent_sdis)
        self.assertEqual(messages[0].object_id, preavis.id)          # Préavis Groupement
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.sdis.email_s])
        self.assertEqual(outbox[0].to, [self.agent_sdis.email])
        self.assertEqual(outbox[0].subject, 'Tous les préavis sont rendus')

        print('**** test 26 avis sdis ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en encours
        self.presence_avis('agent_sdis', 'encours')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        url_list = "/instructions/tableaudebord/" + str(self.sdis.pk) + "/list/"
        reponse = self.client.get(url_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        reponse = self.rediger_avis(reponse)
        reponse = self.rendre_avis(reponse)
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        self.aucune_action(reponse)
        self.avis_nb -= 1
        self.affichage_avis()
        # Vérifier le passage en rendu
        self.presence_avis('agent_sdis', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(messages), 4)
        self.assertIn('Avis rendu à la préfecture', messages[0].corps)
        self.assertEqual(2, messages[0].message_enveloppe.all().count())
        destinaires = [self.instructeur, self.agent_group]
        for env in messages[0].message_enveloppe.all():
            self.assertIn(env.destinataire_id, destinaires)
        self.assertEqual(messages[0].object_id, self.id_avis_sdis)          # Avis Sdis
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        # self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.prefecture.email_s])
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[1].to, [self.agent_group.email])
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture')

        print('**** test 27 instructeur - autorisation ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en warning
        self.presence_avis('instructeur', 'encours')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'avis manquants')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Ajouter un document officiel', count=1)
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        publish_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Ajouter un document officiel', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(publish_form, 'group'))
        with open('/tmp/recepisse_declaration.txt') as file1:
            self.client.post(publish_form.group('url'), {'nature': '3', 'fichier': file1}, follow=True, HTTP_HOST='127.0.0.1:8000')
        # appel ajax pour avoir la liste
        reponse = self.client.get(url_intr_list + '?filtre_etat=autorise', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction)
        # Vérifier le passage en info et le nombre d'avis manquants
        self.assertContains(reponse, 'table_termine', count=1)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        # for out in outbox:
        #     print(out.to, end="")
        #     print("--" + out.subject)
        # for mess in messages:
        #     for env in mess.message_enveloppe.all():
        #         print("**" + env.destinataire_txt)
        #     print("---" + str(mess.pk) + mess.corps)
        self.assertEqual(len(messages), 2)
        self.assertEqual(messages[0].corps, '<p>Récépissé de déclaration publié pour la manifestation Manifestation_Test</p>')
        self.assertEqual(16, messages[0].message_enveloppe.all().count())
        destinaires = [self.organisateur, self.instructeur, self.agent_fede, self.agent_mairie,
                       self.agent_cg, self.agent_cgsup, self.agent_ddsp, self.agent_edsr,
                       self.agent_ggd, self.agent_sdis, self.agent_cgd1, self.agent_cgd2,
                       self.agent_usg, self.agent_commiss, self.agent_cgserv, self.agent_group]
        for env in messages[0].message_enveloppe.all():
            self.assertIn(env.destinataire_id, destinaires)
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "recepisse")
        # self.assertEqual(len(outbox), 1)
        # self.assertEqual(outbox[0].to, [self.structure.email_s])
        self.assertEqual(len(outbox), 16)
        mailist = [self.agent_edsr.email, self.agent_ggd.email, self.agent_cgd1.email,
                   self.agent_cgd2.email, self.agent_usg.email,
                   self.agent_ddsp.email, self.agent_commiss.email, self.agent_fede.email,
                   self.agent_cg.email, self.agent_cgserv.email, self.agent_cgsup.email,
                   self.agent_sdis.email, self.agent_codis.email, self.agent_group.email,
                   self.organisateur.email, self.agent_mairie.email, self.instructeur.email]
        for email in outbox:
            self.assertIn(email.to[0], mailist)
        self.assertEqual(outbox[0].subject, 'Récépissé de déclaration publié')
