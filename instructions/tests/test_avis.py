# coding: utf-8
import re
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.hashers import make_password as make
from django.test import TestCase
from django.contrib.contenttypes.models import ContentType
from datetime import timedelta
from structure.factories.structure import StructureOrganisatriceFactory
from ..factories import *
from structure.factories.service import FederationFactory
from sports.factories import ActiviteFactory
from core.factories import UserFactory
from administrative_division.factories import CommuneFactory, ArrondissementFactory


class AvisTests(TestCase):
    """ Tests de la base des avis """

    def setUp(self):
        """ Configuration """
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        self.instruction = InstructionFactory.create()
        self.instance = self.instruction.instance
        dep = self.instance.departement
        activ = ActiviteFactory.create()
        self.fede = FederationFactory.create(instance_fk=self.instance, departement_m2m=dep, discipline_fk=activ.discipline)
        self.avis = Avis.objects.create(service_consulte_fk=self.fede, service_consulte_origine_fk=self.fede, instruction=self.instruction)
        self.organisateur = UserFactory.create(username='organisateur', password=make("123"))
        self.arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        self.commune = CommuneFactory(name='Bard', arrondissement=self.arrondissement)
        self.structure = StructureOrganisatriceFactory.create(precision_nom_s="structure_test",
                                                              commune_m2m=self.commune, instance_fk=dep.instance)

    def test_dateLimite_avis_preavis(self):
        """ Tester la date limite pour l'avis et le preavis lors de la création """
        # Cas du préavis
        if self.avis.avis_parent_fk:
            expected_deadline = timezone.now() + timezone.timedelta(self.fede.delai_jour_preavis_int)
        # Cas de l'avis
        else:
            expected_deadline = timezone.now() + timezone.timedelta(self.fede.delai_jour_avis_int)
        self.assertEqual(self.avis.date_limite_reponse_dt - timezone.timedelta(seconds=self.avis.date_limite_reponse_dt.second) - timezone.timedelta(microseconds=self.avis.date_limite_reponse_dt.microsecond),
                         expected_deadline - timezone.timedelta(seconds=expected_deadline.second) - timezone.timedelta(microseconds=expected_deadline.microsecond))
        self.assertEqual(self.avis.delai_expire(), False)
        self.avis.service_consulte_fk = self.fede

    def test_validite_date_limite_avis(self):
        """ AR - Tester si la date limite renseignée pour un avis est acceptable """
        """ NOK si < aujourd\'hui et > 5 jours avant début manif """
        if self.avis.date_limite_reponse_dt < timezone.now() or (self.avis.date_limite_reponse_dt > (self.avis.instruction.manif.date_debut - timedelta(days=5))):
            print('Date limite réponse avis incorrecte')
        else:
            print('Date limite réponse avis correcte')

    def test_validite_date_limite_preavis(self):
        """ AR - Tester si la date limite renseignée pour un préavis est acceptable """
        """ NOK si < aujourd\'hui et > 2 jours avant début manif """
        if self.avis.date_limite_reponse_dt < timezone.now() or (self.avis.date_limite_reponse_dt > (self.avis.instruction.manif.date_debut - timedelta(days=2))):
            print('Date limite réponse préavis incorrecte')
        else:
            print('Date limite réponse préavis correcte')

    def test_dateLimite_depassee(self):
        # expirable_date = timezone.now() - timezone.timedelta(days=self.instance.avis_delai_service + 2)
        expirable_date = timezone.now() - timezone.timedelta(days=self.fede.delai_jour_avis_int + 2) # aujourd'hui - 23j
        # self.avis.date_demande = expirable_date
        self.avis.date_limite_reponse_dt = expirable_date
        self.assertEqual(self.avis.delai_expire(), True)

    def test_preavis(self):
        """ Tester les méthodes concernant les préavis """
        self.assertEqual(self.avis.get_nb_preavis(), 0)  # Aucun préavis par défaut
        self.assertEqual(self.avis.get_nb_preavis_a_rendre(), 0)  # Donc aucun préavis en attente
        self.assertTrue(self.avis.tous_preavis_rendus())  # Et tous les préavis sont rendus

    def test_access_url(self):
        """ Tester l'URL d'accès """
        self.assertEqual(self.avis.get_absolute_url(), reverse('instructions:avis_detail', kwargs={'pk': self.avis.pk}))