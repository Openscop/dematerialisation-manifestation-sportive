import re, time

from django.test import tag, override_settings
from selenium.webdriver.common.by import By

from .test_base_selenium import SeleniumCommunClass


class InstructionCircuit_EDSRTests(SeleniumCommunClass):
    """
    Test du circuit d'instance EDSR avec sélénium pour une Dnm
        circuit_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ WF_EDSR (Sel) =============')
        SeleniumCommunClass.init_setup(cls)
        cls.agentlocal_edsr.delete()
        cls.avis_nb = 6
        super().setUpClass()

    @tag('selenium')
    @override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
    def test_Circuit_EDSR(self):
        """
        Test des différentes étapes du circuit EDSR pour une autorisationNM
        """

        print('**** test 0 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username="organisateur", password='123'))
        # Appel de la page tableau de bord organisateur
        url_orga = '/tableau-de-bord-organisateur/' + str(self.structure.pk) + '/'
        reponse = self.client.get(url_orga + 'liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^\']+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        # Ajout des fichiers nécessaires
        liste_files = ["reglement_manifestation", "disposition_securite", "itineraire_horaire"]
        for file in liste_files:
            with open('/tmp/' + file + '.txt') as openfile:
                url_file = re.search('action="(?P<url>([^"]+)).+data-name="' + file, reponse.content.decode('utf-8'))
                self.assertTrue(hasattr(url_file, 'group'))
                self.client.post(url_file.group('url'), {'fichier': openfile}, follow=True, HTTP_HOST='127.0.0.1:8000')

        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 1 vérification avis; 0 pour tous sauf la fédération ****')
        # Vérification des avis des divers agents
        # GGD
        self.connexion('agent_ggd')
        self.presence_avis('agent_ggd', 'none')
        self.deconnexion()
        # EDSR
        self.connexion('agent_edsr')
        self.presence_avis('agent_edsr', 'none')
        self.deconnexion()
        # Mairie
        self.connexion('agent_mairie')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_elements(By.CLASS_NAME, 'nav-item')[1].click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_mairie', 'none')
        self.deconnexion()
        # CG
        self.connexion('agent_cg')
        self.presence_avis('agent_cg', 'none')
        self.deconnexion()
        # CGSuperieur
        self.connexion('agent_cgsup')
        self.presence_avis('agent_cgsup', 'none')
        self.deconnexion()
        # SDIS
        self.connexion('agent_sdis')
        time.sleep(self.DELAY * 3)
        self.presence_avis('agent_sdis', 'none')
        self.deconnexion()

        print('**** test 2 instructeur - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.distribuer_avis(['Police', 'Gendarmerie', 'Pompier', 'Conseil départemental', 'Mairie'])
        # Vérifier le passage en encours et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 3 avis fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.connexion('agent_fede')
        self.presence_avis('agent_fede', 'nouveau')
        self.assertIn('Délai : 28 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_fede', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 4 vérification avis; 0 pour GGD, CGD et Brigade ****')
        # Vérification des avis des divers agents
        # EDSR
        self.connexion('agent_ggd')
        self.presence_avis('agent_ggd', 'none')
        self.deconnexion()
        # CGD
        self.connexion(self.agent_cgd1.username)
        self.presence_avis('agent_cgd', 'none')
        self.deconnexion()
        # Brigade
        self.connexion(self.agent_brg1.username)
        self.presence_avis('agent_brg', 'none')
        self.deconnexion()

        print('**** test 5 avis edsr - distribution ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en nouveau
        self.connexion('agent_edsr')
        self.presence_avis('agent_edsr', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.distribuer_preavis(self.cgd1.precision_nom_s)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_edsr', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 6 preavis cgd ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en nouveau
        self.connexion(self.agent_cgd1.username)
        self.presence_avis('agent_cgd', 'nouveau')
        print('\t >>> Informer brigade')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Octroyer un accès en lecture', self.selenium.page_source)
        # Ouvrir l'accès en lecture
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.CLASS_NAME, 'formconsult_btn').click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.ID, 'complet').click()
        time.sleep(self.DELAY)
        self.chosen_select('select_instances_modal_chosen', self.dep.instance.__str__())
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % 'Gendarmerie').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, "show_sous_service").click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, "service-search").send_keys('BTA1')
        time.sleep(self.DELAY * 4)
        self.selenium.find_element(By.ID, f'name_container_{self.brigade1.pk}').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, self.brigade1.pk).click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, 'btn-close-modal-avis').click()
        time.sleep(self.DELAY * 3)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cgd', 'demarre')
        # # Appel de la vue de détail et test présence manifestation
        self.vue_detail()

        print('\t >>> Rendre préavis')
        self.envoyer_avis("préavis")
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Vérification du passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cgd', 'rendu')
        self.deconnexion()

        print('\t >>> Vérifier brigade')
        self.connexion(self.agent_brg1.username)
        self.assertIn('Accès en lecture', self.selenium.page_source)
        self.deconnexion()

        print('**** test 7 avis edsr - soumission ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en encours
        self.connexion('agent_edsr')
        self.presence_avis('agent_edsr', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.adresser_avis('GGD GGD (42)')
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # vérification de la présence de l'événement en encours
        self.selenium.execute_script("window.scroll(0, 600)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_edsr', 'rendu')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.deconnexion()

        print('**** test 8 avis ggd ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en encours
        self.connexion('agent_ggd')
        self.presence_avis('agent_ggd', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # vérification de la présence de l'événement en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ggd', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 9 vérification avis; 0 pour Commissariat ****')
        # Vérification des avis des divers agents
        # CGD
        self.connexion('agent_commiss')
        self.presence_avis('agent_commiss', 'none')
        self.deconnexion()

        print('**** test 10 avis ddsp - distribution ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.distribuer_preavis('Bard')
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        # self.aucune_action()
        # Vérifier le passage en encours
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ddsp', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 11 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        self.connexion('agent_commiss')
        self.presence_avis('agent_commiss', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis("préavis")
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Vérification du passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_commiss', 'rendu')
        self.deconnexion()

        print('**** test 12 avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en encours
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ddsp', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 13 avis mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en nouveau
        self.connexion('agent_mairie')
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Demande').click()
        self.presence_avis('agent_mairie', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Demande').click()
        self.presence_avis('agent_mairie', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 14 vérification avis; 0 pour CGService ****')
        # Vérification des avis des divers agents
        # CGService
        self.connexion('agent_cgserv')
        self.presence_avis('agent_cgserv', 'none')
        self.deconnexion()

        print('**** test 15 avis cg - distribution ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en nouveau
        self.connexion('agent_cg')
        self.presence_avis('agent_cg', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.distribuer_preavis('STD')
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Vérifier le passage en encours
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cg', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 16 preavis cgservice ****')
        # Instruction du préavis par le cgservice, vérification de la présence de l'événement en nouveau
        self.connexion('agent_cgserv')
        self.presence_avis('agent_cgserv', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis("préavis")
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Vérification du passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cgserv', 'rendu')
        self.deconnexion()

        print('**** test 17 vérification avis; 0 pour CGSupérieur ****')
        # Vérification des avis des divers agents
        # CGSuperieur
        self.connexion('agent_cgsup')
        self.presence_avis('agent_cgsup', 'none')
        self.deconnexion()

        print('**** test 18 avis cg - soumission ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en encours
        self.connexion('agent_cg')
        self.presence_avis('agent_cg', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.adresser_avis('CDSUP CDSUP (42)')
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # vérification de la présence de l'événement en encours
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cg', 'rendu')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.deconnexion()

        print('**** test 19 agent cgsup ****')
        # Instruction de l'avis par le cgsup, vérification de la présence de l'événement en encours
        self.connexion('agent_cgsup')
        self.presence_avis('agent_cgsup', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cgsup', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 20 vérification avis; 0 pour Groupement, codis et cis ****')
        # Vérification des avis des divers agents
        # CIS
        self.connexion('agent_cis')
        self.selenium.execute_script("window.scroll(0, 1000)")
        self.presence_avis('agent_cis', 'none')
        self.deconnexion()
        # CODIS
        self.connexion('agent_codis')
        self.presence_avis('agent_codis', 'none')
        self.deconnexion()
        # SDIS groupement
        self.connexion('agent_group')
        self.presence_avis('agent_group', 'none')
        self.deconnexion()

        print('**** test 21 avis sdis - distribution ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en nouveau
        self.connexion('agent_sdis')
        self.presence_avis('agent_sdis', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.distribuer_preavis('GROUP')
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_sdis', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 22 preavis groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en nouveau
        self.connexion('agent_group')
        self.presence_avis('agent_group', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis("préavis")
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_group', 'rendu')
        self.deconnexion()

        print('**** test 23 avis sdis ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en encours
        self.connexion('agent_sdis')
        self.presence_avis('agent_sdis', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        self.envoyer_avis()
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_sdis', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.deconnexion()
