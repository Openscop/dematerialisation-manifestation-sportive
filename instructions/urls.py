# coding: utf-8
from django.urls import path, re_path

from instructions.views.instruction import *
from instructions.views.acces import *
from instructions.views.avis import *
from instructions.views.ajax import CreateAvisAJAXView
from instructions.views.tableaudebord import *
from evenements.views.piecejointe import ValidationInstructeur, DemandePieceComplementaireInstructeur


app_name = 'instructions'
urlpatterns = [
    # Dashboard
    path('tableaudebord/', TableauBody.as_view(), name="tableaudebord"),
    path('tableaudebord/<int:service_pk>/', TableauBody.as_view(), name="tableaudebord"),
    path('tableaudebord/choix/', TableauChoix.as_view(), name="tableaudebord_choix"),
    path('tableaudebord/<int:service_pk>/list/', TableauListe.as_view(), name="tableaudebordliste"),
    path('tableaudebord/<int:service_pk>/ajax/', TableauAjax.as_view(), name="tableaudebordajax"),

    # Avis
    re_path('avis/(?P<pk>\d+)/interroger/', AvisInterroger.as_view(), name='avis_interroger'),
    re_path('avis/(?P<pk>\d+)/mandater/', AvisMandater.as_view(), name='avis_mandater'),
    re_path('avis/(?P<pk>\d+)/adresser/', AvisAdresser.as_view(), name='avis_adresser'),
    re_path('avis/(?P<pk>\d+)/rediger/', AvisRediger.as_view(), name='avis_rediger'),
    path('avis/<int:pk>/rendre/<initiateur>/', AvisRendre.as_view(), name='avis_rendre'),
    re_path('avis/(?P<pk>\d+)/resend/', AvisResend.as_view(), name='avis_resend'),
    re_path('avis/(?P<pk>\d+)/relance/', AvisRedemande.as_view(), name='avis_relance'),
    re_path('avis/(?P<pk>\d+)/redemande_avis/', RedemandeAvis.as_view(), name="redemande_avis"),
    path('avis/<int:pk>/addfile/', AvisAddFile.as_view(), name='avis_ajout_fichier'),
    path('avis/<int:pk>/getpreavis/', AvisGetPreavisAjax.as_view(), name='avis_get_preavis'),
    path('avis/<int:pk>/getavis/', AvisGetAvisAjax.as_view(), name='avis_get_avis'),
    path('avis/<int:pk>/del/', AvisRemove.as_view(), name='avis_remove'),
    path(r'avis/<int:pk>/confirme/', AvisConfirme.as_view(), name='avis_confirme'),
    path(r'avis/<int:pk>/datelimite/', DateLimiteReponse.as_view(), name='date_limite_reponse_edit'),
    re_path('avis/(?P<pk>\d+)/', AvisDetail.as_view(), name='avis_detail'),

    # AutorisationAcces
    path('acces/<int:pk>/create/', AutorisationAccesCreate.as_view(), name='acces_create'),
    path('acces/<int:pk>/remove/', AutorisationAccesRemove.as_view(), name='acces_remove'),

    path('ajouter/<int:pk>/avis/', CreateAvisAJAXView.as_view(), name='ajouter_avis_ajax'),
    path('ajouter/<int:pk>/cdsr/', CreateAvisAJAXView.as_view(), name='ajouter_cdsr_ajax'),

    # Autres vues
    re_path('add/(?P<manif_pk>\d+)', InstructionCreateView.as_view(), name='instruction_add'),
    re_path('(?P<pk>\d+)/valider/', ValidationInstructeur.as_view(), name='valider_documents'),
    re_path('(?P<pk>\d+)/avis_prefecture/', AvisPrefecture.as_view(), name='avis_prefecture'),
    re_path('(?P<pk>\d+)/avis_prefecture_pj/', AvisPrefecturePiecejointe.as_view(), name='avis_prefecture_pj'),
    re_path('(?P<pk>\d+)/avis_prefecture_ajout_pj/', AvisPrefAjoutPJ.as_view(), name='avis_prefecture_ajout_pj'),

    re_path('(?P<pk>\d+)/demandepj/', DemandePieceComplementaireInstructeur.as_view(), name='demande_piece_complementaire'),
    re_path('(?P<pk>\d+)/publish/', InstructionPublishBylawView.as_view(), name='instruction_publish'),
    path('<int:pk>/publier_pj/', InstructionTransfertPjVersDocOfficiel.as_view(), name='instruction_transfert_pj_vers_doc_officiel'),
    re_path('(?P<pk>\d+)/relance/', RelanceAvis.as_view(), name="relance_avis"),
    re_path('(?P<pk>\d+)/assigner/', AssignationDossier.as_view(), name="assigner"),
    # re_path('(?P<pk>\d+)/edit/', InstructionUpdateView.as_view(), name="instruction_update"),
    re_path('(?P<pk>\d+)/', InstructionDetailView.as_view(), name="instruction_detail"),
]
