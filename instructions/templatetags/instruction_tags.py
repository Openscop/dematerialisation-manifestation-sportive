# coding: utf-8
from django import template
from instructions.models.instruction import Instruction
from instructions.models.acces import AutorisationAcces
from instructions.models.avis import Avis
from structure.models import Organisation
from evenements.templatetags.get_history import date_history

register = template.Library()


@register.filter
def avis(value, service):
    """ Renvoyer l'avis correspondant à l'instruction et à l'agent """
    if isinstance(value, Instruction):

        avis = value.get_avis_service(service)
        if avis:
            return value.get_avis_service(service)
        if AutorisationAcces.objects.filter(manif=value.manif, organisation=service, avis__isnull=False).exists():
            acces = AutorisationAcces.objects.filter(manif=value.manif, organisation=service, avis__isnull=False).first()
            return acces.avis
    return None


@register.filter
def interaction(avis, request):
    if isinstance(avis, Avis):
        interaction_filtrer = list(filter(lambda x: x['sortant'] == request.session['service_pk'], avis.historique_circuit_json))
        if interaction_filtrer:
            html = ''
            match interaction_filtrer[-1]['type']:
                case "creer":
                    html = "<span data-bs-toggle='tooltip'  title='Avis reçu'><i class='recevoir-avis ico_double no-tooltip'></i></span>"
                case "adresser":
                    if not avis.avis_parent_fk:
                        html = "<span data-bs-toggle='tooltip'  title='Adressé'><i class='adresser-avis ico_double no-tooltip'></i></span>"
                    else:
                        html = "<span data-bs-toggle='tooltip'  title='Adressé'><i class='adresser-preavis ico_double no-tooltip'></i></span>"
                case "mandater":
                    html = "<span data-bs-toggle='tooltip'  title='Mandaté'><i class='mandater-avis ico_double no-tooltip'></i></span>"
                case "interroger":
                    html = "<span data-bs-toggle='tooltip'  title='Interrogé'><i class='interroger-preavis ico_double no-tooltip'></i></span>"
                case "relance":
                    if avis.avis_parent_fk:
                        html = "<span data-bs-toggle='tooltip'  title='Interrogé'><i class='interroger-preavis ico_double no-tooltip'></i> <i class='fa-solid fa-arrows-repeat'></i></span>"
                    else:
                        html = "<span data-bs-toggle='tooltip'  title='Avis reçu'><i class='recevoir-avis ico_double no-tooltip'></i> <i class='fa-solid fa-arrows-repeat'></i></span>"
                case "redemande":
                    html = "<span data-bs-toggle='tooltip'  title='Avis reçu'><i class='recevoir-avis ico_double no-tooltip'></i> <i class='fa-solid fa-arrows-repeat'></i></span>"
            return html
    return ""


@register.filter
def historique_circuit_json_avis(json):
    """
    Afficher l'historique d'un avis
    """
    html = "<ul>"
    for etape in json:
        try:
            organisation_entrante = Organisation.objects.get(pk=etape['entrant'])
        except:
            organisation_entrante = "Service inexistant"

        try:
            organisation_sortante = Organisation.objects.get(pk=etape['sortant'])
        except:
            organisation_sortante = "Service inexistant"
        date = date_history(etape['date']) if "date" in etape else "",

        match etape["type"]:
            case "creer":
                texte = "a créé l'avis pour"
            case "adresser":
                texte = "a adressé l'avis à"
            case "mandater":
                texte = "a mandaté l'avis à"
            case "rendre":
                texte = "a rendu l'avis à"
            case "interroger":
                texte = "a interrogé"
            case "redemande":
                texte = "a redemandé l'avis à"
            case "relance":
                texte = "a voulu rendre un nouvel avis à"
            case _:
                texte = ""
        html += f"<li>{date[0]} {organisation_entrante} {texte} {organisation_sortante }</li>"
    html += "</ul>"
    return html


@register.filter
def get_instruction(value, request):
    organisation = Organisation.objects.get(pk=request.session['service_pk'])
    instruction = value.get_instruction(instance=organisation.instance_fk)
    if instruction:
        return instruction
    if organisation.is_service_instructeur():
        if AutorisationAcces.objects.filter(manif=value, organisation=organisation, instruction__isnull=False).exists():
            acces = AutorisationAcces.objects.filter(manif=value, organisation=organisation, instruction__isnull=False).first()
            return acces.instruction
    if organisation.is_service_consulte():
        if AutorisationAcces.objects.filter(manif=value, organisation=organisation, avis__isnull=False).exists():
            acces = AutorisationAcces.objects.filter(manif=value, organisation=organisation, avis__isnull=False).first()
            return acces.avis.instruction
    return None


@register.simple_tag
def setvar(value):
    return value
