import factory

from evenements.factories import DcnmFactory
from .models import Instruction, Avis, DocumentOfficiel


class InstructionFactory(factory.django.DjangoModelFactory):
    """ Factory pour les instructions """

    # Champs
    manif = factory.SubFactory(DcnmFactory)
    instance = factory.SelfAttribute('manif.instance')

    # Meta
    class Meta:
        model = Instruction


class AvisInstructionFactory(factory.django.DjangoModelFactory):
    """ Factory des avis """

    instruction = factory.SubFactory(InstructionFactory)

    # Meta
    class Meta:
        model = Avis


class DocOfficielFactory(factory.django.DjangoModelFactory):
    """ Factory des documents officiels """

    instruction = factory.SubFactory(InstructionFactory)

    # Meta
    class Meta:
        model = DocumentOfficiel
