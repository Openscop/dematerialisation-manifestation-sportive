# coding: utf-8
import asyncio
from datetime import timedelta

from asgiref.sync import sync_to_async, async_to_sync
from django.views.generic.base import View
from django.db.models import Q, Sum, Count, Avg, F
from django.utils import timezone
from django.utils.decorators import method_decorator, classonlymethod
from django.contrib.auth.decorators import login_required
from django.http.response import JsonResponse
from django.shortcuts import render
from django.conf import settings

from collections import OrderedDict
from post_office.models import Email

from carto.models import Parcours
from core.models import User, Instance
from evenements.models import Manif, Avtm, Dvtmcir, Dvtm, Dnm, Dnmc, Dcnm, Dcnmc
from instructions.models import Avis as InstructionAvis, AutorisationAcces, Instruction
from messagerie.models import Enveloppe, Message
from evaluation_incidence.models import EvaluationN2K, EvaluationRnr
from structure.models import Organisation, ServiceInstructeur, StructureOrganisatrice, CategorieService, ServiceConsulte
from forum.forum_conversation.models import Topic, Post
from forum.forum.models import Forum
from forum.forum_permission.viewmixins import PermissionRequiredMixin

@method_decorator(login_required, name="dispatch")
class StatBodyView(View):

    def get(self, request):
        if request.session['o_support']:
            instances = Instance.objects.all().order_by("departement__name")
            instance_def = Instance.objects.get_master()
        else:
            instances = Instance.objects.all().order_by("departement__name")
            instance_def = Instance.objects.get(departement=request.user.get_departement())
        nb_session_filter = Q(default_instance=instance_def) if not request.session['o_support'] else Q()
        last_time = timezone.now() - timedelta(minutes=settings.INACTIVE_TIME)
        connected_user = User.objects.filter(nb_session_filter, last_visit__gte=last_time).count()
        annee_dispo = range(2018, timezone.now().year + 2)
        context = {"instances": instances,
                   "annee_dispo": annee_dispo,
                   "instance_def": instance_def,
                   "connected_user": connected_user}
        return render(request, 'stat/stat_body.html', context=context)


class StatAsync(PermissionRequiredMixin, View):
    def __init__(self, **kwargs):
        super(StatAsync, self).__init__()
        self.annee = None
        self.annee_fin = timezone.now().year + 2

    @classonlymethod
    def as_view(cls, **initkwargs):
        view = super().as_view(**initkwargs)
        view._is_coroutine = asyncio.coroutines._is_coroutine
        return view

    async def get_stat_annee(self, **kwargs):
        """
        Fonction pour avec les stats de plusieurs années
        Chaque année sera calculée dans une instance de coroutine différente, ce qui permet de lancer les calculs tous en même temps.
        """
        @sync_to_async(thread_sensitive=False)
        def filter_coro(y, liste, filter):
            """
            Coroutine pour calculer le nombre d'objet dans une liste avec un filtre donné
            """
            return y, liste.filter(**{filter: y}).count()

        @sync_to_async(thread_sensitive=False)
        def count_coro(y, liste, filter, count):
            """
            Coroutine pour calculer la somme d'un attribut d'une requete avec un filtrage donné
            """
            liste = liste.filter(**{filter: y}).aggregate(Sum(count))
            return y, liste

        @sync_to_async(thread_sensitive=False)
        def count_modif(y, liste, filter):
            """
            Coroutine pour calculer le nombre de modifications total d'une requete avec un filtrage donnée
            """
            manifs = liste.filter(**{filter: y})
            count = 0
            for manif in manifs:
                count += len(manif.modif_json)
            return y, count

        @sync_to_async(thread_sensitive=False)
        def moyenne_avis(y, liste, filter):
            """
            Coroutine pour avoir la moyenne du temps de réponse d'un avis avec un filtre donnée
            """
            delta = liste.filter(**{filter: y}).aggregate(delai=Avg(F('date_reponse') - F('date_demande')))
            return y, delta['delai'].days if delta['delai'] else ''

        data = None
        if "liste" in kwargs and "critere" in kwargs:
            liste = kwargs.get('liste')
            critere = kwargs.get('critere')
            data = {}
            if not "ope" in kwargs:
                coros = [filter_coro(y, liste, f"{critere}__year") for y in range(self.annee, self.annee_fin)]
            elif kwargs.get('ope') == "moyenne_avis":
                coros = [moyenne_avis(y, liste, f"{critere}__year") for y in range(self.annee, self.annee_fin)]
            elif kwargs.get('ope') == "count_modif":
                coros = [count_modif(y, liste, f"{critere}__year") for y in range(self.annee, self.annee_fin)]
            else:
                coros = [count_coro(y, liste, f"{critere}__year", kwargs.get('ope')) for y in range(self.annee, self.annee_fin)]
            tab = await asyncio.gather(*coros)
            for x in tab:
                    data[x[0]] = x[1]
        return OrderedDict(sorted(data.items(), key=lambda t: t[0]))

    async def merge_dicts(self, d1, d2):
        """
        Fusionner deux tableaux
        """
        array = [d1, d2]
        result = {}
        for k in d1.keys():
            result[k] = tuple(result[k] for result in array)
        return result

    async def get(self, request):
        if request.user.is_anonymous:
            return render(
                request, "core/access_restricted.html",
                {'message': "Veuillez vous authentifier pour accéder à votre information.<br>Pour cela,"
                            " utilisez le bouton <a href=\"/accounts/login/\" class=\"font-italic\">"
                            "Connexion</a> en haut à droite de cette page..."}, status=401)
        stat = {}
        if request.GET.get('annee'):
            self.annee = int(request.GET.get('annee', 2018))
        else:
            self.annee = int(request.GET.get('annee_unique', 2018))
            self.annee_fin = self.annee + 1
        stat['annees'] = range(self.annee, self.annee_fin)
        instance_pk = request.GET.get("instance") if request.GET.get("instance") and request.GET.get('instance') != "2" else None

        if instance_pk:
            filtre_dept = Q(ville_depart__arrondissement__departement__instance__pk=instance_pk)
        else:
            filtre_dept = Q()

        @sync_to_async
        def filter_annee(request, critere):
            return request.filter(**{f"{critere}__year__gte": self.annee}).filter(**{f"{critere}__year__lte": self.annee_fin -1})

        if request.GET.get('id') == "forum":
            nb_sujet_filter = Q(poster__default_instance__departement__instance__pk=instance_pk) if instance_pk else Q()
            requete = await sync_to_async(Topic.objects.filter)(nb_sujet_filter)
            requete = await filter_annee(requete, "created")
            stat['nb_sujet'] = await sync_to_async(requete.count)()
            stat['forums'] = {}
            forum_dict = {}
            forums_array = []
            @sync_to_async
            def get_forums():
                forums = self.request.forum_permission_handler.forum_list_filter(Forum.objects.all(), self.request.user,)
                if instance_pk:
                    forums = forums.filter(Q(departement__instance__pk=instance_pk) | Q(parent=None))
                for forum in forums:
                    forums_array.append(forum)
            await get_forums()

            for forum in forums_array:
                sujet = await sync_to_async(requete.filter)(forum=forum)
                nb_sujet_total = await sync_to_async(sujet.count)()
                forum_dict[str(forum)] = {}
                forum_dict[str(forum)]["nb_sujet_par_annee"] = await self.get_stat_annee(liste=sujet, critere="created")
                forum_dict[str(forum)]["nb_sujet_total"] = nb_sujet_total

            nb_reponse_filter = Q(poster__default_instance__departement__instance__pk=instance_pk, approved=True) if instance_pk else Q(approved=True)
            requete = await sync_to_async(Post.objects.filter)(nb_reponse_filter)
            requete = await filter_annee(requete, "created")
            for forum in forums_array:
                nb_reponse = requete.filter(topic__forum=forum)
                nb_reponse_total = await sync_to_async(nb_reponse.count)()
                forum_dict[str(forum)]["nb_reponse_par_annee"] = await self.get_stat_annee(liste=nb_reponse, critere="created")
                forum_dict[str(forum)]["nb_reponse_total"] = nb_reponse_total
            for forum in forums_array:
                stat['forums'][forum] = await self.merge_dicts(forum_dict[str(forum)]["nb_reponse_par_annee"], forum_dict[str(forum)]["nb_sujet_par_annee"])
                stat['forums'][forum]['total'] = (forum_dict[str(forum)]["nb_reponse_total"], forum_dict[str(forum)]["nb_sujet_total"])
            return render(request, "stat/stat_forum.html", context={"stat": stat})

        if request.GET.get('id') == "manif":
            # Nombre de dossiers prévus
            requete = await sync_to_async(Manif.objects.filter)(filtre_dept)
            requete = await filter_annee(requete, "date_debut")
            stat['nb_dossier'] = await sync_to_async(requete.count)()
            if stat['nb_dossier'] == 0:  # éviter une division par zéro
                stat['nb_dossier'] = 0.1
            stat['nb_dossier_par_annee'] = await self.get_stat_annee(liste=requete,
                                                              critere="date_debut")

            type_manif_tab = [['dnm', Dnm], ['dnmc', Dnmc], ['dcnm', Dcnm], ['dcnmc', Dcnmc],
                          ['dvtm', Dvtm], ['avtm', Avtm], ['dvtmcir', Dvtmcir]]
            for type_manif in type_manif_tab:
                filtre_cerfa_dept = Q(
                    manifestation_ptr__ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q()
                requete = await sync_to_async(type_manif[1].objects.filter)(filtre_cerfa_dept)
                requete = await filter_annee(requete, "date_debut")
                stat[f"nb_cerfa_{type_manif[0]}"] = await sync_to_async(requete.count)()
                stat[f'pourcentage_cerfa_{type_manif[0]}'] = round((stat[f'nb_cerfa_{type_manif[0]}'] * 100) / stat['nb_dossier'])
                nb_cerfa_par_annee = await self.get_stat_annee(liste=requete, critere="date_debut")
                pourcentage_cerfa_par_annee = OrderedDict()
                for key, value in nb_cerfa_par_annee.items():
                    pourcentage_cerfa_par_annee[key] = round(
                        (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
                stat[f'{type_manif[0]}_par_annee'] = await self.merge_dicts(nb_cerfa_par_annee, pourcentage_cerfa_par_annee)

            # Nombre de manifestations créées
            requete = await sync_to_async(Manif.objects.filter)(filtre_dept)
            stat['nb_manif_crees'] = await sync_to_async(requete.count)()
            stat['nb_manif_crees_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_creation")

            # Nombre de manifestations publiées dans le calendrier
            nb_manif_dans_calendrier_nouveau_filter = Q(
                dans_calendrier=True, ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q(
                dans_calendrier=True)
            requete = await sync_to_async(Manif.objects.filter)(nb_manif_dans_calendrier_nouveau_filter)
            requete = await filter_annee(requete, "date_debut")
            stat['nb_manif_dans_calendrier'] = await sync_to_async(requete.count)()
            stat['pourcentage_manif_dans_calendrier'] = round(
                (stat['nb_manif_dans_calendrier'] * 100) / stat['nb_dossier'])
            nb_manif_dans_calendrier_par_annee = await self.get_stat_annee(liste=requete, critere="date_debut")
            pourcentage_manif_dans_calendrier_par_annee = OrderedDict()
            for key, value in nb_manif_dans_calendrier_par_annee.items():
                pourcentage_manif_dans_calendrier_par_annee[key] = round(
                    (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
            stat['manif_dans_calendrier_par_annee'] = await self.merge_dicts(nb_manif_dans_calendrier_par_annee,
                                                                       pourcentage_manif_dans_calendrier_par_annee)

            # Nombre de manifestations motorisées
            manif_moto_filter = Q(activite__discipline__motorise=True,
                ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q(
                activite__discipline__motorise=True)
            requete = await sync_to_async(Manif.objects.filter)(manif_moto_filter)
            requete = await filter_annee(requete, "date_debut")
            stat['nb_manif_motorisees'] = await sync_to_async(requete.count)()
            stat['pourcentage_manif_motorisees'] = round((stat['nb_manif_motorisees'] * 100) / stat['nb_dossier'])
            nb_manif_motorisees_par_annee = await self.get_stat_annee(liste=requete, critere="date_debut")
            pourcentage_manif_motorisees_par_annee = OrderedDict()
            for key, value in nb_manif_motorisees_par_annee.items():
                pourcentage_manif_motorisees_par_annee[key] = round(
                    (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
            stat['manif_motorisees_par_annee'] = await self.merge_dicts(nb_manif_motorisees_par_annee,
                                                                  pourcentage_manif_motorisees_par_annee)

            # Nombre de manifestations non motorisées
            manif_non_moto_filter = Q(activite__discipline__motorise=False,
                                  ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q(
                activite__discipline__motorise=False)
            requete = await sync_to_async(Manif.objects.filter)(manif_non_moto_filter)
            requete = await filter_annee(requete, "date_debut")
            stat['nb_manif_non_motorisees'] = await sync_to_async(requete.count)()
            stat['pourcentage_manif_non_motorisees'] = round(
                (stat['nb_manif_non_motorisees'] * 100) / stat['nb_dossier'])
            nb_manif_non_motorisees_par_annee = await self.get_stat_annee(liste=requete, critere="date_debut")
            pourcentage_manif_non_motorisees_par_annee = OrderedDict()
            for key, value in nb_manif_non_motorisees_par_annee.items():
                pourcentage_manif_non_motorisees_par_annee[key] = round(
                    (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
            stat['manif_non_motorisees_par_annee'] = await self.merge_dicts(nb_manif_non_motorisees_par_annee,
                                                                      pourcentage_manif_non_motorisees_par_annee)

            # Nombre de manifestations sur une seule commune
            manif_une_commune_filter = Q(villes_traversees__isnull=True,
                                  ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q(
                villes_traversees__isnull=True)
            requete = await sync_to_async(Manif.objects.filter)(manif_une_commune_filter)
            requete = await filter_annee(requete, "date_debut")
            stat['nb_manif_une_commune'] = await sync_to_async(requete.count)()
            stat['pourcentage_manif_une_commune'] = round((stat['nb_manif_une_commune'] * 100) / stat['nb_dossier'])
            nb_manif_une_commune_par_annee = await self.get_stat_annee(liste=requete, critere="date_debut")
            pourcentage_manif_une_commune_par_annee = OrderedDict()
            for key, value in nb_manif_une_commune_par_annee.items():
                pourcentage_manif_une_commune_par_annee[key] = round(
                    (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
            stat['manif_une_commune_par_annee'] = await self.merge_dicts(nb_manif_une_commune_par_annee,
                                                                   pourcentage_manif_une_commune_par_annee)

            # Nombre d'évaluations N2k
            nb_eval_n2k_filter = Q(manif__ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q()
            requete = await sync_to_async(EvaluationN2K.objects.filter)(nb_eval_n2k_filter)
            requete = await filter_annee(requete, "manif__date_debut")
            stat['nb_eval_n2k'] = await sync_to_async(requete.count)()
            stat['pourcentage_eval_n2k'] = round((stat['nb_eval_n2k'] * 100) / stat['nb_dossier'])
            nb_eval_n2k_par_annee = await self.get_stat_annee(liste=requete, critere="manif__date_debut")
            pourcentage_eval_n2k_par_annee = OrderedDict()
            for key, value in nb_eval_n2k_par_annee.items():
                pourcentage_eval_n2k_par_annee[key] = round(
                    (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
            stat['nb_eval_n2k_par_annee'] = await self.merge_dicts(nb_eval_n2k_par_annee, pourcentage_eval_n2k_par_annee)

            # Nombre d'évaluations Rnr
            nb_eval_rnr_filter = Q(manif__ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q()
            requete = await sync_to_async(EvaluationRnr.objects.filter)(nb_eval_rnr_filter)
            requete = await filter_annee(requete, "manif__date_debut")
            stat['nb_eval_rnr'] = await sync_to_async(requete.count)()
            stat['pourcentage_eval_rnr'] = round((stat['nb_eval_rnr'] * 100) / stat['nb_dossier'])
            nb_eval_rnr_par_annee = await self.get_stat_annee(liste=requete, critere="manif__date_debut")
            pourcentage_eval_rnr_par_annee = OrderedDict()
            for key, value in nb_eval_rnr_par_annee.items():
                pourcentage_eval_rnr_par_annee[key] = round(
                    (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
            stat['nb_eval_rnr_par_annee'] = await self.merge_dicts(nb_eval_rnr_par_annee, pourcentage_eval_rnr_par_annee)

            # nombre de participant:
            requete = await sync_to_async(Manif.objects.filter)(filtre_dept)
            requete = await filter_annee(requete, "date_debut")
            stat['nb_participant'] = await sync_to_async(requete.aggregate)(Sum('nb_participants'))
            stat['nb_participant_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_debut", ope="nb_participants")

            # nombre de spectateurs
            requete = await sync_to_async(Manif.objects.filter)(filtre_dept)
            requete = await filter_annee(requete, "date_debut")
            stat['nb_spectateur'] = await sync_to_async(requete.aggregate)(Sum('nb_spectateurs'))
            stat['nb_spectateur_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_debut",
                                                                         ope="nb_spectateurs")

            # nombre de parcours
            nb_parcours_filter = Q(
                manif__ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q()
            requete = await sync_to_async(Parcours.objects.filter)(nb_parcours_filter)
            requete = await filter_annee(requete, "manif__date_debut")
            stat['nb_parcours'] = await sync_to_async(requete.count)()
            stat['moyenne_nb_parcours'] = round(stat['nb_parcours'] / stat['nb_dossier'])
            nb_parcours_par_annee = await self.get_stat_annee(liste=requete, critere="manif__date_debut")
            moyenne_manif_parcours_par_annee = OrderedDict()
            for key, value in nb_parcours_par_annee.items():
                moyenne_manif_parcours_par_annee[key] = round(
                    value / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
            stat['nb_parcours_par_annee'] = await self.merge_dicts(nb_parcours_par_annee,
                                                                  moyenne_manif_parcours_par_annee)


            @sync_to_async
            def get_requete_interdep():
                nb_parcours_filter = Q(nb_instance_instruites__gt=1,
                    ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q(nb_instance_instruites__gt=1)
                return Manif.objects.annotate(nb_instance_instruites=Count('instance_instruites')).filter(nb_parcours_filter)
            requete = await get_requete_interdep()
            requete = await filter_annee(requete, "date_debut")
            stat['nb_interdep'] = await sync_to_async(requete.count)()
            stat['nb_interdep_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_debut")

            @sync_to_async
            def get_requete():
                requete = Manif.objects.filter(filtre_dept).exclude(modif_json=[]).exclude(modif_json__isnull=True)
                requete = async_to_sync(filter_annee)(requete, "date_debut")
                count = requete.count()
                return requete, count

            requete, count = await get_requete()
            stat['nb_dossier_modif'] = count
            stat['nb_dossier_modif_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_debut")

            @sync_to_async
            def get_requete():
                requete = Manif.objects.filter(filtre_dept).exclude(modif_json=[]).exclude(modif_json__isnull=True)

                requete = async_to_sync(filter_annee)(requete, "date_debut")
                count = 0
                for manif in requete:
                    count += len(manif.modif_json)
                return requete, count

            requete, count = await get_requete()
            stat['nb_dossier_modif_count'] = count
            stat['moyenne_dossier_count'] = round(count / stat['nb_dossier_modif']) if stat['nb_dossier_modif'] else 0
            nb_dossier_par_annee_count = await self.get_stat_annee(liste=requete, critere="date_debut",
                                                                   ope="count_modif")
            moyenne_dossier_par_annee_count = OrderedDict()
            for key, value in nb_dossier_par_annee_count.items():
                moyenne_dossier_par_annee_count[key] = round(value / stat['nb_dossier_modif_par_annee'][key]) if \
                stat['nb_dossier_modif_par_annee'][key] else 0
            stat['nb_dossier_modif_par_annee_count'] = await self.merge_dicts(nb_dossier_par_annee_count,
                                                                              moyenne_dossier_par_annee_count)

            return render(request, 'stat/stat_manif.html', context={"stat": stat})

        if request.GET.get('id') == "utilisateur":
            nb_utilisateur_filter = Q(default_instance__departement__instance__pk=instance_pk) if instance_pk else Q()
            requete = await sync_to_async(User.objects.filter)(nb_utilisateur_filter)
            requete = await filter_annee(requete, "date_joined")
            stat['nb_utilisateur'] = await sync_to_async(requete.count)()
            stat['nb_utilisateur_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_joined")

            service_instructeurs = await sync_to_async(Organisation.objects.instance_of)(ServiceInstructeur)

            nb_instructeur_filter = Q(organisation_m2m__in=service_instructeurs,
                                      default_instance__departement__instance__pk=instance_pk) if instance_pk else \
                Q(organisation_m2m__in=service_instructeurs)
            requete = await sync_to_async(User.objects.filter)(nb_instructeur_filter)
            requete = await filter_annee(requete, "date_joined")
            stat['nb_instructeur'] = await sync_to_async(requete.count)()
            stat['nb_instructeur_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_joined")

            structure_organisatrice = await sync_to_async(Organisation.objects.instance_of)(StructureOrganisatrice)
            nb_organisateur_filter = Q(organisation_m2m__in=structure_organisatrice,
                                     default_instance__departement__instance__pk=instance_pk) if instance_pk else \
                Q(organisation_m2m__in=structure_organisatrice)
            requete = await sync_to_async(User.objects.filter)(nb_organisateur_filter)
            requete = await filter_annee(requete, "date_joined")
            stat['nb_organisateur'] = await sync_to_async(requete.count)()
            stat['nb_organisateur_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_joined")

            stat['nb_pll_categorie'] = []
            @sync_to_async
            def get_categorie():
                categories = [x for x in CategorieService.objects.exclude(pk__in=[1, 2]).order_by("my_order")]
                return categories
            categories = await get_categorie()
            for categorie in categories:
                temp = {}
                organisation = await sync_to_async(ServiceConsulte.objects.filter)(type_service_fk__categorie_fk=categorie)
                nb_pll_filter = Q(organisation_m2m__in=organisation,
                                           default_instance__departement__instance__pk=instance_pk) if instance_pk else \
                    Q(organisation_m2m__in=organisation)
                requete = await sync_to_async(User.objects.filter)(nb_pll_filter)
                requete = await filter_annee(requete, "date_joined")
                temp['nom'] = str(categorie)
                temp['nb'] = await sync_to_async(requete.count)()
                temp['nb_par_annee'] = await self.get_stat_annee(liste=requete,critere="date_joined")
                stat['nb_pll_categorie'].append(temp)

            return render(request, 'stat/stat_utilisateur.html', context={"stat": stat, "categories": categories})

        if request.GET.get('id') == "instruction":

            @sync_to_async
            def get_stat_instructions():
                context ={}
                instruction_dept_filter = Q(
                    manif__ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q()
                manif_pas_passe = Q(manif__date_debut__gte=timezone.now())
                context['instruction_demande'] = Instruction.objects.filter(instruction_dept_filter,
                                                                            etat='demandée').filter(
                    manif_pas_passe).count()
                context['instruction_commence'] = Instruction.objects.filter(instruction_dept_filter,
                                                                             etat='distribuée').filter(
                    manif_pas_passe).count()
                context['instruction_attente_decision'] = Instruction.objects.annotate(
                    nb_avis=Count('avis')).annotate(nb_avis_rendu=Count('avis', filter=Q(avis__etat='rendu'))).filter(
                    instruction_dept_filter, etat='distribuée').filter(manif_pas_passe).filter(
                    nb_avis=F('nb_avis_rendu')).count()
                context['instruction_en_attente_avis'] = Instruction.objects.annotate(
                    nb_avis=Count('avis'), nb_preavis=Count('avis__avis_enfant')).annotate(
                    nb_avis_rendu=Count('avis', filter=Q(avis__etat='rendu')),
                    nb_preavis_rendu=Count('avis__avis_enfant', filter=Q(avis__avis_enfant__etat='rendu'))).filter(
                    instruction_dept_filter, etat='distribuée').filter(manif_pas_passe).filter(
                    nb_preavis=F('nb_preavis_rendu'),
                    nb_avis__gt=F('nb_avis_rendu')).count()
                context['instruction_en_attente_preavis'] = Instruction.objects.annotate(
                    nb_preavis=Count('avis__avis_enfant')).annotate(
                    nb_preavis_rendu=Count('avis__avis_enfant', filter=Q(avis__avis_enfant__etat='rendu'))).filter(
                    instruction_dept_filter, etat='distribuée').filter(manif_pas_passe).filter(
                    nb_preavis__gt=F('nb_preavis_rendu')).count()

                context['instruction_en_cours'] = Instruction.objects.exclude(
                    etat__in=['interdite', 'autorisée', 'annulée']).filter(manif_pas_passe).filter(
                    instruction_dept_filter).count()
                context['instruction_terminée'] = Instruction.objects.exclude(
                    etat__in=['distribuée', 'demandée']).filter(manif_pas_passe).filter(instruction_dept_filter).count()
                return context
            data_instruction = await get_stat_instructions()
            @sync_to_async
            def get_categorie():
                categories = [x for x in CategorieService.objects.exclude(pk__in=[1, 2]).order_by("my_order")]
                return categories
            categories = await get_categorie()

            # Nombre d'avis émis
            nb_avis_filter = Q(
                instruction__manif__ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q()
            requete = await sync_to_async(InstructionAvis.objects.filter)(nb_avis_filter)
            requete = await filter_annee(requete, "date_demande")
            stat['nb_avis'] = await sync_to_async(requete.count)()
            stat['nb_avis_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_demande")

            stat['nb_avis_categorie'] = []
            for categorie in categories:
                temp = {}
                nb_avis_filter = Q(service_consulte_origine_fk__type_service_fk__categorie_fk=categorie,
                    instruction__manif__ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk \
                    else Q(service_consulte_origine_fk__type_service_fk__categorie_fk=categorie)
                requete = await sync_to_async(InstructionAvis.objects.filter)(nb_avis_filter)
                requete = await filter_annee(requete, "date_demande")
                temp['nom'] = str(categorie)
                temp['nb'] = await sync_to_async(requete.count)()
                temp['nb_par_annee'] = await self.get_stat_annee(liste=requete,critere="date_demande")
                stat['nb_avis_categorie'].append(temp)

            # Nombre de préavis émis
            nb_avis_filter = Q(
                instruction__manif__ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q()
            nb_preavis_filter = (nb_avis_filter & Q(avis_parent_fk__isnull=False))
            requete = await sync_to_async(InstructionAvis.objects.filter)(nb_preavis_filter)
            requete = await filter_annee(requete, "date_demande")
            stat['nb_preavis'] = await sync_to_async(requete.count)()
            stat['nb_preavis_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_demande")

            stat['nb_preavis_categorie'] = []
            for categorie in categories:
                temp = {}
                nb_avis_filter = Q(service_consulte_origine_fk__type_service_fk__categorie_fk=categorie,
                                   instruction__manif__ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk \
                    else Q(service_consulte_origine_fk__type_service_fk__categorie_fk=categorie)
                nb_preavis_filter = (nb_avis_filter & Q(avis_parent_fk__isnull=False))
                requete = await sync_to_async(InstructionAvis.objects.filter)(nb_preavis_filter)
                requete = await filter_annee(requete, "date_demande")
                temp['nom'] = str(categorie)
                temp['nb'] = await sync_to_async(requete.count)()
                temp['nb_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_demande")
                stat['nb_preavis_categorie'].append(temp)

            nb_consultation_filter = Q(
                manif__ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q()
            requete = await sync_to_async(AutorisationAcces.objects.filter)(nb_consultation_filter)
            requete = await filter_annee(requete, "date_creation")
            stat['nb_consultation'] = await sync_to_async(requete.count)()
            stat['nb_consultation_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_creation")

            structure_organisatrices = await sync_to_async(Organisation.objects.instance_of)(StructureOrganisatrice)
            nb_consultation_orga_filter = Q(organisation__in=structure_organisatrices,
                manif__ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else \
                Q(organisation__in=structure_organisatrices,)
            requete = await sync_to_async(AutorisationAcces.objects.filter)(nb_consultation_orga_filter)
            requete = await filter_annee(requete, "date_creation")
            stat['nb_consultation_orga'] = await sync_to_async(requete.count)()
            stat['nb_consultation_orga_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_creation")

            stat['nb_consultation_categorie'] = []
            for categorie in categories:
                temp = {}
                @sync_to_async
                def get_services_consulte_pks():
                    services_consultes = ServiceConsulte.objects.filter(type_service_fk__categorie_fk=categorie)
                    return [x.pk for x in services_consultes]
                services_consulte_pks = await get_services_consulte_pks()
                nb_consultation_filter = Q(organisation__pk__in=services_consulte_pks,
                    manif__ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else \
                    Q(organisation__pk__in=services_consulte_pks)
                requete = await sync_to_async(AutorisationAcces.objects.filter)(nb_consultation_filter)
                requete = await filter_annee(requete, "date_creation")
                temp['nom'] = str(categorie)
                temp['nb'] = await sync_to_async(requete.count)()
                temp['nb_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_creation")
                stat['nb_consultation_categorie'].append(temp)

            return render(request, 'stat/stat_instruction.html', context={"stat": stat} | data_instruction)

        if request.GET.get('id') == "delai":
            # Délais de restitution des avis
            @sync_to_async
            def get_avis_requete(categorie=None, preavis=None, instance_pk=None):
                avis_dept_filter = Q(
                    instruction__manif__ville_depart__arrondissement__departement__instance__pk=instance_pk) if instance_pk else Q()
                if categorie:
                    avis_dept_filter = avis_dept_filter & Q(service_consulte_origine_fk__type_service_fk__categorie_fk=categorie)
                if preavis:
                    avis_dept_filter = avis_dept_filter & Q(avis_parent_fk__isnull=False)
                requete = InstructionAvis.objects.exclude(date_reponse__isnull=True).filter(avis_dept_filter)
                requete = async_to_sync(filter_annee)(requete, "date_demande")
                result = requete.aggregate(delai_moyen=Avg(F('date_reponse') - F('date_demande')))['delai_moyen'].days if requete else ''
                return requete, result
            requete, result = await get_avis_requete(instance_pk=instance_pk)
            stat['delai_avis_moyen'] = result
            stat['delai_avis_moyen_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_demande", ope="moyenne_avis")
            if instance_pk:
                requete, result = await get_avis_requete(instance_pk=None)
                stat['delai_avis_moyen_national'] = result
                delai_avis_moyen_par_annee_national = await self.get_stat_annee(liste=requete, critere="date_demande",
                                                                               ope="moyenne_avis")
                stat['delai_avis_moyen_par_annee'] = await self.merge_dicts(stat['delai_avis_moyen_par_annee'], delai_avis_moyen_par_annee_national)


            @sync_to_async
            def get_categorie():
                categories = [x for x in CategorieService.objects.exclude(pk__in=[1, 2]).order_by("my_order")]
                return categories

            categories = await get_categorie()
            stat['delai_avis_moyen_categorie'] = []
            for categorie in categories:
                temp = {}
                requete, result = await get_avis_requete(categorie=categorie, instance_pk=instance_pk)
                temp['nom'] = str(categorie)
                temp['nb'] = result
                temp['nb_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_demande", ope="moyenne_avis")
                if instance_pk:
                    requete, result = await get_avis_requete(instance_pk=None, categorie=categorie)
                    temp['nb_national'] = result
                    nb_par_annee_national = await self.get_stat_annee(liste=requete, critere="date_demande",
                                                                                            ope="moyenne_avis")
                    temp['nb_par_annee'] = await self.merge_dicts(temp['nb_par_annee'], nb_par_annee_national)
                stat['delai_avis_moyen_categorie'].append(temp)

            requete, result = await get_avis_requete(preavis=True, instance_pk=instance_pk)
            stat['delai_preavis_moyen'] = result
            stat['delai_preavis_moyen_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_demande",
                                                                           ope="moyenne_avis")
            if instance_pk:
                requete, result = await get_avis_requete(preavis=True, instance_pk=None)
                stat['delai_preavis_moyen_national'] = result
                delai_preavis_moyen_par_annee_national = await self.get_stat_annee(liste=requete, critere="date_demande",
                                                                                  ope="moyenne_avis")
                stat['delai_preavis_moyen_par_annee'] = await self.merge_dicts(stat['delai_preavis_moyen_par_annee'], delai_preavis_moyen_par_annee_national)

            stat['delai_preavis_moyen_categorie'] = []
            for categorie in categories:
                temp = {}
                requete, result = await get_avis_requete(categorie=categorie, preavis=True, instance_pk=instance_pk)

                temp['nom'] = str(categorie)
                temp['nb'] = result
                temp['nb_par_annee'] = await self.get_stat_annee(liste=requete, critere="date_demande",
                                                                 ope="moyenne_avis")
                if instance_pk:
                    requete, result = await get_avis_requete(categorie=categorie, preavis=True, instance_pk=None)
                    temp['nb_national'] = result
                    nb_par_annee_national = await self.get_stat_annee(liste=requete, critere="date_demande",
                                                                              ope="moyenne_avis")
                    temp['nb_par_annee'] = await self.merge_dicts(temp['nb_par_annee'], nb_par_annee_national)
                stat['delai_preavis_moyen_categorie'].append(temp)


            return render(request, "stat/stat_delai.html", context={"stat": stat})

        if request.GET.get('id') == "messagerie":
            @sync_to_async
            def get_enveloppe(type):
                return Enveloppe.objects.filter(type=type).distinct('corps')

            # Nombre de conversation envoyées
            requete = await get_enveloppe('conversation')
            requete = await filter_annee(requete, "date")
            stat['nb_msg_conversation'] = await sync_to_async(requete.count)()
            stat['nb_msg_conversation_par_annee'] = await self.get_stat_annee(liste=requete, critere="date")

            # Nombre d'action envoyées
            requete = await get_enveloppe('action')
            requete = await filter_annee(requete, "date")
            stat['nb_msg_action'] = await sync_to_async(requete.count)()
            stat['nb_msg_action_par_annee'] = await self.get_stat_annee(liste=requete, critere="date")

            # Nombre d'info_suivi envoyées
            requete = await get_enveloppe('info_suivi')
            requete = await filter_annee(requete, "date")
            stat['nb_msg_info_suivi'] = await sync_to_async(requete.count)()
            stat['nb_msg_info_suivi_par_annee'] = await self.get_stat_annee(liste=requete, critere="date")

            # Nombre de news envoyées
            requete = await get_enveloppe('news')
            requete = await filter_annee(requete, "date")
            stat['nb_msg_news'] = await sync_to_async(requete.count)()
            stat['nb_msg_news_par_annee'] = await self.get_stat_annee(liste=requete, critere="date")

            # Nombre de tracabillite envoyées
            requete = await get_enveloppe('tracabilite')
            requete = await filter_annee(requete, "date")
            stat['nb_msg_tracabilite'] = await sync_to_async(requete.count)()
            stat['nb_msg_tracabilite_par_annee'] = await self.get_stat_annee(liste=requete, critere="date")

            # Nombre de forum envoyées
            requete = await get_enveloppe('forum')
            requete = await filter_annee(requete, "date")
            stat['nb_msg_forum'] = await sync_to_async(requete.count)()
            stat['nb_msg_forum_par_annee'] = await self.get_stat_annee(liste=requete, critere="date")

            # Nombre d'emails envoyés
            requete = await sync_to_async(Email.objects.all)()
            requete = await filter_annee(requete, "created")
            stat['nb_email'] = await sync_to_async(requete.count)()
            stat['nb_email_par_annee'] = await self.get_stat_annee(liste=requete, critere="created")


            return render(request, 'stat/stat_messagerie.html', context={"stat": stat})


class AbsenceInstructionMairieCount(View):

    """Retourne le nombre de messages envoyés pour des instructions en mairie impossible"""

    @method_decorator(login_required)
    def get(self, request):
        if self.request.session['o_support'] or self.request.user.has_group("Administrateurs d\'instance"):
            annee = request.GET.get('annee', None)
            instance_pk = request.GET.get('instance', None)
            message_qs = Message.objects.filter(message_enveloppe__objet="Instruction en mairie impossible").distinct()
            if self.request.session['o_support']:
                instance = Instance.objects.filter(pk=instance_pk).first()
            else:
                instance = self.request.user.default_instance
            if annee or instance:
                if instance:
                    message_qs = message_qs.filter(message_enveloppe__instance=instance)
                if annee:
                    message_qs = message_qs.filter(message_enveloppe__date__year=annee)
            message_envoie_count = message_qs.count()
            message_pas_agent_count = message_qs.filter(corps__contains="Pas de compte agent créé.").count()
            message_pas_agent_actif_count = message_qs.filter(corps__contains="Compte agent inactif").count()
            if annee or instance_pk:
                count = {
                    'message_envoie_count': message_envoie_count,
                    'message_pas_agent_count': message_pas_agent_count,
                    'message_pas_agent_actif_count': message_pas_agent_actif_count,
                }
                return JsonResponse(count, safe=False)
            liste_annee_archives = []
            annee_archives = 2022
            while annee_archives <= timezone.now().year:
                liste_annee_archives.append(str(annee_archives))
                annee_archives += 1
            if self.request.session['o_support']:
                instances = Instance.objects.order_by('departement')
            else:
                instances = None
            context = {
                'message_envoie_count': message_envoie_count,
                'message_pas_agent_count': message_pas_agent_count,
                'message_pas_agent_actif_count': message_pas_agent_actif_count,
                'instances': instances,
                'liste_annee_archives': liste_annee_archives
            }
            return render(request, "compteur_absence_instruction_mairie.html", context)
        else:
            return render(self.request, "core/access_restricted.html",
                          {'message': "Vous n'avez pas accès a cette page"}, status=403)
