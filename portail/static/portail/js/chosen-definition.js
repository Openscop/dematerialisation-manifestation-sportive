$(document).ready(function () {

    // To style all <select>s
    $('.field-role select').attr("class", "selectchosen")
    $('.field-departements select').attr("class", "selectchosen")
    $('.selectchosen').chosen({
        placeholder_text_multiple: 'Laissez vide pour tous',
        no_results_text: "Aucun résultat avec : ",
        width: "275px"
    });
    $('.field-role, .field-departements').css({overflow:'visible'});
    $('.field-departements div:nth-child(2)').removeClass('related-widget-wrapper');

});