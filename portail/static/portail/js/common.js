/**
 * Created by david on 11/05/16.
 */

function html_chargement_en_cours() {
    return '<div class="blocs-chargement-spinner alert alert-secondary w-50 m-auto text-center">Chargement en cours <i class="spinner ms-2 iconex2"></i></div>\n' +
        '<script>AfficherIcones();</script>'
}

function activer_tooltip() {
    $('[data-bs-toggle="tooltip"]').tooltip({'html': true, 'delay': { show: 500, hide: 100 }});
    $('[data-bs-toggle-quick="tooltip"]').tooltip({'html': true, 'delay': { show: 50, hide: 100 }});
}

function dateSorter(date1, date2) {
    var date12 = date1.split('/').reverse().join('');
    var date22 = date2.split('/').reverse().join('');
    if (date12 > date22) return 1;
    if (date12 < date22) return -1;
    return 0;
};

function scroll_to(selector) {
    var target_top = $("#searchform").offset().top - 50;
    $('html, body').animate({scrollTop: target_top}, 1500);
}