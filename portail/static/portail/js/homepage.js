/**
 * Created by david on 19/10/2017.
 */


    // bof Gestion de la carte de France
    try {
        map = new jvm.Map({
            container: $('#carte-fr'),
            map: 'fr_merc',
            backgroundColor: '#015a70',
            regionsSelectable: true,
            regionStyle: {
                initial: {
                  fill: '#d5d5d5'
                },
                hover: {
                    fill: '#1b3642'
                },
                selected: {
                    fill: '#ffffff'
                },
            },
            onRegionClick: function (e, code) {
                if (typeof instances[code] !== 'undefined') {
                    window.location = instances[code];
                }
                else {
                    var departement = map.mapData.paths[code].name;
                    var myModal = new bootstrap.Modal(document.getElementById('myModal'), {
                        keyboard: false
                    })
                    $('#myModal .modal-title').text(departement);
                    $('#myModal .modal-body').html("Ce département ne propose pas la dématérialisation des demandes sur cette plateforme.<br>" +
                        "Nous vous invitons à vous inscrire pour nous permettre de vous tenir informé dès que la situation évolue...");
                    myModal.show();
                }
            },
            onRegionTipShow: function (e, el, code) {
                if (typeof instances[code] !== 'undefined') {
                    if (instances[code + '-etat'] == 'ouvert') {
                        el.html('<center>' + el.html() + '<br> - <strong>opérationnel</strong> - <br><em>à disposition des organisateurs</em></center>');
                    } else if (instances[code + '-etat'] == 'test') {
                        el.html('<center>' + el.html() + '<br> - <strong>en cours de test</strong> - <br><em>quelques jours de patience...</em></center>');
                    } else if (instances[code + '-etat'] == 'deploiement') {
                        el.html('<center>' + el.html() + '<br> - <strong>en cours de déploiement</strong> - <br><em>quelques semaines de patience...</em></center>');
                    } else if (instances[code + '-etat'] == 'ferme') {
                        el.html('<center>' + el.html() + '<br> - <strong>fin 2022 / 2023</strong>- <br><em>déploiement dans les prochains mois</em></center>');
                    }
                } else {
                    el.html('<center>' + el.html() + '<br> - <strong>à venir</strong>- <br><em>déploiement en cours d\'étude</em></center>');
                }
            }
        });

        for (var code in map.regions) {
            if (typeof instances[code] !== 'undefined') {
                region = map.regions[code]
                map.clearSelectedRegions();
                if (instances[code + '-etat'] == 'ouvert') {
                    region.element.config.style.initial.fill = "#a3ad01"
                } else if (instances[code + '-etat'] == 'test') {
                    region.element.config.style.initial.fill = "#8dc197"
                } else if (instances[code + '-etat'] == 'deploiement') {
                    region.element.config.style.initial.fill = "#85b6be"
                } else if (instances[code + '-etat'] == 'ferme') {
                    region.element.config.style.initial.fill = "#cfcfcf"
                } else {
                    region.element.config.style.initial.fill = "#767676"
                }
                region.element.setStyle();
            }
        }

        if (dep_number != "NATIONALE") {
            map.setSelectedRegions("FR-" + dep_number);
        }

    } catch (e) {

    }
    // eof Gestion de la carte de France


    // bof compteur des stats animation
    function animateValue(obj, start, end, duration) {
        var range = end - start;
        var minTimer = 50;
        var stepTime = Math.abs(Math.floor(duration / range));
        stepTime = Math.max(stepTime, minTimer);
        var startTime = new Date().getTime();
        var endTime = startTime + duration;
        var timer;
        function run() {
            var now = new Date().getTime();
            var remaining = Math.max((endTime - now) / duration, 0);
            var value = Math.round(end - (remaining * range));
            obj.text(value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "\u00A0 "));
            if (value == end) {
                clearInterval(timer);
                obj.animate({'zoom': 1.08}, 400).delay(250).animate({'zoom': 1}, 400)
            }
        }
        timer = setInterval(run, stepTime);
        run();
    }
    $('.stat_result').each(function (index){
        var duration = 1000 + index * 250
        animateValue($(this), parseInt($(this).attr('data-start')), parseInt($(this).attr('data-end')), duration)
    })
    // eof