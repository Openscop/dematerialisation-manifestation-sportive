$('.sort').on('click', function (){
    $('.sort').each(function (){
        $(this).find('i').removeClass('fa-sort-down fa-sort-up').addClass('fa-sort text-muted')
    })
    if($(this).hasClass('sort-asc')){
        $(this).addClass('sort-desc').removeClass('sort-asc')
        $(this).find('i').removeClass('fa-sort-down text-muted').addClass('fa-sort-up')
    }
    else {
        $(this).addClass('sort-asc').removeClass('sort-desc')
        $(this).find('i').removeClass('fa-sort fa-sort-up text-muted').addClass('fa-sort-down')
    }
})