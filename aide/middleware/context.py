# coding: utf-8

from aide.models.context import ContextHelp


class ContextHelpMiddleware:
    """
    Middleware d'aide contextuelle

    Middleware qui remplace les nœuds du DOM possédant un id débutant
    par help-, en y adjoignant un contenu d'aide contextuelle.

    Cette méthode est une alternative à la version Tooltip + AJAX
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        contexthelps = []

        # récupère les aides contextuelles définies pour la page demandée
        if hasattr(request.resolver_match, 'url_name') and request.resolver_match.url_name:
            contexthelps = [contexthelp for contexthelp in
                            ContextHelp.objects.filter(page_names__contains=request.resolver_match.url_name) if
                            contexthelp.is_visible(request)]
        # récupère et ajoute les aides contextuelles définies sans page spécifique
        # contexthelps += [contexthelp for contexthelp in
        #                 ContextHelp.objects.filter(page_names__exact='').exclude(positions__exact='') if
        #                 contexthelp.is_visible(request)]

        for contexthelp in ContextHelp.objects.filter(page_names__exact=''):
            contexthelps.append(contexthelp) if contexthelp.is_visible(request) else ""

        request.contexthelps = contexthelps
        return self.get_response(request)
