# coding: utf-8
import html

from django.views.decorators.csrf import csrf_exempt
from django.views import View
from django.http.response import Http404, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.db.models import Q

from aide.models.context import ContextHelp
from aide.models.aide import PageAide, PanneauPageAide, OngletPageAide
from carto.serializers import PoiCategorieApiSerializer, PoiApiSerializer
from carto.services.repository import getPoisList


def get_groups(user):
    # Renvoyer la liste des rôles et des groupes d'un utilisateur
    if user.is_anonymous:
        user_groupes = []
    else:
        user_groupes = [str(id) for id in list(user.groups.values_list('id', flat=True))]
    return user_groupes


def check_affichage_elements(user, query):
    # Vérifier les permissions d'accès aux éléments de la page (notes ou panneaux)
    user_groupes = get_groups(user)
    if user.is_anonymous:
        # Récupère les éléments qui n'ont pas de role et pas de département
        elements = query.filter(active=True).filter(groupe="").filter(organisation="").filter(departements__isnull=True)

    else:
        if not user.has_group('_support'):
            # Renvoie chaine "national" pour les utilisateurs d'instance nationale ce qui revient à bloquer les éléments qui ont un département assigné
            user_departement = user.get_instance().get_departement() is not None and user.get_instance().get_departement().name or "national"

            # Récupère les éléments qui ont un département indéfinie ou un département conforme à l'utilisateur
            elements = query.filter(active=True).filter(
                Q(departements__isnull=True) | Q(departements__name__contains=user_departement))

            # Retourne True si pas d'organisation ou que le (ou les) organisation
            # de l'élément sont en intersection avec le (ou les) organisation de l'utilisateur
            def check_organisation(element):
                if element.organisation == "":
                    return True
                for organisation in element.organisation:
                    if organisation == "service_consulte" and user.is_agent_service_consulte():
                        return True
                    if organisation == "service_instructeur" and user.is_agent_service_instructeur():
                        return True
                    if organisation == "structure" and user.is_organisateur():
                        return True
                return False

            # Retourne True si pas de groupe ou que le (ou les) groupe de l'élément sont en intersection avec le (ou les) groupe de l'utilisateur
            def check_groupe(user_groupes, element):
                return (element.groupe == "" or bool(
                    set(user_groupes).intersection(set([id for id in element.groupe.split(',') if id]))))

            # Ne conserver les éléments que si role de l'élément indéfinie ou role de l'élément conforme aux roles de l'utilisateur
            elements_list = [element for element in elements if check_organisation(element)]
            elements_list = [element for element in elements_list if check_groupe(user_groupes, element)]
            for element in elements:
                if element not in elements_list:
                    elements = elements.exclude(id=element.id)
        else:
            elements = query.filter(active=True)

    return elements


def check_page(user, page):
    # Vérifier les permissions d'accès de la page et renvoyer la page conforme aux permissions de l'utilisateur
    # Sans permissions, renvoie d'une chaine pour la cause du refus

    user_groupes = get_groups(user)
    help_organisation = page.organisation
    help_groupes = [id for id in page.groupe.split(',') if id]
    departements = [dept.name for dept in page.departements.all()]
    # Pas d'accès aux visiteurs dans les cas suivants
    if user.is_anonymous:
        if page.authenticated_only or help_organisation or departements or help_groupes:
            return "Vous devez être connecté pour avoir accès à cette page"
    elif not user.has_group('_support'):
        user_departement = user.get_instance().get_departement_name()
        # Si le contenu cible des groupes particuliers, filtrer l'accès
        if help_groupes:
            if not bool(set(user_groupes).intersection(set(help_groupes))):
                return "Cette page ne vous est pas destinée (groupe)"
        # Si le contenu cible des rôles particuliers, filtrer l'accès
        if help_organisation:
            for organisation in help_organisation:
                if organisation == "service_consulte" and not user.is_agent_service_consulte():
                    return "Cette page ne vous est pas destinée"
                if organisation == "service_instructeur" and not user.is_agent_service_instructeur():
                    return "Cette page ne vous est pas destinée"
                if organisation == "structure" and not user.is_organisateur():
                    return "Cette page ne vous est pas destinée"
        # Si le contenu cible des départements particuliers, filtrer l'accès
        if departements:
            if user_departement not in departements:
                return "Cette page ne concerne pas votre département"
    return page


class PanelAjaxView(View):

    def get(self, request, pk):
        onglet = get_object_or_404(OngletPageAide, pk=pk)
        panels = check_affichage_elements(request.user, onglet.panels.all().order_by("paneltabs__panel_order"))
        page = onglet.page
        focus = int(request.GET.get('focus', "")) if request.GET.get('focus', "").isdigit() else ""
        return render(request, 'aide/panel_ajax.html',
                      {"panels": panels, "focus": focus, "page": page, "group": onglet})


@csrf_exempt
def page_aide(request, path, pk=None):
    """ Trouver une page d'aide ou lever un HTTP404 """

    user = request.user
    user_groupes = get_groups(user)

    if request.method == "POST":
        # Gestion du champ de recherche des pages d'aide
        if 'recherche' in request.POST and request.POST['recherche']:
            recherche = request.POST['recherche']
            recherche = recherche.replace("<", "&lt;").replace(">", "&gt;")
            # Modifications de la page sommaire
            page = PageAide()
            page.contentbefore = '<div class="help-page"><h2>recherche avec le mot clé : ' + recherche + "</h2></div>"

            # Recherche dans les pages d'aide
            liste_pages = PageAide.objects.filter(active=True).filter(Q(title__unaccent__icontains=recherche) |
                                                                      Q(contentbefore__unaccent__icontains=recherche) |
                                                                      Q(contentafter__unaccent__icontains=recherche))
            # Filtrer les pages en fonction des permissions
            liste_pages = [page for page in liste_pages if type(check_page(user, page)) is not str]
            # Recherche dans les pages simples avec panneaux
            liste_panels = PageAide.objects.filter(active=True).filter(
                Q(onglet__panels__title__unaccent__icontains=recherche) |
                Q(onglet__panels__content__unaccent__icontains=recherche)).distinct()
            for page_panels in liste_panels:
                # Filtrer les panneaux en fonction des permissions
                panels = check_affichage_elements(user, page_panels.get_panels())
                # Refaire la recherche avec les panneaux restants. Si le mot clé n'est plus présent, enlever la page
                if not panels.filter(
                        Q(title__unaccent__icontains=recherche) | Q(content__unaccent__icontains=recherche)).exists():
                    liste_panels = liste_panels.exclude(id=page_panels.id)
            liste = liste_pages + list(liste_panels)

            # Modifications de la page sommaire
            for match in liste:
                page.contentafter += '&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-minus" style="color:#015a70;">' \
                                     '</i>&nbsp;&nbsp;&nbsp;&nbsp;<a href="' + match.path + '">' + match.title + "</a><br>"
            template_path = 'aide/page-aide.html'
            return render(request, template_path, {'page': page,
                                                   'groups': [],
                                                   'search': True,
                                                   'count': 0})
    try:
        page = PageAide.objects.get(path=path)
    except PageAide.DoesNotExist:
        raise Http404("Pas de page d'aide pour cette URL : " + path)

    # Si get_for_url n'a pas renvoyé de 404, effectuer le rendu

    # Vérifier les permissions d'accès à la page
    if page.active is False:
        raise Http404("La page d'aide n'est plus active")

    retour_check = check_page(user, page)
    if type(retour_check) is str:
        raise PermissionDenied(retour_check)

    if pk:
        pk = int(pk)
    try:
        activ = page.get_tabs()[0].title
        if request.GET.get('tab', None) and request.GET['tab'].isdigit():
            activ = OngletPageAide.objects.filter(pk=int(request.GET['tab'])).first().title
        elif pk:
            activ = PanneauPageAide.objects.filter(pk=pk).first().tabs.first().title
    except:
        activ = None
    # Filtrer la liste des panels en fonction des permissions
    panels = check_affichage_elements(user, page.get_panels())
    # tabs = page.get_tabs().values_list('title', flat=True)
    # Générer la liste des onglets en supprimant ceux qui n'ont pas de panneaux visibles
    tabs = []
    for tab in page.get_tabs():
        for panel in tab.panels.all():
            if panel in panels:
                tabs.append(tab)
                break
    # Ouvrir directement le panneau s'il est seul
    if len(tabs) == 1 and len(panels) == 1:
        pk = panels.get().pk
    # Incrémenter le compteur de vues du panneau demandé si toujours dans la liste
    if pk:
        try:
            panel = panels.get(pk=pk)
            panel.nbr_vues += 1
            panel.save()
        except:
            pass

    # Filtrer la liste des notes en fonction des permissions
    notes = check_affichage_elements(user, page.notes)
    # Si la page doit être incorporée dans une div (cerfa dans recherche_création_formulaire)
    # Le paramètre "embed" est ajouté à l'url (pas de panneaux affichés dans texte-aide.html)
    if request.GET.get('embed'):
        template_path = 'aide/texte-aide.html'
    else:
        template_path = 'aide/page-aide.html'
    response = render(request, template_path, {'page': page,
                                               'groups': tabs,
                                               'count': len(tabs),
                                               'activ_tab': activ,
                                               'focus': pk,
                                               'user_groupes': user_groupes,
                                               'notes': notes})
    page.nbr_vues += 1
    page.save()
    return response


def liste_pages_aide(request):
    """ Afficher la liste des pages d'aide """
    pages = PageAide.objects.all().order_by('title')
    return render(request, 'aide/liste-pages-aide.html', {'pages': pages})


def view_context_help(request, name):
    """ Contenu des aides contextuelles, à récupérer en AJAX """
    ctx = ContextHelp.objects.for_identifier(name)
    user = request.user
    ctx = check_affichage_elements(user, ctx)
    if ctx.exists():
        for help in ctx:
            help.nbr_vues += 1
            help.save()
        contents = "<br>".join(ctx.values_list('text', flat=True))
        response = HttpResponse(contents)
        response['X-Robots-Tag'] = 'noindex'
        return response
    raise Http404()


def AjoutVuePanelAjax(request, pk=None):
    """ Vue pour incrémenter le nombre de vues des panneaux par un appel ajax """
    if pk:
        try:
            panel = PanneauPageAide.objects.get(pk=pk)
        except PanneauPageAide.DoesNotExist:
            raise Http404()
        panel.nbr_vues += 1
        panel.save()
        return HttpResponse()


def view_pois_help(request):
    context = {}
    page = {'title': 'Pois'}
    context['page'] = page
    context['pois'] = getPoisList()
    for poi in context['pois']:
        # on ajoute une clé sort pour que la valeur soit correcte
        if poi.id.isnumeric():
            poi.sort = poi.id.zfill(3)
        else:
            poi.sort = poi.id
        if poi.html_code:
            poi.html_code = html.unescape(poi.html_code + ";")
    return render(request, "aide/carto_pois.html", context)
