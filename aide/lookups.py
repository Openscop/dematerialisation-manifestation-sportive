# coding: utf-8
from ajax_select import register, LookupChannel
from django.db.models import Q
from unidecode import unidecode

from .models import OngletPageAide, PageAide

@register('tabs')
class TabLookup(LookupChannel):
    """ Lookup AJAX des Panneaux de page d'aide """

    # Configuration
    model = OngletPageAide

    # Overrides
    def get_query(self, q, request):
        q = unidecode(q)
        # Recherche générale sur le titre
        query = self.model.objects.filter(Q(title__icontains=q) | Q(page__title__icontains=q)).order_by('title')[:20]
        return query

    def format_item_display(self, item):
        return u"<span class='tag'>%s -> %s</span>" % (item.page.title, item.title)

    def format_match(self, item):
        return "<span class='tag'>{page} -> {title}</span>".format(title=item.title, page=item.page.title)


@register('PageAide')
class PageAideLookup(LookupChannel):
    """ Lookup AJAX des Panneaux de page d'aide """

    # Configuration
    model = PageAide

    # Overrides
    def get_query(self, q, request):
        q = unidecode(q)
        # Recherche générale sur le titre
        query = self.model.objects.filter(Q(title__icontains=q)).order_by('title')[:20]
        return query

    def format_item_display(self, item):
        return u"<span class='tag'>%s</span>" % (item.title)

    def format_match(self, item):
        return "<span class='tag'>{title} </span>".format(title=item.title)
