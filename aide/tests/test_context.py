# coding: utf-8
from django.test import TestCase
from django.utils import timezone
from django.contrib.auth.hashers import make_password as make

from core.factories import UserFactory
from administrative_division.factories import CommuneFactory
from aide.models.context import ContextHelp


class ContextTests(TestCase):
    """ Tester les fonctionnalités des aides contextuelles """

    def test_identifier_detection(self):
        """
        Test des identifiants

        Teste qu'un identifiant (ex ctx-help-foo ctx-help-bar)
        renvoie bien les aides contextuelles avec les noms foo
        et bar.
        """
        ctx1 = ContextHelp.objects.create(name='foo', text="Foo")
        ctx2 = ContextHelp.objects.create(name='bar', text="Bar")
        self.assertEquals(ContextHelp.objects.for_identifier('ctx-help-foo ctx-help-bar').count(), 2)
        self.assertEquals(ContextHelp.objects.for_identifier('ctx-help-foo').count(), 1)
        self.assertEquals(ContextHelp.objects.for_identifier('ctx-help-foo-bar').count(), 0)
        self.assertTrue(ctx1 in ContextHelp.objects.for_identifier('ctx-help-foo'))
        self.assertTrue(ctx2 in ContextHelp.objects.for_identifier('ctx-help-bar'))

    def test_role_detection(self):
        """
        Test des rôles

        Vérifier que les rôles auxquels est assignée une aide contextuelle
        peuvent voir cette aide, et seulement eux.
        """
        com = CommuneFactory.create()
        pref = com.arrondissement.organisations.first()
        ddsp = com.arrondissement.departement.organisation_set.get(precision_nom_s="DDSP")
        ctx1 = ContextHelp.objects.create(name='foo', text="Foo", organisation=['service_instructeur'])
        ctx2 = ContextHelp.objects.create(name='bar', text="Bar")
        yesterday = timezone.now() - timezone.timedelta(days=2)
        agent = UserFactory.create(username='agent', password=make("123"),
                                               default_instance=com.arrondissement.departement.instance, date_joined=yesterday)
        agent.organisation_m2m.add(ddsp)
        instructeur = UserFactory.create(username='instructeur', password=make("123"),
                                               default_instance=com.arrondissement.departement.instance, date_joined=yesterday)
        instructeur.organisation_m2m.add(pref)
        # Normalement, un instructeur peut voir l'aide, mais pas un agent
        self.assertFalse(ctx1.is_visible(agent))
        self.assertTrue(ctx1.is_visible(instructeur))
        # Pas d'organisation renseigné donc les deux peuvent le voire
        self.assertTrue(ctx2.is_visible(agent))
        self.assertTrue(ctx2.is_visible(instructeur))
