# Generated by Django 4.0.6 on 2022-07-12 16:15

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('aide', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('administrative_division', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='demande',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='demandeur', to=settings.AUTH_USER_MODEL, verbose_name='utilisateur'),
        ),
        migrations.AddField(
            model_name='contexthelp',
            name='departements',
            field=models.ManyToManyField(blank=True, to='administrative_division.departement', verbose_name='Départements'),
        ),
    ]
