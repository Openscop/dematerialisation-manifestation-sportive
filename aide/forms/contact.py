# coding: utf-8
import html
from lxml.html.clean import Cleaner
from django import forms
from ckeditor.widgets import CKEditorWidget
from crispy_forms.layout import Layout, Fieldset
from core.forms.base import GenericForm
from django.core.exceptions import ValidationError

from aide.models import Demande


class DemandeForm(GenericForm):
    """ Formulaire de contact """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(DemandeForm, self).__init__(*args, **kwargs)
        self.fields['code'] = forms.CharField(max_length=5, label='Code postal concerné' , help_text='Entrez le code postal de la commune (ou de l\'une des communes) concernée(s).')
        self.helper.layout = Layout(Fieldset(*["Demande", 'departement', 'code', 'contenu', 'email', 'nom', 'prenom']))
        self.helper.field_class = 'col-sm-8'
        self.helper.form_tag = False

    def clean_code(self):
        dep = self.cleaned_data['departement'].name
        if self.cleaned_data['code'][:2] != dep and self.cleaned_data['code'][:3] != dep:
            raise ValidationError("le code postal ne correspond pas au département !")
        return self.cleaned_data['code']

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        cleaner = Cleaner()
        cleaner.javascript = True
        style_autorise = frozenset(['style'])
        cleaner.safe_attrs = cleaner.safe_attrs.union(style_autorise)
        if self.cleaned_data['contenu']:
            self.instance.contenu = cleaner.clean_html(html.unescape(self.cleaned_data['contenu']))
        return super().save(commit=commit)

    class Meta:
        model = Demande
        exclude = ["status", "date", "user"]
        widgets = {'contenu': CKEditorWidget(config_name='minimal')}


class DemandeAdminForm(GenericForm):
    """ Gestion des demandes de contact """

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        cleaner = Cleaner()
        cleaner.javascript = True
        style_autorise = frozenset(['style'])
        cleaner.safe_attrs = cleaner.safe_attrs.union(style_autorise)
        if self.cleaned_data['contenu']:
            self.instance.contenu = cleaner.clean_html(html.unescape(self.cleaned_data['contenu']))
        return super().save(commit=commit)

    class Meta:
        model = Demande
        exclude = []
        widgets = {'contenu': CKEditorWidget(config_name='minimal')}
