# coding: utf-8
import html
from lxml.html.clean import Cleaner
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.forms import models
from django.forms.fields import TypedMultipleChoiceField
from django.utils import timezone
from django.contrib.auth.models import Group
from ajax_select.fields import AutoCompleteSelectMultipleField
from django.forms.fields import MultipleChoiceField
from django.forms.widgets import CheckboxSelectMultiple


from aide.models.context import ContextHelp
from aide.models.aide import PageAide, PanneauPageAide, OngletPageAide, NotePageAide, PanelTabs


class PageAideFormSuperUser(models.ModelForm):
    """ Formulaire admin des pages d'aide """
    model = PageAide

    # Champs
    organisation = MultipleChoiceField(choices=ContextHelp.ORGANISATION_TYPE, widget=CheckboxSelectMultiple(),
                                       required=False,
                                       label="Organisation")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groupe'] = TypedMultipleChoiceField(choices=list(Group.objects.all().values_list('id', 'name')),
                                                         required=False, label="Groupes")

        if 'groupe' in self.initial and self.initial['groupe']:
            self.initial['groupe'] = self.initial['groupe'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        if not self.cleaned_data['organisation']:
            self.instance.organisation = ""
        self.instance.groupe = ','.join(sorted(self.cleaned_data['groupe']))
        if not self.cleaned_data['organisation']:
            self.instance.organisation = ""
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'contentbefore': CKEditorUploadingWidget(), 'contentafter': CKEditorUploadingWidget()}
        labels = {
            'get_contentbefore_html': 'Contenu avant',
            'get_contentafter_html': 'Contenu après',
        }
        

class PageAideForm(PageAideFormSuperUser):
    
    def save(self, commit=True):

        cleaner = Cleaner()
        cleaner.javascript = True
        style_autorise = frozenset(['style'])
        cleaner.safe_attrs = cleaner.safe_attrs.union(style_autorise)
        if 'contentbefore' in self.cleaned_data and self.cleaned_data['contentbefore']:
            self.instance.contentbefore = cleaner.clean_html(html.unescape(self.cleaned_data['contentbefore']))
        if 'contentafter' in self.cleaned_data and self.cleaned_data['contentafter']:
            self.instance.contentafter = cleaner.clean_html(html.unescape(self.cleaned_data['contentafter']))
        return super().save(commit=commit)


class PanneauPageAideFormSuperUser(models.ModelForm):
    """ Formulaire admin des panneaux de page d'aide """
    model = PanneauPageAide

    # Champs
    organisation = MultipleChoiceField(choices=ContextHelp.ORGANISATION_TYPE, widget=CheckboxSelectMultiple(),
                                       required=False,
                                       label="Organisation")
    tabs = AutoCompleteSelectMultipleField('tabs', required=False, help_text=None)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groupe'] = TypedMultipleChoiceField(choices=list(Group.objects.all().values_list('id', 'name')),
                                                         required=False, label="Groupes")
        if 'groupe' in self.initial and self.initial['groupe']:
            self.initial['groupe'] = self.initial['groupe'].split(',')

    def clean(self):
        cleaned_data = super().clean()
        # Le champ de django-ajax-selects utilisé pour 'tabs' ne renvoie que des ids
        # alors que les données initiales sont des objets 'tab'
        # du coup, une nouvelle entrée dans la table de jonction est créée et on perd la position du panneau
        # convertir les données pour que le formulaire ne les considère pas changées
        if 'tabs' in self.initial and 'tabs' in self.changed_data:
            if cleaned_data['tabs'] == [str(tab.id) for tab in self.initial['tabs']]:
                self.changed_data.remove('tabs')
        cleaned_data['tabs'] = list(OngletPageAide.objects.filter(id__in=cleaned_data['tabs']))
        return cleaned_data

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.updated = timezone.now()
        if not self.cleaned_data['organisation']:
            self.instance.organisation = ""
        self.instance.groupe = ','.join(sorted(self.cleaned_data['groupe']))
        if not self.cleaned_data['organisation']:
            self.instance.organisation = ""
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'content': CKEditorUploadingWidget()}


class PanneauPageAideForm(PanneauPageAideFormSuperUser):

    def save(self, commit=True):
        cleaner = Cleaner()
        cleaner.javascript = True
        style_autorise = frozenset(['style'])
        cleaner.safe_attrs = cleaner.safe_attrs.union(style_autorise)
        if self.cleaned_data['content']:
            self.instance.content = cleaner.clean_html(html.unescape(self.cleaned_data['content']))
        return super().save(commit=commit)


class NotePageAideForm(models.ModelForm):
    """ Formulaire admin des nouveautés """
    model = NotePageAide

    # Champs
    organisation = MultipleChoiceField(choices=ContextHelp.ORGANISATION_TYPE, widget=CheckboxSelectMultiple(),
                                       required=False,
                                       label="Organisation")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groupe'] = TypedMultipleChoiceField(choices=list(Group.objects.all().values_list('id', 'name')),
                                                         required=False, label="Groupes")
        self.fields['departements'].help_text = None
        if 'groupe' in self.initial and self.initial['groupe']:
            self.initial['groupe'] = self.initial['groupe'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        if not self.cleaned_data['organisation']:
            self.instance.organisation = ""
        self.instance.groupe = ','.join(sorted(self.cleaned_data['groupe']))
        if not self.cleaned_data['organisation']:
            self.instance.organisation = ""
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'content': CKEditorUploadingWidget()}


class PanelTabsInLineForm(models.ModelForm):
    """ Formulaire admin des panneaux inline """
    model = PanelTabs

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['panel'].queryset = self.fields['panel'].queryset.order_by('title')

    class Meta:
        exclude = []
