# coding: utf-8
import html
from lxml.html.clean import Cleaner
from ckeditor.widgets import CKEditorWidget
from django.forms import models
from django.forms.fields import MultipleChoiceField
from django.forms.widgets import CheckboxSelectMultiple, Textarea

from aide.models.context import ContextHelp


class ContextHelpForm(models.ModelForm):
    """ Formulaire admin des nouveautés """
    model = ContextHelp

    # Champs
    organisation = MultipleChoiceField(choices=ContextHelp.ORGANISATION_TYPE, widget=CheckboxSelectMultiple(), required=False,
                                    label="Organisation")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'organisation' in self.initial:
            self.fields['organisation'].initial = self.initial['organisation']

    def save(self, commit=True):
        """Sauvegarde de l'objet"""

        cleaner = Cleaner()
        cleaner.javascript = True
        style_autorise = frozenset(['style'])
        cleaner.safe_attrs = cleaner.safe_attrs.union(style_autorise)
        if self.cleaned_data['text']:
            self.instance.text = cleaner.clean_html(html.unescape(self.cleaned_data['text']))
        if not self.cleaned_data['organisation']:
            self.instance.organisation = ""
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'text': CKEditorWidget(), 'page_names': Textarea, 'positions': Textarea}
