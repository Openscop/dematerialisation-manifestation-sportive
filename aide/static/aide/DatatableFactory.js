$.fn.dataTable.Api.register('filtersOn()', function () {

        const dataTable = this;
        const id = $(dataTable.context[0].nTable).attr('id');
        const state = dataTable.state.loaded();

        // Create the filter header row
        $('#' + id + ' thead').after(
            $('<thead />').addClass('filter').append($('<tr />'))
        );

        // Populate the filter header
        $('#' + id + ' thead th').each(function (index) {
            const searchable = dataTable.context[0].aoColumns[index].bSearchable;
            const searchtype = dataTable.context[0].aoColumns[index].searchtype;
            const tablabel = [];
            // Add input only if current column is searchable
            if (searchable) {

                if (searchtype == "select") {

                    // Select input
                    // "<span class=\"badge bg-light icone delai-30j\" title=\"\" data-bs-toggle=\"tooltip\" data-original-title=\"Au delà de 30 jours avant le début de la manifestation\"> <i class=\"far fa-stopwatch\"></i> +30j&nbsp; <i class=\"fas fa-signal-alt text-info\"></i> </span>"
                    const select = $('<select ></select>').addClass('form-select filter-select');
                    select.append('<option value="">' + dataTable.context[0].aoColumns[index].sTitle + '</option>');
                    dataTable.column(index).data().unique().sort().each(function (d, j) {

                        if (/^<./.test(d) === true) {
                            label = $(d).find('span[hidden]').text();
                            if (label == "") {
                                label = $(d).siblings('span[hidden]').text();
                            }
                        } else {
                            label = d;
                        }
                        let found = tablabel.find(function (element) {
                            return label === element
                        })

                        if (!found) {
                            select.append('<option value="' + label + '">' + label + '</option>')
                            tablabel.push(label);
                        }
                    });

                    $('#' + id + ' .filter tr').append($('<th>').append(select));
                } else if (searchtype === 'disabled') {
                    $('#' + id + ' .filter tr').append($('<th>').append($('<input type="text" disabled/>').addClass('form-control input-sm')));
                } else {
                    // Text input
                    $('#' + id + ' .filter tr').append($('<th>').append($('<input type="text"/>').addClass('form-control input-sm')));
                }
            } else {
                $('#' + id + ' .filter tr').append($('<th>'));
            }
        });

        // Restore filter (only if stateSave)
        if (state) {
            dataTable.columns().eq(0).each(function (index) {
                const colSearch = state.columns[index].search;
                if (colSearch.search) {
                    // Restore input value
                    $('#' + id + ' .filter input').get(index).value = colSearch.search;
                }
            });
        }

        // Filter input event : filter matching column
        $('#' + id + ' .filter input, #' + id + ' .filter select').each(function (index) {
            $(this).on('keyup change', function () {
                dataTable.column(index).search(this.value).draw();
            });
        });

        return dataTable;

    });

$.fn.dataTable.Api.register('filtersClear()', function () {

        const dataTable = this;
        const id = $(dataTable.context[0].nTable).attr('id');
        const state = dataTable.state.loaded();

        // Clean filters (only if stateSave)
        if (state) {
            $('#' + id + ' .filter input, #' + id + ' .filter select').each(function (index) {
                // Clear input value
                this.value = '';
                // Clear column filter
                dataTable.column(index).search('');
            });

            // Re-draw datatable
            dataTable.draw();
        }

    });

const createDatatable = (selector, options, searchSelector, filterBtnSelector) => {
        $(selector).DataTable(options).filtersOn()
        $(`${selector} thead.filter`).hide();
        $(`${selector}_filter`).hide();
        $(searchSelector).on("keyup", function () {
            const value = $(this).val().toLowerCase();
            $(`${selector} tbody tr`).filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $(filterBtnSelector).on('click', function () {
            $(`${selector} thead.filter`).toggle();
            $(`${selector} thead:first-child`).toggle();
        })
    }