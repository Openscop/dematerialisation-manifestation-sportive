from django.db import models
from django.conf import settings
from django.utils import timezone

class CptMsg(models.Model):
    """
    filtre d'affichage custom des utilisateurs
    """

    utilisateur = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='cpt_msg')
    convocation = models.IntegerField(default=0)
    action = models.IntegerField(default=0)
    info_suivi = models.IntegerField(default=0)
    conversation = models.IntegerField(default=0)
    news = models.IntegerField(default=0)
    forum = models.IntegerField(default=0)

    def __str__(self):
        return self.utilisateur.username

    def update_cpt(self):
        user = self.utilisateur
        from messagerie.models import Enveloppe
        self.convocation = Enveloppe.objects.filter(
            destinataire_id=user, lu_datetime__isnull=True, lu_requis=True, type='convocation').exclude(expediteur_id=user).count()
        self.action = Enveloppe.objects.filter(
            destinataire_id=user, lu_datetime__isnull=True, lu_requis=True, type='action').exclude(expediteur_id=user).count()
        self.info_suivi = Enveloppe.objects.filter(
            destinataire_id=user, lu_datetime__isnull=True, lu_requis=True, type='info_suivi').exclude(expediteur_id=user).count()
        self.conversation = Enveloppe.objects.filter(
            destinataire_id=user, lu_datetime__isnull=True, lu_requis=True, type='conversation').exclude(expediteur_id=user).count()
        self.news = Enveloppe.objects.filter(
            destinataire_id=user, lu_datetime__isnull=True, lu_requis=True, type='news').exclude(expediteur_id=user).count()
        self.forum = Enveloppe.objects.filter(
            destinataire_id=user, lu_datetime__isnull=True, lu_requis=True, type='forum').exclude(expediteur_id=user).count()
        self.save()

    def last_day(self):
        user = self.utilisateur
        from messagerie.models import Enveloppe
        envel = Enveloppe.objects.filter(
            destinataire_id=user, lu_datetime__isnull=True, lu_requis=True, date__gte=(timezone.now()-timezone.timedelta(days=1))).exclude(
            expediteur_id=user)
        convocation = envel.filter(type='convocation').count()
        action = envel.filter(type='action').count()
        info_suivi = envel.filter(type='info_suivi').count()
        conversation = envel.filter(type='conversation').count()
        news = envel.filter(type='news').count()
        forum = envel.filter(type='forum').count()
        return convocation, action, info_suivi, conversation, news, forum
