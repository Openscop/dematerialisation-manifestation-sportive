import os
import random
import threading
import html

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.db import models
from django.conf import settings
from lxml.html.clean import Cleaner
from post_office import mail
from webpush import send_user_notification
from ckeditor.fields import RichTextField

from core.models import ConfigurationGlobale
from core.models.optionuser import OptionUser
from instructions.models import Avis, DocumentOfficiel
from evenements.models.manifestation import Manif
from structure.models.cdsr import PresentationCDSR, ReunionCDSR


class PushThread(threading.Thread):
    def __init__(self, user):
        self.user = user
        threading.Thread.__init__(self)

    def run(self):
        payload = {
            "head": "Manifestation Sportive",
            "body": "Vous avez reçu un nouveau message",
        }
        send_user_notification(user=self.user, payload=payload, ttl=1000)


class MessageQuerySet(models.QuerySet):

    def creer_et_envoyer(self, type_msg, expediteur, destinataires, titre, contenu, manifestation_liee=None,
                         objet_lie_nature=None, objet_lie_pk=0, reponse_a_pk=None, fichier=None):
        """
        :param type_msg: type de l'enveloppe (conversation, news, action,  info_suivi) cf : Enveloppe.LISTE_TYPE
        :param expediteur: model user si Null = plateforme
        :param destinataires: tableau avec des User ou des parents d'User ou des structures (qui ont un email associé).
        :param titre: text
        :param contenu: text brut ou html
        :param manifestation_liee: model manifestation concerné par le message
        :param objet_lie_nature: type de l'objet concerné par le message(dossier, piece_jointe, avis, preavis, arrete, recepisse)
         cf : Enveloppe.LISTE_DOC_OBJET
        :param objet_lie_pk: pk de l'objet concerné par le message
        :param reponse_a_pk: pk du message original de la conversation (si ce message est une réponse)
        :param fichier: path absolu du fichier à rajouter en pj
        :return:
        """

        # clean du html du message (enlever tout le js)
        cleaner = Cleaner()
        cleaner.javascript = True
        if contenu:
            contenu_clean = cleaner.clean_html(html.unescape(contenu))
        # creation du message (le corps et l'object (avis preavis ou doc associé)
        obj = None
        if objet_lie_pk:
            if objet_lie_nature == 'avis':
                obj = Avis.objects.get(pk=objet_lie_pk)
            elif objet_lie_nature == 'piece_jointe':
                from evenements.models import PieceJointe
                obj = PieceJointe.objects.get(pk=objet_lie_pk)
            elif objet_lie_nature in ['arrete', 'recepisse', 'notice_rd', 'document']:
                obj = DocumentOfficiel.objects.get(pk=objet_lie_pk)
            elif objet_lie_nature == 'presentation':
                obj = PresentationCDSR.objects.get(pk=objet_lie_pk)
            elif objet_lie_nature == 'reunion':
                obj = ReunionCDSR.objects.get(pk=objet_lie_pk)
            message = self.create(corps=contenu_clean, content_object=obj)
        else:
            message = self.create(corps=contenu_clean)

        if fichier:
            if objet_lie_nature == 'reunion' and obj:
                dossier_cdsr = f"{settings.MEDIA_ROOT}{obj.service_cdsr.instance_fk.get_departement_name()}/reunion_cdsr/{objet_lie_pk}"
                if not os.path.exists(dossier_cdsr):
                    os.makedirs(dossier_cdsr)
                nouveau_nom = f"{dossier_cdsr}/" + fichier.split('/')[-1].replace(' ', '').replace("'", '').replace('"', "")
                if os.path.exists(nouveau_nom):
                    nouveau_nom = f"{dossier_cdsr}/{round(random.random() * 10000000)}-" + fichier.split('/')[-1].replace(
                        ' ', '').replace("'", '').replace('"', "")
                os.system(f"mv \"{fichier}\" {nouveau_nom}")
                message.fichier = f"{obj.service_cdsr.instance_fk.get_departement_name()}/reunion_cdsr/{objet_lie_pk}{nouveau_nom.split(dossier_cdsr)[-1]}"
            elif objet_lie_nature == 'presentation' and obj:
                reunion_pk = obj.reunion_cdsr_fk.pk
                service_cdsr = obj.reunion_cdsr_fk.service_cdsr
                dossier_cdsr = f"{settings.MEDIA_ROOT}{service_cdsr.instance_fk.get_departement_name()}/reunion_cdsr/{reunion_pk}/presentation_cdsr/{objet_lie_pk}"
                if not os.path.exists(dossier_cdsr):
                    os.makedirs(dossier_cdsr)
                nouveau_nom = f"{dossier_cdsr}/" + fichier.split('/')[-1].replace(' ', '').replace("'", '').replace('"', "")
                if os.path.exists(nouveau_nom):
                    nouveau_nom = f"{dossier_cdsr}/{round(random.random() * 10000000)}-" + fichier.split('/')[-1].replace(
                        ' ', '').replace("'", '').replace('"', "")
                os.system(f"mv \"{fichier}\" {nouveau_nom}")
                message.fichier = f"{service_cdsr.instance_fk.get_departement_name()}/reunion_cdsr/{reunion_pk}/presentation_cdsr/{objet_lie_pk}{nouveau_nom.split(dossier_cdsr)[-1]}"
            elif manifestation_liee:
                dossier_manif = f"{settings.MEDIA_ROOT}{manifestation_liee.get_instance().get_departement_name()}/{manifestation_liee.pk}"
                if not os.path.exists(dossier_manif):
                    os.makedirs(dossier_manif)
                if not os.path.exists(dossier_manif+"/pj_msg"):
                    os.makedirs(dossier_manif+"/pj_msg")
                nouveau_nom = f"{dossier_manif}/pj_msg/" + fichier.split('/')[-1].replace(' ', '').replace("'", '').replace('"', "")
                if os.path.exists(nouveau_nom):
                    nouveau_nom = f"{dossier_manif}/pj_msg/{round(random.random()*10000000)}-" + fichier.split('/')[-1].replace(' ', '').replace("'", '').replace('"', "")
                os.system(f"mv \"{fichier}\" {nouveau_nom}")
                message.fichier = f"{manifestation_liee.get_instance().get_departement_name()}/{manifestation_liee.pk}{nouveau_nom.split(dossier_manif)[-1]}"
            if settings.CELERY_ENABLED:
                message.scan_antivirus = False
            message.save()

        # definition de l'expediteur en fonction de son existance ou non
        if expediteur:
            expediteur = getattr(expediteur, 'user', expediteur)
            if expediteur.__class__.__name__ == 'User':
                if expediteur.get_instance().is_master() and (expediteur.has_group("Administrateurs d'instance") or expediteur.is_superuser):
                    # Les Administrateurs d'instance nationaux et _support nationaux sont rattachés au service factice "Manifestation Sportive"
                    expediteur_txt = "Manifestation Sportive → " + expediteur.first_name + " " + expediteur.last_name
                else:
                    expediteur_txt = expediteur.first_name + ' ' + expediteur.last_name
            elif isinstance(expediteur, list):
                if isinstance(expediteur[0], list):
                    for user in expediteur[0]:
                        expediteur_txt = user.first_name + ' ' + user.last_name + ' ' + expediteur[1].__str__()
                        expediteur = user
                else:
                    if expediteur[1].is_mairie():
                        expediteur_txt = 'Mairie de ' + expediteur[1].precision_nom_s + ' → ' + expediteur[0].first_name + ' ' + expediteur[
                            0].last_name
                    else:
                        expediteur_txt = expediteur[1].__str__() + ' → ' + expediteur[0].first_name + ' ' + expediteur[0].last_name
                    expediteur = expediteur[0]
            else:
                expediteur_txt = str(expediteur)
                expediteur = None
        else:
            expediteur_txt = 'Manifestation Sportive'

        # compte des destinataires
        nombre_destinataire = len(destinataires)
        # boucle pour créer les enveloppes
        for destinataire in destinataires:

            # verification si le destinataire est User
            # destinataire = getattr(destinataire, 'user', destinataire)
            if destinataire.__class__.__name__ == "User":
                service = destinataire.organisation_m2m.first()
                if service:
                    destinataire_txt = service.__str__() + ' → ' + destinataire.first_name + ' ' + destinataire.last_name
                else:
                    destinataire_txt = f"Sans service rattaché → {destinataire.first_name} {destinataire.last_name}"
            if isinstance(destinataire, list):
                destinataire_txt = destinataire[1].__str__() + ' → ' + destinataire[0].first_name + ' ' + destinataire[0].last_name
                service = destinataire[1]
                destinataire = destinataire[0]
            if destinataire.__class__.__name__ == "User" and destinataire.is_active:
                option = OptionUser.objects.get(user=destinataire)
                instance = destinataire.get_instance() if destinataire.get_instance() else None
                lurequis = False
                affichage_messagerie = False
                envoi_courriel = False
                notif_bureau = False

                if objet_lie_pk:
                    action = False
                else:
                    action = None

                # Definition des notifications faites en fonction du type de message et des options du destinataire
                for obj, attrib in [('conversation', 'conversation'), ('news', 'nouveaute'), ('action', 'action'),
                                    ('info_suivi', 'notification'), ('tracabilite', 'tracabilite'), ('forum', 'forum')]:
                    if type_msg == obj:
                        if getattr(option, attrib + '_mail'):
                            envoi_courriel = True
                        if getattr(option, attrib + '_push'):
                            notif_bureau = True
                        if getattr(option, attrib + '_msg'):
                            affichage_messagerie = True
                            lurequis = True
                if type_msg == "convocation":
                    lurequis = True
                    affichage_messagerie = True
                    envoi_courriel = True
                    notif_bureau = True

                enveloppe = Enveloppe(corps=message, expediteur_id=expediteur, expediteur_txt=expediteur_txt,
                                      destinataire_id=destinataire, destinataire_txt=destinataire_txt, objet=titre,
                                      type=type_msg, manifestation=manifestation_liee, reponse=reponse_a_pk, lu_requis=lurequis,
                                      action_traitee=action, affichage_messagerie=affichage_messagerie, notif_bureau=notif_bureau,
                                      doc_objet=objet_lie_nature, nombre_destinataire=nombre_destinataire)

                enveloppe.save()
                enveloppe.refresh_from_db()
                enveloppe.instance.add(instance)
                enveloppe.save()

                # on envoi une notification push si le destinataire le veut
                if notif_bureau and not settings.DEBUG:
                    PushThread(destinataire).start()
                # on envoie un message si le destinataire le veut et qu'il est actif
                if envoi_courriel and destinataire.is_active:
                    msg = enveloppe.mail_nouveau_msg(service=service)
                    enveloppe.envoi_courriel = msg
                    enveloppe.save()

        return message


class Message(models.Model):
    corps = RichTextField()
    fichier = models.FileField('Pièce jointe', null=True, blank=True)
    scan_antivirus = models.BooleanField(verbose_name="scan antivirus ok", default=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    objects = MessageQuerySet.as_manager()

    def __str__(self):
        return self.corps[:10]

    class Meta:
        verbose_name = "Message"
        verbose_name_plural = "Messages"
        ordering = ["pk"]


class Enveloppe(models.Model):
    """
    Model qui détient les détails du messages
    """
    LISTE_TYPE = [('conversation', 'Conversation'), ('news', 'News de la plateforme'), ('convocation', 'Convocation en CDSR'),
                  ('info_suivi', 'Information de suivi'), ('action', "Demande d'action"), ('tracabilite', 'Traçabilité'), ('forum', 'Forum')]

    LISTE_DOC_OBJET = [('dossier', 'Dossier'), ('piece_jointe', 'Document joint'), ('avis', 'Avis'), ('preavis', 'Préavis'),
                       ('presentation', 'Présentation en CDSR'), ('reunion', 'Réunion CDSR'),
                       ('arrete', 'Arrêté'), ('recepisse', 'Récépissé'), ('notice_rd', 'Notice RD')]

    expediteur_id = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL,
                                      related_name='message_expediteur')
    expediteur_txt = models.CharField(max_length=500)
    destinataire_id = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,  on_delete=models.SET_NULL,
                                        related_name='message_destinataire')
    destinataire_txt = models.CharField(max_length=500)
    nombre_destinataire = models.IntegerField(blank=True, null=True)
    objet = models.CharField(max_length=500)
    doc_objet = models.CharField(max_length=30, choices=LISTE_DOC_OBJET, blank=True, null=True)
    corps = models.ForeignKey(Message, on_delete=models.CASCADE, related_name='message_enveloppe')

    type = models.CharField(max_length=30, choices=LISTE_TYPE)
    date = models.DateTimeField(default=timezone.now, blank=True)
    manifestation = models.ForeignKey(Manif, on_delete=models.CASCADE, blank=True, null=True,
                                      related_name='message_manifestation')
    manif_terminee = models.BooleanField(default=False)
    reponse = models.IntegerField(blank=True, null=True)
    instance = models.ManyToManyField('core.instance', blank=True, related_name="message_enveloppe", verbose_name="département du destinataire")

    lu_requis = models.BooleanField()
    lu_datetime = models.DateTimeField(verbose_name="Date et heure de lecture", null=True, blank=True)
    action_traitee = models.BooleanField(verbose_name="action traitée (non utilisé)", null=True, blank=True)

    affichage_messagerie = models.BooleanField()
    envoi_courriel = models.OneToOneField(mail.Email, on_delete=models.CASCADE, null=True, blank=True)
    notif_bureau = models.BooleanField()

    def mail_nouveau_msg(self, email=None, service=None):
        """
        Envoi d'un email de notification
        """
        C = ConfigurationGlobale.objects.get(pk=1)
        envoi_bloque = True
        if self.type == "conversation" and C.mail_conversation:
            envoi_bloque = False
        elif self.type == 'news' and C.mail_actualite:
            envoi_bloque = False
        elif self.type == 'action' and C.mail_action:
            envoi_bloque = False
        elif self.type == "info_suivi" and C.mail_suivi:
            envoi_bloque = False
        elif self.type == "forum" and C.mail_forum:
            envoi_bloque = False
        elif self.type == "tracabilite" and C.mail_tracabilite:
            envoi_bloque = False
        elif self.type == "convocation":
            envoi_bloque = False
        if envoi_bloque:
            return None
        desti_mail = email if email else self.destinataire_id.email
        envoi_service = False
        # type_non_service = ['conversation', 'news', 'tracabilite', 'forum']
        # if service and service.email_s and not self.type in type_non_service:
        #     for enveloppe in self.corps.message_enveloppe.all():
        #         if enveloppe.envoi_courriel:
        #             return None  # pas besoin de créer un nouveau mail il a dejà été envoyé
        #     desti_mail = service.email_s
        #     envoi_service = True
        if service and service.email_s and self.type == "convocation":
            for enveloppe in self.corps.message_enveloppe.filter(destinataire_id__organisation_m2m=service):
                if enveloppe.envoi_courriel:
                    return None  # pas besoin de créer un nouveau mail il a dejà été envoyé
            desti_mail = service.email_s
            envoi_service = True

        # l'adresse de l'expéditeur est définie au niveau de l'instance départementale (normalement)
        expe_mail = None
        if self.expediteur_id:
            expe_mail = self.expediteur_id.get_instance().get_email()
        if not expe_mail and self.destinataire_id:
            expe_mail = self.destinataire_id.get_instance().get_email()
        if not expe_mail:
            expe_mail = settings.DEFAULT_FROM_EMAIL
            
        titre = self.objet
        url = settings.MAIN_URL + "messagerie/message/" + str(self.pk)
        if self.destinataire_id and not envoi_service:
            html_message = f"Ce message est destiné à {self.destinataire_id.first_name} {self.destinataire_id.last_name}.<br><hr>"
        elif service:
            html_message = f"Ce message est destiné au service {service.nom_s}.<br><hr>"
        else:
            html_message = f"Ce message est destiné au service {self.destinataire_txt}.<br><hr>"
        if self.type == "convocation":
            nom_pj = self.corps.fichier.name.split('/')[-1]
            html_message += self.corps.corps
            return mail.send(desti_mail, expe_mail, subject=titre, html_message=html_message,
                             attachments={nom_pj: self.corps.fichier})
        else:
            if self.manifestation:
                html_message += 'Bonjour,<br><br>' + \
                               'Vous avez un nouveau message sur la plateforme <a href="https://www.manifestationsportive.fr/">' + \
                               'manifestationsportive.fr</a> concernant la manifestation <em>' + \
                               str(self.manifestation) + '</em><br>'
            else:
                html_message += 'Bonjour,<br><br>' + \
                               'Vous avez un nouveau message sur la plateforme <a href="https://www.manifestationsportive.fr/">' + \
                               'manifestationsportive.fr</a>.<br>'
            if self.destinataire_id:
                html_message += 'Consulter ce message : ' + \
                                '<a href="' + url + '" target="_blank">' + url + '</a>.'
            else:
                html_message += "NB : N'oubliez pas au préalable de vous connecter avec un compte agent..."
            return mail.send(desti_mail, expe_mail, subject=titre, html_message=html_message)
