from .models.message import Enveloppe
from core.models import User
from instructions.models.avis import Avis
from instructions.models.acces import AutorisationAcces
from instructions.models.instruction import Instruction

def get_carnet(request, manif=None):
    moi = request.user
    carnet = []
    # Dans le cas d'une manifestation hors delay qui n'est pas encore instruite on prend les instructeurs de la prefecture
    if manif.delai_depasse and not manif.get_instruction():
        instructeurs = Instruction.get_instructeurs_prefecture(manif, message=True)
    # Sinon les instructeurs de la manifestation
    else:
        instructeurs = manif.get_instructeurs_manif()

    for user in instructeurs:
        if isinstance(user, list):
            if isinstance(user[0], User) and user[0].is_active:
                carnet.append(user[0])

    # puis on rajoute les personnes qui ont envoyé un message à l'user
    messagerecus = Enveloppe.objects.filter(destinataire_id=moi, manifestation=manif).distinct('expediteur_id').exclude(type="news")
    for messagerecu in messagerecus:
        if messagerecu.expediteur_id and messagerecu.expediteur_id not in carnet and messagerecu.expediteur_id.is_active:
            carnet.append(messagerecu.expediteur_id)

    # puis on rajoute les personnes qui ont envoyé un message à l'user
    avis_manif = Avis.objects.filter(instruction__manif=manif).distinct()
    for avis in avis_manif:
        for service in avis.get_tout_services_concerne():
            carnet.append(service)

    # puis on rajoute les personnes qui ont un accès a cette manifestation
    acces_manif = AutorisationAcces.objects.filter(manif=manif, date_revocation__isnull=True).distinct()
    for acces in acces_manif:
        carnet.append(acces.organisation)

    return sorted(carnet, key=lambda x: str(x))
