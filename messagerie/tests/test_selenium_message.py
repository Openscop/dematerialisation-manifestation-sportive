import time

from django.contrib.auth.models import Group
from django.test import tag, override_settings
from django.utils import timezone
from django.contrib.auth.hashers import make_password as make
from django.contrib.contenttypes.models import ContentType
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

from allauth.account.models import EmailAddress

from instructions.tests.test_base_selenium import SeleniumCommunClass
from instructions.factories import InstructionFactory
from core.factories import UserFactory
from messagerie.models import Enveloppe
from structure.factories.service import CategorieServiceFactory, TypeServiceFactory, ServiceConsulteFactory

class SeleniumMessagerieTests(SeleniumCommunClass):
    """
    Test des fonctions de base de la messagerie
    """
    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ Test Messagerie (Sel) =============')
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme
        SeleniumCommunClass.init_setup(cls)
        group_admin = Group(name='Administrateurs d\'instance')
        group_admin.save()
        group_admin.user_set.add(cls.instructeur)
        cls.organisateur.last_login = timezone.now() - timezone.timedelta(days=370)
        cls.organisateur.save()

        categorie_police = CategorieServiceFactory(nom_s="Police")
        cls.type_ddsp = TypeServiceFactory(abreviation_s="DDSP", nom_s="DDSP", categorie_fk=categorie_police)
        cls.ddsp = ServiceConsulteFactory(type_service_fk=cls.type_ddsp, departement_m2m=cls.dep,
                                           instance_fk=cls.dep.instance, porte_entree_avis_b=True,
                                           autorise_rendre_avis_b=True, )
        cls.agent_ddsp2 = UserFactory.create(username='agent_ddsp2', password=make("123"),
                                             default_instance=cls.dep.instance)
        cls.agent_ddsp2.organisation_m2m.add(cls.ddsp)

        EmailAddress.objects.create(user=cls.agent_ddsp2, email='agent_ddsp2@example.com', primary=True, verified=True)
        categorie_pompier = CategorieServiceFactory(nom_s="Pompier")
        cls.type_sdis = TypeServiceFactory(abreviation_s="SDIS", nom_s="Sdis", categorie_fk=categorie_pompier)

        cls.agent_sdis2 = UserFactory.create(username='agent_sdis2', password=make("123"), default_instance=cls.dep.instance)
        EmailAddress.objects.create(user=cls.agent_sdis2, email='agent_sdis2@example.com', primary=True, verified=True)
        cls.service_sdis = ServiceConsulteFactory(type_service_fk=cls.type_sdis, departement_m2m=cls.dep,
                                                   instance_fk=cls.dep.instance, porte_entree_avis_b=True)
        cls.agent_sdis2.organisation_m2m.add(cls.service_sdis)

        super().setUpClass()

    @tag('selenium')
    @override_settings(DEBUG=True)
    def test_messagerie(self):
        """
        Test de la messagerie
        """
        qs_env = Enveloppe.objects.order_by('pk')


        print('*** messagerie instructeur liste organisateur ***')
        self.connexion('instructeur')
        # Affichage de la messagerie
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Messagerie").click()
        time.sleep(self.DELAY*2)
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY)
        # Envoi d'un msg
        self.selenium.find_element(By.ID, "show_write").click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 200)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn-modal-selecteur-carnet').click()
        time.sleep(self.DELAY*2)
        self.chosen_select('select_instances_modal_chosen', 'instance de test (42)')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % 'Organisateurs actifs depuis un an').click()
        time.sleep(self.DELAY*2)
        list = self.selenium.find_element(By.ID, 'service-liste')
        self.assertEqual(len(list.find_elements(By.TAG_NAME, 'li')), 0, msg="la liste doit être vide")

        self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % 'Organisateurs inactifs depuis un an').click()
        time.sleep(self.DELAY * 2)
        list = self.selenium.find_element(By.ID, 'service-liste')
        self.assertEqual(len(list.find_elements(By.TAG_NAME, 'li')), 1, msg="la liste doit contenir un nom")
        self.selenium.find_element(By.ID, 'btn-close-modal-avis').click()
        time.sleep(self.DELAY * 2)
        self.deconnexion()

        print('*** test de la messagerie organisateur avant envoi manif ***')
        self.connexion('organisateur')
        # Affichage Tdb
        bloc = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_attente')]")
        bloc.click()
        self.assertEqual('1', bloc.find_element(By.CLASS_NAME, 'test_nb_atraiter').text)
        declar = self.selenium.find_element(By.TAG_NAME, 'tbody')
        self.assertIn('Manifestation_Test', declar.text)
        try:
            declar.find_element(By.CLASS_NAME, 'table_afaire')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas de nouveau dossier")
        # Appel de la vue de détail et test présence manifestation
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY)
        url = self.selenium.current_url
        self.selenium.find_element(By.XPATH, '//td[contains(text(), "Manifestation_Test")]').click()
        wait = WebDriverWait(self.selenium, 15)
        wait.until(lambda driver: self.selenium.current_url != url)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        time.sleep(self.DELAY*2)
        # Appel messagerie
        self.selenium.find_element(By.XPATH, "//button[contains(.,'Messagerie')]").click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, "show_write").click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 500)")
        chosen_select = self.selenium.find_element(By.ID, 'carnet_chosen')
        chosen_select.click()
        time.sleep(self.DELAY)
        menu = chosen_select.find_element(By.CLASS_NAME, 'chosen-drop')
        # time.sleep(30)
        # self.assertEqual(len(menu.find_elements(By.TAG_NAME, 'li')), 1, msg="la liste contient 1 nom")
        self.deconnexion()

        print('*** test de la messagerie organisateur après envoi manif ***')
        self.instruction = InstructionFactory.create(manif=self.manifestation)
        self.connexion('organisateur')
        # Affichage Tdb
        bloc = self.selenium.find_element(By.XPATH, "//*[starts-with(@id, 'btn')][contains(@id, '_etat_demande')]")
        bloc.click()
        self.assertEqual('1', bloc.find_element(By.CLASS_NAME, 'test_nb_encours').text)
        declar = self.selenium.find_element(By.TAG_NAME, 'tbody')
        self.assertIn('Manifestation_Test', declar.text)
        try:
            declar.find_element(By.CLASS_NAME, 'table_encours')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas de nouveau dossier")
        # Appel de la vue de détail et test présence manifestation
        self.selenium.execute_script("window.scroll(0, 700)")
        time.sleep(self.DELAY)
        url = self.selenium.current_url
        self.selenium.find_element(By.XPATH, '//td[contains(text(), "Manifestation_Test")]').click()
        wait = WebDriverWait(self.selenium, 15)
        wait.until(lambda driver: self.selenium.current_url != url)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        time.sleep(self.DELAY*2)
        # Appel messagerie
        self.selenium.find_element(By.XPATH, "//button[contains(.,'Messagerie')]").click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY)
        # Envoi d'un msg à l'instructeur
        self.selenium.find_element(By.ID, "show_write").click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 500)")
        self.chosen_select('carnet_chosen', 'Bob ROBERT')
        time.sleep(self.DELAY * 5)
        self.selenium.execute_script("window.scroll(0, 600)")
        time.sleep(self.DELAY * 5)
        self.chosen_select('doc_objet_chosen', 'Dossier')
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, "msg_objet").send_keys('Test orga vers instr')
        # Ici on change pour ecrire dans l'iframe
        frame = self.selenium.find_element(By.CLASS_NAME, 'cke_wysiwyg_frame')
        self.selenium.switch_to.frame(frame)
        body = self.selenium.find_element(By.TAG_NAME, 'body')
        body.send_keys('tester organisateur vers instructeur')
        self.selenium.switch_to.default_content()
        self.selenium.find_element(By.ID, "envoi").click()
        time.sleep(self.DELAY)

        # Vérifier presence dans la liste des mails
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY * 2)
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'show_mail').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY)
        # Quatrième message créé
        env_1 = qs_env.last()
        env_1_msg = env_1.corps
        print("id enveloppe : " + str(env_1.pk))
        self.selenium.find_element(By.ID, "msgid_" + str(env_1.pk)).click()
        time.sleep(self.DELAY)
        texte = self.selenium.find_element(By.ID, "corps_msg_" + str(env_1_msg.pk))
        self.assertIn('tester organisateur vers instructeur', texte.text)
        self.deconnexion()

        print('*** test de la messagerie instructeur ***')
        self.connexion('instructeur')
        # Affichage de la messagerie
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Messagerie").click()
        time.sleep(self.DELAY)
        # Vérifier presence dans la liste des mails
        mess = self.selenium.find_element(By.ID,  "msgid_" + str(env_1.pk))
        self.assertIn('bold', mess.get_attribute("style"))
        mess.click()
        texte = self.selenium.find_element(By.ID, "corps_msg_" + str(env_1_msg.pk))
        self.assertIn('tester organisateur vers instructeur', texte.text)
        mess.click()
        # Vérifier passage en "lu"
        self.assertNotIn('bold', mess.get_attribute("style"))

        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY)

        # Envoi d'un msg
        self.selenium.find_element(By.ID, "show_write").click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 200)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'btn-modal-selecteur-carnet').click()
        time.sleep(self.DELAY * 2)
        self.chosen_select('select_instances_modal_chosen', 'instance de test (42)')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % 'Organisateurs actifs depuis un an').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'label').click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, 'send_multiple').click()
        time.sleep(self.DELAY * 2)
        time.sleep(self.DELAY)
        self.chosen_select('doc_objet_chosen', 'Dossier')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, "msg_objet").send_keys('test instr vers orga')
        # Ici on change pour ecrire dans l'iframe
        frame = self.selenium.find_element(By.CLASS_NAME, 'cke_wysiwyg_frame')
        self.selenium.switch_to.frame(frame)
        body = self.selenium.find_element(By.TAG_NAME, 'body')
        body.send_keys('tester instructeur vers organisateur')
        self.selenium.switch_to.default_content()
        time.sleep(1)
        self.selenium.find_element(By.ID, "envoi").click()

        # Vérifier presence dans la liste des mails
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY * 15)
        self.selenium.find_element(By.ID, 'show_mail').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY)
        env_2 = qs_env.last()
        env_2_msg = env_2.corps
        print("id enveloppe : " + str(env_2.pk))
        self.assertNotIn('bold', self.selenium.find_element(By.ID,  "msgid_" + str(env_2.pk)).get_attribute("style"))
        self.selenium.find_element(By.ID,  "msgid_" + str(env_2.pk)).click()
        time.sleep(self.DELAY)
        texte = self.selenium.find_element(By.ID, "corps_msg_" + str(env_2_msg.pk))
        self.assertIn('tester instructeur vers organisateur', texte.text)
        time.sleep(self.DELAY)

        # Vérifier les filtres
        self.selenium.refresh()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY)
        test = self.selenium.find_element(By.ID, 'filtre_mail').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.CLASS_NAME, 'avis').click()
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'filtre_save').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'nom_filtre').send_keys('aze')
        self.selenium.find_element(By.ID, "form_filtre_submit").click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.CLASS_NAME, 'filtre_perso_list').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.CLASS_NAME, "supprimer").click()
        time.sleep(self.DELAY * 4)
        self.selenium.find_element(By.ID, "supp_conf_1").click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, "supp_1").click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 0)")
        time.sleep(self.DELAY)
        test = self.selenium.find_element(By.ID, 'filtre_mail').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, 'Tableau de bord').click()
        time.sleep(self.DELAY)

        print('*** Instructeur : distribution des avis ***')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Changer l'assignation").click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Me l'assigner").click()
        time.sleep(self.DELAY * 5)
        self.selenium.execute_script("window.scroll(0, 400)")
        # Vérifier l'action disponible
        self.assertIn("Envoyer une demande d'avis", self.selenium.page_source)
        # Distribuer les demandes d'avis de l'événement
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Envoyer une demande d'avis").click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % 'Police').click()
        time.sleep(self.DELAY * 3)
        self.selenium.find_element(By.CLASS_NAME, 'name_service').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.CLASS_NAME, 'btn-action-selecteur').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, 'btn-close-modal-avis').click()
        time.sleep(self.DELAY * 2)
        env_3 = qs_env.filter(objet__icontains='Demande').first()
        env_3_msg = env_3.corps
        env_3_org = qs_env.filter(objet__icontains='Démarrage').first()
        print("id enveloppe service : " + str(env_3.pk))
        print("id enveloppe orga : " + str(env_3_org.pk))
        time.sleep(10)
        self.deconnexion()

        print('**** Message avis ddsp2 ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'nouveau')
        # Affichage de la messagerie
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Messagerie").click()
        time.sleep(self.DELAY)
        # Vérifier presence dans la liste des mails
        mess = self.selenium.find_element(By.ID,  "msgid_" + str(env_3.pk))
        self.assertIn('bold', mess.get_attribute("style"))
        mess.click()
        texte = self.selenium.find_element(By.ID, "corps_msg_" + str(env_3_msg.pk))
        self.assertIn('Demande d\'avis à traiter', texte.text)
        mess.click()
        self.deconnexion()

        print('*** Instructeur : envoi à un service ***')
        # Envoi message à un service
        self.connexion('instructeur')
        # Affichage de la messagerie
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Messagerie").click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY)
        # Envoi d'un msg
        self.selenium.find_element(By.ID, "show_write").click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 200)")
        time.sleep(self.DELAY)

        self.selenium.find_element(By.ID, 'btn-modal-selecteur-carnet').click()
        time.sleep(self.DELAY * 2)
        self.chosen_select('select_instances_modal_chosen', 'instance de test (42)')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.XPATH, "//li[contains(text(), '%s')]" % 'Pompier').click()

        self.selenium.find_element(By.ID, "checkbox_" + str(self.service_sdis.pk) + '_Organisation').click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        time.sleep(self.DELAY * 2)
        self.selenium.find_element(By.ID, 'send_multiple').click()
        time.sleep(self.DELAY * 2)

        self.chosen_select('doc_objet_chosen', 'Dossier')
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, "msg_objet").send_keys('message sdis')
        # Ici on change pour ecrire dans l'iframe
        frame = self.selenium.find_element(By.CLASS_NAME, 'cke_wysiwyg_frame')
        self.selenium.switch_to.frame(frame)
        body = self.selenium.find_element(By.TAG_NAME, 'body')
        body.send_keys('tester instruc vers service')
        self.selenium.switch_to.default_content()
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, "envoi").click()
        time.sleep(self.DELAY)
        # Vérifier presence dans la liste des mails
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.ID, 'menu_msg').click()
        time.sleep(self.DELAY * 5)
        self.selenium.find_element(By.ID, 'show_mail').click()
        env_4 = qs_env.last()
        env_4_msg = env_4.corps
        print("id enveloppe : " + str(env_4.pk))
        self.deconnexion()

        print('**** Message sdis1 ****')
        # Vérification de la présence du message
        self.connexion('agent_sdis2')
        # Affichage de la messagerie
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Messagerie").click()
        time.sleep(self.DELAY)
        # Vérifier presence dans la liste des mails
        mess = self.selenium.find_element(By.ID,  "msgid_" + str(env_4.pk))
        self.assertIn('bold', mess.get_attribute("style"))
        mess.click()
        texte = self.selenium.find_element(By.ID, "corps_msg_" + str(env_4_msg.pk))
        self.assertIn('tester instruc vers service', texte.text)
        mess.click()
        self.deconnexion()

        print('*** messagerie organisateur ***')
        # Vérification de la présence du message
        self.connexion('organisateur')
        # Affichage de la messagerie
        self.selenium.find_element(By.CLASS_NAME, 'navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element(By.PARTIAL_LINK_TEXT, "Messagerie").click()
        time.sleep(self.DELAY)
        # Vérifier presence dans la liste des mails
        mess = self.selenium.find_element(By.ID,  "msgid_" + str(env_3_org.pk))
        self.assertIn('bold', mess.get_attribute("style"))
        mess.click()
        texte = self.selenium.find_element(By.ID, "corps_msg_" + str(env_3_org.corps.pk))
        self.assertIn('traitement de votre dossier', texte.text)
        mess.click()
        mess = self.selenium.find_element(By.ID,  "msgid_" + str(env_2.pk))
        self.assertIn('bold', mess.get_attribute("style"))
        mess.click()
        texte = self.selenium.find_element(By.ID, "corps_msg_" + str(env_2_msg.pk))
        self.assertIn('tester instructeur vers organisateur', texte.text)
        mess.click()
        self.deconnexion()
