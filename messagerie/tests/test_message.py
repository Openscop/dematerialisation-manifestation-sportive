import json, time

from django.test import TestCase
from django.utils import timezone
from django.contrib.auth.hashers import make_password as make
from django.contrib.contenttypes.models import ContentType

from post_office.models import EmailTemplate

from core.models import Instance
from core.factories import UserFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from sports.factories import ActiviteFactory
from evenements.factories import DcnmFactory
from instructions.factories import InstructionFactory
from messagerie.models import Enveloppe
from structure.factories.structure import StructureOrganisatriceFactory
from structure.factories.service import ServiceConsulteFactory

class MessagerieTests(TestCase):

    @classmethod
    def setUpTestData(cls):

        print()
        print('========== Message ===========')
        ContentType.objects.clear_cache()  # Nécessaire pour faire fonctionner les tests avec le polymorphisme

        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__instruction_mode=Instance.IM_CIRCUIT_D)
        arrond_1 = ArrondissementFactory.create(name='Montbrison', code='421', departement=dep)
        arrond_2 = ArrondissementFactory.create(name='Roanne', code='422', departement=dep,
                                                prefecture__type_service_fk__nom_s="Sous-Préfecture",
                                                prefecture__type_service_instructeur_s='sousprefecture')
        arrond_3 = ArrondissementFactory.create(name='ST Etienne', code='420', departement=dep,
                                                prefecture__type_service_fk__nom_s="Sous-Préfecture",
                                                prefecture__type_service_instructeur_s='sousprefecture')
        cls.dep = dep
        cls.instance = dep.instance
        cls.prefecture1 = arrond_1.organisations.first()
        cls.prefecture2 = arrond_2.organisations.first()
        cls.prefecture3 = arrond_3.organisations.first()

        cls.commune1 = CommuneFactory(name='Bard', arrondissement=arrond_1)
        cls.autrecommune1 = CommuneFactory(name='Roche', arrondissement=arrond_1)
        cls.commune2 = CommuneFactory(name='Bully', arrondissement=arrond_2)
        cls.autrecommune2 = CommuneFactory(name='Régny', arrondissement=arrond_2)
        cls.commune3 = CommuneFactory(name='Villars', arrondissement=arrond_3)
        cls.edsr = dep.organisation_set.get(precision_nom_s="EDSR")
        cls.usg = ServiceConsulteFactory(instance_fk=dep.instance, precision_nom_s="PGM", service_parent_fk=cls.edsr,
                                          type_service_fk__categorie_fk__nom_s="Gendarmerie", type_service_fk__nom_s="PGM",
                                          autorise_rendre_preavis_a_service_avis_b=True, autorise_acces_consult_organisateur_b=True, commune_m2m=cls.commune1)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        # Organisateur actif
        cls.structure1 = StructureOrganisatriceFactory.create(precision_nom_s="structure_test_1",
                                                              commune_m2m=cls.commune1, instance_fk=dep.instance)
        cls.organisateur1 = UserFactory.create(username='organisateur1', password=make('123'),
                                               last_name='Orga1', default_instance=dep.instance)
        cls.organisateur1.organisation_m2m.add(cls.structure1)

        # Organisateur inactif
        cls.structure2 = StructureOrganisatriceFactory.create(precision_nom_s="structure_test_2",
                                                          commune_m2m=cls.commune2, instance_fk=dep.instance)
        cls.organisateur2 = UserFactory.create(username='organisateur2', password=make('123'),
                                                   last_name='Orga2', default_instance=dep.instance)
        cls.organisateur2.last_login = timezone.now() - timezone.timedelta(days=370)
        cls.organisateur2.save()
        cls.organisateur2.organisation_m2m.add(cls.structure2)

        # Instructeur de préfecture
        cls.instructeur1 = UserFactory.create(username='instructeur1', password=make('123'),
                                              last_name='Instru1', default_instance=dep.instance)
        cls.instructeur1.organisation_m2m.add(cls.prefecture1)

        cls.instructeur2 = UserFactory.create(username='instructeur2', password=make('123'),
                                              last_name='Instru2', default_instance=dep.instance)
        cls.instructeur2.organisation_m2m.add(cls.prefecture2)

        cls.instructeur3 = UserFactory.create(username='instructeur3', password=make('123'),
                                              last_name='Instru3', default_instance=dep.instance)
        cls.instructeur3.organisation_m2m.add(cls.prefecture3)

        # Agent mairie de la commune 1
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make('123'),
                                              last_name='Mairie1', default_instance=dep.instance, email='mair@test.fr')
        cls.mairie1 = cls.commune1.organisation_set.first()
        cls.agent_mairie.organisation_m2m.add(cls.mairie1)
        # Agent EDSR
        cls.agent_edsr = UserFactory.create(username='agent_edsr', password=make('123'),
                                            last_name='Edsr', default_instance=dep.instance, email='edsr@test.fr')
        cls.agent_edsr.organisation_m2m.add(dep.organisation_set.get(precision_nom_s="EDSR"))
        # Création d'une manif NM sur 2 commmune, arrondissement de préfecture1
        cls.manifestation1 = DcnmFactory.create(ville_depart=cls.commune1, structure_organisatrice_fk=cls.structure1, activite=activ,
                                                nom='Manifestation_Test', villes_traversees=(cls.autrecommune1,))
        InstructionFactory.create(manif=cls.manifestation1)

        EmailTemplate.objects.create(name='new_msg')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def test_affichagemessagerie(self):

        print('\t>>> Test affichage')
        # Affichage sans login 403
        retour = self.client.get("/messagerie/", HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 401)

        self.assertTrue(self.client.login(username="organisateur1", password='123'))

        # Affichage messagerie principale
        retour = self.client.get("/messagerie/", HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, 'Messagerie')
        self.assertTemplateUsed(retour, 'messagerie/messagerie_globale.html')

        # Affichage du body de la messagerie global
        retour = self.client.get("/messagerie/messagerie/", HTTP_HOST='127.0.0.1:8000')
        self.assertTemplateUsed(retour, 'messagerie/messagerie.html')
        self.assertContains(retour, "Vous êtes dans la messagerie générale")

        # Affichage du compteur
        retour = self.client.get('/messagerie/cpt/00000/', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        self.assertTemplateUsed(retour, 'messagerie/compteurnonlu.html')
        self.assertContains(retour, '</i>0', 6)

        # Affichage de la liste des msg
        retour = self.client.get('/messagerie/listemessage/?limit=10&manif=&suivi=1&action=1&conv=1&nouv=1&dossier=1&docjoin=1&avis=1&preavis=1&arrete=1&recepisse=1&envoi=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertTemplateUsed(retour, 'messagerie/liste_message.html')
        self.assertContains(retour, "Destinataire/Expediteur")

        print('\t>>> Test filtre')
        # Création d'un filtre
        data = {
            "titre": "filtre_test",
            'option': "mes-options",
        }
        retour = self.client.post('/messagerie/filtre/', data=data, HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)

        # Affichage du filtre
        retour = self.client.get('/messagerie/filtre/', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Test du contenu retourné
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"model": "messagerie.filtre",
              "pk": 1,
              "fields": {
                  "titre": "filtre_test",
                  "utilisateur": self.organisateur1.pk,
                  "option": "mes-options"
              }}]
        )

        # Suppression du filtre
        retour = self.client.get('/messagerie/filtre/del/?pk=1', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)

        print('\t>>> Test envoi')
        # Envoi d'un message
        retour = self.client.get('/messagerie/envoimessage/?manif=' + str(self.manifestation1.pk), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "<div id=\"formmail")

        # Envoi du msg conversation dossier
        data = {
            "service": "orga",
            'destinataires': self.instructeur1.pk,
            "doc_objet": "dossier",
            "objet": "test envoi sur dossier message instructeur1",
            "corps": "<p>test dossier</p>"
        }
        retour = self.client.post('/messagerie/envoimessage/?manif=' + str(self.manifestation1.pk),
                                  data=data, HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        self.msg1 = Enveloppe.objects.last()
        time.sleep(1)
        # Envoi du msg conversation avis
        data = {
            "service": "orga",
            'destinataires': self.instructeur1.pk,
            "doc_objet": "avis",
            "objet": "test envoi sur avis message instructeur1",
            "corps": "<p>test avis</p>"
        }
        retour = self.client.post('/messagerie/envoimessage/?manif=' + str(self.manifestation1.pk), data=data, HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        self.msg2 = Enveloppe.objects.last()
        time.sleep(1)

        print('\t>>> Test affichage messages organisateur')
        # Affichage de la liste des msg conversation concernant un dossier, envoyés et lus de la manif 1
        retour = self.client.get('/messagerie/listemessage/?manif=' + str(self.manifestation1.pk) + '&conv=1&dossier=1&envoi=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        # self.assertContains(retour, "Pour Préfecture de Montbrison")
        self.assertContains(retour, "test envoi sur dossier message instructeur1")
        # Affichage de la liste générale des msg conversation concernant un dossier, envoyés et lus
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&envoi=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        # self.assertContains(retour, "Pour Préfecture de Montbrison")
        self.assertContains(retour, "test envoi sur dossier message instructeur1")
        # Affichage de la liste générale des msg conversation concernant un dossier, envoyés et non lus
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&envoi=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertNotIn('class="msg_header', retour.content.decode('utf-8'))
        # Affichage de la liste générale des conversation concernant un dossier, msg reçus et lus
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertNotIn('class="msg_header', retour.content.decode('utf-8'))
        # Affichage de la liste générale des msg conversation concernant un avis, envoyés et non lus
        time.sleep(1)
        retour = self.client.get('/messagerie/listemessage/?manif=&suivi=1&action=1&conv=1&trac=1&nouv=1&dossier=1&docjoin=1&avis=1&preavis=1&arrete=1&recepisse=1&envoi=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        # self.assertContains(retour, "Pour Préfecture de Montbrison")
        self.assertContains(retour, "test envoi sur avis message instructeur1")
        # Affichage de la liste générale des msg action concernant un dossier, envoyés et non lus
        retour = self.client.get('/messagerie/listemessage/?action=1&dossier=1&envoi=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertNotIn('class="msg_header', retour.content.decode('utf-8'))
        # Affichage de la liste générale des msg traçabilité, envoyés et non lus
        retour = self.client.get('/messagerie/listemessage/?trac=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        self.assertContains(retour, "Manifestation Sportive")
        self.assertContains(retour, "Connexion à la plateforme")

        print('\t>>> Test carnet instructeur')
        # Delog pour log en instructeur
        self.client.logout()
        self.assertTrue(self.client.login(username="instructeur1", password='123'))

        # Récupération du carnet sans query : 403 normalement
        retour = self.client.get('/messagerie/carnetinstructeur/', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 403)

        # Récupération du carnet instructeur "organisateurs actifs"
        retour = self.client.get(f'/structure/ajax/servicelist/?secteur=instance_{self.instance.pk}&categorie=orga_new'
                                 f'&action=carnet_messagerie&show_agent=false&show_old=true&show_sous_service=true&manif=',
                                 HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # # Test du contenu retourné
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"id": self.organisateur1.pk,
              "service": self.structure1.pk,
              "name": str(self.structure1) + ' - ' + self.organisateur1.get_full_name(),
              "data": ""}]
        )
        # # Récupération du carnet instructeur "organisateurs inactifs"
        retour = self.client.get(f'/structure/ajax/servicelist/?secteur=instance_{self.instance.pk}&categorie=orga_old'
                                 f'&action=carnet_messagerie&show_agent=false&show_old=true&show_sous_service=true&manif=',
                                 HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Test du contenu retourné
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"id": self.organisateur2.pk,
              "service": self.structure2.pk,
              "name": str(self.structure2) + ' - ' + self.organisateur2.get_full_name(),
              "data": ""}]
        )
        # # Récupération du carnet instructeur "organisateur de la manifestation"
        retour = self.client.get(f'/structure/ajax/servicelist/?secteur=instance_{self.instance.pk}&categorie=orga_manif'
                                 f'&action=carnet_messagerie&show_agent=false&show_old=true&show_sous_service=true&manif={str(self.manifestation1.pk)}',
                                 HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Test du contenu retourné
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"id": self.organisateur1.pk,
              "service": self.structure1.pk,
              "name": str(self.structure1) + ' - ' + self.organisateur1.get_full_name(),
              "data": ""}]
        )

        print('\t>>> Test affichage message instructeur')
        # Le paramètre nonlu agit en inverse : nonlu=1 => tous les messages, sans paramètre => uniquement les non lus
        # Affichage de la liste des msg conversation concernant un dossier, reçus et non lus de la manif 1
        retour = self.client.get('/messagerie/listemessage/?manif=' + str(self.manifestation1.pk) + '&conv=1&dossier=1&recu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertContains(retour, self.structure1)
        self.assertContains(retour, "test envoi sur dossier message instructeur1")
        # Affichage de la liste générale des msg conversation concernant un dossier, reçus et non lus
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&recu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        self.assertContains(retour, self.structure1)
        self.assertContains(retour, "test envoi sur dossier message instructeur1")
        # Affichage de la liste générale des msg conversation concernant un avis, reçus et non lus
        retour = self.client.get('/messagerie/listemessage/?conv=1&avis=1&recu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        self.assertContains(retour, self.structure1)
        self.assertContains(retour, "test envoi sur avis message instructeur1")

        # Nombre de message non lus : 2
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&avis=1&recu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertContains(retour, 'class="msg_header', count=2)

        # Affichage du contenu du message
        retour = self.client.get('/messagerie/message/' + str(self.msg2.pk), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Préfecture de Montbrison")
        self.assertContains(retour, "test avis")
        # Todo a faire dans les selnium à cause de la non précense de l'ajax ici
        # self.assertContains(retour, "Repondre à ce message")

        # Marquer ce message comme lu
        retour = self.client.post('/messagerie/lu/?msg=' + str(self.msg2.pk), HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Marquer ce message comme lu
        retour = self.client.post('/messagerie/lu/?msg=' + str(self.msg1.pk), HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)

        # Nombre de message non lus : 0
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&avis=1&recu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertNotContains(retour, 'class="msg_header')

        # Nombre de message lus : 2
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&avis=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertContains(retour, 'class="msg_header', count=2)

        # Affichage de la liste générale des msg conversation concernant un avis, reçus et lus
        retour = self.client.get('/messagerie/listemessage/?conv=1&avis=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        self.assertContains(retour, self.structure1)
        self.assertContains(retour, "test envoi sur avis message instructeur1")

        print('\t>>> Test envoi réponse')
        # Envoi d'une réponse au message
        retour = self.client.post('/messagerie/envoimessagereponse/?pk=' + str(self.msg2.pk),
                                  {'objet': "Réponse au message #3", 'corps': "Réponse au message #3"},
                                  HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        time.sleep(1)
        retour = self.client.post('/messagerie/envoimessagereponse/?pk=' + str(self.msg2.pk),
                                  {'objet': "Réponse au message #3", 'corps': "Réponse au message #3"},
                                  HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        time.sleep(1)

        # Affichage de la liste générale des msg conversation concernant un avis, reçus, envoyés et lus
        retour = self.client.get('/messagerie/listemessage/?manif=&suivi=1&action=1&conv=1&trac=1&nouv=1&dossier=1&docjoin=1&avis=1&preavis=1&arrete=1&recepisse=1&envoi=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header title_msg', retour.content.decode('utf-8'))
        self.assertContains(retour, self.structure1)
        self.assertIn('Réponse au message #3', retour.content.decode('utf-8'))
        self.assertContains(retour, "Réponse au message #3")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        self.assertContains(retour, self.structure1)
        self.assertContains(retour, "test envoi sur avis message instructeur1")

        # print(retour.content.decode('utf-8'))
        # for user in User.objects.all():
        #     print(user.username + " - " + user.get_full_name())
