$(function() {
    // lors de test cette fonction n'existe pas comme on ne créer pas d'aide
    if (typeof showcontexthelps === "function"){
        showcontexthelps();
    }

});
// function qui va afficher la date.
// l'astuce est que l'on va comparer la date d'un message avec le precedant. s'il differe on créer une div et on y met la date dedans.
 function AfficherDate(){
        var clas= $('.title_msg');
        var ancien = '';
        for (var ligne in clas){

            if (clas[ligne].id){
                let myid= '#date_'+clas[ligne].id;
                if ($(myid).text() !== ancien){
                    $("<p class='date_msg'><span>" + $(myid).text() + '</span></p>').insertBefore("#"+clas[ligne].id)
                    ancien = $(myid).text()
                }
            }
        }
    }
    AfficherDate();

    AfficherIcones();


    // function faite quand l'utilisateur veut consulter un message.
    $('.title_msg').click(function (e) {
        if ($(this).hasClass('msg_header_open')){
            $('.message_body').empty().removeClass('message_body_open');
            $('.title_msg').removeClass('msg_header_open');
        }
        else {
            $('.message_body').empty().removeClass('message_body_open');
            $('.title_msg').removeClass('msg_header_open');
            $(this).addClass('msg_header_open');
            let id= '#body_'+$(this).attr('id').split("_")[1];
            let pk=$(this).attr('id').split("_")[1];
            let url_msg="/messagerie/message/"+pk;
            $(this).css('font-weight', 'normal').css('color', '#555555');
            $.get(url_msg,function (data) {
                $(id).html(data);
                $(id).addClass('message_body_open');
                let url_nonlu = `/messagerie/lu/?nonlu=${pk}`
                html = `<div class="d-flex"  style="margin-left:auto; text-align:right; width: 50px"><a data-bs-toggle="tooltip" data-placement="top" title='Mettre en non lu' href='${url_nonlu}' id="nonlu_msg" target="_blank" style='display:block; top:-0px;  color:#97a200; margin-right:1em; width:12px; position:relative;'><i class="fal fa-envelope-open"></i></a>`
                html += `<a href='${url_msg}' target="_blank" style=' display:block; top:-0px;  color:#97a200; margin-right:0.4em; width:12px; position:relative;'><i class="fenetre"></i></a>`
                $(id).append(html);
                 AfficherIcones();
                // ici on envoi à django le fait que l'utilisateur à lu le message.
                $.post("/messagerie/lu/?msg="+pk, function (data) {});
            });
        }
    });

    $('body').on('click', '#nonlu_msg', function (e){
        e.preventDefault()
        let url = $(this).attr('href')
        $.post(url, {}, function (data){
            console.log(data)
            $('.message_body').empty().removeClass('message_body_open');
            $('.title_msg').removeClass('msg_header_open');
        })
    })