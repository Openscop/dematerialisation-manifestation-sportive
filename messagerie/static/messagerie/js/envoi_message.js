$(function() {
    // lors de test cette fonction n'existe pas comme on ne créer pas d'aide
    if (typeof showcontexthelps === "function"){
        showcontexthelps();
    }

});
/*
    fonction pour charger la boite d'envoi de la messsagerie
     */
function affichage_envoi(){
    let url =url_envoiunique+"?manif="+manif;
    $.get(url, function (data) {
        $('#show_writediv').html(data)
    })
}


// ici on vas mettre en place chosen, on attente un peu sinon les script sont lancé avant que l'init de chosen soit finit

$(function () {
    $('.chosen-select').chosen();
    setTimeout(function () {

        $('#carnet_chosen').css('width', '100%');
        $('#doc_objet_chosen').css('width', '100%');
        // $('#service_chosen').css('width', '100%');
        $('#departement_actu_chosen').css('width', '100%');
        $('#departement_instru_chosen').css('width', '100%');
        $('#role_actu_chosen').css('width', '100%');
        $('#type_desti_chosen').css('width', '100%');
    },500)
    $('#attente_en_cours').fadeOut();
    $('#formmail_englobe').fadeIn();
})
AfficherIcones();

// function qui va donner la liste des destinataire possible d'un service
$('body').on("change", "#service",function (e) {

    var val = $(this).val();
    if (val.length===1){
        var orga_manif = false
        val = val[0]
        if (manif && val.startsWith('orga|')){
            orga_manif = true
        }
        var dep = $('#departement_instru').val()
        let url = url_instrucarnet+"?idservice="+val+'&dep='+dep;

        let spinner = '<i class="spinner"></i><span>Chargement en cours</span>';
        $('#intru_destinataire').empty().html(spinner).show();
        AfficherIcones();
        $.get(url, function (data) {
            var display ="<select id=\"carnet\" class=\"chosen-select\" data-placeholder=\"Destinataires : laissez vide pour envoyer au service entier\" name=\"destinataires\" multiple=\"multiple\">\n";

            if (orga_manif){
                for (let i=0; i<data.length;i++){
                    display+='<option value="'+data[i].id+'" selected="selected">'+data[i].service+' - '+data[i].name+'</option>'
                }
            }
            else {
                for (let i=0; i<data.length;i++){
                    display+='<option value="'+data[i].id+'">'+data[i].service+' - '+data[i].name+'</option>'
                }
            }

            display+="</select>";

            $('#intru_destinataire').empty().html(display).show();
            $('#carnet').chosen({width: '100%'})
        })
    }
    else{
        $('#intru_destinataire').empty().hide();
    }

})

// function qui si on est dans une manif va permettre de selectionner le document associé à la manif
$('#doc_objet').change(function (e) {
    let val = $(this).val();
    if (manif){
        if (val==='avis' || val==='preavis' || val==='arrete' || val==='recepisse' || val==='piece_jointe'){
            let url = url_doc_associe+"?manif="+manif+"&"+val+"=1";
            $.get(url, function (data) {
                let datadecode= JSON.parse(data);
                if (datadecode.length===0){
                    $('#list_doc_associe').chosen("destroy");
                    $('#list_doc_objet').empty().hide();
                }
                else{
                    let display = "<select id='list_doc_associe' class='chosen-select' required name='doc_associe_link'>" +
                        "<option selected disabled value=''>Choissez le document concerné</option>";
                    for (let k=0; k<datadecode.length; k++){
                        display+= "<option value='"+datadecode[k].pk+"'>"+datadecode[k].nom+"</option>"
                    }
                    display+="</select>";
                    $('#list_doc_objet').html(display).show();
                    $('#list_doc_associe').chosen({width: "100%"})
                }


            })
        }
        else{
            $('#list_doc_associe').chosen("destroy");
            $('#list_doc_objet').empty().hide();
        }
    }


});

// on va verifier que tout est renseigner avant d'envoyer le mail
var tentative_envoi = 0;
$('#envoi').click(function (e) {
    // function pour mettre le contenu de ckeditor directement dans le texte area pour l'envoi en ajax
    e.preventDefault()
    $("#id_corps").val(CKEDITOR.instances.id_corps.getData())
    if (url_encour === url_envoiunique){
        if (is_instru===1){
            if (($('#destinataire').val() == "" || $('#carnet').val() == "")|| !$('#doc_objet').val() || !$('#msg_objet').val()){
                if ($('#destinataire').length && $('#destinataire').val() == "" ){
                    $('#destinataire_chosen').css('border', 'red 1px solid')
                    $('#btn-modal-selecteur-carnet').css('border-color', 'red')
                }
                if ($('#carnet').length && $('#carnet').val() == "" ){
                    $('#carnet_chosen').css('border', 'red 1px solid')
                }
                if (!$('#doc_objet').val()){
                    $('#doc_objet_chosen').css('border', 'red 1px solid')
                }
                if (!$('#msg_objet').val()){
                    $('#msg_objet').css('border', "red 1px solid")
                }
            }
            else {
                send_le_mail();
            }
        }
        else {
            if (!$('#carnet').val().length || !$('#doc_objet').val() || !$('#msg_objet').val()){
                if (!$('#carnet').val().length){
                    $('#carnet_chosen').css('border', 'red 1px solid')
                }
                if (!$('#doc_objet').val()){
                    $('#doc_objet_chosen').css('border', 'red 1px solid')
                }
                if (!$('#msg_objet').val()){
                    $('#msg_objet').css('border', "red 1px solid")
                }
            }
            else {
                send_le_mail();
            }
        }
    }
    else {
        if ( !$('#msg_objet').val()){
            if (!$('#msg_objet').val()){
                $('#msg_objet').css('border', "red 1px solid")
            }
        }
        else {
            send_le_mail();
        }
    }


})
// envoi du mail, le problème étant que ckeditor, et l'ajax c'est pas ça.
// le premier envoi donnera un formulaire incomplet. on le renvoi donc une seconde fois.
// si la seconde fois n'est pas passé on arrete l'erreur n'étant plus celà
function send_le_mail() {
    var data = new FormData($('#formmail')[0]);
    $.ajax({
        url: url_encour+"?manif="+manif,
        type: 'POST',
        data : data,
          contentType: false,
          processData: false,
    }).done(function (response) {
        $('#msg_envoi_alert').html("<div class='alert alert-success'>Message envoyé, redirection en cours</div>")
        setTimeout(function () {
            $('#show_mail').trigger("click")
        },1000)
    }).fail(function (response) {
        if (response.status===403){
            $('#msg_envoi_alert').html(`<div class='alert alert-danger'>${response.responseText}</div>`)
        }
        else{
            $('#msg_envoi_alert').html("<div class='alert alert-danger'>Erreur d'envoi du message</div>")
        }
    })
}

// ici c'est la fonction qui va switch entre l'envoi de conversation ou d'actualité
var url_encour = url_envoiunique;
$('#actualite_toogle').click(function (e) {
    if (!$(this).is(':checked')) {
        $('#actu_block').hide()
        $('.desti_block').show()
        $('#doc_objet_block').show()
        $('#list_doc_objet').show()
        $("#btn-modal-selecteur-carnet").show()
        // $('#formmailactu').attr('id', 'formmail');
        url_encour = url_envoiunique;
    } else {
        $('#actu_block').show()
        $('.desti_block').hide()
        $('#doc_objet_block').hide()
        $('#list_doc_objet').hide()
        $("#btn-modal-selecteur-carnet").hide()
        // $('#formmail').attr('id', 'formmailactu')
        url_encour = url_envoi_actu
    }

})