import os
from datetime import timedelta
from post_office import mail

from django_cron import CronJobBase, Schedule
from django_cron.models import CronJobLog
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.conf import settings

from core.models import User, ConfigurationGlobale
from messagerie.models import CptMsg


class UpdateCPT(CronJobBase):
    """
    Mise à jour des compteur de message non lu
    """
    RUN_EVERY_MINS = settings.MESSAGERIE_CPT_TIME_REFRESH  # à lancer toutes les minutes

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'messagerie.UpdateCPT'    # a unique code

    def do(self):
        if not ConfigurationGlobale.objects.get(pk=1).UpdateCPT:
            return 'Cron désactivé'
        # Purger les logs
        CronJobLog.objects.filter(code="messagerie.UpdateCPT").filter(is_success=True).filter(
            end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        log_retour = ""
        last_time = timezone.now() - timedelta(minutes=settings.INACTIVE_TIME)
        total = get_user_model().objects.filter(last_visit__gte=last_time).count()
        for user in get_user_model().objects.filter(last_visit__gte=last_time):

            cpt = CptMsg.objects.get(utilisateur=user)
            cpt.update_cpt()
        log_retour += str(total) + " compteurs mis à jour"
        return log_retour


class SendRecap(CronJobBase):
    """
    Cron pour envoyer un message récapitulatif des msg reçu
    """
    RUN_AT_TIME = ['1:05']  # à lancer à 1h05

    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'messagerie.SendRecap'  # a unique code

    def do(self):

        if not ConfigurationGlobale.objects.get(pk=1).SendRecap:
            return 'Cron désactivé'
        # Purger les logs
        CronJobLog.objects.filter(code="messagerie.SendRecap").filter(is_success=True).filter(
            end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        log_retour = ""

        total = 0
        temps = timezone.now()
        C = ConfigurationGlobale.objects.get(pk=1)
        if os.path.exists('/tmp/verrou_cron_SendRecap'):
            return "Verrou en place pas d'envoi"
        os.system('touch /tmp/verrou_cron_SendRecap')
        # on n'a qu'un nombre limité de mail par minute qu'on peut envoyer on va donc separer les envoi de 1 min
        for user in User.objects.filter(is_active=True).order_by('username'):
            if user.recap:
                cpt = CptMsg.objects.get(utilisateur=user)
                convocation, action, info_suivi, conversation, news, forum = cpt.last_day()
                if action or conversation or info_suivi or forum or convocation:
                    total_cpt = action + conversation + convocation + info_suivi + news + forum
                    context = {
                        "total": total_cpt,
                        "action": action,
                        "conv": conversation,
                        "convoc": convocation,
                        "info": info_suivi,
                        "nouv": news,
                        "forum": forum
                    }
                    send = user.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
                    try:
                        mail.send(user.email, send, template="msg_recap", context=context, scheduled_time=temps)
                    except:
                        pass
                    total += 1
                    # Décaler l'envoi tous les x minutes
                    if total % C.recap_nbre == 0:
                        temps = temps + timezone.timedelta(minutes=C.recap_ecart)
        print(total)
        log_retour += str(total) + " mails envoyés"
        os.system("rm /tmp/verrou_cron_SendRecap")
        return log_retour
