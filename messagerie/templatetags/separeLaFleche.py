from django import template

register = template.Library()


@register.filter()
def separeLaFleche(value):
    """
    Ce tag va prendre un string s'il a une fleche on ne va prendre ce qui est avant.
    Cela pour empecher les organisateur d'avoir le nom et prenom des expediteur de message.
    """
    return value.split('→')[0]
