import os
import json
from datetime import timedelta

from django.utils import timezone
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse, HttpResponseForbidden, JsonResponse
from django.db.models import Q
from django.db.models import Subquery
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404
from django.conf import settings

from administrative_division.models import Departement, Region
from ..models import Enveloppe, CptMsg
from core.util.permissions import login_requis
from core.FileTypevalidator import file_type_valide_non_form
from core.models import User, Instance
from messagerie.crea_carnet import get_carnet
from messagerie.forms.messages import MessageForm
from messagerie.tasks import envoi_msg_celery
from instructions.models import DocumentOfficiel
from evenements.models import Manif, PieceJointe
from structure.models import Organisation, ServiceInstructeur, ServiceConsulte, StructureOrganisatrice, TypeService


@method_decorator(login_requis(), name='dispatch')
class ListeMessageView(View):
    """
    affichage des messages reçu par l'utilisateur
    Cette vue n'est appelée que apr ajax
    """

    def get(self, request):
        limit = request.GET['limit'] if request.GET.get('limit') else 10
        # dans le cas manif
        if request.GET.get('manif'):
            manif = get_object_or_404(Manif, pk=request.GET['manif'])
            # solution abandonée
            # si intructeur ou mairie agent on prend tout les message de la manif sauf les conv privée
            # if request.user.has_role('instructeur') or request.user.has_role('mairieagent'):
            #
            #     enveloppe = Enveloppe.objects.filter(
            #         pk__in=Subquery(Enveloppe.objects.filter(
            #             manifestation=manif).distinct('corps').all().values('pk'))).order_by('-date')
            #     # Cette requete exclu toutes les conversations dont l'user n'est ni expediteur ni destinataire
            #     enveloppe = enveloppe.exclude(Q(type='conversation'))
            #     conv = Enveloppe.objects.filter(
            #         pk__in=Subquery(Enveloppe.objects.filter(
            #             Q(destinataire_id=request.user) | Q(expediteur_id=request.user)).filter(
            #             manifestation=manif).filter(type="conversation").distinct('corps').all().values('pk'))).order_by('-date')
            #     enveloppe = enveloppe | conv

            # un user n'aura le droit qu'à ses propres messages
            enveloppe = Enveloppe.objects.filter(
                pk__in=Subquery(Enveloppe.objects.filter(
                    Q(destinataire_id=request.user) | Q(expediteur_id=request.user)).filter(
                    manifestation=manif).distinct('corps').all().values('pk'))).order_by('-date')

        # cas dans la messagerie générale.
        else:
            enveloppe = Enveloppe.objects.filter(
                pk__in=Subquery(Enveloppe.objects.filter(
                    Q(destinataire_id=request.user) | Q(expediteur_id=request.user)).distinct('corps').all().values('pk'))).order_by('-date')
            manif = None

        # on fait ensuite le tri en fonction des filtres de l'utilisateur

        enveloppe = enveloppe.filter(~Q(type='conversation')) if not request.GET.get('conv') else enveloppe
        enveloppe = enveloppe.filter(~Q(type='convocation')) if not request.GET.get('convoc') else enveloppe
        enveloppe = enveloppe.filter(~Q(type='news')) if not request.GET.get('nouv') else enveloppe
        enveloppe = enveloppe.filter(~Q(type='info_suivi')) if not request.GET.get('suivi') else enveloppe
        enveloppe = enveloppe.filter(~Q(type='action')) if not request.GET.get('action') else enveloppe
        enveloppe = enveloppe.filter(~Q(type='forum')) if not request.GET.get('forum') else enveloppe
        enveloppe = enveloppe.filter(~Q(type='tracabilite')) if not request.GET.get('trac') else enveloppe
        # TODO A supprimer une fois le type de message correctement réattribuer
        enveloppe = enveloppe.filter(~Q(type='traçabilité')) if not request.GET.get('trac') else enveloppe
        enveloppe = enveloppe.filter(~Q(doc_objet='dossier')) if not request.GET.get('dossier') else enveloppe
        enveloppe = enveloppe.filter(~Q(doc_objet='piece_jointe')) if not request.GET.get('docjoin') else enveloppe
        enveloppe = enveloppe.filter(~Q(doc_objet='avis')) if not request.GET.get('avis') else enveloppe
        enveloppe = enveloppe.filter(~Q(doc_objet='preavis')) if not request.GET.get('preavis') else enveloppe
        enveloppe = enveloppe.filter(~Q(doc_objet='arrete')) if not request.GET.get('arrete') else enveloppe
        enveloppe = enveloppe.filter(~Q(doc_objet='recepisse')) if not request.GET.get('recepisse') else enveloppe

        enveloppe = enveloppe.filter(~Q(destinataire_id=request.user)) if not request.GET.get('recu') else enveloppe
        enveloppe = enveloppe.filter(~Q(expediteur_id=request.user)) if not request.GET.get('envoi') else enveloppe

        enveloppe = enveloppe.filter(~Q(lu_requis=False)).filter(~Q(lu_datetime__isnull=False)).filter(~Q(expediteur_id=request.user)) if not \
            request.GET.get('nonlu') else enveloppe

        # on compte le nombre d'enveloppe puis on trie selon la limite d'enveloppe affichées
        max_enveloppe = len(enveloppe)
        enveloppe = enveloppe.all()[:int(limit)] if enveloppe else ''

        tab_env = []

        for envel in enveloppe:
            if not envel.type == "news" or not envel.expediteur_id == request.user:
                tab_env.append(envel)
            else:
                enve_desti = Enveloppe.objects.filter(corps=envel.corps).filter(~Q(destinataire_id=request.user))
                if not enve_desti:
                    tab_env.append(envel)
                else:
                    enve_desti = enve_desti.first()
                    tab_env.append(enve_desti)

        service = Organisation.objects.get(pk=self.request.session['service_pk'])

        context = {
            'manif': manif,
            'messages': tab_env,
            'max': max_enveloppe,
            'service': service
        }
        return render(request, 'messagerie/liste_message.html', context)


@method_decorator(login_requis(), name='dispatch')
class MessageView(View):
    """
    on affiche un message
    """

    def get(self, request, *args, **kwargs):
        enveloppe = get_object_or_404(Enveloppe, pk=self.kwargs.get('pk'))
        env_destinataires = Enveloppe.objects.filter(corps=enveloppe.corps).all()
        if not request.user.has_group("Administrateurs technique") and not (request.user == enveloppe.destinataire_id or request.user == enveloppe.expediteur_id):
            return render(request, "core/access_restricted.html",
                          {'message': "Vous n'avez pas accès à cette page"}, status=403)


        destinataires = []
        # ici on va modifié le nom des destinataires pour n'afficher que le service pour les organisateurs
        # pour le reste ou affiche service + nom, prenom s'il y a
        if not enveloppe.type == "news":
            for enve in env_destinataires:
                if request.organisation.is_structure_organisatrice():
                    service = enve.destinataire_txt.split('→')[0]
                    lu = True
                    destinataires.append({"service": service, "lu": lu}) if len(list(filter(lambda sch: sch['service'] == service, destinataires))) == 0 else ""
                else:
                    service_deja_ajoute = False
                    nom = enve.destinataire_txt.split('→')[1] if (len(enve.destinataire_txt.split('→')) > 1 and
                                                                  (not enve.destinataire_txt.split('→')[1] == '  ' and
                                                                   not enve.destinataire_txt.split('→')[1] == ' ')) else False
                    for user in destinataires:
                        if user.get("service", None) == enve.destinataire_txt.split('→')[0].strip():
                            service_deja_ajoute = True
                            lu = False if not enve.lu_datetime and enve.lu_requis else True
                            if lu:
                                user.get("utilisateur_lu", None) .append({"nom": nom})
                            else:
                                user.get("utilisateur_nonlu", None).append({"nom": nom})
                    if not service_deja_ajoute:
                        service = enve.destinataire_txt.split('→')[0].strip()
                        lu = False if not enve.lu_datetime and enve.lu_requis else True
                        if lu:
                            destinataires.append({"service": service, "utilisateur_lu": [{"nom": nom}], "utilisateur_nonlu": []})
                        else:
                            destinataires.append({"service": service, "utilisateur_lu": [], "utilisateur_nonlu": [{"nom": nom}]})

        # si on a une conversation on va chercher les message de cette conversation
        if enveloppe.reponse:
            old = Enveloppe.objects.filter(Q(reponse=enveloppe.reponse) | Q(pk=enveloppe.reponse)).filter(date__lt=enveloppe.date).order_by("date")
            new = Enveloppe.objects.filter(reponse=enveloppe.reponse).filter(date__gt=enveloppe.date).order_by("date")
        else:
            new = Enveloppe.objects.filter(reponse=enveloppe.pk).order_by("date")
            old = []
        orga = True if request.organisation.is_structure_organisatrice() else False

        context = {
            'enveloppe': enveloppe,
            'new': new,
            "old": old,
            'destinataires': destinataires,
            'organisateur': orga
        }
        # Dans le cas d'un accès direct au lien pour une nouvelle fenêtre
        is_ajax = request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'
        if not is_ajax:
            context['fenetre'] = 1
            return render(request, 'messagerie/message_fenetre.html', context)
        else:
            return render(request, 'messagerie/message_ajax.html', context)

    def is_ajax(self, request):
        return request == request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'


@method_decorator(login_requis(), name='dispatch')
class LuMessage(View):
    """
    le message est lu
    """

    def post(self, request):
        # car on lu une enveloppe
        if request.GET.get('msg', None):
            enveloppe = get_object_or_404(Enveloppe, pk=request.GET.get('msg', None))
            if enveloppe.destinataire_id == request.user:
                enveloppe.lu_datetime = timezone.now()
                enveloppe.save()
                cpt = CptMsg.objects.get(utilisateur=request.user)
                cpt.update_cpt()

            elif enveloppe.type == "news":
                enve = Enveloppe.objects.filter(corps=enveloppe.corps, destinataire_id=request.user)
                if enve:
                    enve = enve.get()
                    enve.lu_datetime = timezone.now()
                    enve.save()
                    cpt = CptMsg.objects.get(utilisateur=request.user)
                    cpt.update_cpt()
        elif request.GET.get('nonlu', None):
            enveloppe = get_object_or_404(Enveloppe, pk=request.GET.get('nonlu', None))
            if enveloppe.destinataire_id == request.user:
                enveloppe.lu_datetime = None
                enveloppe.save()
                cpt = CptMsg.objects.get(utilisateur=request.user)
                cpt.update_cpt()
        # ca où on va lire toute les enveloppes d'un filtre
        elif request.GET.get("type", None) == 'all':
            enveloppes = Enveloppe.objects.filter(destinataire_id=request.user, lu_datetime__isnull=True)
            enveloppes = enveloppes.filter(~Q(type='conversation')) if not request.GET.get('conv') else enveloppes
            enveloppes = enveloppes.filter(~Q(type='convocation')) if not request.GET.get('convoc') else enveloppes
            enveloppes = enveloppes.filter(~Q(type='news')) if not request.GET.get('nouv') else enveloppes
            enveloppes = enveloppes.filter(~Q(type='info_suivi')) if not request.GET.get('suivi') else enveloppes
            enveloppes = enveloppes.filter(~Q(type='action')) if not request.GET.get('action') else enveloppes
            enveloppes = enveloppes.filter(~Q(type='tracabilite')) if not request.GET.get('trac') else enveloppes

            enveloppes = enveloppes.filter(~Q(doc_objet='dossier')) if not request.GET.get('dossier') else enveloppes
            enveloppes = enveloppes.filter(~Q(doc_objet='piece_jointe')) if not request.GET.get('docjoin') else enveloppes
            enveloppes = enveloppes.filter(~Q(doc_objet='avis')) if not request.GET.get('avis') else enveloppes
            enveloppes = enveloppes.filter(~Q(doc_objet='preavis')) if not request.GET.get('preavis') else enveloppes
            enveloppes = enveloppes.filter(~Q(doc_objet='arrete')) if not request.GET.get('arrete') else enveloppes
            enveloppes = enveloppes.filter(~Q(doc_objet='recepisse')) if not request.GET.get('recepisse') else enveloppes

            for enveloppe in enveloppes:
                enveloppe.lu_datetime = timezone.now()
                enveloppe.save()

        return HttpResponse(200)


@method_decorator(login_requis(), name='dispatch')
class ListeDocConcerne(View):
    """
    envoi de la liste des docs demandé ajax
    pk+ nom str (peut etre le service concerné ou le type)
    """

    def get(self, request):
        manif = get_object_or_404(Manif, pk=request.GET['manif'])
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        reponses = []

        if request.GET.get('avis'):
            if manif.get_instruction(instance=organisation.instance_fk):
                if type(organisation) == ServiceInstructeur:
                    for avi in manif.get_instruction(instance=organisation.instance_fk).avis.all():
                        reponse = {"nom": str(avi), "pk": avi.pk}
                        reponses.append(reponse)
                elif type(organisation) == ServiceConsulte:
                    avis = manif.get_instruction(instance=organisation.instance_fk).get_avis_service(organisation)
                    if avis:
                        reponse = {"nom": str(avis), "pk": avis.pk}
                        reponses.append(reponse)
                else:
                    return HttpResponseForbidden(403)
                reponsesjson = json.dumps(reponses)
                return HttpResponse(reponsesjson)
        elif request.GET.get('arrete'):
            docs = DocumentOfficiel.objects.filter(instruction__manif=manif)
            docs = docs.filter(~Q(nature=3))
            for doc in docs:
                reponse = {"nom": str(doc.get_nature_nom()), "pk": doc.pk}
                reponses.append(reponse)
            reponsesjson = json.dumps(reponses)
            return HttpResponse(reponsesjson)
        elif request.GET.get('recepisse'):
            docs = DocumentOfficiel.objects.filter(instruction__manif=manif, nature=3)
            for doc in docs:
                reponse = {"nom": str(doc.get_nature_nom()), "pk": doc.pk}
                reponses.append(reponse)
            reponsesjson = json.dumps(reponses)
            return HttpResponse(reponsesjson)
        elif request.GET.get('piece_jointe'):
            docs = PieceJointe.objects.filter(manif=manif, champ_cerfa__startswith="demande_instructeur")
            for doc in docs:
                reponse = {"nom": doc.nom, "pk": doc.pk}
                reponses.append(reponse)
            reponsesjson = json.dumps(reponses)
            return HttpResponse(reponsesjson)
        return HttpResponse([])


@method_decorator(login_requis(), name='dispatch')
class CarnetInstructeur(View):
    """
    création du carnet des instructeur selon le service qu'ils ont séléctionné
    """

    def get(self, request):
        organisation = Organisation.objects.get(pk=request.session['service_pk'])
        if request.GET.get('idservice'):
            dep = organisation.instance_fk.departement
            if request.GET.get('dep', '').isdigit():
                try:
                    dep = Departement.objects.get(pk=request.GET['dep'])
                except:
                    pass
            carnet = []
            last_year = timezone.now() - timedelta(days=360)
            param_ajax = request.GET['idservice']
            manif = request.GET.get('manif')
            param_ajax_els = param_ajax.split('_')

            if param_ajax_els[0] == 'orga':
                # ici on va prendre les organisateurs qui ont été connectés cette années
                orgas = []
                if param_ajax_els[1] == "new":
                    # TODO V6 corriger
                    pass
                    # orgas = Organisateur.objects.filter(user__default_instance__departement=dep,
                    #                                     user__last_login__gte=last_year)
                # ici on va prendre les organisateurs qui n'ont PAS été connectés cette années
                elif param_ajax_els[1] == "old":
                    # TODO V6 corriger
                    pass
                    # orgas = Organisateur.objects.filter(user__default_instance__departement=dep,
                    #                                     user__last_login__lt=last_year)
                # organisateur de la manifestation
                else:
                    manif = get_object_or_404(Manif, pk=manif)
                    orgas = manif.structure_organisatrice_fk.get_users_list()
                for orga in orgas:
                    user = orga.user
                    if user.is_active:
                        if hasattr(orga, 'structure'):
                            carnet.append({"id": orga.pk,
                                           "service": orga.__class__.__name__,
                                           "name": str(orga.structure) + ' - ' + orga.user.get_full_name(),
                                           "data": ""})
                        else:
                            carnet.append({"id": orga.pk,
                                           "service": orga.__class__.__name__,
                                           "name": 'Pas de structure - ' + orga.user.get_full_name(),
                                           "data": ""})

            elif param_ajax_els[0] == 'instructeurs':
                manif = get_object_or_404(Manif, pk=manif)
                if manif.get_instruction(instance=organisation.instance_fk):
                    liste = manif.get_instructeurs_manif()
                    for recipient in liste:
                        if isinstance(recipient, list):
                            if isinstance(recipient, User) and recipient.is_active:
                                carnet.append({"id": recipient.pk,
                                               "service": "User",
                                               "name":  " - " + recipient.get_full_name(),
                                               "data": ""})

            return JsonResponse(carnet, safe=False)
        else:
            return HttpResponseForbidden(403)


@method_decorator(login_requis(), name='dispatch')
class EnvoiMessageActualite(View):
    """
    Envoi d'une actualité
    """

    def post(self, request):
        ad_tech = request.user.groups.filter(name="_support").exists()
        ad_instance = request.user.groups.filter(name="Administrateurs d'instance").exists()
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        if ad_instance or ad_tech:
            # si admin terch on a le droit d'envoyer à tous
            if ad_tech:
                departement = request.POST.getlist('departement')
            else:
                # si admin instance on ne peut envoyer qu'à son departement
                departement = [request.user.get_departement().pk]
            role = request.POST.getlist('role')
            formulaire = MessageForm(request.POST)

            if formulaire.is_valid():

                msg_json = {
                    'expediteur': [request.user.pk, organisation.pk],
                    'msg': formulaire.cleaned_data['corps'],
                    'objet': request.POST['objet'],
                    'option': {
                        'doc_type': None,
                        'doc_pk': None,
                    },
                    'type': "news",
                    'manif': None,
                    'destinataire': {
                        'dep': departement,
                        'role': role,
                    },
                }
                if settings.CELERY_ENABLED:
                    envoi_msg_celery.delay(msg_json=msg_json, type_envoi='actualite')
                else:
                    envoi_msg_celery(msg_json=msg_json, type_envoi='actualite')
                return HttpResponse(200)
            else:
                return HttpResponseForbidden(403)
        else:
            return HttpResponseForbidden(403)


@method_decorator(login_requis(), name='dispatch')
class EnvoiMessage(View):
    """
    Envoi un message à l'utilisateur
    """
    # affichage de la boite d'envoi

    def get(self, request):
        formulaire = MessageForm()
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])

        admin_tech = request.user.groups.filter(name="_support").exists()
        admin_instance = request.user.groups.filter(name="Administrateurs d'instance").exists()
        if request.GET.get('manif'):
            manif = request.GET['manif']
            manif = get_object_or_404(Manif, pk=manif)
        else:
            manif = None

        if request.organisation.is_service_instructeur() or admin_instance or admin_tech:
            est_instructeur = True
        else:
            est_instructeur = False
        # si la personne est instructeur elle a accès à la recherche et l'envoi par service
        if not organisation.is_structure_organisatrice():
            instru = 1
            carnet = []
        else:
            instru = 0
            carnet = get_carnet(request, manif)
        dep = Departement.objects.all() if est_instructeur else None
        mydep = request.user.get_departement()
        role_actu = [["admin_instance", "Tous les administrateurs d'instance"], ["organisateur", "Tous les organisateurs"],
                     ["instru_sans_mairie", " Tous les instructeurs sans les mairies"], ["instru_avec_marie", "Tous les instructeurs avec les mairies"],
                     ["consult", "Tous les services consultés"], ["consult_famille", "Tous les services consultés en famille"],
                     ["consult_indep", "Tous les services consultés indépendants"]]
        for t in TypeService.objects.exclude(pk__in=[1,2]).order_by("categorie_fk__nom_s", "nom_s"):
            role_actu.append([t.pk, f"{str(t.categorie_fk)} / {str(t)}"])
        ad_tech = request.user.groups.filter(name="_support").exists()
        ad_instance = request.user.groups.filter(name="Administrateurs d'instance").exists()
        context = {
            "est_instructeur": est_instructeur,
            'form': formulaire,
            'manif': manif,
            'carnet': carnet,
            'instru': instru,
            'deps': dep,
            "mydep": mydep,
            'roles': role_actu,
            'ad_tech': ad_tech,
            'ad_instance': ad_instance,
            "instances": Instance.objects.configured().order_by('departement__name'),
            'regions': Region.objects.all().order_by('nom'),
        }
        return render(request, 'messagerie/envoi_message.html', context)

    # l'envoi de conversation
    def post(self, request):
        destinataire_liste = request.POST.getlist('destinataire_liste')
        destinataires = request.POST.getlist('destinataires')
        doc_objet = request.POST.getlist('doc_objet')
        doc_objet = doc_objet[0] if doc_objet else None
        doc_objet = None if doc_objet == "aucun" else doc_objet
        doc_associe = request.POST.getlist('doc_associe_link')
        doc_associe = doc_associe[0] if doc_associe else None
        fichier_joint = request.FILES.get('fichier', None)
        formulaire = MessageForm(request.POST)
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])

        if doc_associe is None:
            action_pk = None
            action_type = None
        elif doc_objet == 'avis':
            action_pk = doc_associe
            action_type = 'avis'
        elif doc_objet == 'preavis':
            action_pk = doc_associe
            action_type = 'preavis'
        elif doc_objet == 'piece_jointe':
            action_pk = doc_associe
            action_type = 'doc'
        elif doc_objet == 'arrete':
            action_pk = doc_associe
            action_type = 'offi'
        elif doc_objet == 'recepisse':
            action_pk = doc_associe
            action_type = 'offi'
        else:
            action_pk = None
            action_type = None

        if fichier_joint and not file_type_valide_non_form(request.FILES.get('fichier')):
            return HttpResponseForbidden("Format de fichier incorrect", status=403)

        if formulaire.is_valid():
            if fichier_joint:
                if not os.path.exists(settings.MEDIA_ROOT + 'tmp'):
                    os.makedirs(settings.MEDIA_ROOT + "tmp")
                from django.core.files.storage import FileSystemStorage
                fs = FileSystemStorage(location=f'{settings.MEDIA_ROOT}tmp/')
                nom_fichier = f"{settings.MEDIA_ROOT}tmp/{fs.save(fichier_joint.name, fichier_joint)}"
            else:
                nom_fichier = ""

            msg_json = {
                'expediteur': [request.user.pk, organisation.pk],
                'msg': formulaire.cleaned_data['corps'],
                'objet': request.POST['objet'],
                'option': {
                    'doc_type': action_type,
                    'doc_pk': action_pk,
                    'doc_objet': doc_objet,
                },
                'type': "conversation",
                'manif': request.GET.get('manif', None),
                'fichier': nom_fichier
            }
            if destinataires:
                # si des destinataires precis ont été selectionner
                msg_json['destinataire'] = destinataires
                if settings.CELERY_ENABLED:
                    envoi_msg_celery.delay(msg_json=msg_json, type_envoi='destinataire_user')
                else:
                    # cas de test
                    envoi_msg_celery(msg_json, "destinataire_user")

            else:
                # si liste de service et utilisateur
                msg_json['destinataire'] = [{'service': destinataire.split('_')[1], 'pk': destinataire.split('_')[0]}
                                            for destinataire in destinataire_liste]
                if settings.CELERY_ENABLED:
                    envoi_msg_celery.delay(msg_json, 'destinataire_service')
                else:
                    envoi_msg_celery(msg_json, 'destinataire_service')

            return HttpResponse(200)
        else:
            return HttpResponseForbidden("Message vide ou manquant", status=403)


@method_decorator(login_requis(), name='dispatch')
class AffichageMessagerie(View):
    """
    affichage messagerie
    """

    def get(self, request):
        if request.GET.get('manif'):
            manif = request.GET['manif']
            manif = get_object_or_404(Manif, pk=manif)
        else:
            manif = None

        if request.user.has_group('Administrateurs d\'instance') or request.user.has_group('_support'):
            actu = 1
        else:
            actu = 0
        context = {
            'manif': manif,
            "actu": actu,
        }
        return render(request, 'messagerie/messagerie.html', context)


class EnvoiMessageReponse(View):
    """
    Envoi d'une réponse (différe du message normal)
    """

    @method_decorator(login_requis())
    def dispatch(self, request, *args, **kwargs):
        pk = request.GET.get('pk')
        if pk:
            request.enve = get_object_or_404(Enveloppe, pk=pk)
            return super().dispatch(request, *args, **kwargs)
        else:
            return HttpResponseForbidden(403)

    def get(self, request):
        formulaire = MessageForm()
        is_manif = True if request.enve.manifestation else False
        context = {
            'enve': request.enve,
            'form': formulaire,
            'is_manif': is_manif
        }
        return render(request, 'messagerie/envoi_message_reponse.html', context)

    def post(self, request):
        enve = request.enve
        formulaire = MessageForm(request.POST)
        fichier_joint = request.FILES.get('fichier', None)
        organisation = Organisation.objects.get(pk=self.request.session['service_pk'])
        if fichier_joint and not file_type_valide_non_form(request.FILES.get('fichier')):
            return HttpResponseForbidden("Format de fichier incorrect", status=403)
        if formulaire.is_valid():
            if request.user == enve.destinataire_id:
                destinataire = enve.expediteur_id
            else:
                destinataire = enve.destinataire_id
            # on va mettre le pk du premier msg de la conversation
            original = enve.reponse if enve.reponse else enve.pk
            manif = enve.manifestation.pk if enve.manifestation else None

            if fichier_joint and manif:
                if not os.path.exists(settings.MEDIA_ROOT + 'tmp'):
                    os.makedirs(settings.MEDIA_ROOT + "tmp")
                from django.core.files.storage import FileSystemStorage
                fs = FileSystemStorage(location=f'{settings.MEDIA_ROOT}tmp/')
                nom_fichier = f"{settings.MEDIA_ROOT}tmp/{fs.save(fichier_joint.name, fichier_joint)}"
            else:
                nom_fichier = ""

            msg_json = {
                'expediteur': [request.user.pk, organisation.pk],
                'msg': formulaire.cleaned_data['corps'],
                'objet': request.POST['objet'],
                'original': original,
                'option': {
                    'doc_type': None,
                    'doc_pk': None,
                    'doc_objet': enve.doc_objet,
                },
                'type': "conversation",
                'manif': manif,
                'fichier': nom_fichier,
                'destinataire': [destinataire.pk],
            }

            if settings.CELERY_ENABLED:
                envoi_msg_celery.delay(msg_json=msg_json, type_envoi='destinataire_user')
            else:
                envoi_msg_celery(msg_json=msg_json, type_envoi='destinataire_user')
            return HttpResponse(200)
        else:
            return HttpResponseForbidden(403)


@method_decorator(login_requis(), name='dispatch')
class PageMessagerieGlobale(View):
    """
    Affichage du webmail n'étant pas exclusif à une manifestation
    """

    def get(self, request):
        return render(request, 'messagerie/messagerie_globale.html')
