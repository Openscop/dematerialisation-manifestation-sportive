import json

from django.views.generic import View
from django.shortcuts import redirect, render
from django.http import HttpResponseForbidden
from django.conf import settings
from post_office import mail

from ..models import CptMsg


class CptView(View):
    """
    envoi des donnée cpt
    """
    def get(self, request, pk):
        if request.user.is_authenticated:
            cpt = CptMsg.objects.get(utilisateur=request.user)
            data = {
                "convocation": cpt.convocation,
                "action": cpt.action,
                "info_suivi": cpt.info_suivi,
                "conversation": cpt.conversation,
                "news": cpt.news,
                "forum": cpt.forum
            }
            return render(request, 'messagerie/compteurnonlu.html', data)
        else:
            return HttpResponseForbidden(403)


class EnvoiMessageRecap(View):
    """
    Envoi d'un message de recap lors de la demande de l'utilisateur
    """
    def get(self, request):
        if not request.user.is_authenticated:
            return HttpResponseForbidden(403)
        user = request.user
        if user.recap:
            cpt = CptMsg.objects.get(utilisateur=user)
            if cpt.action or cpt.convocation or cpt.conversation or cpt.info_suivi or cpt.news or cpt.forum:
                total = cpt.action + cpt.convocation + cpt.conversation + cpt.info_suivi + cpt.news
                action = cpt.action
                conv = cpt.conversation
                convoc = cpt.convocation
                info = cpt.info_suivi
                nouv = cpt.news
                forum = cpt.forum
                context = {
                    "total": total,
                    "action": action,
                    "conv": conv,
                    "convoc": convoc,
                    "info": info,
                    "nouv": nouv,
                    "forum": forum
                }
                send = user.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
                mail.send(user.email, send, template="msg_recap", context=context)
        return redirect('profile')
