from __future__ import absolute_import, unicode_literals
from configuration.celery import app

from django.utils import timezone
from django.db.models import Q
from django.apps import apps

from core.models import User
from core.tasks import pj_messagerie
from evenements.models.manifestation import Manif
from messagerie.models import Message, Enveloppe
from administrative_division.models import Departement
from structure.models import ServiceConsulte, Organisation, StructureOrganisatrice, ServiceInstructeur


@app.task(bind=True, queue="default")
def envoi_msg_celery(self, msg_json, type_envoi):
    """
    Fonction pour envoyer un message via les pages de messagerie
    :param msg_json: msg_json = {
                    'expediteur': expediteur,
                    'msg': corps du messages,
                    'objet': objet,
                    'option': {
                        'doc_pk': objet_lie_pk,
                        'doc_objet': objet_lie_nature,
                    },
                    'type': type_msg",
                    'manif': pk de la manif,
                    'destinataire': destinataires,
                    'reponse': pk de la reponse
                }
    Tout doit être sous format str ou serialisable
    :param type_envoi: type du message à envoyer (actualite, destinataire_service, destinataire_user)
    """

    tab_desti = []
    expediteur = User.objects.get(pk=msg_json['expediteur'][0])
    expediteur_service = Organisation.objects.get(pk=msg_json['expediteur'][1])
    manif = Manif.objects.get(pk=msg_json['manif']) if msg_json['manif'] else None
    reponse = msg_json.get('original', None)
    fichier = msg_json.get('fichier', None)

    if type_envoi == 'actualite':
        # création de la liste destinataire

        # ici si l'user a choisi un departement precis ou non
        if msg_json['destinataire']['dep']:
            userindep = User.objects.none()
            for departement in msg_json['destinataire']['dep']:
                if type(departement) == 'str':
                    departement = int(departement)
                obj_dep = Departement.objects.filter(pk=departement)
                useradd = User.objects.filter(Q(default_instance__departement__in=obj_dep))
                # Cas où le departement reçoit une liste vide pour eviter les erreurs dans le second filtre
                userindep = userindep | useradd
        else:
            userindep = User.objects.all()

        # ici on va filtre ou non selon le role choisi
        if msg_json['destinataire']['role']:
            for user in userindep:
                for role in msg_json['destinataire']['role']:
                    if role.isdigit():
                       for organisation in user.organisation_m2m.all():
                           if type(organisation) == ServiceConsulte and organisation.type_service_fk.pk == int(role):
                               tab_desti.append(user) if not user in tab_desti else ""
                    elif role == "admin_instance":
                        if user.has_group("Administrateurs d'instance"):
                            tab_desti.append(user) if not user in tab_desti else ""
                    elif role == "organisateur":
                        if user.organisation_m2m.instance_of(StructureOrganisatrice):
                            tab_desti.append(user) if not user in tab_desti else ""
                    elif role == "instru_sans_mairie":
                        if user.organisation_m2m.instance_of(ServiceInstructeur):
                            for organisation in user.organisation_m2m.instance_of(ServiceInstructeur):
                                tab_desti.append(user) if not user in tab_desti and not organisation.is_mairie() else ""
                    elif role == "instru_avec_marie":
                        if user.organisation_m2m.instance_of(ServiceInstructeur):
                            tab_desti.append(user) if not user in tab_desti else ""
                    elif role == "consult":
                        if user.organisation_m2m.instance_of(ServiceConsulte):
                            tab_desti.append(user) if not user in tab_desti else ""
                    elif role == "consult_famille":
                        if user.organisation_m2m.instance_of(ServiceConsulte):
                            for organisation in user.organisation_m2m.instance_of(ServiceConsulte):
                                tab_desti.append(user) if not user in tab_desti and organisation.is_membre_famille() else ""
                    elif role == "consult_indep":
                        if user.organisation_m2m.instance_of(ServiceConsulte):
                            for organisation in user.organisation_m2m.instance_of(ServiceConsulte):
                                tab_desti.append(user) if not user in tab_desti and not organisation.is_membre_famille() else ""

        else:
            for user in userindep:
                tab_desti.append(user)

        message = Message.objects.creer_et_envoyer(msg_json['type'], [expediteur, expediteur_service], tab_desti, msg_json['objet'],
                                                   msg_json['msg'])

    else:
        users = []

        # cas où on envoi le message à une liste d'utilisateur
        if manif:
            instruction = manif.get_instruction(instance=expediteur.get_instance())
            if expediteur_service.is_service_instructeur() and instruction and expediteur in instruction.get_instructeurs():
                instruction.referent = expediteur
                instruction.save()

        if type_envoi == "destinataire_user":
            for destinataire in msg_json['destinataire']:
                if isinstance(destinataire, str) and "_" in destinataire:
                    service = Organisation.objects.get(pk=destinataire.split('_')[1])
                    for user in service.get_users_list():
                        users.append([user, service])
                else:
                    user = User.objects.get(pk=destinataire)
                    users += [user]
        else:
            for destinataire in msg_json['destinataire']:
                if destinataire['service'] == "Organisation":
                    service = Organisation.objects.get(pk=destinataire['pk'])
                    for user in service.get_users_list():
                        users.append([user, service])
                else:
                    user = User.objects.get(pk=destinataire['pk'])
                    user_service = Organisation.objects.get(pk=destinataire['service'])
                    users.append([user, user_service])

        for user in users:
            tab_desti.append(user)
        message = Message.objects.creer_et_envoyer(msg_json['type'], [expediteur, expediteur_service], tab_desti, msg_json['objet'],
                                                   msg_json['msg'], manifestation_liee=manif,
                                                   objet_lie_pk=msg_json['option']['doc_pk'],
                                                   objet_lie_nature=msg_json['option']['doc_objet'],
                                                   reponse_a_pk=reponse, fichier=fichier)
        if message.fichier:
            pj_messagerie.delay(pk_manif=manif.pk, pk_msg=message.pk)

    # ici on va voir si l'utilisateur s'est envoyé lui même le message on le passera en lu
    if message and expediteur:
        env_auto_envoye = Enveloppe.objects.filter(expediteur_id=expediteur, destinataire_id=expediteur, corps=message)
        if env_auto_envoye:
            env_auto_envoye = env_auto_envoye.get()
            env_auto_envoye.lu_datetime = timezone.now()
            env_auto_envoye.save()
