from forum.urls import urlpatterns as machina_urls

from django.conf import settings
from django.urls import include, path, re_path
from django.conf.urls.static import static
from django.http import HttpResponseBadRequest, HttpResponseForbidden

from django.contrib import admin
from django.contrib.sitemaps import views as sitemaps_views
from django.contrib.flatpages.sitemaps import FlatPageSitemap
from core.util.dynamic_static import staticfiles_dynamicurlpatterns
from core.views.user import (ProfileDetailView, UserUpdateView, signupagentview, signuporganisateurview, login,
                             EmailConfirmedView, NomPrenomChange, PasswordExpired, PasswordChangeCustomView,
                             RenvoiMailConfirm, PasswordResetCustomView, ProfilPublicView)
from core.views.instance import SendMailVerifInstance
from core.views.optionuser import ParametreNotificationMessagerie
from core.views.media import media_access
from core.views.logs_SMTP import ListLogUser, LogsDetailView, ResolutionLog
from portail.views.index import HomePage
from forum.forum_conversation.view import ForumAbonnementMultiUpdateView

admin.autodiscover()

sitemaps = {
    'flatpages': FlatPageSitemap,
}
handler400 = 'portail.views.erreur400view'
handler403 = 'portail.views.erreur403view'
handler404 = 'portail.views.erreur404view'
handler500 = 'portail.views.erreur500view'

urlpatterns = [
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('sitemap\.xml', sitemaps_views.sitemap, {'sitemaps': sitemaps}),
    path('accounts/signup/', HttpResponseBadRequest),
    path('accounts/login/', login, name='account_login'),
    path('accounts/password/change/', PasswordChangeCustomView.as_view(), name='password_change_custom'),
    path('accounts/password/reset/', PasswordResetCustomView.as_view(), name='password_reset_custom'),
    path('accounts/', include('allauth.urls')),
    path('accounts/passwordexpired', PasswordExpired.as_view(), name="password_expired"),
    path('accounts/send_confirm_mail/<int:pk>/', RenvoiMailConfirm.as_view(), name="send_confirm_mail"),
    path('accounts/email_confirmed', EmailConfirmedView.as_view(), name='email_confirmed'),
    path('profile/<int:pk>/', ProfilPublicView.as_view(), name='profile_publique'),
    path('accounts/profile/', ProfileDetailView.as_view(), name='profile'),
    path('accounts/profile/edit', UserUpdateView.as_view(), name='profile_edit'),
    path('accounts/profilte/name', NomPrenomChange.as_view(), name="nom_prenom_change"),
    path('accounts/profile/notification/edit', ParametreNotificationMessagerie.as_view(), name='parametre_notification_messagerie'),
    path('accounts/profile/gerer_mes_abonnements', ForumAbonnementMultiUpdateView.as_view(), name='gerer_mes_abonnements'),
    path('inscription/agent', signupagentview, name='inscription_agent'),
    path('inscription/organisateur', signuporganisateurview, name='inscription_organisateur'),
    re_path('core/instance/(?P<pk>\d+)/', SendMailVerifInstance.as_view(), name='verif_mail_instance'),
    path('carto/', include('carto.urls')),
    path('instructions/', include('instructions.urls')),
    path('incidence/', include('evaluation_incidence.urls')),
    path('sports/', include('sports.urls')),
    path('administrative_division/', include('administrative_division.urls')),
    path('', include('evenements.urls')),
    path('ajax_select/', include('ajax_select.urls')),
    path('', include('portail.urls')),
    path('', HomePage.as_view(), name='home_page'),
    path('aide/', include('aide.urls')),
    path('api/', include('api.urls')),
    path('messagerie/', include('messagerie.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('o/applications/', HttpResponseForbidden, name="creaapp"),  # surcharge d'oauth pour eviter la création d'application
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path(r'webpush/', include('webpush.urls')),
    path('forum/', include(machina_urls), name='forum'),
    path('list_logs/', ListLogUser.as_view(), name='list_logs'),
    path('detail_log/<int:pk>/', LogsDetailView.as_view(), name='detail_log'),
    path('detail_log/upd/<int:pk>/', ResolutionLog.as_view(), name='resolution_log'),
    path('structure/', include('structure.urls'), name='structure.urls'),
]

# ici on choisis l'url media qu'on veut utiliser en debug, les non protegés sinon les protegés
if settings.DEBUG and not settings.TESTS_IN_PROGRESS:
    import debug_toolbar
    urlpatterns = [path('__debug__/', include(debug_toolbar.urls))] + urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += staticfiles_dynamicurlpatterns(settings.STATIC_ROOT_URL)
else:
    urlpatterns += re_path(r'^media/(?P<path>.*)', media_access, name='media'),

# Les flatpages doivent être toujours après les média pour eviter une interceptions des demandes de media
urlpatterns += path('', include('django.contrib.flatpages.urls')),
