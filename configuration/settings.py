# coding: utf-8
import logging
import sys
import os
from collections import OrderedDict
import yaml
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.celery import CeleryIntegration

from core.util.migrations import DisableMigrations
from core.util.dynamic_static import get_static_url_now
from configuration.directory import Paths
from configuration import thirdparty

from forum import MACHINA_MAIN_TEMPLATE_DIR
from forum import MACHINA_MAIN_STATIC_DIR


# Get the local settings of the server
server_settings = {}
SETTINGS_FILE = 'SETTINGS_FILE' in os.environ and os.environ['SETTINGS_FILE'] or '/etc/django/settings-manifestationsportive.yaml'
try:
    with open(SETTINGS_FILE, 'r') as f:
        server_settings = yaml.load(f, Loader=yaml.FullLoader)
except FileNotFoundError:
    print('No local settings.')
    pass

try:
    with open('version', 'r') as f:
        VERSION = f.read()
except FileNotFoundError:
    print('pas de numéro de version')
    VERSION = 'Aucune'


# DJANGO BASE SETTINGS
# =========================
# Sécurité
SECRET_KEY = 'SECRET_KEY' in server_settings and server_settings['SECRET_KEY'] or 'olqk+0l-a0yh@!yd%+#6-0n5l8k8&_^ncmo_4$q==2l$+@knq0'
ALLOWED_HOSTS = 'ALLOWED_HOSTS' in server_settings and server_settings['ALLOWED_HOSTS'] or ['0.0.0.0', '127.0.0.1', 'localhost', '.manifestationsportive.fr']
INTERNAL_IPS = ['127.0.0.1']
ADMIN_INSTANCE_IP = 'ADMIN_INSTANCE_IP' in server_settings  and server_settings['ADMIN_INSTANCE_IP'] or ['127.0.0.1']
# Ajout lors du passage à django 4, La vérification CSRF a échoué. La requête a été interrompue
# Autoriser l'origine manifsport en carte blanche csrf (cas du multiserver)
CSRF_TRUSTED_ORIGINS = ["https://*.manifestationsportive.fr","https://manifestationsportive.fr"]
# Debug
DEBUG = 'DEBUG' in server_settings and server_settings['DEBUG'] or False
TEMPLATE_DEBUG = 'DEBUG' in server_settings and server_settings['DEBUG'] or False


# WSGI et routage
ROOT_URLCONF = 'configuration.urls'
WSGI_APPLICATION = 'configuration.wsgi.application'

# Traduction et Régionalisation (https://docs.djangoproject.com/en/1.6/topics/i18n/)
TIME_ZONE = 'Europe/Paris'
LANGUAGE_CODE = 'fr'  # bootstrap3_datetime nécessite un code en 2 lettres
USE_I18N = True
USE_L10N = True
USE_TZ = True
DATETIME_INPUT_FORMATS = ['%d/%m/%Y %H:%M', '%d/%m/%Y %H:%M:%s']
DATE_INPUT_FORMATS = ['%d/%m/%Y']
DATE_FORMAT = "d/m/Y"
DATETIME_FORMAT = "d F Y H:i"

# Framework Sites (https://docs.djangoproject.com/en/1.6/ref/contrib/sites/)
SITE_ID = 1

ACCOUNT_DEFAULT_HTTP_PROTOCOL = "https"

# Fichiers statiques et fichiers uploadés (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
# https://docs.djangoproject.com/en/1.6/topics/files/
STATIC_ROOT_URL = '/static/'
STATIC_ROOT = 'STATIC_ROOT' in server_settings and server_settings['STATIC_ROOT'] or '/tmp/static/'
STATIC_URL = get_static_url_now(STATIC_ROOT_URL, True, SECRET_KEY) # permet de generer une adresse aléatoire pour les statics
# STATICFILES_DIRS = (
#     MACHINA_MAIN_STATIC_DIR,
# )
MEDIA_ROOT = 'MEDIA_ROOT' in server_settings and server_settings['MEDIA_ROOT'] or '/tmp/media/'
MEDIA_URL = '/media/'
LOCALE_PATHS = os.path.join('forum/locale'),
# Limite mémoire d'upload de fichier dans Django (mais pas dans Apache/Nginx)
FILE_UPLOAD_MAX_MEMORY_SIZE = 10485760  # 10 Mégaoctets
DATA_UPLOAD_MAX_MEMORY_SIZE = 15000000 # 15 mégaoctets

# BOF settings multiserver

MASTER_IP = 'MASTER_IP' in server_settings and server_settings['MASTER_IP'] or ['127.0.0.1:8000',] # Ip des servers executant l'application principale django
EXPORTATION_IP = 'EXPORTATION_IP' in server_settings and server_settings['EXPORTATION_IP'] or '127.0.0.1:8000' # IP du server executant l'exportation
# EOF settings multiserver


# Email backend (https://docs.djangoproject.com/en/1.6/topics/email/#email-backends)
EMAIL_BACKEND = ('EMAIL_BACKEND' in server_settings and server_settings['EMAIL_BACKEND'] == 'smtp') and 'django.core.mail.backends.smtp.EmailBackend' or 'post_office.EmailBackend'
EMAIL_HOST = 'EMAIL_HOST' in server_settings and server_settings['EMAIL_HOST'] or ''
EMAIL_PORT = 587
EMAIL_HOST_USER = 'EMAIL_HOST_USER' in server_settings and server_settings['EMAIL_HOST_USER'] or ''
EMAIL_HOST_PASSWORD = 'EMAIL_HOST_PASSWORD' in server_settings and server_settings['EMAIL_HOST_PASSWORD'] or ''
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'DEFAULT_FROM_EMAIL' in server_settings and server_settings['DEFAULT_FROM_EMAIL'] or 'plateforme@manifestationsportive.fr'

POST_OFFICE = {
    'BACKENDS': {
        'default': 'core.SMTPCustomBackend.SMTPCustomBackend',
    },
    "THREADS_PER_PROCESS": 1, # éviter que postoffice s'y perde entre differents threads au niveau des connections smtp, mis en place de plusieurs process lors de l'execution de la commande
    'CELERY_ENABLED': False,
}

# Utilisé par mail_admins. surcharge de la valeur défaut qui génère une erreur smtp
SERVER_EMAIL = 'DEFAULT_FROM_EMAIL' in server_settings and server_settings['DEFAULT_FROM_EMAIL'] or 'plateforme@manifestationsportive.fr'
ADMINS = [('Notif', 'notif@openscop.fr'), ]

# Vue CSRF utilisé en cas d'erreur
CSRF_FAILURE_VIEW = "core.views.csrf.csrf_failure"

# DATABASE SETTINGS
# =================
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'DATABASE_NAME' in server_settings and server_settings['DATABASE_NAME'] or 'default',
        'USER': 'DATABASE_USER' in server_settings and server_settings['DATABASE_USER'] or 'postgres',
        'PASSWORD': 'DATABASE_PASSWORD' in server_settings and server_settings['DATABASE_PASSWORD'] or '',
        'HOST': "DATABASE_HOST" in server_settings and server_settings["DATABASE_HOST"] or 'localhost',
        'PORT': 5432,
        'NUMBER': 42
    },
    'geoserver': {
         'ENGINE': 'django.contrib.gis.db.backends.postgis',
         'NAME': 'DATABASE_NAME_GEOSERVER' in server_settings and server_settings['DATABASE_NAME_GEOSERVER'] or 'geoserver',
         'USER': 'DATABASE_USER_GEOSERVER' in server_settings and server_settings['DATABASE_USER_GEOSERVER'] or 'postgres',
         'PASSWORD': 'DATABASE_PASSWORD_GEOSERVER' in server_settings and server_settings['DATABASE_PASSWORD_GEOSERVER'] or '',
         'HOST': "DATABASE_HOST_GEOSERVER" in server_settings and server_settings["DATABASE_HOST_GEOSERVER"] or 'localhost',
         'PORT': 5432,
         'NUMBER': 42
     }
}


# celery
CELERY_ENABLED = True
CELERY_BROKER = 'CELERY_BROKER' in server_settings and server_settings['CELERY_BROKER'] or 'amqp://guest:guest@localhost:5672'

FILE_UPLOAD_PERMISSIONS = 0o644

CKEDITOR_BROWSE_SHOW_DIRS = True
CKEDITOR_RESTRICT_BY_DATE = True
CKEDITOR_IMAGE_BACKEND = "ckeditor_uploader.backends.PillowBackend"

# CACHE SETTINGS
# ==============
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    },
    'machina_attachments': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/tmp',
    },
}

# DJANGO USER AND AUTH SETTINGS
# =============================
# Cookie
# SESSION_COOKIE_DOMAIN=".manifestationsportive.fr"

# Modèle utilisateur utilisé par Django (1.5+)
AUTH_USER_MODEL = 'core.user'

# Authentification (https://docs.djangoproject.com/en/1.6/ref/settings/#authentication-backends)
AUTHENTICATION_BACKENDS = (
    'axes.backends.AxesBackend',
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

# Password hashers (https://docs.djangoproject.com/en/1.6/ref/settings/#password-hashers)
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'core.util.hashers.DrupalPasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.UnsaltedMD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
)

# Validateur de mot de passe pour empecher des mot de passe peu sûr
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 9,
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# TEMPLATES & MIDDLEWARE
# ======================
# Configuration du moteur de templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            Paths.get_root_dir('portail', 'templates'),
            Paths.get_root_dir('templates'),
            Paths.get_root_dir('forum', 'templates',),

            MACHINA_MAIN_TEMPLATE_DIR,
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'core.processors.core.core',
                # Machina
                'forum.core.context_processors.metadata',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'debug': DEBUG,
        },
    },
]

# Middleware
MIDDLEWARE_MINI = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'core.ManifMiddleware.ManifMiddleware',
    'aide.middleware.context.ContextHelpMiddleware',
    'forum.forum_permission.middleware.ForumPermissionMiddleware',
    'axes.middleware.AxesMiddleware',
)

MIDDLEWARE_DEV = (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

MIDDLEWARE_PROD = ()

MIDDLEWARE = ('DEVELOPMENT_MODE' in server_settings and server_settings['DEVELOPMENT_MODE']) and (MIDDLEWARE_MINI + MIDDLEWARE_DEV) or (MIDDLEWARE_MINI + MIDDLEWARE_PROD)


# APPLICATIONS
# ============
INSTALLED_APPS_MINI = (
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'polymorphic',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.humanize',
    'django.contrib.gis',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.postgres',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django_extensions',
    'django_filters',
    'django_jenkins',
    'django_cron',
    'adminsortable2',
    'bootstrap_datepicker_plus',
    'ajax_select',
    'allauth',
    'allauth.account',
    'ckeditor',
    'ckeditor_uploader',
    'crispy_forms',
    'crispy_bootstrap5',
    'import_export',
    'fixture_magic',
    'oauth2_provider',
    'rest_framework',
    'rest_framework_gis',
    'drf_yasg',
    'administrative_division',
    'aide',
    'carto',
    'core',
    'evaluation_incidence',
    'evenements',
    'instructions',
    'portail',
    'sports',
    'clever_selects',
    'messagerie',
    'post_office',
    'webpush',
    'structure',
    "celery.contrib.testing.tasks",
    'axes',
    # Machina dependencies:
    # 'mptt',
    # 'haystack',
    'widget_tweaks',
    # Custom Forum
    'forum.forum',
    'forum.forum_conversation',
    'forum.forum_conversation.forum_polls',
    'forum.forum_moderation',
    'forum.forum_tracking',
    'forum.forum_member',
    'forum.forum_feeds',
    'forum.forms',
    'forum.forum_conversation.forum_attachments',
    'forum.forum_permission',
)

WEBPUSH_SETTINGS ={
    "VAPID_PUBLIC_KEY": "VAPID_PUBLIC_KEY" in server_settings and server_settings['VAPID_PUBLIC_KEY'] or "BMzJJa177GqcxhaQD1zn9a8k1BQO3wKGewH8j0HNTtF8sPNTCmI6rzSdhI6nvij95VR0KGiTXh3jn7AFUhDxrHA",
    "VAPID_PRIVATE_KEY": "VAPID_PRIVATE_KEY" in server_settings and server_settings['VAPID_PRIVATE_KEY'] or "JOP3unX8MDb1J9f8EQ0tlEHDrpPw_BSS1ths26Ug-Tc",
    "VAPID_ADMIN_EMAIL": "VAPID_ADMIN_EMAIL" in server_settings and server_settings['VAPID_ADMIN_EMAIL'] or "admin@example.com"
}

INSTALLED_APPS_DEV = (
    'debug_toolbar',
)
INSTALLED_APPS_PROD = (
)

INSTALLED_APPS = ('DEVELOPMENT_MODE' in server_settings and server_settings['DEVELOPMENT_MODE']) and (INSTALLED_APPS_MINI + INSTALLED_APPS_DEV) or (INSTALLED_APPS_MINI + INSTALLED_APPS_PROD)

# LOGGING
# ==================================

LOG_ROOT = 'LOG_ROOT' in server_settings and server_settings['LOG_ROOT'] or '/tmp/log/manifsport/'
if not os.path.exists(LOG_ROOT):
    os.makedirs(LOG_ROOT)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'carto_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': LOG_ROOT + 'carto.debug.log',
            'formatter': 'verbose'
        },
        'smtp_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': LOG_ROOT + 'smtp.debug.log',
            'formatter': 'verbose'
        },
        'api_file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': LOG_ROOT + 'api.info.log',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
            'email_backend': 'django.core.mail.backends.smtp.EmailBackend',
        },
    },
    'formatters': {
        'verbose': {'format': '%(levelname)s %(asctime)s %(module)s (pid %(process)d) %(message)s'},
        'simple': {'format': '%(levelname)s | %(asctime)s | %(message)s'},
    },
    'loggers': {
        'carto': {
            'handlers': ['carto_file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'smtp': {
            'handlers': ['smtp_file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'api': {
            'handlers': ['api_file'],
            'level': 'INFO',
            'propagate': True,
        },
        'django': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
    },
}


# APPLICATION MANIFESTATION SPORTIVE
# ==================================
# URL principale du site.
MAIN_URL = 'MAIN_URL' in server_settings and server_settings['MAIN_URL'] or 'http://localhost:8000/'


# Paramètre de maintenance : désactiver la création auto d'avis etc.
DISABLE_SIGNALS = False

# Gestion des dates pour campagne (définie manuellement) de modification des mots de passe
DATE_CHANGE_PASSWORD = "17/06/2020"  # date à partir de laquelle le mot de passe doit être changé (affichage formulaire nouveau mot de passe).
DATE_INIT_PASSWORD = "30/06/2020"  # date à laquelle le mot de passe sera changé (envoi par mail du lien pour nouveau mot de passe.

# TESTS
# =====
TESTS_IN_PROGRESS = False
if 'test' in sys.argv[1:] or 'jenkins' in sys.argv[1:]:
    logging.disable(logging.CRITICAL)
    DEBUG = True  # Todo: ce réglage fait que Travis effectue les tests avec le Debug actif. Ceci nous éloigne de la configuration de la production. Etudier une manière différente.
    TEMPLATE_DEBUG = False
    TESTS_IN_PROGRESS = True
    MIGRATION_MODULES = DisableMigrations()
    DATABASES = {k: v for k, v in DATABASES.items() if k == 'default'}
    DISABLE_SIGNALS = False
    DATE_CHANGE_PASSWORD = ""
    CELERY_ENABLED = False


# FUSION des bases / LEGACY
# =========================
if 'full_merge' in sys.argv[1:] or 'import_external' in sys.argv[1:]:
    DISABLE_SIGNALS = True  # Ne surtout pas conserver le comportement des signaux lors de l'importation

# PROTECTION de base du code sensible
# ===================================
PROTECT_CODE = False  # Certaines commandes ne peuvent pas s'exécuter si == True

# SETTINGS RUNSERVER
# ==================
for arg in sys.argv[1:]:
    if 'runserver' in arg:
        DISABLE_SIGNALS = False
        DATE_CHANGE_PASSWORD = ""

# Empêcher l'effacement de .thirdparty lors du nettoyage automatique des imports
for key in thirdparty.__dict__:
    if key == key.upper():
        locals()[key] = getattr(thirdparty, key)

# CRISPY
# ======
CRISPY_ALLOWED_TEMPLATE_PACKS = "bootstrap5"

CRISPY_TEMPLATE_PACK = "bootstrap5"

# crontab classes
"""
Les crons ont un classement au vu de l'ordre de lancement des crons.
Tout d'abord tout cron envoyant un email.
Ensuite les crons les plus rapide
Pour finir par les plus longs  qui ne sont pas des emails.
"""
CRON_CLASSES = [
    "instructions.cronjob.RelanceAvis",            # 7h05
    "instructions.cronjob.RelanceAction",          # 7h25
    "instructions.cronjob.TraitementDossier",      # 5h05
    "messagerie.cronjob.SendRecap",              # 7h45
    "structure.cronjob.RelanceReunionCDSR",        # 6h00
    "core.cronjob.RenvoyerConfirmationMail",       # 5h30
    "structure.cronjob.UpdateInvitation",          # 6h05
    "core.cronjob.EmailAdresseValide",             # 6h25
    "evenements.cronjob.CompletudeDossier",      # 6h45

    "messagerie.cronjob.UpdateCPT",              # toutes les minutes
    "core.cronjob.RelanceScanAntivirus",           # toutes les heures
    "evenements.cronjob.UpdateDelai",             # 6h25

    "core.cronjob.UpdateStat",                      # 0h00
    "evenements.cronjob.DeleteFichierExportation",  # toutes les 15 min
    "core.cronjob.DeleteFlagFile",                   # toutes les heures
    "core.cronjob.DeleteExportationFile",            # toutes les heures
    "core.cronjob.ErreurSMTPEmail",                # 6h25
    "core.cronjob.NotificationPbEmail",              # 6h25
]


MESSAGERIE_CPT_TIME_REFRESH = 1  # minute paramètre interne

SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_SECURE = False if DEBUG else True
SESSION_COOKIE_AGE = 4 * 60 * 60  # 4 heures
INACTIVE_TIME = 15  # minutes, paramètre interne

# Exceptions logging :
# https://sentry.io/Openscop/prodmanifsport3/
SENTRY_CONFIG = 'SENTRY_CONFIG' in server_settings and server_settings['SENTRY_CONFIG'] or ''
ENVIRONNEMENT = 'ENVIRONNEMENT' in server_settings and server_settings['ENVIRONNEMENT'] or 'développement'
if SENTRY_CONFIG:
    sentry_sdk.init(
        dsn=SENTRY_CONFIG,
        integrations=[DjangoIntegration(), CeleryIntegration()],
        send_default_pii=True,
        release=VERSION,
        environment=ENVIRONNEMENT
    )

# CARTOGRAPHIE SETTINGS
CARTO_OVERLAY_SERVICE_URL = 'CARTO_OVERLAY_SERVICE_URL' in server_settings and server_settings['CARTO_OVERLAY_SERVICE_URL'] or 'https://carto.manifestationsportive.fr/geoserver/cite/ows'

# OPENRUNNER settings
OPENRUNNER_ROUTING_ROUTE = 'CARTO_ROUTING_WRAPPER_SERVICE_URL' in server_settings and server_settings['CARTO_ROUTING_WRAPPER_SERVICE_URL'] or 'http://localhost:4000/routing/direction'
OPENRUNNER_POIS_ROUTE = 'OPENRUNNER_POIS_ROUTE' in server_settings and server_settings['OPENRUNNER_POIS_ROUTE'] or 'http://localhost:4000/settings/pois'

# django-ajax-selects
# ======
# Empêcher le chargement de dépendances jQuery et jQueryUI obsolètes
AJAX_SELECT_BOOTSTRAP = False


# Configuration de l'API
# =======================
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
            'rest_framework.renderers.JSONRenderer',
            'rest_framework.renderers.BrowsableAPIRenderer',
        ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
        'rest_framework.authentication.SessionAuthentication',
        ),
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAuthenticated', ),
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend', ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 30
}
OAUTH2_PROVIDER = {
    # this is the list of available scopes
    'OAUTH2_BACKEND_CLASS': 'oauth2_provider.oauth2_backends.JSONOAuthLibCore',
    'SCOPES': {'read': 'Read scope', 'write': 'Write scope', 'groups': 'Access to your groups'}
}
SWAGGER_SETTINGS = {
    'DEFAULT_MODEL_RENDERING': 'example',
   'SECURITY_DEFINITIONS': {
      'Bearer': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
      }
   }
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'


MACHINA_MARKUP_WIDGET = 'forum.forms.widgets.MarkdownTextareaWidget'

MACHINA_TOPIC_POSTS_NUMBER_PER_PAGE = 12

# Configurer a 10000 pour empecher qu'il interfaire avec le nouveau sytéme de pagination
MACHINA_FORUM_TOPICS_NUMBER_PER_PAGE = 10000

MACHINA_USER_DISPLAY_NAME_METHOD = 'get_full_name'

MACHINA_ATTACHMENT_FILE_UPLOAD_TO = 'forum'

# Configuration de django axe pour sécuriser contre les attaques brute force
if TESTS_IN_PROGRESS:
    AXES_ENABLED = False
if DEBUG:
    AXES_COOLOFF_TIME = 0.01
else:
    AXES_COOLOFF_TIME = 0.1
AXES_VERBOSE = False
AXES_LOCKOUT_TEMPLATE = "portail/tentative_connexion_exceder.html"
AXES_DISABLE_ACCESS_LOG = True


# Configuration des logins de la boite mail pour le cron erreur smtp
MANIF_COMPTE_EMAIL = 'MANIF_EMAIL' in server_settings and server_settings['MANIF_EMAIL'] or '',
MANIF_COMPTE_EMAIL_PASSWORD = 'MANIF_EMAIL_PASSWORD' in server_settings and server_settings['MANIF_EMAIL_PASSWORD'] or '',